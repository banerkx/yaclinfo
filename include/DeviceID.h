
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#pragma GCC diagnostic ignored "-Wignored-attributes"

#include <algorithm>
#include <memory>
#include <string>

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

class DeviceID
{
  public:

    DeviceID() noexcept;

    ~DeviceID() noexcept;

    void reset(
      const cl_platform_id          tPlatformID,
      const cl_device_type          eDeviceType,
      const cl_uint                 nNumberOfDevices,
      std::shared_ptr<cl_device_id> pDeviceIds,
      const std::string&            sErrorMessage = std::string()) noexcept;

    void resetForError(
      const cl_platform_id tPlatformID,
      const cl_device_type eDeviceType,
      const std::string&   sError) noexcept;

    inline const std::string& getError() const noexcept
    {
      return (m_sErrorMessage);
    }

    cl_device_id operator[](const size_t nIndex);

    inline cl_uint size() const noexcept
    {
      return (m_nNumberOfDevices);
    }

    static void deleteDeviceIDArray(cl_device_id*& p);

  private:

    cl_platform_id                m_tPlatformID;
    cl_device_type                m_eDeviceType;
    cl_uint                       m_nNumberOfDevices;
    std::shared_ptr<cl_device_id> m_pDeviceIds;
    std::string                   m_sErrorMessage;

    static void deleteDeviceIDMem(cl_device_id* p, const cl_uint nSize);

    // Intentionally deleted.
    DeviceID(const DeviceID& rhs) = delete;
    DeviceID& operator=(const DeviceID& rhs) = delete;
    DeviceID& operator=(DeviceID&& rhs) = delete;
    DeviceID(DeviceID&& rhs)            = delete;
    DeviceID*       operator&()         = delete;
    const DeviceID* operator&() const   = delete;
};

