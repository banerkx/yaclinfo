
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GIT_MK_INCLUDE_GUARD # {
GIT_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# Function to add real and links targets to .gitignore.                        #
# $(1) - real target or link                                                   #
################################################################################
GIT_IGNORE_FILE ?= $(CURDIR)/.gitignore
define add_to_gitignore
  if [[ -e $(GIT_IGNORE_FILE) ]];                                                       \
  then                                                                                  \
    if [[ ! -w $(GIT_IGNORE_FILE) ]];                                                   \
    then                                                                                \
      echo '$(ERROR_LABEL) The git ignore file [$(GIT_IGNORE_FILE)] is not writeable.'; \
      exit 1;                                                                           \
    fi;                                                                                 \
    grep -q "^$(1)$$" $(GIT_IGNORE_FILE);                                               \
    if [[ $${?} -ne 0 ]];                                                               \
    then                                                                                \
      echo $(1) >> $(GIT_IGNORE_FILE);                                                  \
    fi;                                                                                 \
  else                                                                                  \
    echo $(1) >> $(GIT_IGNORE_FILE);                                                    \
  fi;
endef

################################################################################
# Function to delete phony targets from .gitignore.                            #
# $(1) - phony target                                                          #
################################################################################
define delete_from_gitignore
  if [[ -e $(GIT_IGNORE_FILE) ]];                                                       \
  then                                                                                  \
    if [[ ! -w $(GIT_IGNORE_FILE) ]];                                                   \
    then                                                                                \
      echo '$(ERROR_LABEL) The git ignore file [$(GIT_IGNORE_FILE)] is not writeable.'; \
      exit 1;                                                                           \
    fi;                                                                                 \
    grep -q '^$(1)$$' $(GIT_IGNORE_FILE);                                               \
    if [[ $${?} -eq 0 ]];                                                               \
    then                                                                                \
      sed -i '\:^$(1)$$:d' $(GIT_IGNORE_FILE);                                          \
    fi;                                                                                 \
  fi;
endef

################################################################################
# Recipe to update .gitignore.                                                 #
################################################################################
MAKE_OPTS ?= -pnrRw
LINKS=$(shell find . -type l | sed 's/^\.\///')
TARGETS=$(shell $(MAKE) $(MAKE_OPTS) | sed '/^make\[1\]:/d' | sed '/^\# Not a target/,+1d' | grep '^[A-Z,a-z,/,0-9].*:' | sed '/=/d;/"/d;s/:.*$$//;/^make$$/d;/%/d;/^all$$/d' | sort -u | tr '\n' ' ')
PHONY_TARGETS=$(shell $(MAKE) $(MAKE_OPTS) | grep "^\.PHONY" | sed -e '/^\.PHONY: \$$/d' -e 's/^\.PHONY: //')
REAL_TARGETS=$(filter-out $(PHONY_TARGETS), $(TARGETS))
.PHONY: gitignore
gitignore:
ifneq ($(GIT_IGNORE_FILE),) # {
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'LINKS           = [$(LINKS)]'
	@echo 'TARGETS         = [$(TARGETS)]'
	@echo 'PHONY_TARGETS   = [$(PHONY_TARGETS)]'
	@echo 'REAL_TARGETS    = [$(REAL_TARGETS)]'
	@echo 'GIT_IGNORE_FILE = [$(GIT_IGNORE_FILE)]'
	@touch $(GIT_IGNORE_FILE)
	@$(foreach target,$(REAL_TARGETS),$(call add_to_gitignore,$(target)))
	@$(foreach phony_target,$(PHONY_TARGETS),$(call delete_from_gitignore,$(phony_target)))
	@$(foreach link,$(LINKS),$(call add_to_gitignore,$(link)))
################################################################################
# Now those paths that may lie outside of $(CURDIR) must be deleted from       #
# $(GIT_IGNORE_FILE).                                                          #
################################################################################
	@while IFS= read -r GITIGNORE_ENTRY;                           \
   do                                                            \
     GITIGNORE_ENTRY_DIR=$$(dirname $${GITIGNORE_ENTRY});        \
     if [[ -d $${GITIGNORE_ENTRY_DIR} ]];                        \
     then                                                        \
       FULL_GITIGNORE_ENTRY=$$(readlink -f $${GITIGNORE_ENTRY}); \
     else                                                        \
       FULL_GITIGNORE_ENTRY=$(CURDIR)/$${GITIGNORE_ENTRY};       \
     fi;                                                         \
     if [[ ! $${FULL_GITIGNORE_ENTRY} =~ $(CURDIR)/.* ]];        \
     then                                                        \
       $(call delete_from_gitignore,$${GITIGNORE_ENTRY})         \
     fi;                                                         \
   done < $(GIT_IGNORE_FILE)
	@sed -i "s@$(CURDIR)/@@" $(GIT_IGNORE_FILE)
	@sort -u $(GIT_IGNORE_FILE) -o $(GIT_IGNORE_FILE)
else # } {
	@$(error $(ERROR_LABEL) The GIT_IGNORE_FILE variable is not defined)
endif # }

################################################################################
# Lists files in the current directory ignored by git.                         #
################################################################################
.PHONY: gitnored
gitnored:
	@git check-ignore * .* || :

################################################################################
# Lists files in the current directory and its subdirectories ignored by git.  #
################################################################################
.PHONY: gitnoredr
gitnoredr:
	@git check-ignore */* .* || :

GIT_BRANCH:=$(shell git branch --show-current 2> /dev/null || echo 'UNKNOWN')
GIT_COMMIT:=$(shell git log --pretty=format:'%H' -n 1 2> /dev/null || echo 'UNKNOWN')
GIT_REMOTE_REPO=$(shell { git config --get remote.origin.url 2> /dev/null || echo 'UNKNOWN'; } | sed 's|^.*@|https://|' | sed 's@\(^https:\)\(.*\)\(:\)\(.*\)@\1\2/\4@')
GIT_ROOT:=$(subst ${HOME},$${HOME},$(shell git rev-parse --show-toplevel 2> /dev/null || echo 'UNKNOWN'))
GIT_VERSION=$(call get_stdout_version,git,--version,| sed 's/^.* //')

################################################################################
# Finds the HEAD commit hash for the remote repo.                              #
#                                                                              #
# $(1) - git remote repo URL                                                   #
################################################################################
GIT_LS_REMOTE=git ls-remote --quiet
git_head_remote_hash=$(if $(1),$(shell $(GIT_LS_REMOTE) $(1) HEAD | cut -f1))

platform_info::
	@echo 'git branch                      = [$(GIT_BRANCH)]'
	@echo 'git remote repository           = [$(GIT_REMOTE_REPO)]'
	@echo 'git root                        = [$(GIT_ROOT)]'
	@echo 'git submodules                  = [$(GIT_SUBMODULES)]'
	@echo 'git version                     = [$(GIT_VERSION)]'

ifneq ($(GIT_ROOT),UNKNOWN) # {
GIT_SUBMODULES:=$(shell cd $(GIT_ROOT); git submodule --quiet foreach --recursive 'echo $$displaypath'  2> /dev/null | xargs realpath 2> /dev/null)
endif # }
ifeq ($(GIT_SUBMODULES),) # {
GIT_SUBMODULES:=NONE
endif # }

.PHONY: run_gitstats
run_gitstats:
	@echo   'Branch                    > [$(GIT_BRANCH)]'
	@echo   "Last Commit               > [$$(git log --pretty=format:'%H' -n 1)]"
	@echo   "Last Local Commit         > [$$(git log -1 --format="%cd %cr")]"
	@printf "Number Of Local Branches  > [%'d]\n" $$(git branch | wc -l)
	@printf "Number Of Remote Branches > [%'d]\n" $$(git ls-remote 2> /dev/null | grep -v $$'\tHEAD$$' | wc -l)
	@printf "Number Of Tracked Files   > [%'d]\n" $$(cd $(GIT_ROOT); git ls-files | wc -l)
	@echo   'Project Root              > [$(GIT_ROOT)]'
	@echo   'Remote url                > [$(GIT_REMOTE_REPO)]'
	@printf "Stash Count               > [%'d]\n" $$(git stash list | wc -l)
	@echo   'Submodules                > [$(GIT_SUBMODULES)]'
	@printf "Total Commit Count        > [%'d]\n" $$(git rev-list HEAD --count)
	@echo   "User Name                 > [$$(git config user.name)]"
	@echo   "User e-mail               > [$$(git config user.email)]"

.PHONY: git_submodules
git_submodules:
ifneq ($(GIT_SUBMODULES),NONE) # {
	@{ echo '|Submodule |Latest Commit Date |Latest Commit |URL |';                                                                                                                   \
   for SUB_FILE in $$(find $(GIT_ROOT) -type f -name "\.gitmodules" | sort);                                                                                                        \
   do                                                                                                                                                                               \
     for SUB in $$(grep "^\[submodule .*\]$$" $${SUB_FILE} | sed 's/\(^.*"\)\(.*\)\("\]$$\)/\2/');                                                                                  \
     do                                                                                                                                                                             \
       read -r SUB_PATH URL <<< $$(gawk "/^\[submodule \"$${SUB//\//\\/}\"\]$$/{getline;print;getline;print;}" $${SUB_FILE} | sed 's/^.*= //' | tr '\n' ' ');                       \
       SUB_PATH=$$(dirname $${SUB_FILE})/$${SUB_PATH};                                                                                                                              \
       echo "|$${SUB_PATH} | $$(cd $${SUB_PATH}; git log --pretty=format:'%ad %H' -n1 |  gawk '{printf("%s %s %s %s %s %s |%s |", $$1, $$2, $$3, $$4, $$5, $$6, $$7);}')$${URL} |"; \
     done                                                                                                                                                                           \
   done; } | gawk 'NR == 1; NR > 1 { print $$0 }' | column -s"|" -o"|" -t
else # } {
	@echo "No git submodules found."
endif # }

.PHONY: gitstats
gitstats:
	+@$(MAKE_NO_DIR) run_gitstats | sort | column -s">" -o":" -t

GIT_DOWNLOAD=https://mirrors.edge.kernel.org/pub/software/scm/git/
GIT_LATEST_VERSION=$(shell { curl -s $(GIT_DOWNLOAD) 2> /dev/null; echo UNKNOWN; } | egrep '(UNKNOWN|href=.*git-)' | egrep -v '(core|htmldocs|manpages|\.tar\.sign)' | sed 's/^.*git-//;s/\.tar.*//' | sort -uV | tail -2 | head -1)

.PHONY: show_updates
show_updates::
	@$(call check_updates,git,$(GIT_DOWNLOAD),$(GIT_VERSION),$(GIT_LATEST_VERSION))

GIT_ARCHIVE=git archive
GIT_ARCHIVE_FORMAT ?= tar.gz
GIT_ARCHIVE_OUTPUT ?= ./$(notdir $(CURDIR)).$(GIT_ARCHIVE_FORMAT)
GIT_ARCHIVE_REF    ?= HEAD

.PHONY: git_archive
git_archive:
	@$(GIT_ARCHIVE) --output=$(GIT_ARCHIVE_OUTPUT) --format=$(GIT_ARCHIVE_FORMAT) $(GIT_ARCHIVE_REF)

.PHONY: git_archive_formats
git_archive_formats:
	@$(GIT_ARCHIVE) --list | sort

################################################################################
# This function assembles the https link to the web page for the remote repo,  #
# including the branch, by:                                                    #
#   - getting the local git branch                                             #
#   - getting the url to the remote repo                                       #
#   - NOTE: the url is expected to resemble one of the following:              #
#       git@github.com:user/XYZ.git             (ssh protocol)                 #
#       https://github.com/user/XYZ.git         (https protocol)               #
#       https://user@bitbucket.org/user/XYZ.git (https protocol)               #
#   - we check to see if the local branch is tracked by the remote repo        #
#   - we reformat the url to use https, ending up with the base url            #
#   - we set GIT_REMOTE_BRANCH_URL to the base url                             #
#   - if the local branch is tracked by the remote repo, we append             #
#       "/[project root]/[branch name]" to the base url                        #
################################################################################
ifneq ($(GIT_REMOTE_REPO),UNKNOWN) # {
get_remote_branch_url=$(shell GIT_LOCAL_BRANCH=$$(git branch | grep "^\* " | sed 's/^\* //');                                                                                               \
                              GIT_WEB_REPO=$$(git config --get remote.origin.url | sed "s/\.git$$//;s@^https://@@;s/^.*@//;s@:@/@;s@^@https://@");                                          \
                              GIT_LOCAL_BRANCH=$$(git show-branch --list -r | grep " \[origin/$${GIT_LOCAL_BRANCH}\] " | sed "s@\(^.* \[origin/\)\($${GIT_LOCAL_BRANCH}\)\(\] .*$$\)@\2@"); \
                              GIT_REMOTE_BRANCH_URL=$${GIT_WEB_REPO};                                                                                                                       \
                              if [[ -n "$${GIT_LOCAL_BRANCH}" ]];                                                                                                                           \
                              then                                                                                                                                                          \
                                PROJECT_ROOT=$$(((echo $${GIT_WEB_REPO} | grep -q bitbucket) && echo 'src' ) || echo 'tree');                                                               \
                                GIT_REMOTE_BRANCH_URL=$${GIT_WEB_REPO}/$${PROJECT_ROOT}/$${GIT_LOCAL_BRANCH};                                                                               \
                              fi;                                                                                                                                                           \
                              echo $${GIT_REMOTE_BRANCH_URL})
endif # }

################################################################################
# Creates a link for the specified git submodule url.                          #
#                                                                              #
# $(1) - submodule url                                                         #
################################################################################
define create_submodule_link
echo '| $(call create_url_link,$(1)) |';
endef

clean::
	@rm -f $(filter-out --output,$(subst =, ,$(GIT_ARCHIVE_OUTPUT)))

.PHONY: git_mk_help
git_mk_help::
	@echo '$(MAKE) git_archive → create a git archive'
	@echo '$(MAKE) gitignore → a heuristic to create a .gitignore file'
	@echo '$(MAKE) gitnored → shows files ignored by git in the current directory'
	@echo '$(MAKE) gitnoredr → shows files ignored by git recursively starting with the current directory'
	@echo '$(MAKE) gitstats → displays some git statistics/information'

show_help:: git_mk_help

endif # }

