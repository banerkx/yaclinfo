
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PYTHON_MK_INCLUDE_GUARD # {
PYTHON_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including vars_common.mk.                  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

PYTHON=python3
PYTHON_COMMAND=$(shell command -v $(PYTHON))
ifeq ($(PYTHON_COMMAND),) # {
$(error $(ERROR_LABEL) Could not find the [$(PYTHON)] executable)
endif # }

PYTHON_VERSION=$(call get_stdout_version_index,python,--version,2)
MIN_PYTHON_VERSION=3.5
ifeq ($(call ver_greater_than_eq,$(PYTHON_VERSION),$(MIN_PYTHON_VERSION)),F) # {
$(error $(ERROR_LABEL) The current Python version = [$(PYTHON_VERSION)]; need version ≥ [$(MIN_PYTHON_VERSION)])
endif # }

PIP=pip3
PIP_COMMAND:=$(shell command -v $(PIP))
PIP_VERSION=$(shell { $(PIP) --version 2> /dev/null || echo UNKNWON; } | sed 's/\(^pip.* \)\([1-9].*\)\( from.*$$\)/\2/')

################################################################################
# Determines if the input package is missing. If it is missing, its name is    #
# added to MISSING_PYTHON_PACKAGES.                                            #
# NOTE: $(PYTHON_PACKAGES) must be set **before** including this makefile.     #
#                                                                              #
# $(1) - Python package                                                        #
################################################################################
MISSING_PYTHON_PACKAGES=
define is_python_package_missing
MISSING_PYTHON_PACKAGES += $(shell $(PYTHON) -c "import $(1)" 2> /dev/null || $(PYTHON) -c "import $$(echo $(1) | tr '[:upper:]' '[:lower:]')" 2> /dev/null || echo $(1))
endef
$(foreach package, $(PYTHON_PACKAGES), $(eval $(call is_python_package_missing,$(package))))
MISSING_PYTHON_PACKAGES:=$(strip $(MISSING_PYTHON_PACKAGES))
ifneq ($(MISSING_PYTHON_PACKAGES),) # {
$(error $(ERROR_LABEL) Missing the following $(PYTHON) packages: $(call setify,$(MISSING_PYTHON_PACKAGES)))
endif # }

################################################################################
# Gets the full path of the input python script by examining the directories   #
# in ${PYTHONPATH}.                                                            #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_python_script=$(firstword $(wildcard $(addsuffix /$(1),$(subst :, ,${PYTHONPATH}))))

################################################################################
# Imports the input python module; if successful, the result is 0. Otherwise   #
# the result is 1.                                                             #
#                                                                              #
# $(1) - python script or module                                               #
################################################################################
import_python_module=$(if $(1),$(shell $(PYTHON_COMMAND) -c "import $(subst .py,,$(1))" 2> /dev/null; echo $${?}))

################################################################################
# Gets the version of the installed python package by using "pip show".        #
#                                                                              #
# $(1) - installed python package                                              #
################################################################################
get_python_package_version=$(shell { $(PIP) show $(1) 2> /dev/null || echo 'UNKNOWN'; } | grep -E "^(Version: |UNKNOWN)" | cut -d" " -f2)

################################################################################
# Gets the version of the installed python package by trying to import the     #
# package.                                                                     #
#                                                                              #
# $(1) - installed python package                                              #
################################################################################
get_python_package_version_using_import=$(eval $(1)_pkg=$(firstword $(subst ., ,$(1))))$(shell { python -c "import $($(1)_pkg); print($($(1)_pkg).__version__)" 2> /dev/null || echo 'UNKNOWN'; })

################################################################################
# Creates a target to display information for the input python package.        #
#                                                                              #
# $(1) - package                                                               #
################################################################################
define create_pip_show_target
$(1)_LOG=$(PIP)_$(1).log
PIP_LOGS += $$($(1)_LOG)
.PHONY: pip_$(1)
pip_$(1):
ifeq ($(PIP_COMMAND),) # {
	@:
	@$$(warning $(WARNING_LABEL) For target [$$(@)], the [$(PIP)] command is not available.)
else # } {
	@$(PIP) show $(1) 2> /dev/null | tee $$($(1)_LOG)
	@echo '$(EDITOR) $$($(1)_LOG)'
endif # }
endef

################################################################################
# Creates a target to display information for the input python package,        #
# including the files for the package.                                         #
#                                                                              #
# $(1) - package                                                               #
################################################################################
PIP_SHOW_OPTS=--files
define create_pip_show_files_target
$(1)_FILES_LOG=$(PIP)_files_$(1).log
PIP_LOGS += $$($(1)_FILES_LOG)
.PHONY: pip_files_$(1)
pip_files_$(1):
ifeq ($(PIP_COMMAND),) # {
	@:
	@$$(warning $(WARNING_LABEL) For target [$$(@)], the [$(PIP)] command is not available.)
else # } {
	@$(PIP) show $(PIP_SHOW_OPTS) $(1) 2> /dev/null | tee $$($(1)_FILES_LOG)
	@echo '$(EDITOR) $$($(1)_FILES_LOG)'
endif # }
endef

$(foreach package, $(PYTHON_PACKAGES), $(eval $(call create_pip_show_target,$(package))))
$(foreach package, $(PYTHON_PACKAGES), $(eval $(call create_pip_show_files_target,$(package))))
################################################################################
# Pattern rules for flagging an invalid python package.                        #
################################################################################
.PHONY: pip_%
pip_%:
	@$(error $(ERROR_LABEL) For target [$(@)], [$(*)] is an invalid python package. The valid python packages are: $(call setify,$(PYTHON_PACKAGES)))
.PHONY: pip_files_%
pip_files_%:
	@$(error $(ERROR_LABEL) For target [$(@)], [$(*)] is an invalid python package. The valid python packages are: $(call setify,$(PYTHON_PACKAGES)))

################################################################################
# Target that displays information ∀ packages in $(PYTHON_PACKAGES).           #
################################################################################
PIP_LOG=$(PIP).log
PIP_LOGS += $(PIP_LOG)
.PHONY: pips
pips:
ifeq ($(PIP_COMMAND),) # {
	@:
	@$(warning $(WARNING_LABEL) For target [$(@)], the [$(PIP)] command is not available.)
else # } {
	@echo '$(PYTHON) Packages Size = [$(words $(PYTHON_PACKAGES))]' | tee $(PIP_LOG)
	@echo '$(PYTHON) Packages      = [$(PYTHON_PACKAGES)]'          | tee -a $(PIP_LOG)
	@echo '---------------------------------------'                 | tee -a $(PIP_LOG)
	@pip3 show $(PYTHON_PACKAGES) 2> /dev/null                      | tee -a $(PIP_LOG)
	@echo '$(EDITOR) $(PIP_LOG)'
endif # }

.PHONY: pyvers
PACKAGE_VERSIONS_LOG=package_versions.log
PIP_LOGS += $(PACKAGE_VERSIONS_LOG)
pyvers:
ifeq ($(PIP_COMMAND),) # {
	@:
	@$(warning $(WARNING_LABEL) For target [$(@)], the [$(PIP)] command is not available.)
else # } {
	@$(PIP) show $(PYTHON_PACKAGES) 2> /dev/null | \
   egrep "^(Name|Version): "                   | \
   sed 's/^.*: //'                             | \
   sed 'N;s/\n/ version → /'                   | \
   column -s" " -o" " -t                       | \
   column -s"→" -o"→" -t                       | \
   sort                                        | \
   tee $(PACKAGE_VERSIONS_LOG)
	@echo '$(EDITOR) $(PACKAGE_VERSIONS_LOG)'
endif # }

################################################################################
# Function to verify that all the Python script(s) in $(1) exist.              #
#                                                                              #
# $(1) - list of Python scripts                                                #
################################################################################
define check_python_scripts_existence
$(if $(filter-out $(wildcard $(1)),$(1)),$(error $(ERROR_LABEL) The following Python script(s) were not found $(call setify,$(filter-out $(wildcard $(1)),$(1)))))
endef

pip_clean:
	@rm -f $(PIP_LOGS)

clean:: pip_clean

PYTHON_BLACK=black
PYTHON_BLACK_COMMAND=$(shell command -v $(PYTHON_BLACK))
PYTHON_BLACK_VERSION=$(call get_python_package_version,$(PYTHON_BLACK))
PYTHON_BLACK_URL=https://github.com/psf/black
.PHONY: py_black
py_black:
	@$(if $(PYTHON_BLACK_COMMAND),                                                                                 \
     $(if $(PYTHON_SCRIPTS),                                                                                     \
       $(foreach py,$(PYTHON_SCRIPTS),python -m $(PYTHON_BLACK) $(py);),                                         \
       : $(warning $(WARNING_LABEL) No python scripts found in [PYTHON_SCRIPTS].)),                              \
     $(error $(ERROR_LABEL) The python black command [$(PYTHON_BLACK)] was not found; see: $(PYTHON_BLACK_URL)))

PYTHON_JSON=json.tool
PYTHON_JSON_VERSION=$(call get_python_package_version_using_import,$(PYTHON_JSON))

PYTHON_MDFORMAT=mdformat
PYTHON_MDFORMAT_VERSION=$(call get_python_package_version,$(PYTHON_MDFORMAT))

PYTHON_YAMLFIX=yamlfix
PYTHON_YAMLFIX_VERSION=$(call get_python_package_version,$(PYTHON_YAMLFIX))

PYTHON_YAMLLINT=yamllint
PYTHON_YAMLLINT_VERSION=$(call get_python_package_version,$(PYTHON_YAMLLINT))

.PHONY: python_mk_info
python_mk_info:
	@echo 'PYTHONPATH                 = [$(subst ${HOME},$${HOME},$(subst $(escaped_space),:,$(realpath $(subst :, ,${PYTHONPATH}))))]'
	@echo '$(PIP) version             = [$(PIP_VERSION)]'
	@echo '$(PYTHON) version          = [$(PYTHON_VERSION)]'
	@echo '$(PYTHON_BLACK) version    = [$(PYTHON_BLACK_VERSION)]'
	@echo '$(PYTHON_JSON) version     = [$(PYTHON_JSON_VERSION)]'
	@echo '$(PYTHON_MDFORMAT) version = [$(PYTHON_MDFORMAT_VERSION)]'
	@echo '$(PYTHON_YAMLFIX) version  = [$(PYTHON_YAMLFIX_VERSION)]'
	@echo '$(PYTHON_YAMLLINT) version = [$(PYTHON_YAMLLINT_VERSION)]'

platform_info:: python_mk_info

.PHONY: python_mk_help
python_mk_help::
	@echo '$(MAKE) pip_% → shows information for the python package % and % ∈ $(call setify,$(PYTHON_PACKAGES))'
	@echo '$(MAKE) pip_files_% → shows information for the python package %, including the package files, and % ∈ $(call setify,$(PYTHON_PACKAGES))'
	@echo '$(MAKE) pips → shows information ∀ python packages in $(call setify,$(PYTHON_PACKAGES)), saving the output to [$(PIP_LOG)]'
	@echo '$(MAKE) pyvers → prints version numbers for the Python packages in $(call setify,$(PYTHON_PACKAGES)) and saves them to the file [$(PACKAGE_VERSIONS_LOG)]'
ifneq ($(PYTHON_BLACK_COMMAND),)
ifneq ($(PYTHON_SCRIPTS),)
	@echo '$(MAKE) py_black → runs $(PYTHON_BLACK) on each python script in [$(call setify,$(PYTHON_SCRIPTS))]'
endif
endif

show_help:: python_mk_help

endif # }

