
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef OPENGL_MK_INCLUDE_GUARD # {
OPENGL_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including vars_common.mk.                  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including platform.mk.                     #
################################################################################
ifeq ($(PLATFORM_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/platform.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# Dynamic libraries.                                                           #
################################################################################
LIB_GL=GL
LIB_GLEW=GLEW
LIB_GLU=GLU
LIB_GLUT=glut
LIB_X11=X11
LIB_XMU=Xmu

DYNAMIC_OPENGL_LIBS += $(LIB_GL)
DYNAMIC_OPENGL_LIBS += $(LIB_GLEW)
DYNAMIC_OPENGL_LIBS += $(LIB_GLU)
DYNAMIC_OPENGL_LIBS += $(LIB_GLUT)
DYNAMIC_OPENGL_LIBS += $(LIB_X11)
DYNAMIC_OPENGL_LIBS += $(LIB_XMU)

################################################################################
# Static libraries.                                                            #
################################################################################
LIB_GLFW3=glfw3

STATIC_OPENGL_LIBS += $(LIB_GLFW3)

OPENGL_LIBS:=$(DYNAMIC_OPENGL_LIBS) $(STATIC_OPENGL_LIBS)

OPENGL_LIB_DIRS=$(call get_static_lib_dirs,$(STATIC_OPENGL_LIBS))

GLXINFO64_COMMAND=glxinfo64
OPENGL_VERSIONS_INFO=$(shell { $(GLXINFO64_COMMAND) 2> /dev/null || echo 'UNKNOWN'; } | egrep "^(UNKNOWN$$|GLX version|OpenGL shading language version string|OpenGL version string): " | sed 's/^.*: //')
GLXINFO64_VERSION=$(firstword $(OPENGL_VERSIONS_INFO))
OPENGL_SHADING_LANGUAGE_VERSION=$(lastword $(OPENGL_VERSIONS_INFO))
OPENGL_VERSION=$(if $(filter-out UNKNOWN,$(OPENGL_VERSIONS_INFO)),$(filter-out $(GLXINFO64_VERSION) $(OPENGL_SHADING_LANGUAGE_VERSION),$(OPENGL_VERSIONS_INFO)),UNKNOWN)
ESSL_VERSION=$(call get_stdout_version,glslangValidator,--version,| sed -n '2{p;q}' | gawk '{print $$6}')
GLSLANG_VALIDATOR_VERSION=$(call get_stdout_version,glslangValidator,--version,| head -1 | sed 's/^Glslang Version: 10://')
GLSL_VERSION=$(call get_stdout_version,glslangValidator,--version,| sed -n '3{p;q}' | gawk '{print $$3}')
GLAD=$(realpath $(shell command -v glad))

.PHONY: opengl_libs
opengl_libs: opengl_info
	@echo 'Dynamic OpenGL libraries:'
	@{ $(foreach lib_name,$(DYNAMIC_OPENGL_LIBS),                                                                                                                                     \
        echo '$(lib_name): path = [$(call get_shared_lib_path,$(lib_name))] version = [$(call get_shared_lib_version,$(lib_name))] soname = [$(call get_soname,$(lib_name))]';) } | \
        column -s":" -o":" -t | column -s"=" -o"=" -t
	@echo ''
	@echo 'Static OpenGL libraries:'
	@{ $(foreach lib_name,$(STATIC_OPENGL_LIBS),echo '$(lib_name): path = [$(call get_static_lib_path,$(lib_name))] version = [$(call get_static_lib_version,$(lib_name))]';) } | \
        column -s":" -o":" -t | column -s"=" -o"=" -t

################################################################################
# Creates a target for the OpenGL application.                                 #
#                                                                              #
# $(1) - OpenGL target                                                         #
################################################################################
OPENGL_SRC_DIR=./src
OPENGL_INCLUDE_DIR=./include
define cr8_opengl_target
$(1): $(OPENGL_SRC_DIR)/$(1).cpp
	@$$(call target_info,$$(@),$$(<),$$(?),$$(^),$$(|),$$(*))
	@$$(eval TARGET_START_TIME_NSEC:=$$(shell date +%s%N))
	@echo "EXECUTABLE SOURCE FILE = [$(1).cpp]"
	$(CXX) $(CXXFLAGS) $(STATIC_LIB) -I $(OPENGL_INCLUDE_DIR) -o ./$(1) $(OPENGL_SRC_DIR)/$(1).cpp $(OPENGL_LIB_DIRS) $$(addprefix -l,$$(OPENGL_LIBS))
	@$$(call elapsed_time_nsec,$$(@),$$(TARGET_START_TIME_NSEC))
endef

.PHONY: opengl_info
opengl_info:
	@echo 'ESSL version                    = [$(ESSL_VERSION)]'
	@echo 'GLSL version                    = [$(GLSL_VERSION)]'
	@echo 'OpenGL Shading Language version = [$(OPENGL_SHADING_LANGUAGE_VERSION)]'
	@echo 'OpenGL version                  = [$(OPENGL_VERSION)]'
	@echo 'glslangValidator version        = [$(GLSLANG_VALIDATOR_VERSION)]'
	@echo 'glxinfo64 version               = [$(GLXINFO64_VERSION)]'
	@echo 'glad                            = [$(GLAD)]'

.PHONY: opengl_mk_help
opengl_mk_help:
	@echo '$(MAKE) opengl_libs → lists paths and versions for OpenGL dynamic and static libraries'

show_help:: opengl_mk_help

platform_info:: opengl_info

endif # }

