
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cerrno>
#include <cstring>

class ErrorBuffer
{
  public:

    ErrorBuffer();

    ~ErrorBuffer();

    static size_t getErrorBufferSize()
    {
      return (ErrorBuffer::m_nErrorBufferSize);
    }

    inline operator char*()
    {
      return (m_sErrorBuffer);
    }

    inline operator const char*() const
    {
      return (m_sErrorBuffer);
    }

    inline char& operator[](const size_t nIndex)
    {
      // Don't want to exceed ErrorBuffer::m_nErrorBufferSize nor throw
      // an exception indicating an out of range error. Therefore, modular
      // arithmetic is used to guarantee that a reference to an  element
      // with a valid index is returned. However, this means the calling
      // function may not get the C-style string that it expects.
      return (m_sErrorBuffer[nIndex % ErrorBuffer::m_nErrorBufferSize]);
    }

    ErrorBuffer& operator=(const char* s);

    void clear();

    size_t size() const;

  private:

    static const size_t m_nErrorBufferSize = 512;

    char m_sErrorBuffer[ErrorBuffer::m_nErrorBufferSize];

    // Intentionally deleted.
    ErrorBuffer(const ErrorBuffer& rhs) = delete;
    ErrorBuffer(ErrorBuffer&& rhs)      = delete;
    ErrorBuffer& operator=(const ErrorBuffer& rhs) = delete;
    ErrorBuffer& operator=(ErrorBuffer&& rhs) = delete;
    ErrorBuffer* operator&()                  = delete;
};

// NOTE: errno is thread local, meaning that if errno is set in one
//       thread, it does not affect errno's value in any other thread (see
//       man 3 errno).
// NOTE: This RAII class can be used to make the use of errno re-entrant.
//       Example:
//         {
//           // Saving the current value of errno and then setting errno to 0.
//           ScopedErrno tErrno;
//
//           some_function_that_modifies_errno();
//           if (errno != 0)
//           {
//             logErrorMessage("Function error");
//           }
//         } // tErrno goes out of scope and the previous value of errno is
//         restored.
class ScopedErrno
{
  public:

    ScopedErrno();

    ~ScopedErrno();

    inline int getSavedErrno() const
    {
      return (m_nSavedErrno);
    }

    inline operator int() const
    {
      return (errno);
    }

    const char* getErrorString(ErrorBuffer& sErrorMessageBuffer);

  private:

    const int m_nSavedErrno;

    void strcpy_r(ErrorBuffer& pDest, const char* pSrc, const size_t nSrcSize);

    // Intentionally deleted.
    ScopedErrno(const ScopedErrno& rhs) = delete;
    ScopedErrno(ScopedErrno&& rhs)      = delete;
    ScopedErrno&       operator=(const ScopedErrno& rhs) = delete;
    ScopedErrno&       operator=(ScopedErrno&& rhs) = delete;
    ScopedErrno*       operator&()                  = delete;
    const ScopedErrno* operator&() const            = delete;
};

