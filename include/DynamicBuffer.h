
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>

template<class T> class DynamicBuffer
{
  public:

    explicit DynamicBuffer(const size_t nSize, const T tInitValue = T());

    ~DynamicBuffer();

    size_t size() const
    {
      return (m_nSize);
    }

    operator T*()
    {
      return (m_pBuffer.get());
    }

  private:

    const size_t         m_nSize;
    std::unique_ptr<T[]> m_pBuffer;

    // Intentionally deleted.
    DynamicBuffer()                         = delete;
    DynamicBuffer(const DynamicBuffer& rhs) = delete;
    DynamicBuffer(DynamicBuffer&& rhs)      = delete;
    DynamicBuffer&       operator=(const DynamicBuffer& rhs) = delete;
    DynamicBuffer&       operator=(DynamicBuffer&& rhs) = delete;
    DynamicBuffer*       operator&()                    = delete;
    const DynamicBuffer* operator&() const              = delete;
};

