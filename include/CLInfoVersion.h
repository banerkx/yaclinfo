
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#define CLINFO_MAJOR_VERSION 1
#define CLINFO_MINOR_VERSION 6
#define CLINFO_PATCH_VERSION 18

#define CLINFO_VERSION_CONCAT_3(x, y, z) \
  QUOTE_B(CLINFO_VERSION_CONCAT_B(       \
    x,                                   \
    CLINFO_VERSION_CONCAT_B(CLINFO_VERSION_CONCAT_B(., y), CLINFO_VERSION_CONCAT_B(., z))))
#define CLINFO_VERSION_CONCAT_B(x, y) CLINFO_VERSION_CONCAT_A(x, y)
#define CLINFO_VERSION_CONCAT_A(x, y) x##y

#define QUOTE_A(x) #x
#define QUOTE_B(x) QUOTE_A(x)

#define CLINFO_VERSION \
  CLINFO_VERSION_CONCAT_3(CLINFO_MAJOR_VERSION, CLINFO_MINOR_VERSION, CLINFO_PATCH_VERSION)

namespace CLINFO
{
  const char* clinfoVersion();

  unsigned short clinfoMajorVersion();

  unsigned short clinfoMinorVersion();

  unsigned short clinfoPatchVersion();
}  // namespace CLINFO

