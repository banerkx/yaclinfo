
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#pragma GCC diagnostic ignored "-Wignored-attributes"

#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <tuple>
#include <vector>

#include "HostPlatformInfo.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

#include "SingletonBase.h"

class PlatformInfo;

template<typename ENUM_TYPE>
constexpr typename std::underlying_type<ENUM_TYPE>::type UnderlyingEnumType(ENUM_TYPE e)
{
  return (static_cast<typename std::underlying_type<ENUM_TYPE>::type>(e));
}

class CLInfo : public ConstSingletonBase<CLInfo>
{
  public:

    /**
     * @brief overloaded insertion operator
     * @param tStream - output stream
     * @param rhs - const instance
     * @return - output stream
     */
    friend std::ostream& operator<<(std::ostream& tStream, const CLInfo& rhs) noexcept;

    /**
     * @brief sends html output to the specified output stream
     * @param tStream - specified output stream
     * @param sHTMLTitle - title of the html page
     * @param sPlatformTableName - table name for the html table
     */
    void getHTMLOutput(
      std::ostream&      tStream,
      const std::string& sHTMLTitle         = "OpenCL Information",
      const std::string& sPlatformTableName = "opencl_platform_info") const noexcept;

    /**
     * @brief creates an html frames file consisting of both html and text output
     * @param sFrameFile - frames file name
     * @param tCLInfoHtml - html information
     * @param tCLInfoText - text information
     */
    static void createHTMLFrameFile(
      const std::string&       sFrameFile,
      const std::stringstream& tCLInfoHtml,
      const std::stringstream& tCLInfoText);

    /**
     * @brief accessor for the version
     * @return - version string
     */
    const std::string& clinfoVersion() const
    {
      return (m_sCLInfoVersion);
    }

  private:

    typedef std::string                                       ICDFile;
    typedef std::string                                       LibraryName;
    typedef std::string                                       LibraryLocation;
    typedef std::tuple<ICDFile, LibraryName, LibraryLocation> ICDFileInfo;

    typedef enum
    {
      ICD_FILE         = 0,
      LIBRARY_NAME     = 1,
      LIBRARY_LOCATION = 2
    } ICD_INDEX;

    typedef HostPlatformInfo::VideoCard           VideoCard;
    typedef HostPlatformInfo::VideoCardProperties VideoCardProperties;

    const std::string                          m_sOpenCLOverview;
    long double                                m_fElapsedSeconds;
    long double                                m_fElapsedCPUTime;
    std::vector<ICDFileInfo>                   m_tICDInfo;
    cl_uint                                    m_nNumberOfPlatforms;
    std::map<cl_platform_id, unsigned short>   m_tPlatformIDs;
    std::vector<std::shared_ptr<PlatformInfo>> m_tPlatformInfo;
    const std::string                          m_sCLInfoVersion;

    /**
     * @brief used for sorting OpenCL platforms by platform version
     * @param lhs - left platform
     * @param rhs - right platform
     * @return true if the left platform version is less than the right platform
     * version, otherwise false
     */
    static bool platformVersionLessThan(
      const std::shared_ptr<PlatformInfo>& lhs,
      std::shared_ptr<PlatformInfo>&       rhs);

    CLInfo();

    /**
     * @brief destructor
     */
    virtual ~CLInfo() noexcept
    {
      m_tICDInfo.clear();
      m_tPlatformInfo.clear();
      m_tPlatformIDs.clear();
    }

    void getPlatformIds();
    void printHostAttributes(std::ostream& tStream) const;
    void setHostAttributeHTMLTableRow(std::ostream& tStream) const;

    void getPlatformInfo() noexcept;

    void getPlatformVersions(std::unique_ptr<cl_platform_id[]>& pPlatformIDs);

    inline cl_uint numberOfPlatforms() const noexcept
    {
      return (m_nNumberOfPlatforms);
    }

    template<class T> void printAttribute(
      std::ostream&      tStream,
      const std::string& sLabel,
      const T&           tAttribute,
      const char*        pUnits = nullptr) const noexcept;

    void printMemoryAttribute(
      std::ostream&      tStream,
      const double       fMemory,
      const std::string& sLabel) const;

    static void setStyle(std::ostream& tStream, const std::string& sPlatformTableName) noexcept;

    static void setPlatformTableStyle(
      std::ostream&      tStream,
      const std::string& sTableName) noexcept;

    static void setDeviceNumberRowStyle(std::ostream& tStream);

    static void setToolTipStyle(std::ostream& tStream) noexcept;

    static void setScript(std::ostream& tStream) noexcept;

    void buildHTMLIPAddressesTable(std::ostream& tStream) const noexcept;

    void buildHTMLResourceLimitsTable(std::ostream& tStream) const noexcept;

    void buildHTMLVideoCardsTable(std::ostream& tStream) const noexcept;

    void buildHTMLRUsageInfoTables(std::ostream& tStream) const noexcept;

    void buildHTMLRUsageInfoTable(
      std::ostream&                                           tStream,
      const std::string&                                      sLabel,
      const std::vector<std::pair<std::string, std::string>>& tResourceUsages) const noexcept;

    void printIPAddresses(std::ostream& tStream) const noexcept;

    void printResourceLimits(std::ostream& tStream) const noexcept;

    void printVideoCardInfo(std::ostream& tStream) const noexcept;

    void printRUsageInfo(std::ostream& tStream) const noexcept;

    static unsigned short getPlatformVersion(const std::string& sVersionInfo);

    static std::vector<std::string> getICDFiles();

    static std::string getOpenCLLibraryName(const std::string& sICDFile);

    static std::string getLibraryPath(
      const std::vector<std::string>& tLibDirs,
      const std::string&              sLibrary);

    void setICDFilesInfo();

    void buildOpenCLICDFilesTable(std::ostream& tStream) const;

    static std::string setHTMLUnits(const std::string& s);

    static std::string processHtmlLine(const std::string& s);

    static std::string replaceAngleBrackets(const std::string& s);

    static std::string processTextLine(const std::string& s);

    static std::string italicizeUptimeUnits(std::string sUptime);

    static void setHTMLMemoryAttributeRow(
      std::ostream&      tStream,
      const double       fMemory,
      const std::string& sLabel);

    // NOTE: The calling function must delete dynamic memory returned by this
    // method.
    static PlatformInfo* getPlatform(
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nPlatformVersion);

    friend class ConstSingletonBase<CLInfo>;

    // Intentionally deleted.
    CLInfo(const CLInfo& rhs) = delete;
    CLInfo(CLInfo&& rhs)      = delete;
    CLInfo&       operator=(const CLInfo& rhs) = delete;
    CLInfo&       operator=(CLInfo&& rhs) = delete;
    CLInfo*       operator&()             = delete;
    const CLInfo* operator&() const       = delete;
};

