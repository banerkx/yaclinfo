
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PROFILERS_MK_INCLUDE_GUARD # {
PROFILERS_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

include $(PROFILERS)/gcov.mk
include $(PROFILERS)/gperf.mk
include $(PROFILERS)/gprof.mk
include $(PROFILERS)/ld_debug.mk
include $(PROFILERS)/sprof.mk
include $(PROFILERS)/valgrind.mk
include $(PROFILERS)/profilers_md.mk

.PHONY: clean_profiles
clean_profiles:
	@rm -rf $(PROFILER_OUTPUT_DIRS)

.PHONY: prof
prof: export DOT_CLEAN_TARGET      = build_clean
prof: export GCOV_CLEAN_TARGET     = build_clean
prof: export GPERF_CLEAN_TARGET    = build_clean
prof: export GPROF_CLEAN_TARGET    = build_clean
prof: export LD_DEBUG_CLEAN_TARGET = build_clean
prof: export SPROF_CLEAN_TARGET    = build_clean
prof: export VALGRIND_CLEAN_TARGET = build_clean
prof::
	@$(eval $(@)_START_TIME_NSEC:=$(shell date +%s%N))
	+@$(MAKE_NO_DIR) clean
	+@$(MAKE_NO_DIR) gcov
	+@$(MAKE_NO_DIR) gheap
	+@$(MAKE_NO_DIR) gprof
	+@$(MAKE_NO_DIR) sprof
ifdef DEP_BUILD_MK_INCLUDE_GUARD # {
	+@$(MAKE_NO_DIR) dep_$(EXE)
endif # }
	+@$(MAKE_NO_DIR) cachegrind FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) callgrind  FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) drd        FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) helgrind   FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) massif     FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) memcheck   FORCE_RECOMPILE=NO
	+@$(MAKE_NO_DIR) ld_debug   FORCE_RECOMPILE=NO
##	+@$(MAKE_NO_DIR) ld_debug_children FORCE_RECOMPILE=NO
################################################################################
# Since this is a double colon rule, it is possible that further recipes exist #
# in other makefiles. To ensure that the elapsed time is calculated just once  #
# for all recipes for this target, define the variable                         #
# DONT_CALCULATE_PROF_ELAPSED, before this makefile is included and in which-  #
# ever makefile is appropriate, to prevent the elapsed time being calculated   #
# here.                                                                        #
################################################################################
ifndef DONT_CALCULATE_PROF_ELAPSED # {
	@$(call elapsed_time_nsec,$(@),$($(@)_START_TIME_NSEC))
endif # }
	@echo ''

endif # }

