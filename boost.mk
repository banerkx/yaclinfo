
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef BOOST_MK_INCLUDE_GUARD # {
BOOST_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# $(1) - boost include directory (this directory must be the parent directory  #
#        of the subdirectory boost, where this subdirectory contains the       #
#        actual boost headers)                                                 #
################################################################################
define boost_include
  ifeq ($(BOOST),)
    BOOST=$(abspath $(wildcard $(1)/.))
  endif
endef
BOOST_INCLUDE_DIRS ?= /opt/boost/include /usr/include
$(foreach dir,$(BOOST_INCLUDE_DIRS), $(eval $(call boost_include,$(dir))))
ifeq ($(BOOST),) # {
$(error $(ERROR_LABEL) Could not determine the boost include directory from the set of directories [$(BOOST_INCLUDE_DIRS)])
endif # }

ifneq ($(BOOST),/usr/include) # {
BOOST_INCLUDE_PARAM=-isystem $(BOOST)
endif # }

################################################################################
# $(1) - boost libraries directory                                             #
################################################################################
define boost_lib
  ifeq ($(BOOST_LIB_DIR),)
    BOOST_LIB_DIR=$(abspath $(wildcard $(1)/.))
  endif
endef
BOOST_LIB_DIRS ?= /opt/boost/lib64 /usr/lib64
$(foreach dir,$(BOOST_LIB_DIRS), $(eval $(call boost_lib,$(dir))))
ifeq ($(BOOST_LIB_DIR),) # {
$(error $(ERROR_LABEL) Could not determine the boost libraries directory from the set of directories [$(BOOST_LIB_DIRS)])
endif # }
LIB_BOOST_SYSTEM ?= $(BOOST_LIB_DIR)/libboost_system.so
ifeq ($(wildcard $(LIB_BOOST_SYSTEM)),) # {
$(error $(ERROR_LABEL) The boost shared object library file [$(notdir $(LIB_BOOST_SYSTEM))] could not be found in the boost library directory [$(dir $(LIB_BOOST_SYSTEM))])
endif # }

BOOST_INCLUDE_DIR=$(BOOST)/boost
BOOST_VERSION_FILE=$(BOOST_INCLUDE_DIR)/version.hpp
ifeq ($(wildcard $(BOOST_VERSION_FILE)),) # {
$(error $(ERROR_LABEL) The boost version header file [$(notdir $(BOOST_VERSION_FILE))] could not be found in the boost include directory [$(dir $(BOOST_VERSION_FILE))])
endif # }
BOOST_VERSION=$(shell { grep "^\#define BOOST_LIB_VERSION " $(BOOST_VERSION_FILE) 2> /dev/null || echo 'UNKNOWN'; } | sed 's/^\#define BOOST_LIB_VERSION //;s/\"//g;s/_/./')
ifeq ($(findstring $(BOOST_LIB_DIR),${LD_LIBRARY_PATH}),) # {
  LD_LIBRARY_PATH:=$(BOOST_LIB_DIR):${LD_LIBRARY_PATH}
endif # }

platform_info::
	@echo 'boost version = [$(BOOST_VERSION)]'

.PHONY: boostinfo
BOOST_DOWNLOAD=https://www.boost.org/users/download/
BOOST_LATEST_VERSION=$(shell { curl -s $(BOOST_DOWNLOAD) 2> /dev/null; echo UNKNOWN; } | egrep '(UNKNOWN|title.*Version)' | sed 's/^.*Version //;s/<.*//;s/\.0$$//' | head -1)
boostinfo:
	@echo 'BOOST                = [$(BOOST)]'
	@echo 'BOOST_DOWNLOAD       = [$(BOOST_DOWNLOAD)]'
	@echo 'BOOST_INCLUDE_DIR    = [$(BOOST_INCLUDE_DIR)]'
	@echo 'BOOST_LATEST_VERSION = [$(BOOST_LATEST_VERSION)]'
	@echo 'BOOST_LIB_DIR        = [$(BOOST_LIB_DIR)]'
	@echo 'BOOST_VERSION        = [$(BOOST_VERSION)]'

show_updates::
	@$(call check_updates,boost,$(BOOST_DOWNLOAD),$(BOOST_VERSION),$(BOOST_LATEST_VERSION))

################################################################################
# Adding the Boost library directory and library flags.                        #
################################################################################
BOOST_LIBS ?=
ifneq ($(strip $(BOOST_LIBS)),) # {
BOOST_LIBS:=-L $(BOOST_LIB_DIR) $(addprefix -l,$(BOOST_LIBS))
endif # }

################################################################################
# Adding the Boost include directory and library flags.                        #
################################################################################
ifeq ($(wordlist 1,2,$(strip $(subst /, ,$(BOOST)))),usr include) # {
  INCLUDES += $(BOOST)
else # } {
  SYSTEM_HEADER_DIRS += $(BOOST)
endif # }

.PHONY: boost_mk_help
boost_mk_help:
	@echo '$(MAKE) boostinfo → shows relevant boost variables'

show_help:: boost_mk_help

endif # }

