
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GPROF_MK_INCLUDE_GUARD # {
GPROF_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# See: https://ftp.gnu.org/old-gnu/Manuals/gprof-2.9.1/html_node/gprof_1.html  #
################################################################################

GPROF_COMMAND=gprof
GPROF_OUTPUT=$(CURDIR)/gprof_output
GPROF_EXE ?= $(EXE)

PROFILER_OUTPUT_DIRS += $(GPROF_OUTPUT)

DOT_IMAGE_EXTENSION=svg
GPROF_DOT_OUTPUT=$(GPROF_OUTPUT)/$(GPROF_EXE)_gprof.$(DOT_IMAGE_EXTENSION)

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=
GPROF_EXE_COMMAND=$(strip ./$(GPROF_EXE) $(RUN_OPTS))

GMON_OUT=$(GPROF_OUTPUT)/$(GPROF_EXE)_gmon.out
GMON_RESULTS=$(GPROF_OUTPUT)/$(GPROF_EXE)_$(GPROF_COMMAND)_results.txt

GPROF_TARGETS=gprof gprof_clean gprof_prep
.PHONY: $(GPROF_TARGETS)
GPROF_EXTERNAL_TARGETS=clean gprof_mk_help show_help vars print-% qprint-% platform_info show_help
ifneq ($(filter $(GPROF_TARGETS) $(GPROF_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

ifneq ($(CXX),g++) # {
ifneq ($(filter $(GPROF_TARGETS),$(MAKECMDGOALS)),) # {
$(error $(ERROR_LABEL) [$(GPROF_COMMAND)] can only be used with [g++], not with [$(CXX)])
endif # }
endif # }

GPROF:=$(shell command -v $(GPROF_COMMAND))
GPROF_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,$(GPROF_COMMAND),--version,4))))

ifneq ($(GPROF),) # {

################################################################################
# Relevant target(s):                                                          #
#                                                                              #
#   make gprof --> produces the ASCII file $(GPROF_EXE)_gprof.results          #
#                                                                              #
# NOTE:                                                                        #
#                                                                              #
#   - If the value of $(GPROF_OPTS) should be changed from the default (see    #
#     below), then $(GPROF_OPTS) must be specified **before** including this   #
#     makefile.                                                                #
#   - If running with the gprof profiler succeeds, then the output will be     #
#     saved to the file $(GPROF_EXE)_gprof_results.                            #
################################################################################

################################################################################
# -A for annotated source listing.                                             #
################################################################################
##ANNOTATED=--annotated-source
GPROF_OPTS ?= $(ANNOTATED)               \
              --demangle                 \
              --display-unused-functions \
              --exec-counts              \
              --flat-profile             \
              --function-ordering        \
              --graph                    \
              --ignore-non-functions     \
              --inline-file-names        \
              --print-path               \
              --static-call-graph

GPROF_GPROF2_DOT_COMMAND=gprof2dot
GPROF2_DOT:=$(shell command -v $(GPROF_GPROF2_DOT_COMMAND))
GPROF2_DOT_VERSION=$(if $(shell command -v $(GPROF2_DOT) 2> /dev/null),INSTALLED,UNKNOWN)

GPROF_GPROF2_DOT_OPTS=--color-nodes-by-selftime \
                      --colormap=color          \
                      --format=prof             \
                      --node-thres=1.0          \
                      --total=callratios

GPROF_DOT_COMMAND=dot
GPROF_DOT:=$(shell command -v $(GPROF_DOT_COMMAND))
GPROF_DOT_SVG_RENDERER ?= :svg:core
GPROF_DOT_OPTS=-T$(DOT_IMAGE_EXTENSION)$(GPROF_DOT_SVG_RENDERER) -o $(GPROF_DOT_OUTPUT)

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than clean.                                                            #
################################################################################
export GPROF_CLEAN_TARGET ?= clean

# Use "-pg" to enable gprof profiling.
$(GPROF_COMMAND): GPROF_CXXFLAGS=$(CXXFLAGS) -pg
$(GPROF_COMMAND): DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
$(GPROF_COMMAND): $(GPROF_CLEAN_TARGET) gprof_clean gprof_prep
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] GPROF_CLEAN_TARGET = [$(GPROF_CLEAN_TARGET)]'
	+@$(MAKE_NO_DIR) CXXFLAGS="$(GPROF_CXXFLAGS)" $(GPROF_EXE)
	@mkdir -p $(GPROF_OUTPUT)
################################################################################
# Specifying the GMON_OUT_PREFIX environment variable when doing the gprof run #
# results in the gmon.out file being call $(GMON_OUT_PREFIX).[PID]. Now        #
# creating the gprof output file by executing the application.                 #
################################################################################
	@GMON_OUT_PREFIX=$(GMON_OUT) $(GPROF_EXE_COMMAND) || :
	@mv $$(ls -1t $(GMON_OUT).[0-9]* | head -1) $(GMON_OUT)
ifeq ($(if $(GPROF2_DOT),$(if $(GPROF_DOT),T)),T) # {
	@$(GPROF) $(GPROF_OPTS) $(GPROF_EXE) $(GMON_OUT)                                                     | \
   tee $(GMON_RESULTS)                                                                                 | \
   $(GPROF2_DOT) $(GPROF_GPROF2_DOT_OPTS)                                                              | \
   sed '$$i\\ \ labelloc="t";'                                                                         | \
   sed '$$i\\ \ label="$(GPROF_COMMAND) Output for ($(GPROF_EXE_COMMAND)) Date/Time: [$(DATE_TIME)]";' | \
   $(GPROF_DOT) $(GPROF_DOT_OPTS)
	@$(call open_file_command,$(GPROF_DOT_OUTPUT),$(BROWSER),$(GPROF_COMMAND) call graph diagram)
else # } {
ifeq ($(GPROF_DOT),) # {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(GPROF_DOT_COMMAND)] command was not found; no [$(GPROF_COMMAND)] call graph image will be created.)
endif # }
ifeq ($(GPROF2_DOT),) # {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(GPROF_GPROF2_DOT_COMMAND)] command was not found; no [$(GPROF_COMMAND)] call graph image will be created.)
endif # }
	@$(GPROF) $(GPROF_OPTS) $(GPROF_EXE) $(GMON_OUT) >| $(GMON_RESULTS)
endif # }
	@sed -i '/^ % * the percentage of the total/,/^/{/^% * the percentage of the total/!{/^/!d}}' $(GMON_RESULTS)
	@sed -i '1 i\
   $(GPROF_COMMAND) Flat Profile Key:\n\
    %         the percentage of the total running time of the\
   time       program used by this function.\n\
   cumulative a running sum of the number of seconds accounted\
    seconds   for by this function and those listed above it.\n\
    self      the number of seconds accounted for by this\
   seconds    function alone.  This is the major sort for this\
              listing.\n\
   calls      the number of times this function was invoked, if\
              this function is profiled, else blank.\n\
   self       the average number of milliseconds spent in this\
   ms/call    function per call, if this function is profiled,\
              else blank.\n\
    total     the average number of milliseconds spent in this\
   ms/call    function and its descendants per call, if this\
              function is profiled, else blank.\n\
   name       the name of the function.  This is the minor sort\
              for this listing. The index shows the location of\
              the function in the gprof listing. If the index is\
              in parenthesis it shows where it would appear in\
              the gprof listing if it were to be printed.\n'     $(GMON_RESULTS)
	@sed -i "1iDate/Time [$(DATE_TIME)]"                           $(GMON_RESULTS)
	@sed -i "1i$(GPROF_COMMAND) Output for ($(GPROF_EXE_COMMAND))" $(GMON_RESULTS)
	@$(call open_file_command,$(GMON_RESULTS),$(EDITOR),$(GPROF_COMMAND) ASCII results file)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

gprof_prep::;

.PHONY: gprof_clean
gprof_clean:
	@rm -f $(GMON_OUT) $(GMON_RESULTS) $(GPROF_DOT_OUTPUT)
	@rm -rf $(GPROF_OUTPUT)

clean:: gprof_clean

.PHONY: gprof_mk_help
gprof_mk_help::
ifneq ($(wildcard $(GPROF_EXE)),) # {
	@echo '$(MAKE) $(GPROF_COMMAND) → perform a $(GPROF_COMMAND) run'
endif # }

show_help:: gprof_mk_help

platform_info::
	@echo '$(GPROF_GPROF2_DOT_COMMAND) version = [$(GPROF2_DOT_VERSION)]'
	@echo '$(GPROF_COMMAND) version            = [$(GPROF_VERSION)]'

else # } {

$(GPROF_TARGETS):
	@$(warning [$(@)] $(WARNING_LABEL) Can not make target [$(@)]; the [$(GPROF_COMMAND)] command is not available.)

endif # }

endif # }

endif # }

