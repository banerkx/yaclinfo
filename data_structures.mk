
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef DATA_STRUCTURES_MK_INCLUDE_GUARD # {
DATA_STRUCTURES_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/vars_common.mk.  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# BEGIN stack                                                                  #
################################################################################

################################################################################
# Pushes the input frame onto the input stack.                                 #
#                                                                              #
# $(1) - stack name                                                            #
# $(2) - new stack frame                                                       #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_push,MY_STACK,fr6)                                            #
################################################################################
stack_push=$(if $(1),$(eval $(1) += $(strip $(2))),$(eval $($(1))=$(strip $(2))))

################################################################################
# Removes the last frame from the stack and does not return it. If the stack   #
# is empty, then nothing happens.                                              #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_void_pop,MY_STACK)                                            #
################################################################################
stack_void_pop=$(if $($(1)),$(eval $(1):=$(strip $(wordlist 2, $(words $($(1))),x $($(1))))))

################################################################################
# Removes the last frame from the stack and returns it. If the stack is empty, #
# the returned frame will be empty.                                            #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_pop,MY_STACK)                                                 #
################################################################################
stack_pop=$(if $($(1)),$(lastword $($(1))))$(call stack_void_pop,$(1))

################################################################################
# Gets the stack size.                                                         #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_size,$(MY_STACK))                                             #
################################################################################
stack_size=$(words $($(1)))

################################################################################
# Returns T if the stack is empty, F otherwise.                                #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_empty,$(MY_STACK))                                            #
################################################################################
stack_empty=$(if $(filter-out 0,$(words $($(1)))),F,T)

################################################################################
# Returns the last frame in the stack without changing its contents.           #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_peek,$(MY_STACK))                                             #
################################################################################
stack_peek=$(lastword $($(1)))

################################################################################
# Clears the stack.                                                            #
#                                                                              #
# $(1) - stack name                                                            #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call stack_clear,MY_STACK)                                               #
################################################################################
stack_clear=$(if $($(1)),$(eval $(1):=))

################################################################################
# Populates a stack with the contents of the input list.                       #
#                                                                              #
# $(1) - list                                                                  #
# $(2) - stack name                                                            #
################################################################################
define list_to_stack
$(foreach item,$(1),$(call stack_push,$(2),$(item)))
endef

################################################################################
# Prints the frames of the stack (meant to be used as a debugging aid).        #
#                                                                              #
# $(1) - stack name                                                            #
# $(2) - calling function name (optional)                                      #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call print_stack,MY_STACK,$(@))                                          #
################################################################################
define print_stack
$(if $(2),echo '[$(2)] ----- TOP:    $(1) stack -----';,echo '----- TOP:    $(1) stack -----';) \
echo 'STACK SIZE = [$(words $($(1)))]';                                                         \
$(foreach item,$(call reverse_list,$($(1))),echo '[$(item)]';)                                  \
$(if $(2),echo '[$(2)] ----- BOTTOM: $(1) stack -----';,echo '----- BOTTOM: $(1) stack -----';)
endef

################################################################################
# Prints the frames of the stack (meant to be used as a debugging aid). The    #
# stack trace file will be named $(1)_stack_trace.log.                         #
#                                                                              #
# $(1) - stack name                                                            #
# $(2) - calling function name (optional)                                      #
#                                                                              #
# Example call:                                                                #
#   MY_STACK=fr1 fr2 fr3 fr4 fr5                                               #
#   $(call print_stack_file,MY_STACK,$(@))                                     #
################################################################################
define print_stack_file
$(if $(2),$(file >> $(1)_stack_trace.log,[$(2)] ----- TOP:    $(1) stack -----),$(file >> $(1)_stack_trace.log,----- TOP:    $(1) stack -----))
$(file >> $(1)_stack_trace.log,STACK SIZE = [$(words $($(1)))])
$(foreach item,$(call reverse_list,$($(1))),$(file >> $(1)_stack_trace.log,[$(item)]))
$(if $(2),$(file >> $(1)_stack_trace.log,[$(2)] ----- BOTTOM: $(1) stack -----),$(file >> $(1)_stack_trace.log,----- BOTTOM: $(1) stack -----))
endef

################################################################################
# END stack                                                                    #
################################################################################

endif # }

