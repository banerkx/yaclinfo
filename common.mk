
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef COMMON_MK_INCLUDE_GUARD # {
COMMON_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

EXE ?= $(notdir $(CURDIR))

include $(PROJECT_ROOT)/vars_common.mk
include $(PROJECT_ROOT)/git.mk
include $(PROJECT_ROOT)/graph_viz.mk
include $(PROJECT_ROOT)/boost.mk
include $(PROJECT_ROOT)/msg_digests.mk

################################################################################
# Function to get the value of the __cplusplus compiler macro.                 #
# $(1) - input compiler                                                        #
# Example call: GPP_C_PLUS_PLUS:=$(call get_cpp_standard,g++)                  #
################################################################################
get_cpp_standard=$(if $(1),$(shell $(1) -dM -E -x c++ - < /dev/null 2> /dev/null | grep __cplusplus | sed 's/^.* //;s/L$$//'),"")

ifndef SCAN_BUILD_FLAG # {
OPT ?= -O6
endif # }
DEBUG_FLAG ?= -fno-inline-functions -Og
ifeq ($(CXX),clang) # {
DEBUG_FLAG := $(filter-out -Og,$(DEBUG_FLAG))
endif # }
##PEDANTIC=-Wpedantic -pedantic
CXXFLAGS ?= $(DEBUG_FLAG) $(PEDANTIC) $(OPT)
CXX_WARNINGS=-Wsign-conversion -Wall -Wshadow -Wconversion -Weffc++ -Wnon-virtual-dtor -Wextra -Woverloaded-virtual -Wmissing-braces
CXXFLAGS += $(CXX_WARNINGS)
################################################################################
# If no modern C++ is used, then __cplusplus = 199711L                         #
################################################################################
CXX_STD ?=
# In g++, macro __cplusplus = 201103L
CXX_11= -std=c++11
# In g++, macro __cplusplus = 201402L
CXX_14= -std=c++14
# In g++, macro __cplusplus = 201703L
CXX_17= -std=c++17
# In g++, macro __cplusplus = 202002L
CXX_17= -std=c++20
# In g++, macro __cplusplus = 202100L
CXX_17= -std=c++23

################################################################################
# Gets the C++ __cplusplus macro value for the input C++ compiler.             #
#                                                                              #
# $(1) - C++ compiler                                                          #
################################################################################
get_compiler_std_macro=$(strip $(if $(filter $(1),g++ clang),                                                                             \
                         $(shell $(1) -E -x c++ <(echo __cplusplus) | tail -1),                                                           \
                         $(if $(filter $(1),CC),                                                                                          \
                           $(shell $(1) -E -xdumpmacros=defs - <(echo "main(){}") 2>&1 | grep "^#define .*__cplusplus" | sed 's/^.* //'), \
                            UNKNOWN)))

################################################################################
# Gets the C++ standard for the input C++ compiler.                            #
#                                                                              #
# $(1) - C++ compiler                                                          #
################################################################################
get_compiler_std=$(shell echo $(call get_compiler_std_macro,$(1)) | sed 's/\([0-9][0-9]\)\([0-9][0-9]\)\(.*\)/c++\2/;s/97/98/')

STATIC_LIB ?= -static-libgcc
CXX_VERSION_INTEGER=$(shell $(CXX) -dumpversion 2> /dev/null | sed 's/\.\([0-9][0-9]\)/\1/g;s/\.\([0-9]\)/0\1/g;s/^[0-9]\{3,4\}$$/&00/')
PTHREAD=-pthread

ifneq ($(EXE),) # {
################################################################################
# Prints which optimizer flags are enabled by using the input $(CXX) optimizer #
# level.                                                                       #
#                                                                              #
# $(1) - optimizer level                                                       #
################################################################################
MIN_OPTIMIZER_LEVEL=1
MAX_OPTIMIZER_LEVEL=3
OPTIMIZER_LEVELS:=$(shell seq $(MIN_OPTIMIZER_LEVEL) $(MAX_OPTIMIZER_LEVEL)) fast g s
define print_optimizer_info
.PHONY: opt$(1)
opt$(1):
	@$(CXX) -Q -O$(1) --help=optimizers >| $(CXX)_optimizer_$(1)_info.log
	@$$(call open_file_command,$(CXX)_optimizer_$(1)_info.log,$(EDITOR),[$(CXX)] optimizer level [$(1)] info log)

.PHONY: common_mk_help
common_mk_help::
	@echo '$(MAKE) opt$(1) → prints the optimizations enabled by using [-O$(1)] compiler flag to the file [$(CXX)_optimizer_$(1)_info.log]'
endef
$(foreach level,$(OPTIMIZER_LEVELS),$(eval $(call print_optimizer_info,$(level))))

################################################################################
# Pattern rule target to disallow invalid optimizer levels.                    #
################################################################################
opt%:
	@$(error $(ERROR_LABEL) [$(*)] is not a valid [$(CXX)] optimizer level)

endif # }

################################################################################
# Link with libdl.so when the dlopen() API is needed.                          #
################################################################################
DYNAMIC=-ldl

MATH=-lm

################################################################################
# Checking to see if we need to add the flag for compiling C++11 code to g++.  #
################################################################################
MODERN_CPP_THRESHOLD:=199711
COMPILER_VERSION_WARNING=$(WARNING_LABEL) Your version of the [$(CXX)] compiler, [$(CXX_VERSION)], does not natively support C++11.
ifeq ($(CXX),g++) # {
  GPP_C_PLUS_PLUS:=$(call get_cpp_standard,g++)
  ifeq ($(filter wRrpn,$(MAKEFLAGS)),)
    ifeq ($(shell expr $(GPP_C_PLUS_PLUS) \<= $(MODERN_CPP_THRESHOLD) 2> /dev/null), 1)
      CXX_STD=$(CXX_11)
      CXXFLAGS += $(CXX_STD)
    endif
  endif
endif # }

################################################################################
# If the clang compiler or the clang analyzer, scan-build, is being used, we   #
# may need to add $(CXX_11) to the compiler flags.                             #
################################################################################
ifneq ($(or $(SCAN_BUILD_FLAG), $(shell (test "$(CXX)" == "clang" && echo 1;) || :)),) # {
  CLANG_C_PLUS_PLUS:=$(call get_cpp_standard,clang)
  ifeq ($(shell expr $(CLANG_C_PLUS_PLUS) \<= $(MODERN_CPP_THRESHOLD) 2> /dev/null), 1)
    CXX_STD=$(CXX_11)
    CXXFLAGS += $(CXX_STD)
  endif
endif # }

################################################################################
# Fixes linker errors due inter-library dependencies (order of libraries).     #
# Example use:                                                                 #
# LIBS:=$(call linker_dependency_fix)                                          #
################################################################################
linker_dependency_fix=-Wl,--start-group $(LIBS) -Wl,--end-group

MAIN ?= $(wildcard main.cpp)

################################################################################
# If INCLUDE_DIR is empty and HEADERS is non-empty, then print an error if at  #
# least of the following holds:                                                #
#   - MAKECMDGOALS is empty (which means the default goal is being made)       #
#   - all is being made                                                        #
#   - the executable is being made                                             #
#   - the default goal is being made                                           #
#   - a dependency or object file is being made                                #
#   - a library is being made                                                  #
################################################################################
ifeq ($(INCLUDE_DIR),) # {
ifneq ($(HEADERS),) # {
ifeq ($(shell echo $(MAKECMDGOALS) | tr ' ' '\n' | egrep -q "(^|^all|^$(EXE)|^$(.DEFAULT_GOAL)|*\.[do]|^lib.*\.a|lib.*\.so)$$"; echo $${?}),0) # {
  $(error $(ERROR_LABEL) In makefile [$(realpath $(firstword $(MAKEFILE_LIST)))], must specify the variable INCLUDE_DIR since the variable HEADERS is non-empty)
endif # }
endif # }
endif # }

ifneq ($(INCLUDE_DIR),) # {
  INCLUDES += $(INCLUDE_DIR)
  HEADERS:=$(addprefix $(INCLUDE_DIR)/, $(HEADERS))
endif # }

INCLUDES += $(KERNELS_DIR)
ifeq ($(filter $(KHRONOS_OPENCL_C_UNIFIED_HEADERS),$(INCLUDES)),) # {
INCLUDES:=$(KHRONOS_OPENCL_C_UNIFIED_HEADERS) $(INCLUDES)
endif # }
INCLUDES:=$(addprefix -I , $(INCLUDES))
EXE_INCLUDES:=$(addprefix -I , $(EXE_INCLUDES))

################################################################################
# If we tell g++ that a particular directory contains system headers files,    #
# by using the "-isystem" command line option instead of "-I", then g++ will   #
# not report compiler warnings for those header files. By default, every thing #
# in /usr/include, is considered a system header.                              #
################################################################################
SYSTEM_HEADER_DIRS ?=
ifneq ($(SYSTEM_HEADER_DIRS),) # {
  SYSTEM_HEADER_DIRS:=$(addprefix -isystem , $(SYSTEM_HEADER_DIRS))
  CXXFLAGS += $(SYSTEM_HEADER_DIRS)
endif # }

ifneq ($(strip $(KERNELS)),) # {
KERNELS:=$(sort $(KERNELS))
KERNELS:=$(addprefix $(KERNELS_DIR)/, $(KERNELS))
endif # }

################################################################################
# Use these variables to link with static instead of dynamic libraries and     #
# then back to linking with dynamic libraries.                                 #
################################################################################
USE_STATIC_LIBS_ON=-Wl,-Bstatic
USE_STATIC_LIBS_OFF=-Wl,-Bdynamic

################################################################################
# Adds linker options to turn on static linking for specified libraries and    #
# then turn off static linking.                                                #
# $(1) - a list of of dynamic libraries                                        #
#   Example: MY_LIBS=-labc -ldef -lghi                                         #
#   Example call: $(call enforce_static_link,$(MY_LIBS))                       #
################################################################################
enforce_static_link=$(USE_STATIC_LIBS_ON) $(1) $(USE_STATIC_LIBS_OFF)

ifneq ($(strip $(LIB_DIRS)),) # {
LIB_DIRS:=$(sort $(LIB_DIRS))
LIB_DIRS:=$(addprefix -L ,$(LIB_DIRS))
endif # }

LIBS += $(OPENCL_LIB_STEM)
ifneq ($(LIBS),) # {
LIBS:=$(sort $(LIBS))
LIBS:=$(addprefix -l, $(LIBS))
endif # }

SRC_DIR ?= $(PROJECT_ROOT)/src

get_source_files=$(if $(INCLUDE_DIR),$(if $(HEADERS),$(HEADERS) $(wildcard $(subst $(INCLUDE_DIR),$(SRC_DIR),$(HEADERS:.h=.cpp)) $(realpath $(MAIN)))))

ifneq ($(EXE),) # {
CODE_FILES=$(realpath $(HEADERS) $(if $(strip $(HEADERS)),$(wildcard $(subst .h,.cpp,$(subst $(INCLUDE_DIR),$(SRC_DIR),$(HEADERS))))) $(shell echo $(MAKEFILE_LIST) | sed 's/ /\n/g' | grep -v "*\.d$$") $(MAIN))
CODE_FILES += $(PYTHON_SCRIPTS) $(SCRIPTS)
.PHONY: cloc
ifneq ($(CLOC),UNKNOWN) # {
################################################################################
# NOTE: It is possible that $(MAKEFILE_LIST) holds the names of the            #
#       automatically generated dependeny files (*.d files) included by the    #
#       build makefiles. Therefore, we must deleted these files from           #
#       $(MAKEFILE_LIST) since we do not want them to be counted.              #
################################################################################
cloc:
	@echo '$(CODE_FILES)'                                                                                                            | \
   tr ' ' '\n'                                                                                                                     | \
   xargs realpath                                                                                                                  | \
   xargs cloc --by-file-by-lang --exclude-dir=.git,dOxygen --quiet                                                                 | \
   egrep -v '(github.com|^$$)'                                                                                                     | \
   sed 's/^awk\([ |]\)/gawk\1/;s/gawk /gawk/'                                                                                      | \
   sed 's/\([ |]\)blank[ |]/\1Blank\1/;s/\([ |]\)comment[ |]/\1Comment\1/;s/\([ |]\)code$$/\1Code/;s/\([ |]\)files[ |]/\1Files\1/'
	@echo ''

.PHONY: cloc_percentages
cloc_percentages:
	+@$(MAKE_NO_DIR) cloc                                                             | \
     gawk 'BEGIN { array_index = 0; }                                                 \
     {                                                                                \
       printf("%s\n", $$0);                                                           \
       if (match($$0, /^Language * Files * Blank * Comment * Code/) > 0)              \
       {                                                                              \
         in_language_table = 1;                                                       \
       }                                                                              \
       else                                                                           \
       {                                                                              \
         if ((1 == in_language_table) && (NF >= 5))                                   \
         {                                                                            \
           if ("SUM:" != $$1)                                                         \
           {                                                                          \
             code_array[array_index]    = $$NF;                                       \
             comment_array[array_index] = $$(NF - 1);                                 \
             blank_array[array_index]   = $$(NF - 2);                                 \
             files_array[array_index]   = $$(NF - 3);                                 \
             for (i = 1; i < (NF - 3); i++)                                           \
             {                                                                        \
               if (1 == i)                                                            \
               {                                                                      \
                 language_array[array_index] = $$i;                                   \
               }                                                                      \
               else                                                                   \
               {                                                                      \
                 language_array[array_index] = language_array[array_index] " " $$i;   \
               }                                                                      \
             }                                                                        \
             array_index++;                                                           \
           }                                                                          \
           else if ("SUM:" == $$1)                                                    \
           {                                                                          \
             code_sum    = $$NF;                                                      \
             comment_sum = $$(NF - 1);                                                \
             blank_sum   = $$(NF - 2);                                                \
             files_sum   = $$(NF - 3);                                                \
           }                                                                          \
         }                                                                            \
       }                                                                              \
     }                                                                                \
     END {                                                                            \
       title = "Languages            Files      Blank    Comment       Code";         \
       dashes = gensub(/ /, "-", "g", sprintf("%*s", length(title), ""));             \
       printf("%s\n", dashes);                                                        \
       printf("%s\n", title);                                                         \
       printf("%s\n", dashes);                                                        \
       for (i = 0; i < array_index; i++)                                              \
       {                                                                              \
         printf("%-15s%10.2f%%%10.2f%%%10.2f%%%10.2f%%\n",                            \
                 language_array[i],                                                   \
                 (files_array[i] / files_sum) * 100.0,                                \
                 (blank_array[i] / blank_sum) * 100.0,                                \
                 (comment_array[i] / comment_sum) * 100.0,                            \
                 (code_array[i] / code_sum) * 100.0);                                 \
       }                                                                              \
       printf("%s\n", dashes);                                                        \
     }'
	@echo ''

common_mk_help::
	@echo '$(MAKE) cloc → counts lines of code'

else # } {
cloc:
	@$(error $(ERROR_LABEL) The cloc executable was not found. Please install the cloc executable from [$(CLOC_DOWNLOAD)] or correct your PATH environment variable)
endif # }
endif # }

.PHONY: objects_clean
ifneq ($(OBJECTS),) # {

OBJECTS_DIR ?= objects
SO_OBJECTS:=$(OBJECTS)
OBJECTS:=$(addprefix $(OBJECTS_DIR)/, $(OBJECTS))
LOCAL_LIB_DIR ?= $(CURDIR)/lib
LOCAL_LIB ?= $(LOCAL_LIB_DIR)/lib$(EXE).a
LIB_DIRS += -L $(LOCAL_LIB_DIR)
LIBS += -l$(EXE)

objects_clean:
	@rm -rf $(OBJECTS_DIR)
	@rm -f *.o
else # } {

objects_clean:
	@rm -f *.o

endif # }

.PHONY: so_objects_clean
ifneq ($(SO_OBJECTS),) # {

SO_OBJECTS_DIR ?= so_objects
SO_OBJECTS:=$(addprefix $(SO_OBJECTS_DIR)/, $(SO_OBJECTS))
SO_OBJECTS:=$(sort $(SO_OBJECTS))
LOCAL_LIB_DIR ?= $(CURDIR)/lib

so_objects_clean:
	@rm -rf $(SO_OBJECTS_DIR)
	@rm -f *.o

else # } {

so_objects_clean:
	@rm -f *.o

endif # }

################################################################################
# Concatenates the input object file to the static and dynamic object          #
# directory names.                                                             #
#                                                                              #
# $(1) - object file (the ".o" file extension is optional)                     #
################################################################################
add_object_file=$(if $(1),$(addsuffix /$(basename $(1)).o,$(OBJECTS_DIR) $(SO_OBJECTS_DIR)))

.PHONY: build_clean
build_clean:: local_lib_clean objects_clean so_objects_clean
ifneq ($(EXE),) # {
	@rm -f ./$(EXE)
endif # }

################################################################################
# Pattern rule to create object file, from a *.cpp file and its corresponding  #
# *.h file, in the $(OBJECTS_DIR) directory.                                   #
# Example usage:                                                               #
#   make MyClass.o                                                             #
################################################################################
%.o: $(OBJECTS_DIR)/%.o
	@:

$(OBJECTS_DIR):
	@mkdir -p $(OBJECTS_DIR)

$(SO_OBJECTS_DIR):
	@mkdir -p $(SO_OBJECTS_DIR)

################################################################################
# greps the list of input files for inclusion of any cl_* enum files to add to #
# the list of prerequisites.                                                   #
#                                                                              #
# $(1) - space separated list of files                                         #
################################################################################
define get_clenum_prerequisites
$(if $(1), $(shell function get_headers                                                                                                                                                 \
                   {                                                                                                                                                                    \
                     for HEADER in $$(grep -h '^#include ".*.*\.h"' $${*} 2> /dev/null | grep -v '#include "cl_.*\.h"' | sed 's/"//g;s/\.h.*$$/.h/;s@^.* @$(INCLUDE_DIR)/@' | sort -u); \
                     do                                                                                                                                                                 \
                       echo $${ALL_HEADER_FILES} | grep -q $${HEADER};                                                                                                                  \
                       if [[ 1 == $${?} ]];                                                                                                                                             \
                       then                                                                                                                                                             \
                         ALL_HEADER_FILES="$${ALL_HEADER_FILES} $${HEADER}";                                                                                                            \
                         get_headers $${HEADER};                                                                                                                                        \
                       fi;                                                                                                                                                              \
                     done;                                                                                                                                                              \
                   };                                                                                                                                                                   \
                   get_headers $(1);                                                                                                                                                    \
                   grep -h '^#include "cl_.*\.h"' $${ALL_HEADER_FILES} $(1) 2> /dev/null | sed 's/"//g;s/\.h.*$$/.h/;s@^.* @$(INCLUDE_DIR)/@' | sort -u))
endef

DEPENDS ?= depends
################################################################################
# The special target .SECONDEXPANSION is used to enable a second expansion of  #
# the prerequisite list for $(DEPENDS)/%.d. This second expansion allows us to #
# generate the needed clenum prerequisites on a target specific basis by       #
# calling the user-defined makefile function get_clenum_prerequisites.         #
################################################################################
.SECONDEXPANSION:
################################################################################
# Pattern rule to automatically create dependency files.                       #
################################################################################
##.PRECIOUS: $(DEPENDS)/%.d
$(DEPENDS)/%.d: $(SRC_DIR)/%.cpp $(INCLUDE_DIR)/%.h $$(call get_clenum_prerequisites,$$(SRC_DIR)/$$(*).cpp $$(INCLUDE_DIR)/$$(*).h)
	@echo 'DEPENDENCY FILE PATTERN RULE'
	@$(eval CLENUM_PREREQUISITES:=$(call get_clenum_prerequisites,$(SRC_DIR)/$(*).cpp $(INCLUDE_DIR)/$(*).h))
	@$(call target_info,$(@),$(<),$(filter-out $(CLENUM_PREREQUISITES),$(?)),$(filter-out $(CLENUM_PREREQUISITES),$(^)),$(|),$(*))
	@$(if $(CLENUM_PREREQUISITES),echo 'clenum PREREQUISITE(S)     = [$(CLENUM_PREREQUISITES)]')
	@mkdir -p $(DEPENDS)
	@echo 'DEPENDS            = [$(DEPENDS)]'
	$(CXX) $(CXX_STD) -MT $(OBJECTS_DIR)/$(*).o -MM $(EXTRA_INCLUDES) -I $(INCLUDE_DIR) $(BOOST_INCLUDE_PARAM) $(SRC_DIR)/$(*).cpp -MF $(@)
	@echo "$$(column -s"\\" -o"\\" -t $(@))" > $(@)
	@echo ''
include $(wildcard $(DEPENDS)/*.d)

build_clean::
	@rm -rf $(DEPENDS)

################################################################################
# Pattern rule to create object file, from its corresponding dependency file,  #
# in $(OBJECTS_DIR).                                                           #
################################################################################
$(OBJECTS_DIR)/%.o: $(DEPENDS)/%.d | $(OBJECTS_DIR)
	@echo 'OBJECT FILE PATTERN RULE'
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'OBJECTS_DIR        = [$(OBJECTS_DIR)]'
	$(CXX) $(CXXFLAGS) -c $(SRC_DIR)/$(*).cpp -o $(@) $(EXTRA_INCLUDES) $(INCLUDES)
	@chmod 400 $(@)
	@echo ''

################################################################################
# Pattern rule to create object file, from its corresponding dependency file,  #
# in $(SO_OBJECTS_DIR).                                                        #
################################################################################
$(SO_OBJECTS_DIR)/%.o: $(DEPENDS)/%.d | $(SO_OBJECTS_DIR)
	@echo 'SO OBJECT FILE PATTERN RULE'
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
ifneq ($(OBJECT_FILE_INCLUDES),) # {
	@echo 'OBJECT_FILE_INCLUDES = [$(OBJECT_FILE_INCLUDES)]'
endif # }
	@echo 'SO_OBJECTS_DIR       = [$(SO_OBJECTS_DIR)]'
	$(CXX) $(CXXFLAGS) -fPIC -c $(SRC_DIR)/$(*).cpp  -o $(@) $(INCLUDES)
	@chmod 400 $(@)
	@echo ''

.PHONY: so_local_lib_clean
ifneq ($(LOCAL_LIB_DIR),) # {

so_local_lib_clean: local_lib_clean
	@rm -f lib*.so lib*\.so\.*[O-9]*\.[O-9]* lib*.a

LIB_REAL_NAME ?= $(LOCAL_LIB_DIR)/lib$(EXE).so
.SECONDEXPANSION:
solib $(LIB_REAL_NAME): $(SO_OBJECTS) $$(CPP_UNIT_TEST_SO_OBJECTS) $$(GTEST_SO_OBJECTS) | $(LOCAL_LIB_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo "SO_OBJECTS           = [$(SO_OBJECTS)]"
ifneq ($(OBJECT_FILE_INCLUDES),) # {
	@echo "OBJECT_FILE_INCLUDES = [$(OBJECT_FILE_INCLUDES)]"
endif # }
ifdef SO_NAME # {
	@$(CXX) $(CXXFLAGS) -shared -o $(LIB_REAL_NAME) -Wl,-soname,$(SO_NAME) $(SO_OBJECTS)
else # } {
	@$(CXX) $(CXXFLAGS) -shared -o $(LIB_REAL_NAME) $(SO_OBJECTS)
endif # }
ifneq ($(filter solib $(LIB_REAL_NAME),$(MAKECMDGOALS)),) # {
	@rm -rf $(DEPENDS)
endif # }
	@echo ''
else # } {
so_local_lib_clean:
	@rm -f lib*\.so lib\*\.so\.*[0-9]*\.[0-9]* lib*\.a
endif # }

################################################################################
# Pattern rule to quickly create the object file for a class. To use, enter:   #
#  make ClassName (without any file extension)                                 #
################################################################################
%: $(SRC_DIR)/%.cpp $(PROJECT_ROOT)/include/%.h
	@echo 'SINGLE OBJECT FILE PATTERN RULE'
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	+@$(MAKE) $(OBJECTS_DIR)/$(*).o

.PHONY: local_lib_clean
ifneq ($(LOCAL_LIB_DIR),) # {
local_lib_clean:
ifneq ($(strip $(LOCAL_LIB_DIR)),$(strip $(CURDIR))) # {
	@rm -rf $(LOCAL_LIB_DIR)
endif # }

################################################################################
# Creates the directory for the local static library file.                     #
################################################################################
$(LOCAL_LIB_DIR):
ifdef EXE # {
	@mkdir -p $(LOCAL_LIB_DIR)
endif # }

.SECONDEXPANSION:
stlib $(LOCAL_LIB): $(OBJECTS) $$(CPP_UNIT_TEST_OBJECTS) $$(GTEST_OBJECTS) | $(LOCAL_LIB_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'OBJECTS            = [$(OBJECTS)]'
################################################################################
# Only replacing the newer object files (in verbose mode).                     #
# NOTE: Using the "s" option with ar is equivalent to running ranlib on the    #
#       static library file.                                                   #
################################################################################
	@ar rsv $(LOCAL_LIB) $(?)
ifneq ($(filter stlib $(LOCAL_LIB),$(MAKECMDGOALS)),) # {
	@rm -rf $(DEPENDS)
endif # }
	@echo ''
else # } {
local_lib_clean:;
endif # }

################################################################################
# Phony target to warn about non-modern C++ compiler.                          #
################################################################################
.PHONY: warn_non_modern_cpp
ifneq ($(findstring $(CXX_STD),$(CXXFLAGS)),) # {
warn_non_modern_cpp:
	@echo "$(COMPILER_VERSION_WARNING)"
else # } {
warn_non_modern_cpp:;
endif # }

################################################################################
# Determines the minimal set of header file prerequisites for the $(EXE)       #
# target. This is accomplished by:                                             #
#   - finding those header files in $(1) for which a corresponding *.cpp file  #
#     exists                                                                   #
#   - for such header files, filter them out from $(1)                         #
#   - the clenum header files are also filtered out                            #
#                                                                              #
# $(1) - list of header files                                                  #
################################################################################
get_exe_minimal_headers=$(filter-out $(addprefix $(INCLUDE_DIR)/,$(OPENCL_ENUMS_HEADERS)) $(subst .cpp,.h,$(subst $(SRC_DIR),$(INCLUDE_DIR),$(wildcard $(subst $(INCLUDE_DIR),$(SRC_DIR),$(1:.h=.cpp))))),$(1))

################################################################################
# Creates the executable.                                                      #
################################################################################
$(EXE): CXXFLAGS += $(STATIC_LIB)
$(EXE): $(MAIN) $(call get_exe_minimal_headers,$(HEADERS)) $(LOCAL_LIB) $(KERNELS)
ifneq ($(findstring $(CXX_STD),$(CXXFLAGS)),) # {
	@$(warning $(COMPILER_VERSION_WARNING))
endif # }
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'OS_PLATFORM        = [$(OS_PLATFORM)]'
	@echo '$(CXX) VERSION     = [$(CXX_VERSION)]'
	@echo 'MAIN SOURCE FILE   = [$(MAIN)]'
ifneq ($(KERNELS),) # {
	@echo 'KERNELS            = [$(KERNELS)]'
endif # }
ifdef ESPEAK # {
	($(CXX) $(BOOST_LIBS) $(CXXFLAGS) -o ./$(EXE) $(MAIN) $(EXTRA_INCLUDES) $(INCLUDES) $(LINKER_FLAGS) $(LIB_DIRS) $(LIBS) $(EXE_INCLUDES) && $(ESPEAK) "Compilation success") || $(ESPEAK) "Compilation failure"
else # } {
	$(CXX) $(BOOST_LIBS) $(CXXFLAGS) -o ./$(EXE) $(MAIN) $(EXTRA_INCLUDES) $(INCLUDES) $(LINKER_FLAGS) $(LIB_DIRS) $(LIBS) $(EXE_INCLUDES)
endif # }
	@rm -rf $(DEPENDS)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))
	@echo ''

.PHONY: junk_files_clean
junk_files_clean:
	@rm -f $(JUNK_FILES)

.PHONY: clean
clean:: build_clean junk_files_clean so_local_lib_clean
ifneq ($(notdir $(EXE)),) # {
	rm -f $(EXE)
endif # }
	@clear

################################################################################
# BEGIN gdb section.                                                           #
################################################################################
DEBUGGER=gdb
CORE_FILE:=$(shell ls -1tr core.* 2> /dev/null | tail -1)
GDB:=$(shell command -v $(DEBUGGER))
.PHONY: core gdb gdb_script

################################################################################
# If the debugger is available ...                                             #
################################################################################
ifdef GDB # {

core:
################################################################################
# If the executable is not available ...                                       #
################################################################################
ifeq ($(wildcard $(EXE)),) # {
	@$(error $(ERROR_LABEL) Could not find the executable [$(EXE)])
endif # }
################################################################################
# If the most recent core file has been found ...                              #
################################################################################
ifdef CORE_FILE # {
ifeq ($(CORE_FILE_SIZE),0) # {
	@$(warning $(WARNING_LABEL) Core file size limit is set to [0].)
endif # }
ifeq ($(CORE_FILE_SIZE),UNKNOWN) # {
	@$(warning $(WARNING_LABEL) Core file size limit is [UNKNOWN].)
endif # }
	@echo 'CORE_FILE = [$(CORE_FILE)]'
	@$(GDB) -q ./$(EXE) $(CORE_FILE)
else # } {
	@$(info $(INFO_LABEL) No core file found.)
endif # }

gdb: $(EXE)
	@{ [ -f $(CURDIR)/breaks ] && $(GDB) -q -command=$(CURDIR)/breaks ./$(EXE); } || $(GDB) -q ./$(EXE)

################################################################################
# Usage:                                                                       #
#   make gdb_script GDB_FILE=script_file                                       #
################################################################################
GDB_LOG=./$(EXE)_gdb.log
GDB_BATCH_OPTS=-ex "set logging off" -ex "set logging file $(GDB_LOG)"  -ex "set logging overwrite on" -ex "set logging on" -batch -n -q
gdb_script: $(EXE)

################################################################################
# If GDB_FILE has not been specified by the user, we have an error.            #
################################################################################
ifndef GDB_FILE # {
	@$(error $(ERROR_LABEL) The gdb script file has not been specified. Usage: make gdb_script GDB_FILE=script_file)
endif # }

################################################################################
# If GDB_FILE does not exist, we have an error.                                #
################################################################################
ifeq ($(wildcard $(GDB_FILE)),) # {
	@$(error $(ERROR_LABEL) The specified gdb script file [$(GDB_FILE)] does not exist)
endif # }

	@echo 'gdb script file = [$(GDB_FILE)]'
	@echo 'gdb log file    = [$(GDB_LOG)]'
	@if [[ -r $(GDB_FILE) ]];                                                    \
   then                                                                        \
     $(GDB) -x $(GDB_FILE) $(GDB_BATCH_OPTS) ./$(EXE);                         \
   else                                                                        \
     echo '$(ERROR_LABEL) The gdb script file [$(GDB_FILE)] is not readable.'; \
     exit 1;                                                                   \
   fi

clean::
	@rm -f $(GDB_LOG) gdb.log gdb_history.log

else # } {
core gdb gdb_script:
	@$(error $(ERROR_LABEL) The [$(DEBUGGER)] debugger is not available)
endif # }

################################################################################
# END gdb section.                                                             #
################################################################################

STRIP:=$(shell command -v strip)
.PHONY: strip
strip: $(EXE)
ifndef STRIP # {
	@$(error $(ERROR_LABEL) The strip command is not available)
endif # }
	@strip ./$(EXE)

.PHONY: demo
demo:: $(EXE)
	@./$(EXE) $(RUN_OPTS) 2>&1 | tee $(EXE).log
	@$(call open_file_command,./$(EXE).log,$(EDITOR),$(EXE) demo log)

FILE_PERMS=644
EXEC_PERMS=744
.PHONY: perms
perms:
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@find . -type f -not \( -path "*/dOxygen/*" -o -path "*/$(CLANG_OUTPUT_DIR)/*" -o -path "*/\.git/*" -o -path "*/\.settings/*" \) ! \( -name "\.cproject" -o -name "\.project" -o -name "\.pydevproject" \) ! -executable -exec chmod $(FILE_PERMS) {} \;
	@find . -type d -not \( -path "*/dOxygen/*" -o -path "*/$(CLANG_OUTPUT_DIR)/*" -o -path "*/\.git/*" -o -path "*/\.settings/*" \) ! \( -name "\.cproject" -o -name "\.project" -o -name "\.pydevproject" \) -exec chmod $(EXEC_PERMS) {} \;

TIME_LOG=$(EXE)_time.log
.PHONY: $(TIME_LOG)
$(TIME_LOG): $(EXE)
	@{ time ./$(EXE) $(RUN_OPTS) > /dev/null 2>&1; } 2>&1 | sed 's/^$$/$(EXE) time:/' | sed 's/\t/ /g;s/ \+/ /' | column -s" " -o" " -t | tee $(@)

.PHONY: time
time: $(TIME_LOG)

clean::
	@rm -f $(TIME_LOG)

ARCHIVE_FILES=$(CURDIR)/$(EXE) $(DIGESTS_FILE)
ARCHIVE_NAME_STUB=$(CURDIR)/$(notdir $(CURDIR)).tar

################################################################################
# Creates recipes needed to make tarred archives.                              #
#                                                                              #
# $(1) - recipe prefix, e.g., BZIP2                                            #
# $(2) - compressiom executable, e.g., bzip2                                   #
# $(3) - archive file extension, e.g., bz2                                     #
################################################################################
define create_archive_recipes
.PHONY: $(2)
ifneq ($(shell command -v $(2)),) # {
$(1)_ARCHIVE=$(2)
$(1)_FILE=$(ARCHIVE_NAME_STUB).$(3)
$(2): $(ARCHIVE_FILES)
	@mkdir -p $$(@)
	@echo 'ARCHIVE_FILES = [$$(ARCHIVE_FILES)]'
	@cp $$(ARCHIVE_FILES) $$($(1)_ARCHIVE)
	@tar --$(2) -cvf $$($(1)_FILE) $$($(1)_ARCHIVE)
	@rm -rf $$($(1)_ARCHIVE)/
	@echo 'Table Of Contents: tar --$(2) -tvf $$($(1)_FILE)'
	@echo 'Extract Command:   tar --$(2) -xvf $$($(1)_FILE)'

.PHONY: $(2)_clean
$(2)_clean:
	@([[ -d $$($(1)_ARCHIVE) ]] && rm -rf $$($(1)_ARCHIVE)) || :
	@rm -f $$($(1)_FILE) $(DIGESTS_FILE)

clean:: $(2)_clean
else # } {
$(2):
	@echo '$(WARNING_LABEL) The compression executable [$(2)] was not found.'
endif # }
endef

$(eval $(call create_archive_recipes,BZIP2,bzip2,bz2))
$(eval $(call create_archive_recipes,GZIP,gzip,gz))
$(eval $(call create_archive_recipes,LZIP,lzip,lz))
$(eval $(call create_archive_recipes,LZMA,lzma,lzma))
$(eval $(call create_archive_recipes,LZOP,lzop,lzo))
$(eval $(call create_archive_recipes,XZ,xz,xz))

TEMP_ZIP_ARCHIVE=zip_directory
$(TEMP_ZIP_ARCHIVE):
	@mkdir -p $(TEMP_ZIP_ARCHIVE)

ZIP_FILE=$(subst .tar,.zip,$(ARCHIVE_NAME_STUB))
.PHONY: zip
ifneq ($(shell command -v zip),) # {
zip: $(ARCHIVE_FILES) | $(TEMP_ZIP_ARCHIVE)
	@echo 'ARCHIVE_FILES = [$(ARCHIVE_FILES)]'
	@cp $(ARCHIVE_FILES) $(TEMP_ZIP_ARCHIVE)
	@zip $(ZIP_OPTS) $(ZIP_FILE) $(TEMP_ZIP_ARCHIVE)
	@echo 'Table Of Contents: unzip -l $(ZIP_FILE)'
	@echo 'Extract command:   unzip $(ZIP_FILE)'
else # } {
zip:
	@echo '$(WARNING_LABEL) The compression executable [zip] was not found.'
endif # }

.PHONY: zip_clean
zip_clean:
	@rm -rf $(TEMP_ZIP_ARCHIVE)
	@rm -f $(ZIP_FILE) $(DIGESTS_FILE)

clean:: zip_clean

PROFILERS       ?= $(PROJECT_ROOT)/profilers
STATIC_ANALYSIS ?= $(PROJECT_ROOT)/static_analysis
UNIT_TESTS      ?= $(PROJECT_ROOT)/unit_tests

common_mk_help::
ifdef EXE # {
	@echo '$(MAKE) → builds the executable $(EXE)'
	@echo '$(MAKE) bzip2 → creates the bzip2 compressed tar archive [$(BZIP2_FILE)] with the files [$(sort $(notdir $(ARCHIVE_FILES)))]'
ifneq ($(OPENCL_ENUMS_HEADERS),) # {
	@echo '$(MAKE) clenums → creates the OpenCL enums header and implementation files'
endif # }
	@echo '$(MAKE) core → starts gdb on $(EXE) with the most recent core file'
	@echo '$(MAKE) demo → executes $(EXE) and saves output to [$(EXE).log] and opens this file with $(EDITOR)'
	@echo '$(MAKE) gdb → starts gdb on $(EXE)'
	@echo '$(MAKE) gdb_script GDB_FILE=script_file → starts gdb on $(EXE) using the gdb script file specified in GDB_FILE'
	@echo '$(MAKE) gzip → creates the gzip compressed tar archive [$(GZIP_FILE)] with the files [$(sort $(notdir $(ARCHIVE_FILES)))]'
	@echo '$(MAKE) time → executes $(EXE) and shows timing information; this information is saved in [$(TIME_LOG)]'
	@echo '$(MAKE) xz → creates the xz compressed tar archive [$(XZ_FILE)] with the files [$(sort $(notdir $(ARCHIVE_FILES)))]'
	@echo '$(MAKE) zip → creates the zip compressed archive [$(ZIP_FILE)] with the files [$(sort $(notdir $(ARCHIVE_FILES)))]'
endif # }
	@echo '$(MAKE) clean → cleans'

show_help:: common_mk_help

include $(PROJECT_ROOT)/doxygen.mk

endif # }

