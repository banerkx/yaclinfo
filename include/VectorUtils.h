
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <algorithm>
#include <iostream>
#include <iterator>
#include <ostream>
#include <sstream>
#include <string>
#include <valarray>
#include <vector>

template<class T> void printVectorHorizontal(
  const std::string&    sLabel,
  const std::vector<T>& v,
  std::ostream&         tStream = std::cout)
{
  tStream << sLabel << ":" << std::endl;
  tStream << "SIZE = [" << v.size() << "]" << std::endl;
  tStream << "[";
  std::copy(v.begin(), v.end() - 1, std::ostream_iterator<T>(tStream, ", "));
  if (false == v.empty())
  {
    tStream << v.back();
  }
  tStream << "]" << std::endl;
}

template<class T> void printVector(
  const std::string&                      sLabel,
  const std::vector<T>&                   v,
  typename std::vector<T>::const_iterator first,
  typename std::vector<T>::const_iterator last,
  std::ostream&                           tStream = std::cout)
{
  tStream << sLabel << ":" << std::endl;
  tStream << "SIZE = [" << v.size() << "]" << std::endl;

  auto print = [&tStream](const T& e) -> void
  {
    static size_t i = 0;
    tStream << "v[" << i << "] = [" << e << "]\n";
    ++i;
  };
  std::for_each(first, last, print);
}

template<class T> void
printVector(const std::string& sLabel, const std::vector<T>& v, std::ostream& tStream = std::cout)
{
  printVector(sLabel, v, v.begin(), v.end(), tStream);
}

template<class T> void reverse(std::vector<T>& v)
{
  std::reverse(v.begin(), v.end());
}

template<class T> std::string formatWithLocale(const T nValue)
{
  std::stringstream tNumber;
  tNumber.imbue(std::locale(""));
  tNumber << std::fixed << nValue;
  return (tNumber.str());
}

template<>
void printVector(const std::string& sLabel, const std::vector<size_t>& v, std::ostream& tStream)
{
  tStream << sLabel << ":" << std::endl;
  tStream << "SIZE = [" << v.size() << "]" << std::endl;
  tStream << "[";

  for (size_t i = 0; i < v.size() - 1; i++)
  {
    tStream << formatWithLocale(v[i]) << "  ";
  }

  tStream << formatWithLocale(v[v.size() - 1]) << "]" << std::endl;
}

template<class T> void printValarrayHorizontal(
  const std::string&      sLabel,
  const std::valarray<T>& v,
  std::ostream&           tStream = std::cout)
{
  tStream << sLabel << ":" << std::endl;
  tStream << "SIZE = [" << v.size() << "]" << std::endl;
  tStream << "[";
  std::copy(std::begin(v), std::end(v) - 1, std::ostream_iterator<T>(tStream, ", "));
  if (0 != v.size())
  {
    tStream << v[v.size() - 1];
  }
  tStream << "]" << std::endl;
}

template<class T> void printValarray(
  const std::string&      sLabel,
  const std::valarray<T>& v,
  std::ostream&           tStream = std::cout)
{
  tStream << sLabel << ":" << std::endl;
  tStream << "SIZE = [" << v.size() << "]" << std::endl;
  std::copy(std::begin(v), std::end(v), std::ostream_iterator<T>(tStream, "\n"));
}

template<class T> std::valarray<T> convert(const std::vector<T>& v)
{
  return (std::valarray<T>(&v[0], v.size()));
}

template<class T> std::vector<T> convert(const std::valarray<T>& v)
{
  return (std::vector<T>(std::begin(v), std::end(v)));
}

