
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PlatformInfo_110.h"

#include "DeviceInfo_110.h"

PlatformInfo_110::PlatformInfo_110(
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nPlatformVersion) noexcept :
  PlatformInfo(tPlatformID, nPlatformNumber, nPlatformVersion)
{
}

PlatformInfo_110::~PlatformInfo_110() noexcept
{
}

void PlatformInfo_110::getPlatformAttributes()
{
  constexpr static size_t nBufferSize = 2048;
  char                    pPlatformInfoBuffer[nBufferSize + 1];
  size_t                  nParamValueSize = 0;

  cl_int nStat = CL_SUCCESS;
  for (size_t i = 0; i < m_tPlatformInfoTypes.size(); ++i)
  {
    std::memset(pPlatformInfoBuffer, 0, nBufferSize + 1);
    nStat = clGetPlatformInfo(
      m_tPlatformID,
      scopedEnumToCLEnum(m_tPlatformInfoTypes[i]),
      nBufferSize,
      pPlatformInfoBuffer,
      &nParamValueSize);

    switch (nStat)
    {
      case CL_SUCCESS:
        add(scopedEnumToCLEnum(m_tPlatformInfoTypes[i]), pPlatformInfoBuffer);
        break;

      case CL_INVALID_PLATFORM:
        add(scopedEnumToCLEnum(m_tPlatformInfoTypes[i]), "Invalid platform ID.");
        break;

      case CL_INVALID_VALUE:
      {
        std::stringstream tError;
        tError << "ERROR: Invalid value: [" << m_tPlatformInfoTypes[i] << "] ==> [0x"
               << std::internal << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
               << scopedEnumToCLEnum(m_tPlatformInfoTypes[i]) << "] = [" << std::nouppercase
               << std::dec << scopedEnumToCLEnum(m_tPlatformInfoTypes[i]) << "].";
        add(scopedEnumToCLEnum(m_tPlatformInfoTypes[i]), tError.str());
      }
      break;

      case CL_OUT_OF_HOST_MEMORY:
        add(scopedEnumToCLEnum(m_tPlatformInfoTypes[i]), "Out of host memory.");
        break;

      default:
        add(scopedEnumToCLEnum(m_tPlatformInfoTypes[i]), "Unknown error.");
        break;
    };
  }
}

