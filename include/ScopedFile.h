
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdio>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

class ScopedInputFile : public std::ifstream
{
  public:

    explicit ScopedInputFile(const std::string& sFileName);

    virtual ~ScopedInputFile() override;

    inline const std::string& fileName() const
    {
      return (m_sFileName);
    }

  private:

    std::string m_sFileName;

    // Intentionally deleted.
    ScopedInputFile()                           = delete;
    ScopedInputFile(const ScopedInputFile& rhs) = delete;
    ScopedInputFile(ScopedInputFile&& rhs)      = delete;
    ScopedInputFile&       operator=(const ScopedInputFile& rhs) = delete;
    ScopedInputFile&       operator=(ScopedInputFile&& rhs) = delete;
    ScopedInputFile*       operator&()                      = delete;
    const ScopedInputFile* operator&() const                = delete;
};

class ScopedOutputFile : public std::ofstream
{
  public:

    explicit ScopedOutputFile(
      const std::string&            sFileName,
      const std::ios_base::openmode nOpenMode = std::ios_base::out);

    virtual ~ScopedOutputFile() override;

    inline const std::string& fileName() const
    {
      return (m_sFileName);
    }

  protected:

    const std::string             m_sFileName;
    const std::ios_base::openmode m_nOpenMode;

  private:

    // Intentionally unimplemented.
    ScopedOutputFile();
    ScopedOutputFile(const ScopedOutputFile& rhs);
    ScopedOutputFile&       operator=(const ScopedOutputFile& rhs);
    ScopedOutputFile*       operator&();
    const ScopedOutputFile* operator&() const;
};

class ScopedBinaryOutputFile : public ScopedOutputFile
{
  public:

    explicit ScopedBinaryOutputFile(
      const std::string&            sFileName,
      const std::ios_base::openmode nOpenMode = std::ios_base::out);

    virtual ~ScopedBinaryOutputFile() override;

  private:

    // Intentionally unimplemented.
    ScopedBinaryOutputFile();
    ScopedBinaryOutputFile(const ScopedBinaryOutputFile& rhs);
    ScopedBinaryOutputFile&       operator=(const ScopedBinaryOutputFile& rhs);
    ScopedBinaryOutputFile*       operator&();
    const ScopedBinaryOutputFile* operator&() const;
};

class ScopedFileDescriptor
{
  public:

    explicit ScopedFileDescriptor(const int nFileDescriptor);

    ~ScopedFileDescriptor();

    operator int() const;

    ScopedFileDescriptor& operator=(const int nFD);

    bool operator==(const int rhs) const;

    friend bool operator==(const int lhs, const ScopedFileDescriptor& rhs);

    bool operator!=(const int rhs) const;

    friend bool operator!=(const int lhs, const ScopedFileDescriptor& rhs);

  private:

    int m_nFileDescriptor;

    // Intentionally deleted.
    ScopedFileDescriptor()                                = delete;
    ScopedFileDescriptor(const ScopedFileDescriptor& rhs) = delete;
    ScopedFileDescriptor(ScopedFileDescriptor&& rhs)      = delete;
    ScopedFileDescriptor&       operator=(const ScopedFileDescriptor& rhs) = delete;
    ScopedFileDescriptor&       operator=(ScopedFileDescriptor&& rhs) = delete;
    ScopedFileDescriptor*       operator&()                           = delete;
    const ScopedFileDescriptor* operator&() const                     = delete;
};

class ScopedFILE
{
  public:

    explicit ScopedFILE(FILE* pFile);

    ~ScopedFILE();

    operator FILE*() const;

  private:

    FILE* m_pFile;

    // Intentionally deleted.
    ScopedFILE()                      = delete;
    ScopedFILE(const ScopedFILE& rhs) = delete;
    ScopedFILE(ScopedFILE&& rhs)      = delete;
    ScopedFILE&       operator=(const ScopedFILE& rhs) = delete;
    ScopedFILE&       operator=(ScopedFILE&& rhs) = delete;
    ScopedFILE*       operator&()                 = delete;
    const ScopedFILE* operator&() const           = delete;
};

