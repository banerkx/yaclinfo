
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CCCC_MK_INCLUDE_GUARD # {
CCCC_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

CCCC_COMMAND=cccc
CCCC:=$(shell command -v $(CCCC_COMMAND))

CCCC_GITHUB_URL=https://github.com/sarnold/cccc

CCCC_PROJECT=$(notdir $(CURDIR))
CCCC_OUTPUT_DIR=$(CCCC_PROJECT)_cccc
CCCC_HTML_FILE=$(CCCC_OUTPUT_DIR)/$(CCCC_PROJECT)_cccc.html
CCCC_DATABASE=$(CCCC_OUTPUT_DIR)/$(CCCC_PROJECT)_cccc.db
CCCC_LOG=$(CCCC_OUTPUT_DIR)/$(CCCC_PROJECT)_cccc.log

CCCC_TARGETS=cccc cccc_help cccc_clean cccc_mk_help
CCCC_EXTERNAL_TARGETS=clean platform_info print-% qprint-% dox doxygen post_doxygen
.PHONY: $(CCCC_TARGETS)
ifneq ($(filter $(CCCC_TARGETS) $(CCCC_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

ifneq ($(CCCC),) # {

CCCC_VERSION=$(call get_stderr_version,$(CCCC),,| grep "^Version" | sed 's/^Version //')

EDITOR ?= vim +1
BROWSER ?= firefox

CCCC_OPTS=--outdir=$(CCCC_OUTPUT_DIR) --html_outfile=$(CCCC_HTML_FILE) --lang=c++ --db_outfile=$(CCCC_DATABASE)

CCCC_FILES=$(call get_source_files)

ifneq ($(CCCC_FILES),) # {

IMAGE_PATH ?= $(PROJECT_ROOT)/images
CYCLOMATIC_COMPLEXITY_IMAGE=$(IMAGE_PATH)/cyclomatic_complexity.png
MCCABE_PAPER_URL=http://www.mccabe.com/pdf/mccabe-nist235r.pdf
MCCABE_PAPER_IMAGE_URL=<center><a href="$(MCCABE_PAPER_URL)" target="_blank"><img src="$(notdir $(CYCLOMATIC_COMPLEXITY_IMAGE))" $(call get_gif_png_dims,$(CYCLOMATIC_COMPLEXITY_IMAGE))/></a></center>

cccc: REFERENCE_LINKS += www.google.com/search?q=chidamber+kemerer+metrics:Chidamber\x20Kemerer\x20metrics\x20google\x20search
cccc: REFERENCE_LINKS += www.google.com/search?q=henry+kafura:Henry\x20Kafura\x20google\x20search
cccc: REFERENCE_LINKS += www.cqse.eu/en/news/blog/mccabe-cyclomatic-complexity:McCabe\x27s\x20Cyclomatic\x20Complexity\x20and\x20Why\x20We\x20Don\x27t\x20Use\x20It
cccc: REFERENCE_LINKS += www.google.com/search?q=cyclomatic+complexity:google\x20search
cccc: REFERENCE_LINKS += www.youtube.com/results?search_query=cyclomatic+complexity:You\x20Tube\x20search

CYCLOMATIC_FAVICON=$(IMAGE_PATH)/cyclomatic_favicon.png
cccc: CCCC_GITHUB_URL_LINK=<a href="$(CCCC_GITHUB_URL)" target="_blank"><b>CCCC</b></a>
cccc: CYCLOMATIC_COMPLEXITY_WIKI=<a href="https://en.wikipedia.org/wiki/Cyclomatic_complexity" target="_blank"><b>McCabe\x27s cyclomatic complexity</b></a>
cccc: cccc_clean
	@$(eval $(@)_START_TIME:=$(shell date +%s))
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@mkdir -p $(CCCC_OUTPUT_DIR)
	@echo '========== BEGIN $(CCCC_PROJECT) cccc ==========' | tee $(CCCC_LOG)
	@$(CCCC) $(CCCC_OPTS) $(CCCC_FILES) 2>&1 | sed 's@Database dump is in $(CCCC_OUTPUT_DIR)/$(CCCC_COMMAND).db@Database dump is in $(CCCC_DATABASE)@' | tee -a $(CCCC_LOG)
	@echo '==========  END  $(CCCC_PROJECT) cccc ==========' | tee -a $(CCCC_LOG)
	@sed -i 's@CCCC Software Metrics Report@$(CCCC_PROJECT) $(CCCC_GITHUB_URL_LINK) Software Metrics Report@' $(CCCC_HTML_FILE)
	@sed -i 's@McCabe\x27s cyclomatic complexity@$(CYCLOMATIC_COMPLEXITY_WIKI)@' $(CCCC_HTML_FILE)
ifneq ($(wildcard $(CYCLOMATIC_COMPLEXITY_IMAGE)),)
	@cp $(CYCLOMATIC_COMPLEXITY_IMAGE) $(CCCC_OUTPUT_DIR)
	@sed -i 's@<HEAD>@<HEAD>$(MCCABE_PAPER_IMAGE_URL)@' $(CCCC_HTML_FILE)
	@sed -i '\@^</BODY>@i <table border width="50%">' $(CCCC_HTML_FILE)
	@sed -i '\@^</BODY>@i <tr><th colspan="1">References</th></tr>' $(CCCC_HTML_FILE)
	@$(foreach link,$(REFERENCE_LINKS),sed -i '\@^</BODY>@i <tr><td><a href="https://$(firstword $(subst :, ,$(link)))" target="_blank">$(lastword $(subst :, ,$(link)))</a></td></tr>' $(CCCC_HTML_FILE);)
	@sed -i '\@^</BODY>@i </table>' $(CCCC_HTML_FILE)
	@$(call beautify_html,$(CCCC_HTML_FILE))
endif
ifneq ($(wildcard $(CYCLOMATIC_FAVICON)),) # {
	@$(call add_favicon,$(CYCLOMATIC_FAVICON),$(CCCC_HTML_FILE))
endif # }
	@$(call open_sole_target_file,$(@),$(CCCC_LOG),$(EDITOR),cccc log file)
	@$(call open_sole_target_file,$(@),$(CCCC_DATABASE),$(EDITOR),cccc database dump)
	@$(call open_sole_target_file,$(@),$(CCCC_HTML_FILE),$(BROWSER),cccc html results)
	@$(if $(elapsed_time),$(call elapsed_time,$(@),$($(@)_START_TIME)))

else # } {

cccc:
	@echo '[$(@)] $(INFO_LABEL) No source files found.'

endif # }

CCCC_HELP=cccc_help.txt
cccc_help:
	@$(CCCC) --help >| $(CCCC_HELP)
	@$(call open_file_command,$(CCCC_HELP),$(EDITOR),cccc help file)

cccc_clean:
	@rm -rf $(CCCC_HELP) $(CCCC_OUTPUT_DIR)
	@clear

clean:: cccc_clean

cccc_mk_help::
	@echo '$(MAKE) cccc → runs the $(CCCC_COMMAND) analyzer on the source files, saving the results to [$(CCCC_HTML_FILE)]'
	@echo '$(MAKE) cccc_help → saves $(CCCC_COMMAND) help information to the file [$(CCCC_HELP)]'

platform_info::
	@echo '$(CCCC_COMMAND) version = [$(CCCC_VERSION)]'

show_help:: cccc_mk_help

else # } {

################################################################################
# At this point, we know that cccc is not available. So we tell the user.      #
################################################################################
$(CCCC_TARGETS):
	@$(error $(ERROR_LABEL) The [$(CCCC_COMMAND)] executable is not available, ∴ the target [$(@)] can not be made; see [$(CCCC_GITHUB_URL)])

endif # }

endif # }

endif # }

