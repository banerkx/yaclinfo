
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <cppunit/BriefTestProgressListener.h>
#include <getopt.h>
#include <unistd.h>

#include <boost/filesystem.hpp>
#include <chrono>
#include <cstring>
#include <iostream>
#include <memory>
//#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestSuccessListener.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/portability/Stream.h>

#include "CppUnitTestRunner.h"
#include "CppUnitTestTimingsCollector.h"
#include "CppUnitTestXMLOutputterHook.h"

typedef CPPUNIT_NS::CppUnitTestTimingsCollector::TimeInfo TimeInfo;

namespace CppUnitTestRunner
{
  void usage(
    const std::string &sProgramName,
    const std::string &sDefaultTestsResultsFile,
    const std::string &sDefaultTestsTitle,
    const short        nExitValue)
  {
    std::cout << "Usage: \n"
              << boost::filesystem::basename(sProgramName)
              << " --file,-f [test results file] --noelapsed,-n --title,-t [test "
                 "title] --xml,-x --help,-h\n"
              << "--file,-f [test results file] Specifies the tests results "
                 "output file. Default is ["
              << sDefaultTestsResultsFile << "].\n"
              << "                              Optional.\n"
              << "--noelapsed,-n                Do not save the elapsed times of "
                 "the tests. Default is to save the elapsed\n"
              << "                              times. Optional.\n"
              << "--title,-t [test title]       Specifies the tests' title. "
                 "Default is ["
              << sDefaultTestsTitle << "]. Optional.\n"
              << "--xml,-x                      Save the tests' results in XML "
                 "format to the tests results file. Optional.\n"
              << "--help,-h                     Print this usage information and "
                 "exit. Takes precedence over all other options. Optional.\n";
    exit(nExitValue);
  }

  std::string createOptionsString(const struct option *pOptions)
  {
    std::string sOptions;
    for (unsigned short i = 0; 0 != pOptions[i].val; ++i)
    {
      sOptions.push_back(static_cast<char>(pOptions[i].val));
      if (required_argument == pOptions[i].has_arg)
      {
        sOptions.push_back(':');
      }
      else if (optional_argument == pOptions[i].has_arg)
      {
        sOptions.push_back(':');
        sOptions.push_back(':');
      }
    }
    return (sOptions);
  }

  void handleOptionError(
    const char         cOpt,
    const std::string &sProgramName,
    const std::string &sDefaultTestsResultsFile,
    const std::string &sDefaultTestsTitle,
    const std::string &sOptions)
  {
    if (isprint(cOpt))
    {
      // The error could be that a required parameter was not used with the
      // command line cOption.
      const size_t nOptLocation = sOptions.find_first_of(cOpt);

      // The command line cOption is not valid.
      if (std::string::npos == nOptLocation)
      {
        std::cerr << "ERROR: The option [" << static_cast<char>(cOpt) << "] is not recognized.\n";
      }
      // The command line option is missing a required parameter.
      else
      {
        std::cerr << "ERROR: The option [" << static_cast<char>(cOpt)
                  << "] is missing a required parameter.\n";
      }

      usage(sProgramName, sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
    }
    else
    {
      std::cerr << "ERROR: The command line option is not printable and unknown.\n";
      usage(sProgramName, sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
    }
  }

  void ensureTextExtension(std::string &sFile)
  {
    if (false == sFile.empty())
    {
      constexpr static const char *pExt = "txt";
      boost::filesystem::path      tPath(sFile);
      tPath.replace_extension(pExt);
      sFile = tPath.string();
    }
  }

  void ensureXMLExtension(std::string &sFile)
  {
    if (false == sFile.empty())
    {
      constexpr static const char *pExt = "xml";
      boost::filesystem::path      tPath(sFile);
      tPath.replace_extension(pExt);
      sFile = tPath.string();
    }
  }

  void processCommandLineOptions(
    const std::string &sDefaultTestsResultsFile,
    std::string       &sTestsResultsFile,
    bool              &bNoElapsedTime,
    const std::string &sDefaultTestsTitle,
    std::string       &sTestsTitle,
    bool              &bXML,
    int                argc,
    char             **argv)
  {
    static struct option pOptions[] = {
      {"file",      required_argument, nullptr, 'f'},
      {"noelapsed", no_argument,       nullptr, 'n'},
      {"title",     required_argument, nullptr, 't'},
      {"help",      no_argument,       nullptr, 'h'},
      {"xml",       no_argument,       nullptr, 'x'},
      {nullptr,     0,                 nullptr, 0  },
    };

    const std::string sOptions(createOptionsString(pOptions));

    int nOpt         = 0;
    int nOptionIndex = 0;

    // Suppressing getopt_long()'s own error messages.
    opterr = 0;

    while ((nOpt = getopt_long(argc, argv, sOptions.c_str(), pOptions, &nOptionIndex)) != -1)
    {
      switch (nOpt)
      {
        case 'e':
          bNoElapsedTime = true;
          break;

        case 'f':
          sTestsResultsFile = optarg;
          break;

        case 't':
          sTestsTitle = optarg;
          break;

        case 'x':
          bXML = true;
          break;

        case 'h':
          usage(argv[0], sDefaultTestsResultsFile, sDefaultTestsTitle, 0);
          break;

        case '?':
        default:

          handleOptionError(
            static_cast<char>(optopt),
            argv[0],
            sDefaultTestsResultsFile,
            sDefaultTestsTitle,
            sOptions);
          break;
      };
    }
  }

  int executeCppUnitTests(
    const std::string     &sTestsResultsFile,
    const std::string     &sTestsTitle,
    const char             cSeparator,
    const bool             bNoElapsedTime,
    const bool             bXML,
    CPPUNIT_NS::TestSuite &tTestSuite)
  {
    CPPUNIT_NS::TestResult          tTestResult;
    CPPUNIT_NS::TestSuccessListener tSuccessListener;
    tTestResult.addListener(&tSuccessListener);

    std::unique_ptr<CPPUNIT_NS::TestResultCollector> pCollector(
      (false == bNoElapsedTime)
        ? new CPPUNIT_NS::CppUnitTestTimingsCollector(CPPUNIT_NS::stdCOut(), cSeparator)
        : new CPPUNIT_NS::TestResultCollector());
    tTestResult.addListener(pCollector.get());

    // TestListener that prints the name of each test before running it.
    std::unique_ptr<CPPUNIT_NS::BriefTestProgressListener> pBriefProgress(nullptr);
    pBriefProgress.reset(new CPPUNIT_NS::BriefTestProgressListener());
    tTestResult.addListener(pBriefProgress.get());

    // Redirecting stdout to sTestsResultsFile.
    std::freopen(sTestsResultsFile.c_str(), "w", stdout);

    // Writing the tests' title and date/time.
    if (false == bXML)
    {
      std::cout << sTestsTitle << std::endl;
      time_t nRawTime;
      time(&nRawTime);
      std::cout << ctime(&nRawTime) << std::endl;
    }

    // Tests' start time.
    std::chrono::high_resolution_clock::time_point tStartTime;
    if (false == bNoElapsedTime)
    {
      tStartTime = std::chrono::high_resolution_clock::now();
    }

    // Running the unit tests.
    tTestSuite.run(&tTestResult);

    std::string sTotalExecutionTime;
    if (false == bNoElapsedTime)
    {
      std::chrono::high_resolution_clock::time_point tStopTime(
        std::chrono::high_resolution_clock::now());
      sTotalExecutionTime +=
        CPPUNIT_NS::CppUnitTestTimingsCollector::secondsToText(tStopTime, tStartTime);
      if (false == bXML)
      {
        CPPUNIT_NS::stdCOut() << std::endl
                              << "TOTAL EXECUTION TIME = [" << sTotalExecutionTime << "]"
                              << std::endl;
      }
    }

    // Outputting a new line that will separate those tests that passed and
    // those tests that failed.
    CPPUNIT_NS::stdCOut() << '\n';
    if (true == bXML)
    {
      CPPUNIT_NS::XmlOutputter tUnitTestOutputter(pCollector.get(), CPPUNIT_NS::stdCOut());
      CPPUNIT_NS::CppUnitTestXMLOutputterHook tXMLHook(sTestsTitle, sTotalExecutionTime);
      tUnitTestOutputter.addHook(&tXMLHook);
      tUnitTestOutputter.write();
    }
    else
    {
      //      CPPUNIT_NS::CompilerOutputter tUnitTestOutputter(pCollector.get(),
      //      CPPUNIT_NS::stdCOut());
      //      // NOTE: The format string "%p:%l " means that the full path name
      //      and line
      //      //       number of those unit tests that fail will be output.
      //      tUnitTestOutputter.setLocationFormat("%p:%l ");

      CPPUNIT_NS::TextOutputter tUnitTestOutputter(pCollector.get(), CPPUNIT_NS::stdCOut());
      tUnitTestOutputter.write();
    }
    std::fclose(stdout);

    return ((true == tSuccessListener.wasSuccessful()) ? 0 : 1);
  }
}  // namespace CppUnitTestRunner

