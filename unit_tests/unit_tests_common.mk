
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef UNIT_TESTS_COMMON_MK_INCLUDE_GUARD # {
UNIT_TESTS_COMMON_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

BOOST_UNIT_TEST_LOGO=boost_test_logo.png
CATCH2_UNIT_TEST_LOGO=catch2_unit_test.png
CPP_UNIT_TEST_LOGO=cppunit.png
CRITERION_UNIT_TEST_LOGO=criterion_unit_test.png
GTEST_UNIT_TEST_LOGO=google_test.png
UNIT_TEST_LOGOS=$(BOOST_UNIT_TEST_LOGO) $(CATCH2_UNIT_TEST_LOGO) $(CPP_UNIT_TEST_LOGO) $(CRITERION_UNIT_TEST_LOGO) $(GTEST_UNIT_TEST_LOGO)

################################################################################
# If the input header path is not already in $(INCLUDES), then it is added to  #
# $(INCLUDES):                                                                 #
#   o if the header path begins with /usr/include, then -I is used             #
#   o otherwise, -isystem is used                                              #
#                                                                              #
# $(1) - input header path                                                     #
################################################################################
define add_to_includes
ifeq ($(wordlist 1,2,$(strip $(subst /, ,$(1)))),usr include) # {
$(if $(wildcard $(1)),,$(error $(ERROR_LABEL) In function [$(0)], input path [$(1)] does not exist))
ifeq ($(findstring -I $(1),$(INCLUDES)),) # {
INCLUDES += -I $(1)
endif # }
else # } {
ifeq ($(findstring -isystem $(1),$(INCLUDES)),) # {
INCLUDES += -isystem $(1)
endif # }
endif # }
endef

################################################################################
# When linking in the static library that holds the Google test class(es), the #
# entire library must be linked. For example, if the static library is named   #
# libmygtest.a, then the link step must include:                               #
#   -Wl,--whole-archive -lmygtest -Wl,--no-whole-archive                       #
# The following two variables are used to accomplish this step.                #
################################################################################
ifndef WHOLE_ARCHIVE # {
WHOLE_ARCHIVE=-Wl,--whole-archive
endif # }
ifndef NO_WHOLE_ARCHIVE # {
NO_WHOLE_ARCHIVE=-Wl,--no-whole-archive
endif # }

################################################################################
# Modifies the input set of static libraries so that the entire specified      #
# static library is used for linking.                                          #
#                                                                              #
# $(1) - the static library that must be linked in its entirety                #
# $(2) - the set of libraries that the executable must be linked with          #
#        ($(1) must be in this list)                                           #
#   Assume that the list of libraries that the executable must link with is:   #
#     -labc -ldef -lXYZ                                                        #
#   Further assume that libdef.a must be linked in its entirety, then the call #
#   would be:                                                                  #
#     $(call enforce_whole_lib,-ldef,-labc -ldef -lXYZ)                        #
################################################################################
enforce_whole_lib=$(if $(1),$(if $(2),$(subst -l$(1),$(WHOLE_ARCHIVE) -l$(1) $(NO_WHOLE_ARCHIVE),$(2))))

################################################################################
# User defined function to beautify XML files using xmllint. For this function #
# to work:                                                                     #
#   - the xmllint command must be available                                    #
#   - the input XML file name must be non-empty                                #
#   - the input XML file must exist                                            #
#                                                                              #
# $(1) - input XML file                                                        #
################################################################################
XMLLINT_COMMAND=xmllint
XMLLINT:=$(shell command -v $(XMLLINT_COMMAND))
xml_beautify=$(if $(XMLLINT),$(XMLLINT) --noout $(1) && $(XMLLINT) --output $(1) --format $(1),$(warning WARNING: [$(XMLLINT_COMMAND)] command not found.))

################################################################################
# Compiler option to allow access to private members.                          #
################################################################################
ifeq ($(findstring -fno-access-control,$(CXXFLAGS)),) # {
CXXFLAGS += -fno-access-control
endif # }

################################################################################
# In case there are set up steps before the unit tests execute.                #
################################################################################
.PHONY: unit_tests_setup
unit_tests_setup::
	@:

################################################################################
# In case there are clean up steps after the unit tests execute.               #
################################################################################
.PHONY: unit_tests_teardown
unit_tests_teardown::
	@:

################################################################################
# If the static library containing the gtest test(s) is named libmygtest.a,    #
# then LOCAL_LIB_STUB will be set to mygtest.                                  #
################################################################################
LOCAL_LIB_STUB=$(subst .a,,$(subst lib,,$(notdir $(LOCAL_LIB))))

endif # }

