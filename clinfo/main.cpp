
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <unistd.h>

#include <boost/filesystem.hpp>
#include <ctime>
#include <iostream>
#include <regex>
#include <sstream>
#include <type_traits>

#include "BuildDateTime.h"
#include "CLInfo.h"
#include "CheckIDInfo.h"
#include "ScopedFile.h"
#include "StrError.h"

// NOTE: <term.h> **must** be included only after <CL/cl.h> (or any header file
// that includes
//       <CL/cl.h>) has been included. Otherwise, the following compilation
//       errors will occur:
//         .../CL/cl.h:867:33: error: expected ‘,’ or ‘...’ before ‘->’ token
//                         cl_device_type      device_type,
//                                 ^
//         .../CL/cl.h:928:45: error: expected ‘,’ or ‘...’ before ‘->’ token
//                         cl_device_type      device_type,
#include <term.h>

/**
 * @brief displays usage information
 * @param sProgram - program name
 * @param sDefaultHTMLFile - default HTML file name
 * @param nExitValue - exit value
 */
void usage(const std::string& sProgram, const std::string& sDefaultHTMLFile, short nExitValue)
{
  std::ostream& tStream = ((0 == nExitValue) ? std::cout : std::cerr);
  tStream << "Usage:\n"
          << sProgram
          << " --html,-H [optional file name] --frame,-f [file name] --text,-t "
             "[file name] --build,-b --version,-v --help,-h\n"
          << "--html,-H [file name]  Save HTML output to the specified html "
             "file. The\n"
          << "                       default html file is [" << sDefaultHTMLFile << "].\n"
          << "                       If the \".html\" extension is omitted "
             "from the file name,\n"
          << "                       it will be added to the file name. All "
             "needed parent directories\n"
          << "                       will be created for the output html file. "
             "Optional.\n"
          << "--frame,-f [file name] Create an HTML file that will show both "
             "HTML and text\n"
          << "                       output in vertical frames. If the "
             "\".html\" extension is\n"
          << "                       omitted from the file name, it will be "
             "added to the file name.\n"
          << "                       All needed parent directories will be "
             "created for the output html\n"
          << "                       frames file. Optional.\n"
          << "--text,-t [file name]  Save text output to the specified file. "
             "If no options\n"
          << "                       are used, output is sent to stdout. If "
             "the \".txt\" extension\n"
          << "                       is omitted from the file name, it will be "
             "added to the file name.\n"
          << "                       All needed parent directories will be "
             "created for the output text\n"
          << "                       file. Optional.\n"
          << "--build,-b             Print the build date/time and exit "
             "(overrides all other\n"
          << "                       options except --help,-h). Optional.\n"
          << "--version,-v           Print the version and exit (overrides all "
             "other\n"
          << "                       options except --help,-h). Optional.\n"
          << "--help,-h              Print usage information and exit "
             "(overrides all other\n"
          << "                       options). Optional.\n\n"
          << "NOTE: If no command line option is used, then output is sent to "
             "stdout.\n"
          << "      If using the long option to specify an HTML output file, "
             "ensure that the\n"
          << "      '=' sign is used:\n"
          << "          " << sProgram << " --html=" << sDefaultHTMLFile << '\n'
          << "      If using the short option to specify an HTML output file, "
             "ensure that\n"
          << "      there is no space between \"-H\" and the file name:\n"
          << "          " << sProgram << " -H" << sDefaultHTMLFile << '\n'
          << "      If a specified file already exists, it will not be "
             "overwritten and this\n"
          << "      application will exit.\n"
          << "      The options --build,-b and --version,-v have the same "
             "precedence.\n";
  exit(nExitValue);
}

/**
 * @brief creates the command line options string used by getopt_long()
 * @param pOptions - command line options struct
 * @return - string holding the command line options for use with getopt_long()
 */
std::string createOptionsString(const struct option* pOptions)
{
  std::string sOptions;

  for (size_t i = 0; 0 != pOptions[i].val; ++i)
  {
    sOptions.push_back((char) pOptions[i].val);
    if (required_argument == pOptions[i].has_arg)
    {
      sOptions.push_back(':');
    }
    else if (optional_argument == pOptions[i].has_arg)
    {
      sOptions.push_back(':');
      sOptions.push_back(':');
    }
  }
  return (sOptions);
}

/**
 * @brief ensures that the input file has the input file extension
 * @param sFile - input file
 * @param sExt - input file extension (sExt should not have a leading '.'
 * character)
 */
void verifyFileExtension(std::string& sFile, const char* sExt)
{
  const std::regex tExtCheck("\\." + std::string(sExt) + "$");
  if (false == std::regex_search(sFile, tExtCheck))
  {
    sFile += "." + std::string(sExt);
  }
}

/**
 * @brief prints the command line options (meant as an aid in debugging)
 * @param argc - the number of command line options
 * @param argv - the command line options
 */
void printCommandLine(const int argc, char** const argv)
{
  std::stringstream tCommandLine;
  std::copy(argv, argv + argc, std::ostream_iterator<std::string>(tCommandLine, " "));
  std::cout << '[' << tCommandLine.str() << "]\n";
}

/**
 * checkOutputFile
 * @brief Checks to see if the output file already exists or not. Then creates
 *        the output file's parent directory.
 * @param sOutputFile - output file name
 */
void checkOutputFile(const std::string& sOutputFile)
{
  if (("/dev/null" != sOutputFile) && (true == boost::filesystem::exists(sOutputFile)))
  {
    std::cerr << "ERROR: The output file [" << sOutputFile
              << "] already exists; it will not be overwritten. Exiting.\n";
    exit(-1);
  }

  // If the parent directory is empty, then sOutputFile consists of just a file
  // name with no directory path portion. This is interpreted to mean that
  // sOutputFile should be in the current directory.
  boost::filesystem::path tParentDirectory(boost::filesystem::path(sOutputFile).parent_path());
  if (true == tParentDirectory.string().empty())
  {
    return;
  }

  if (false == boost::filesystem::exists(tParentDirectory))
  {
    boost::system::error_code tError;
    boost::filesystem::create_directories(tParentDirectory, tError);
    if (0 != tError.value())
    {
      std::cerr << "ERROR: Could not create the directory for output file [" << sOutputFile
                << "] due to [" << StrError::getInstance().getErrorMessage(tError.value())
                << "]. Exiting.\n";
      exit(tError.value());
    }
  }
}

/**
 * @brief processes command line options
 * @param sProgramName - program name
 * @param sOutputFile - text output file
 * @param sHTMLFile - html output file
 * @param sFrameFile - html frames output file
 * @param argc - the number of command line options
 * @param argv - the command line options
 */
void processCommandLineOptions(
  const std::string& sProgramName,
  std::string&       sOutputFile,
  std::string&       sHTMLFile,
  std::string&       sFrameFile,
  int                argc,
  char**             argv)
{
  static struct option pOptions[] = {
    {"html",    optional_argument, nullptr, 'H'},
    {"frame",   required_argument, nullptr, 'f'},
    {"text",    required_argument, nullptr, 't'},
    {"build",   no_argument,       nullptr, 'b'},
    {"version", no_argument,       nullptr, 'v'},
    {"help",    no_argument,       nullptr, 'h'},
    {nullptr,   0,                 nullptr, 0  },
  };

  const std::string sOptions(createOptionsString(pOptions));

  const std::string sDefaultHTMLFile  = std::string(argv[0]) + ".html";
  bool              bBuildDateAndTime = false;
  bool              bPrintVersion     = false;

  int nOpt         = 0;
  int nOptionIndex = 0;
  while ((nOpt = getopt_long(argc, argv, sOptions.c_str(), pOptions, &nOptionIndex)) != -1)
  {
    switch (nOpt)
    {
      case 'H':
      {
        if (nullptr != optarg)
        {
          sHTMLFile = optarg;
          verifyFileExtension(sHTMLFile, "html");
          checkOutputFile(sHTMLFile);
        }
        else
        {
          sHTMLFile = sDefaultHTMLFile;
        }
      }
      break;

      case 'f':
      {
        if (nullptr != optarg)
        {
          sFrameFile = optarg;
          verifyFileExtension(sFrameFile, "html");
          checkOutputFile(sFrameFile);
        }
        else
        {
          std::cerr << "ERROR: Must specify a HTML frame file name." << std::endl;
          usage(sProgramName, sDefaultHTMLFile, -1);
        }
      }
      break;

      case 't':
        if (nullptr != optarg)
        {
          sOutputFile = optarg;
          verifyFileExtension(sOutputFile, "txt");
          checkOutputFile(sOutputFile);
        }
        else
        {
          std::cerr << "ERROR: Must specify an output file name." << std::endl;
          usage(sProgramName, sDefaultHTMLFile, -1);
        }
        break;

      case 'b':
        bBuildDateAndTime = true;
        break;

      case 'v':
        bPrintVersion = true;
        break;

      case 'h':
        usage(sProgramName, sDefaultHTMLFile, 0);
        break;

      default:
        std::cerr << "ERROR: Unrecognized option or unrecognized/missing "
                     "option argument."
                  << std::endl;
        usage(sProgramName, sDefaultHTMLFile, -1);
        break;
    }
  }

  if ((true == bBuildDateAndTime) && (false == bPrintVersion))
  {
    buildDateAndTime(sProgramName);
    exit(0);
  }
  else if ((false == bBuildDateAndTime) && (true == bPrintVersion))
  {
    std::cout << sProgramName << " version " << CLInfo::getInstance().clinfoVersion() << '\n';
    exit(0);
  }
  else if ((true == bBuildDateAndTime) && (true == bPrintVersion))
  {
    buildDateAndTime(sProgramName);
    std::cout << sProgramName << " version " << CLInfo::getInstance().clinfoVersion() << '\n';
    exit(0);
  }
}

/**
 * @brief clears the screen
 */
void clearScreen() noexcept
{
  if (nullptr == cur_term)
  {
    int nResult = 0;
    setupterm(nullptr, STDOUT_FILENO, &nResult);
    if (nResult <= 0)
    {
      return;
    }
  }
  putp(tigetstr(const_cast<char*>("clear")));
}

/**
 * @brief outputs the OpenCL information
 * @param sOutputFile - text output file
 * @param sHTMLFile - html output file
 * @param sFrameFile - html frames output file
 */
void getCLInfo(
  const std::string& sOutputFile,
  const std::string& sHTMLFile,
  const std::string& sFrameFile)
{
  std::stringstream tText;
  std::stringstream tHtml;

  tText << CLInfo::getInstance() << '\n';

  if ((true == sOutputFile.empty()) && (true == sHTMLFile.empty()) && (true == sFrameFile.empty()))
  {
    if (isatty(fileno(stdout)))
    {
      clearScreen();
    }
    std::cout << tText.str();
  }
  else
  {
    if (false == sOutputFile.empty())
    {
      ScopedOutputFile(sOutputFile) << tText.str();
    }

    if (false == sHTMLFile.empty())
    {
      CLInfo::getInstance().getHTMLOutput(tHtml, "OpenCL Information", "opencl_platform_info");
      ScopedOutputFile tHTML(sHTMLFile);
      tHTML << tHtml.str() << '\n';
    }

    if (false == sFrameFile.empty())
    {
      if (true == tHtml.str().empty())
      {
        CLInfo::getInstance().getHTMLOutput(tHtml, "OpenCL Information", "opencl_platform_info");
      }

      CLInfo::createHTMLFrameFile(sFrameFile, tHtml, tText);
    }
  }
}

void getOpenCLInfo(int argc, char* argv[])
{
  // Compilation is only valid on a 64-bit platform.
  static_assert(sizeof(void*) == 8, "Must compile on a 64-bit platform");

  // Want to ensure this application is not executed with elevated privileges.
  CheckIDInfo tCheckIDInfo(argc, argv);

  try
  {
    std::string sOutputFile;
    std::string sHTMLFile;
    std::string sFrameFile;

    if (argc > 1)
    {
      const std::string sArgv0(argv[0]);
      const std::string sProgramName(boost::filesystem::path(sArgv0).filename().string());
      processCommandLineOptions(sProgramName, sOutputFile, sHTMLFile, sFrameFile, argc, argv);
    }

    getCLInfo(sOutputFile, sHTMLFile, sFrameFile);
  }
  catch (const std::runtime_error& e)
  {
    std::cout << "[" << e.what() << "]." << std::endl;
    exit(-1);
  }
  catch (...)
  {
    std::cout << "ERROR: Function = [" << __FUNCTION__ << "] File = [" << __FILE__ << "] Line = ["
              << __LINE__ << "] Unknown exception.\n";
    exit(-1);
  }
}
/*!
 * \mainpage UML Sequence Diagram
 * \htmlonly <center><div><a href="https://www.google.com/search?q=opencl"
 * onclick="window.open('https://www.youtube.com/results?search_query=opencl');"
 * target="_blank"><img src="opencl_dial_logo.png"/></a></div></center>
 * \endhtmlonly \htmlonly <center><div><a href="http://plantuml.com"
 * target="_blank"><img src="plantUML_logo.png"/></a></div></center>
 * \endhtmlonly \uml_seq_diagram
 */

/**
 * @brief main function
 * @param argc - the number of command line options
 * @param argv[] - the command line options
 * @return - 0 upon success, -1 otherwise
 */
int main(int argc, char* argv[])
{
  getOpenCLInfo(argc, argv);
  return (0);
}

