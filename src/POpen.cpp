
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "POpen.h"

#include <iostream>
#include <sstream>

#include "ScopedErrno.h"
#include "StrError.h"

POpen::POpen(const std::string& sCommand, const POpen::POPEN_MODE eMode) :
  m_pStream(nullptr), m_sCommand(sCommand), m_eMode(eMode), m_bPipeClosed(false)
{
  ScopedErrno tErrno;
  m_pStream = popen(m_sCommand.c_str(), (POpen::READ == m_eMode) ? "r" : "w");

  if (nullptr == m_pStream)
  {
    if (0 == errno)  // Memory allocation failed (errno **is not** set).
    {
      throw std::runtime_error("ERROR: Memory allocation for popen() failed.");
    }
    else  // Underlying fork(2) or pipe(2) failed (errno **is** set).
    {
      std::stringstream tError;
      tError << "ERROR: Could not open pipe due to ["
             << StrError::getInstance().getErrorMessage(errno) << "].";
      throw std::runtime_error(tError.str());
    }
  }
}

POpen::~POpen()
{
  try
  {
    close();
  }
  catch (...)
  {
  }
}

void POpen::close()
{
  if (false == m_bPipeClosed)
  {
    ScopedErrno tErrno;
    if (0 != pclose(m_pStream))
    {
      std::stringstream tError;
      tError << "ERROR: Could not close pipe due to ["
             << StrError::getInstance().getErrorMessage(errno) << "].";
      throw std::runtime_error(tError.str());
    }
    m_bPipeClosed = true;
  }
}

