
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ScopedErrno.h"

ErrorBuffer::ErrorBuffer()
{
  clear();
}

ErrorBuffer::~ErrorBuffer()
{
}

ErrorBuffer& ErrorBuffer::operator=(const char* s)
{
  clear();
  size_t i = 0;
  for (; i < ErrorBuffer::m_nErrorBufferSize && s[i] != '\0'; ++i)
  {
    m_sErrorBuffer[i] = s[i];
  }

  return (*this);
}

void ErrorBuffer::clear()
{
  for (size_t i = 0; i < ErrorBuffer::m_nErrorBufferSize; ++i)
  {
    m_sErrorBuffer[i] = '\0';
  }
}

size_t ErrorBuffer::size() const
{
  size_t i = 0;
  for (; i < ErrorBuffer::m_nErrorBufferSize; ++i)
  {
    if ('\0' == m_sErrorBuffer[i])
    {
      return (i);
    }
  }

  if (ErrorBuffer::m_nErrorBufferSize > 0)
  {
    return ((i - 1));
  }

  return (0);
}

ScopedErrno::ScopedErrno() : m_nSavedErrno(errno)
{
  errno = 0;
}

ScopedErrno::~ScopedErrno()
{
  errno = m_nSavedErrno;
}

const char* ScopedErrno::getErrorString(ErrorBuffer& sErrorMessageBuffer)
{
  sErrorMessageBuffer.clear();
  ScopedErrno tErrno;
  sErrorMessageBuffer = strerror_r(
    tErrno.getSavedErrno(),
    sErrorMessageBuffer,
    sErrorMessageBuffer.getErrorBufferSize());
  switch (errno)
  {
    case 0:  // No errors.
      break;

    // The value of tErrno.getSavedErrno() is not a valid error number.
    case EINVAL:
    {
      const char sError[] = "ERROR: EINVAL strerror_r() received an invalid error number.";
      ScopedErrno::strcpy_r(sErrorMessageBuffer, sError, sizeof(sError));
    }
    break;

    // Insufficient storage was supplied to contain the error description
    // string.
    case ERANGE:
    {
      const char sError[] = "ERROR: ERANGE strerror_r() received insufficient storage for the "
                            "error message.";
      ScopedErrno::strcpy_r(sErrorMessageBuffer, sError, sizeof(sError));
    }
    break;

    default:  // Unknown strerror_r() error.
    {
      const char sError[] = "ERROR: Unknown strerror_r() error.";
      ScopedErrno::strcpy_r(sErrorMessageBuffer, sError, sizeof(sError));
    }
    break;
  }

  return (sErrorMessageBuffer);
}

void ScopedErrno::strcpy_r(ErrorBuffer& pDest, const char* pSrc, const size_t nSrcSize)
{
  size_t nMinSize = pDest.getErrorBufferSize();
  if (nSrcSize < nMinSize)
  {
    nMinSize = nSrcSize;
  }

  for (size_t i = 0; i < nMinSize; ++i)
  {
    pDest[i] = *pSrc++;
  }

  if (0 != nMinSize)
  {
    pDest[nMinSize] = '\0';
  }
}

