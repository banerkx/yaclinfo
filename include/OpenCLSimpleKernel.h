
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Encapsulates OpenCL setup.
 */

#pragma once

#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#include <iostream>
#include <string>
#include <vector>

#include "OpenCLErrors.h"
#include "cl_device_type.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

class OpenCLSimpleKernel
{
  public:

    OpenCLSimpleKernel(
      cl_platform_id tCLPlatformID,
      const size_t   nPlatformNumber,
      cl_device_id   tCLDeviceID,
      const size_t   nDeviceNumber,
      unsigned long  nCLDeviceType = CL_DEVICE_TYPE_DEFAULT);

    virtual ~OpenCLSimpleKernel();

    inline bool hasErrors() const
    {
      return (false == m_tErrors.empty());
    }

    inline const std::vector<std::string>& errors() const
    {
      return (m_tErrors);
    }

    inline const char* kernel()
    {
      return (m_sKernel);
    }

    inline std::string deviceType() const
    {
      return (cl_device_type_ToString(m_nCLDeviceType));
    }

    inline size_t dataSize() const
    {
      return (m_nDataSize);
    }

    inline cl_ulong elapsedTime() const
    {
      return (m_nElapsedTime);
    }

  private:

    cl_platform_id m_tCLPlatformID;
    const size_t   m_nPlatformNumber;
    cl_device_id   m_tCLDeviceID;
    const size_t   m_nDeviceNumber;
    size_t         m_nDataSize;

    cl_context               m_tCLContext;
    cl_context_properties    m_tCLContextProperties[3];
    cl_kernel                m_tCLKernel;
    cl_command_queue         m_tCLCommandQueue;
    cl_program               m_tCLProgram;
    std::vector<std::string> m_tErrors;
    unsigned long            m_nCLDeviceType;
    std::vector<cl_mem>      m_tCLBuffers;
    const char*              m_sKernel;
    cl_event                 m_tCLEvent;
    cl_ulong                 m_nElapsedTime;  // In nano-seconds

    void setCLCreateContext();
    void createOpenCLProgramFromKernel();
    void createOpenCLCommandQueue();
    void buildProgram();
    void setKernel();
    void vectorSquareInPlace();

    cl_mem createReadOnlyFloatBuffer(size_t nSize);
    cl_mem createWriteOnlyFloatBuffer(size_t nSize);
    cl_mem createReadWriteFloatBuffer(size_t nSize);

    void loadInputBuffer(cl_mem pInputBuffer, const size_t nDataSize, const float* pInputData);

    void setOutputBuffer(cl_mem pOutputBuffer, const size_t nDataSize, float* pOutputData);

    void setKernelArgument(unsigned short nArgumentIndex, cl_mem tDataBuffer);

    void enqueueKernelForExecution(const size_t nDataSize);

    void finish();

    void recordError(const cl_int nCLError, const std::string& sErrorText);

    void setDeviceType();

    // Intentionally deleted.
    OpenCLSimpleKernel()                              = delete;
    OpenCLSimpleKernel(const OpenCLSimpleKernel& rhs) = delete;
    OpenCLSimpleKernel(OpenCLSimpleKernel&& rhs)      = delete;
    OpenCLSimpleKernel&       operator=(const OpenCLSimpleKernel& rhs) = delete;
    OpenCLSimpleKernel&       operator=(OpenCLSimpleKernel&& rhs) = delete;
    OpenCLSimpleKernel*       operator&()                         = delete;
    const OpenCLSimpleKernel* operator&() const                   = delete;
};

