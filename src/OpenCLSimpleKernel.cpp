
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OpenCLSimpleKernel.h"

#include <algorithm>
#include <iterator>
#include <sstream>

#include "DynamicBuffer.h"

/**
 * Default constructor.
 */
OpenCLSimpleKernel::OpenCLSimpleKernel(
  cl_platform_id tCLPlatformID,
  const size_t   nPlatformNumber,
  cl_device_id   tCLDeviceID,
  const size_t   nDeviceNumber,
  unsigned long  nCLDeviceType) :
  m_tCLPlatformID(tCLPlatformID),
  m_nPlatformNumber(nPlatformNumber),
  m_tCLDeviceID(tCLDeviceID),
  m_nDeviceNumber(nDeviceNumber),
  m_nDataSize(10000),
  m_tCLContext(nullptr),
  m_tCLContextProperties(),
  m_tCLKernel(nullptr),
  m_tCLCommandQueue(nullptr),
  m_tCLProgram(nullptr),
  m_tErrors(),
  m_nCLDeviceType(nCLDeviceType),
  m_tCLBuffers(),
  m_sKernel(nullptr),
  m_tCLEvent(nullptr),
  m_nElapsedTime(0)
{
  setDeviceType();
  vectorSquareInPlace();
}

/**
 * Virtual destructor.
 */
OpenCLSimpleKernel::~OpenCLSimpleKernel()
{
  (void) clReleaseProgram(m_tCLProgram);
  m_tCLProgram = nullptr;

  (void) clReleaseCommandQueue(m_tCLCommandQueue);
  m_tCLCommandQueue = nullptr;

  (void) clReleaseContext(m_tCLContext);
  m_tCLContext = nullptr;

  m_tErrors.clear();

  std::for_each(m_tCLBuffers.begin(), m_tCLBuffers.end(), clReleaseMemObject);
  m_tCLBuffers.clear();

  (void) clReleaseKernel(m_tCLKernel);
  m_tCLKernel = nullptr;

  (void) clReleaseEvent(m_tCLEvent);
  m_tCLEvent = nullptr;
}

/**
 * \f[
 *     Performs\, the\, computation:\, v_{i} \leftarrow v_{i}^2
 * \f]
 */
void OpenCLSimpleKernel::vectorSquareInPlace()
{
  m_sKernel = "__kernel void vector_square_in_place(__global float* pInput)\n"
              "{\n"
              "  size_t nID   = get_global_id(0);\n"
              "  pInput[nID] *= pInput[nID];\n"
              "}\n";

  DynamicBuffer<float> tInputData(m_nDataSize);

  for (size_t i = 0; i < m_nDataSize; ++i)
  {
    if (1 == (i % 2))
    {
      tInputData[i] = -1.0;
    }
    else
    {
      tInputData[i] = 1.0;
    }
  }

  setCLCreateContext();
  if (false == m_tErrors.empty())
  {
    return;
  }

  createOpenCLCommandQueue();
  if (false == m_tErrors.empty())
  {
    return;
  }

  createOpenCLProgramFromKernel();
  if (false == m_tErrors.empty())
  {
    return;
  }

  buildProgram();
  if (false == m_tErrors.empty())
  {
    return;
  }

  // Creating a buffer for the input.
  cl_mem tInput = createReadWriteFloatBuffer(m_nDataSize);
  if (false == m_tErrors.empty())
  {
    return;
  }

  // Loading data into the input buffer.
  loadInputBuffer(tInput, m_nDataSize, tInputData);
  if (false == m_tErrors.empty())
  {
    return;
  }

  setKernel();
  if (false == m_tErrors.empty())
  {
    return;
  }

  // Setting the argument list for the kernel command.
  setKernelArgument(0, tInput);
  if (false == m_tErrors.empty())
  {
    return;
  }

  // Enqueueing the kernel command for execution.
  enqueueKernelForExecution(m_nDataSize);
  if (false == m_tErrors.empty())
  {
    return;
  }

  finish();
  if (false == m_tErrors.empty())
  {
    return;
  }

  setOutputBuffer(tInput, m_nDataSize, tInputData);
  if (false == m_tErrors.empty())
  {
    return;
  }

  // Checking to see that each element of tInputData is 1.0.
  for (size_t i = 0; i < m_nDataSize; ++i)
  {
    if (1.0 != tInputData[i])
    {
      std::stringstream tErrorMessage;
      tErrorMessage << "ERROR: Platform [" << m_nPlatformNumber << "] Device [" << m_nDeviceNumber
                    << "] At index [" << i << "], the actual value [" << tInputData[i]
                    << "] does not equal the expected value [1.0].";
      m_tErrors.push_back(tErrorMessage.str());
      break;
    }
  }
}

/**
 * Create a context with the device.
 */
void OpenCLSimpleKernel::setCLCreateContext()
{
  // context properties list - must be terminated with 0
  m_tCLContextProperties[0] = CL_CONTEXT_PLATFORM;
  m_tCLContextProperties[1] = (cl_context_properties) m_tCLPlatformID;
  m_tCLContextProperties[2] = 0;

  cl_int nCLError = 0;
  m_tCLContext =
    clCreateContext(m_tCLContextProperties, 1, &m_tCLDeviceID, nullptr, nullptr, &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateContext");
  }
}

/**
 * Creates a program from the kernel source code.
 */
void OpenCLSimpleKernel::createOpenCLProgramFromKernel()
{
  cl_int nCLError = 0;
  m_tCLProgram    = clCreateProgramWithSource(m_tCLContext, 1, &m_sKernel, nullptr, &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateProgramWithSource");
  }
}

/**
 * Retrieves the specified kernel.
 */
void OpenCLSimpleKernel::setKernel()
{
  cl_int nCLError = 0;
  m_tCLKernel     = clCreateKernel(m_tCLProgram, "vector_square_in_place", &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateKernel");
  }
}

/**
 * Compiles the program.
 */
void OpenCLSimpleKernel::buildProgram()
{
  cl_int nCLError = clBuildProgram(m_tCLProgram, 0, nullptr, nullptr, nullptr, nullptr);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clBuildProgram");
  }
}

/**
 * Creates the command queue using the context and device.
 */
void OpenCLSimpleKernel::createOpenCLCommandQueue()
{
  cl_int nCLError   = 0;
  m_tCLCommandQueue = clCreateCommandQueue(
    m_tCLContext,
    m_tCLDeviceID,
    CL_QUEUE_PROFILING_ENABLE,  // Want to compute the kernel's elapsed time.
    &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateCommandQueue");
  }
}

/**
 * Creates a read-only buffer, of the specified size, for float.
 * @param nSize - the specified size
 * @return a pointer to the buffer
 */
cl_mem OpenCLSimpleKernel::createReadOnlyFloatBuffer(size_t nSize)
{
  cl_int nCLError = 0;
  cl_mem pBuffer =
    clCreateBuffer(m_tCLContext, CL_MEM_READ_ONLY, sizeof(float) * nSize, nullptr, &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateBuffer (read-only)");
    return (nullptr);
  }

  m_tCLBuffers.push_back(pBuffer);
  return (pBuffer);
}

/**
 * Creates a write-only buffer, of the specified size, for float.
 * @param nSize - the specified size
 * @return a pointer to the buffer
 */
cl_mem OpenCLSimpleKernel::createWriteOnlyFloatBuffer(size_t nSize)
{
  cl_int nCLError = 0;
  cl_mem pBuffer =
    clCreateBuffer(m_tCLContext, CL_MEM_WRITE_ONLY, sizeof(float) * nSize, nullptr, &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateBuffer (write-only)");
    return (nullptr);
  }

  m_tCLBuffers.push_back(pBuffer);
  return (pBuffer);
}

/**
 * Creates a read/write buffer, of the specified size, for float.
 * @param nSize - the specified size
 * @return a pointer to the buffer
 */
cl_mem OpenCLSimpleKernel::createReadWriteFloatBuffer(size_t nSize)
{
  cl_int nCLError = 0;
  cl_mem pBuffer =
    clCreateBuffer(m_tCLContext, CL_MEM_READ_WRITE, sizeof(float) * nSize, nullptr, &nCLError);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clCreateBuffer (read/write)");
    return (nullptr);
  }

  m_tCLBuffers.push_back(pBuffer);
  return (pBuffer);
}

/**
 * Loads the input buffer.
 * @param pInputBuffer - the input OpenCL buffer
 * @param nDataSize - the data size
 * @param pInputData - the input data
 */
void OpenCLSimpleKernel::loadInputBuffer(
  cl_mem       pInputBuffer,
  const size_t nDataSize,
  const float* pInputData)
{
  cl_int nCLError = clEnqueueWriteBuffer(
    m_tCLCommandQueue,
    pInputBuffer,
    CL_TRUE,
    0,
    sizeof(float) * nDataSize,
    pInputData,
    0,
    nullptr,
    nullptr);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clEnqueueWriteBuffer");
  }
}

/**
 * Sets the output buffer.
 * @param pOutputBuffer - the output OpenCL buffer
 * @param nDataSize - the data size
 * @param pOutputData - the output data
 */
void OpenCLSimpleKernel::setOutputBuffer(
  cl_mem       pOutputBuffer,
  const size_t nDataSize,
  float*       pOutputData)
{
  cl_int nCLError = clEnqueueReadBuffer(
    m_tCLCommandQueue,
    pOutputBuffer,
    CL_TRUE,
    0,
    sizeof(float) * nDataSize,
    pOutputData,
    0,
    nullptr,
    nullptr);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clEnqueueReadBuffer");
  }
}

/**
 * Sets an OpenCL kernel argument.
 * @param nArgumentIndex - the argument index
 * @param tDataBuffer - the data buffer
 */
void OpenCLSimpleKernel::setKernelArgument(unsigned short nArgumentIndex, cl_mem tDataBuffer)
{
  cl_int nCLError = clSetKernelArg(m_tCLKernel, nArgumentIndex, sizeof(cl_mem), &tDataBuffer);

  if (CL_SUCCESS != nCLError)
  {
    std::stringstream tErrorMessage;
    tErrorMessage << "ERROR: clSetKernelArg, argument index [" << nArgumentIndex << "]";
    recordError(nCLError, tErrorMessage.str());
  }
}

/**
 * Saves the error to m_tErrors.
 * @param nCLError - the OpenCL error code
 * @param sErrorText - the error text
 */
void OpenCLSimpleKernel::recordError(const cl_int nCLError, const std::string& sErrorText)
{
  std::stringstream tErrorMessage;
  tErrorMessage << "ERROR: Platform [" << m_nPlatformNumber << "] Device [" << m_nDeviceNumber
                << "] " << sErrorText << " ==> " << OpenCLErrors::getInstance()[nCLError];
  m_tErrors.push_back(tErrorMessage.str());
}

/**
 * Enqueues the input kernel for execution.
 * @param nDataSize - the data size
 */
void OpenCLSimpleKernel::enqueueKernelForExecution(const size_t nDataSize)
{
  cl_int nCLError = clEnqueueNDRangeKernel(
    m_tCLCommandQueue,
    m_tCLKernel,
    1,
    nullptr,
    &nDataSize,
    nullptr,
    0,
    nullptr,
    &m_tCLEvent);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clEnqueueNDRangeKernel");
  }
}

/**
 * Finishes the command queue.
 */
void OpenCLSimpleKernel::finish()
{
  cl_int nCLError = clWaitForEvents(1, &m_tCLEvent);
  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clWaitForEvents");
    return;
  }

  // NOTE: Blocks until all previously queued OpenCL commands
  //       in m_tCLCommandQueue are issued to the associated
  //       device and have completed.
  nCLError = clFinish(m_tCLCommandQueue);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clFinish");
    return;
  }

  cl_ulong nStartTime = 0;
  cl_ulong nEndTime   = 0;

  nCLError = clGetEventProfilingInfo(
    m_tCLEvent,
    CL_PROFILING_COMMAND_START,
    sizeof(nStartTime),
    &nStartTime,
    nullptr);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clGetEventProfilingInfo CL_PROFILING_COMMAND_START");
    return;
  }

  nCLError = clGetEventProfilingInfo(
    m_tCLEvent,
    CL_PROFILING_COMMAND_END,
    sizeof(nEndTime),
    &nEndTime,
    nullptr);

  if (CL_SUCCESS != nCLError)
  {
    recordError(nCLError, "clGetEventProfilingInfo CL_PROFILING_COMMAND_END");
    return;
  }

  m_nElapsedTime = nEndTime - nStartTime;
}

void OpenCLSimpleKernel::setDeviceType()
{
  // We could have a bit-wise OR of 2 or more device type enum values. In such a
  // case, we try to extract the actual device type.
  if ("CL_INVALID_VALUE" == cl_device_type_ToString(m_nCLDeviceType))
  {
    bool bFoundDeviceType = false;
    for (auto i = OPEN_cl_device_type::BEGIN; i != OPEN_cl_device_type::END; ++i)
    {
      switch (scopedEnumToCLEnum(i) & m_nCLDeviceType)
      {
        // We are looking for the specific device type. Therefore, we ignore
        // these 2 device types.
        case CL_DEVICE_TYPE_DEFAULT:
        case CL_DEVICE_TYPE_ALL:
          break;

        case CL_DEVICE_TYPE_CPU:
        case CL_DEVICE_TYPE_GPU:
        case CL_DEVICE_TYPE_ACCELERATOR:
        case CL_DEVICE_TYPE_CUSTOM:
          m_nCLDeviceType  = scopedEnumToCLEnum(i);
          bFoundDeviceType = true;
          break;

        // Something has gone wrong. Therefore, we will use
        // CL_DEVICE_TYPE_DEFAULT.
        default:
          m_nCLDeviceType  = CL_DEVICE_TYPE_DEFAULT;
          bFoundDeviceType = true;
          break;
      };
      if (true == bFoundDeviceType)
      {
        break;
      }
    }
  }
}

