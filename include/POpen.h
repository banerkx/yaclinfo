
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdio>
#include <stdexcept>
#include <string>

class POpen
{
  public:

    typedef enum
    {
      READ,
      WRITE
    } POPEN_MODE;

    POpen(const std::string& sCommand, const POpen::POPEN_MODE eMode);

    ~POpen();

    inline operator FILE*()
    {
      return (m_pStream);
    }

    inline FILE* file()
    {
      return (m_pStream);
    }

    inline const std::string& command() const
    {
      return (m_sCommand);
    }

    inline bool pipeIsClosed() const
    {
      return (m_bPipeClosed);
    }

    void close();

  private:

    std::FILE*        m_pStream;
    const std::string m_sCommand;
    POpen::POPEN_MODE m_eMode;
    bool              m_bPipeClosed;

    // Intentionally deleted.
    POpen()                 = delete;
    POpen(const POpen& rhs) = delete;
    POpen(POpen&& rhs)      = delete;
    POpen&       operator=(const POpen& rhs) = delete;
    POpen&       operator=(POpen&& rhs) = delete;
    POpen*       operator&()            = delete;
    const POpen* operator&() const      = delete;
};

