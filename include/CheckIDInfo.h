
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <unistd.h>

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

class CheckIDInfo
{
  public:

    CheckIDInfo(const int argc, char** const argv) : m_nArgc(argc), m_pArgv(argv)
    {
      checkIDs();
    }

    // For seeing if ASLR is working.
    static void* getExecutableAddress()
    {
      size_t* pAddress = static_cast<size_t*>(__builtin_return_address(0));
      pAddress -= 0x5;
      return (pAddress);
    }

  private:

    const int    m_nArgc;
    char** const m_pArgv;

    // NOTE: The effective user/group id for root is 0.
    constexpr static uid_t m_nRootID = 0;

    constexpr static uid_t m_nExpectedMinimumUID = 500;

    constexpr static size_t m_nUserNameBufferSize = 512;
    static char             m_sUserName[CheckIDInfo::m_nUserNameBufferSize];

    void checkIDs()
    {
      CheckIDInfo::checkMinimumExpectedUID();
      CheckIDInfo::checkForSUDOEnvVars();
      checkEffectiveUID();
      checkEffectiveGID();
    }

    static void setUserName()
    {
      if (0 != getlogin_r(CheckIDInfo::m_sUserName, m_nUserNameBufferSize))
      {
        static const std::string sUnknown("UNKNOWN");
        std::memcpy(CheckIDInfo::m_sUserName, sUnknown.c_str(), sUnknown.size());
      }
    }

    static std::string getDateTime()
    {
      static const size_t nBufferSize = 128;
      char                sTimeBuffer[nBufferSize];

      time_t nCurrentTime = std::time(nullptr);
      if (-1l == static_cast<long>(nCurrentTime))
      {
        return ("UNKNOWN DATE/TIME");
      }

      struct tm* tTimeInfo = std::localtime(&nCurrentTime);

      // "%c" ==> The preferred date and time  representation  for  the  current
      // locale (from man 3 strftime).
      static const std::string sTimeFormat("%c");
      if (0 == strftime(sTimeBuffer, nBufferSize, sTimeFormat.c_str(), tTimeInfo))
      {
        return ("UNKNOWN DATE/TIME");
      }

      return (sTimeBuffer);
    }

    static void checkMinimumExpectedUID()
    {
      static const uid_t nUID = getuid();
      if (nUID < CheckIDInfo::m_nExpectedMinimumUID)
      {
        CheckIDInfo::setUserName();
        std::stringstream tMessage;
        tMessage << "The id for user [" << CheckIDInfo::m_sUserName << "], [" << nUID
                 << "], is less than the expected minimum id of ["
                 << CheckIDInfo::m_nExpectedMinimumUID << "]." << std::endl;
        CheckIDInfo::printWarningMessage(tMessage.str());
      }
    }

    static bool effectiveUIDIsNotRoot()
    {
      return (CheckIDInfo::m_nRootID != geteuid());
    }

    static bool effectiveGIDIsNotRoot()
    {
      return (CheckIDInfo::m_nRootID != getegid());
    }

    void checkEffectiveUID()
    {
      if (false == CheckIDInfo::effectiveUIDIsNotRoot())
      {
        CheckIDInfo::setUserName();
        CheckIDInfo::printErrorMessage("The effective user id is for the root user! Aborting!\n");
        this->abort();
      }
    }

    void checkEffectiveGID()
    {
      if (false == CheckIDInfo::effectiveGIDIsNotRoot())
      {
        CheckIDInfo::setUserName();
        CheckIDInfo::printErrorMessage("The effective group id is for the root group! Aborting!\n");
        this->abort();
      }
    }

    std::string getCommandLine()
    {
      std::vector<std::string> tArgs(m_pArgv, m_pArgv + m_nArgc);
      std::stringstream        tCommandLine;
      std::copy(
        tArgs.begin(),
        tArgs.end() - 1,
        std::ostream_iterator<std::string>(tCommandLine, " "));
      tCommandLine << tArgs.back();
      return (tCommandLine.str());
    }

    void abort()
    {
      // If you are looking at this from within a core file, note the values
      // of the following variables.
      // NOTE: The volatile specifier is used to prevent the compiler from
      //       optimizing out the relevant variable.
      volatile const uid_t nUID          = getuid();
      volatile const uid_t nEffectiveUID = geteuid();
      volatile const gid_t nGID          = getgid();
      volatile const gid_t nEffectiveGID = getegid();
      volatile const pid_t nPID          = getpid();
      volatile std::string sUserName(m_sUserName);
      volatile std::string sCurrentTime(CheckIDInfo::getDateTime());
      volatile std::string sCommandLine(this->getCommandLine());

      // The following method call does nothing, it is simply used to prevent
      // "unused variable" compiler warnings.
      CheckIDInfo::noUnusedVariableWarnings(
        nUID,
        nEffectiveUID,
        nGID,
        nEffectiveGID,
        nPID,
        sUserName,
        sCurrentTime,
        sCommandLine);

      std::abort();
    }

    static void noUnusedVariableWarnings(
      const uid_t,
      const uid_t,
      const gid_t,
      const gid_t,
      const pid_t,
      const volatile std::string&,
      const volatile std::string&,
      const volatile std::string&)
    {
    }

    static void checkForSUDOEnvVars()
    {
      std::vector<std::string> tSudoEnvVars;
      tSudoEnvVars.push_back("SUDO_COMMAND");
      tSudoEnvVars.push_back("SUDO_USER");
      tSudoEnvVars.push_back("SUDO_UID");
      tSudoEnvVars.push_back("SUDO_GID");

      for (size_t i = 0; i < tSudoEnvVars.size(); ++i)
      {
        if (nullptr != getenv(tSudoEnvVars[i].c_str()))
        {
          std::stringstream sMessage;
          sMessage << "The sudo environment [" << tSudoEnvVars[i]
                   << "] is defined. This program should not be executed under sudo!" << std::endl;
          CheckIDInfo::printWarningMessage(sMessage.str());
        }
      }
    }

    static void printErrorMessage(const std::string& sMessage)
    {
      std::cout << "ERROR: " << sMessage;
    }

    static void printWarningMessage(const std::string& sMessage)
    {
      std::cout << "WARNING: " << sMessage;
    }

    // Intentionally deleted.
    CheckIDInfo()                       = delete;
    CheckIDInfo(const CheckIDInfo& rhs) = delete;
    CheckIDInfo(CheckIDInfo&& rhs)      = delete;
    CheckIDInfo&       operator=(const CheckIDInfo& rhs) = delete;
    CheckIDInfo&       operator=(CheckIDInfo&& rhs) = delete;
    CheckIDInfo*       operator&()                  = delete;
    const CheckIDInfo* operator&() const            = delete;
};

char CheckIDInfo::m_sUserName[CheckIDInfo::m_nUserNameBufferSize];

