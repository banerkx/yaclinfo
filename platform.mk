
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PLATFORM_MK_INCLUDE_GUARD # {
PLATFORM_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including vars_common.mk.                  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# Functions that get the version and path of the input static library using    #
# the pkg-config command.                                                      #
#                                                                              #
# $(1) - static library                                                        #
################################################################################
PKG_CONFIG_COMMAND=pkg-config
PKG_CONFIG=$(shell command -v $(PKG_CONFIG_COMMAND))
PKG_CONFIG_VERSION=$(call get_stdout_version,$(PKG_CONFIG_COMMAND),--version)
get_static_lib_version=$(if $(1),$(if $(PKG_CONFIG),$(shell $(PKG_CONFIG) $(subst lib,,$(firstword $(subst ., ,$(notdir $(1))))) --modversion 2> /dev/null || echo UNKNOWN),UNKNOWN))
get_static_lib_path=$(if $(1),$(eval path=$(if $(PKG_CONFIG),$(wildcard $(strip $(subst -L,,$(shell $(PKG_CONFIG) --libs-only-L --keep-system-libs $(1) 2> /dev/null)))/lib$(1).a),UNKNOWN))$(if $(path),$(path),UNKNOWN))

################################################################################
# Uses pkg-config to get the -L directories for the specified static           #
# libraries.                                                                   #
#                                                                              #
# $(1) - list of static libraries                                              #
################################################################################
get_static_lib_dirs=$(if $(1),$(subst -L,-L ,$(strip $(shell $(PKG_CONFIG) --libs-only-L $(1)))))

.PHONY: platform_info

################################################################################
# BEGIN beautifiers section.                                                   #
################################################################################

ASTYLE_VERSION=$(call get_stdout_version_index,astyle,--version,4)
BEAUTYSH_VERSION=$(call get_stdout_version,beautysh,--version)
CHKTEX_VERSION=$(call get_stdout_version,chktex,--version,| head -1 | cut -d' ' -f2 | sed 's/[a-z]//g')
CLIPPY_DRIVER_VERSION=$(call get_stdout_version_index,clippy-driver,--version,2)
CMAKE_FORMAT_VERSION=$(call get_stdout_version,cmake-format,--version)
CMAKE_LINT_VERSION=$(call get_stdout_version,cmake-lint,--version)
CPPLINT_VERSION=$(call get_stdout_version,cpplint,--version,| sed -n '2{p;q}' | cut -d' ' -f2)
CSSLINT_VERSION=$(call get_stdout_version,csslint,--version,| sed 's/[a-z]//g')
CSS_BEAUTIFY_VERSION=$(call get_stdout_version,css-beautify,--version)
FRINK_VERSION=$(call get_stdout_version_index,frink,-V,2)
GOFMT_VERSION=$(call get_go_module_version,$(realpath $(shell command -v gofmt 2> /dev/null)))
GOLINT_VERSION=$(call get_go_module_version,$(realpath $(shell command -v golint 2> /dev/null)))
JSHINT_VERSION=$(call get_node_js_package_version,jshint)
JS_BEAUTIFY_VERSION=$(call get_stdout_version,js-beautify,--version)
LATEX_INDENT_VERSION=$(call get_stdout_version,latexindent,--version,| sed 's/\x2c.*$$//')
LUACHECK_VERSION=$(call get_stdout_version,luacheck,--version,| head -1 | sed 's/^.*: //')
MDL_VERSION=$(call get_stdout_version,mdl,--version)
MH_LINT_VERSION=$(call get_stdout_version_index,mh_lint,--version,2)
MH_STYLE_VERSION=$(call get_stdout_version_index,mh_style,--version,2)
NODE_JS_VERSION=$(call get_stdout_version,node,--version,| sed 's/[a-z]//g')
PRETTIER_AWK_VERSION=$(call get_node_js_package_version,prettier-plugin-awk)
PRETTIER_GLSL_VERSION=$(call get_node_js_package_version,prettier-plugin-glsl)
PRETTIER_GO_TEMPLATE_VERSION=$(call get_node_js_package_version,prettier-plugin-go-template)
PRETTIER_JAVA_VERSION=$(call get_node_js_package_version,prettier-plugin-java)
PRETTIER_PHP_VERSION=$(call get_node_js_package_version,@prettier/plugin-php)
PRETTIER_PRINT_VERSION=$(call get_ruby_gem_version,prettier_print)
PRETTIER_PROPERTIES_VERSION=$(call get_node_js_package_version,prettier-plugin-properties)
PRETTIER_RUBY_VERSION=$(call get_node_js_package_version,@prettier/plugin-ruby)
PRETTIER_SH_VERSION=$(call get_node_js_package_version,prettier-plugin-sh)
PRETTIER_SQL_VERSION=$(call get_node_js_package_version,prettier-plugin-sql)
PRETTIER_TOML_VERSION=$(call get_node_js_package_version,prettier-plugin-toml)
PRETTIER_VERSION=$(call get_stdout_version,prettier,--version)
PRETTIER_XML_VERSION=$(call get_node_js_package_version,@prettier/plugin-xml)
RUBOCOP_VERSION=$(call get_stdout_version_index,rubocop,--version,1)
RUFO_VERSION=$(call get_ruby_gem_version,rufo)
RUSTFMT_VERSION=$(call get_stdout_version,rustfmt,--version,| sed 's/\(^rustfmt \)\([0-9].*[0-9]\)\(.*$$\)/\2/')
SCALARIFORM_VERSION=$(call get_stdout_java_jar_file_version_index,$(call find_jar_location,scalariform.jar),--version,2)
SHELLCHECK_VERSION=$(call get_stdout_version,shellcheck,--version,| sed -n '2{p;q}' | sed 's/^.* //')
SHFMT_VERSION=$(call get_stdout_version,shfmt,--version)
SPONGE_VERSION=$(call get_rpm_package_version,moreutils)
SQLFLUFF_VERSION=$(call get_stdout_version_index,sqlfluff,--version,3)
SQL_FORMATTER_VERSION=$(call get_stdout_version_index,sql-formatter,--version,3)
SQL_LINT_VERSION=$(call get_stdout_version,sql-lint,--version)
STYLUA_VERSION=$(call get_stdout_version_index,stylua,--version,2)
TOMLL_VERSION=$(call get_rpm_package_version,golang-github-pelletier-toml)
XMLINDENT_VERSION=$(call get_stdout_version,xmllint,-v,| head -1 | gawk '{print $NF}')
XMLLINT_VERSION=$(call get_stderr_version,xmllint,-version,| head -1 | sed 's/^xmllint: using //;s/ version//')

.PHONY: beautifiers_info
beautifiers_info:
	@echo '@prettier/plugin-php version        = [$(PRETTIER_PHP_VERSION)]'
	@echo '@prettier/plugin-ruby version       = [$(PRETTIER_RUBY_VERSION)]'
	@echo '@prettier/plugin-xml version        = [$(PRETTIER_XML_VERSION)]'
	@echo 'astyle version                      = [$(ASTYLE_VERSION)]'
	@echo 'beautysh version                    = [$(BEAUTYSH_VERSION)]'
	@echo 'chktex version                      = [$(CHKTEX_VERSION)]'
	@echo 'clippy-driver version               = [$(CLIPPY_DRIVER_VERSION)]'
	@echo 'cmake-format version                = [$(CMAKE_FORMAT_VERSION)]'
	@echo 'cmake-lint version                  = [$(CMAKE_LINT_VERSION)]'
	@echo 'cpplint version                     = [$(CPPLINT_VERSION)]'
	@echo 'css-beautify version                = [$(CSS_BEAUTIFY_VERSION)]'
	@echo 'csslint version                     = [$(CSSLINT_VERSION)]'
	@echo 'frink version                       = [$(FRINK_VERSION)]'
	@echo 'gofmt version                       = [$(GOFMT_VERSION)]'
	@echo 'golint version                      = [$(GOLINT_VERSION)]'
	@echo 'js-beautify version                 = [$(JS_BEAUTIFY_VERSION)]'
	@echo 'jshint version                      = [$(JSHINT_VERSION)]'
	@echo 'latexindent version                 = [$(LATEX_INDENT_VERSION)]'
	@echo 'luacheck version                    = [$(LUACHECK_VERSION)]'
	@echo 'mdl version                         = [$(MDL_VERSION)]'
	@echo 'mh_lint version                     = [$(MH_LINT_VERSION)]'
	@echo 'mh_style version                    = [$(MH_STYLE_VERSION)]'
	@echo 'node.js version                     = [$(NODE_JS_VERSION)]'
	@echo 'prettier version                    = [$(PRETTIER_VERSION)]'
	@echo 'prettier-plugin-awk version         = [$(PRETTIER_AWK_VERSION)]'
	@echo 'prettier-plugin-glsl version        = [$(PRETTIER_GLSL_VERSION)]'
	@echo 'prettier-plugin-go-template version = [$(PRETTIER_GO_TEMPLATE_VERSION)]'
	@echo 'prettier-plugin-java version        = [$(PRETTIER_JAVA_VERSION)]'
	@echo 'prettier-plugin-properties version  = [$(PRETTIER_PROPERTIES_VERSION)]'
	@echo 'prettier-plugin-sh version          = [$(PRETTIER_SH_VERSION)]'
	@echo 'prettier-plugin-sql version         = [$(PRETTIER_SQL_VERSION)]'
	@echo 'prettier-plugin-toml version        = [$(PRETTIER_TOML_VERSION)]'
	@echo 'prettier_print version              = [$(PRETTIER_PRINT_VERSION)]'
	@echo 'rubocop version                     = [$(RUBOCOP_VERSION)]'
	@echo 'rufo version                        = [$(RUFO_VERSION)]'
	@echo 'rustfmt version                     = [$(RUSTFMT_VERSION)]'
	@echo 'scalariform version                 = [$(SCALARIFORM_VERSION)]'
	@echo 'shellcheck version                  = [$(SHELLCHECK_VERSION)]'
	@echo 'shfmt version                       = [$(SHFMT_VERSION)]'
	@echo 'sponge version                      = [$(SPONGE_VERSION)]'
	@echo 'sql-formatter version               = [$(SQL_FORMATTER_VERSION)]'
	@echo 'sql-lint version                    = [$(SQL_LINT_VERSION)]'
	@echo 'sqlfluff version                    = [$(SQLFLUFF_VERSION)]'
	@echo 'stylua version                      = [$(STYLUA_VERSION)]'
	@echo 'tomll version                       = [$(TOMLL_VERSION)]'
	@echo 'xmlindent version                   = [$(XMLINDENT_VERSION)]'
	@echo 'xmllint version                     = [$(XMLLINT_VERSION)]'

platform_info:: beautifiers_info

################################################################################
# END beautifiers section.                                                     #
################################################################################

################################################################################
# BEGIN binary utilities section.                                              #
################################################################################

AR_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,ar,--version,4))))
CODE_SPELL_VERSION=$(call get_stdout_version,codespell,--version)
CPP_FILTER_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,c++filt,--version,4))))
IDENTIFY_VERSION=$(call get_stdout_version_index,identify,--version,3)
OBJDUMP_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,objdump,--version,4))))
READELF_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,readelf,--version,4))))

.PHONY: binutils_info
binutils_info:
	@echo 'ar version        = [$(AR_VERSION)]'
	@echo 'codespell version = [$(CODE_SPELL_VERSION)]'
	@echo 'c++filt version   = [$(CPP_FILTER_VERSION)]'
	@echo 'identify version  = [$(IDENTIFY_VERSION)]'
	@echo 'objdump version   = [$(OBJDUMP_VERSION)]'
	@echo 'readelf version   = [$(READELF_VERSION)]'

platform_info:: binutils_info

################################################################################
# END binary utilities section.                                                #
################################################################################

################################################################################
# BEGIN build utilities section.                                               #
################################################################################

ACLOCAL_VERSION=$(call get_stdout_version_index,aclocal,--version,4)
AUTOCONF_VERSION=$(call get_stdout_version_index,autoconf,--version,4)
AUTOGEN_VERSION=$(call get_stdout_version_index,autogen,--version,4)
AUTOHEADER_VERSION=$(call get_stdout_version_index,autoheader,--version,4)
AUTOM4TE_VERSION=$(call get_stdout_version_index,autom4te,--version,4)
AUTOMAKE_VERSION=$(call get_stdout_version_index,automake,--version,4)
AUTORECONF_VERSION=$(call get_stdout_version_index,autoreconf,--version,4)
AUTOSCAN_VERSION=$(call get_stdout_version_index,autoscan,--version,4)
AUTOUPDATE_VERSION=$(call get_stdout_version_index,autoupdate,--version,4)
IFNAMES_VERSION=$(call get_stdout_version_index,ifnames,--version,4)
INSTALL_VERSION=$(call get_stdout_version_index,install,--version,4)
LIBTOOLIZE_VERSION=$(call get_stdout_version_index,libtoolize,--version,4)
LIBTOOL_VERSION=$(call get_stdout_version_index,libtool,--version,4)
M4_VERSION=$(call get_stdout_version_index,m4,--version,4)

.PHONY: build_utitiles_info
build_utitiles_info:
	@echo 'aclocal version    = [$(ACLOCAL_VERSION)]'
	@echo 'autoconf version   = [$(AUTOCONF_VERSION)]'
	@echo 'autogen version    = [$(AUTOGEN_VERSION)]'
	@echo 'autoheader version = [$(AUTOHEADER_VERSION)]'
	@echo 'autom4te version   = [$(AUTOM4TE_VERSION)]'
	@echo 'automake version   = [$(AUTOMAKE_VERSION)]'
	@echo 'autoreconf version = [$(AUTORECONF_VERSION)]'
	@echo 'autoscan version   = [$(AUTOSCAN_VERSION)]'
	@echo 'autoupdate version = [$(AUTOUPDATE_VERSION)]'
	@echo 'ifnames version    = [$(IFNAMES_VERSION)]'
	@echo 'install version    = [$(INSTALL_VERSION)]'
	@echo 'libtool version    = [$(LIBTOOL_VERSION)]'
	@echo 'libtoolize version = [$(LIBTOOLIZE_VERSION)]'
	@echo 'm4 version         = [$(M4_VERSION)]'
	@echo 'pkg-config version = [$(PKG_CONFIG_VERSION)]'

platform_info:: build_utitiles_info

################################################################################
# END build utilities section.                                                 #
################################################################################

################################################################################
# BEGIN cmake utilities section.                                               #
################################################################################
CCMAKE_VERSION=$(call get_stdout_version_index,ccmake,--version,3)
CMAKE_GUI_VERSION=$(call get_stdout_version_index,cmake-gui,--version,3)
CMAKE_INIT_VERSION=$(call get_stdout_version,cmake-init,--version)
CMAKE_VERSION=$(call get_stdout_version_index,cmake,--version,3)
CPACK_VERSION=$(call get_stdout_version_index,cpack,--version,3)
CTEST_VERSION=$(call get_stdout_version_index,ctest,--version,3)

.PHONY: cmake_utitiles_info
cmake_utitiles_info:
	@echo 'ccmake version     = [$(CCMAKE_VERSION)]'
	@echo 'cmake-init version = [$(CMAKE_INIT_VERSION)]'
	@echo 'cmake-gui version  = [$(CMAKE_GUI_VERSION)]'
	@echo 'cmake version      = [$(CMAKE_VERSION)]'
	@echo 'cpack version      = [$(CPACK_VERSION)]'
	@echo 'ctest version      = [$(CTEST_VERSION)]'

platform_info:: cmake_utitiles_info

################################################################################
# END cmake utilities section.                                                 #
################################################################################

################################################################################
# BEGIN commands section.                                                      #
################################################################################

GAWK_VERSION=$(call get_stdout_version,gawk,--version,| head -1 | gawk '{print $$3}' | sed 's/\x2c.*$$//')
SED_VERSION=$(call get_stdout_version_index,sed,--version,4)
SSH_VERSION=$(call get_stderr_version,ssh,-V,| sed 's/  / /g')
TAR_VERSION=$(call get_stdout_version_index,tar,--version,4)

.PHONY: commands_info
commands_info:
	@echo 'gawk version           = [$(GAWK_VERSION)]'
	@echo 'sed version            = [$(SED_VERSION)]'
	@echo 'ssh version            = [$(SSH_VERSION)]'
	@echo 'tar version            = [$(TAR_VERSION)]'

platform_info:: commands_info

################################################################################
# END commands section.                                                        #
################################################################################

################################################################################
# BEGIN compilers section.                                                     #
################################################################################

CLANG_COMMAND=clang
CLANG=$(shell command -v $(CLANG_COMMAND))
CLANG_VERSION=$(call get_stdout_version_index,$(CLANG),--version,3)
CLANG_CPP_STANDARD=$(if $(CLANG),$(call get_compiler_std,$(CLANG_COMMAND)))

GNU_CPP_COMMAND=g++
GNU_CPP=$(shell command -v $(GNU_CPP_COMMAND))
GNU_CPP_VERSION=$(call get_stdout_version_index,$(GNU_CPP),--version,3)
GNU_CPP_STANDARD=$(if $(GNU_CPP),$(call get_compiler_std,$(GNU_CPP_COMMAND)))

ORACLE_CC_COMMAND=CC
ORACLE_CC=$(shell command -v $(ORACLE_CC_COMMAND))
ORACLE_CC_VERSION=$(call get_stderr_version_index,$(ORACLE_CC),-V,6)
ORACLE_SUN_STUDIO_VERSION=$(call get_stderr_version_index,$(ORACLE_CC),-V,3)
ORACLE_CPP_STANDARD=$(if $(ORACLE_CC),$(call get_compiler_std,$(ORACLE_CC_COMMAND)))

ifeq ($(notdir $(CXX)),$(CLANG_COMMAND)) # {
CXX_VERSION:=$(CLANG_VERSION)
else ifeq ($(notdir $(CXX)),$(GNU_CPP_COMMAND)) # } {
CXX_VERSION:=$(GNU_CPP_VERSION)
else ifeq ($(notdir $(CXX)),$(ORACLE_CC_COMMAND)) # } {
CXX_VERSION:=$(ORACLE_CC_VERSION)
else # } {
CXX_VERSION:=UNKNOWN
endif # }

LIBC_VERSION=$(call get_stdout_version,/lib/libc.so.6,,| head -1)
LIBSTDCPP_VERSION=$(call get_stdout_version,readelf,-sV /usr/lib64/libstdc++.so*,| sed -n 's/.*@@GLIBCXX_//p' | sort -Vru | head -1)

.PHONY: compilers_info
compilers_info:
ifneq ($(ORACLE_CC),) # {
	@echo 'Oracle $(ORACLE_CC_COMMAND) version      = [$(ORACLE_CC_VERSION)]'
	@echo 'Oracle $(ORACLE_CC_COMMAND) C++ standard = [$(ORACLE_CPP_STANDARD)]'
	@echo 'Oracle Sun Studio version                = [$(ORACLE_SUN_STUDIO_VERSION)]'
endif # }
ifneq ($(CLANG),) # {
	@echo 'clang version                            = [$(CLANG_VERSION)]'
	@echo 'clang C++ standard                       = [$(CLANG_CPP_STANDARD)]'
endif # }
ifneq ($(GNU_CPP),) # {
	@echo 'g++ version                              = [$(GNU_CPP_VERSION)]'
	@echo 'g++ C++ standard                         = [$(GNU_CPP_STANDARD)]'
endif # }
	@echo 'compiler                                 = [$(CXX)]'
	@echo 'libc version                             = [$(LIBC_VERSION)]'
	@echo 'libstdc++ version                        = [$(LIBSTDCPP_VERSION)]'

platform_info:: compilers_info

################################################################################
# END compilers section.                                                       #
################################################################################

################################################################################
# BEGIN compression/decompression section.                                     #
################################################################################

BZIP2=$(shell command -v bzip2)
BZIP2_VERSION=$(call get_stderr_version,$(BZIP2),--help,| sed 's/^.*Version //;s/\x2c.*$$//' | head -1)
BUNZIP2=$(shell command -v bunzip2)
BUNZIP2_VERSION=$(call get_stderr_version,$(BUNZIP2),--help,| sed 's/^.*Version //;s/\x2c.*$$//' | head -1)

GZIP=$(shell command -v gzip)
GZIP_VERSION=$(call get_stdout_version_index,$(GZIP),--version,2)
GUNZIP=$(shell command -v gunzip)
GUNZIP_VERSION=$(call get_stdout_version_index,$(GUNZIP),--version,3)

LZIP=$(shell command -v lzip)
LZIP_VERSION=$(call get_stdout_version_index,$(LZIP),--version,2)

LZMA=$(shell command -v lzma)
LZMA_VERSION=$(call get_stdout_version_index,$(LZMA),--version,4)
UNLZMA=$(shell command -v unlzma)
UNLZMA_VERSION=$(call get_stdout_version_index,$(UNLZMA),--version,4)

LZOP=$(shell command -v lzop)
LZOP_VERSION=$(call get_stdout_version_index,$(LZOP),--version,2)

XZ=$(shell command -v xz)
XZ_VERSION=$(call get_stdout_version_index,$(XZ),--version,4)
UNXZ=$(shell command -v unxz)
UNXZ_VERSION=$(call get_stdout_version_index,$(UNXZ),--version,4)

ZIP=$(shell command -v zip)
ZIP_VERSION=$(call get_stdout_version,$(ZIP),-v,| sed -n '2{p;q}' | gawk '{print $$4}')
UNZIP=$(shell command -v unzip)
UNZIP_VERSION=$(call get_stdout_version_index,$(UNZIP),-v,2)

.PHONY: compression_info
compression_info:
	@echo 'bunzip2 version = [$(BUNZIP2_VERSION)]'
	@echo 'bzip2 version   = [$(BZIP2_VERSION)]'
	@echo 'gunzip version  = [$(GUNZIP_VERSION)]'
	@echo 'gzip version    = [$(GZIP_VERSION)]'
	@echo 'lzip version    = [$(LZIP_VERSION)]'
	@echo 'lzma version    = [$(LZMA_VERSION)]'
	@echo 'lzop version    = [$(LZOP_VERSION)]'
	@echo 'unlzma version  = [$(UNLZMA_VERSION)]'
	@echo 'unxz version    = [$(UNXZ_VERSION)]'
	@echo 'unzip version   = [$(UNZIP_VERSION)]'
	@echo 'xz version      = [$(XZ_VERSION)]'
	@echo 'zip version     = [$(ZIP_VERSION)]'

platform_info:: compression_info

################################################################################
# END compression/decompression section.                                       #
################################################################################

################################################################################
# BEGIN core file section.                                                     #
################################################################################

CORE_FILE_SIZE=$(shell ulimit -c 2> /dev/null || echo 'UNKNOWN')
ifneq ($(CORE_FILE_SIZE),UNKNOWN) # {
ifneq ($(CORE_FILE_SIZE),unlimited) # {
CORE_FILE_SIZE=$(shell printf "%'d bytes" $(CORE_FILE_SIZE))
else # } {
CORE_FILE_SIZE += bytes
endif # }
endif # }

################################################################################
# NOTE: Assuming that the core file name will be core.PID and that it will be  #
#       in the current directory.                                              #
# NOTE: Do not want to overwrite an existing file with the core file.          #
################################################################################
CORE_FILE_ENABLED=$(shell if [[ "0" != $$(ulimit -c) ]];                           \
                           then                                                    \
                             if [[ -w $(CURDIR) ]];                                \
                             then                                                  \
                               bash &                                              \
                               BACKGROUND_PID=$${!};                               \
                               if [[ ! -e core.$${BACKGROUND_PID} ]];              \
                               then                                                \
                                 kill -6 %1;                                       \
                                 wait > /dev/null 2>&1;                            \
                                 if [[ -e core.$${BACKGROUND_PID} ]];              \
                                 then                                              \
                                   file core.$${BACKGROUND_PID} | grep -q "/bash"; \
                                   if [[ 0 == $${?} ]];                            \
                                   then                                            \
                                     echo 'yes';                                   \
                                     rm core.$${BACKGROUND_PID};                   \
                                   else                                            \
                                     echo 'no';                                    \
                                   fi;                                             \
                                 else                                              \
                                   echo 'no';                                      \
                                 fi;                                               \
                               else                                                \
                                 kill -9 %1;                                       \
                                 wait > /dev/null 2>&1;                            \
                                 echo 'UNKNOWN';                                   \
                               fi;                                                 \
                             else                                                  \
                               echo 'UNKNOWN';                                     \
                             fi;                                                   \
                           else                                                    \
                             echo 'no';                                            \
                           fi)

CORE_FILE_PATTERN=$(shell /sbin/sysctl kernel.core_pattern 2> /dev/null | sed 's/^.*= //')

.PHONY: core_file_info
core_file_info:
	@echo 'core file enabled = [$(CORE_FILE_ENABLED)]'
	@echo 'core file pattern = [$(CORE_FILE_PATTERN)]'
	@echo 'core file size    = [$(CORE_FILE_SIZE)]'

platform_info:: core_file_info

################################################################################
# END core file section.                                                       #
################################################################################

################################################################################
# BEGIN developer utilities section.                                           #
################################################################################

CLOC=$(shell { command -v cloc || echo 'UNKNOWN'; })
ifeq ($(CLOC),UNKNOWN) # {
CLOC_VERSION=UNKNOWN
else ifeq ($(CLOC),) # } {
CLOC=UNKNOWN
CLOC_VERSION=UNKNOWN
else # } {
CLOC_VERSION=$(call get_stdout_version,$(CLOC),--version)
endif # }

CCACHE_VERSION=$(call get_stdout_version_index,ccache,--version,3)
GDB_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,gdb,--version,5))))
GEM_VERSION=$(call get_stdout_version,gem,--version)
INDENT_VERSION=$(call get_stdout_version,indent,--version,| sed 's/^.* //')
MELD_VERSION=$(call get_stdout_version,meld,--version,| sed 's/^.* //')
NODE_JS_VERSION=$(call get_stdout_version,node,--version,| sed 's/^v//')
VIM_VERSION=$(call get_stderr_version,vim,--version,| head -1)

.PHONY: developer_utils_info
developer_utils_info:
	@echo 'ccache version      = [$(CCACHE_VERSION)]'
	@echo 'cloc version        = [$(CLOC_VERSION)]'
	@echo 'gdb version         = [$(GDB_VERSION)]'
	@echo 'gem version         = [$(GEM_VERSION)]'
	@echo 'indent version      = [$(INDENT_VERSION)]'
	@echo 'meld version        = [$(MELD_VERSION)]'
	@echo 'Node.js version     = [$(NODE_JS_VERSION)]'
	@echo 'vim version         = [$(VIM_VERSION)]'

platform_info:: developer_utils_info

################################################################################
# END developer utilities section.                                             #
################################################################################

################################################################################
# BEGIN host architecture section.                                             #
################################################################################

################################################################################
# Getting the Address Space Layout Randomization level.                        #
################################################################################
ASLR_LEVEL=$(shell /sbin/sysctl -a --pattern randomize 2> /dev/null | sed 's/^.* //')
ifeq ($(ASLR_LEVEL), 0) # {
  ASLR_LEVEL:=Disabled ($(ASLR_LEVEL))
else ifeq ($(ASLR_LEVEL), 1) # } {
  ASLR_LEVEL:=Conservative Randomization ($(ASLR_LEVEL))
else ifeq ($(ASLR_LEVEL), 2) # } {
  ASLR_LEVEL:=Full Randomization ($(ASLR_LEVEL))
else # } {
  ASLR_LEVEL:=UNKNOWN
endif # }

################################################################################
# Checks to see if this is a 64-bit computer.                                  #
################################################################################
ARCHITECTURE=$(shell uname --machine)
check_64bit_arch=$(if $(filter x86_64,$(ARCHITECTURE)),,$(error $(ERROR_LABEL) This computer does not have 64-bit architecture, but has [$(ARCHITECTURE)]))

CXX ?= g++
COMPILER_TARGET_MACHINE=$(shell $(CXX) -dumpmachine 2> /dev/null)
CPU_MODEL=$(shell lscpu 2> /dev/null | grep "^Model name:" | sed 's/^Model name://;s/^ *//')
CPU_OP_MODES=$(shell lscpu 2> /dev/null | grep "^CPU op-mode(s):" | sed 's/^CPU op-mode(s)://;s/^ *//')
ENDIANESS=$(shell echo I | tr -d [:space:] | od -to2 | head -n1 | gawk '{print $$2}' | cut -c6 | sed -e 's/^1$$/little/' -e 's/^0$$/big/')
HW_CONCURRENCY=$(shell lscpu 2> /dev/null | grep "^CPU(s):" | sed 's/^.* //')
HYPERVISOR_VENDOR=$(shell (((type lscpu > /dev/null 2>&1) && ((lscpu 2> /dev/null | grep "^Hypervisor vendor: ") || echo 'Hypervisor vendor: real machine')) || echo 'Hypervisor vendor: UNKNOWN') | sed 's/^Hypervisor vendor: //')

.PHONY: host_architecture_info
host_architecture_info:
	@echo 'ASLR Level              = [$(ASLR_LEVEL)]'
	@echo 'Architecture            = [$(ARCHITECTURE)]'
	@echo 'CPU Model               = [$(CPU_MODEL)]'
	@echo 'CPU Op Mode(s)          = [$(CPU_OP_MODES)]'
	@echo 'Endianess               = [$(ENDIANESS)]'
	@echo 'Hardware Concurrency    = [$(HW_CONCURRENCY)]'
	@echo 'compiler target machine = [$(COMPILER_TARGET_MACHINE)]'
	@echo 'hypervisor vendor       = [$(HYPERVISOR_VENDOR)]'

platform_info:: host_architecture_info

################################################################################
# END host architecture section.                                               #
################################################################################

################################################################################
# BEGIN host specific section.                                                 #
################################################################################

DOMAIN_NAME=$(shell domainname)
HOSTNAME=$(shell hostname -s)
KERNEL_RELEASE=$(shell uname --kernel-release)
OS_PLATFORM=$(shell uname -s -v -m -p -o)
UPTIME=$(shell { uptime 2> /dev/null || echo 'UNKNOWN'; } | sed 's/\(^.*up \+\)\(.*\)\(,.*users.*\)/\2/;s/\(^.*\)\(:\)\(.*\)/up \1 hours, \3 minutes/')

.PHONY: host_specific_info
host_specific_info:
	@echo 'Operating System = [$(OS_PLATFORM)]'
	@echo 'domain name      = [$(DOMAIN_NAME)]'
	@echo 'hostname         = [$(HOSTNAME)]'
	@echo 'kernel release   = [$(KERNEL_RELEASE)]'
	@echo 'uptime           = [$(UPTIME)]'

platform_info:: host_specific_info

################################################################################
# END host specific section.                                                   #
################################################################################

################################################################################
# BEGIN java build tools section.                                              #
################################################################################

ANT_VERSION=$(call get_stdout_version_index,ant,-version,4)
GRADLE_VERSION=$(call get_stdout_version,gradle,--version,| sed -n '3{p;q}' | sed 's/^.* //')
MAVEN_VERSION=$(call get_stdout_version_index,mvn,--version,3)

.PHONY: java_info
java_info:
	@echo 'ant version    = [$(ANT_VERSION)]'
	@echo 'mvn version    = [$(MAVEN_VERSION)]'
	@echo 'gradle version = [$(GRADLE_VERSION)]'

platform_info:: java_info

################################################################################
# END java build tools section.                                                #
################################################################################

################################################################################
# BEGIN miscellaneous section.                                                 #
################################################################################

CURL_VERSION=$(call get_stdout_version_index,curl,--version,2)
ESPEAK_VERSION=$(call get_stdout_version_index,$(ESPEAK_COMMAND),--version,4)
FC_LIST_VERSION=$(call get_stderr_version,fc-list,--version,|grep ' version ' | sed 's/^.* //')
FFTW3_VERSION:=$(call get_static_lib_version,fftw3)
LSCPU_VERSION=$(call get_stdout_version,lscpu,--version,| sed 's/^.* //')
LSOF_VERSION=$(call get_stderr_version,lsof,-v,| grep "^ *revision:" | sed 's/^.* //')
LSPCI_VERSION=$(call get_stdout_version,lspci,--version,| sed 's/^.* //')
LUA_VERSION=$(call get_stdout_version_index,lua,-v,2)
MAN2HTML_VERSION=$(call get_stdout_version,man2html,-version,| tail -1 | sed 's/^.* //;s/<.*$$//;s/man-//')
MYSQL_VERSION=$(call get_stdout_version_index,mysql,--version,3)
NETSTAT_VERSION=$(call get_stdout_version_index,netstat,--version,2)
NPM_VERSION=$(call get_stdout_version,npm,--version)
PSTREE_VERSION=$(call get_stderr_version,pstree,--version,| grep "^pstree " | sed 's/^pstree .* //')
QT_VERSION=$(call get_stdout_version,qmake,--version,| tail -1 | gawk '{print $$4}')
RUBY_VERSION=$(call get_stdout_version_index,ruby,--version,2)
RUSTC_VERSION=$(call get_stdout_version_index,rustc,--version,2)
STRINGS_VERSION=$(subst $(escaped_space),.,$(wordlist 1,2,$(subst ., ,$(call get_stdout_version_index,strings,--version,4))))
WGET_VERSION=$(call get_stdout_version_index,wget,--version,3)

.PHONY: miscellaneous_info
miscellaneous_info:
	@echo 'Qt version                = [$(QT_VERSION)]'
	@echo '$(ESPEAK_COMMAND) version = [$(ESPEAK_VERSION)]'
	@echo 'curl version              = [$(CURL_VERSION)]'
	@echo 'fc-list version           = [$(FC_LIST_VERSION)]'
	@echo 'fftw3 version             = [$(FFTW3_VERSION)]'
	@echo 'lscpu version             = [$(LSCPU_VERSION)]'
	@echo 'lsof version              = [$(LSOF_VERSION)]'
	@echo 'lspci version             = [$(LSPCI_VERSION)]'
	@echo 'lua version               = [$(LUA_VERSION)]'
	@echo 'man2html version          = [$(MAN2HTML_VERSION)]'
	@echo 'mysql version             = [$(MYSQL_VERSION)]'
	@echo 'netstat version           = [$(NETSTAT_VERSION)]'
	@echo 'npm version               = [$(NPM_VERSION)]'
	@echo 'pstree version            = [$(PSTREE_VERSION)]'
	@echo 'ruby version              = [$(RUBY_VERSION)]'
	@echo 'rustc version             = [$(RUSTC_VERSION)]'
	@echo 'strings version           = [$(STRINGS_VERSION)]'
	@echo 'wget version              = [$(WGET_VERSION)]'

platform_info:: miscellaneous_info

################################################################################
# END miscellaneous section.                                                   #
################################################################################

################################################################################
# BEGIN numeric utilities section.                                             #
################################################################################

GNU_DL_VERSION=$(call get_stderr_version,gdl,--version,| sed 's/^.*Version //')
OCTAVE_VERSION=$(call get_stderr_version_index,octave,--version,4)

.PHONY: numeric_utilities_info
numeric_utilities_info:
	@echo 'gdl version    = [$(GNU_DL_VERSION)]'
	@echo 'octave version = [$(OCTAVE_VERSION)]'

platform_info:: numeric_utilities_info

################################################################################
# END numeric utilities section.                                               #
################################################################################

################################################################################
# BEGIN parallel frameworks section.                                           #
################################################################################

# See: https://www.openmp.org/specifications/
CXX ?= g++
OPEN_MP ?= -fopenmp
OPEN_MP_VERSION=$(shell $(CXX) $(OPEN_MP) -dM -E -x c++ /dev/null 2> /dev/null | grep _OPENMP | sed 's/\#define _OPENMP //')
ifeq ($(OPEN_MP_VERSION),199810) # {
  OPEN_MP_VERSION:=1.0
else ifeq ($(OPEN_MP_VERSION),200203) # } {
  OPEN_MP_VERSION:=2.0
else ifeq ($(OPEN_MP_VERSION),200505) # } {
  OPEN_MP_VERSION:=2.5
else ifeq ($(OPEN_MP_VERSION),200805) # } {
  OPEN_MP_VERSION:=3.0
else ifeq ($(OPEN_MP_VERSION),200811) # } {
  OPEN_MP_VERSION:=3.0
else ifeq ($(OPEN_MP_VERSION),201107) # } {
  OPEN_MP_VERSION:=3.1
else ifeq ($(OPEN_MP_VERSION),201109) # } {
  OPEN_MP_VERSION:=3.1
else ifeq ($(OPEN_MP_VERSION),201307) # } {
  OPEN_MP_VERSION:=4.0
else ifeq ($(OPEN_MP_VERSION),201310) # } {
  OPEN_MP_VERSION:=4.0
else ifeq ($(OPEN_MP_VERSION),201402) # } {
  OPEN_MP_VERSION:=4.0.1
else ifeq ($(OPEN_MP_VERSION),201503) # } {
  OPEN_MP_VERSION:=4.0.2
else ifeq ($(OPEN_MP_VERSION),201511) # } {
  OPEN_MP_VERSION:=4.5
else ifeq ($(OPEN_MP_VERSION),201811) # } {
  OPEN_MP_VERSION:=5.0
else # } {
  OPEN_MP_VERSION=$(shell [[ $$(echo $(OPEN_MP_VERSION)) =~ ^[1-9][0-9][0-9][0-9][01][1-9]$$ ]];                                            \
                              if [[ 0 == $${?} ]];                                                                                          \
                              then                                                                                                          \
                                echo $$(echo $${BASH_REMATCH[0]}                                                                          | \
                                sed -e 's/\(^[1-9][0-9][0-9][0-9]\)\([0-9][0-9]\)$$/\2 \1/' -e 's/^0//'                                   | \
                                gawk '{split("Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec", month, " "); print month[$$1]",", $$2; }' | \
                                sed 's/^/Specification Date: /');                                                                           \
                              else                                                                                                          \
                                echo UNKNOWN;                                                                                               \
                              fi)
endif # }

OPEN_MPI_VERSION=$(call get_stdout_version,ompi_info,,| grep "Open MPI: " | gawk '{print $$3}')

################################################################################
# Getting the full path of the pthread library.                                #
################################################################################
ifdef LD_LIBRARY_PATH # {
PTHREAD_LIB=$(firstword $(realpath $(shell locate libpthread.so 2> /dev/null | grep "/lib64/")))
PTHREAD_LIB:=$(if $(PTHREAD_LIB),$(PTHREAD_LIB),UNKNOWN)
else # } {
PTHREAD_LIB:=UNKNOWN
endif # }

.PHONY: parallel_frameworks_info
parallel_frameworks_info:
	@echo 'OpenMP version  = [$(OPEN_MP_VERSION)]'
	@echo 'OpenMPI version = [$(OPEN_MPI_VERSION)]'
	@echo 'pthread library = [$(PTHREAD_LIB)]'

platform_info:: parallel_frameworks_info

################################################################################
# END parallel frameworks section.                                             #
################################################################################

################################################################################
# BEGIN programming utilities section.                                         #
################################################################################

DDD_VERSION=$(call get_stdout_version_index,ddd,--version,3)
ECLIPSE_IDE=$(shell type -fap eclipse | { xargs stat --terse -L -c"%n %Z" 2> /dev/null || echo 'UNKNOWN'; } | sort -n -k2,2 -r | head -1 | sed 's/\(^.*\)\( \)\([0-9]\+$$\)/\1/')
GLOBAL_VERSION=$(call get_stdout_version_index,global,--version,4)
GO_VERSION=$(call get_stdout_version,go,version,| sed 's/^.* version go//;s/ .*$$//')
GTAGS_VERSION=$(call get_stdout_version_index,gtags,--version,4)
GUILE_VERSION=$(call get_stdout_version_index,guile,--version,4)
HTAGS_VERSION=$(call get_stdout_version_index,htags,--version,4)
MAKE_PID=$(shell echo $${PPID})
NASM_VERSION=$(call get_stdout_version_index,nasm,-version,3)

.PHONY: programming_utils_info
programming_utils_info:
	@echo '$(MAKE) PID     = [$(MAKE_PID)]'
	@echo '$(MAKE) version = [$(MAKE_VERSION)]'
	@echo '.INCLUDE_DIRS   = [$(.INCLUDE_DIRS)]'
	@echo 'Eclipse IDE     = [$(subst ${HOME},$${HOME},$(ECLIPSE_IDE))]'
	@echo 'ddd version     = [$(DDD_VERSION)]'
	@echo 'global version  = [$(GLOBAL_VERSION)]'
	@echo 'go version      = [$(GO_VERSION)]'
	@echo 'gtags version   = [$(GTAGS_VERSION)]'
	@echo 'guile version   = [$(GUILE_VERSION)]'
	@echo 'htags version   = [$(HTAGS_VERSION)]'
	@echo 'nasm version    = [$(NASM_VERSION)]'

platform_info:: programming_utils_info

################################################################################
# END programming utilities section.                                           #
################################################################################

################################################################################
# BEGIN rpm utilities section.                                                 #
################################################################################

DNF_VERSION=$(call get_stdout_version_index,dnf,--version,1)
RPM_VERSION=$(call get_stdout_version,rpm,--version,| sed 's/^.* //')
YUM_VERSION=$(call get_stdout_version_index,yum,--version,1)

.PHONY: rpm_utils_info
rpm_utils_info:
	@echo 'dnf version = [$(DNF_VERSION)]'
	@echo 'rpm version = [$(RPM_VERSION)]'
	@echo 'yum version = [$(YUM_VERSION)]'

platform_info:: rpm_utils_info

################################################################################
# END rpm utilities section.                                                   #
################################################################################

################################################################################
# BEGIN shell section.                                                         #
################################################################################

################################################################################
# Gets the version of the specified shell by excuting the shell with the       #
# --version option. Use this function when the version string is written to    #
# stdout.                                                                      #
#                                                                              #
# $(1) - shell                                                                 #
# $(2) - word index that contains the version                                  #
################################################################################
get_shell_version_stdout=$(eval version=$(word $(2),$(shell $(1) --version 2> /dev/null | head -1)))$(if $(version),,$(eval version=UNKNOWN))$(version)

################################################################################
# Gets the version of the specified shell by excuting the shell with the       #
# --version option and redirecting stderr to stdout. Use this function when    #
# the version string is written to stderr.                                     #
#                                                                              #
# $(1) - shell                                                                 #
# $(2) - word index that contains the version                                  #
################################################################################
get_shell_version_stderr=$(eval version=$(word $(2),$(shell $(1) --version 2>&1 | head -1)))$(if $(version),,$(eval version=UNKNOWN))$(version)

################################################################################
# Gets the version of the specified shell by querying the rpm database.        #
# stdout.                                                                      #
#                                                                              #
# $(1) - shell                                                                 #
################################################################################
get_shell_version_rpm=$(eval version=$(shell rpm --query $(notdir $(1)) | sed 's/^$(notdir $(1))-//;s/\.fc.*$$//'))$(if $(version),,$(eval version:=UNKNOWN))$(version)

################################################################################
# Gets the shell version.                                                      #
#                                                                              #
# $(1) - shell                                                                 #
################################################################################
get_shell_version=$(if $(filter bash,$(notdir $(1))),$(call get_shell_version_stdout,$(1),4),                  \
                    $(if $(filter tcsh zsh,$(notdir $(1))),$(call get_shell_version_stdout,$(1),2),            \
                      $(if $(filter ksh93,$(notdir $(1))),$(call get_shell_version_stderr,$(1),5),             \
                        $(if $(filter mksh dash,$(notdir $(1))),$(call get_shell_version_rpm,$(1)),UNKNOWN))))

ETC_SHELLS=$(sort $(realpath $(shell egrep '(sh$$|ksh)' /etc/shells)))
SHELL_VERSION=$(call get_stdout_version,$(SHELL),--version,| head -1)
.PHONY: shell_info
shell_info:
	@echo 'shell         = [$(realpath $(SHELL))]'
	@echo 'shell version = [$(SHELL_VERSION)]'
	@$(foreach sh,$(ETC_SHELLS),echo '$(notdir $(sh)) version = [$(strip $(call get_shell_version,$(sh)))]';)

platform_info:: shell_info

################################################################################
# END shell section.                                                           #
################################################################################

################################################################################
# BEGIN web browser section.                                                   #
################################################################################

################################################################################
# Gets the web browser version.                                                #
#                                                                              #
# $(1) - web browser                                                           #
# $(2) - command line option to get the web browser version                    #
################################################################################
get_web_browser_version=$(if $(1),$(shell { $(1) $(2) 2> /dev/null || echo 'UNKNOWN'; } | sed 's/ *$$//;s/^.* //'),UNKNOWN)

################################################################################
# Prints the browser, and if the input browser matches the specific browser,   #
# the browser version is also printed.                                         #
#                                                                              #
# $(1) - the input browser                                                     #
# $(2) - a specific browser                                                    #
# $(3) - the specific browser's version                                        #
################################################################################
print_browser_info=$(if $(filter $(notdir $(1)),$(notdir $(2))),echo 'browser $(notdir $(1)) version = [$(3)]',echo '$(notdir $(2)) version = [$(3)]')

FIREFOX_BROWSER=firefox
FIREFOX=$(shell command -v $(FIREFOX_BROWSER))
FIREFOX_VERSION:=$(call get_web_browser_version,$(FIREFOX),-v)

GOOGLE_CHROME_BROWSER=google-chrome
GOOGLE_CHROME=$(shell command -v $(GOOGLE_CHROME_BROWSER))
GOOGLE_CHROME_VERSION:=$(call get_web_browser_version,$(GOOGLE_CHROME),--version)

BROWSER ?= $(notdir $(shell command -v $(FIREFOX)))
ifneq ($(BROWSER),) # {
ifeq ($(BROWSER),$(notdir $(FIREFOX))) # {
  BROWSER_VERSION=$(FIREFOX_VERSION)
else ifeq ($(BROWSER),$(notdir $(GOOGLE_CHROME))) # } {
  BROWSER_VERSION:=$(GOOGLE_CHROME_VERSION)
else # } {
  BROWSER_VERSION:=UNKNOWN
endif # }
else # } {
  BROWSER="UNKNOWN BROWSER"
  BROWSER_VERSION:=UNKNOWN
endif # }

.PHONY: web_browser_info
web_browser_info:
	@$(if $(filter-out $(notdir $(FIREFOX_BROWSER) $(GOOGLE_CHROME_BROWSER)),$(notdir $(BROWSER))),echo 'browser = [$(BROWSER)]')
	@$(call print_browser_info,$(BROWSER),$(FIREFOX_BROWSER),$(FIREFOX_VERSION))
	@$(call print_browser_info,$(BROWSER),$(GOOGLE_CHROME_BROWSER),$(GOOGLE_CHROME_VERSION))

platform_info:: web_browser_info

################################################################################
# END web browser section.                                                     #
################################################################################

.PHONY: platform
platform:
	@$(MAKE) $(NO_DIR) platform_info | sort | sed 's/=/~/2' | column -s"=" -o"=" -t | sed 's/~/=/'
ifneq ($(filter $(@),$(MAKECMDGOALS)),) # {
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))
endif # }

PLATFORM_LOG=platform.log
.PHONY: platform_log
platform_log: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
platform_log:
	@$(MAKE) $(NO_DIR) platform  >| $(PLATFORM_LOG)
	@sed -i "s@${HOME}@\$${HOME}@g" $(PLATFORM_LOG)
	@sed -i '1 i\$(DATE_TIME)\n'    $(PLATFORM_LOG)
	@$(if $(filter $(@),$(MAKECMDGOALS)),[[ -s $(PLATFORM_LOG) ]] && echo '$(EDITOR) $(PLATFORM_LOG)' || echo '$(ERROR_LABEL) The platform log file [$(PLATFORM_LOG)] is empty.')
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))

HTML_DIR=html
PLATFORM_HTML=$(HTML_DIR)/platform.html
.PHONY: platform_html
platform_html: IMAGE_PATH ?= $(PROJECT_ROOT)/images
platform_html: PLATFORM_FAVICON=$(IMAGE_PATH)/tux_fedora_hat.png
platform_html: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
platform_html:
	@mkdir -p $(HTML_DIR)
	@rm -f $(PLATFORM_HTML)
	@echo '<!DOCTYPE html>'                                                                                                                                       >> $(PLATFORM_HTML)
	@echo '<html>'                                                                                                                                                >> $(PLATFORM_HTML)
	@echo '<head>'                                                                                                                                                >> $(PLATFORM_HTML)
	@echo '  <meta charset="utf-8">'                                                                                                                              >> $(PLATFORM_HTML)
	@echo '  <style>'                                                                                                                                             >> $(PLATFORM_HTML)
	@echo '    .styled-table'                                                                                                                                     >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      margin: 25px 0;'                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      font-size: 0.9em;'                                                                                                                               >> $(PLATFORM_HTML)
	@echo '      font-family: sans-serif;'                                                                                                                        >> $(PLATFORM_HTML)
	@echo '      width: 100%;'                                                                                                                                    >> $(PLATFORM_HTML)
	@echo '      box-shadow: 0 0 20px rgba(0, 0, 0.15);'                                                                                                          >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table thead'                                                                                                                               >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      background-color: #009879;'                                                                                                                      >> $(PLATFORM_HTML)
	@echo '      color: #ffffff;'                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      text-align: center;'                                                                                                                             >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table th td'                                                                                                                               >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      padding: 12px 15px;'                                                                                                                             >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table tbody tr'                                                                                                                            >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      padding: 12px 15px;'                                                                                                                             >> $(PLATFORM_HTML)
	@echo '      border-bottom: 1px solid #dddddd;'                                                                                                               >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table tbody tr:nth-of-type(even)'                                                                                                          >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      background-color: #f3f3f3;'                                                                                                                      >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table tbody tr:last-of-type(even)'                                                                                                         >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      border-bottom: 2px solid #009879;'                                                                                                               >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    .styled-table tbody tr.active-row'                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    {'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '      font-weight: bold;'                                                                                                                              >> $(PLATFORM_HTML)
	@echo '      color: #009879;'                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '    }'                                                                                                                                                 >> $(PLATFORM_HTML)
	@echo '  </style>'                                                                                                                                            >> $(PLATFORM_HTML)
	@echo '  <title>Platform Attributes</title>'                                                                                                                  >> $(PLATFORM_HTML)
	@echo '  <style type="text/css">'                                                                                                                             >> $(PLATFORM_HTML)
	@echo '    td.c2 {font-weight: bold}'                                                                                                                         >> $(PLATFORM_HTML)
	@echo '    div.c1 {text-align: center}'                                                                                                                       >> $(PLATFORM_HTML)
	@echo '  </style>'                                                                                                                                            >> $(PLATFORM_HTML)
	@echo '  <style type="text/css">'                                                                                                                             >> $(PLATFORM_HTML)
	@echo '    td.c1 {font-weight: bold}'                                                                                                                         >> $(PLATFORM_HTML)
	@echo '  </style>'                                                                                                                                            >> $(PLATFORM_HTML)
	@echo '</head>'                                                                                                                                               >> $(PLATFORM_HTML)
	@echo '<body>'                                                                                                                                                >> $(PLATFORM_HTML)
	@echo '  <div class="c1">'                                                                                                                                    >> $(PLATFORM_HTML)
	@echo '    <h2>Platform Information Date/Time: $(DATE_TIME)</h2>'                                                                                             >> $(PLATFORM_HTML)
	@echo '  </div>'                                                                                                                                              >> $(PLATFORM_HTML)
	@echo '    <table class="styled-table">'                                                                                                                      >> $(PLATFORM_HTML)
	@echo '      <thead>'                                                                                                                                         >> $(PLATFORM_HTML)
	@echo '        <tr>'                                                                                                                                          >> $(PLATFORM_HTML)
	@echo '          <th colspan="3">Platform Attributes</th>'                                                                                                    >> $(PLATFORM_HTML)
	@echo '        </tr>'                                                                                                                                         >> $(PLATFORM_HTML)
	@echo '      </thead>'                                                                                                                                        >> $(PLATFORM_HTML)
	@echo '      <tbody>'                                                                                                                                         >> $(PLATFORM_HTML)
	@echo '        <tr>'                                                                                                                                          >> $(PLATFORM_HTML)
	@echo '          <td colspan="3">'                                                                                                                            >> $(PLATFORM_HTML)
	@echo '            <div class="c1"><img src="$(notdir $(PLATFORM_FAVICON))" alt="$(notdir $(PLATFORM_FAVICON)) unavailable" class="c2"></div>'                >> $(PLATFORM_HTML)
	@echo '          </td>'                                                                                                                                       >> $(PLATFORM_HTML)
	@echo '        </tr>'                                                                                                                                         >> $(PLATFORM_HTML)
	@echo '        <tr>'                                                                                                                                          >> $(PLATFORM_HTML)
	@echo '          <td class="c1">Number</td>'                                                                                                                  >> $(PLATFORM_HTML)
	@echo '          <td class="c1">Attribute</td>'                                                                                                               >> $(PLATFORM_HTML)
	@echo '          <td class="c1">Value</td>'                                                                                                                   >> $(PLATFORM_HTML)
	@echo '        </tr>'                                                                                                                                         >> $(PLATFORM_HTML)
	@$(MAKE) $(NO_DIR) platform | sed 's/ *= /=/;s/\[//;s/\]//' | gawk -F'=' ' { printf("        <tr><td>%d</td><td>%s</td><td>%s</td></tr>\n", NR, $$1, $$2); }' >> $(PLATFORM_HTML)
	@echo '      </tbody>'                                                                                                                                        >> $(PLATFORM_HTML)
	@echo '    </table>'                                                                                                                                          >> $(PLATFORM_HTML)
	@echo '</body>'                                                                                                                                               >> $(PLATFORM_HTML)
	@echo '</html>'                                                                                                                                               >> $(PLATFORM_HTML)
	@sed -i "s@${HOME}@\$${HOME}@g" $(PLATFORM_HTML)
	@$(call beautify_html,$(PLATFORM_HTML))
	@$(call add_favicon,$(PLATFORM_FAVICON),$(PLATFORM_HTML))
	@$(call open_sole_target_file,$(@),$(PLATFORM_HTML),$(BROWSER),platform html file,$(EDITOR))
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))

clean::
	@rm -rf $(HTML_DIR)

.PHONY: platform_mk_help
platform_mk_help::
	@echo '$(MAKE) platform → displays platform information'
	@echo '$(MAKE) platform_log → saves platform information to the file [$(PLATFORM_LOG)]'
	@echo '$(MAKE) platform_html → saves platform information, in html, to the file [$(PLATFORM_HTML)]'

show_help:: platform_mk_help

endif # }

