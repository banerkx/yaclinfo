
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OpenCLErrors.h"

/**
 * Default constructor.
 */
OpenCLErrors::OpenCLErrors() : ConstSingletonBase<OpenCLErrors>(), m_tOpenCLErrors()
{
  populateErrors();
}

const std::string& OpenCLErrors::error(const int nError) const
{
  for (const auto& tErrorTuple : m_tOpenCLErrors)
  {
    if (nError == std::get<ERROR_CODE>(tErrorTuple))
    {
      return (std::get<ERROR_NAME>(tErrorTuple));
    }
  }

  static const std::string sUnknownError("Unknown OpenCL Error");
  return (sUnknownError);
}

const std::string& OpenCLErrors::errorDescription(const int nError) const
{
  for (const auto& tErrorTuple : m_tOpenCLErrors)
  {
    if (nError == std::get<ERROR_CODE>(tErrorTuple))
    {
      return (std::get<ERROR_DESCRIPTION>(tErrorTuple));
    }
  }

  static const std::string sUnknownErrorDescription("Unknown OpenCL Error Description");
  return (sUnknownErrorDescription);
}

const std::string& OpenCLErrors::errorCategory(const int nError) const
{
  for (const auto& tErrorTuple : m_tOpenCLErrors)
  {
    if (nError == std::get<ERROR_CODE>(tErrorTuple))
    {
      return (std::get<ERROR_CATEGORY>(tErrorTuple));
    }
  }

  static const std::string sUnknownErrorCategory("Unknown OpenCL Error Category");
  return (sUnknownErrorCategory);
}

const std::string& OpenCLErrors::operator[](const int nError) const
{
  return (error(nError));
}

void OpenCLErrors::populateErrors()
{
  m_tOpenCLErrors.emplace_back(
    0,
    "CL_SUCCESS",
    "N/A",
    "No OpenCL error",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -1,
    "CL_DEVICE_NOT_FOUND",
    "clGetDeviceIDs()",
    "No OpenCL devices that matched device_type were found",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -2,
    "CL_DEVICE_NOT_AVAILABLE",
    "clCreateContext()",
    "A device in devices is currently not available even though the device "
    "was returned by clGetDeviceIDs()",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -3,
    "CL_COMPILER_NOT_AVAILABLE",
    "clBuildProgram()",
    "If program is created with clCreateProgramWithSource() and a compiler "
    "is not available i.e. CL_DEVICE_COMPILER_AVAILABLE specified in the "
    "table of OpenCL Device Queries for clGetDeviceInfo() is set to CL_FALSE",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -4,
    "CL_MEM_OBJECT_ALLOCATION_FAILURE",
    "N/A",
    "If there is a failure to allocate memory for buffer object",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -5,
    "CL_OUT_OF_RESOURCES",
    "N/A",
    "If there is a failure to allocate resources required by the OpenCL "
    "implementation on the device",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -6,
    "CL_OUT_OF_HOST_MEMORY",
    "N/A",
    "If there is a failure to allocate resources required by the OpenCL "
    "implementation on the host",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -7,
    "CL_PROFILING_INFO_NOT_AVAILABLE",
    "clGetEventProfilingInfo()",
    "If the CL_QUEUE_PROFILING_ENABLE flag is not set for the command-queue, "
    "if the execution status of the command identified by event is not "
    "CL_COMPLETE or if event is a user event object",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -8,
    "CL_MEM_COPY_OVERLAP",
    "clEnqueueCopyBuffer(), clEnqueueCopyBufferRect(), clEnqueueCopyImage()",
    "If src_buffer and dst_buffer are the same buffer or sub-buffer object "
    "and the source and destination regions overlap or if src_buffer and "
    "dst_buffer are different sub-buffers of the same associated buffer "
    "object and they overlap. The regions overlap if src_offset ≤ to "
    "dst_offset ≤ to src_offset + size – 1, or if dst_offset ≤ to src_offset "
    "≤ to dst_offset + size – 1",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -9,
    "CL_IMAGE_FORMAT_MISMATCH",
    "clEnqueueCopyImage()",
    "If src_image and dst_image do not use the same image format",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -10,
    "CL_IMAGE_FORMAT_NOT_SUPPORTED",
    "clCreateImage()",
    "If the image_format is not supported",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -11,
    "CL_BUILD_PROGRAM_FAILURE",
    "clBuildProgram()",
    "If there is a failure to build the program executable. This error will "
    "be returned if clBuildProgram() does not return until the build has "
    "completed",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -12,
    "CL_MAP_FAILURE",
    "clEnqueueMapBuffer(), clEnqueueMapImage()",
    "If there is a failure to map the requested region into the host address "
    "space. This error cannot occur for image objects created with "
    "CL_MEM_USE_HOST_PTR or CL_MEM_ALLOC_HOST_PTR",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -13,
    "CL_MISALIGNED_SUB_BUFFER_OFFSET",
    "N/A",
    "If a sub-buffer object is specified as the value for an argument that "
    "is a buffer object and the offset specified when the sub-buffer object "
    "is created is not aligned to CL_DEVICE_MEM_BASE_ADDR_ALIGN value for "
    "device associated with queue",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -14,
    "CL_EXEC_STATUS_ERROR_ FOR_EVENTS_IN_WAIT_LIST",
    "N/A",
    "If the execution status of any of the events in event_list is a "
    "negative integer value",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -15,
    "CL_COMPILE_PROGRAM_FAILURE",
    "clCompileProgram()",
    "If there is a failure to compile the program source. This error will be "
    "returned if clCompileProgram() does not return until the compile has "
    "completed",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -16,
    "CL_LINKER_NOT_AVAILABLE",
    "clLinkProgram()",
    "If a linker is not available i.e. CL_DEVICE_LINKER_AVAILABLE specified "
    "in the table of allowed values for param_name for clGetDeviceInfo() is "
    "set to CL_FALSE",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -17,
    "CL_LINK_PROGRAM_FAILURE",
    "clLinkProgram()",
    "If there is a failure to link the compiled binaries and/or libraries",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -18,
    "CL_DEVICE_PARTITION_FAILED",
    "clCreateSubDevices()",
    "If the partition name is supported by the implementation but in_device "
    "could not be further partitioned",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -19,
    "CL_KERNEL_ARG_INFO_NOT_AVAILABLE",
    "clGetKernelArgInfo()",
    "If the argument information is not available for kernel",
    "Run Time And JIT Compiler (driver-dependent)");

  m_tOpenCLErrors.emplace_back(
    -30,
    "CL_INVALID_VALUE",
    "clGetDeviceIDs(), clCreateContext()",
    "This depends on the function: two or more coupled parameters had errors",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -31,
    "CL_INVALID_DEVICE_TYPE",
    "clGetDeviceIDs()",
    "If an invalid device_type is given",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -32,
    "CL_INVALID_PLATFORM",
    "clGetDeviceIDs()",
    "If an invalid platform was given",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -33,
    "CL_INVALID_DEVICE",
    "clCreateContext(), clBuildProgram()",
    "If devices contains an invalid device or are "
    "not associated with the specified platform",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -34,
    "CL_INVALID_CONTEXT",
    "N/A",
    "If context is not a valid context",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -35,
    "CL_INVALID_QUEUE_PROPERTIES",
    "clCreateCommandQueue()",
    "If specified command-queue-properties are "
    "valid but are not supported by the device",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -36,
    "CL_INVALID_COMMAND_QUEUE",
    "N/A",
    "If command_queue is not a valid command-queue",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -37,
    "CL_INVALID_HOST_PTR",
    "clCreateImage(), clCreateBuffer()",
    "This flag is valid only if host_ptr is not NULL. If specified, it "
    "indicates that the application wants the OpenCL implementation to "
    "allocate memory for the memory object and copy the data from memory "
    "referenced by host_ptr.CL_MEM_COPY_HOST_PTR and CL_MEM_USE_HOST_PTR are "
    "mutually exclusive.CL_MEM_COPY_HOST_PTR can be used with "
    "CL_MEM_ALLOC_HOST_PTR to initialize the contents of the cl_mem object "
    "allocated using host-accessible ( e.g. PCIe) memory",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -38,
    "CL_INVALID_MEM_OBJECT",
    "N/A",
    "If memobj is not a valid OpenCL memory object",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -39,
    "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
    "N/A",
    "If the OpenGL/DirectX texture internal format does not map to a "
    "supported OpenCL image format",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -40,
    "CL_INVALID_IMAGE_SIZE",
    "N/A",
    "If an image object is specified as an argument value and the image "
    "dimensions (image width, height, specified or compute row and/or slice "
    "pitch) are not supported by device associated with queue",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -41,
    "CL_INVALID_SAMPLER",
    "clGetSamplerInfo(), clReleaseSampler(), "
    "clRetainSampler(), clSetKernelArg()",
    "If sampler is not a valid sampler object",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -42,
    "CL_INVALID_BINARY",
    "clCreateProgramWithBinary(), clBuildProgram()",
    "The provided binary is unfit for the selected device (if program is "
    "created with clCreateProgramWithBinary() and devices listed in "
    "device_list do not have a valid program binary loaded)",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -43,
    "CL_INVALID_BUILD_OPTIONS",
    "clBuildProgram()",
    "If the build options specified by options are invalid",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -44,
    "CL_INVALID_PROGRAM",
    "N/A",
    "If program is a not a valid program object",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -45,
    "CL_INVALID_PROGRAM_EXECUTABLE",
    "N/A",
    "If there is no successfully built program executable available for "
    "device associated with command_queue",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -46,
    "CL_INVALID_KERNEL_NAME",
    "clCreateKernel()",
    "If kernel_name is not found in program",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -47,
    "CL_INVALID_KERNEL_DEFINITION",
    "clCreateKernel()",
    "If the function definition for__kernel function given by kernel_name "
    "such as the number of arguments, the argument types are not the same "
    "for all devices for which the program executable has been built",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -48,
    "CL_INVALID_KERNEL",
    "N/A",
    "If kernel is not a valid kernel object",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -49,
    "CL_INVALID_ARG_INDEX",
    "clSetKernelArg(), clGetKernelArgInfo()",
    "If arg_index is not a valid argument index",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -50,
    "CL_INVALID_ARG_VALUE",
    "clSetKernelArg(), clGetKernelArgInfo()",
    "If arg_value specified is not a valid value",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -51,
    "CL_INVALID_ARG_SIZE",
    "clSetKernelArg()",
    "If arg_size does not match the size of the data type for an argument "
    "that is not a memory object or if the argument is a memory object and "
    "arg_size != sizeof( cl_mem) or if arg_size is zero and the argument is "
    "declared with the__local qualifier or if the argument is a sampler and "
    "arg_size != sizeof(cl_sampler)",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -52,
    "CL_INVALID_KERNEL_ARGS",
    "N/A",
    "If the kernel argument values have not been specified",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -53,
    "CL_INVALID_WORK_DIMENSION",
    "N/A",
    "If work_dim is not a valid value (i.e. a value between 1 and 3)",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -54,
    "CL_INVALID_WORK_GROUP_SIZE",
    "N/A",
    "If local_work_size is specified and number of work-items specified by "
    "global_work_size is not evenly divisible by size of work-group given by "
    "local_work_size or does not match the work-group size specified for "
    "kernel using the__attribute__ ((reqd_work_group_size(X, Y, Z))) "
    "qualifier in program source.if local_work_size is specified and the "
    "total number of work-items in the work-group computed as "
    "local_work_size[0] *… local_work_size[work_dim – 1] is greater than the "
    "value specified by CL_DEVICE_MAX_WORK_GROUP_SIZE in the table of OpenCL "
    "Device Queries for clGetDeviceInfo(). if local_work_size is NULL and "
    "the __attribute__ ((reqd_work_group_size(X, Y, Z))) qualifier is used "
    "to declare the work-group size for kernel in the program source",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -55,
    "CL_INVALID_WORK_ITEM_SIZE",
    "N/A",
    "If the number of work-items specified in any of local_work_size[0], … "
    "local_work_size[work_dim – 1] is greater than the corresponding values "
    "specified by CL_DEVICE_MAX_WORK_ITEM_SIZES[0], …. "
    "CL_DEVICE_MAX_WORK_ITEM_SIZES[work_dim – 1]",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -56,
    "CL_INVALID_GLOBAL_OFFSET",
    "N/A",
    "If the value specified in global_work_size + the corresponding values "
    "in global_work_offset for any dimensions is greater than the "
    "sizeof(size_t) for the device on which the kernel execution will be "
    "enqueued",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -57,
    "CL_INVALID_EVENT_WAIT_LIST",
    "N/A",
    "If event_wait_list is NULL and num_events_in_wait_list > 0, or "
    "event_wait_list is not NULL and num_events_in_wait_list is 0, or if "
    "event objects in event_wait_list are not valid events",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -58,
    "CL_INVALID_EVENT",
    "N/A",
    "If event objects specified in event_list are not valid event objects",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -59,
    "CL_INVALID_OPERATION",
    "N/A",
    "If interoperability is specified by setting "
    "CL_CONTEXT_ADAPTER_D3D9_KHR, CL_CONTEXT_ADAPTER_D3D9EX_KHR or "
    "CL_CONTEXT_ADAPTER_DXVA_KHR to a non-NULL value, and interoperability "
    "with another graphics API is also specified. (only if the "
    "cl_khr_dx9_media_sharing extension is supported)",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -60,
    "CL_INVALID_GL_OBJECT",
    "N/A",
    "If texture is not a GL texture object whose type matches "
    "texture_target, if the specified miplevel of texture is not defined, or "
    "if the width or height of the specified miplevel is zero",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -61,
    "CL_INVALID_BUFFER_SIZE",
    "clCreateBuffer(), clCreateSubBuffer()",
    "If size is 0.Implementations may return CL_INVALID_BUFFER_SIZE if size "
    "is greater than the CL_DEVICE_MAX_MEM_ALLOC_SIZE value specified in the "
    "table of allowed values for param_name for clGetDeviceInfo() for all "
    "devices in context",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -62,
    "CL_INVALID_MIP_LEVEL",
    "OpenGL-functions",
    "If miplevel is greater than zero and the OpenGL implementation does not "
    "support creating from non-zero mipmap levels",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -63,
    "CL_INVALID_GLOBAL_WORK_SIZE",
    "N/A",
    "If global_work_size is NULL, or if any of the values specified in "
    "global_work_size[0], …global_work_size [work_dim – 1] are 0 or exceed "
    "the range given by the sizeof(size_t) for the device on which the "
    "kernel execution will be enqueued",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -64,
    "CL_INVALID_PROPERTY",
    "clCreateContext()",
    "Vague error, depends on the function",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -65,
    "CL_INVALID_IMAGE_DESCRIPTOR",
    "clCreateImage()",
    "If values specified in image_desc are not "
    "valid or if image_desc is NULL",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -66,
    "CL_INVALID_COMPILER_OPTIONS",
    "clCompileProgram()",
    "If the compiler options specified by options are invalid",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -67,
    "CL_INVALID_LINKER_OPTIONS",
    "clLinkProgram()",
    "If the linker options specified by options are invalid",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -68,
    "CL_INVALID_DEVICE_PARTITION_COUNT",
    "clCreateSubDevices()",
    "If the partition name specified in properties is "
    "CL_DEVICE_PARTITION_BY_COUNTS and the number of sub-devices requested "
    "exceeds CL_DEVICE_PARTITION_MAX_SUB_DEVICES or the total number of "
    "compute units requested exceeds CL_DEVICE_PARTITION_MAX_COMPUTE_UNITS "
    "for in_device, or the number of compute units requested for one or more "
    "sub-devices is less than zero or the number of sub-devices requested "
    "exceeds CL_DEVICE_PARTITION_MAX_COMPUTE_UNITS for in_device",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -69,
    "CL_INVALID_PIPE_SIZE",
    "clCreatePipe()",
    "If pipe_packet_size is 0 or the pipe_packet_size exceeds "
    "CL_DEVICE_PIPE_MAX_PACKET_SIZE value for all devices in context or if "
    "pipe_max_packets is 0",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -70,
    "CL_INVALID_DEVICE_QUEUE",
    "clSetKernelArg()",
    "When an argument is of type queue_t when it’s "
    "not a valid device queue object",
    "Compile Time (driver-independent)");

  m_tOpenCLErrors.emplace_back(
    -1000,
    "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR",
    "clGetGLContextInfoKHR(), clCreateContext()",
    "OpenCL and OpenGL not on the same device (only when using a GPU)",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1001,
    "CL_PLATFORM_NOT_FOUND_KHR",
    "clGetPlatform()",
    "No valid ICD's found",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1002,
    "CL_INVALID_D3D10_DEVICE_KHR",
    "clCreateContext(), clCreateContextFromType()",
    "If the Direct3D 10 device specified for interoperability is not "
    "compatible with the devices against which the context is to be created",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1003,
    "CL_INVALID_D3D10_RESOURCE_KHR",
    "clCreateFromD3D10BufferKHR(), clCreateFromD3D10Texture2DKHR(), "
    "clCreateFromD3D10Texture3DKHR()",
    "If the resource is not a Direct3D 10 buffer or texture object",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1004,
    "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR",
    "clEnqueueAcquireD3D10ObjectsKHR()",
    "If a mem_object is already acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1005,
    "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR",
    "clEnqueueReleaseD3D10ObjectsKHR()",
    "If a mem_object is not acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1006,
    "CL_INVALID_D3D11_DEVICE_KHR",
    "clCreateContext(), clCreateContextFromType()",
    "If the Direct3D 11 device specified for interoperability is not "
    "compatible with the devices against which the context is to be created",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1007,
    "CL_INVALID_D3D11_RESOURCE_KHR",
    "clCreateFromD3D11BufferKHR(), clCreateFromD3D11Texture2DKHR(), "
    "clCreateFromD3D11Texture3DKHR()",
    "If the resource is not a Direct3D 11 buffer or texture object",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1008,
    "CL_D3D11_RESOURCE_ALREADY_ACQUIRED_KHR",
    "clEnqueueAcquireD3D11ObjectsKHR()",
    "If a mem_object is already acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1009,
    "CL_D3D11_RESOURCE_NOT_ACQUIRED_KHR",
    "clEnqueueReleaseD3D11ObjectsKHR()",
    "If a ‘mem_object’ is not acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1010,
    "CL_INVALID_D3D9_DEVICE_NV CL_INVALID_DX9_DEVICE_INTEL",
    "clCreateContext(), clCreateContextFromType()",
    "If the Direct3D 9 device specified for interoperability is not "
    "compatible with the devices against which the context is to be created",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1011,
    "CL_INVALID_D3D9_RESOURCE_NV CL_INVALID_DX9_RESOURCE_INTEL",
    "clCreateFromD3D9VertexBufferNV(), clCreateFromD3D9IndexBufferNV(), "
    "clCreateFromD3D9SurfaceNV(), clCreateFromD3D9TextureNV(), "
    "clCreateFromD3D9CubeTextureNV(), clCreateFromD3D9VolumeTextureNV()",
    "If a ‘mem_object’ is not a Direct3D 9 resource of the required type",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1012,
    "CL_D3D9_RESOURCE_ALREADY_ACQUIRED_NV "
    "CL_DX9_RESOURCE_ALREADY_ACQUIRED_INTEL",
    "clEnqueueAcquireD3D9ObjectsNV()",
    "If any of the ‘mem_objects’ is currently already acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1013,
    "CL_D3D9_RESOURCE_NOT_ACQUIRED_NV CL_DX9_RESOURCE_NOT_ACQUIRED_INTEL",
    "clEnqueueReleaseD3D9ObjectsNV()",
    "If any of the ‘mem_objects’ is currently not acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1092,
    "CL_EGL_RESOURCE_NOT_ACQUIRED_KHR",
    "clEnqueueReleaseEGLObjectsKHR()",
    "If a ‘mem_object’ is not acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1093,
    "CL_INVALID_EGL_OBJECT_KHR",
    "clCreateFromEGLImageKHR(), clEnqueueAcquireEGLObjectsKHR()",
    "If a ‘mem_object’ is not a EGL resource of the required type",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1094,
    "CL_INVALID_ACCELERATOR_INTEL",
    "clSetKernelArg()",
    "When ‘arg_value’ is not a valid accelerator object, and by "
    "clRetainAccelerator(), clReleaseAccelerator(), and "
    "clGetAcceleratorInfo() when ‘accelerator’ is not a valid accelerator "
    "object",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1095,
    "CL_INVALID_ACCELERATOR_TYPE_INTEL",
    "clSetKernelArg(), clCreateAccelerator()",
    "When ‘arg_value’ is not an accelerator object of the correct type, or "
    "when ‘accelerator_type’ is not a valid accelerator type",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1096,
    "CL_INVALID_ACCELERATOR_DESCRIPTOR_INTEL",
    "clCreateAccelerator()",
    "When values described by ‘descriptor’ are not valid, or if a "
    "combination of values is not valid",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1097,
    "CL_ACCELERATOR_TYPE_NOT_SUPPORTED_INTEL",
    "clCreateAccelerator()",
    "When ‘accelerator_type’ is a valid accelerator type, but it not "
    "supported by any device in ‘context’",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1098,
    "CL_INVALID_VA_API_MEDIA_ADAPTER_INTEL",
    "clCreateContext(), clCreateContextFromType()",
    "If the VA API display specified for interoperability is not compatible "
    "with the devices against which the context is to be created",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1099,
    "CL_INVALID_VA_API_MEDIA_SURFACE_INTEL",
    "clEnqueueReleaseVA_APIMediaSurfacesINTEL()",
    "If ‘surface’ is not a VA API surface of the required type, by "
    "clGetMemObjectInfo() when ‘param_name’ is "
    "CL_MEM_VA_API_MEDIA_SURFACE_INTEL when was not created from a VA API "
    "surface, and from clGetImageInfo() when ‘param_name’ is "
    "CL_IMAGE_VA_API_PLANE_INTEL and ‘image’ was not created from a VA API "
    "surface",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1100,
    "CL_VA_API_MEDIA_SURFACE_ALREADY_ACQUIRED_INTEL",
    "clEnqueueReleaseVA_APIMediaSurfacesINTEL()",
    "If any of the ‘mem_objects’ is already acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -1101,
    "CL_VA_API_MEDIA_SURFACE_NOT_ACQUIRED_INTEL",
    "clEnqueueReleaseVA_APIMediaSurfacesINTEL()",
    "If any of the ‘mem_objects’ are not currently acquired by OpenCL",
    "Errors From Extensions");

  m_tOpenCLErrors.emplace_back(
    -9999,
    "NVidia",
    "clEnqueueNDRangeKernel()",
    "Illegal read or write to a buffer",
    "Errors From Vendors");
}

