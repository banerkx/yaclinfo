
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PERL_MK_INCLUDE_GUARD # {
PERL_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including platform.mk.                     #
################################################################################
ifeq ($(PLATFORM_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/platform.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# Gets the version of the specified perl module.                               #
#                                                                              #
# $(1) - perl module                                                           #
################################################################################
get_perl_module_version=$(shell perl -M$(1) -e 'print $$$(1)::VERSION ."\n";' 2> /dev/null || echo 'UNKNOWN')

PERL_DEPARSE=B::Deparse
PERL_DEPARSE_VERSION=$(call get_perl_module_version,$(PERL_DEPARSE))
CPAN_VERSION=$(call get_stderr_version,cpan,-v,| sed 's/^.*script version //;s/\x2c.*$$//')
CPAN_PM_VERSION=$(call get_stderr_version,cpan,-v,| sed 's/^.*version //')
PERL_VERSION=$(call get_stdout_version,perl,--version,| sed -n '2{p;q}' | sed 's/^.*(v//;s/).*$$//')
PERLDOC_VERSION=$(call get_stdout_version,perldoc,-V,| gawk '{print $$2}' | sed 's/^v//;s/\x2c//')
PERL_TIDY_VERSION=$(call get_stdout_version,perltidy,--version,| head -1 | sed 's/^.* v//;s/ $$//')

.PHONY: perl_info
perl_info:
	@echo 'B::Deparse version = [$(PERL_DEPARSE_VERSION)]'
	@echo 'CPAN.pm version    = [$(CPAN_PM_VERSION)]'
	@echo 'cpan version       = [$(CPAN_VERSION)]'
	@echo 'perl version       = [$(PERL_VERSION)]'
	@echo 'perldoc version    = [$(PERLDOC_VERSION)]'
	@echo 'perltidy version   = [$(PERL_TIDY_VERSION)]'

platform_info:: perl_info

endif # }

