
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef VALGRIND_MK_INCLUDE_GUARD # {
VALGRIND_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=

################################################################################
# See: https://valgrind.org                                                    #
################################################################################

VALGRIND_COMMAND=valgrind

VALGRIND_CLEAN_TARGETS=valgrind_clean
VALGRIND_PREP_TARGETS=valgrind_prep
VALGRIND_TARGETS=valgrind valgrind_xml

################################################################################
# Creates the necessary valgrind tools related variables.                      #
#                                                                              #
# $(1) - lower case valgrind tool name                                         #
################################################################################
define create_valgrind_variables
UPPER_$(1):=$(shell echo $(1) | tr [:lower:] [:upper:])
$$(UPPER_$(1))_TOOL=$(VALGRIND_COMMAND) --tool=$(1)
$$(UPPER_$(1))_OUTPUT=$(CURDIR)/$(1)_output
$$(UPPER_$(1))_EXE ?= $(EXE)
$$(UPPER_$(1))_EXE_COMMAND=$$(strip ./$$($$(UPPER_$(1))_EXE) $(RUN_OPTS))
$$(UPPER_$(1))_LOG=$$($$(UPPER_$(1))_OUTPUT)/$$($$(UPPER_$(1))_EXE)_$(1).txt
$$(UPPER_$(1))_ERROR_MARKERS=--error-markers="BEGIN ERROR $(1)","END ERROR $(1)"
VALGRIND_CLEAN_TARGETS += $(1)_clean
VALGRIND_PREP_TARGETS += $(1)_prep
VALGRIND_TARGETS += $(1) $(1)_xml
PROFILER_OUTPUT_DIRS += $$($$(UPPER_$(1))_OUTPUT)
endef

VALGRIND_TOOLS=cachegrind callgrind drd helgrind massif memcheck
$(foreach valgrind_tool,$(VALGRIND_TOOLS),$(eval $(call create_valgrind_variables,$(valgrind_tool))))

################################################################################
# Creates a shortened valgrind target with FORCE_RECOMPILE set to NO.          #
#                                                                              #
# $(1) - valgrind target                                                       #
################################################################################
define cr8_short_target
SHORT_TARGET=$$(shell echo $(1) | sed 's/\(^[a-z][a-z][a-z]\)\(.*$$$$\)/\1/')
SHORT_TARGET:=$$(subst cac,cach,$$(SHORT_TARGET))
SHORT_TARGET:=$$(subst cal,call,$$(SHORT_TARGET))
SHORT_TARGET:=$$(subst dha,dhat,$$(SHORT_TARGET))
FRNO_SHORT_TARGET=r$$(SHORT_TARGET)
.PHONY: $$(FRNO_SHORT_TARGET)
$$(FRNO_SHORT_TARGET):
	+@$(MAKE_NO_DIR) $(1) FORCE_RECOMPILE=NO

.PHONY: valgrind_mk_help
valgrind_mk_help::
	@echo '$(MAKE) $$(FRNO_SHORT_TARGET) → runs $(1) with FORCE_RECOMPILE=NO'

show_help:: valgrind_mk_help

endef
$(foreach tool,$(VALGRIND_TOOLS),$(eval $(call cr8_short_target,$(tool))))

VALGRIND_TARGETS += $(VALGRIND_CLEAN_TARGETS) $(VALGRIND_PREP_TARGETS)

VALGRIND_DOT_IMAGE_EXTENSION=svg
CALLGRIND_DOT_OUTPUT=$(CALLGRIND_OUTPUT)/$(CALLGRIND_EXE)_callgrind.$(VALGRIND_DOT_IMAGE_EXTENSION)

################################################################################
# If an interactive application is to be run under a valgrind tool, fill in    #
# VALGRIND_INPUTS **before** including this file.                              #
################################################################################
VALGRIND_INPUTS ?=

.PHONY: $(VALGRIND_TARGETS)
VALGRIND_EXTERNAL_TARGETS=clean platform_info show_help valgrind_mk_help vars print-% qprint-%
ifneq ($(filter $(VALGRIND_TARGETS) $(VALGRIND_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

################################################################################
# Relevant target(s):                                                          #
#                                                                              #
#   make cachegrind -> performs a cachegrind run                               #
#   make callgrind  -> performs a callgrind run                                #
#   make drd        -> performs a drd run                                      #
#   make helgrind   -> performs a helgrind run                                 #
#   make massif     -> performs a massif run                                   #
#   make memcheck   -> performs a memcheck run                                 #
#   make valgrind   -> performs a memcheck run (alias for memcheck)            #
################################################################################

VALGRIND:=$(shell command -v $(VALGRIND_COMMAND))
VALGRIND_VERSION=$(subst valgrind-,,$(call get_stdout_version,$(VALGRIND_COMMAND),--version))

ifneq ($(VALGRIND),) # {

VALGRIND_COMMON_OPTS=--demangle=yes     \
                     --error-exitcode=1 \
                     --max-threads=500  \
                     --num-callers=20

CACHEGRIND_FILE=$(CACHEGRIND_OUTPUT)/cachegrind.out
CACHEGRIND_OPTS=--branch-sim=yes                         \
                --cachegrind-out-file=$(CACHEGRIND_FILE) \
                --cache-sim=yes
CACHEGRIND_OPTS += $(VALGRIND_COMMON_OPTS)

CG_ANNOTATE_COMMAND=cg_annotate
CG_ANNOTATE:=$(shell command -v $(CG_ANNOTATE_COMMAND))
ifneq ($(CG_ANNOTATE),) # {
CG_ANNOTATE_VERSION=$(call get_stderr_version,$(CG_ANNOTATE),--version,| sed 's/^.*-//')
CG_ANNOTATE_OPTS:=--threshold=0 --show-percs=yes --auto=yes --context=8 --include=$(CURDIR)
ifdef INCLUDE_DIR # {
CG_ANNOTATE_OPTS += --include=$(INCLUDE_DIR)
endif # }
ifdef SRC_DIR # {
CG_ANNOTATE_OPTS += --include=$(SRC_DIR)
endif # }
endif # }

CALLGRIND_FILE=$(CALLGRIND_OUTPUT)/callgrind.out
CALLGRIND_OPTS=--collect-systime=yes                  \
               --callgrind-out-file=$(CALLGRIND_FILE) \
               --compress-strings=no
CALLGRIND_OPTS += $(VALGRIND_COMMON_OPTS)

CALLGRIND_ANNOTATE_COMMAND=callgrind_annotate
CALLGRIND_ANNOTATE:=$(shell command -v $(CALLGRIND_ANNOTATE_COMMAND))
ifneq ($(CALLGRIND_ANNOTATE),) # {
CALLGRIND_ANNOTATE_VERSION=$(call get_stderr_version,$(CALLGRIND_ANNOTATE),--version,| sed 's/^.*-//')
CALLGRIND_ANNOTATE_OPTS:=--threshold=100 --show-percs=yes --auto=yes --context=8 --inclusive=yes --tree=both --include=$(CURDIR)
ifdef INCLUDE_DIR # {
CALLGRIND_ANNOTATE_OPTS += --include=$(INCLUDE_DIR)
endif # }
ifdef SRC_DIR # {
CALLGRIND_ANNOTATE_OPTS += --include=$(SRC_DIR)
endif # }
endif # }

VALGRIND_GPROF2_DOT_COMMAND=gprof2dot
VALGRIND_GPROF2_DOT:=$(shell command -v $(VALGRIND_GPROF2_DOT_COMMAND))
VALGRIND_GPROF2_DOT_OPTS=--color-nodes-by-selftime \
                         --colormap=color          \
                         --format=callgrind        \
                         --node-thres=1.0          \
                         --total=callratios

VALGRIND_DOT_COMMAND=dot
VALGRIND_DOT:=$(shell command -v $(VALGRIND_DOT_COMMAND))
VALGRIND_DOT_SVG_RENDERER ?= :svg:core
VALGRIND_DOT_OPTS=-T$(VALGRIND_DOT_IMAGE_EXTENSION)$(VALGRIND_DOT_SVG_RENDERER) -o

LOCK_THRESHOLD ?= 45
DRD_OPTS=--check-stack-var=yes                   \
         --exclusive-threshold=$(LOCK_THRESHOLD) \
         --first-race-only=yes                   \
         --free-is-write=yes                     \
         --ignore-thread-creation=no             \
         --join-list-vol=10                      \
         --report-signal-unlocked=yes            \
         --shared-threshold=$(LOCK_THRESHOLD)    \
         --show-confl-seg=yes                    \
         --show-error-list=yes                   \
         --show-stack-usage=yes                  \
         --trace-alloc=yes                       \
         --trace-barrier=no                      \
         --trace-cond=yes                        \
         --trace-fork-join=yes                   \
         --trace-mutex=yes                       \
         --trace-rwlock=yes                      \
         --trace-semaphore=yes
DRD_OPTS += $(VALGRIND_COMMON_OPTS)

HELGRIND_OPTS=--check-stack-refs=yes        \
              --conflict-cache-size=1000000 \
              --free-is-write=yes           \
              --history-level=full          \
              --ignore-thread-creation=no   \
              --show-error-list=yes         \
              --track-lockorders=yes
HELGRIND_OPTS += $(VALGRIND_COMMON_OPTS)

MASSIF_THRESHOLD=1.0
MASSIF_FILE=$(MASSIF_OUTPUT)/massif_$(MASSIF_EXE).out
MASSIF_OPTS=--detailed-freq=10               \
            --heap=yes                       \
            --massif-out-file=$(MASSIF_FILE) \
            --max-snapshots=100              \
            --stacks=yes                     \
            --threshold=$(MASSIF_THRESHOLD)  \
            --time-unit=B
MASSIF_OPTS += $(VALGRIND_COMMON_OPTS)

MEMCHECK_OPTS=--error-limit=no                          \
              --leak-check=full                         \
              --leak-resolution=high                    \
              --show-below-main=yes                     \
              --track-origins=yes                       \
              --show-error-list=yes                     \
              --show-leak-kinds=definite,possible       \
              --errors-for-leak-kinds=definite,possible \
              --leak-check-heuristics=all
MEMCHECK_OPTS += $(VALGRIND_COMMON_OPTS)

VALGRIND_VERBOSE_MODE= -v
##MEMCHECK_EXTRA_OPTS=--show-reachable=no --show-possibly-lost=no
MEMCHECK_EXTRA_OPTS=--show-reachable=yes --show-possibly-lost=yes
MEMCHECK_OPTS += $(MEMCHECK_EXTRA_OPTS)
##MEMCHECK_OPTS += $(VALGRIND_VERBOSE_MODE)

ifdef VALGRIND_SUPPRESSIONS_FILE # {
  MEMCHECK_OPTS += --suppressions=$(VALGRIND_SUPPRESSIONS_FILE)
endif # }

################################################################################
# If FORCE_RECOMPILE is YES, then $(VALGRIND_CLEAN_TARGET) is a prerequisite   #
# and the executable under investigation is rebuilt.                           #
################################################################################
FORCE_RECOMPILE ?= YES

VALGRIND_MS_PRINT_COMMAND=ms_print
VALGRIND_MS_PRINT:=$(shell command -v $(VALGRIND_MS_PRINT_COMMAND))
MS_PRINT_VERSION=$(if $(VALGRIND_MS_PRINT),$(call get_stderr_version,$(VALGRIND_MS_PRINT),--version,| sed 's/$(VALGRIND_MS_PRINT_COMMAND)-//'))
VALGRIND_MS_PRINT_OPTS=--threshold=$(MASSIF_THRESHOLD) \
                       --x=72                          \
                       --y=40

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than build_clean.                                                      #
################################################################################
export VALGRIND_CLEAN_TARGET ?= build_clean

################################################################################
# Creates the valgrind tool target recipe.                                     #
#                                                                              #
# $(1) - valgrind tool                                                         #
# $(2) - the executable to compile                                             #
# $(3) - the directory for the results file                                    #
# $(4) - options for the valgrind tool                                         #
# $(5) - the complete command line for the executable                          #
# $(6) - the text results file                                                 #
################################################################################
define create_valgrind_target
.PHONY: $(1)
$(1): DATE_TIME:=$$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
ifeq ($(FORCE_RECOMPILE),YES) # {
$(1): $(VALGRIND_CLEAN_TARGET) $(1)_clean $(1)_prep
else # } {
$(1): $(1)_clean $(1)_prep
endif # }
	@$$(call target_info,$$(@) Profiler,$$(<),$$(?),$$(^),$$(|),$$(*))
	+@$(MAKE_NO_DIR) $(2)
	@mkdir -p $(3)
ifneq ($(strip $(VALGRIND_INPUTS)),) # {
	@echo $(VALGRIND_INPUTS) | $(VALGRIND_COMMAND) --tool=$(1) $(4) $$(VALGRIND_XML_OPTS) $$($$(UPPER_$(1))_ERROR_MARKERS) $(5) 2>&1 | sed 's/^[=-][=-][0-9]\+[=-][=-]//' | tee $(6)
else # } {
	@                          $(VALGRIND_COMMAND) --tool=$(1) $(4) $$(VALGRIND_XML_OPTS) $$($$(UPPER_$(1))_ERROR_MARKERS) $(5) 2>&1 | sed 's/^[=-][=-][0-9]\+[=-][=-]//' | tee $(6)
endif # }
	@echo '$$(UPPER_$(1))_EXE_COMMAND = [$$($$(UPPER_$(1))_EXE_COMMAND)]'         >> $(6)
ifeq ($(1),cachegrind) # {
	@echo ''                                                                      >> $(6)
ifneq ($(CG_ANNOTATE),) # {
	@echo '$(CG_ANNOTATE) $(CG_ANNOTATE_OPTS) $(CACHEGRIND_FILE)'                 >> $(6)
	@$(CG_ANNOTATE) $(CG_ANNOTATE_OPTS) $(CACHEGRIND_FILE)                        >> $(6)
else # } {
	@cat $(CACHEGRIND_FILE)                                                       >> $(6)
endif # }
	@rm $(CACHEGRIND_FILE)
else ifeq ($(1),callgrind) # } {
	@echo ''                                                                      >> $(6)
ifneq ($(CALLGRIND_ANNOTATE),) # {
	@echo '$(CALLGRIND_ANNOTATE) $(CALLGRIND_ANNOTATE_OPTS) $(CALLGRIND_FILE)'    >> $(6)
	@$(CALLGRIND_ANNOTATE) $(CALLGRIND_ANNOTATE_OPTS) $(CALLGRIND_FILE)           >> $(6)
else # } {
	@cat $(CALLGRIND_FILE)                                                        >> $(6)
endif # }
ifeq ($(if $$(VALGRIND_GPROF2_DOT),$(if $$(VALGRIND_DOT),T)),T) # {
	@$$(VALGRIND_GPROF2_DOT) $$(VALGRIND_GPROF2_DOT_OPTS) $$(CALLGRIND_FILE)                                       | \
   sed '$$$$i\\ \ labelloc="t";'                                                                                 | \
   sed '$$$$i\\ \ label="$$(CALLGRIND_TOOL) Output for ($$(CALLGRIND_EXE_COMMAND)) Date/Time: [$$(DATE_TIME)]";' | \
   $$(VALGRIND_DOT_COMMAND) $$(VALGRIND_DOT_OPTS) $(CALLGRIND_DOT_OUTPUT)
	@$$(call open_file_command,$$(CALLGRIND_DOT_OUTPUT),$$(BROWSER),$$(CALLGRIND_TOOL) diagram)
else # } {
ifeq ($$(VALGRIND_DOT),) # {
	@$$(warning [$$(@)] $(WARNING_LABEL) The [$$(VALGRIND_DOT_COMMAND)] command was not found; no [$$(CALLGRIND_TOOL)] graph image will be created.)
endif # }
ifeq ($$(VALGRIND_GPROF2_DOT),) # {
	@$$(warning [$$(@)] $(WARNING_LABEL) The [$$(VALGRIND_GPROF2_DOT_COMMAND)] command was not found; no [$$(CALLGRIND_TOOL)] graph image will be created.)
endif # }
endif # }
	@rm $$(CALLGRIND_FILE)
else ifeq ($(1),helgrind) # } {
	@grep -q "^ \?ERROR SUMMARY: 0 errors from 0 contexts" $(6); \
  if [[ 0 == $$$${?} ]];                                       \
  then                                                         \
    echo 'HELGRIND: No thread issues found.';                  \
  else                                                         \
    echo 'HELGRIND: thread issues found.';                     \
  fi;
else ifeq ($(1),massif) # } {
ifneq ($(VALGRIND_MS_PRINT),) # {
	@$(VALGRIND_MS_PRINT) $(VALGRIND_MS_PRINT_OPTS) $(MASSIF_FILE)                >> $(6)
	@rm $(MASSIF_FILE)
else # } {
	@$$(warning [$$(@)] $(WARNING_LABEL) The [$$(VALGRIND_MS_PRINT_COMMAND)] command was not found; no human readable [$(1)] output will be generated.)
endif # }
endif # }
	@sed -i '1a\ $(1) options: $(4) $(VALGRIND_XML_OPTS)'                            $(6)
	@sed -i '1a\ Date/Time: [$$(DATE_TIME)]'                                         $(6)
	@$$(call open_sole_target_file,$$(@),$(6),$(EDITOR),$(1) output)
	@$$(call elapsed_time,$$(@),$$(TARGET_START_TIME))

.PHONY: $(1)_xml
$(1)_xml: XM_FILE=$(subst .txt,.xml,$(6))
$(1)_xml: VALGRIND_XML_OPTS=--xml=yes --xml-file=$$(XM_FILE)
$(1)_xml: $(1)
	@sed -i "s@${HOME}@\$$$${HOME}@" $$(XM_FILE)
	@$$(call open_sole_target_file,$$(@),$$(XM_FILE),$(EDITOR),$(1) XML output)
	@$$(call elapsed_time,$$(@),$$(TARGET_START_TIME))

.PHONY: $(1)_prep
$(1)_prep::;

.PHONY: $(1)_clean
$(1)_clean::
	@rm -rf $(3)

clean:: $(1)_clean

.PHONY: valgrind_mk_help
valgrind_mk_help::
ifneq ($(wildcard $(2)),) # {
	@echo '$(MAKE) $(1) → performs a valgrind $(1) run'
	@echo '$(MAKE) $(1)_xml → performs a valgrind $(1) run saving results in XML format'
endif # }

endef

$(eval $(call create_valgrind_target,cachegrind,$(CACHEGRIND_EXE),$(CACHEGRIND_OUTPUT),$(CACHEGRIND_OPTS),$(CACHEGRIND_EXE_COMMAND),$(CACHEGRIND_LOG)))
$(eval $(call create_valgrind_target,callgrind,$(CALLGRIND_EXE),$(CALLGRIND_OUTPUT),$(CALLGRIND_OPTS),$(CALLGRIND_EXE_COMMAND),$(CALLGRIND_LOG)))
$(eval $(call create_valgrind_target,drd,$(DRD_EXE),$(DRD_OUTPUT),$(DRD_OPTS),$(DRD_EXE_COMMAND),$(DRD_LOG)))
$(eval $(call create_valgrind_target,helgrind,$(HELGRIND_EXE),$(HELGRIND_OUTPUT),$(HELGRIND_OPTS),$(HELGRIND_EXE_COMMAND),$(HELGRIND_LOG)))
$(eval $(call create_valgrind_target,massif,$(MASSIF_EXE),$(MASSIF_OUTPUT),$(MASSIF_OPTS),$(MASSIF_EXE_COMMAND),$(MASSIF_LOG)))
$(eval $(call create_valgrind_target,memcheck,$(MEMCHECK_EXE),$(MEMCHECK_OUTPUT),$(MEMCHECK_OPTS),$(MEMCHECK_EXE_COMMAND),$(MEMCHECK_LOG)))

valgrind: memcheck
valgrind_xml: memcheck_xml

valgrind_clean: memcheck_clean

$(VALGRIND_PREP_TARGETS)::;

platform_info::
ifneq ($(CALLGRIND_ANNOTATE),) # {
	@echo '$(CALLGRIND_ANNOTATE_COMMAND) version = [$(CALLGRIND_ANNOTATE_VERSION)]'
endif # }
ifneq ($(CG_ANNOTATE),) # {
	@echo '$(CG_ANNOTATE_COMMAND) version = [$(CG_ANNOTATE_VERSION)]'
endif # }
ifneq ($(VALGRIND_MS_PRINT),) # {
	@echo '$(VALGRIND_MS_PRINT_COMMAND) version = [$(MS_PRINT_VERSION)]'
endif # }
	@echo 'valgrind version = [$(VALGRIND_VERSION)]'

else # } {

$(VALGRIND_TARGETS):
	@$(warning [$(@)] $(WARNING_LABEL) Can not make target [$(@)]; the [$(VALGRIND_COMMAND)] command is not available)

endif # }

endif # }

endif # }

