
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GPERF_MK_INCLUDE_GUARD # {
GPERF_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# See: https://gperftools.github.io/gperftools                                 #
################################################################################

GPERF_OUTPUT=$(CURDIR)/gperf_output
GPERF_EXE ?= $(EXE)

PROFILER_OUTPUT_DIRS += $(GPERF_OUTPUT)

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=
GPERF_EXE_COMMAND=$(strip ./$(GPERF_EXE) $(RUN_OPTS))

PPROF_COMMAND=pprof
PPROF:=$(shell command -v $(PPROF_COMMAND))
PPROF_VERSION=$(call get_go_module_version,$(PPROF))

GPERF_DOT_COMMAND=dot
GPERF_DOT:=$(shell command -v $(GPERF_DOT_COMMAND))
GPERF_DOT_SVG_RENDERER ?= :svg:core
GPERF_DOT_IMAGE_EXTENSION=svg
ifneq ($(GPERF_DOT),) # {
PPROF_OUTPUT_TYPE=--dot
PPROF_CPU_PROFILER_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_cpu_profiler.$(GPERF_DOT_IMAGE_EXTENSION)
PPROF_CALL_GRAPH_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_call_graph.$(GPERF_DOT_IMAGE_EXTENSION)
PPROF_HEAP_LEAKS_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_heap_leaks.$(GPERF_DOT_IMAGE_EXTENSION)
GPERF_DOT_CPU_PROFILER_OPTS=-T$(GPERF_DOT_IMAGE_EXTENSION)$(GPERF_DOT_SVG_RENDERER) -o $(PPROF_CPU_PROFILER_FILE)
GPERF_DOT_CALL_GRAPH_OPTS=-T$(GPERF_DOT_IMAGE_EXTENSION)$(GPERF_DOT_SVG_RENDERER) -o $(PPROF_CALL_GRAPH_FILE)
GPERF_DOT_HEAP_LEAKS_OPTS=-T$(GPERF_DOT_IMAGE_EXTENSION)$(GPERF_DOT_SVG_RENDERER) -o $(PPROF_HEAP_LEAKS_FILE)
else # } {
PPROF_OUTPUT_TYPE=--pdf
PPROF_CPU_PROFILER_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_cpu_gperf_profiler.pdf
PPROF_CALL_GRAPH_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_call_graph.pdf
PPROF_HEAP_LEAKS_FILE=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_heap_leaks.pdf
endif # }

GPERF_TARGETS=gheap gheap_prep gperf gperf_clean gperf_libs gperf_prep gperftools show_lib_info
.PHONY: $(GPERF_TARGETS)
GPERF_EXTERNAL_TARGETS=clean gperf_mk_help platform_info show_help vars print-% qprint-%
ifneq ($(filter $(GPERF_TARGETS) $(GPERF_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

################################################################################
# If FORCE_RECOMPILE is YES, then $(GPERF_CLEAN_TARGET) is a prerequisite and  #
# the executable under investigation is rebuilt.                               #
################################################################################
FORCE_RECOMPILE ?= YES

################################################################################
# For the input gperf dynamic library, creates a variable to hold the full     #
# path of the library. If the name of the library is libabc.so, then the       #
# variable will be named LIB_ABC.                                              #
#                                                                              #
# $(1) - gperf library                                                         #
#                                                                              #
#   Example: $(eval $(call gperftools_lib_variables,profiler))                 #
################################################################################
define gperftools_lib_variables
LIB_NAME_$(1):=$(shell echo $(1) | sed 's/^lib//;s/\.so$$//' | tr '[:lower:]' '[:upper:]')
LIB_$$(LIB_NAME_$(1)):=$(shell echo $(1) | sed 's/^lib//;s/\.so$$//;s/\(^.*$$\)/lib\1.so/')
LIB_$$(LIB_NAME_$(1))_DIR:=$$(strip $$(call get_dynamic_library_directory,$$(LIB_$$(LIB_NAME_$(1)))))
ifeq ($$(LIB_$$(LIB_NAME_$(1))_DIR),) # {
$$(error $(ERROR_LABEL) [$(0)]: Could not determine the directory containing the dynamic library [$$(LIB_$$(LIB_NAME_$(1)))])
endif # }
LIB_$$(LIB_NAME_$(1)):=$$(realpath $$(LIB_$$(LIB_NAME_$(1))_DIR)/$$(LIB_$$(LIB_NAME_$(1))))
LIB_$$(LIB_NAME_$(1))_SONAME:=$$(call get_soname,$$(LIB_$$(LIB_NAME_$(1))))
show_lib_info::
	@echo 'LIB_$$(LIB_NAME_$(1)) = [$$(LIB_$$(LIB_NAME_$(1)))] SONAME = [$$(LIB_$$(LIB_NAME_$(1))_SONAME)] LD_PRELOAD=$$(LIB_$$(LIB_NAME_$(1)))'
################################################################################
# This variable is no longer needed.                                           #
################################################################################
undefine LIB_$$(LIB_NAME_$(1))_DIR
endef

gperf_libs:
	+@$(MAKE_NO_DIR) show_lib_info | column -s" " -o" " -t

GPERF_LIBS=profiler tcmalloc
$(foreach gperf_lib,$(GPERF_LIBS),$(eval $(call gperftools_lib_variables,$(gperf_lib))))

TCMALLOC:=$(shell echo $(LIB_TCMALLOC_SONAME) | sed 's/\(^lib\)\(.*\)\(\.so.*$$\)/-l\2/')

gperf gheap: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")

################################################################################
# From: https://gperftools.github.io/gperftools/cpuprofile.html                #
#                                                                              #
# In addition to defining the environment variable CPUPROFILE, one can also    #
# define CPUPROFILESIGNAL. This allows profiling to be controlled via the      #
# signal number that you specify. The signal number must be unused by the      #
# program under normal operation. Internally, it acts as a switch, triggered   #
# by the signal, which is off by default. For instance, if you had a copy of   #
# /bin/chrome that had been been linked against libprofilers.o, you could run: #
#                                                                              #
#   CPUPROFILE=chrome.prof CPUPROFILESIGNAL=10 /bin/chrome &                   #
#                                                                              #
# You can then trigger profiling to start:                                     #
#                                                                              #
#   % kill -SIGUSR1 chrome                                                     #
#                                                                              #
# Then after a period of time you can tell it to stop which will generate the  #
# profile:                                                                     #
#                                                                              #
#   % kill -SIGUSR1 chrome                                                     #
#                                                                              #
# NOTE: 10 = SIGUSR1                                                           #
################################################################################
CPUPROFILESIGNAL ?=

################################################################################
# If set to any value (including 0 or the empty string), use ITIMER_REAL       #
# instead of ITIMER_PROF to gather profiles. In general, ITIMER_REAL is not as #
# accurate as ITIMER_PROF, and also interacts badly with alarm(). Therefore,   #
# use ITIMER_PROF unless there is a good reason to use ITIMER_REAL. Default is #
# to not set.                                                                  #
################################################################################
CPUPROFILE_REALTIME=1

################################################################################
# The number of interrupts/second for the cpu-profiler samples. Default value  #
# is 100.                                                                      #
#                                                                              #
# NOTE: If CPUPROFILE_FREQUENCY is too small, no pprof output will be          #
#       produced.                                                              #
################################################################################
CPUPROFILE_FREQUENCY ?= 10000

################################################################################
# Files to hold the profiling data.                                            #
################################################################################
GPERF_PROFILE=$(GPERF_OUTPUT)/$(GPERF_EXE).prof
GPERF_HEAP_PROFILE=$(GPERF_OUTPUT)/$(GPERF_EXE).heap

################################################################################
# minimal - starts as late as possible in a initialization, meaning you can    #
# leak some memory in your initialization routines (that run before main(),    #
# say), and not trigger a leak message. If you frequently (and purposefully)   #
# leak data in one-time global initializers, minimal mode is useful for        #
# you. Otherwise, you should avoid it for stricter modes.                      #
#                                                                              #
# normal - tracks live objects and reports a leak for any data that is not     #
# reachable via a live object when the program exits.                          #
#                                                                              #
# strict - is much like normal but has a few extra checks that memory          #
# isn't lost in global destructors. In particular, if you have a global        #
# variable that allocates memory during program execution, and then            #
# "forgets" about the memory in the global destructor (say, by setting the     #
# pointer to it to NULL) without freeing it, that will prompt a leak message   #
# in strict mode, though not in normal mode.                                   #
#                                                                              #
# draconian - appropriate for those who like to be very precise about their    #
# memory management, and want the heap-checker to help them enforce it. In     #
# draconian mode, the heap-checker does not do "live object" checking at       #
# all, so it reports a leak unless all allocated memory is freed before        #
# program exit. (However, you can use IgnoreObject() to re-enable              #
# liveness-checking on an object-by-object basis.)                             #
#                                                                              #
# as-is - the most flexible mode; it allows you to specify the various knobs   #
# of the heap checker explicitly                                               #
#                                                                              #
# local - activates the explicit heap-check instrumentation, but does not turn #
# on any whole-program leak checking                                           #
#                                                                              #
# Tuning the Heap Checker:                                                     #
#                                                                              #
# The heap leak checker has many options, some that trade off running time     #
# and accuracy, and others that increase the sensitivity at the risk of        #
# returning false positives. For most uses, the range covered by the           #
# heap-check flavors is enough, but in specialized cases more control can be   #
# helpful.                                                                     #
#                                                                              #
# These options are specified via environment varaiables.                      #
#                                                                              #
# This first set of options controls sensitivity and accuracy. These options   #
# are ignored unless you run the heap checker in as-is mode.                   #
#                                                                              #
# HEAP_CHECK_AFTER_DESTRUCTORS - Default: false                                #
# When true, do the final leak check after all other global destructors have   #
# run. When false, do it after all REGISTER_HEAPCHECK_CLEANUP, typically       #
# much earlier in the global-destructor process.                               #
#                                                                              #
# HEAP_CHECK_IGNORE_THREAD_LIVE - Default: true                                #
# If true, ignore objects reachable from thread stacks and registers (that     #
# is, do not report them as leaks).                                            #
#                                                                              #
# HEAP_CHECK_IGNORE_GLOBAL_LIVE - Default: true                                #
# If true, ignore objects reachable from global variables and data (that is,   #
# do not report them as leaks).                                                #
#                                                                              #
# These options modify the behavior of whole-program leak checking.            #
#                                                                              #
# HEAP_CHECK_MAX_LEAKS - Default: 20                                           #
# The maximum number of leaks to be printed to stderr (all leaks are still     #
# emitted to file output for pprof to visualize). If negative or zero, print   #
# all the leaks found.                                                         #
#                                                                              #
# These options apply to all types of leak checking.                           #
#                                                                              #
# HEAP_CHECK_IDENTIFY_LEAKS - Default: false                                   #
# If true, generate the addresses of the leaked objects in the generated       #
# heap leak profile files.                                                     #
#                                                                              #
# HEAP_CHECK_TEST_POINTER_ALIGNMENT - Default: false                           #
# If true, check all leaks to see if they might be due to the use of           #
# unaligned pointers.                                                          #
#                                                                              #
# HEAP_CHECK_POINTER_SOURCE_ALIGNMENT - Default: sizeof(void*)                 #
# Alignment at which all pointers in memory are supposed to be located. Use    #
# 1 if any alignment is okay.                                                  #
#                                                                              #
# PPROF_PATH   Default: pprof -                                                #
# The location of the pprof executable.                                        #
#                                                                              #
# HEAP_CHECK_DUMP_DIRECTORY - Default: /tmp                                    #
# Where the heap-profile files are kept while the program is running.          #
################################################################################
HEAPCHECK ?= normal
HEAP_CHECK_MAX_LEAKS ?= -1
HEAP_CHECK_IDENTIFY_LEAKS ?= false
HEAP_CHECK_TEST_POINTER_ALIGNMENT ?= true
HEAP_CHECK_POINTER_SOURCE_ALIGNMENT ?= $(shell $(CXX) -dM -E -x c++ - < /dev/null 2> /dev/null | grep __SIZEOF_POINTER__ | sed 's/^.* //')
HEAP_CHECK_DUMP_DIRECTORY ?= $(GPERF_OUTPUT)
HEAP_CHECK_AFTER_DESTRUCTORS ?= false
HEAP_CHECK_IGNORE_THREAD_LIVE ?= true
HEAP_CHECK_IGNORE_GLOBAL_LIVE ?= true
PPROF_PATH=$(PPROF)
TCMALLOC_RECLAIM_MEMORY ?= false

HEAPCHECK_ENV_VARS=HEAPCHECK=$(HEAPCHECK)                                                     \
                   HEAP_CHECK_AFTER_DESTRUCTORS=$(HEAP_CHECK_AFTER_DESTRUCTORS)               \
                   HEAP_CHECK_DUMP_DIRECTORY=$(HEAP_CHECK_DUMP_DIRECTORY)                     \
                   HEAP_CHECK_IDENTIFY_LEAKS=$(HEAP_CHECK_IDENTIFY_LEAKS)                     \
                   HEAP_CHECK_IGNORE_GLOBAL_LIVE=$(HEAP_CHECK_IGNORE_GLOBAL_LIVE)             \
                   HEAP_CHECK_IGNORE_THREAD_LIVE=$(HEAP_CHECK_IGNORE_THREAD_LIVE)             \
                   HEAP_CHECK_MAX_LEAKS=$(HEAP_CHECK_MAX_LEAKS)                               \
                   HEAP_CHECK_POINTER_SOURCE_ALIGNMENT=$(HEAP_CHECK_POINTER_SOURCE_ALIGNMENT) \
                   HEAP_CHECK_TEST_POINTER_ALIGNMENT=$(HEAP_CHECK_TEST_POINTER_ALIGNMENT)     \
                   PPROF_PATH=$(PPROF_PATH)                                                   \
                   TCMALLOC_RECLAIM_MEMORY=$(TCMALLOC_RECLAIM_MEMORY)

GPERF_LOG=$(GPERF_OUTPUT)/$(GPERF_EXE)_gperf_log.txt
GHEAP_LOG=$(GPERF_OUTPUT)/$(GPERF_EXE)_gheap_log.txt

EXIFTOOL_COMMAND=exiftool
EXIFTOOL:=$(shell command -v $(EXIFTOOL_COMMAND))
EXIFTOOL_VERSION=$(call get_stdout_version,$(EXIFTOOL),-ver)

PPROF_CPU_PROFILER_OPTS ?= --cum                \
                           --lines              \
                           $(PPROF_OUTPUT_TYPE)

PPROF_CALL_GRAPH_OPTS ?= --edgefraction=0.001 \
                         --nodecount=200      \
                         --nodefraction=0.005 \
                         $(PPROF_OUTPUT_TYPE)

##--alloc_space        \
##--inuse_objects      \
##--inuse_space        \
##--alloc_objects      \
##--show_bytes         \
##$(PPROF_OUTPUT_TYPE)

PPROF_HEAP_LEAKS_OPTS ?= --edgefraction=1e-10 \
                         --heapcheck          \
                         --inuse_objects      \
                         --lines              \
                         --nodefraction=1e-10 \
                         $(PPROF_OUTPUT_TYPE)

################################################################################
# Checks the pprof results file.                                               #
#                                                                              #
# $(1) - pprof output file                                                     #
# $(2) - title string                                                          #
# $(3) - gperftools log file                                                   #
# $(4) - output file from gperf run (*.prof or *.heap file)                    #
################################################################################
define check_pprof_results_file
	@if [[ "prof" == $(suffix $(4)) ]];                                                                                 \
   then                                                                                                               \
     if [[ ! -e $(1) || 0 == $$(stat -L -c %s $(1) 2> /dev/null || echo 0) ]];                                        \
     then                                                                                                             \
       if [[ ! -e $(1) ]];                                                                                            \
       then                                                                                                           \
         ERROR_TYPE="non-existent";                                                                                   \
       else                                                                                                           \
         ERROR_TYPE="empty";                                                                                          \
       fi;                                                                                                            \
       echo -e "$(ERROR_LABEL) The gperftools $(2) results file [$(1)] is $${ERROR_TYPE}; \c"          | tee -a $(3); \
       printf "try increasing the value of CPUPROFILE_FREQUENCY from [%'d].\n" $(CPUPROFILE_FREQUENCY) | tee -a $(3); \
     fi;                                                                                                              \
   fi
endef

################################################################################
# Performs pprof analysis using the gperftools prof or heap file.              #
#                                                                              #
# $(1) - pprof command line options                                            #
# $(2) - output file from gperf run (*.prof or *.heap file)                    #
# $(3) - title string                                                          #
# $(4) - date/time string                                                      #
# $(5) - dot command line options                                              #
# $(6) - pprof output file                                                     #
# $(7) - gperftools log file                                                   #
################################################################################
ifdef GPERF_DOT # {
define perform_pprof
	@if [[ -e $(2) ]];                                                                                 \
   then                                                                                              \
     $(PPROF) $(1) ./$(GPERF_EXE) $(2)                                                             | \
      sed '$$i\\ \ labelloc="t";'                                                                  | \
      sed '$$i\\ \ label="gperftools $(3) Output for ($(GPERF_EXE_COMMAND)) Date/Time: [$(4)]";'   | \
      $(GPERF_DOT) $(5);                                                                             \
   fi
	@$(call check_pprof_results_file,$(6),$(3),$(7),$(2))
endef
else # } {
ifneq ($(EXIFTOOL),) # {
define perform_pprof
	@if [[ -e $(2) ]];                                                                                                               \
   then                                                                                                                            \
     $(PPROF) $(1) ./$(GPERF_EXE) $(2) >| $(6) || : ;                                                                              \
     if [[ 0 < $$(stat -L -c %s $(6) 2> /dev/null || echo 0) ]];                                                                   \
     then                                                                                                                          \
       $(EXIFTOOL) -overwrite_original_in_place -Title="gperftools $(3) Output for ($(GPERF_EXE_COMMAND)) Date/Time: [$(4)]" $(6); \
     fi;                                                                                                                           \
   fi
	@$(call check_pprof_results_file,$(6),$(3),$(7),$(2))
endef
else # } {
define perform_pprof
	@if [[ -e $(2) ]];                                              \
   then                                                           \
     $(PPROF) $(1) ./$(GPERF_EXE) $(GPERF_PROFILE) >| $(6) || : ; \
   fi
	@$(call check_pprof_results_file,$(6),$(3),$(7),$(2))
endef

endif # }

endif # }

################################################################################
# Checks the gperf heaps log file to see if any keaks were reported. If no     #
# leaks were reported, then the heap leaks dot graph will have a message       #
# indicating such.                                                             #
#                                                                              #
# $(1) - gperf heaps log file                                                  #
# $(2) - gperf executablefile                                                  #
# $(3) - gperf executable command                                              #
# $(4) - date/time                                                             #
# $(5) - gperf dot command                                                     #
# $(6) - gperf dot heap leaks command line options                             #
################################################################################
ifdef GPERF_DOT # {
define check_heap_leaks
	@grep -q "No leaks found for check" $(1);                                       \
   if [[ 0 == $${?} ]];                                                           \
   then                                                                           \
     echo 'graph';                                                                \
     echo '{';                                                                    \
     echo '  bgcolor="#C6CFD532";';                                               \
     echo '  ""[shape="box";color="red";label="No heap leaks found in $(2)";];';  \
     echo '  labelloc="t";';                                                      \
     echo '  label="gperftools Heap Leaks Output for ($(3)) Date/Time: [$(4)]";'; \
     echo '}';                                                                    \
   fi | $(5) $(6)
endef
else # } {
define check_heap_leaks
endef
endif # }

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than clean.                                                            #
################################################################################
export GPERF_CLEAN_TARGET ?= build_clean

DO_GPERF_CLEAN ?= 1

GPERF_PREREQUISITES=
ifeq ($(FORCE_RECOMPILE),YES) # {
GPERF_PREREQUISITES += $(GPERF_CLEAN_TARGET)
endif # }
ifeq ($(DO_GPERF_CLEAN),1) # {
GPERF_PREREQUISITES += gperf_clean
endif # }
GPERF_PREREQUISITES += gperf_prep

gheap gperf: GPERF_CXXFLAGS:=$(filter-out -Og -g -Ofast -O1 -O2 -O3 -O4 -O5 -O6,$(CXXFLAGS)) -Og -g
gperf: $(GPERF_PREREQUISITES)
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] FORCE_RECOMPILE    = [$(FORCE_RECOMPILE)]'
	@echo '[$(@)] GPERF_CLEAN_TARGET = [$(GPERF_CLEAN_TARGET)]'
	+@$(MAKE_NO_DIR) CXXFLAGS="$(GPERF_CXXFLAGS)" $(GPERF_EXE)
	@mkdir -p $(GPERF_OUTPUT)
	@echo 'TARGET                              = [$(@)]'                                   | tee    $(GPERF_LOG)
	@echo 'LD_PRELOAD                          = [$(LIB_PROFILER)]'                        | tee -a $(GPERF_LOG)
	@echo 'GPERF_PROFILE                       = [$(GPERF_PROFILE)]'                       | tee -a $(GPERF_LOG)
	@echo 'CPUPROFILE_FREQUENCY                = [$(CPUPROFILE_FREQUENCY)]'                | tee -a $(GPERF_LOG)
ifdef CPUPROFILE_REALTIME # {
	@echo 'ITIMER                              = [ITIMER_REAL]'                            | tee -a $(GPERF_LOG)
	@LD_PRELOAD=$(LIB_PROFILER) CPUPROFILE=$(GPERF_PROFILE) CPUPROFILE_FREQUENCY=$(CPUPROFILE_FREQUENCY) CPUPROFILE_REALTIME=$(CPUPROFILE_REALTIME) $(GPERF_EXE_COMMAND)
else # } {
	@echo 'ITIMER                              = [ITIMER_PROF]'                            | tee -a $(GPERF_LOG)
	@LD_PRELOAD=$(LIB_PROFILER) CPUPROFILE=$(GPERF_PROFILE) CPUPROFILE_FREQUENCY=$(CPUPROFILE_FREQUENCY) $(GPERF_EXE_COMMAND)
endif # }
ifneq ($(PPROF),) # {
	@$(call perform_pprof,$(PPROF_CPU_PROFILER_OPTS),$(GPERF_PROFILE),CPU Profiler,$(DATE_TIME),$(GPERF_DOT_CPU_PROFILER_OPTS),$(PPROF_CPU_PROFILER_FILE),$(GPERF_LOG))
	@$(call perform_pprof,$(PPROF_CALL_GRAPH_OPTS),$(GPERF_PROFILE),Call Graph,$(DATE_TIME),$(GPERF_DOT_CALL_GRAPH_OPTS),$(PPROF_CALL_GRAPH_FILE),$(GPERF_LOG))
ifeq ($(filter gperftools,$(MAKECMDGOALS)),) # {
	@$(call open_file_command,$(PPROF_CPU_PROFILER_FILE),$(BROWSER),gperf CPU profiler graph)
	@$(call open_file_command,$(PPROF_CALL_GRAPH_FILE),$(BROWSER),gperf call graph)
endif # }
else # } {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(PPROF_COMMAND)] was not found; no human readable output will be generated for target [$(@)].)
endif # }
ifeq ($(filter gperftools,$(MAKECMDGOALS)),) # {
	@$(call open_file_command,$(GPERF_LOG),$(EDITOR),gperftools CPU profiler/call graph log)
endif # }
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

GHEAP_PREREQUISITES=
ifeq ($(FORCE_RECOMPILE),YES) # {
GHEAP_PREREQUISITES += $(GPERF_CLEAN_TARGET)
endif # }
ifeq ($(DO_GPERF_CLEAN),1) # {
GHEAP_PREREQUISITES += gperf_clean
endif # }
GHEAP_PREREQUISITES += gperf_prep

gheap: $(GHEAP_PREREQUISITES)
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] FORCE_RECOMPILE    = [$(FORCE_RECOMPILE)]'
	@echo '[$(@)] GPERF_CLEAN_TARGET = [$(GPERF_CLEAN_TARGET)]'
	+@$(MAKE_NO_DIR) CXXFLAGS="$(GPERF_CXXFLAGS) -L$(dir $(LIB_TCMALLOC)) $(TCMALLOC)" $(GPERF_EXE)
	@mkdir -p $(GPERF_OUTPUT)
	@echo 'LIB_TCMALLOC                        = [$(LIB_TCMALLOC)]'                        | tee    $(GHEAP_LOG)
	@echo 'HEAPCHECK                           = [$(HEAPCHECK)]'                           | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_AFTER_DESTRUCTORS        = [$(HEAP_CHECK_AFTER_DESTRUCTORS)]'        | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_DUMP_DIRECTORY           = [$(HEAP_CHECK_DUMP_DIRECTORY)]'           | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_IDENTIFY_LEAKS           = [$(HEAP_CHECK_IDENTIFY_LEAKS)]'           | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_IGNORE_GLOBAL_LIVE       = [$(HEAP_CHECK_IGNORE_GLOBAL_LIVE)]'       | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_IGNORE_THREAD_LIVE       = [$(HEAP_CHECK_IGNORE_THREAD_LIVE)]'       | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_MAX_LEAKS                = [$(HEAP_CHECK_MAX_LEAKS)]'                | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_POINTER_SOURCE_ALIGNMENT = [$(HEAP_CHECK_POINTER_SOURCE_ALIGNMENT)]' | tee -a $(GHEAP_LOG)
	@echo 'HEAP_CHECK_TEST_POINTER_ALIGNMENT   = [$(HEAP_CHECK_TEST_POINTER_ALIGNMENT)]'   | tee -a $(GHEAP_LOG)
	@echo 'PPROF_PATH                          = [$(PPROF_PATH)]'                          | tee -a $(GHEAP_LOG)
	@echo 'TCMALLOC_RECLAIM_MEMORY             = [$(TCMALLOC_RECLAIM_MEMORY)]'             | tee -a $(GHEAP_LOG)
ifdef GPERF_DOT # {
	@$(HEAPCHECK_ENV_VARS) $(GPERF_EXE_COMMAND) 2>&1 || :                                                             | \
   sed '/^If the preceding .*, try running THIS shell command:$$/ { n; n; s@--gv$$@--dot                            | \
   sed '\''$$i\\ \ labelloc="t";'\''                                                                                | \
   sed '\''$$i\\ \ label="gperftools Heap Leak(s) Output for ($(GPERF_EXE_COMMAND)) Date/Time: [$(DATE_TIME)]";'\'' | \
   $(GPERF_DOT) $(GPERF_DOT_HEAP_LEAKS_OPTS)@; }'                                                                   | \
   tee -a $(GHEAP_LOG)
else # } {
	@$(HEAPCHECK_ENV_VARS) $(GPERF_EXE_COMMAND) 2>&1 || :                                                                                                          | \
   sed '/^If the preceding .*, try running THIS shell command:$$/ { n; n; s@--gv$$@--pdf >| $(PPROF_HEAP_LEAKS_FILE)@; }'                                        | tee -a $(GHEAP_LOG)
ifdef EXIFTOOL # {
	@echo '$(EXIFTOOL) -overwrite_original_in_place -Title="gperftools Heap Leaks Output for ($(GPERF_EXE_COMMAND)) Date/Time: [$(DATE_TIME)] $(HEAP_LEAKS_FILE)"' | tee -a $(GHEAP_LOG)
endif # }
endif # }
ifdef PPROF # {
	@HEAP_FILE=$$(ls -1 $(GPERF_OUTPUT)/$(GPERF_EXE)*.heap 2> /dev/null); \
   if [[ -n "$${HEAP_FILE}" ]];                                         \
   then                                                                 \
     mv $${HEAP_FILE} $(GPERF_HEAP_PROFILE);                            \
     sed -i "s@$${HEAP_FILE}@$(GPERF_HEAP_PROFILE)@" $(GHEAP_LOG);      \
   fi
	@$(call perform_pprof,$(PPROF_HEAP_LEAKS_OPTS),$(GPERF_HEAP_PROFILE),Heap Leaks,$(DATE_TIME),$(GPERF_DOT_HEAP_LEAKS_OPTS),$(PPROF_HEAP_LEAKS_FILE),$(GHEAP_LOG))
	@$(call check_heap_leaks,$(GHEAP_LOG),$(GPERF_EXE),$(GPERF_EXE_COMMAND),$(DATE_TIME),$(GPERF_DOT),$(GPERF_DOT_HEAP_LEAKS_OPTS))
ifeq ($(DO_GPERF_CLEAN),1) # {
	@$(call open_file_command,$(PPROF_HEAP_LEAKS_FILE),$(BROWSER),gperf heap leaks graph)
endif # }
else # } {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(PPROF_COMMAND)] was not found; no human readable output will be generated for target [$(@)].)
endif # }
ifeq ($(DO_GPERF_CLEAN),1) # {
	@$(call open_file_command,$(GHEAP_LOG),$(EDITOR),gperftools heap leaks log)
endif # }
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

gperftools: gperf
	+@$(MAKE_NO_DIR) DO_GPERF_CLEAN=0 gheap
	@$(call open_file_command,$(PPROF_CPU_PROFILER_FILE),$(BROWSER),gperf CPU profiler graph)
	@$(call open_file_command,$(PPROF_CALL_GRAPH_FILE),$(BROWSER),gperf call graph)
	@$(call open_file_command,$(GPERF_LOG),$(EDITOR),gperftools CPU profiler/call graph log)
	@$(call open_file_command,$(PPROF_HEAP_LEAKS_FILE),$(BROWSER),gperf heap leaks graph)
	@$(call open_file_command,$(GHEAP_LOG),$(EDITOR),gperftools heap leaks log)

gheap_prep gperf_prep::;

gheap_clean:
	@rm -f $(PPROF_HEAP_LEAKS_FILE) $(GHEAP_LOG) $(GPERF_OUTPUT)/*.heap

gperf_clean:
	@rm -rf $(GPERF_OUTPUT)

clean:: gperf_clean

.PHONY: gperf_mk_help
gperf_mk_help::
	@echo '$(MAKE) gheap → perform a gperftools heap leaks run'
##	@echo '$(MAKE) gperf → perform gperftools CPU profiler and call graph runs'
##	@echo '$(MAKE) gperftools → perform gperftools CPU profiler, call graph, and heap leaks runs'

show_help:: gperf_mk_help

platform_info::
	@echo '$(PPROF_COMMAND) version    = [$(PPROF_VERSION)]'
	@echo '$(EXIFTOOL_COMMAND) version = [$(EXIFTOOL_VERSION)]'

endif # }

endif # }

