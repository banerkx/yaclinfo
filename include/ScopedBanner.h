
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <string>

class ScopedBanner
{
  public:

    explicit ScopedBanner(const std::string& sBanner) : m_sBanner(sBanner)
    {
      std::cout << "========== BEGIN " << m_sBanner << " ==========" << std::endl;
    }

    ~ScopedBanner()
    {
      std::cout << "==========  END  " << m_sBanner << " ==========" << std::endl;
    }

  private:

    const std::string m_sBanner;

    // Intentionally deleted.
    ScopedBanner()                        = delete;
    ScopedBanner(const ScopedBanner& rhs) = delete;
    ScopedBanner(ScopedBanner&& rhs)      = delete;
    ScopedBanner&       operator=(const ScopedBanner& rhs) = delete;
    ScopedBanner&       operator=(ScopedBanner&& rhs) = delete;
    ScopedBanner*       operator&()                   = delete;
    const ScopedBanner* operator&() const             = delete;
};

