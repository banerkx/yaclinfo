
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of clinfo.
 *
 *  clinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  clinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with clinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ScopedChronoTimer.h"

#include <thread>

ScopedChronoTimer::ScopedChronoTimer(long double& fElapsedWallTime, long double& fElapsedCPUTime) :
  m_fElapsedWallTime(fElapsedWallTime),
  m_fElapsedCPUTime(fElapsedCPUTime),
  m_tStartWallTime(std::chrono::high_resolution_clock::now()),
  m_nClockTicks(clock())
{
}

ScopedChronoTimer::~ScopedChronoTimer()
{
  m_fElapsedWallTime = elapsedWallTime();
  m_fElapsedCPUTime  = elapsedCPUTime();
}

long double ScopedChronoTimer::avgCPUTime(const long double fTime)
{
  return (fTime / static_cast<long double>(std::thread::hardware_concurrency()));
}

long double ScopedChronoTimer::elapsedWallTime() const
{
  // Computing the elapsed wall time in seconds.
  auto tElapsedTime = std::chrono::duration_cast<std::chrono::nanoseconds>(
    std::chrono::high_resolution_clock::now() - m_tStartWallTime);
  return ((static_cast<long double>(tElapsedTime.count()) * 1.0e-9));
}

long double ScopedChronoTimer::elapsedCPUTime() const
{
  return (static_cast<double>(clock() - m_nClockTicks) / static_cast<double>(CLOCKS_PER_SEC));
}

