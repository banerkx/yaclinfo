
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "DeviceInfo.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 100
#include <CL/opencl.h>

class DeviceInfo_100 : public DeviceInfo
{
  public:

    DeviceInfo_100(
      const cl_device_id   nDeviceID,
      const size_t         nDeviceNumber,
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nDeviceVersion = 100) noexcept;

    virtual ~DeviceInfo_100() noexcept override;

    virtual void getTextOutput(std::ostream& tStream) const noexcept override;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept override;

  protected:

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_fp_config,
      OpenCLError,
      OpenCLToolTip>
      m_tHalfPrecisionFloatingPointConfiguration;
    std::
      tuple<const OPEN_cl_device_info_100, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
        m_tMaxSimultaneousImageObjectsWrite;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_int, OpenCLError, OpenCLToolTip>
      m_tMinDataTypeAlignSize;
    std::tuple<
      const OPEN_cl_device_info_100,
      std::string,
      size_t,
      cl_command_queue_properties,
      OpenCLError,
      OpenCLToolTip>
      m_tQueueProperties;

    virtual void getAllDeviceInfo() noexcept override;

  private:

    // Intentionally deleted.
    DeviceInfo_100()                          = delete;
    DeviceInfo_100(const DeviceInfo_100& rhs) = delete;
    DeviceInfo_100(DeviceInfo_100&& rhs)      = delete;
    DeviceInfo_100&       operator=(const DeviceInfo_100& rhs) = delete;
    DeviceInfo_100&       operator=(DeviceInfo_100&& rhs) = delete;
    DeviceInfo_100*       operator&()                     = delete;
    const DeviceInfo_100* operator&() const               = delete;
};

