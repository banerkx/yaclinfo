
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

// Example use:
//  class MySingleton : public (Const)SingletonBase<MySingleton>
//  {
//    public:
//
//      // public methods ...
//      // public data members ...
//
//      virtual ~MySingleton()
//      {
//        .
//        .
//        .
//      }
//
//    private:
//
//      MySingleton() :
//        (Const)SingletonBase<MySingleton>(),
//        .
//        .
//        .
//      {
//        ...
//      }
//
//      // private methods ...
//      // private data members ...
//
//      friend class (Const)SingletonBase<MySingleton>;
//
//      // Intentionally deleted.
//      MySingleton(const MySingleton& rhs)            = delete;
//      MySingleton(MySingleton&& rhs)                 = delete;
//      MySingleton& operator=(const MySingleton& rhs) = delete;
//      MySingleton& operator=(MySingleton&& rhs)      = delete;
//      MySingleton* operator&()                       = delete;
//      const MySingleton* operator&() const           = delete;
//  };

template<class T> class SingletonBase
{
  public:

    /**
     * getInstance
     * @brief returns a reference to an instance of type T
     * @return
     */
    static T& getInstance()
    {
      static T tInstance;
      return (tInstance);
    }

    /**
     * ~SingletonBase
     * @brief default destructor
     */
    virtual ~SingletonBase() = default;

  protected:

    /**
     * SingletonBase
     * @brief default constructor
     */
    SingletonBase() = default;

  private:

    // Intentionally deleted.
    SingletonBase(const SingletonBase& rhs) = delete;
    SingletonBase(SingletonBase&& rhs)      = delete;
    SingletonBase&       operator=(const SingletonBase& rhs) = delete;
    SingletonBase&       operator=(SingletonBase&& rhs) = delete;
    SingletonBase*       operator&()                    = delete;
    const SingletonBase* operator&() const              = delete;
};

template<class T> class ConstSingletonBase
{
  public:

    /**
     * getInstance
     * @brief returns a const reference to an instance of type T
     * @return
     */
    static const T& getInstance()
    {
      static const T tInstance;
      return (tInstance);
    }

    /**
     * ~ConstSingletonBase
     * @brief default destructor
     */
    virtual ~ConstSingletonBase() = default;

  protected:

    /**
     * ConstSingletonBase
     * @brief default constructor
     */
    ConstSingletonBase() = default;

  private:

    // Intentionally deleted.
    ConstSingletonBase(const ConstSingletonBase& rhs) = delete;
    ConstSingletonBase(ConstSingletonBase&& rhs)      = delete;
    ConstSingletonBase&       operator=(const ConstSingletonBase& rhs) = delete;
    ConstSingletonBase&       operator=(ConstSingletonBase&& rhs) = delete;
    ConstSingletonBase*       operator&()                         = delete;
    const ConstSingletonBase* operator&() const                   = delete;
};

#define SINGLETON(T)      \
  static T& getInstance() \
  {                       \
    static T tInstance;   \
    return (tInstance);   \
  }
#define CONST_SINGLETON(T)      \
  static const T& getInstance() \
  {                             \
    static const T tInstance;   \
    return (tInstance);         \
  }

