
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <boost/tokenizer.hpp>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

class StringTokenizer
{
  public:

    constexpr static const char* WHITE_SPACE = "\n\t ";

    /**
     * Default constructor.
     */
    StringTokenizer() noexcept;

    /**
     * Constructor.
     * @param inString - the string to be tokenized
     * @param inDelimiters - the set of delimiters
     * @throws No exceptions are thrown.
     */
    StringTokenizer(const std::string& inString, const std::string& inDelimiters) noexcept;

    /**
     * Constructor.
     * @param inString - the string to be tokenized
     * @param inDelimiter - the delimiter character
     * @throws No exceptions are thrown.
     */
    explicit StringTokenizer(const std::string& inString, const char inDelimiter = ' ') noexcept;

    /**
     * Copy constructor.
     * @param rhs - the object to be copied
     */
    StringTokenizer(const StringTokenizer& rhs) noexcept;

    /**
     * Move copy constructor.
     * @param rhs - the object to be copied
     */
    StringTokenizer(StringTokenizer&& rhs) noexcept;

    /**
     * Overloaded assignment operator.
     * @param rhs - the StringTokenizer object to be assigned.
     * @return The current instance has its data members assigned and
     * then it's returned.
     * @throws No exceptions are thrown.
     */
    StringTokenizer& operator=(const StringTokenizer& rhs) noexcept;

    /**
     * Overloaded move assignment operator.
     * @param rhs - the StringTokenizer object to be assigned.
     * @return The current instance has its data members assigned and
     * then it's returned.
     * @throws No exceptions are thrown.
     */
    StringTokenizer& operator=(StringTokenizer&& rhs) noexcept;

    /**
     * Destructor.
     * @throws No exceptions are thrown.
     */
    virtual ~StringTokenizer() noexcept
    {
      m_tTokens.clear();
      m_sString.clear();
      m_sDelimiters.clear();
    }

    /**
     * Returns the number of tokens.
     * @return number of tokens
     */
    inline size_t size() const
    {
      return (m_tTokens.size());
    }

    /**
     * Prints out the tokens.
     * @param tStream - the stream to which the tokens are printed.
     * @return The tokens are printed to the passed in stream.
     * @throws No exceptions are thrown.
     */
    std::ostream& printTokens(std::ostream& tStream = std::cout) const noexcept;

    /**
     * Resets the string to be tokenized to the input string. Then this new
     * string is tokenized.
     * @param sNewString - new string
     * @throws No exceptions are thrown.
     */
    void reset(const std::string& sNewString) noexcept;

    /**
     * Resets the string to be tokenized to the input string using the new input
     * delimiters.
     * @param sNewString - new string
     * @param sNewDelimiters - new delimiters
     * @throws No exceptions are thrown.
     */
    void reset(const std::string& sNewString, const std::string& sNewDelimiters) noexcept;

    /**
     * Resets the string to be tokenized to the input string using the new input
     * delimiter.
     * @param sNewString - new string
     * @param cNewDelimiter - new delimiter
     * @throws No exceptions are thrown.
     */
    void reset(const std::string& sNewString, const char cNewDelimiter) noexcept;

    /**
     * Overloaded extraction operator.
     * @param tStream - the output stream
     * @param tTokenizer - the StringTokenizer object
     * @return The tokens of the StringTokenizer object are sent to the output
     * stream.
     * @throws No exceptions are thrown.
     */
    friend std::ostream& operator<<(
      std::ostream&          tStream,
      const StringTokenizer& tTokenizer) noexcept;

    /**
     * get the string that was originally parsed
     * @return the string that was originally parsed
     */
    inline const std::string& getString() const
    {
      return (m_sString);
    }

    /**
     * Accessor for m_sDelimiters.
     * @return the string of delimiters
     */
    inline const std::string& getDelimiters() const
    {
      return (m_sDelimiters);
    }

    /**
     * Iterator to the beginning of the tokens.
     * @return m_tTokens.begin()
     */
    std::vector<std::string>::iterator begin()
    {
      return (m_tTokens.begin());
    }

    /**
     * Constant iterator to the beginning of the tokens.
     * @return m_tTokens.begin()
     */
    std::vector<std::string>::const_iterator begin() const
    {
      return (m_tTokens.begin());
    }

    /**
     * Iterator to one past the last token.
     * @return m_tTokens.end()
     */
    std::vector<std::string>::iterator end()
    {
      return (m_tTokens.end());
    }

    /**
     * Constant iterator to one past the last token.
     * @return m_tTokens.end()
     */
    std::vector<std::string>::const_iterator end() const
    {
      return (m_tTokens.end());
    }

    /**
     * Reverse iterator to the end of the tokens.
     * @return m_tTokens.rbegin()
     */
    std::vector<std::string>::reverse_iterator rbegin()
    {
      return (m_tTokens.rbegin());
    }

    /**
     * Constant reverse iterator to the end of the tokens.
     * @return m_tTokens.rbegin()
     */
    std::vector<std::string>::const_reverse_iterator rbegin() const
    {
      return (m_tTokens.rbegin());
    }

    /**
     * Reverse iterator to the beginning of the tokens.
     * @return m_tTokens.rend()
     */
    std::vector<std::string>::reverse_iterator rend()
    {
      return (m_tTokens.rend());
    }

    /**
     * Constant reverse iterator to the beginning of the tokens.
     * @return m_tTokens.rend()
     */
    std::vector<std::string>::const_reverse_iterator rend() const
    {
      return (m_tTokens.rend());
    }

    /**
     * Accessor for token at the specified index.
     * @param nIndex - the specified index
     * @return token at the specified index
     * @throws out_of_range if nIndex is out of bounds.
     */
    std::string getToken(const size_t nIndex);

    /**
     * Accessor for token at the specified index.
     * @param nIndex - the specified index
     * @return token at the specified index
     * @throws out_of_range if nIndex is out of bounds.
     */
    std::string operator[](const size_t nIndex);

    size_t getMaxTokenLength() const;

    // NOTE: No range checking!!
    const std::string& at(const size_t nIndex);

    /**
     * trim
     * @brief trims leading and trailing whitespace from all tokens.
     */
    void trim();

  private:

    std::vector<std::string> m_tTokens;
    std::string              m_sString;
    std::string              m_sDelimiters;

    /**
     * Tokenizes m_sString.
     * @throws No exceptions are thrown.
     */
    void tokenize() noexcept;

    /**
     * Removes leading and trailing white space characters.
     * @param s - the input string
     */
    static void trimWhiteSpace(std::string& s);
};

