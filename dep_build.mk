
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# This makefile can be included to generate a dependency graph diagram that    #
# shows the relationships between targets and their prerequisites.             #
#                                                                              #
# Example uses:                                                                #
#   make dot_$(EXE)                                                            #
#   make dep_$(EXE)                                                            #
#                                                                              #
# NOTE: There is a quirk. If the command executed is "make dot_all" or         #
#       "make dep_all" and the recipe body of the all target is empty, then    #
#       the dot or dependency file will not be properly created; the recipe    #
#       body must have at least one statement. For example, changing           #
#                                                                              #
#         all: $(EXE)                                                          #
#                                                                              #
#       to:                                                                    #
#                                                                              #
#         all: $(EXE)                                                          #
#         	@:                                                                 #
#                                                                              #
#       will suffice to create a proper dot or dependency file with the        #
#       command "make dot_all" or "make dep_all", respectively. The hypothesis #
#       for this quirk is that in an empty recipe body, the automatic variable #
#       $(@) is empty or undefined.                                            #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef DEP_BUILD_MK_INCLUDE_GUARD # {
DEP_BUILD_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/graph_viz.mk.    #
################################################################################
ifeq ($(GRAPH_VIZ_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/graph_viz.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

ifeq ($(DOT),) # {
$(error $(ERROR_LABEL) The [$(DOT_COMMAND)] was not found; no makefile dependency graph can be generated)
endif # }

################################################################################
# dot_file holds the name of the dot graph output file.                        #
################################################################################
DEP_FILE_STUB ?=
ifndef DOT_FILE_STUB # {
ifdef EXE # {
DEP_FILE_STUB=$(EXE)
else # } {
DEP_FILE_STUB=$(notdir $(CURDIR))
endif # }
endif # }
FILE_PREFIX=build
dot_file:=$(FILE_PREFIX)_$(DEP_FILE_STUB).$(DOT_FILE_EXTENSION)

################################################################################
# DOT_CLEAN_TARGET holds the name of the target that should be made to do a    #
# "clean" in order to get the full build. Set this variable before including   #
# this make file.                                                              #
################################################################################
export DOT_CLEAN_TARGET ?= build_clean

################################################################################
# dep_file holds the name of the file that has the dependency graph.           #
################################################################################
dep_file:=$(dot_file:.$(DOT_FILE_EXTENSION)=.$(DOT_IMAGE_EXTENSION))
DOT_OPTS=-T$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER) -o $(dep_file)
##DOT_OPTS += -v
BACK_GROUND_COLOR=\#C6CFD532

cpp_color=crimson
dependency_color=blue
directory_color=steelblue
exec_color=darkgreen
gawk_color=goldenrod
header_color=black
library_color=saddlebrown
misc_color=indigo
object_color=darkslategray

default_node_color=$(misc_color)
default_edge_color=$(misc_color)

################################################################################
# Adds colors to the graph's nodes and edges.                                  #
#                                                                              #
# $(1) - dot file                                                              #
################################################################################
define color_nodes_and_edges
################################################################################
# The following sed commands color the nodes of the graph.                     #
################################################################################
	@sed -i 's/\(^.*$(DEP_FILE_STUB)"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(exec_color)";\3/'                                 $(1)
	@sed -i 's/\(^.*\.awk"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(gawk_color)";\3/'                                            $(1)
	@sed -i 's/\(^.*\.cpp"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(cpp_color)";\3/'                                             $(1)
	@sed -i 's/\(^.*\.d"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(dependency_color)";\3/'                                        $(1)
	@sed -i 's/\(^.*\.h"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(header_color)";\3/'                                            $(1)
	@sed -i 's@\(^.*lib.*\.a"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(library_color)";\3@'                                      $(1)
	@sed -i 's@\(^.*lib.*\.so.*"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(library_color)";\3@'                                   $(1)
	@sed -i 's/\(^.*\.o"\[color=\)\("$(default_node_color)";\)\(.*\)/\1"$(object_color)";\3/'                                            $(1)
	@$(if $(DEPENDS), sed -i 's@\(^.*\$(DEPENDS)"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(directory_color)";\3@'               $(1))
	@$(if $(LOCAL_LIB_DIR), sed -i 's@\(^.*\$(LOCAL_LIB_DIR)"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(directory_color)";\3@'   $(1))
	@$(if $(OBJECTS_DIR), sed -i 's@\(^.*\$(OBJECTS_DIR)"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(directory_color)";\3@'       $(1))
	@$(if $(SO_OBJECTS_DIR), sed -i 's@\(^.*\$(SO_OBJECTS_DIR)"\[color=\)\("$(default_node_color)";\)\(.*\)@\1"$(directory_color)";\3@' $(1))
################################################################################
# The following sed commands color the edges of the graph.                     #
################################################################################
	@sed -i 's/\(^.* -> "$(DEP_FILE_STUB)"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(exec_color)";\3\4/'                                $(1)
	@sed -i 's/\(^.* -> ".*\.awk"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(gawk_color)";\3\4/'                                         $(1)
	@sed -i 's/\(^.* -> ".*\.cpp"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(cpp_color)";\3\4/'                                          $(1)
	@sed -i 's/\(^.* -> ".*\.d"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(dependency_color)";\3\4/'                                     $(1)
	@sed -i 's/\(^.* -> ".*\.h"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(header_color)";\3\4/'                                         $(1)
	@sed -i 's@\(^.* -> ".*lib.*\.a"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(library_color)";\3\4@'                                   $(1)
	@sed -i 's@\(^.* -> ".*lib.*\.so.*"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(library_color)";\3\4@'                                $(1)
	@sed -i 's/\(^.* -> ".*\.o"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)/\1"$(object_color)";\3\4/'                                         $(1)
	@$(if $(DEPENDS), sed -i 's@\(^.* -> "$(DEPENDS)"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(directory_color)";\3\4@'               $(1))
	@$(if $(LOCAL_LIB_DIR), sed -i 's@\(^.* -> "$(LOCAL_LIB_DIR)"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(directory_color)";\3\4@'   $(1))
	@$(if $(OBJECTS_DIR), sed -i 's@\(^.* -> "$(OBJECTS_DIR)"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(directory_color)";\3\4@'       $(1))
	@$(if $(SO_OBJECTS_DIR), sed -i 's@\(^.* -> "$(SO_OBJECTS_DIR)"\[color=\)\("$(default_edge_color)";\)\(.*\)\(\];$$\)@\1"$(directory_color)";\3\4@' $(1))
endef

################################################################################
# Groups the nodes in the dot file into ranks (for a better organized          #
# display).                                                                    #
#                                                                              #
# ${1} - dot file                                                              #
################################################################################
ifneq ($(LOCAL_LIB_DIR),) # {
define rank_nodes
	@sed -i '/^}$$/d'                                 $(1)
	@{ sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.awk"'           | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // gawk files\n\    rank="same";'                         | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.h"'             | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // header files\n\    rank="same";'                       | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.cpp"'           | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // implementation files\n\    rank="same";'               | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(DEPENDS)/.*\.d"'     | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // dependency files\n\    rank="same";'                   | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(OBJECTS_DIR)/.*\.o"' | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // object files\n\    rank="same";'                       | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) | egrep '^  ".*lib.*\.(a|so.*)"'    | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // library files\n\    rank="same";'                      | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) | egrep '^  ".*($(LOCAL_LIB_DIR)|$(OBJECTS_DIR)|$(DEPENDS))"' | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // directories\n\    rank="same";' | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(MAIN)"'              | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // $(MAIN)\n\    rank="same";'                            | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(EXE)"'               | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // executable\n\    rank="same";'                         | sed '$$a\  }'; } >> $(1)
	@echo '}' >> $(1)
endef
else # } {
################################################################################
# NOTE: If $(LOCAL_LIB_DIR) is empty, then no libraries, no objects, and no    #
#       dependency files were created. Therefore, we ignore such files when    #
#       creating the dot graph ranks.                                          #
################################################################################
define rank_nodes
	@sed -i '/^}$$/d'                                 $(1)
	@{ sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.awk"'           | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // gawk files\n\    rank="same";'                         | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.h"'             | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // header files\n\    rank="same";'                       | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  ".*/.*\.cpp"'           | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // implementation files\n\    rank="same";'               | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(MAIN)"'              | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // $(MAIN)\n\    rank="same";'                            | sed '$$a\  }'; \
     sed 's/ -> /\n/;s/^ */  /;s/ *$$//;s/\[.*$$//' $(1) |  grep '^  "$(EXE)"'               | sort -u | sed '$$!s/$$/,/;s/^/  /;1i\  {\n\    // executable\n\    rank="same";'                         | sed '$$a\  }'; } >> $(1)
	@echo '}' >> $(1)
endef
endif # }

DEP_EXTERNAL_TARGETS=clean show_help dep_build_mk_help vars print-% qprint-%
DEP_TARGETS=dot_% dep_%
ifneq ($(filter $(DEP_TARGETS) $(DEP_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

################################################################################
# "SHELL" hack:                                                                #
#   GNU make will expand the value of $(SHELL) for every rule that is run in   #
#   the makefile. When $(SHELL) is expanded, the per rule automatic variables, #
#   e.g., $(@), have been set. Therefore, by modifying SHELL, we can perform   #
#   a particular task for every rule as it runs.                               #
#                                                                              #
# dep_old_shell holds the original value of $(SHELL) and then we set $(SHELL)  #
# to be the expansion of $(dot_run). Since the "return" value of $(dot_run) is #
# empty, the effect is that dot_run is expanded for each rule without          #
# affecting the actual $(SHELL) use.                                           #
################################################################################
dep_old_shell:=$(SHELL)

BUILD_DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
GRAPH_LABEL=$(call get_graph_label,$(DOT_LABEL_FONT_SIZE)$(DEP_FILE_STUB) Build Dependency Graph,$(BUILD_DATE_TIME))

DOT_SIZE=20,20

DOT_FILE_COMMAND=// $(DOT) $(DOT_OPTS) $(dot_file)

################################################################################
# If the dot image format is svg, then we want to beautify the svg XML file    #
# with xmllint.                                                                #
################################################################################
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
ifneq ($(XMLLINT),) # {
DOT_XMLLINT_COMMAND:=&& $(call get_dot_xmllint_command,$(dep_file))
DOT_FILE_COMMAND += $(DOT_XMLLINT_COMMAND)
endif # }
endif # }
DOT_FILE_COMMAND:=$(DOT_FILE_COMMAND)

################################################################################
# Pattern rule to handle each dot_* target. Since the prerequisite of dot_% is #
# %, this pattern rule has the effect of building the target matching % and    #
# then building dot_%.                                                         #
################################################################################
.PHONY: dot_%
dot_%: SHELL=$(dot_run)$(dep_old_shell)
dot_%: DOT_RANK_DIR=LR
dot_%: %
	@$(shell rm -f $(dot_file))$(call dot_dump,$(*))
	+@if [[ -r $(dot_file) ]];                                                                                         \
   then                                                                                                              \
     sed -i '1s/^/  graph[fontname="$(DOT_GRAPH_FONT)";colorscheme="$(DOT_COLOR_SCHEME)";];\n/'         $(dot_file); \
     sed -i '1s/^/  edge[fontname="$(DOT_EDGE_FONT)";];\n/'                                             $(dot_file); \
     sed -i '1s/^/  node[fontname="$(DOT_NODE_FONT)";];\n/'                                             $(dot_file); \
     sed -i '1s/^/  bgcolor=\"$(BACK_GROUND_COLOR)\";\n/'                                               $(dot_file); \
     sed -i '1s/^/  size="$(DOT_SIZE)";\n/'                                                             $(dot_file); \
     sed -i '1s/^/  splines="$(DOT_SPLINE)";\n/'                                                        $(dot_file); \
     sed -i '1s/^/  rankdir="$(DOT_RANK_DIR)";\n/'                                                      $(dot_file); \
     sed -i '1s/^/{\n/'                                                                                 $(dot_file); \
     sed -i '1s/^/digraph $(DEP_FILE_STUB)_Build_Dependency_Graph\n/'                                   $(dot_file); \
     sed -i '1s@^@\n$(subst &,\&,$(DOT_FILE_COMMAND))\n\n@'                                             $(dot_file); \
     echo '  labelloc="t";'                                                                          >> $(dot_file); \
     echo '  label=$(GRAPH_LABEL)'                                                                   >> $(dot_file); \
     echo '}'                                                                                        >> $(dot_file); \
     echo ''                                                                                         >> $(dot_file); \
     $(MAKE) $(NO_DIR) dot_extra_$(*);                                                                               \
   else                                                                                                              \
     echo '$(ERROR_LABEL) The file [$(dot_file)] was not created; may need to perform a $(DOT_CLEAN_TARGET) first.'; \
     exit 1;                                                                                                         \
   fi
	@$(call add_node_degrees,$(dot_file))
	@$(call add_nodes_edges_cardinalities,$(dot_file))
	@sed -i "s@${HOME}@\$${HOME}@g" $(dot_file)
	@$(if $(EDITOR),$(call open_sole_target_file,$(@),$(dot_file),$(EDITOR),$(DEP_FILE_STUB) dot graph definition))

.PHONY: dot_extra_%
dot_extra_%::
	@:

################################################################################
# Pattern rule to first perform a $(DOT_CLEAN_TARGET) and then to create the   #
# corresponding dependency dot file $(dot_file).                               #
################################################################################
.PHONY: dot_clean_%
dot_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dot_$(*)

################################################################################
# Pattern rule to group nodes by rank.                                         #
################################################################################
OLD_EDITOR:=$(EDITOR)
.PHONY: dot_rank_%
dot_rank_%: EDITOR=
dot_rank_%: dot_%
	@$(call rank_nodes,$(dot_file))
	+@$(MAKE_NO_DIR) dot_extra_rank_$(*)
	@$(if $(OLD_EDITOR),$(call open_sole_target_file,$(@),$(dot_file),$(OLD_EDITOR),$(DEP_FILE_STUB) dot graph definition))

.PHONY: dot_extra_rank_%
dot_extra_rank_%::
	@:

.PHONY: dot_rank_clean_%
dot_rank_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dot_rank_$(*)

################################################################################
# Experimental target to color nodes and edges by file type.                   #
################################################################################
.PHONY: dot_colors_%
dot_colors_%: dot_%
	@$(call color_nodes_and_edges,$(dot_file))
	+@$(MAKE_NO_DIR) dot_extra_colors_$(*)
	@$(if $(EDITOR),$(call open_sole_target_file,$(@),$(dot_file),$(EDITOR),$(DEP_FILE_STUB) dot graph definition))

.PHONY: dot_extra_colors_%
dot_extra_colors_%::
	@:

.PHONY: dot_colors_clean_%
dot_colors_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dot_colors_$(*)

################################################################################
# Pattern rule to color nodes and edges.                                       #
################################################################################
.PHONY: dot_rank_colors_%
dot_rank_colors_%: dot_rank_%
	@$(call color_nodes_and_edges,$(dot_file))
	+@$(MAKE_NO_DIR) dot_extra_colors_$(*)
	@$(if $(OLD_EDITOR),$(call open_sole_target_file,$(@),$(dot_file),$(OLD_EDITOR),$(DEP_FILE_STUB) dot graph definition))

.PHONY: dot_rank_colors_clean_%
dot_rank_colors_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dot_rank_colors_$(*)

################################################################################
# Pattern rule to create the corresponding dependency graph diagram for        #
# $(dot_file).                                                                 #
################################################################################
.PHONY: dep_%
################################################################################
# Example of intermediate file (which gets automatically deleted):             #
#   - use the special target .INTERMEDIATE to specify the intermediate         #
#     files                                                                    #
#   - ensure that the intermediate files are actual targets in the makefile    #
#   - ensure that the intermediate files are prerequisites for other targets   #
################################################################################
.PHONY: $(dot_file)
.INTERMEDIATE: $(dot_file)
$(dot_file):;

################################################################################
# Adds a node of the specified color to the output file.                       #
#                                                                              #
# $(1) - color                                                                 #
# $(2) - node type                                                             #
# $(3) - output file                                                           #
################################################################################
define add_color_node
	@echo '  "$(1)"[color="$(1)";penwidth="$(DOT_PEN_WIDTH)";label=<[$(1)] $(2)>];' >> $(3)
endef

dot_colors_file=dot_colors.$(DOT_FILE_EXTENSION)
dot_colors_$(DOT_IMAGE_EXTENSION)=$(dot_colors_file:.$(DOT_FILE_EXTENSION)=.$(DOT_IMAGE_EXTENSION))
dot_colors_$(DOT_IMAGE_EXTENSION)=$(dot_colors_file:.$(DOT_FILE_EXTENSION)=.$(DOT_IMAGE_EXTENSION))
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
DOT_COLORS_XMLLINT_COMMAND:=&& $(notdir $(XMLLINT)) --noout $(dot_colors_$(DOT_IMAGE_EXTENSION)) && $(notdir $(XMLLINT)) --output $(dot_colors_$(DOT_IMAGE_EXTENSION)) --format $(dot_colors_$(DOT_IMAGE_EXTENSION))
endif # }
DOT_COLORS_COMMAND=// $(notdir $(DOT)) -T$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER) -o $(dot_colors_$(DOT_IMAGE_EXTENSION)) $(dot_colors_file) $(DOT_COLORS_XMLLINT_COMMAND) && $(BROWSER) $(dot_colors_$(DOT_IMAGE_EXTENSION))
.PHONY: dot_colors
dot_colors: DOT_RANK_DIR=LR
dot_colors:
	@rm -f $(dot_colors_file) $(dot_colors_$(DOT_IMAGE_EXTENSION))
	@echo -e '$(DOT_COLORS_COMMAND)'                                                                       >> $(dot_colors_file)
	@echo 'graph dot_colors'                                                                               >> $(dot_colors_file)
	@echo '{'                                                                                              >> $(dot_colors_file)
	@echo '  rankdir="$(DOT_RANK_DIR)";'                                                                    >> $(dot_colors_file)
	@echo '  bgcolor="#C6CFD532";'                                                                         >> $(dot_colors_file)
	@echo '  node[fontname="$(DOT_NODE_FONT)";];'                                                          >> $(dot_colors_file)
	@echo '  edge[fontname="$(DOT_EDGE_FONT)";];'                                                          >> $(dot_colors_file)
	@echo '  graph[fontname="$(DOT_GRAPH_FONT)";colorscheme="$(DOT_COLOR_SCHEME)";];'                      >> $(dot_colors_file)
	@$(call add_color_node,$(object_color),*.o file,$(dot_colors_file))
	@$(call add_color_node,$(misc_color),misc file,$(dot_colors_file))
	@$(call add_color_node,$(library_color),library file,$(dot_colors_file))
	@$(call add_color_node,$(header_color),*.h file,$(dot_colors_file))
	@$(call add_color_node,$(gawk_color),gawk file,$(dot_colors_file))
	@$(call add_color_node,$(exec_color),exec file,$(dot_colors_file))
	@$(call add_color_node,$(directory_color),directory,$(dot_colors_file))
	@$(call add_color_node,$(dependency_color),*.d file,$(dot_colors_file))
	@$(call add_color_node,$(cpp_color),*.cpp file,$(dot_colors_file))
	@echo '  labelloc="t";'                                                                                >> $(dot_colors_file)
	@echo '  label=<<FONT POINT-SIZE="20">dot $(DOT_IMAGE_EXTENSION) Colors [$(BUILD_DATE_TIME)]</FONT>>;' >> $(dot_colors_file)
	@echo '}'                                                                                              >> $(dot_colors_file)
	@echo ''                                                                                               >> $(dot_colors_file)
	@$(DOT) -T$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER) -o $(dot_colors_$(DOT_IMAGE_EXTENSION)) $(dot_colors_file)
	@$(call open_file_command,$(dot_colors_file),$(EDITOR),dot color scheme)
	@$(call open_file_command,$(dot_colors_$(DOT_IMAGE_EXTENSION)),$(BROWSER),dot color scheme diagram)

dot_build_clean::
	@rm -f $(dot_colors_file) $(dot_colors_$(DOT_IMAGE_EXTENSION))

.PHONY: dep_%
dep_%: dot_% $(dot_file)
	@rm -f $(dep_file)
	@$(DOT) $(DOT_OPTS) $(dot_file) $(DOT_XMLLINT_COMMAND)
	@$(call open_file_command,$(dep_file),$(BROWSER),$(DEP_FILE_STUB) dependency graph diagram)

################################################################################
# Pattern rule to first perform a $(DOT_CLEAN_TARGET) and then to create the   #
# corresponding dependency graph diagram from $(dot_file).                     #
################################################################################
.PHONY: dep_clean_%
dep_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dep_$(*)

################################################################################
# Pattern rule to create the corresponding dependency graph diagram for        #
# $(dot_file), grouping nodes by rank.                                         #
################################################################################
.PHONY: dep_rank_%
dep_rank_%: dot_rank_% $(dot_file)
	@rm -f $(dep_file)
	@$(DOT) $(DOT_OPTS) $(dot_file) $(DOT_XMLLINT_COMMAND)
	@$(call open_file_command,$(dep_file),$(BROWSER),$(DEP_FILE_STUB) dependency graph diagram)

################################################################################
# Pattern rule to first perform a $(DOT_CLEAN_TARGET) and then to create the   #
# corresponding dependency graph diagram from $(dot_file), grouping nodes by   #
# rank.                                                                        #
################################################################################
.PHONY: dep_rank_clean_%
dep_rank_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dep_rank_$(*)

################################################################################
# Pattern rule to create the corresponding dependency graph diagram for        #
# $(dot_file), coloring nodes and edges.                                       #
################################################################################
.PHONY: dep_colors_%
dep_colors_%: dot_colors_% $(dot_file)
	@rm -f $(dep_file)
	@$(DOT) $(DOT_OPTS) $(dot_file) $(DOT_XMLLINT_COMMAND)
	@$(call open_file_command,$(dep_file),$(BROWSER),$(DEP_FILE_STUB) dependency graph diagram)

################################################################################
# Pattern rule to first perform a $(DOT_CLEAN_TARGET) and then to create the   #
# corresponding dependency graph diagram from $(dot_file), coloring nodes and  #
# edges.                                                                       #
################################################################################
.PHONY: dep_colors_clean_%
dep_colors_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dep_colors_$(*)

################################################################################
# Pattern rule to create the corresponding dependency graph diagram for        #
# $(dot_file), grouping nodes by rank and with colored nodes and edges.        #
################################################################################
.PHONY: dep_rank_colors_%
dep_rank_colors_%: dot_rank_colors_% $(dot_file)
	@rm -f $(dep_file)
	@$(DOT) $(DOT_OPTS) $(dot_file) $(DOT_XMLLINT_COMMAND)
	@$(call open_file_command,$(dep_file),$(BROWSER),$(DEP_FILE_STUB) dependency graph diagram)

################################################################################
# Pattern rule to first perform a $(DOT_CLEAN_TARGET) and then to create the   #
# corresponding dependency graph diagram from $(dot_file), grouping nodes by   #
# rank and with colored nodes and edges.                                       #
################################################################################
.PHONY: dep_rank_colors_clean_%
dep_rank_colors_clean_%: $(DOT_CLEAN_TARGET)
	+@$(MAKE_NO_DIR) dep_rank_colors_$(*)

################################################################################
# A "short cut" target alias for dep_rank_colors_clean_$(EXE).                 #
################################################################################
ifdef EXE # {
.PHONY: dep_$(EXE)
dep_$(EXE): dep_rank_colors_clean_$(EXE)
endif # }

################################################################################
# dot_dump uses the helper function dot_write to write out fragments of dot to #
# the output file and then calls itself for each of the targets in the         #
# prerequisites (prerequisites are extracted from the dot_prereq_$(1)          #
# variables created by dot_run).                                               #
################################################################################
dot_write=$(shell echo '$(1)' >> $(dot_file))
dot_dump=$(if $(dot_prereq_$(1)),                                                                                                       \
$(call dot_write,  "$(1)"[color="$(default_node_color)";penwidth="$(DOT_PEN_WIDTH)";label=<$(1)>];)                                     \
$(foreach p,$(dot_prereq_$(1)),                                                                                                         \
$(call dot_write,  "$(p)" -> "$(1)"[color="$(default_edge_color)";];)),                                                                 \
$(if $(shell grep '^  "$(1)"\[color="$(default_node_color)";penwidth="$(DOT_PEN_WIDTH)";label=<$(1)>\];$$' $(dot_file) 2> /dev/null ),, \
$(call dot_write,  "$(1)"[color="$(default_node_color)";penwidth="$(DOT_PEN_WIDTH)";label=<$(1)>];)))                                   \
$(foreach p,$(dot_prereq_$(1)),$(call dot_dump,$(p)))$(eval dot_prereq_$(1):=)

################################################################################
# Using $(eval ...) to store the relationship between the target being built   #
# and its prerequisites.                                                       #
# NOTE: $(^) contains all of the target's prerequisites.                       #
# NOTE: $(|) contains all of the target's order-only prerequisites.            #
################################################################################
dot_run=$(if $(@),$(eval dot_prereq_$(@):=$(^) $(|)))

.PHONY: dot_build_clean
dot_build_clean::
	@rm -f $(dot_file) $(basename $(dep_file)).* $(DOT_BUILD_FILES)

clean:: dot_build_clean

################################################################################
# If dot graph files must be created for multiple makefile targets, then set   #
# the variable DEP_TARGETS with the target names. This variable must be set    #
# **before** including this make file. Then execute the following to generate  #
# the dot graph files:                                                         #
#   make dot_targets                                                           #
# To generate the dependency diagram files, use:                               #
#   make dep_targets                                                           #
################################################################################
ifdef DEP_TARGETS # {

################################################################################
# DOT_BUILD_FILES will hold the names of the dot and dependency diagram files  #
# created by the targets dot_targets and dep_targets. The target               #
# dot_build_clean can then delete all of these files.                          #
################################################################################
DOT_BUILD_FILES=$(addsuffix .$(DOT_FILE_EXTENSION),$(addprefix $(FILE_PREFIX)_,$(filter-out $(EXE),$(DEP_TARGETS))))
DOT_BUILD_FILES += $(addsuffix .$(DOT_IMAGE_EXTENSION),$(addprefix $(FILE_PREFIX)_,$(filter-out $(EXE),$(DEP_TARGETS))))

################################################################################
# Creates a dot graph makefile target for the input target.                    #
#                                                                              #
# $(1) - a dot graph target (from $(DEP_TARGETS))                              #
################################################################################
define create_dot_target
dot_targets::
	+@$(MAKE_NO_DIR) DOT_FILE_STUB=$(1) dot_clean_$(1)
endef
$(foreach dot_target,$(DEP_TARGETS),$(eval $(call create_dot_target,$(dot_target))))

################################################################################
# Creates a dependency diagram makefile target for the input target.           #
#                                                                              #
# $(1) - a dependency diagram target (from $(DEP_TARGETS))                     #
################################################################################
define create_dep_target
dep_targets::
	+@$(MAKE_NO_DIR) DOT_FILE_STUB=$(1) dep_clean_$(1)
endef
$(foreach dot_target,$(DEP_TARGETS),$(eval $(call create_dep_target,$(dot_target))))

endif # }
endif # }

.PHONY: help_dep_build_mk
help_dep_build_mk::
	@echo '$(MAKE) dep_% → creates a $(DOT_IMAGE_EXTENSION) dependency diagram'
	@echo '$(MAKE) dep_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a $(DOT_IMAGE_EXTENSION) dependency diagram'
	@echo '$(MAKE) dep_colors_% → creates a $(DOT_IMAGE_EXTENSION) dependency diagram, with nodes colored by type'
	@echo '$(MAKE) dep_colors_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a $(DOT_IMAGE_EXTENSION) dependency diagram'
	@echo '$(MAKE) dep_rank_% → creates a $(DOT_IMAGE_EXTENSION) dependency diagram with nodes organized by type'
	@echo '$(MAKE) dep_rank_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a $(DOT_IMAGE_EXTENSION) dependency diagram with nodes organized by type'
	@echo '$(MAKE) dep_rank_colors_% → creates a $(DOT_IMAGE_EXTENSION) dependency diagram, with nodes organized by and colored by type'
	@echo '$(MAKE) dep_rank_colors_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a $(DOT_IMAGE_EXTENSION) dependency diagram, with nodes organized by and colored by type'
	@echo '$(MAKE) dot_% → creates a dot file for a dependency diagram'
	@echo '$(MAKE) dot_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a dot file for a dependency diagram'
	@echo '$(MAKE) dot_colors_% → creates a dot file for a dependency diagram, with nodes colored by type'
	@echo '$(MAKE) dot_colors_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a dot file for a dependency diagram, with nodes colored by type'
	@echo '$(MAKE) dot_rank_% → creates a dot file for a dependency diagram, with nodes organized by type'
	@echo '$(MAKE) dot_rank_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a dot file for a dependency diagram, with nodes organized by type'
	@echo '$(MAKE) dot_rank_colors_% → creates a dot file for a dependency diagram, with nodes organized by and colored by type'
	@echo '$(MAKE) dot_rank_colors_clean_% → performs a $(DOT_CLEAN_TARGET), and then creates a dot file for a dependency diagram, with nodes organized by and colored by type'

show_help:: help_dep_build_mk

endif # }

