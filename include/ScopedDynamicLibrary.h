
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <dlfcn.h>

#include <memory>
#include <string>

// NOTE: Link with "-ldl".

class ScopedDynamicLibrary
{
  public:

    ScopedDynamicLibrary(const std::string& sSharedLibrary, const int nFlag);

    ~ScopedDynamicLibrary();

    inline operator void*()
    {
      return (m_pLibraryHandle.get());
    }

    inline const std::string& sharedLibrary()
    {
      return (m_sSharedLibrary);
    }

    inline int flag() const
    {
      return (m_nFlag);
    }

    inline const std::string& error() const
    {
      return (m_sError);
    }

    inline bool hasError() const
    {
      return (false == m_sError.empty());
    }

    inline bool hasRTLD_LAZY() const
    {
      return (0 != (m_nFlag & RTLD_LAZY));
    }

    inline bool hasRTLD_NOW() const
    {
      return (0 != (m_nFlag & RTLD_NOW));
    }

    inline bool hasRTLD_GLOBAL() const
    {
      return (0 != (m_nFlag & RTLD_GLOBAL));
    }

    inline bool hasRTLD_LOCAL() const
    {
      return (0 != (m_nFlag & RTLD_LOCAL));
    }

    inline bool hasRTLD_NODELETE() const
    {
      return (0 != (m_nFlag & RTLD_NODELETE));
    }

    inline bool hasRTLD_NOLOAD() const
    {
      return (0 != (m_nFlag & RTLD_NOLOAD));
    }

    inline bool hasRTLD_DEEPBIND() const
    {
      return (0 != (m_nFlag & RTLD_DEEPBIND));
    }

    inline bool isNull() const
    {
      return (nullptr == m_pLibraryHandle);
    }

  private:

    std::string           m_sSharedLibrary;
    const int             m_nFlag;
    std::string           m_sError;
    std::shared_ptr<void> m_pLibraryHandle;

    // Intentionally deleted.
    ScopedDynamicLibrary()                                = delete;
    ScopedDynamicLibrary(const ScopedDynamicLibrary& rhs) = delete;
    ScopedDynamicLibrary(ScopedDynamicLibrary&& rhs)      = delete;
    ScopedDynamicLibrary&       operator=(const ScopedDynamicLibrary& rhs) = delete;
    ScopedDynamicLibrary&       operator=(ScopedDynamicLibrary&& rhs) = delete;
    ScopedDynamicLibrary*       operator&()                           = delete;
    const ScopedDynamicLibrary* operator&() const                     = delete;
};

