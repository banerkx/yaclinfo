
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "IPAddresses.h"

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

#include <algorithm>
#include <cstring>
#include <iomanip>
#include <memory>
#include <sstream>

#include "ScopedErrno.h"
#include "ScopedFile.h"
#include "StrError.h"

IPAddresses::IPAddresses() :
  SingletonBase<IPAddresses>(),
  m_tIPAddresses(),
  m_tAFInetIPAddresses(),
  m_bPartionedInet(false),
  m_tAFInet6IPAddresses(),
  m_bPartionedInet6(false),
  m_tAFInetPacketIPAddresses(),
  m_bPartionedInetPacket(false),
  m_sLocalHostName(IPAddresses::getLocalHostName()),
  m_sLocalIPv4Address(),
  m_sLocalIPv6Address(),
  m_sLocalMACAddress(),
  m_tErrors()
{
  getLocalHostIPAddresses();
}

IPAddresses::~IPAddresses()
{
  m_tIPAddresses.clear();
  m_tAFInetIPAddresses.clear();
  m_tAFInet6IPAddresses.clear();
  m_tAFInetPacketIPAddresses.clear();
  m_tErrors.clear();
}

void IPAddresses::getLocalHostIPAddresses()
{
  struct ifaddrs* pIfaddr = nullptr;

  ScopedErrno tErrno;
  int         nStat = getifaddrs(&pIfaddr);

  // Something went wrong; we save the error and then return.
  if (-1 == nStat)
  {
    m_tErrors.push_back(StrError::getInstance().getErrorMessage(errno));
    return;
  }

  std::shared_ptr<struct ifaddrs> tIfAddrs(pIfaddr, freeifaddrs);

  char sHost[NI_MAXHOST + 1];
  for (struct ifaddrs* pIfa = pIfaddr; pIfa != nullptr; pIfa = pIfa->ifa_next)
  {
    if (nullptr == pIfa->ifa_addr)
    {
      continue;
    }

    // Getting the interface family.
    int nFamily = pIfa->ifa_addr->sa_family;

    // Ignoring the AF_PACKET family.
    if (AF_PACKET == nFamily)
    {
      continue;
    }

    IPAddressInfo tInfo;

    // Setting the interface name.
    std::get<IP_INTERFACE>(tInfo) = pIfa->ifa_name;

    socklen_t nAddrLength = 0;
    switch (nFamily)
    {
      case AF_PACKET:  // Do nothing (program flow will never end up here).
        break;

      case AF_INET:
        std::get<IP_FAMILY>(tInfo) = "AF_INET";
        nAddrLength                = sizeof(struct sockaddr_in);
        break;

      case AF_INET6:
        std::get<IP_FAMILY>(tInfo) = "AF_INET6";
        nAddrLength                = sizeof(struct sockaddr_in6);
        break;

      default:
        std::get<IP_FAMILY>(tInfo) = "UNKNOWN";
        break;
    };

    if ((AF_INET == nFamily) || (AF_INET6 == nFamily))
    {
      std::memset(sHost, 0, NI_MAXHOST + 1);
      nStat =
        getnameinfo(pIfa->ifa_addr, nAddrLength, sHost, NI_MAXHOST, nullptr, 0, NI_NUMERICHOST);

      if (0 != nStat)
      {
        std::string sError("Could not get IP address due to: [");
        sError += gai_strerror(nStat);
        sError += "].";
        m_tErrors.push_back(sError);
        std::get<IP_ADDRESS>(tInfo) = "UNKNOWN";
      }
      else
      {
        std::get<IP_ADDRESS>(tInfo) = sHost;
      }

      if (nullptr != pIfa->ifa_netmask)
      {
        std::memset(sHost, 0, NI_MAXHOST + 1);
        nStat = getnameinfo(
          pIfa->ifa_netmask,
          nAddrLength,
          sHost,
          NI_MAXHOST,
          nullptr,
          0,
          NI_NUMERICHOST);

        if (0 != nStat)
        {
          std::string sError("Could not get subnet mask due to: [");
          sError += gai_strerror(nStat);
          sError += "].";
          m_tErrors.push_back(sError);
          std::get<SUBNET_MASK>(tInfo) = "UNKNOWN";
        }
        else
        {
          if (AF_INET == nFamily)
          {
            std::get<SUBNET_MASK>(tInfo) = sHost;
          }
          // NOTE: An IPv6 address does not have a subnet mask, but a prefix
          // length (in bits).
          else  // AF_INET6
          {
            // To get the prefix length for IPv6, we count the number of
            // groups of 4, separated by ':', and then multiply that by 16.
            // To get the number of groups of 4, we simply count the number
            // of ':' characters in sHost, minus any trailing ':''s, and then
            // add 1.
            std::string                         sIPv6Prefix(sHost);
            size_t                              nTrailingColons = 0;
            std::string::const_reverse_iterator i               = sIPv6Prefix.rbegin();
            while ((':' == *(i)) && (i != sIPv6Prefix.rend()))
            {
              ++nTrailingColons;
              ++i;
            }
            sIPv6Prefix = sIPv6Prefix.substr(0, sIPv6Prefix.size() - nTrailingColons);
            size_t nColons =
              static_cast<size_t>(std::count(sIPv6Prefix.begin(), sIPv6Prefix.end(), ':'));
            ++nColons;
            std::get<SUBNET_MASK>(tInfo) = std::to_string(16U * nColons) + " bits";
          }
        }
      }
      else
      {
        std::get<SUBNET_MASK>(tInfo) = "UNKNOWN";
      }
    }
    else
    {
      std::get<IP_ADDRESS>(tInfo)  = "UNKNOWN";
      std::get<SUBNET_MASK>(tInfo) = "UNKNOWN";
    }
    setIPAddress(tInfo, m_sLocalIPv4Address, "AF_INET");
    setIPAddress(tInfo, m_sLocalIPv6Address, "AF_INET6");
    setMACAddress(tInfo);
    m_tIPAddresses.push_back(tInfo);
  }
  IPAddresses::ifEmptySetUnknown(m_sLocalIPv4Address);
  IPAddresses::ifEmptySetUnknown(m_sLocalIPv6Address);
  IPAddresses::ifEmptySetUnknown(m_sLocalMACAddress);
  std::sort(m_tIPAddresses.begin(), m_tIPAddresses.end(), IPAddresses::lessThan);
}

std::ostream& operator<<(std::ostream& tStream, const IPAddresses::IPAddressInfo& rhs) noexcept
{
  tStream << std::setw(IPAddresses::m_nInterfaceWidth) << std::left
          << std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(rhs)
          << std::setw(IPAddresses::m_nFamilyWidth) << std::left
          << std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(rhs)
          << std::setw(IPAddresses::m_nIPAddressWidth) << std::left
          << std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(rhs)
          << std::setw(IPAddresses::m_nMACAddressWidth) << std::left
          << std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(rhs);
  return (tStream);
}

std::ostream& operator<<(std::ostream& tStream, const IPAddresses& rhs) noexcept
{
  tStream << "Local Host = [" << rhs.m_sLocalHostName << "]\n"
          << std::setw(IPAddresses::m_nInterfaceWidth) << std::left << "INTERFACE"
          << std::setw(IPAddresses::m_nFamilyWidth) << std::left << "ADDRESS FAMILY"
          << std::setw(IPAddresses::m_nIPAddressWidth) << std::left << "IP ADDRESS"
          << std::setw(IPAddresses::m_nMACAddressWidth) << std::left << "MAC ADDRESS" << '\n';

  for (const auto& tInfo : rhs.m_tIPAddresses)
  {
    tStream << tInfo << std::endl;
  }

  return (tStream);
}

bool IPAddresses::lessThan(const IPAddressInfo& lhs, const IPAddressInfo& rhs)
{
  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(lhs) <
    std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(rhs))
  {
    return (true);
  }
  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(lhs) >
    std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(rhs))
  {
    return (false);
  }

  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(lhs) <
    std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(rhs))
  {
    return (true);
  }
  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(lhs) >
    std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(rhs))
  {
    return (false);
  }

  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(lhs) <
    std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(rhs))
  {
    return (true);
  }
  if (
    std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(lhs) >
    std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(rhs))
  {
    return (false);
  }

  if (
    std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(lhs) <
    std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(rhs))
  {
    return (true);
  }
  if (
    std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(lhs) >
    std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(rhs))
  {
    return (false);
  }

  if (
    std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(lhs) <
    std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(rhs))
  {
    return (true);
  }
  return (false);
}

std::string IPAddresses::getLocalHostName() noexcept
{
  const size_t            hostNameSize = 1024;
  std::unique_ptr<char[]> pHostName(new char[hostNameSize + 1]);
  std::memset(pHostName.get(), 0, hostNameSize + 1);
  ScopedErrno tErrno;
  int         nStat = gethostname(pHostName.get(), hostNameSize);

  if (0 == nStat)
  {
    return (std::string(pHostName.get()));
  }
  else
  {
    m_tErrors.push_back(StrError::getInstance().getErrorMessage(errno));
  }
  return ("Host name lookup failure.");
}

void IPAddresses::setIPAddress(IPAddressInfo& tInfo, std::string& sIPAddress, const char* pIPFamily)
{
  if (true == sIPAddress.empty())
  {
    if (
      ("lo" != std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo)) &&
      ("virbr0" != std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo)) &&
      (pIPFamily == std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(tInfo)))
    {
      sIPAddress = std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(tInfo);
    }
  }
}

void IPAddresses::setMACAddress(IPAddressInfo& tInfo)
{
  if ("lo" == std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo))
  {
    std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo) = "N/A";
    return;
  }

  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (fd < 0)
  {
    std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo) = "UNKNOWN";
    return;
  }

  ScopedFileDescriptor tFd(fd);

  struct ifreq ifr;
  std::memset(ifr.ifr_name, 0, IFNAMSIZ);
  std::strncpy(
    ifr.ifr_name,
    std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo).c_str(),
    IFNAMSIZ - 1);
  ifr.ifr_addr.sa_family = AF_INET;

  if (-1 == ioctl(tFd, SIOCGIFHWADDR, &ifr))
  {
    std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo) = "UNKNOWN";
    return;
  }

  unsigned char* pMACAddress = (unsigned char*) ifr.ifr_hwaddr.sa_data;

  std::stringstream   tMACAddress;
  static const size_t nMACAddressLength = 6;
  for (size_t i = 0; i < nMACAddressLength; ++i)
  {
    tMACAddress << std::setfill('0') << std::setw(2) << std::hex
                << static_cast<unsigned short>(pMACAddress[i]);
    if (i < (nMACAddressLength - 1))
    {
      tMACAddress << ':';
    }
  }
  std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo) = tMACAddress.str();

  if (true == m_sLocalMACAddress.empty())
  {
    m_sLocalMACAddress = std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo);
  }
}

void IPAddresses::ifEmptySetUnknown(std::string& sAddress)
{
  if (true == sAddress.empty())
  {
    sAddress = "UNKNOWN";
  }
}

void IPAddresses::partition(std::vector<IPAddressInfo>& tIPAddresses, PartitionPredicate tPredicate)
{
  std::vector<IPAddresses::IPAddressInfo>           tAddresses(m_tIPAddresses);
  std::vector<IPAddresses::IPAddressInfo>::iterator i =
    std::partition(tAddresses.begin(), tAddresses.end(), tPredicate);
  tIPAddresses.assign(tAddresses.begin(), i);
}

const std::vector<IPAddresses::IPAddressInfo>& IPAddresses::inetAddresses()
{
  if (false == m_bPartionedInet)
  {
    partition(m_tAFInetIPAddresses, IPAddresses::IPAddresses::isAFInet);
    m_bPartionedInet = true;
  }

  return (m_tAFInetIPAddresses);
}

const std::vector<IPAddresses::IPAddressInfo>& IPAddresses::inet6Addresses()
{
  if (false == m_bPartionedInet6)
  {
    partition(m_tAFInet6IPAddresses, IPAddresses::IPAddresses::isAFInet6);
    m_bPartionedInet6 = true;
  }

  return (m_tAFInet6IPAddresses);
}

const std::vector<IPAddresses::IPAddressInfo>& IPAddresses::packetAddresses()
{
  if (false == m_bPartionedInetPacket)
  {
    partition(m_tAFInetPacketIPAddresses, IPAddresses::IPAddresses::isAFPacket);
    m_bPartionedInetPacket = true;
  }

  return (m_tAFInetPacketIPAddresses);
}

bool IPAddresses::isAFInet(const IPAddressInfo& tIPInfo)
{
  return (std::get<IP_INFO_FIELD::IP_FAMILY>(tIPInfo) == "AF_INET");
}

bool IPAddresses::isAFInet6(const IPAddressInfo& tIPInfo)
{
  return (std::get<IP_INFO_FIELD::IP_FAMILY>(tIPInfo) == "AF_INET6");
}

bool IPAddresses::isAFPacket(const IPAddressInfo& tIPInfo)
{
  return (std::get<IP_INFO_FIELD::IP_FAMILY>(tIPInfo) == "AF_PACKET");
}

