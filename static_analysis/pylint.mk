
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PYLINT_MK_INCLUDE_GUARD # {
PYLINT_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

PYLINT_PROJECT=$(notdir $(CURDIR))
PYLINT=pylint
PYLINT_OUTPUT=$(PYLINT_PROJECT)_$(PYLINT)
PYLINT_URL=https://pylint.pycqa.org/en/latest

PYLINT_REPORT=pylint-report
PYLINT_REPORT_PREFIX=$(subst -,_,$(PYLINT_REPORT))
PYLINT_JSON2HTML=pylint-json2html
PYLINT_JSON2HTML_PREFIX=$(subst -,_,$(PYLINT_JSON2HTML))

################################################################################
# NOTE: Include this makefile after including python.mk.                       #
################################################################################
ifeq ($(PYTHON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/python.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

PYTHON_PACKAGES += pylint
$(eval $(call create_pip_show_target,pylint))
$(eval $(call create_pip_show_files_target,pylint))

EDITOR  ?= vim +1
BROWSER ?= firefox
IMAGE_PATH ?= $(PROJECT_ROOT)/images

PYLINT_COMMAND:=$(shell command -v $(PYLINT))
ifeq ($(PYLINT_COMMAND),) # {
$(error $(ERROR_LABEL) The [$(PYLINT)] command is not available)
endif # }
PYLINT_VERSION=$(call get_python_package_version,$(PYLINT))

PYLINT_JSON_OUTPUT_FORMAT=--output-format=json
PYLINT_REPORT_OUTPUT_FORMAT=--output-format=pylint_report.pylint_report.CustomJsonReporter
PYLINT_TEXT_OUTPUT_FORMAT=--output-format=text

################################################################################
# See: pylint --long-help                                                      #
################################################################################
PYLINT_SPELLING_SECTION_OPTS=--spelling-dict=en_US
################################################################################
# Use the variable PYLINT_IGNORE_WORDS to hold a comma delimited list of words #
# whose spell checking should be ignored.                                      #
#                                                                              #
# NOTE: PYLINT_IGNORE_WORDS must be set **before** this makefile is included.  #
################################################################################
ifdef PYLINT_IGNORE_WORDS # {
PYLINT_SPELLING_SECTION_OPTS += --spelling-ignore-words=$(PYLINT_IGNORE_WORDS)
endif # }
PYLINT_BASIC_SECTION_OPTS=--include-naming-hint=y
PYLINT_CLASSES_SECTION_OPTS=
PYLINT_COMMANDS_SECTION_OPTS=
PYLINT_DESIGN_SECTION_OPTS=--max-args=5 --max-locals=15 --max-branches=12 --max-statements=50 --max-parents=7 --max-attributes=7
PYLINT_EXCEPTIONS_SECTION_OPTS=
PYLINT_FORMAT_SECTION_OPTS=--indent-string="  "
PYLINT_IMPORTS_SECTION_OPTS=
PYLINT_LOGGING_SECTION_OPTS=
PYLINT_MASTER_SECTION_OPTS=--verbose --persistent=n --jobs=0 --suggestion-mode=y --exit-zero
PYLINT_MESSAGES_CTRL_SECTION_OPTS=
PYLINT_MISC_SECTION_OPTS=
PYLINT_REFACTORING_SECTION_OPTS=
PYLINT_REPORTS_SECTION_OPTS=--reports=y
PYLINT_SIMILARITIES_SECTION_OPTS=--min-similarity-lines=4 --ignore-comments=n --ignore-docstrings=n --ignore-imports=n
PYLINT_STRING_SECTION_OPTS=
PYLINT_TYPECHECK_SECTION_OPTS=
PYLINT_VARIABLES_SECTION_OPTS=

################################################################################
# See:                                                                         #
#   https://docs.pylint.org/en/1.6.0/extensions.html                           #
################################################################################
##PYLINT_OPIONAL_CHECKERS=pylint.extensions.docparams pylint.extensions.docstyle

##PYLINT_PLUGINS=pylint_report.pylint_report
PYLINT_PLUGINS += $(PYLINT_OPIONAL_CHECKERS)
PYLINT_PLUGINS:=$(if $(PYLINT_PLUGINS),--load-plugins=$(subst $(escaped_space),$(escaped_comma),$(PYLINT_PLUGINS)))

################################################################################
# C0121 → singleton-comparison                                                 #
# C0122 → misplaced-comparison-constant                                        #
# C0200 → consider-using-enumerate                                             #
# C0325 → superfluous-parens                                                   #
#                                                                              #
# See:                                                                         #
#   https://pycodequ.al/docs/pylint-messages.html                              #
################################################################################
PYLINT_DISABLED_CHECKERS=--disable=C0121,C0122,C0200,C0325

PYLINT_OPTS += $(PYLINT_BASIC_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_CLASSES_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_COMMANDS_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_DESIGN_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_EXCEPTIONS_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_FORMAT_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_IMPORTS_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_LOGGING_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_MASTER_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_MESSAGES_CTRL_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_MISC_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_REFACTORING_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_REPORTS_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_SIMILARITIES_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_SPELLING_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_STRING_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_TYPECHECK_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_VARIABLES_SECTION_OPTS)
PYLINT_OPTS += $(PYLINT_DISABLED_CHECKERS)
PYLINT_OPTS += $(PYLINT_PLUGINS)

PYLINT_REPORT_SCRIPT=$(PYLINT_REPORT_PREFIX)
PYLINT_REPORT_COMMAND:=$(shell command -v $(PYLINT_REPORT_SCRIPT))
################################################################################
# If pylint_report.py is not available, then the output from pylint will       #
# remain as text. Otherwise, the pylint output will:                           #
#   pylint_report.pylint_report.CustomJsonReporter                             #
################################################################################
ifneq ($(PYLINT_REPORT_COMMAND),) # {
PYLINT_REPORT_VERSION=$(call get_python_package_version,$(PYLINT_REPORT))
PYLINT_REPORT_OUTPUT_DIR=$(PYLINT_REPORT_PREFIX)
##--score-history-fig SCORE_HISTORY_FIG
PYLINT_REPORT_OPTS=--html-file
endif # }

PYLINT_JSON2HTML_COMMAND:=$(shell command -v $(PYLINT_JSON2HTML))
ifneq ($(PYLINT_JSON2HTML_COMMAND),) # {
PYLINT_JSON2HTML_VERSION=$(call get_python_package_version,$(PYLINT_JSON2HTML))
PYLINT_JSON2HTML_OUTPUT_DIR=$(PYLINT_JSON2HTML_PREFIX)
endif # }

PYTHON_SCRIPTS:=$(sort $(PYTHON_SCRIPTS))
$(call check_python_scripts_existence,$(PYTHON_SCRIPTS))

PYLINT_ERROR_QUERY=https://pycodequ.al/docs/search.html?q=

pylint::
	@rm -rf $(PYLINT_OUTPUT)

################################################################################
# Adds to the pylint target for the input Python script so that pylint can be  #
# run on all Python scripts at once.                                           #
#                                                                              #
# Example:                                                                     #
#   make pylint                                                                #
#                                                                              #
# Adds a "::" pylint target for the script so that this target and another     #
# target can be run simultaneously.                                            #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make my_script.py                                                          #
#                                                                              #
# Adds a pylint target, prefixed with 'p', so that pylint can be run only for  #
# this script.                                                                 #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make pmy_script.py                                                         #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
PYLINT_FAVICON=$(IMAGE_PATH)/pylint.png
PYLINT_JSON_FAVICON=$(IMAGE_PATH)/json.png
.PHONY: pylint
define add_pylint_targets
pylint::
	@$$(eval PYLINT_STUB=$$(subst .py,_py,$$(notdir $(1))))
	@$$(eval PYLINT_OUTPUT_DIR:=$(PYLINT_OUTPUT)/$$(PYLINT_STUB))
	@$$(eval PYLINT_LOG=$$(PYLINT_OUTPUT)/$(PYLINT)_$$(PYLINT_STUB).log)
	@mkdir -p $$(PYLINT_OUTPUT)
ifeq ($(PYLINT_REPORT_COMMAND) $(PYLINT_JSON2HTML_COMMAND),) # {
	@rm -f $$(PYLINT_LOG)
	@$(PYLINT) $(PYLINT_OPTS) $(PYLINT_TEXT_OUTPUT_FORMAT) $(1) 2>&1 | grep -v "^No config file found, using default configuration$$$$" >| $$(PYLINT_LOG)
	@if [[ -s $$(PYLINT_LOG) ]];                                        \
   then                                                               \
     echo '$(EDITOR) $$(PYLINT_LOG)';                                 \
   else                                                               \
     echo '$(WARNING_LABEL) The [$(PYLINT_LOG)] file was not found.'; \
   fi
################################################################################
# If pylint-report is available, we use it to generate an HTML report.         #
################################################################################
else ifneq ($(PYLINT_REPORT_COMMAND),) # } {
	@mkdir -p $$(PYLINT_OUTPUT)/$(PYLINT_REPORT_PREFIX)
	@rm -f $$(PYLINT_OUTPUT)/$(PYLINT_REPORT_PREFIX)/$(PYLINT_REPORT_PREFIX)_$(subst .,_,$(1)).html
	@$$(eval PYLINT_REPORT_HTML_FILE=$$(PYLINT_OUTPUT)/$(PYLINT_REPORT_PREFIX)/$(PYLINT_REPORT_PREFIX)_$$(PYLINT_STUB).html)
	@$(PYLINT) $(PYLINT_OPTS) $(PYLINT_REPORT_PLUGINS) $(PYLINT_REPORT_OUTPUT_FORMAT) $(1) 2>&1 | grep -v "^No config file found, using default configuration$$$$" | $(PYLINT_REPORT_COMMAND) $(PYLINT_REPORT_OPTS) $$(PYLINT_REPORT_HTML_FILE)
	@if [[ -s $$(PYLINT_REPORT_HTML_FILE) ]];                                                                                                \
   then                                                                                                                                    \
     echo '$(BROWSER) $$(PYLINT_REPORT_HTML_FILE)';                                                                                        \
     cp $(PYLINT_FAVICON) $$(dir $$(PYLINT_REPORT_HTML_FILE));                                                                             \
     sed -i '\@<title>.*</title>@a<link rel="icon" type="image/x-icon" href="./$(notdir $(PYLINT_FAVICON))">' $$(PYLINT_REPORT_HTML_FILE); \
   else                                                                                                                                    \
     echo '$(WARNING_LABEL) The [$$(PYLINT_REPORT_HTML_FILE)] html file was not found.';                                                   \
   fi
################################################################################
# If pylint-json2html is available, we use to generate an HTML report.         #
################################################################################
else ifneq ($(PYLINT_JSON2HTML_COMMAND),) # } {
	@$$(eval PYLINT_STUB=$$(subst .py,_py,$$(notdir $(1))))
	@$$(eval PYLINT_OUTPUT_DIR:=$(PYLINT_OUTPUT)/$$(PYLINT_STUB))
	@mkdir -p $$(PYLINT_OUTPUT)/$(PYLINT_JSON2HTML_PREFIX)
	@rm -f $$(PYLINT_OUTPUT)/$(PYLINT_JSON2HTML_PREFIX)/$(PYLINT_JSON2HTML_PREFIX)_$(subst .,_,$(1)).html
	@$$(eval PYLINT_JSON2HTML_FILE=$$(PYLINT_OUTPUT)/$(PYLINT_JSON2HTML_PREFIX)/$(PYLINT_JSON2HTML_PREFIX)_$$(PYLINT_STUB).html)
	@rm -f $$(PYLINT_JSON2HTML_FILE)
	@$(PYLINT) $(PYLINT_OPTS) $(PYLINT_JSON_OUTPUT_FORMAT) $(1) 2>&1 | grep -v "^No config file found, using default configuration$$$$" | $(PYLINT_JSON2HTML_COMMAND) --output $$(PYLINT_JSON2HTML_FILE) --input-format json
	@if [[ -s $$(PYLINT_JSON2HTML_FILE) ]];                                                                                                              \
   then                                                                                                                                                \
     sed -i 's@\(^.*<td>\)\([A-Z][0-9]\{4\}\)\(</td>.*$$$$\)@\1<a href="$(PYLINT_ERROR_QUERY)\2" target="_blank">\2</a>\3@' $$(PYLINT_JSON2HTML_FILE); \
     cp $(PYLINT_JSON_FAVICON) $$(dir $$(PYLINT_JSON2HTML_FILE));                                                                                      \
     sed -i '\@<title>.*</title>@a<link rel="icon" type="image/x-icon" href="./$(notdir $(PYLINT_JSON_FAVICON))">' $$(PYLINT_JSON2HTML_FILE);          \
     echo '$(BROWSER) $$(PYLINT_JSON2HTML_FILE)';                                                                                                      \
   else                                                                                                                                                \
     echo '$(WARNING_LABEL) The [$(PYLINT_JSON2HTML_FILE)] html file was not found.';                                                                  \
   fi
################################################################################
# If pylint-report nor pylint-json2html are available, we generate a text      #
# report.                                                                      #
################################################################################
endif # }

################################################################################
# Creating the "::" target.                                                    #
################################################################################
.PHONY: $(notdir $(1))
$(notdir $(1))::
	+@$(MAKE_NO_DIR) PYTHON_SCRIPTS=$(1) pylint
################################################################################
# Creating the single script target.                                           #
################################################################################
.PHONY: p$(notdir $(1))
p$(notdir $(1)):
	+@$(MAKE_NO_DIR) PYTHON_SCRIPTS=$(1) pylint
endef
$(foreach py,$(PYTHON_SCRIPTS),$(eval $(call add_pylint_targets,$(py))))

.PHONY: pylint_msgs
PYLINT_MSGS=pylint_messages.log
pylint_msgs:
	@$(PYLINT_COMMAND) --list-msgs >| $(PYLINT_MSGS)
	@$(call open_file_command,$(PYLINT_MSGS),$(EDITOR),pylint messages file)

pylint_clean:
	@rm -rf $(PYLINT_OUTPUT) $(PYLINT_MSGS)

clean:: pylint_clean

platform_info::
	@echo '$(PYLINT) version           = [$(PYLINT_VERSION)]'
	@echo '$(PYLINT_JSON2HTML) version = [$(PYLINT_JSON2HTML_VERSION)]'
	@echo '$(PYLINT_REPORT) version    = [$(PYLINT_REPORT_VERSION)]'

################################################################################
# Prints out help information for the pylint target(s).                        #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - first part of target description                                      #
# $(3) - final part of target description (optional)                           #
################################################################################
define pylint_help
echo '$(MAKE) $(1) → $(2) on the Python script$(PLURAL) $(call setify,$(PYTHON_SCRIPTS))$(3)and saves the output file$(PLURAL) under [$(PYLINT_OUTPUT)]';
endef

################################################################################
# Prints help information of the double-colon and the "p" targets.             #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
define pylint_show_help
echo '$(MAKE) $(1) → double-colon target to run [$(PYLINT)], and possibly other target(s), only on [$(1)]';
echo '$(MAKE) p$(1) → run [$(PYLINT)] only on [$(1)]';
endef

PYTHON_SCRIPTS_COUNT=$(words $(PYTHON_SCRIPTS))
ifneq ($(PYTHON_SCRIPTS_COUNT),0) # {
PLURAL=$(if $(filter 1,$(PYTHON_SCRIPTS_COUNT)),,s)

pylint_mk_help::
ifneq ($(PYLINT_REPORT_COMMAND),) # {
	@$(call pylint_help,pylint,pipes the [$(PYLINT)] output, to [$(PYLINT_REPORT)] )
else ifneq ($(PYLINT_JSON2HTML_COMMAND),) # } {
	@$(call pylint_help,pylint,pipes the [$(PYLINT)] output, to [$(PYLINT_JSON2HTML)] )
else # } {
	@$(call pylint_help,pylint,runs [$(PYLINT)],,)
endif # }
	@echo '$(MAKE) pylint_msgs → save the list of [$(PYLINT)] message to [$(PYLINT_MSGS)]'
	@$(foreach py,$(notdir $(PYTHON_SCRIPTS)),$(call pylint_show_help,$(py)))
endif # }

show_help:: pylint_mk_help

endif # }

