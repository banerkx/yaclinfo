
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "PlatformInfo.h"

#include "DeviceInfo_100.h"
#include "DeviceInfo_110.h"
#include "DeviceInfo_120.h"
#include "DeviceInfo_200.h"
#include "DeviceInfo_210.h"
#include "DeviceInfo_220.h"
#include "Macros.h"
#include "OpenCLHTMLUtilities.h"

PlatformInfo::PlatformInfo(
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nPlatformVersion) :
  m_tPlatformID(tPlatformID),
  m_nPlatformNumber(nPlatformNumber),
  m_tPlatformInfo(),
  m_pDeviceIDs(),
  m_pBadDeviceIDs(),
  m_pDeviceInfo(),
  m_nPlatformVersion(nPlatformVersion),
  m_tPlatformInfoTypes(),
  m_tDeviceTypes()
{
}

void PlatformInfo::populatePlatformInfoTypes()
{
  m_tPlatformInfoTypes.push_back(OPEN_cl_platform_info::OPENCL_PLATFORM_EXTENSIONS);
  m_tPlatformInfoTypes.push_back(OPEN_cl_platform_info::OPENCL_PLATFORM_NAME);
  m_tPlatformInfoTypes.push_back(OPEN_cl_platform_info::OPENCL_PLATFORM_PROFILE);
  m_tPlatformInfoTypes.push_back(OPEN_cl_platform_info::OPENCL_PLATFORM_VENDOR);
  m_tPlatformInfoTypes.push_back(OPEN_cl_platform_info::OPENCL_PLATFORM_VERSION);
}

void PlatformInfo::populateDeviceTypes()
{
  m_tDeviceTypes.push_back(OPEN_cl_device_type::OPENCL_DEVICE_TYPE_CPU);
  m_tDeviceTypes.push_back(OPEN_cl_device_type::OPENCL_DEVICE_TYPE_GPU);
  m_tDeviceTypes.push_back(OPEN_cl_device_type::OPENCL_DEVICE_TYPE_ACCELERATOR);
  m_tDeviceTypes.push_back(OPEN_cl_device_type::OPENCL_DEVICE_TYPE_DEFAULT);
  m_tDeviceTypes.push_back(OPEN_cl_device_type::OPENCL_DEVICE_TYPE_ALL);
}

void PlatformInfo::init()
{
  populateDeviceTypes();
  populatePlatformInfoTypes();
  getPlatformAttributes();
  getDeviceIds();
}

void PlatformInfo::getDeviceInfo() noexcept
{
  for (size_t i = 0; i < m_pDeviceIDs.size(); ++i)
  {
    for (size_t j = 0; j < m_pDeviceIDs[i]->size(); ++j)
    {
      m_pDeviceInfo.push_back(std::shared_ptr<DeviceInfo>(PlatformInfo::deviceInfoFactory(
        (*m_pDeviceIDs[i])[j],
        j,
        m_tPlatformID,
        m_nPlatformNumber,
        m_nPlatformVersion)));
    }
  }
}

void PlatformInfo::getDeviceIds() noexcept
{
  cl_int  nStat               = CL_SUCCESS;
  cl_uint nNumMatchingDevices = 0;
  for (size_t j = 0; j < m_tDeviceTypes.size(); ++j)
  {
    if (
      (m_tDeviceTypes[j] != OPEN_cl_device_type::OPENCL_DEVICE_TYPE_ALL) &&
      (m_tDeviceTypes[j] != OPEN_cl_device_type::OPENCL_DEVICE_TYPE_DEFAULT))
    {
      std::shared_ptr<DeviceID> tDeviceID(new DeviceID());
      // Getting the number of devices.
      nStat = clGetDeviceIDs(
        m_tPlatformID,
        scopedEnumToCLEnum(m_tDeviceTypes[j]),
        0,
        nullptr,
        &nNumMatchingDevices);

      switch (nStat)
      {
        case CL_SUCCESS:
          (void) getDeviceIDInfo(
            tDeviceID,
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            nNumMatchingDevices);
          break;

        case CL_INVALID_PLATFORM:
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Invalid Platform");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;

        case CL_INVALID_DEVICE_TYPE:
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Invalid Device Type");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;

        case CL_INVALID_VALUE:
        {
          std::stringstream tError;
          tError << "ERROR: Invalid value: [" << m_tDeviceTypes[j] << "] ==> [0x" << std::internal
                 << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
                 << scopedEnumToCLEnum(m_tDeviceTypes[j]) << "] = [" << std::nouppercase << std::dec
                 << scopedEnumToCLEnum(m_tDeviceTypes[j]) << "].";
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            tError.str());
          m_pBadDeviceIDs.push_back(tDeviceID);
        }
        break;

        case CL_DEVICE_NOT_FOUND:
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Device Not Found");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;

#ifdef CL_OUT_OF_RESOURCES
        case CL_OUT_OF_RESOURCES:
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Out Of Device Resources");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;
#endif

#ifdef CL_OUT_OF_HOST_MEMORY
        case CL_OUT_OF_HOST_MEMORY:
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Out Of Host Resources");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;
#endif

        default:  // Unknown error.
          tDeviceID->resetForError(
            m_tPlatformID,
            scopedEnumToCLEnum(m_tDeviceTypes[j]),
            "Unknown clGetDeviceIDs() Error");
          m_pBadDeviceIDs.push_back(tDeviceID);
          break;
      };
    }
  }
  getDeviceInfo();
}

PlatformInfo::~PlatformInfo() noexcept
{
  m_tPlatformInfo.clear();
  m_pDeviceIDs.clear();
  m_pBadDeviceIDs.clear();
  m_pDeviceInfo.clear();
  m_tPlatformInfoTypes.clear();
  m_tDeviceTypes.clear();
}

void PlatformInfo::add(const cl_platform_info eInfoEnum, const std::string& sPlatformInfo)
{
  switch (eInfoEnum)
  {
    case CL_PLATFORM_PROFILE:
    case CL_PLATFORM_VERSION:
    case CL_PLATFORM_NAME:
    case CL_PLATFORM_VENDOR:
    case CL_PLATFORM_EXTENSIONS:
    case CL_PLATFORM_HOST_TIMER_RESOLUTION:
    case CL_PLATFORM_ICD_SUFFIX_KHR:
      m_tPlatformInfo.insert(std::pair<cl_platform_info, std::string>(eInfoEnum, sPlatformInfo));
      break;

    default:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "Invalid platform id.";
      throw std::runtime_error(tError.str());
    }
    break;
  };
}

void PlatformInfo::populatePlatformExtensions(
  std::vector<std::string>& tPlatformExtensions) const noexcept
{
  tPlatformExtensions.clear();

  const std::string sPlatformExtensions(find(CL_PLATFORM_EXTENSIONS));
  if ((false == sPlatformExtensions.empty()) && ("UNKNOWN" != sPlatformExtensions))
  {
    DeviceInfo::extractTokens(tPlatformExtensions, sPlatformExtensions);
  }
}

std::string PlatformInfo::find(const cl_platform_info eInfoEnum) const noexcept
{
  std::map<cl_platform_info, std::string>::const_iterator i = m_tPlatformInfo.find(eInfoEnum);
  if (m_tPlatformInfo.end() != i)
  {
    return (i->second);
  }
  return ("UNKNOWN");
}

cl_int PlatformInfo::getDeviceIDInfo(
  std::shared_ptr<DeviceID> tDeviceID,
  const cl_platform_id      tPlatformID,
  const cl_device_type      eDeviceType,
  const cl_uint             nNumMatchingDevices) noexcept
{
  std::shared_ptr<cl_device_id> pDeviceIDs(
    new cl_device_id[nNumMatchingDevices],
    DeviceID::deleteDeviceIDArray);

  // Now getting all of the device id's for device type eDeviceType.
  cl_uint      nTemp = 0;
  const cl_int nStat =
    clGetDeviceIDs(tPlatformID, eDeviceType, nNumMatchingDevices, pDeviceIDs.get(), &nTemp);

  if (nTemp == nNumMatchingDevices)
  {
    switch (nStat)
    {
      case CL_SUCCESS:
        tDeviceID->reset(tPlatformID, eDeviceType, nNumMatchingDevices, pDeviceIDs);
        m_pDeviceIDs.push_back(tDeviceID);
        break;

      case CL_INVALID_PLATFORM:
        tDeviceID->resetForError(tPlatformID, eDeviceType, "Invalid Platform");
        break;

      case CL_INVALID_DEVICE_TYPE:
        tDeviceID->resetForError(tPlatformID, eDeviceType, "Invalid Device Type");
        break;

      // This error is only possible if:
      //   (0 == nNumMatchingDevices) && (nullptr != pDeviceIDs)
      //                              OR
      //   (nullptr == &nTemp)        && (nullptr == pDeviceIDs)
      // Since this is not possible, the error will never be CL_INVALID_VALUE.
      // Therefore, we do nothing.
      case CL_INVALID_VALUE:
        break;

      case CL_DEVICE_NOT_FOUND:
        tDeviceID->resetForError(tPlatformID, eDeviceType, "Device Not Found");
        break;

#ifdef CL_OUT_OF_RESOURCES
      case CL_OUT_OF_RESOURCES:
        tDeviceID->resetForError(m_tPlatformID, eDeviceType, "Out Of Device Resources");
        break;
#endif

#ifdef CL_OUT_OF_HOST_MEMORY
      case CL_OUT_OF_HOST_MEMORY:
        tDeviceID->resetForError(m_tPlatformID, eDeviceType, "Out Of Host Resources");
        break;
#endif

      default:  // Unknown error.
        tDeviceID->resetForError(tPlatformID, eDeviceType, "Unknown clGetDeviceIDs() Error");
        break;
    };
    m_pBadDeviceIDs.push_back(tDeviceID);
  }
  else
  {
    std::stringstream tError;
    tError << "clGetDeviceIDs(): The expected number of devices [" << nNumMatchingDevices
           << "] != the actual number of devices [" << nTemp << "]";
    tDeviceID->resetForError(tPlatformID, eDeviceType, tError.str());
  }

  return (nStat);
}

std::ostream& operator<<(std::ostream& tStream, const PlatformInfo& rhs) noexcept
{
  rhs.getTextOutput(tStream);
  return (tStream);
}

void PlatformInfo::getTextOutput(std::ostream& tStream) const noexcept
{
  tStream << "PLATFORM PROFILE         = [" << find(CL_PLATFORM_PROFILE) << "]" << std::endl
          << "PLATFORM VERSION         = [" << find(CL_PLATFORM_VERSION) << "]" << std::endl
          << "PLATFORM NAME            = [" << find(CL_PLATFORM_NAME) << "]" << std::endl
          << "PLATFORM VENDOR          = [" << find(CL_PLATFORM_VENDOR) << "]" << std::endl
          << "PLATFORM EXTENSIONS      : ";

  std::vector<std::string> tPlatformExtensions;
  populatePlatformExtensions(tPlatformExtensions);
  if (false == tPlatformExtensions.empty())
  {
    static const std::string sHorizontalIndent(27, ' ');
    DeviceInfo::printVectorVertically(tStream, tPlatformExtensions, sHorizontalIndent);
  }
  else
  {
    tStream << "        No extensions found." << std::endl;
  }

  std::string sCLPlatformICDSuffixKhr(find(CL_PLATFORM_ICD_SUFFIX_KHR));
  if ("UNKNOWN" != sCLPlatformICDSuffixKhr)
  {
    tStream << "KHR FUNCTION NAME SUFFIX = [" << sCLPlatformICDSuffixKhr << "]" << std::endl;
  }

  tStream << "NUMBER OF DEVICES        = [" << m_pDeviceInfo.size() << "]\n\n";
  for (size_t i = 0; i < m_pDeviceInfo.size(); ++i)
  {
    tStream << "---------------------------------------------------------------"
               "-----------------\n";
    tStream << "DEVICE NUMBER        = [" << i << "]\n" << std::endl;
    tStream << *m_pDeviceInfo[i] << '\n';
    tStream << "---------------------------------------------------------------"
               "-----------------\n";
  }
}

void PlatformInfo::getHTMLOutput(std::ostream& tStream, const size_t nPlatform) const noexcept
{
  setHTMLTableRow(
    tStream,
    "PLATFORM PROFILE",
    find(CL_PLATFORM_PROFILE),
    OpenCLToolTip("Profile supported by the implementation"));
  setHTMLTableRow(
    tStream,
    "PLATFORM VERSION",
    find(CL_PLATFORM_VERSION),
    OpenCLToolTip("OpenCL version"));
  setHTMLTableRow(tStream, "PLATFORM NAME", find(CL_PLATFORM_NAME), OpenCLToolTip("Platform name"));
  setHTMLTableRow(
    tStream,
    "PLATFORM VENDOR",
    find(CL_PLATFORM_VENDOR),
    OpenCLToolTip("Platform vendor"));

  std::vector<std::string> tPlatformExtensions;
  populatePlatformExtensions(tPlatformExtensions);
  if (false == tPlatformExtensions.empty())
  {
    setHTMLTableRow(
      tStream,
      "PLATFORM EXTENSIONS",
      toHTMLTokens(tPlatformExtensions),
      OpenCLToolTip("Extensions supported by the platform."));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      "PLATFORM EXTENSIONS",
      "No extensions found.",
      OpenCLToolTip("Extensions supported by the platform."));
  }
  std::string sCLPlatformICDSuffixKhr(find(CL_PLATFORM_ICD_SUFFIX_KHR));
  if ("UNKNOWN" != sCLPlatformICDSuffixKhr)
  {
    setHTMLTableRow(
      tStream,
      "KHR FUNCTION NAME SUFFIX",
      sCLPlatformICDSuffixKhr,
      OpenCLToolTip("KHR function name suffix"));
  }

  std::stringstream tLinks;
  if (false == m_pDeviceInfo.empty())
  {
    constexpr static const char* sHTMLRightArrow = "&#8658;";
    for (size_t i = 0; i < m_pDeviceInfo.size(); ++i)
    {
      tLinks << "<a href=\"#platform_" << nPlatform << "_device_" << i << "\">Platform ["
             << nPlatform << "] " << sHTMLRightArrow << " Device Number [" << i << "]</a>";
      if (i < (m_pDeviceInfo.size() - 1))
      {
        tLinks << "</br>";
      }
    }
    setHTMLTableRow(tStream, "NUMBER OF DEVICES", m_pDeviceInfo.size());
    setHTMLTableRow(
      tStream,
      "Device Quick Link(s)",
      tLinks.str(),
      OpenCLToolTip("Device quick link(s)"));
    for (size_t i = 0; i < m_pDeviceInfo.size(); ++i)
    {
      tStream << "  <p id=\"platform_" << nPlatform << "_"
              << "device_" << i << "\"></p>\n";
      tStream << "  <tr>\n    <td style=\"text-align: center;\" "
                 "colspan=\"2\"><div class=\"device_number\"><b>DEVICE NUMBER "
              << i << "</b></td>\n  </tr>\n";
      m_pDeviceInfo[i]->getHTMLOutput(tStream);
    }
  }
  else
  {
    setHTMLTableRow(tStream, "NUMBER OF DEVICES", 0);
  }
}

DeviceInfo* PlatformInfo::deviceInfoFactory(
  const cl_device_id   nDeviceID,
  const size_t         nDeviceNumber,
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nPlatformVersion)
{
  DeviceInfo* p = nullptr;
  switch (nPlatformVersion)
  {
    case 100:
      p = new DeviceInfo_100(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    case 110:
      p = new DeviceInfo_110(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    case 120:
      p = new DeviceInfo_120(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    case 200:
      p = new DeviceInfo_200(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    case 210:
      p = new DeviceInfo_210(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    case 220:
      p = new DeviceInfo_220(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber);
      break;

    default:
    {
      std::stringstream tError;
      tError << "ERROR: The device version [" << nPlatformVersion << "] is not valid.";
      throw std::runtime_error(tError.str());
    }
    break;
  };

  p->init();
  return (p);
}

