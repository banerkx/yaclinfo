
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CppUnitTestTimingsCollector.h"

#include <cppunit/portability/Stream.h>

#include <ctime>
#include <iomanip>
#include <sstream>

CPPUNIT_NS_BEGIN

CppUnitTestTimingsCollector::CppUnitTestTimingsCollector(
  std::ostream& tStream,
  const char    cSeparator) :
  CPPUNIT_NS::TestResultCollector(),
  m_tStream(tStream),
  m_cSeparator(cSeparator),
  m_tCurrentLocale(m_tStream.imbue(std::locale(""))),
  m_tStartTime()
{
}

CppUnitTestTimingsCollector::~CppUnitTestTimingsCollector()
{
  (void) m_tStream.imbue(m_tCurrentLocale);
}

void CppUnitTestTimingsCollector::startTest(CPPUNIT_NS::Test* pTest)
{
  CPPUNIT_NS::TestResultCollector::startTest(pTest);
  m_tStartTime = std::chrono::high_resolution_clock::now();
}

void CppUnitTestTimingsCollector::endTest(CPPUNIT_NS::Test* pTest)
{
  auto tElapsedTime = std::chrono::duration_cast<std::chrono::nanoseconds>(
    std::chrono::high_resolution_clock::now() - m_tStartTime);
  CPPUNIT_NS::stdCOut() << m_cSeparator << " (" << static_cast<unsigned long>(tElapsedTime.count())
                        << " ns)";
  //  CPPUNIT_NS::stdCOut() << pTest->getName() << m_cSeparator << " (" <<
  //  static_cast<unsigned long>(tElapsedTime.count()) << " ns)\n";
  CPPUNIT_NS::TestResultCollector::endTest(pTest);
}

std::string CppUnitTestTimingsCollector::secondsToText(
  const std::chrono::high_resolution_clock::time_point& tStopTime,
  const std::chrono::high_resolution_clock::time_point& tStartTime)
{
  constexpr static const unsigned long NSECS_PER_DAY       = 86400000000000;
  constexpr static const unsigned long NSECS_PER_HOUR      = 3600000000000;
  constexpr static const unsigned long NSECS_PER_MIN       = 60000000000;
  constexpr static const unsigned long NSECS_PER_SEC       = 1000000000;
  constexpr static const unsigned long NSECS_PER_MICRO_SEC = 1000;

  std::stringstream tTimeText;
  // So we will have proper punctuation for integers.
  tTimeText.imbue(std::locale(""));

  auto tElapsedTime = std::chrono::duration_cast<std::chrono::nanoseconds>(tStopTime - tStartTime);
  unsigned long nNanoSeconds = static_cast<unsigned long>(tElapsedTime.count());

  std::string sSeparator;

  auto tElapsedTimeFormatter = [&nNanoSeconds, &sSeparator, &tTimeText](
                                 const unsigned long nScalar,
                                 const char*         pTimeUnit) noexcept -> void
  {
    unsigned long nTimeUnits = nNanoSeconds / nScalar;
    nNanoSeconds -= (nTimeUnits * nScalar);
    if (nTimeUnits > 0)
    {
      tTimeText << sSeparator << nTimeUnits << ' ' << pTimeUnit << "(s)";
      sSeparator = ", ";
    }
  };

  tElapsedTimeFormatter(NSECS_PER_DAY, "day");
  tElapsedTimeFormatter(NSECS_PER_HOUR, "hour");
  tElapsedTimeFormatter(NSECS_PER_MIN, "min");
  tElapsedTimeFormatter(NSECS_PER_SEC, "sec");
  tElapsedTimeFormatter(NSECS_PER_MICRO_SEC, "µs");

  tTimeText << sSeparator << nNanoSeconds << " nsec(s)";
  return (tTimeText.str());
}

CPPUNIT_NS_END

