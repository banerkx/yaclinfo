
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CLInfo.h"

#include <algorithm>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem.hpp>
#include <iomanip>
#include <regex>
#include <sstream>

#include "CLInfoVersion.h"
#include "Macros.h"
#include "OpenCLHTMLUtilities.h"
#include "OpenCLString.h"
#include "OpenCLVersions.h"
#include "PlatformInfo_100.h"
#include "PlatformInfo_110.h"
#include "PlatformInfo_120.h"
#include "PlatformInfo_200.h"
#include "PlatformInfo_210.h"
#include "PlatformInfo_220.h"
#include "ScopedChronoTimer.h"
#include "ScopedFile.h"
#include "StringToPrimitive.h"
#include "StringTokenizer.h"

CLInfo::CLInfo() :
  ConstSingletonBase<CLInfo>(),
  m_sOpenCLOverview("https://www.khronos.org/opencl/"),
  m_fElapsedSeconds(0.0),
  m_fElapsedCPUTime(0.0),
  m_tICDInfo(),
  m_nNumberOfPlatforms(0),
  m_tPlatformIDs(),
  m_tPlatformInfo(),
  m_sCLInfoVersion(CLINFO::clinfoVersion())
{
  HostPlatformInfo::getInstance().addPlatformAttribute("clinfo version", m_sCLInfoVersion);
  getPlatformIds();
}

void CLInfo::getPlatformIds()
{
  ScopedChronoTimer tTimer(m_fElapsedSeconds, m_fElapsedCPUTime);

  // Getting the number of available platforms.
  cl_uint         nTemp   = 0;
  cl_platform_id* nTempID = nullptr;
  cl_int          nStat   = clGetPlatformIDs(nTemp, nTempID, &m_nNumberOfPlatforms);
  switch (nStat)
  {
    // Do nothing since we succeeded.
    case CL_SUCCESS:
      break;

    // nStat can only equal CL_INVALID_VALUE if:
    //   (nTemp == 0) && (nTempID != nullptr)
    //                OR
    //   (&m_nNumberOfPlatforms == nullptr) && (nTempID == nullptr)
    // Since this is not the case here, we do nothing.
    //
    case CL_INVALID_VALUE:
      break;

#ifdef CL_OUT_OF_HOST_MEMORY
    case CL_OUT_OF_HOST_MEMORY:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "Could not retrieve the number of OpenCL platforms due "
             << "to [Out of host memory].";
      throw std::runtime_error(tError.str());
    }
    break;
#endif

#ifdef CL_PLATFORM_NOT_FOUND_KHR
    case CL_PLATFORM_NOT_FOUND_KHR:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "The [cl_khr_icd] extension is enabled, but no OpenCL "
                "platforms were found.";
      throw std::runtime_error(tError.str());
    }
    break;
#endif

    default:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "Could not retrieve the number of OpenCL platforms due "
             << "to [Unknown error].";
      throw std::runtime_error(tError.str());
    }
    break;
  };

  // Now getting all m_nNumberOfPlatforms platforms.
  std::unique_ptr<cl_platform_id[]> pPlatformIDs(new cl_platform_id[m_nNumberOfPlatforms]);
  nStat = clGetPlatformIDs(m_nNumberOfPlatforms, pPlatformIDs.get(), nullptr);
  switch (nStat)
  {
    case CL_SUCCESS:
      getPlatformVersions(pPlatformIDs);
      break;

    // nStat can only equal CL_INVALID_VALUE if:
    //   (m_nNumberOfPlatforms == 0) && (nullptr != pPlatformIDs)
    //                OR
    //   (&m_nNumberOfPlatforms == nullptr) && (nTempID == nullptr)
    //
    case CL_INVALID_VALUE:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      if (((m_nNumberOfPlatforms == 0) && (nullptr != pPlatformIDs)) || (nullptr == pPlatformIDs))
      {
        tError << "Could not retrieve the OpenCL platform(s) due "
               << "to [Invalid Value].";
      }
      else
      {
        tError << "Could not retrieve the OpenCL platform(s) due "
               << "to [Unknown Error].";
      }
      throw std::runtime_error(tError.str());
    }
    break;

#ifdef CL_OUT_OF_HOST_MEMORY
    case CL_OUT_OF_HOST_MEMORY:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "Could not retrieve the OpenCL platform(s) due "
             << "to [Out of host memory].";
      throw std::runtime_error(tError.str());
    }
    break;
#endif

#ifdef CL_PLATFORM_NOT_FOUND_KHR
    case CL_PLATFORM_NOT_FOUND_KHR:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "The [cl_khr_icd] extension is enabled, but no OpenCL "
                "platforms were found.";
      throw std::runtime_error(tError.str());
    }
    break;
#endif

    default:
    {
      std::stringstream tError;
      GET_ERROR_PREFIX(tError)
      tError << "Could not retrieve the OpenCL platform(s) due "
             << "to [Unknown error].";
      throw std::runtime_error(tError.str());
    }
    break;
  };
  getPlatformInfo();
  setICDFilesInfo();
}

void CLInfo::getPlatformInfo() noexcept
{
  m_tPlatformInfo.clear();

  cl_uint i = 0;
  for (const auto& iter : m_tPlatformIDs)
  {
    m_tPlatformInfo.push_back(
      std::shared_ptr<PlatformInfo>(CLInfo::getPlatform(iter.first, i, iter.second)));
  }
  std::sort(m_tPlatformInfo.begin(), m_tPlatformInfo.end(), CLInfo::platformVersionLessThan);
}

bool CLInfo::platformVersionLessThan(
  const std::shared_ptr<PlatformInfo>& lhs,
  std::shared_ptr<PlatformInfo>&       rhs)
{
  return (lhs->platformVersion() < rhs->platformVersion());
}

void CLInfo::printIPAddresses(std::ostream& tStream) const noexcept
{
  const std::string sLabel("Local IP Address(es)            = ");
  tStream << sLabel << std::setw(IPAddresses::m_nInterfaceWidth) << std::left << "INTERFACE"
          << std::setw(IPAddresses::m_nFamilyWidth) << std::left << "ADDRESS FAMILY"
          << std::setw(IPAddresses::m_nIPAddressWidth) << std::left << "IP ADDRESS"
          << std::setw(IPAddresses::m_nSubnetMaskWidth) << std::left
          << "IPv4 SUBNET MASK/IPv6 PREFIX LENGTH" << std::setw(IPAddresses::m_nMACAddressWidth)
          << std::left << "MAC ADDRESS";
  tStream << '\n';

  const std::string sIndent(sLabel.size(), ' ');
  for (const auto& tInfo : HostPlatformInfo::getInstance().ipAddresses())
  {
    tStream << sIndent << std::setw(IPAddresses::m_nInterfaceWidth) << std::left
            << std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo)
            << std::setw(IPAddresses::m_nFamilyWidth) << std::left
            << std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(tInfo)
            << std::setw(IPAddresses::m_nIPAddressWidth) << std::left
            << std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(tInfo)
            << std::setw(IPAddresses::m_nIPAddressWidth) << std::left
            << std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(tInfo)
            << std::setw(IPAddresses::m_nMACAddressWidth) << std::left
            << std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo);
    tStream << '\n';
  }
}

void CLInfo::printResourceLimits(std::ostream& tStream) const noexcept
{
  const int nDescriptionWidth = HostPlatformInfo::getInstance().resourceLimits().descriptionWidth();
  const int nResultWidth      = HostPlatformInfo::getInstance().resourceLimits().resultWidth();
  const std::string sLabel("Resource Limits                 = ");
  tStream << sLabel << std::setw(nDescriptionWidth) << std::left << "RESOURCE"
          << std::setw(nResultWidth) << std::left << "LIMIT(s)";
  tStream << '\n';

  const std::string sIndent(sLabel.size(), ' ');
  for (const auto& tLimit : HostPlatformInfo::getInstance().resourceLimits())
  {
    tStream << sIndent << std::setw(nDescriptionWidth) << std::left
            << std::get<ResourceLimits::RESOURCE_DESCRIPTION>(tLimit) << std::setw(nResultWidth)
            << std::left << std::get<ResourceLimits::RESOURCE_RESULT>(tLimit);
    tStream << '\n';
  }
}

void CLInfo::printVideoCardInfo(std::ostream& tStream) const noexcept
{
  tStream << "Video Card(s)                   = ";
  const std::string                               sIndent("                                  ");
  const std::map<VideoCard, VideoCardProperties>& tVideoCardInfo =
    HostPlatformInfo::getInstance().videoCardInfo();
  for (const auto& i : tVideoCardInfo)
  {
    if (*tVideoCardInfo.begin() == i)
    {
      tStream << "Card Name: [" << i.first << "]\n";
    }
    else
    {
      tStream << sIndent << "Card Name: [" << i.first << "]\n";
    }
    tStream << sIndent << "Card Properties:\n";
    StringTokenizer tTokens(i.second, "\n");
    for (const auto& j : tTokens)
    {
      tStream << sIndent << sIndent2 << j << '\n';
    }
  }
}

void CLInfo::printRUsageInfo(std::ostream& tStream) const noexcept
{
  const ResourceLimits& tLimits           = HostPlatformInfo::getInstance().resourceLimits();
  const int             nDescriptionWidth = tLimits.descriptionWidth();
  const int             nResultWidth      = tLimits.resultWidth();

  std::string sLabel("Self Resource Usages            = ");
  tStream << sLabel << std::setw(nDescriptionWidth) << std::left << "DESCRIPTION"
          << std::setw(nResultWidth) << std::left << "USAGE";
  tStream << '\n';

  const std::string                                       sIndent(sLabel.size(), ' ');
  const std::vector<std::pair<std::string, std::string>>& tSelfInfo = tLimits.getRUsageSelfInfo();
  for (const auto& tResource : tSelfInfo)
  {
    tStream << sIndent << std::setw(nDescriptionWidth) << std::left << tResource.first
            << std::setw(nResultWidth) << std::left << tResource.second;
    tStream << '\n';
  }

  sLabel = "Children Resource Usages        = ";
  tStream << sLabel << std::setw(nDescriptionWidth) << std::left << "DESCRIPTION"
          << std::setw(nResultWidth) << std::left << "USAGE";
  tStream << '\n';
  const std::vector<std::pair<std::string, std::string>>& tChildrenInfo =
    tLimits.getRUsageChildrenInfo();
  for (const auto& tResource : tChildrenInfo)
  {
    tStream << sIndent << std::setw(nDescriptionWidth) << std::left << tResource.first
            << std::setw(nResultWidth) << std::left << tResource.second;
    tStream << '\n';
  }

  sLabel = "Main Thread Resource Usages     = ";
  tStream << sLabel << std::setw(nDescriptionWidth) << std::left << "DESCRIPTION"
          << std::setw(nResultWidth) << std::left << "USAGE";
  tStream << '\n';
  const std::vector<std::pair<std::string, std::string>>& tThreadInfo =
    tLimits.getRUsageThreadInfo();
  for (const auto& tResource : tThreadInfo)
  {
    tStream << sIndent << std::setw(nDescriptionWidth) << std::left << tResource.first
            << std::setw(nResultWidth) << std::left << tResource.second;
    tStream << '\n';
  }
}

void CLInfo::printMemoryAttribute(
  std::ostream&      tStream,
  const double       fMemory,
  const std::string& sLabel) const
{
  std::stringstream tMemory;
  tMemory << fMemory << " GB";
  printAttribute(tStream, sLabel, tMemory.str());
}

std::ostream& operator<<(std::ostream& tStream, const CLInfo& rhs) noexcept
{
  rhs.printAttribute(tStream, "OpenCL Overview", rhs.m_sOpenCLOverview);

  rhs.printIPAddresses(tStream);

  rhs.printAttribute(tStream, "Elapsed Time", rhs.m_fElapsedSeconds, "seconds");
  rhs.printAttribute(tStream, "Elapsed CPU Time", rhs.m_fElapsedCPUTime, "seconds");

  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().totalUsableMemory(),
    "Total Usable Memory");
  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().availableMemory(),
    "Available Memory");
  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().sharedMemory(),
    "Shared Memory");
  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().bufferMemory(),
    "Kernel Buffer Memory");
  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().totalSwap(),
    "Total Swap Space");
  rhs.printMemoryAttribute(tStream, HostPlatformInfo::getInstance().freeSwap(), "Free Swap Space");
  rhs.printMemoryAttribute(tStream, HostPlatformInfo::getInstance().highMemory(), "High Memory");
  rhs.printMemoryAttribute(
    tStream,
    HostPlatformInfo::getInstance().freeHighMemory(),
    "Free High Memory");

  rhs.printAttribute(tStream, "Uptime", HostPlatformInfo::getInstance().uptime());
  rhs.printAttribute(tStream, "Load Averages", HostPlatformInfo::getInstance().loadAverages());
  {
    ScopedNumericFormatter tFormatter(tStream);
    rhs.printAttribute(tStream, "Process Count", HostPlatformInfo::getInstance().processCount());
  }

  rhs.printVideoCardInfo(tStream);

  rhs.printResourceLimits(tStream);
  rhs.printRUsageInfo(tStream);

  rhs.printHostAttributes(tStream);

  constexpr static const int nICDFileWidth   = 35;
  constexpr static const int nOpenCLLibWidth = 20;

  static const std::string sTitle("OpenCL ICD File(s)              = ");
  tStream << std::setw(static_cast<int>(sTitle.size())) << sTitle << std::setw(nICDFileWidth)
          << "ICD File" << std::setw(nOpenCLLibWidth) << "OpenCL Library"
          << "Canonical Path\n";

  const std::string sIndent(sTitle.size(), ' ');
  for (size_t i = 0; i < rhs.m_tICDInfo.size(); ++i)
  {
    tStream << sIndent << std::setw(nICDFileWidth) << std::get<CLInfo::ICD_FILE>(rhs.m_tICDInfo[i])
            << std::setw(nOpenCLLibWidth) << std::get<CLInfo::LIBRARY_NAME>(rhs.m_tICDInfo[i])
            << std::get<CLInfo::LIBRARY_LOCATION>(rhs.m_tICDInfo[i]) << '\n';
  }

  rhs.printAttribute(tStream, "Number of Platforms", rhs.m_nNumberOfPlatforms);

  tStream << "\n==============================================================="
             "=================\n\n";
  for (size_t i = 0; i < rhs.m_tPlatformInfo.size(); ++i)
  {
    tStream << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
               "+++++++++++++++++\n";
    tStream << "PLATFORM NUMBER          = [" << i << "]" << std::endl;
    tStream << (*rhs.m_tPlatformInfo[i]);
    tStream << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
               "+++++++++++++++++\n";
  }
  tStream << "================================================================="
             "===============\n";
  return (tStream);
}

template<class T> void CLInfo::printAttribute(
  std::ostream&      tStream,
  const std::string& sLabel,
  const T&           tAttribute,
  const char*        pUnits) const noexcept
{
  constexpr static int nLabelWidth = 32;
  tStream << std::setw(nLabelWidth) << std::left << sLabel << "= [" << tAttribute;
  if (nullptr != pUnits)
  {
    tStream << ' ' << pUnits;
  }
  tStream << "]\n";
}

std::string CLInfo::italicizeUptimeUnits(std::string sUptime)
{
  std::regex tWeeks("(week\\(s\\))");
  sUptime = std::regex_replace(sUptime, tWeeks, "<i>$1</i>");

  std::regex tDays("(day\\(s\\))");
  sUptime = std::regex_replace(sUptime, tDays, "<i>$1</i>");

  std::regex tHours("(hour\\(s\\))");
  sUptime = std::regex_replace(sUptime, tHours, "<i>$1</i>");

  std::regex tMinutes("(minute\\(s\\))");
  sUptime = std::regex_replace(sUptime, tMinutes, "<i>$1</i>");

  std::regex tSeconds("(second\\(s\\))");
  sUptime = std::regex_replace(sUptime, tSeconds, "<i>$1</i>");

  return (sUptime);
}

void CLInfo::setHTMLMemoryAttributeRow(
  std::ostream&      tStream,
  const double       fMemory,
  const std::string& sLabel)
{
  setHTMLTableRow(tStream, sLabel, fMemory, OpenCLUnits("GB"));
}

void CLInfo::getHTMLOutput(
  std::ostream&      tStream,
  const std::string& sHTMLTitle,
  const std::string& sPlatformTableName) const noexcept
{
  tStream << "<!DOCTYPE html>\n";
  tStream << "<html>\n";
  tStream << "<head>\n";
  CLInfo::setScript(tStream);
  tStream << "  <title>" << sHTMLTitle << "</title>\n";

  CLInfo::setStyle(tStream, sPlatformTableName);

  tStream << "</head>\n";
  tStream << "<body>\n";

  tStream << "<h1>" << sHTMLTitle << "</h1>\n";

  tStream << "<table id=\"" << sPlatformTableName << "\" border=\"6\">\n";

  setHTMLTableRow(
    tStream,
    "OpenCL Overview",
    "<a href=\"" + m_sOpenCLOverview + "\" target=\"_blank\">Khronos OpenCL</a>");

  buildHTMLIPAddressesTable(tStream);

  setHTMLTableRow(tStream, "Elapsed Time", m_fElapsedSeconds, OpenCLUnits::m_tSeconds);
  setHTMLTableRow(tStream, "Elapsed CPU Time", m_fElapsedCPUTime, OpenCLUnits::m_tSeconds);

  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().totalUsableMemory(),
    "Total Usable Memory");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().availableMemory(),
    "Available Memory");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().sharedMemory(),
    "Shared Memory");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().bufferMemory(),
    "Kernel Buffer Memory");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().totalSwap(),
    "Total Swap Space");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().freeSwap(),
    "Free Swap Space");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().highMemory(),
    "High Memory");
  CLInfo::setHTMLMemoryAttributeRow(
    tStream,
    HostPlatformInfo::getInstance().freeHighMemory(),
    "Free High Memory");

  setHTMLTableRow(
    tStream,
    "Uptime",
    CLInfo::italicizeUptimeUnits(HostPlatformInfo::getInstance().uptime()));
  setHTMLTableRow(tStream, "Load Averages", HostPlatformInfo::getInstance().loadAverages());
  {
    ScopedNumericFormatter tFormatter(tStream);
    setHTMLTableRow(tStream, "Process Count", HostPlatformInfo::getInstance().processCount());
  }

  buildHTMLVideoCardsTable(tStream);

  buildHTMLResourceLimitsTable(tStream);
  buildHTMLRUsageInfoTables(tStream);

  setHostAttributeHTMLTableRow(tStream);

  buildOpenCLICDFilesTable(tStream);

  setHTMLTableRow(tStream, "Number Of Platforms", m_nNumberOfPlatforms);

  std::stringstream tLinks;
  for (size_t i = 0; i < m_tPlatformInfo.size(); ++i)
  {
    tLinks << "<a href=\"#platform_" << i << "\">Platform Number " << i << "</a>";
    if (i < (m_tPlatformInfo.size() - 1))
    {
      tLinks << "</br>";
    }
  }
  setHTMLTableRow(
    tStream,
    "Platform Quick Link(s)",
    tLinks.str(),
    OpenCLToolTip("Platform quick link(s)"));

  tStream << "</table>\n";

  for (size_t i = 0; i < m_tPlatformInfo.size(); ++i)
  {
    tStream << "<p id=\"platform_" << i << "\"></p>\n";
    tStream << "<table id=\"" << sPlatformTableName << "\" border=\"2\">\n";
    tStream << "  <caption style=\"text-align:left\"><h3>PLATFORM NUMBER " << i
            << "</h3></caption>\n";
    m_tPlatformInfo[i]->getHTMLOutput(tStream, i);
    tStream << "</table>\n";
  }

  tStream << "</body>\n";
  tStream << "</html>\n";
}

void CLInfo::setScript(std::ostream& tStream) noexcept
{
  tStream << "  <noscript>\n"
          << "    <div style=\"border: 5px solid red; padding: 1px\">\n"
          << "      <span style=\"color:red\">ATTENTION: JavaScript is not "
             "enabled.</span>\n"
          << "    </div>\n"
          << "  </noscript>\n"
          << "  <script type=\"text/javascript\">\n"
          << "    (function()\n"
          << "    {\n"
          << "      var blinks = document.getElementsByTagName('blink');\n"
          << "      var visibility = 'hidden';\n"
          << "      window.setInterval(function()\n"
          << "      {\n"
          << "        for (var i = blinks.length - 1; i >= 0; --i) \n"
          << "        {\n"
          << "          blinks[i].style.visibility = visibility;\n"
          << "        }\n"
          << "        visibility = (visibility == 'visible') ? 'hidden' : "
             "'visible';\n"
          << "      }, 250);\n"
          << "    })();\n"
          << "  </script>\n";
}

void CLInfo::setStyle(std::ostream& tStream, const std::string& sPlatformTableName) noexcept
{
  tStream << "  <style>\n";
  CLInfo::setPlatformTableStyle(tStream, sPlatformTableName);
  CLInfo::setDeviceNumberRowStyle(tStream);
  CLInfo::setToolTipStyle(tStream);
  tStream << "  </style>\n";
}

void CLInfo::setPlatformTableStyle(std::ostream& tStream, const std::string& sTableName) noexcept
{
  tStream << "    #" << sTableName << '\n';
  tStream << "    {\n";
  tStream << "      font-family: Arial, sans-serif;\n";
  tStream << "      border-collapse: collapse;\n";
  tStream << "    }\n";
  tStream << "    #" << sTableName << " td, #" << sTableName << " th\n";
  tStream << "    {\n";
  tStream << "      border: lpx, solid #DDDDDD;\n";
  tStream << "      text-align: left;\n";
  tStream << "      padding: 8px;\n";
  tStream << "    }\n";
  tStream << "    #" << sTableName << " tr:nth-child(even)\n";
  tStream << "    {\n";
  tStream << "      background-color: #F2F2F2;\n";
  tStream << "    }\n";
  tStream << "    #" << sTableName << " tr:hover\n";
  tStream << "    {\n";
  tStream << "      background-color: #DDD;\n";
  tStream << "    }\n";
  tStream << "    #" << sTableName << " th\n";
  tStream << "    {\n";
  tStream << "      padding-top: 12px;\n";
  tStream << "      padding-bottom: 12px;\n";
  tStream << "      text-align: left;\n";
  tStream << "      background-color: #4CAF50;\n";
  tStream << "      color: white\n";
  tStream << "    }\n";
}

void CLInfo::setDeviceNumberRowStyle(std::ostream& tStream)
{
  const std::string sBackgroundColor("#0080FF");
  tStream << "    .device_number\n";
  tStream << "    {\n";
  tStream << "      background-color: " << sBackgroundColor << ";\n";
  tStream << "    }\n";
}

void CLInfo::setToolTipStyle(std::ostream& tStream) noexcept
{
  tStream << "    .tooltip\n";
  tStream << "    {\n";
  tStream << "      position: relative;\n";
  tStream << "      display: inline-block;\n";
  tStream << "      border-bottom: lpx, dotted black;\n";
  tStream << "    }\n";
  tStream << "    .tooltip .tooltiptext\n";
  tStream << "    {\n";
  tStream << "      visibility: hidden;\n";
  tStream << "      width: 120px;\n";
  tStream << "      background-color: black;\n";
  tStream << "      color: #FFF;\n";
  tStream << "      text-align: center;\n";
  tStream << "      border-radius: 6px;\n";
  tStream << "      padding: 5px 0;\n";
  tStream << "      position: absolute;\n";
  tStream << "      z-index: 1;\n";
  tStream << "      bottom: 150%;\n";
  tStream << "      left: 50%;\n";
  tStream << "      margin-left: -60px;\n";
  tStream << "    }\n";
  tStream << "    .tooltip:hover .tooltiptext::after\n";
  tStream << "    {\n";
  tStream << "      content: \" \";\n";
  tStream << "      visibility: visible;\n";
  tStream << "      top: 100%;\n";
  tStream << "      left: 50%;\n";
  tStream << "      margin-left: -5px;\n";
  tStream << "      border-width: 5px;\n";
  tStream << "      border-style: solid;\n";
  tStream << "      border-color: black transparent transparent transparent;\n";
  tStream << "    }\n";
  tStream << "    .tooltip:hover .tooltiptext\n";
  tStream << "    {\n";
  tStream << "      visibility: visible;\n";
  tStream << "    }\n";
}

void CLInfo::buildHTMLIPAddressesTable(std::ostream& tStream) const noexcept
{
  tStream << "  <tr>\n";
  tStream << "    <td>\n";
  tStream << "      Local IP Address(es)\n";
  tStream << "    </td>\n";

  tStream << "    <td>\n";
  tStream << "      <table border=\"2\">\n";
  tStream << "        <tr>\n";
  tStream << "          <td>Interface</td>\n";
  tStream << "          <td>Address Family</td>\n";
  tStream << "          <td>IP Address</td>\n";
  tStream << "          <td>IPv4 Subnet Mask/IPv6 Prefix Length</td>\n";
  tStream << "          <td>MAC Address</td>\n";
  tStream << "        </tr>\n";

  static const std::regex tBits("bits");
  for (const auto& tInfo : HostPlatformInfo::getInstance().ipAddresses())
  {
    tStream << "        <tr>\n";
    tStream << "          <td><font size=\"2\">"
            << std::get<IPAddresses::IP_INFO_FIELD::IP_INTERFACE>(tInfo) << "</font></td>\n";
    tStream << "          <td><font size=\"2\">"
            << std::get<IPAddresses::IP_INFO_FIELD::IP_FAMILY>(tInfo) << "</font></td>\n";
    tStream << "          <td><font size=\"2\">"
            << std::get<IPAddresses::IP_INFO_FIELD::IP_ADDRESS>(tInfo) << "</font></td>\n";
    tStream << "          <td><font size=\"2\">"
            << std::regex_replace(
                 std::get<IPAddresses::IP_INFO_FIELD::SUBNET_MASK>(tInfo),
                 tBits,
                 "<i>bits</i>")
            << "</font></td>\n";
    tStream << "          <td><font size=\"2\">"
            << std::get<IPAddresses::IP_INFO_FIELD::MAC_ADDRESS>(tInfo) << "</font></td>\n";
    tStream << "        </tr>\n";
  }

  tStream << "      </table>\n";
  tStream << "    </td>\n";
  tStream << "  </tr>\n";
}

void CLInfo::buildHTMLResourceLimitsTable(std::ostream& tStream) const noexcept
{
  tStream << "  <tr>\n";
  tStream << "    <td>\n";
  tStream << "      Resource Limits\n";
  tStream << "    </td>\n";

  tStream << "    <td>\n";
  tStream << "      <table border=\"2\">\n";
  tStream << "        <tr>\n";
  tStream << "          <td>Resource</td>\n";
  tStream << "          <td>Limit(s)</td>\n";
  tStream << "        </tr>\n";

  for (const auto& tLimit : HostPlatformInfo::getInstance().resourceLimits())
  {
    tStream << "        <tr>\n";
    tStream << "          <td><font size=\"2\">"
            << std::get<ResourceLimits::RESOURCE_DESCRIPTION>(tLimit) << "</font></td>\n";
    tStream << "          <td><font size=\"2\">"
            << CLInfo::setHTMLUnits(std::get<ResourceLimits::RESOURCE_RESULT>(tLimit))
            << "</font></td>\n";
    tStream << "        </tr>\n";
  }

  tStream << "      </table>\n";
  tStream << "    </td>\n";
  tStream << "  </tr>\n";
}

void CLInfo::buildHTMLVideoCardsTable(std::ostream& tStream) const noexcept
{
  tStream << "  <tr>\n";
  tStream << "    <td>Video Card(s)</td>\n";
  tStream << "    <td>\n";
  tStream << "      <table border=\"2\">\n";

  const std::map<VideoCard, VideoCardProperties>& tVideoCardInfo =
    HostPlatformInfo::getInstance().videoCardInfo();
  for (const auto& i : tVideoCardInfo)
  {
    tStream << "        <tr>\n";
    tStream << "          <td><font size=\"2\">"
            << "Card Name: " << i.first << "<br> Card Properties: <br>\n";

    StringTokenizer tTokens(i.second, "\n");
    for (const auto& j : tTokens)
    {
      tStream << "              &nbsp;&nbsp;" << CLInfo::replaceAngleBrackets(j) << "<br>\n";
    }
    tStream << "          </font></td>\n";
  }

  tStream << "          </td>\n";
  tStream << "        </tr>\n";
  tStream << "      </table>\n";
  tStream << "    </td>\n";
  tStream << "  </tr>\n";
}

void CLInfo::buildHTMLRUsageInfoTables(std::ostream& tStream) const noexcept
{
  CLInfo::buildHTMLRUsageInfoTable(
    tStream,
    "Self Resource Usages",
    HostPlatformInfo::getInstance().resourceLimits().getRUsageSelfInfo());

  CLInfo::buildHTMLRUsageInfoTable(
    tStream,
    "Children Resource Usages",
    HostPlatformInfo::getInstance().resourceLimits().getRUsageChildrenInfo());

  CLInfo::buildHTMLRUsageInfoTable(
    tStream,
    "Main Thread Resource Usages",
    HostPlatformInfo::getInstance().resourceLimits().getRUsageThreadInfo());
}

void CLInfo::buildHTMLRUsageInfoTable(
  std::ostream&                                           tStream,
  const std::string&                                      sLabel,
  const std::vector<std::pair<std::string, std::string>>& tResourceUsages) const noexcept
{
  tStream << "  <tr>\n";
  tStream << "    <td>\n";
  tStream << "      " << sLabel << "\n";
  tStream << "    </td>\n";

  tStream << "    <td>\n";
  tStream << "      <table border=\"2\">\n";
  tStream << "        <tr>\n";
  tStream << "          <td>DESCRIPTION</td>\n";
  tStream << "          <td>USAGE</td>\n";
  tStream << "        </tr>\n";

  for (const auto& tUsage : tResourceUsages)
  {
    tStream << "        <tr>\n";
    tStream << "          <td><font size=\"2\">" << tUsage.first << "</font></td>\n";
    tStream << "          <td><font size=\"2\">" << CLInfo::setHTMLUnits(tUsage.second)
            << "</font></td>\n";
    tStream << "        </tr>\n";
  }

  tStream << "      </table>\n";
  tStream << "    </td>\n";
  tStream << "  </tr>\n";
}

std::vector<std::string> CLInfo::getICDFiles()
{
  std::vector<std::string>      tICDFiles;
  const boost::filesystem::path tDirectory("/etc/OpenCL/vendors");
  try
  {
    static const std::string sICDExtension(".icd");

    if (true == boost::filesystem::is_directory(tDirectory))
    {
      for (auto i = boost::filesystem::directory_iterator(tDirectory);
           i != boost::filesystem::directory_iterator();
           ++i)
      {
        if ((sICDExtension == i->path().extension()) && (true == i->path().has_stem()))
        {
          if (
            (true == boost::filesystem::is_regular_file(i->path())) &&
            (false == boost::filesystem::is_symlink(i->path())))
          {
            tICDFiles.push_back(boost::filesystem::absolute(i->path()).string());
          }
          else if (true == boost::filesystem::is_symlink(i->path()))
          {
            std::stringstream       tLinksInfo;
            boost::filesystem::path pLink(i->path());
            for (; boost::filesystem::is_symlink(pLink);
                 pLink = boost::filesystem::read_symlink(pLink))
            {
              tLinksInfo << boost::filesystem::absolute(pLink).string() << " --> ";
            }
            // Ensuring that the link actually exists.
            if (true == boost::filesystem::exists(boost::filesystem::absolute(pLink)))
            {
              tLinksInfo << boost::filesystem::absolute(pLink).string();
              tICDFiles.push_back(tLinksInfo.str());
            }
          }
        }
      }
    }
    else
    {
      std::stringstream tError;
      tError << "ERROR: [" << tDirectory << "] is not a directory.";
      tICDFiles.push_back(tError.str());
    }
  }
  catch (const boost::filesystem::filesystem_error& e)
  {
    tICDFiles.push_back(e.what());
  }
  catch (...)
  {
    std::stringstream tException;
    tException << "ERROR: Function = [" << __FUNCTION__ << "] File = [" << __FILE__ << "] Line = ["
               << __LINE__ << "] Unknown exception.";
    tICDFiles.push_back(tException.str());
  }

  if (true == tICDFiles.empty())
  {
    std::stringstream tError;
    tError << "ERROR: No OpenCL ICD files found in directory [" << tDirectory << "].";
    tICDFiles.push_back(tError.str());
  }
  std::sort(tICDFiles.begin(), tICDFiles.end());
  return (tICDFiles);
}

std::string CLInfo::getOpenCLLibraryName(const std::string& sICDFile)
{
  ScopedInputFile tReader(sICDFile);
  if (true == tReader.is_open())
  {
    std::string sLibrary;
    std::string sLine;
    while (std::getline(tReader, sLine))
    {
      if (('#' == sLine[0]) || (true == sLine.empty()))
      {
        continue;
      }

      const size_t nPoundIndex = sLine.find('#');
      if (std::string::npos != nPoundIndex)
      {
        sLibrary = sLine.substr(0, nPoundIndex - 1);
        break;
      }
      else
      {
        sLibrary = sLine;
      }
    }

    boost::algorithm::trim(sLibrary);
    if (true == sLibrary.empty())
    {
      std::stringstream tError0;
      tError0 << "ERROR: Did not find a library in ICD file [" << sICDFile << "].";
      return (tError0.str());
    }

    return (sLibrary);
  }

  std::stringstream tError;
  tError << "ERROR: Could not open ICD file [" << sICDFile << "] for reading.";
  return (tError.str());
}

std::string CLInfo::getLibraryPath(
  const std::vector<std::string>& tLibDirs,
  const std::string&              sLibrary)
{
  if (0 == sLibrary.find("ERROR: "))
  {
    return ("N/A");
  }

  for (size_t i = 0; i < tLibDirs.size(); ++i)
  {
    const std::string sLibFile(tLibDirs[i] + "/" + sLibrary);
    if (true == boost::filesystem::exists(sLibFile))
    {
      return (boost::filesystem::canonical(sLibFile).string());
    }
  }
  return ("ERROR: Library not found.");
}

void CLInfo::setICDFilesInfo()
{
  const std::vector<std::string>& tLibDirs = HostPlatformInfo::getLibraryDirectories();

  std::vector<std::string> tICDFiles(CLInfo::getICDFiles());
  if (true == tLibDirs.empty())
  {
    for (size_t i = 0; i < tICDFiles.size(); ++i)
    {
      m_tICDInfo.emplace_back(
        tICDFiles[i],
        getOpenCLLibraryName(tICDFiles[i]),
        std::string("ERROR: Did not find any shared object file directories "
                    "in [LD_LIBRARY_PATH]."));
    }
    return;
  }

  std::string sLibrary;
  for (size_t i = 0; i < tICDFiles.size(); ++i)
  {
    sLibrary = getOpenCLLibraryName(tICDFiles[i]);
    m_tICDInfo.emplace_back(tICDFiles[i], sLibrary, getLibraryPath(tLibDirs, sLibrary));
  }
}

void CLInfo::buildOpenCLICDFilesTable(std::ostream& tStream) const
{
  tStream << "  <tr>\n"
          << "    <td>OpenCL ICD File(s)</td>\n"
          << "    <td>\n"
          << "      <table border=\"2\">\n"
          << "        <tr>\n"
          << "          <td>ICD File</td>\n"
          << "          <td>OpenCL Library</td>\n"
          << "          <td>Canonical Path</td>\n"
          << "        </tr>\n";
  for (size_t i = 0; i < m_tICDInfo.size(); ++i)
  {
    tStream << "        <tr>\n"
            << "          <td><font size=\"2\">" << std::get<ICD_FILE>(m_tICDInfo[i])
            << "</font></td>\n"
            << "          <td><font size=\"2\">" << std::get<LIBRARY_NAME>(m_tICDInfo[i])
            << "</font></td>\n"
            << "          <td><font size=\"2\">" << std::get<LIBRARY_LOCATION>(m_tICDInfo[i])
            << "</font></td>\n"
            << "        </tr>\n";
  }
  tStream << "      </table>\n"
          << "    </td>\n"
          << "  </tr>\n";
}

std::string CLInfo::setHTMLUnits(const std::string& s)
{
  static const std::regex tBytes("(^.*) bytes$");
  static const std::regex tMicroSeconds("(^.*) µsec$");
  static const std::regex tPages("(^.*) pages$");
  static const std::regex tSeconds("(^.*) seconds$");
  static const std::regex tKiloBytes("(^.*) kB$");

  if (true == std::regex_match(s, tBytes))
  {
    return (std::regex_replace(s, tBytes, "$1" + getHTMLUnits(OpenCLUnits::m_tBytes)));
  }
  else if (true == std::regex_match(s, tMicroSeconds))
  {
    return (
      std::regex_replace(s, tMicroSeconds, "$1" + getHTMLUnits(OpenCLUnits::m_tMicroSeconds)));
  }
  else if (true == std::regex_match(s, tPages))
  {
    return (std::regex_replace(s, tPages, "$1" + getHTMLUnits(OpenCLUnits::m_tPages)));
  }
  else if (true == std::regex_match(s, tSeconds))
  {
    return (std::regex_replace(s, tSeconds, "$1" + getHTMLUnits(OpenCLUnits::m_tSeconds)));
  }
  else if (true == std::regex_match(s, tKiloBytes))
  {
    return (std::regex_replace(s, tKiloBytes, "$1" + getHTMLUnits(OpenCLUnits::m_tKiloBytes)));
  }

  return (s);
}

std::string CLInfo::processHtmlLine(const std::string& s)
{
  static const std::regex tQuote("\"");
  return (std::regex_replace(s, tQuote, "\\\""));
}

std::string CLInfo::replaceAngleBrackets(const std::string& s)
{
  static const std::regex tLessThan("<");
  static const std::regex tGreaterThan(">");
  return (std::regex_replace(std::regex_replace(s, tLessThan, "&lt;"), tGreaterThan, "&gt;"));
}

std::string CLInfo::processTextLine(const std::string& s)
{
  static const std::regex tEOL("$");
  static const std::regex tSpace(" ");
  return (std::regex_replace(
    std::regex_replace(CLInfo::replaceAngleBrackets(s), tEOL, "<br>"),
    tSpace,
    "&nbsp;"));
}

void CLInfo::createHTMLFrameFile(
  const std::string&       sFrameFile,
  const std::stringstream& tCLInfoHtml,
  const std::stringstream& tCLInfoText)
{
  StringTokenizer tHtmlTokens(tCLInfoHtml.str(), '\n');
  StringTokenizer tTextTokens(tCLInfoText.str(), '\n');

  ScopedOutputFile tHTMLFrame(sFrameFile);

  tHTMLFrame << "<!DOCTYPE html>\n"
             << "<html>\n"
             << "  <head>\n"
             << "    <title>OpenCL Information Frames</title>\n"
             << "    <meta charset=\"utf-8\">\n"
             << "    <body>\n"
             << "      <noscript>\n"
             << "        <div style=\"border: 10px solid red; padding: 1px\">\n"
             << "          <span style=\"color:red\">ATTENTION: JavaScript is not "
                "enabled.</span>\n"
             << "        </div>\n"
             << "      </noscript>\n"
             << "      <iframe id=\"html_frame\" "
                "src=\"javascript:parent.clinfo_html()\" "
                "style=\"border:none;height:99vh;width:50%;float:left;\" "
                "scrolling=\"auto\">\n"
             << "        <p>ATTENTION: Your browser does not support iframes.</p>\n"
             << "      </iframe>\n"
             << "      <iframe id=\"text_frame\" "
                "src=\"javascript:parent.clinfo_text()\" "
                "style=\"border:none;height:99vh;width:50%;float:right;\" "
                "scrolling=\"auto\">\n"
             << "        <p>ATTENTION: Your browser does not support iframes.</p>\n"
             << "      </iframe>\n"
             << "    <div class=\"clear\">\n"
             << "    </div>\n"
             << "  </body>\n"
             << "    <script language=\"JavaScript\" type=\"text/javascript\">\n"
             << "      <!--\n"
             << "        function top_frame()\n"
             << "        {\n"
             << "          top_frame_page  = \"<!DOCTYPE html>\";\n"
             << "          top_frame_page += \"<html>\";\n"
             << "          top_frame_page += \"   <head>\";\n"
             << "          top_frame_page += \"      <title>OpenCL Information "
                "Frames</title>\";\n"
             << "          top_frame_page += \"   </head>\";\n"
             << "          top_frame_page += \"   <h2><center>OpenCL "
                "Information</center></h2>\";\n"
             << "          top_frame_page += \"</html>\";\n"
             << "          return (top_frame_page);\n"
             << "        }\n\n"
             << "        function clinfo_html()\n"
             << "        {\n"
             << "          clinfo_html_page = \"\";\n";

  for (const auto& sHtmlLine : tHtmlTokens)
  {
    static const std::regex tPlatform("(\"#platform_[0-9]+\")([^ ]*)");
    static const std::regex tPlatformDevice("(\"#platform_[0-9]+_device_[0-9]+\")([^ ]*)");
    static const std::regex tPlatformQuickLinks("Platform Quick Link\\(s\\)");
    static const std::regex tDeviceQuickLinks("Device Quick Link\\(s\\)");

    if (
      (true == std::regex_search(sHtmlLine, tPlatform)) ||
      (true == std::regex_search(sHtmlLine, tPlatformDevice)) ||
      (true == std::regex_search(sHtmlLine, tPlatformQuickLinks)) ||
      (true == std::regex_search(sHtmlLine, tDeviceQuickLinks)))
    {
    }
    else
    {
      tHTMLFrame << "          clinfo_html_page += \"" << CLInfo::processHtmlLine(sHtmlLine)
                 << "\";\n";
    }
  }

  tHTMLFrame << "          return (clinfo_html_page);\n"
             << "        }\n\n"
             << "        function clinfo_text()\n"
             << "        {\n"
             << "          clinfo_text_page  = \"<html>\";\n"
             << "          clinfo_text_page += \"  <font face=\\\"Courier New, "
                "Courier, monospace\\\" size=\\\"2\\\">\";\n";

  for (const auto& sTextLine : tTextTokens)
  {
    tHTMLFrame << "          clinfo_text_page += \"" << CLInfo::processTextLine(sTextLine)
               << "\"\n";
  }

  tHTMLFrame << "          clinfo_text_page += \"  </font>\";\n"
             << "          clinfo_text_page += \"</html>\";\n"
             << "          return (clinfo_text_page);\n"
             << "        }\n\n"
             << "      -->\n"
             << "    </script>\n"
             << "  </head>\n"
             << "</html>\n"
             << "\n";
}

void CLInfo::getPlatformVersions(std::unique_ptr<cl_platform_id[]>& pPlatformIDs)
{
  constexpr static size_t nBufferSize = 2048;
  char                    pPlatformInfoBuffer[nBufferSize + 1];
  size_t                  nParamValueSize = 0;

  for (size_t i = 0; i < m_nNumberOfPlatforms; ++i)
  {
    std::memset(pPlatformInfoBuffer, 0, nBufferSize + 1);
    cl_int nStat = clGetPlatformInfo(
      pPlatformIDs[i],
      CL_PLATFORM_VERSION,
      nBufferSize,
      pPlatformInfoBuffer,
      &nParamValueSize);

    switch (nStat)
    {
      case CL_SUCCESS:
        m_tPlatformIDs.insert(
          std::make_pair(pPlatformIDs[i], CLInfo::getPlatformVersion(pPlatformInfoBuffer)));
        break;

      case CL_INVALID_PLATFORM:
      {
        std::stringstream tError;
        GET_ERROR_PREFIX(tError)
        constexpr static const int nFieldWidth = 4;
        tError << "Could not get platform version due to [Invalid platform ID: "
                  "CL_PLATFORM_VERSION ==> "
               << "0x" << std::internal << std::setfill('0') << std::setw(nFieldWidth) << std::hex
               << std::uppercase << CL_PLATFORM_VERSION << " = " << std::nouppercase << std::dec
               << CL_PLATFORM_VERSION << "].";
        throw std::runtime_error(tError.str());
      }
      break;

      // This error can only occur if:
      // the parameter name (CL_PLATFORM_VERSION here) is invalid
      //                           OR
      // (nBufferSize < nParamValueSize) && (nullptr != pPlatformInfoBuffer)
      // Since neither of the two possibilities apply here, we do nothing.
      case CL_INVALID_VALUE:
        break;

#ifdef CL_OUT_OF_HOST_MEMORY
      case CL_OUT_OF_HOST_MEMORY:
      {
        std::stringstream tError;
        GET_ERROR_PREFIX(tError)
        tError << "Could not get platform version due to [Out of host memory].";
        throw std::runtime_error(tError.str());
      }
      break;
#endif

      default:
      {
        std::stringstream tError;
        GET_ERROR_PREFIX(tError)
        tError << "Could not get platform version due to [Unknown error].";
        throw std::runtime_error(tError.str());
      }
      break;
    };
  }
}

unsigned short CLInfo::getPlatformVersion(const std::string& sVersionInfo)
{
  StringTokenizer      tTokens(sVersionInfo);
  bool                 bSuccess       = false;
  double               fVersion       = stringToPrimitive<double>(tTokens[1], bSuccess);
  const unsigned short nVersionFactor = 100;
  if (true == bSuccess)
  {
    return (static_cast<unsigned short>((fVersion * nVersionFactor)));
  }
  std::stringstream tError;
  GET_ERROR_PREFIX(tError)
  tError << "Could not convert [" << tTokens[1] << "] to double.";
  throw std::runtime_error(tError.str());
}

PlatformInfo* CLInfo::getPlatform(
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nPlatformVersion)
{
  PlatformInfo* pPlatform = nullptr;

  switch (static_cast<OpenCL_Version>(nPlatformVersion))
  {
    case OpenCL_Version::OPENCL_100:
      pPlatform = new PlatformInfo_100(tPlatformID, nPlatformNumber);
      break;

    case OpenCL_Version::OPENCL_110:
      pPlatform = new PlatformInfo_110(tPlatformID, nPlatformNumber);
      break;

    case OpenCL_Version::OPENCL_120:
      pPlatform = new PlatformInfo_120(tPlatformID, nPlatformNumber);
      break;

    case OpenCL_Version::OPENCL_200:
      pPlatform = new PlatformInfo_200(tPlatformID, nPlatformNumber);
      break;

    case OpenCL_Version::OPENCL_210:
      pPlatform = new PlatformInfo_210(tPlatformID, nPlatformNumber);
      break;

    case OpenCL_Version::OPENCL_220:
      pPlatform = new PlatformInfo_220(tPlatformID, nPlatformNumber);
      break;

      //    qq
      //    case OpenCL_Version::OPENCL_300:
      //      pPlatform = new PlatformInfo_300(tPlatformID, nPlatformNumber);
      //    break;

    default:
    {
      std::stringstream   tError;
      static const double fVersionMultiplier = 0.01;
      tError << "ERROR: [" << (static_cast<double>(nPlatformVersion) * fVersionMultiplier)
             << "] is an unrecognized OpenCL version.";
      throw std::runtime_error(tError.str());
    }
    break;
  };

  pPlatform->init();
  return (pPlatform);
}

void CLInfo::printHostAttributes(std::ostream& tStream) const
{
  for (const auto& tHostAttribute : HostPlatformInfo::getInstance().hostPlatformAttributes())
  {
    printAttribute(tStream, tHostAttribute.first, tHostAttribute.second);
  }
}

void CLInfo::setHostAttributeHTMLTableRow(std::ostream& tStream) const
{
  for (const auto& tHostAttribute : HostPlatformInfo::getInstance().hostPlatformAttributes())
  {
    // If the label is "core file size", we ensure that the units, "bytes",
    // is Italicized for HTML output.
    const std::string sLabel(tHostAttribute.first);
    if (0 == sLabel.find("core file size"))
    {
      const std::string sValue(tHostAttribute.second);
      size_t            nBytes = sValue.find(" bytes");
      setHTMLTableRow(tStream, sLabel, sValue.substr(0, nBytes), OpenCLUnits::m_tBytes);
    }
    else
    {
      setHTMLTableRow(tStream, sLabel, tHostAttribute.second);
    }
  }
}

