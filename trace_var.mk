
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef TRACE_VAR_MK_INCLUDE_GUARD # {
TRACE_VAR_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# Traces the usage of the specified makefile variable.                         #
#                                                                              #
# The $(warning ...) function writes its message to stderr and returns the     #
# empty string. At a high level, this tracer code changes the value of the     #
# traced variable to include a $(warning ...) message. Along with the actual   #
# warning message, make prints the name of the makefile in use and the line    #
# number.                                                                      #
# Example usage:                                                               #
#   make TRACE_VAR=MY_VAR                                                      #
#                                                                              #
# NOTE: This makefile must be included before that first use of the makefile   #
#       variable of interest in order to see every instance of the variables's #
#       use.                                                                   #
# From: The GNU Make Book by JOHN GRAHAM-CUMMING                               #
################################################################################
ifdef TRACE_VAR # {
.PHONY: _trace _value
_trace:
	+@$(MAKE_NO_DIR) TRACE_VAR= $(TRACE_VAR)='$$(warning TRACE_VAR $(TRACE_VAR))$(shell $(MAKE) TRACE_VAR=$(TRACE_VAR) _value)'
_value:
	@echo '$(value $(TRACE_VAR))'
endif # }

endif # }

