
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cppunit/TestSuite.h>

#include <string>

namespace CppUnitTestRunner
{
  /**
   * usage
   * @brief prints usage information to stdout and exits
   * @param sProgramName - the program name
   * @param sDefaultTestsResultsFile - default test results file
   * @param sDefaultTestsTitle - default tests title
   * @param nExitValue - exit value
   */
  void usage(
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const short        nExitValue);

  /**
   * createOptionsString
   * @brief assembles the command line options string
   * @param pOptions - holds the command line options information
   * @return command line options string meant to be used by getopt_long()
   */
  std::string createOptionsString(const struct option* pOptions);

  /**
   * handleOptionError
   * @brief processes a command line option for error
   * @param cOpt - the command line option in question
   * @param sProgramName - the program name
   * @param sDefaultTestsResultsFile - default test results file
   * @param sDefaultTestsTitle - default tests title
   * @param sOptions - command line options
   */
  void handleOptionError(
    const char         cOpt,
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const std::string& sOptions);

  /**
   * ensureTextExtension
   * @brief ensures the the input file has a ".txt" extension
   * @param sFile - input file
   */
  void ensureTextExtension(std::string& sFile);

  /**
   * ensureTextExtension
   * @brief ensures the the input file has an ".xml" extension
   * @param sFile - input file
   */
  void ensureXMLExtension(std::string& sFile);

  /**
   * processCommandLineOptions
   * @brief processes command line options
   * @param sDefaultTestsResultsFile - default name for the tests results file
   * @param sTestsResultsFile - will hold the name of the tests results file
   * @param bNoElapsedTime - flag for recording the elapsed tests' times
   * @param sDefaultTestsTitle - default tests title
   * @param sTestsTitle - will hold the tests title
   * @param bXML - flag for XML output
   * @param argc - size of argv array
   * @param argv - array of command line arguments
   */
  void processCommandLineOptions(
    const std::string& sDefaultTestsResultsFile,
    std::string&       sTestsResultsFile,
    bool&              bNoElapsedTime,
    const std::string& sDefaultTestsTitle,
    std::string&       sTestsTitle,
    bool&              bXML,
    int                argc,
    char**             argv);

  /**
   * Executes the unit tests.
   *   @param sTestsResultsFile - the output file for the unit test results
   *   @param sTestsTitle - the title of the unit tests
   *   @param cSeparator - separator character for unit test name and execution
   * time
   *   @param bNoElapsedTime - flag to determine if unit test execution times are
   * to be collected or not
   *   @param bXML - flag for XML output
   *   @param tTestSuite - suite of unit tests (all the tests must be added to
   * this object before this function is aclled)
   *   @return - 0 if all the unit tests were successful, 1 otherwise
   */
  int executeCppUnitTests(
    const std::string&     sTestsResultsFile,
    const std::string&     sTestsTitle,
    const char             cSeparator,
    const bool             bNoElapsedTime,
    const bool             bXML,
    CPPUNIT_NS::TestSuite& tTestSuite);
}  // namespace CppUnitTestRunner

