# README clinfo

**Y**et **a**nother Open**CL** **info**rmation application.
This information can be displayed to stdout or saved to text and/or HTML files.

**NOTE**: All commands are relative to the root directory of your local repository.

### Prerequisites

A functioning OpenCL installation. Additionally, the following software is required:

| Prerequisite                         | Link                                                                                                                                                    |
| ------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Khronos Unified OpenCL C API Headers | <a href="https://github.com/KhronosGroup/OpenCL-Headers.git" style="color:blue;" target="_blank">https://github.com/KhronosGroup/OpenCL-Headers.git</a> |

The following dynamic libraries must be available on your system:

| Dynamic Library        |
| ---------------------- |
| libboost_atomic.so     |
| libboost_filesystem.so |
| libboost_system.so     |
| libcrypto.so           |
| libncurses.so          |
| libpthread.so          |

Some makefile targets:

| Target                 | Description                                                                            |
| ---------------------- | -------------------------------------------------------------------------------------- |
| Simply entering "make" | Compiles the executable yaclinfo                                                       |
| clean                  | Deletes all files created by Makefile                                                  |
| cloc                   | Shows lines of code counts in stdout (requires cloc)                                   |
| digest_tables          | Prints message digests as simple ASCII tables                                          |
| digests                | Prints message digests to stdout                                                       |
| doxygen                | Creates HTML documentation (requires doxygen)                                          |
| help                   | Shows information for several makefile targets                                         |
| install                | Copies the executable yaclinfo to ${HOME}/bin                                          |
| log                    | Compiles the executable yaclinfo and saves all make output to yaclinfo_compilation.log |
| opencl                 | Displays information about the OpenCL SDK in stdout                                    |
| valgrind               | Executes valgrind and saves the output to valgrind_yaclinfo.log (requires valgrind)    |

Some makefile variables:

| Variable                         | Value                                 | File In Which Defined |
| -------------------------------- | ------------------------------------- | --------------------- |
| BOOST                            | /opt/boost/include                    | boost.mk              |
| BOOST_LIB_DIR                    | /opt/boost/lib64                      | boost.mk              |
| CXX                              | g++                                   | vars_common.mk        |
| KHRONOS_OPENCL_C_UNIFIED_HEADERS | /usr/include                          | vars_common.mk        |
| OPENCL_ICD_PATH                  | /etc/OpenCL/vendors                   | opencl.mk             |
| OPENCL_LIB_SO                    | /opt/AMDAPP/lib/x86_64/libOpenCL.so.1 | vars_common.mk        |

You **must set these variables** for your build environment.

An example of setting these makefile variables on the command line and compiling:<br>

```
cd clinfo
make BOOST=/opt/boost/include BOOST_LIB_DIR=/opt/boost/lib64 CXX=g++ KHRONOS_OPENCL_C_UNIFIED_HEADERS=/usr/include OPENCL_ICD_PATH=/etc/OpenCL/vendors OPENCL_LIB_SO=/opt/AMDAPP/lib/x86_64/libOpenCL.so.1
```

In general, use the following to compile after updating the above makefile variables for your build environment:<br>

```
cd clinfo
make
```

### Installation

Upon successful compilation, the executable clinfo will be created in the current directory.
Executing the following will place a stripped copy of clinfo in ${HOME}/bin:

```
cd clinfo
make install
```

### doxygen Documentation

To generate the doxygen documentation, enter:

```
cd clinfo
make doxygen
```

The files will be placed in the subdirectory dOxygen of the current directory.

### Executing clinfo

**NOTE**: If either the effective user id or the effective group id is that of root, both id 0, then this application will abort.<br>

To display the OpenCL information to stdout, simply enter:

```
./clinfo
```

To get complete usage information, enter:

```
./clinfo --help
```

### Deployment

After compiling, copy the executable to the desired directory.

### Build Environment

### Testing

Tested with the following OpenCL implementations:

| Implementation                   |
| -------------------------------- |
| OpenCL 1.1 Mesa 12.0.3           |
| OpenCL 1.1 Mesa 20.3.5           |
| OpenCL 1.2 AMD-APP (1214.3)      |
| OpenCL 1.2 pocl 1.5, LLVM 11.0.0 |
| OpenCL 2.0 pocl 0.13, LLVM 3.8.0 |

### Author

**K. Banerjee**

### License

Copyright © 2018-2023 K. Banerjee
This project is licensed under the GPL, version 3.0. See:<br>
<a href="https://www.gnu.org/licenses/gpl-3.0.en.html" style="color:blue;" target="_blank">https://www.gnu.org/licenses/gpl-3.0.en.html</a>
<br>
<a href="https://www.gnu.org/licenses/gpl-3.0.txt" style="color:blue;" target="_blank">https://www.gnu.org/licenses/gpl-3.0.txt</a>

