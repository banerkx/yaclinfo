
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef VARS_COMMON_MK_INCLUDE_GUARD # {
VARS_COMMON_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash
DEBUG_SHELL=/bin/bash -xv

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

include $(PROJECT_ROOT)/platform.mk

GPL3_HTML=https://www.gnu.org/licenses/gpl-3.0.en.html
GPL3_TEXT=https://www.gnu.org/licenses/gpl-3.0.txt

################################################################################
# Removes the specified file from the input list if the specified file does    #
# not exist.                                                                   #
#                                                                              #
# $(1) - specified file                                                        #
# $(2) - file or directory to be removed from $(3)                             #
# $(3) - list of files and/or directories from which $(2) should be removed    #
################################################################################
define check_header_file
$(if $(wildcard $(1)),$(3),$(filter-out $(2),$(3)))
endef

################################################################################
# Reverses the input list.                                                     #
#                                                                              #
# $(1) - list                                                                  #
################################################################################
reverse_list=$(strip $(if $(1),$(call reverse_list,$(wordlist 2,$(words $(1)),$(1)))) $(firstword $(1)))

################################################################################
# Returns those strings that contain the specified substring (which could be a #
# single character).                                                           #
#                                                                              #
# See:                                                                         #
#   https://stackoverflow.com/questions/6145041/makefile-filter-out-strings-containing-a-character
#                                                                              #
# $(1) - substring                                                             #
# $(2) - list of strings to be filtered                                        #
################################################################################
filter_by_sub_string=$(strip $(foreach string,$(2),$(if $(findstring $(1),$(string)),$(string))))

################################################################################
# Returns those strings that do not contain the specified substring (which     #
# could be a single character).                                                #
#                                                                              #
# See:                                                                         #
#   https://stackoverflow.com/questions/6145041/makefile-filter-out-strings-containing-a-character
#                                                                              #
# $(1) - substring                                                             #
# $(2) - list of strings to be filtered                                        #
################################################################################
filter_out_by_sub_string=$(strip $(foreach string,$(2),$(if $(findstring $(1),$(string)),,$(string))))

################################################################################
# Recursive wildcard function.                                                 #
#                                                                              #
# $(1) - starting directory (leave empty to start from the current directory)  #
# $(2) - glob pattern                                                          #
################################################################################
rwildcard=$(foreach d,$(wildcard $(1)*),$(call rwildcard,$(d)/,$(2)) $(filter $(subst *,%,$(2)),$(d)))

################################################################################
# Gets the file extension of the input file.                                   #
#                                                                              #
# $(1) - input file                                                            #
################################################################################
get_file_extension=$(subst $(basename $(1)),,$(1))

################################################################################
# Replaces the directory portion of the input file with the specified input    #
# directory.                                                                   #
#                                                                              #
# Example:                                                                     #
#   NEW_PATH=$(call replace_dir,$(MY_FILE),$(NEW_DIR))                         #
#                                                                              #
# $(1) - input file                                                            #
# $(2) - input directory                                                       #
################################################################################
replace_dir=$(if $(1),$(if $(2),$(2)/$(notdir $(1))))

################################################################################
# Checks to see if the input directory exists. If the directory exists, then   #
# it is returned. Otherwise, the empty string is returned.                     #
# $(1) - specified directory                                                   #
#        Example call: MY_DIR:=$(call check_directory_exists,$(MY_DIR))        #
################################################################################
check_directory_exists=$(if $(1),$(shell ([[ -d $(1) ]] && echo $(1)) || echo ''),"")

################################################################################
# Climbs down the directory tree, starting from '/' and ending at $(CURDIR),   #
# looking for the specified file. If the file is found, the closest location   #
# to $(CURDIR) is returned. If the file is not found, then the empty string is #
# returned.                                                                    #
#                                                                              #
# $(1) - file to search for                                                    #
################################################################################
find_file=$(realpath $(lastword $(foreach dir,/ $(subst /, ,$(CURDIR)),$(eval path:=$(path)/$(dir))$(wildcard $(path)/$(1)))))

################################################################################
# Gets all of the ancestor directories, as a list, starting with the input     #
# branch.                                                                      #
#                                                                              #
# $(1) - starting directory                                                    #
# $(2) - variable to store the list of ancestor directories                    #
################################################################################
define get_parent_directory
$(if $(filter-out /,$(1)),$(2) += $(1),$(2) += /)
$(if $(filter-out /,$(1)),$(call get_parent_directory,$(realpath $(dir $(1))),$(2)))
endef

################################################################################
# Finds the specified file in the nearest ancestor directory starting with     #
# $(CURDIR).                                                                   #
#                                                                              #
# NOTE: The "file" may be any type of Linux file, i.e., link, directory, etc.  #
# NOTE: Do not use the makefile variable name ANCESTOR_DIRS in any other       #
#       makefile.                                                              #
# NOTE: If the file is not found, the path will be empty.                      #
#                                                                              #
# $(1) - file to search for                                                    #
# $(2) - variable that holds the path of the found file                        #
################################################################################
define find_nearest_file
undefine ANCESTOR_DIRS
$(eval $(call get_parent_directory,$(CURDIR),ANCESTOR_DIRS))
$(2)=$(firstword $(wildcard $(addsuffix /$(1),$(ANCESTOR_DIRS))))
undefine ANCESTOR_DIRS
endef

################################################################################
# Gets the complete path to the input command.                                 #
#                                                                              #
# $(1) - command                                                               #
################################################################################
get_command=$(if $(1),$(shell { command -v $(1) || echo 'UNKNOWN'; }),EMPTY)

################################################################################
# Asserts that the input command is present on this system. This assert can be #
# used on a per target basis:                                                  #
#   target: export check_command=$(call assert_command_present,command)        #
#   target: (target prerequisites)                                             #
#   	[recipe for target]                                                      #
#                                                                              #
# $(1) - command                                                               #
################################################################################
assert_command_present=$(if $(1),$(if $(shell command -v $(1)),,$(error $(ERROR_LABEL) The command [$(1)] is not present)))

################################################################################
# Determines if the first version number is greater than the second version    #
# number.                                                                      #
#                                                                              #
# $(1) - first version number                                                  #
# $(2) - second version number                                                 #
################################################################################
ver_greater_than=$(if $(2),$(if $(shell echo -e '$(1)\n$(2)' | sort -V | tail -1 | grep -v "^$(2)$$"),T,F))

################################################################################
# Determines if the first version number is less than the second version       #
# number.                                                                      #
#                                                                              #
# $(1) - first version number                                                  #
# $(2) - second version number                                                 #
################################################################################
ver_less_than=$(if $(2),$(if $(shell echo -e '$(1)\n$(2)' | sort -V | head -1 | grep -v "^$(2)$$"),T,F))

################################################################################
# Determines if the first version number is greater than or equal to the       #
# second version number.                                                       #
#                                                                              #
# $(1) - first version number                                                  #
# $(2) - second version number                                                 #
################################################################################
ver_greater_than_eq=$(if $(1),$(if $(shell echo -e '$(1)\n$(2)' | sort -V | tail -1 | grep -v "^$(1)$$"),F,T))

################################################################################
# Determines if the first version number is less than or equal to the second   #
# version number.                                                              #
#                                                                              #
# $(1) - first version number                                                  #
# $(2) - second version number                                                 #
################################################################################
ver_less_than_eq=$(if $(1),$(if $(shell echo -e '$(1)\n$(2)' | sort -V | head -1 | grep -v "^$(1)$$"),F,T))

escaped_comma := ,
null          :=
escaped_space := $(null) $(null)
################################################################################
# Replaces each single space with ", " for the input list and surrounds the    #
# list with curly braces.                                                      #
#                                                                              #
# $(1) - list                                                                  #
################################################################################
setify={$(subst $(escaped_space),$(escaped_comma) ,$(strip $(1)))}

################################################################################
# Replaces each single space with "," for the input list.                      #
#                                                                              #
# $(1) - list                                                                  #
################################################################################
commafy=$(subst $(escaped_space),$(escaped_comma),$(strip $(1)))

################################################################################
# Converts the input variable from recursive to simple the first time the      #
# variable is used. This means that a variable that is expensive to assign     #
# does not have to be assigned until it is used for the first time.            #
#                                                                              #
# Example use:                                                                 #
#   MY_VAR=$(some expensive assignment)                                        #
#   $(call make_lazy,MY_VAR)                                                   #
#                                                                              #
# $(1) - variable name                                                         #
################################################################################
make_lazy=$(if $(1),$(eval $(1)=$$(eval $(1):=$(value $(1)))$$($(1))))

################################################################################
# Creates an HTML link string from the input URL; the link will open in a new  #
# tab.                                                                         #
#                                                                              #
# $(1) - URL, including the protocol                                           #
# $(2) - link name (optional)                                                  #
# $(3) - link color, optional; default color is blue                           #
################################################################################
create_url_link=$(eval col=style="color:$(if $(3),$(3),blue);")<a href="$(1)" $(col) target="_blank">$(subst ${HOME},$${HOME},$(if $(2),$(2),$(1)))</a>

################################################################################
# Beautifies the input HTML file with tidy.                                    #
#                                                                              #
# $(1) - html file                                                             #
################################################################################
TIDY_VERSION=$(call get_stdout_version,tidy,--version,| sed 's/.* //')
beautify_html=$(if $(TIDY),,$(eval TIDY:=$(shell command -v tidy)))$(if $(TIDY),$(eval TIDY_OPTS=-modify -file /dev/null -indent -clean -utf8 -wrap 200)$(TIDY) $(TIDY_OPTS) $(1) || :)

################################################################################
# Retruns a string with the width and height of a gif or png image file.       #
#   Example return value:                                                      #
#      width="321" height="202"                                                #
#                                                                              #
# NOTE: It is an error to call this function with an image file that is not a  #
#       gif nor a png.                                                         #
#                                                                              #
# $(1) - gif or png image file                                                 #
# $(2) - calling target name (optional)                                        #
################################################################################
get_gif_png_dims=$(if $(filter gif png,$(lastword $(subst ., ,$(1)))),$(shell file $(1) 2> /dev/null | sed 's/\(^.*, \)\([0-9]\+\)\( \?x \?\)\([0-9]\+\)\(,.*$$\)\?/width="\2" height="\4"/'),$(error $(if $(2),[$(2)]) $(ERROR_LABEL) The image file [$(1)] does not have a [gif] nor a [png] file extension))

################################################################################
# Adds a favicon to the specified HTML file.                                   #
#                                                                              #
# $(1) - favicon image file                                                    #
# $(2) - HTML file                                                             #
################################################################################
define add_favicon
cp $(1) $(dir $(2));
sed -i '\@<title>.*</title>@a<link rel="icon" type="image/x-icon" href="./$(notdir $(1))">' $(2)
endef

platform_info::
	@echo 'tidy version = [$(TIDY_VERSION)]'

################################################################################
# $(1) = target: $(@)                                                          #
# $(2) = first prerequisite: $(<)                                              #
# $(3) = newer prerequisite(s): $(?)                                           #
# $(4) = all prerequisite(s): $(^)                                             #
# $(5) = order-only prerequisite(s): $(|)                                      #
# $(6) = pattern rule stem: $(*)                                               #
#                                                                              #
# Example:                                                                     #
#   $(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))                          #
#                                                                              #
# NOTE: If it is desired to omit one of the automatic variables, pass in the   #
#       empty string. For example, if the order-only prerequisite should be    #
#       omitted, then use:                                                     #
#   $(call target_info,$(@),$(<),$(?),$(^),,$(*))                              #
#                                          ^                                   #
#                                          |                                   #
#                                          |___ the empty string               #
################################################################################
define target_info
	@                        echo 'CURDIR                     = [$(CURDIR)]'
	@$(if $(MAKECMDGOALS),   echo 'MAKECMDGOALS               = [$(MAKECMDGOALS)]')
	@$(if $(MAKEFILE_LIST),  echo 'MAKEFILE_LIST              = [$(strip $(MAKEFILE_LIST))]')
	@$(if $(MAKEFLAGS),      echo 'MAKEFLAGS                  = [$(MAKEFLAGS)]')
	@$(if $(.DEFAULT_GOAL),  echo '.DEFAULT_GOAL              = [$(.DEFAULT_GOAL)]')
	@$(if $(1),              echo 'TARGET(S)                  = [$(1)]')
	@$(if $(2),              echo 'FIRST PREREQUISITE         = [$(2)]')
	@$(if $(3),              echo 'NEWER PREREQUISITE(S)      = [$(3)]')
	@$(if $(4),              echo 'ALL PREREQUISITE(S)        = [$(4)]')
	@$(if $(5),              echo 'ORDER-ONLY PREREQUISITE(S) = [$(5)]')
	@$(if $(6),              echo 'PATTERN RULE STEM          = [$(6)]')
endef

################################################################################
# Function used to find the location of the specified jar file. If the jar     #
# is not found, then UNKNOWN is returned.                                      #
#                                                                              #
# $(1) - specified jar file                                                    #
################################################################################
find_jar_location=$(strip $(eval JAR_FILE=UNKNOWN)$(foreach p,$(subst :, ,${CLASSPATH}),$(if $(filter-out UNKNOWN,$(JAR_FILE)),,$(if $(wildcard $(dir $(p))/$(notdir $(1))),$(eval JAR_FILE=$(realpath $(dir $(p))$(notdir $(1)))))))$(JAR_FILE))

IGNORE_ERROR_KEEP_GOING=--ignore-error --keep-going
MAKE_IGNORE_ERROR_KEEP_GOING=$(MAKE) $(IGNORE_ERROR_KEEP_GOING)

NO_DIR=--no-print-directory
MAKE_NO_DIR=$(MAKE) $(NO_DIR)

NO_BUILT_IN_RULES=--no-builtin-rules
NO_DIR_BUILT_IN_RULES=$(NO_DIR) $(NO_BUILT_IN_RULES)
MAKE_NO_DIR_BUILT_IN_RULES=$(MAKE) $(NO_DIR) $(NO_BUILT_IN_RULES)

NO_BUILT_IN_VARS=--no-builtin-variables
NO_DIR_BUILT_IN_VARS=$(NO_DIR) $(NO_BUILT_IN_VARS)
MAKE_NO_DIR_BUILT_IN_VARS=$(MAKE) $(NO_DIR) $(NO_BUILT_IN_VARS)

NO_DIR_BUILT_IN_RULES_VARS=$(NO_DIR) $(NO_BUILT_IN_RULES) $(NO_BUILT_IN_VARS)
MAKE_NO_DIR_BUILT_IN_RULES_VARS=$(MAKE) $(NO_DIR) $(NO_BUILT_IN_RULES) $(NO_BUILT_IN_VARS)

.PHONY: make_vars
make_vars:
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| IGNORE_ERROR_KEEP_GOING         = [$(IGNORE_ERROR_KEEP_GOING)]                                         |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| MAKE_IGNORE_ERROR_KEEP_GOING    = [$(MAKE_IGNORE_ERROR_KEEP_GOING)]                                    |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_DIR                          = [$(NO_DIR)]                                                |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| MAKE_NO_DIR                     = [$(MAKE_NO_DIR)]                                           |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_BUILT_IN_RULES               = [$(NO_BUILT_IN_RULES)]                                                  |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_DIR_BUILT_IN_RULES           = [$(NO_DIR_BUILT_IN_RULES)]                             |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| MAKE_NO_DIR_BUILT_IN_RULES      = [$(MAKE_NO_DIR_BUILT_IN_RULES)]                        |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_BUILT_IN_VARS                = [$(NO_BUILT_IN_VARS)]                                              |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_DIR_BUILT_IN_VARS            = [$(NO_DIR_BUILT_IN_VARS)]                         |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| MAKE_NO_DIR_BUILT_IN_VARS       = [$(MAKE_NO_DIR_BUILT_IN_VARS)]                    |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| NO_DIR_BUILT_IN_RULES_VARS      = [$(NO_DIR_BUILT_IN_RULES_VARS)]      |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'
	@echo '| MAKE_NO_DIR_BUILT_IN_RULES_VARS = [$(MAKE_NO_DIR_BUILT_IN_RULES_VARS)] |'
	@echo '|---------------------------------+-----------------------------------------------------------------------|'

################################################################################
# Colors for highlighted output.                                               #
################################################################################
ESCAPE=
STOP_COLOR   =$(ESCAPE)[0m
BLACK        =$(ESCAPE)[0;30m
DARK_GRAY    =$(ESCAPE)[1;30m
LIGHT_GRAY   =$(ESCAPE)[0;37m
RED          =$(ESCAPE)[0;31m
LIGHT_RED    =$(ESCAPE)[1;31m
GREEN        =$(ESCAPE)[0;32m
LIGHT_GREEN  =$(ESCAPE)[1;32m
ORANGE       =$(ESCAPE)[0;33m
YELLOW       =$(ESCAPE)[1;33m
BLUE         =$(ESCAPE)[0;34m
LIGHT_BLUE   =$(ESCAPE)[1;34m
PURPLE       =$(ESCAPE)[0;35m
LIGHT_PURPLE =$(ESCAPE)[1;35m
CYAN         =$(ESCAPE)[0;36m
LIGHT_CYAN   =$(ESCAPE)[1;36m
WHITE        =$(ESCAPE)[1;37m

add_black=$(if $(MAKE_TERMOUT),$(BLACK)$(1)$(STOP_COLOR),$(1))
add_dark_gray=$(if $(MAKE_TERMOUT),$(DARK_GRAY)$(1)$(STOP_COLOR),$(1))
add_light_gray=$(if $(MAKE_TERMOUT),$(LIGHT_GRAY)$(1)$(STOP_COLOR),$(1))
add_red=$(if $(MAKE_TERMOUT),$(RED)$(1)$(STOP_COLOR),$(1))
add_light_red=$(if $(MAKE_TERMOUT),$(LIGHT_RED)$(1)$(STOP_COLOR),$(1))
add_green=$(if $(MAKE_TERMOUT),$(GREEN)$(1)$(STOP_COLOR),$(1))
add_light_green=$(if $(MAKE_TERMOUT),$(LIGHT_GREEN)$(1)$(STOP_COLOR),$(1))
add_orange=$(if $(MAKE_TERMOUT),$(ORANGE)$(1)$(STOP_COLOR),$(1))
add_yellow=$(if $(MAKE_TERMOUT),$(YELLOW)$(1)$(STOP_COLOR),$(1))
add_blue=$(if $(MAKE_TERMOUT),$(BLUE)$(1)$(STOP_COLOR),$(1))
add_light_blue=$(if $(MAKE_TERMOUT),$(LIGHT_BLUE)$(1)$(STOP_COLOR),$(1))
add_purple=$(if $(MAKE_TERMOUT),$(PURPLE)$(1)$(STOP_COLOR),$(1))
add_light_purple=$(if $(MAKE_TERMOUT),$(LIGHT_PURPLE)$(1)$(STOP_COLOR),$(1))
add_cyan=$(if $(MAKE_TERMOUT),$(CYAN)$(1)$(STOP_COLOR),$(1))
add_light_cyan=$(if $(MAKE_TERMOUT),$(LIGHT_CYAN)$(1)$(STOP_COLOR),$(1))
add_white=$(if $(MAKE_TERMOUT),$(WHITE)$(1)$(STOP_COLOR),$(1))

ifneq ($(MAKE_TERMOUT),) # {
INFO_LABEL=$(call add_green,INFO):
WARNING_LABEL=$(call add_yellow,WARNING):
ERROR_LABEL=$(call add_red,ERROR):
else # } {
INFO_LABEL=INFO:
WARNING_LABEL=WARNING:
ERROR_LABEL=ERROR:
endif # }

EDITOR ?= vim +1
DIFF_TOOL ?= gvimdiff -f

GCC_WEB_SITE=https://gcc.gnu.org
CXX ?= g++

BUILD_OS=$(shell { cat /etc/os-release 2> /dev/null || echo 'UNKNOWN'; } | egrep '^(PRETTY_NAME|UNKNOWN)' | sed 's/"//g;s/^.*=//')
ifeq ($(BUILD_OS),UNKNOWN) # {
  BUILD_OS=$(shell { hostnamectl 2> /dev/null || echo 'UNKNOWN'; } | egrep '(Operating System:|UNKNOWN)' | sed 's/^.*: //')
endif # }
ifeq ($(BUILD_OS),UNKNOWN) # {
  BUILD_OS=$(shell { lsb_release -a 2> /dev/null || echo 'UNKNOWN'; } | egrep '(Description:|UNKNOWN)' | sed 's/^.*\t//')
endif # }
ifeq ($(BUILD_OS),UNKNOWN) # {
  BUILD_OS=$(shell uname -r 2> /dev/null || echo 'UNKNOWN')
endif # }

ifneq ($(ZIP),) # {
################################################################################
# zip command line options:                                                    #
#   -[n]            → integer that specifies the speed of compression (0 means #
#                     no compression); fastest/least compression) is -1;       #
#                     slowest/most compression is -9                           #
#   --move          → move the specified files into the zip archive; delete    #
#                     the target files/directories after the zip archive has   #
#                     been made                                                #
#   --recurse-paths → travel the directory structure recursively               #
#   --test          → test the integrity of the new zip archive file           #
#   --verbose       → verbose output                                           #
################################################################################
ZIP_OPTS=-9 --move --recurse-paths --test --verbose
endif # }

################################################################################
# Searches the 64-bit directories in ${LD_LIBRARY_PATH} for the input dynamic  #
# library.                                                                     #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
LIB_DIRS_32bit=/usr/lib /usr/local/lib /lib
lib64_search=$(firstword $(wildcard $(addsuffix /$(1),$(filter-out $(LIB_DIRS_32bit),$(subst :, ,${LD_LIBRARY_PATH})))))

################################################################################
# Finds the realpath of the dynamic library.                                   #
# NOTE: A dynamic library name may resemble any of the following:              #
#                                                                              #
#         libxyz.so.1.2.3                                                      #
#         libxyz.1.2.3.so                                                      #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
get_shared_lib_path=$(if $(1),$(eval lib=lib$(subst lib,,$(firstword $(subst ., ,$(notdir $(1))))).so)$(realpath $(firstword $(wildcard $(addsuffix /$(lib),$(subst :, ,${LD_LIBRARY_PATH}))))))

################################################################################
# Finds the directory containing of the dynamic library.                       #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
get_dynamic_library_directory=$(dir $(call get_shared_lib_path,$(1)))

################################################################################
# Gets the soname of the dynamic library.                                      #
# NOTE: A dynamic library name may resemble any of the following:              #
#                                                                              #
#         libxyz.so.1.2.3                                                      #
#         libxyz.1.2.3.so                                                      #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
get_soname=$(if $(1),$(eval soname=$(shell objdump -p $(call get_shared_lib_path,$(1)) | grep SONAME | gawk '{print $$2}'))$(if $(soname),$(soname),NONE))

################################################################################
# Gets the version of the dynamic library.                                     #
# NOTE: A dynamic library name may resemble any of the following:              #
#                                                                              #
#         libxyz.so.1.2.3                                                      #
#         libxyz.1.2.3.so                                                      #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
get_shared_lib_version=$(if $(1),$(eval tokens=$(filter-out so,$(subst ., ,$(notdir $(call get_shared_lib_path,$(1))))))$(eval ver=$(subst $(escaped_space),.,$(filter-out $(firstword $(tokens)),$(tokens))))$(if $(ver),$(ver),UNKNOWN))

################################################################################
# Returns the library stem name.                                               #
# Examples:                                                                    #
#   libxyz.so.1.2.3 --> xyz                                                    #
#   libxyz.a        --> xyz                                                    #
#                                                                              #
# $(1) - library                                                               #
################################################################################
get_lib_stem=$(subst lib,,$(firstword $(subst ., ,$(notdir $(1)))))

################################################################################
# Will hold the string "order-only" if this version of make supports           #
# order-only prerequisites, empty otherwise.                                   #
################################################################################
ORDER_ONLY:=$(filter order-only,$(.FEATURES))

KHRONOS_OPENCL_C_UNIFIED_HEADERS ?= /usr/include

JUNK_FILES=core.* vgcore.* *gmon.out *gprof.out *.log gdb* .gdb* qq make.log *.o

.PHONY: makefile_list
makefile_list:
	@echo 'makefiles:'
	@echo $(realpath $(MAKEFILE_LIST)) | sed 's/ /\n/g'

ifneq ($(EXE) $(filter $(CURDIR),$(PROJECT_ROOT)),) # {
################################################################################
# NOTE: The -Otarget and --output-sync=target command line options are only    #
#       available for make versions >= 4.0.                                    #
################################################################################
.PHONY: log
log: ERROR_STRINGS:=ld: | error:| Error [1-9]| warning:| WARNING: |missing separator|multiple definition|No rule to make target|\#pragma message: |undefined reference to|bash: -c: line
log: MAKE_LOG_OPTIONS=--ignore-errors --keep-going $(if $(filter T,$(call ver_greater_than_eq,$(MAKE_VERSION),4.0)),--output-sync=target,--jobs=1)
log: LOG_FILE=$(notdir $(CURDIR))_compilation.log
log:
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@rm -f $(LOG_FILE)
	+@$(MAKE) $(MAKE_LOG_OPTIONS) 2>&1 | egrep -v ': warning: (.*boost: linker input file unused because linking not done|-jN forced in submake: disabling jobserver mode\.)' | grep -v ": warning: /opt/boost/include: linker input file unused because linking not done" | tee $(LOG_FILE)
ifdef EXE # {
	@if [[ ! -e $(EXE) ]];                               \
   then                                                \
     echo ' $(ERROR_LABEL) Could not build [$(EXE)].'; \
   fi | tee -a $(LOG_FILE)
endif # }
	@sed -i 's/\x1b\[[^@-~]*[@-~]//g' $(LOG_FILE)
ifndef NO_EDITOR # {
ifneq ($(filter vi vim,$(EDITOR)),) # {
	@egrep -q "($(ERROR_STRINGS))" $(LOG_FILE);                                                                             \
  if [[ 0 == $${?} ]];                                                                                                    \
  then                                                                                                                    \
    sed -i 's/std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >/std::string/g' $(LOG_FILE); \
    $(EDITOR) "+call histdel('/')" -c "set ic" +/"$(subst |,\|,$(ERROR_STRINGS))" $(LOG_FILE);                            \
  else                                                                                                                    \
    $(EDITOR) $(LOG_FILE);                                                                                                \
  fi
endif # }
endif # }

vars_common_mk_help::
	@echo '$(MAKE) log → performs a build, saving all output to the log file [$(LOG_FILE)]'
endif # }

.PHONY: vars_common_mk_help
vars_common_mk_help::
	@echo '$(MAKE) help → shows this help information'
	@echo '$(MAKE) updates → displays update availability for some applications'
	@echo '$(MAKE) vars → displays values of makefile variables (except automatic, default, and environment variables)'

.PHONY: help
help:
	+@$(MAKE_NO_DIR) show_help | sort | column -s"→" -o"→" -t

.PHONY: show_macros
CompilerMacros show_macros: COMPILER_MACROS_OPTS=-dM -E -x c++ - < /dev/null 2> /dev/null
show_macros:
	@$(CXX) $(COMPILER_MACROS_OPTS)
.PHONY: compiler_macros
CompilerMacros compiler_macros: ORACLE_CC_MACROS_OPTS=-E -xdumpmacros=defs - <(echo "main(){}") 2>&1 | grep "^\#define"
compiler_macros: no_op_target
ifneq ($(shell command -v $(CXX)),) # {
ifneq ($(CXX),$(ORACLE_CC)) # {
	+@$(MAKE_NO_DIR) show_macros | sort | column -s" " -o" " -t                       | \
    sed -e 's/short * unsigned int/short unsigned int/'                               \
        -e 's/unsigned * short/unsigned short/'                                       \
        -e 's/unsigned * int/unsigned int/'                                           \
        -e 's/short * int/short int/'                                                 \
        -e 's/c * ## * L/c ## L/'                                                     \
        -e 's/long * int/long int/'                                                   \
        -e 's/signed * char/signed char/'                                             \
        -e 's/long * unsigned int/long unsigned int/'                                 \
        -e 's/c * ## * U/c ## U/'                                                     \
        -e 's/c * ## * UL/c ## UL/'                                                   \
        -e 's/unsigned * char/unsigned char/'                                         \
        -e 's/(-__SIG_ATOMIC_MAX__ * - * 1)/(-__SIG_ATOMIC_MAX__ - 1)/'               \
        -e 's/(-__WCHAR_MAX__ * - * 1)/(-__WCHAR_MAX__ - 1)/'                         \
        -e 's/6.3.1 * 20161221 * (Red Hat 6.3.1-1)/6.3.1 20161221 (Red Hat 6.3.1-1)/'
else # } {
	@$(ORACLE_CC) $(ORACLE_CC_MACROS_OPTS)                                                                             | \
   sort | column -s" " -o" " -t                                                                                      | \
   sed -e 's/\(^#define\)\( \+\)\(__DATE__\)\( \+\)\(".*[^ ]\)\( \+\)\([0-9]\{1,2\}\)\( \+\)\(.*$$\)/\1 \3 \5 \7 \9/'  \
       -e 's/unsigned * long/unsigned long/'
endif # }
else # } {
	@$(warning $(WARNING_LABEL) The C++ compiler [$(CXX)] was not found.)
endif # }

################################################################################
# Shows make's implicit rules and variables.                                   #
################################################################################
.PHONY: implicit
implicit:
	+@$(MAKE) -p -f /dev/null 2> /dev/null || :

################################################################################
# Target to print the contents and origin of a single makefile variable:       #
#   make looks for a pattern rule and finds print-%                            #
#   $(*) = the value matched by "%" in print-%                                 #
#   Example:                                                                   #
#     make print-CXX                                                           #
#                                                                              #
# Possible values returned by the function $(origin ...):                      #
#   undefined            - variable was never defined                          #
#   default              - variable has a default definition                   #
#   environment          - variable was inherited from the environment         #
#   environment override - variable was inherited from the environment,        #
#                          and is overriding a setting for variable in the     #
#                          makefile as a result of the -e option               #
#   file                 - variable was defined in a makefile                  #
#   command line         - variable was defined on the command line            #
#   override             - variable was defined with an override directive     #
#   automatic            - variable is an automatic variable defined for the   #
#                          execution of the recipe                             #
#                                                                              #
# Possible values returned by the function $(flavor ...):                      #
#   simple    - simply expanded variable                                       #
#   recursive - recursively expanded variable                                  #
#   undefined - never defined                                                  #
################################################################################
print-%:
	@echo '$(*)           = [$($(*))]'
	@echo 'ORIGIN of $(*) = [$(origin $(*))]'
	@echo 'FLAVOR of $(*) = [$(flavor $(*))]'
	@echo ''

################################################################################
# Pattern rule target to print the value of a single makefile variable.        #
################################################################################
qprint-%:
	@echo '$($(*))'

################################################################################
# Targets to print out all of the makefile variables.                          #
################################################################################
ifneq ($(filter vars,$(MAKECMDGOALS)),) # {
.PHONY: vars vars_header
vars_header:
	@echo '========== BEGIN =========='
vars: vars_header
	@$(foreach V, $(sort $(.VARIABLES)),                              \
    $(if                                                            \
      $(filter-out environment% default automatic, $(origin $(V))), \
        $(info $(V)=[$($(V))] EXPANDED FROM: [$(value $(V))])       \
      )                                                             \
  )
	@echo -e '==========  END  ==========\n'
endif # }

################################################################################
# Function to determine if a make feature is available or not in this version  #
# of make.                                                                     #
# $(1) - the feature of interest, e.g., guile                                  #
# Example usage:                                                               #
#   f=$(call is_make_feature,guile)                                            #
################################################################################
is_make_feature=$(if $(filter $(1),$(.FEATURES)),T,F)

.PHONY: features
features:
	@echo 'make version $(MAKE_VERSION) .FEATURES:'
	@echo $(.FEATURES) | tr ' ' '\n' | sort

################################################################################
# This target can be used as a prerequisite for any recipe for which make      #
# may print:                                                                   #
#   make: Nothing to be done for '...'.                                        #
# The only purpose of this target is to simply eliminate such messages from    #
# make. An example use case is a double-colon rule that ends doing nothing.    #
################################################################################
.PHONY: no_op_target
no_op_target:
	@:

################################################################################
# Computes the elapsed time (to seconds resolution) and if the $(ESPEAK)       #
# command is available, speaks and displays the elpased time. If $(ESPEAK) is  #
# not available, then the elapsed time is only displayed.                      #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - target start time in seconds                                          #
# $(3) - message prefix (optional)                                             #
#                                                                              #
#  Examples:                                                                   #
#                                                                              #
#  For a rule that has prerequisites:                                          #
#                                                                              #
#  NOTE: The elapsed time will include the elapsed times for the prerequisites #
#        qq1 and qq2. Furthermore, the elapsed time will also include the time #
#        that elapsed between setting the variable TARGET_START_TIME and the   #
#        time when the qq target is being built.                               #
#                                                                              #
#  qq: qq1 qq2                                                                 #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$(TARGET_START_TIME))                            #
#                                                                              #
#  NOTE: The elapsed time will **NOT** include the elapsed times for the       #
#        prerequisites qq1 and qq2. Furthermore, the elapsed time will also    #
#        **NOT** include the time that elapsed between setting the variable    #
#        TARGET_START_TIME and the time when the qq target is being built.     #
#                                                                              #
#  qq: qq1 qq2                                                                 #
#  	@$(eval $(@)_START_TIME:=$(shell date +%s))                                #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$($(@)_START_TIME))                              #
#                                                                              #
#  For a rule without prerequisites:                                           #
#                                                                              #
#  NOTE: The elapsed time will include the elapsed time for executing the qq   #
#        target recipe. Furthermore, the elapsed time will also include the    #
#        time that elapsed between setting the variable TARGET_START_TIME and  #
#        the time when the qq target is being built.                           #
#                                                                              #
#  qq:                                                                         #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$(TARGET_START_TIME))                            #
#                                                                              #
#  NOTE: The elapsed time will only include the elapsed time for executing the #
#        qq target recipe.                                                     #
#                                                                              #
#  qq:                                                                         #
#  	@$(eval $(@)_START_TIME:=$(shell date +%s))                                #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$($(@)_START_TIME))                              #
################################################################################
TARGET_START_TIME:=$(shell date +%s)
SECONDS_PER_HOUR=3600
SECONDS_PER_MIN=60
ESPEAK_COMMAND:=espeak-ng
ESPEAK:=$(shell command -v $(ESPEAK_COMMAND))
elapsed_time=$(if $(ESPEAK),$(call speak_elapsed_time,$(1),$(2)),$(call display_elapsed_time,$(1),$(2),$(3)))

################################################################################
# For the sole target $(1), computes the elapsed time (to seconds resolution)  #
# and if the $(ESPEAK) command is available, speaks and displays the elpased   #
# time. If $(ESPEAK) is  not available, then the elapsed time is only          #
# displayed.                                                                   #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - target start time in seconds                                          #
# $(3) - message prefix (optional)                                             #
################################################################################
sole_target_elapsed_time=$(if $(MAKECMDGOALS),$(if $(filter-out $(1),$(MAKECMDGOALS)),,$(call elapsed_time,$(1),$(2),$(3))))

################################################################################
# Computes the elapsed time (to seconds resolution) and then displays it to    #
# stdout and pipes it to $(ESPEAK).                                            #
#   $(1) - target name                                                         #
#   $(2) - target start time in seconds                                        #
################################################################################
define speak_elapsed_time
	@echo -e '$(INFO_LABEL) \c';                                                                                                         \
   SECONDS=$$(($$(date +%s) - $(2)));                                                                                                  \
   printf "Finished target $(1) in %'d hours %d minutes %d seconds.\n"                                                                 \
   $$(bc -q <<< $${SECONDS}/$(SECONDS_PER_HOUR)\;$${SECONDS}%$(SECONDS_PER_HOUR)/$(SECONDS_PER_MIN)\;$${SECONDS}%$(SECONDS_PER_MIN)) | \
   sed 's/\( 0 hours\| 0 minutes\| 0 seconds\)//g;s/ 1 \(hour\|minute\|second\)s/ 1 \1/g;s/in\.$$/in 0 seconds./'                    | \
   tee >($(ESPEAK) -g4)
endef

################################################################################
# Computes the elapsed time (to seconds resolution) and then displays it to    #
# stdout.                                                                      #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - target start time in seconds                                          #
# $(3) - message prefix (optional)                                             #
################################################################################
define display_elapsed_time
	@SECONDS=$$(($$(date +%s) - $(2)));                                                                                                  \
   printf "$(if $(3),$(3) )$(INFO_LABEL) Finished target $(1) in %'d hours %d minutes %d seconds.\n"                                   \
   $$(bc -q <<< $${SECONDS}/$(SECONDS_PER_HOUR)\;$${SECONDS}%$(SECONDS_PER_HOUR)/$(SECONDS_PER_MIN)\;$${SECONDS}%$(SECONDS_PER_MIN)) | \
   sed 's/\( 0 hours\| 0 minutes\| 0 seconds\)//g;s/ 1 \(hour\|minute\|second\)s/ 1 \1/g;s/in\.$$/in 0 seconds./'
endef

################################################################################
# Computes the elapsed time (to nanoseconds resolution) and if the $(ESPEAK)   #
# command is available, speaks and displays the elpased time. If $(ESPEAK) is  #
# not available, then the elapsed time is only displayed.                      #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - target start time in nanoseconds                                      #
#                                                                              #
#  Examples:                                                                   #
#                                                                              #
#  For a rule that has prerequisites:                                          #
#                                                                              #
#  NOTE: The elapsed time will include the elapsed times for the prerequisites #
#        qq1 and qq2. Furthermore, the elapsed time will also include the time #
#        that elapsed between setting the variable TARGET_START_TIME_NSEC and  #
#        the time when the qq target is being built.                           #
#                                                                              #
#  qq: qq1 qq2                                                                 #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time_nsec,$(@),$(TARGET_START_TIME_NSEC))                  #
#                                                                              #
#  NOTE: The elapsed time will **NOT** include the elapsed times for the       #
#        prerequisites qq1 and qq2. Furthermore, the elapsed time will also    #
#        **NOT** include the time that elapsed between setting the variable    #
#        TARGET_START_TIME_NSEC and the time when the qq target is being built.#
#                                                                              #
#  qq: qq1 qq2                                                                 #
#  	@$(eval $(@)_START_TIME_NSEC:=$(shell date +%s%N))                         #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time_nsec,$(@),$($(@)_START_TIME_NSEC))                    #
#                                                                              #
#  For a rule without prerequisites:                                           #
#                                                                              #
#  NOTE: The elapsed time will include the elapsed time for executing the qq   #
#        target recipe. Furthermore, the elapsed time will also include the    #
#        time that elapsed between setting the variable TARGET_START_TIME_NSEC #
#        and the time when the qq target is being built.                       #
#                                                                              #
#  qq:                                                                         #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$(TARGET_START_TIME_NSEC))                       #
#                                                                              #
#  NOTE: The elapsed time will only include the elapsed time for executing the #
#        qq target recipe.                                                     #
#                                                                              #
#  qq:                                                                         #
#  	@$(eval $(@)_START_TIME:=$(shell date +%s))                                #
#  	[recipe body]                                                              #
#  	@$(call elapsed_time,$(@),$($(@)_START_TIME))                              #
################################################################################
TARGET_START_TIME_NSEC:=$(shell date +%s%N)
NANO_SECONDS_PER_SECOND=1000000000
define elapsed_time_nsec
	@$(if $(ESPEAK),$(call speak_elapsed_time_nsec,$(1),$(2)), $(call display_elapsed_time_nsec,$(1),$(2)))
endef

################################################################################
# For the sole target $(1), computes the elapsed time (to nanoseconds          #
# resolution) and if the $(ESPEAK) command is available, speaks and displays   #
# the elpased time. If $(ESPEAK) is  not available, then the elapsed time is   #
# only displayed.                                                              #
#                                                                              #
# $(1) - target name                                                           #
# $(2) - target start time in seconds                                          #
# $(3) - message prefix (optional)                                             #
################################################################################
sole_target_elapsed_time_nsec=$(if $(MAKECMDGOALS),$(if $(filter-out $(1),$(MAKECMDGOALS)),,$(call elapsed_time_nsec,$(1),$(2),$(3))))

################################################################################
# Computes the elapsed time (to nanoseconds resolution) and then displays it   #
# to stdout and pipes it to $(ESPEAK).                                         #
#   $(1) - target name                                                         #
#   $(2) - target start time in nanoseconds                                    #
################################################################################
define speak_elapsed_time_nsec
	@NANO_SECONDS=$$(($$(date +%s%N) - $(2)));                                                                                                             \
   SECONDS=$$((NANO_SECONDS / $(NANO_SECONDS_PER_SECOND)));                                                                                              \
   NANO_SECONDS=$$((NANO_SECONDS % $(NANO_SECONDS_PER_SECOND)));                                                                                         \
   printf "Finished target $(1) in %'d hours %d minutes %d seconds %'d nanoseconds.\n"                                                                   \
   $$(bc -q <<< $${SECONDS}/$(SECONDS_PER_HOUR)\;$${SECONDS}%$(SECONDS_PER_HOUR)/$(SECONDS_PER_MIN)\;$${SECONDS}%$(SECONDS_PER_MIN)\;$${NANO_SECONDS}) | \
   sed 's/\( 0 hours\| 0 minutes\| 0 seconds\| 0 nanoseconds\)//g;s/ 1 \(hour\|minute\|second\|nanosecond\)s/ 1 \1/g;s/in\.$$/in 0 nanoseconds./'      | \
  tee >($(ESPEAK) -g4)
endef

################################################################################
# Computes the elapsed time (to seconds nanoresolution) and then displays it   #
# to stdout.                                                                   #
#   $(1) - target name                                                         #
#   $(2) - target start time in nanoseconds                                    #
################################################################################
define display_elapsed_time_nsec
	@NANO_SECONDS=$$(($$(date +%s%N) - $(2)));                                                                                                             \
   SECONDS=$$((NANO_SECONDS / $(NANO_SECONDS_PER_SECOND)));                                                                                              \
   NANO_SECONDS=$$((NANO_SECONDS % $(NANO_SECONDS_PER_SECOND)));                                                                                         \
   printf "$(INFO_LABEL) Finished target $(1) in %'d hours %d minutes %d seconds %'d nanoseconds.\n"                                                     \
   $$(bc -q <<< $${SECONDS}/$(SECONDS_PER_HOUR)\;$${SECONDS}%$(SECONDS_PER_HOUR)/$(SECONDS_PER_MIN)\;$${SECONDS}%$(SECONDS_PER_MIN)\;$${NANO_SECONDS}) | \
   sed 's/\( 0 hours\| 0 minutes\| 0 seconds\| 0 nanoseconds\)//g;s/ 1 \(hour\|minute\|second\|nanosecond\)s/ 1 \1/g;s/in\.$$/in 0 nanoseconds./'
endef

################################################################################
# Outputs a command to open the a file with the specified command. If the      #
# command is not available, then the file description and the file name are    #
# printed as a warning message.                                                #
#                                                                              #
# $(1) - the file(s) to be opened                                              #
# $(2) - the command to use to open the file(s)                                #
# $(3) - file description(s)                                                   #
# $(4) - second command to use to open the file(s) (optional)                  #
################################################################################
define open_file_command
	@$(if $(1),$(if $(2),                                                              \
     for FILE in $(1);                                                               \
     do                                                                              \
       if [[ -e "$${FILE}" ]];                                                       \
       then                                                                          \
         if [[ -r "$${FILE}" ]];                                                     \
         then                                                                        \
           if [[ -z "$${FILES}" ]];                                                  \
           then                                                                      \
             FILES="$${FILE}";                                                       \
           else                                                                      \
             FILES="$${FILES} $${FILE}";                                             \
           fi;                                                                       \
         else                                                                        \
           echo "[$(0)] $(WARNING_LABEL) The $(3) [$${FILE}] file is not readable."; \
         fi;                                                                         \
       else                                                                          \
         echo "[$(0)] $(WARNING_LABEL) The $(3) [$${FILE}] file does not exist.";    \
       fi;                                                                           \
     done;                                                                           \
     if [[ ! -z "$${FILES}" ]];                                                      \
     then                                                                            \
       echo "$(2) $${FILES}";                                                        \
       if [[ -n "$(4)" ]];                                                           \
       then                                                                          \
         echo "$(4) $${FILES}";                                                      \
       fi;                                                                           \
     fi,                                                                             \
     $(warning [$(0)] $(WARNING_LABEL) The command name is empty.)),
     $(warning [$(0)] $(WARNING_LABEL) The file list $(if $(3),for [$(3)]) is empty.))
endef

################################################################################
# If the target is the sole make target, then a command is output to open the  #
# the file created by the target with the specified command. If the command is #
# not available, then the file description and the file name are printed.      #
#                                                                              #
# $(1) - target                                                                #
# $(2) - the file to be opened                                                 #
# $(3) - the command to use to open the file                                   #
# $(4) - file description                                                      #
# $(5) - second command to use to open the file (optional)                     #
################################################################################
open_sole_target_file=$(if $(filter-out $(1),$(MAKECMDGOALS)),,$(call open_file_command,$(2),$(3),$(4),$(5)))

################################################################################
# Removes duplicate items in the in the list without sorting.                  #
# See:                                                                         #
#   https://stackoverflow.com/questions/16144115/makefile-remove-duplicate-words-without-sorting
#                                                                              #
# $(1) - list in which duplicate items are to be removed                       #
################################################################################
remove_dupes=$(strip $(if $(1),$(firstword $(1)) $(call remove_dupes,$(filter-out $(firstword $(1)),$(1)))))

################################################################################
# Returns a sequence of integers starting from 0, ending at ($(1) - 1), and    #
# incremented by 1.                                                            #
#                                                                              #
# $(1) - ending index + 1                                                      #
# See:                                                                         #
#   https://stackoverflow.com/questions/20963590/how-do-i-define-a-variable-which-is-a-sequential-list-of-integers-in-gmake
################################################################################
seq0=$(if $(filter $(1),$(words $(2))),$(2),$(call seq0,$(1),$(2) $(words $(2))))

################################################################################
# Returns a sequence of integers starting from 1, ending at $(1), and          #
# incremented by 1.                                                            #
#                                                                              #
# $(1) - ending index                                                          #
################################################################################
seq1=$(filter-out 0,$(call seq0,$(1))) $(1)

################################################################################
# Determines if the input compiled binary file has debug symbols or not.       #
#                                                                              #
# $(1) - compiled binary file                                                  #
################################################################################
define has_debug_symbols
	@readelf -S $(1) | egrep -q "( \.debug_info|\.gnu_debuglink)"; \
   echo $${?}
endef

################################################################################
# Produces the Cartesian product of the two lists.                             #
#                                                                              #
# $(1) - first list                                                            #
# $(2) - secind list                                                           #
#                                                                              #
# NOTE: If $(1) is non-empty and $(2) is empty, the Cartesian product is $(1). #
#       If $(1) is empty and $(2) is non-empty, the Cartesian product is $(2). #
#       If both $(1) and $(2) are empty, the Cartesian product is empty.       #
################################################################################
cartesian_product=$(if $(1),$(if $(2),$(foreach x,$(1),$(foreach y,$(2),$(x)$(y))),$(1)),$(if $(2),$(2)))

################################################################################
# Compares the two input lists and prints a relevant message based upon the    #
# comparison results. Each list may contain just a single item.                #
#                                                                              #
# $(1) - first list                                                            #
# $(2) - first list description                                                #
# $(3) - second list                                                           #
# $(4) - second list description                                               #
# $(5) - message label                                                         #
################################################################################
define compare_lists
$(if $(filter-out $(1),$(3)),                                                           \
    echo '[$(5)] WARNING: Filter out [$(2)] FROM [$(4)] = [$(filter-out $(1),$(3))]',   \
    $(if $(filter-out $(3),$(1)),                                                       \
      echo '[$(5)] WARNING: Filter out [$(4)] FROM [$(2)] = [$(filter-out $(3),$(1))]', \
      $(if $(filter 1,$(words $(1))),                                                   \
        echo '[$(5)] INFO: [$(2)] [$(1)] == [$(4)] [$(3)]',                             \
        echo '[$(5)] INFO: [$(2)] == [$(4)]')))
endef

################################################################################
# Checks for the existence of the input dynamic library file by first using    #
# "/sbin/ldconfig -p"; if this fails, then the directories of                  #
# ${LD_LIBRARY_PATH} are examined. The end result is echoed.                   #
#   $(1) - the dynamic library file name                                       #
# Usage:                                                                       #
#   $(call check_for_dynamic_library,libabc.so)                                #
################################################################################
define check_for_dynamic_library
$(if $(1),$(shell LIB=$$(echo $(1) | sed 's/\.so.*$$/.so/');                 \
  /sbin/ldconfig -p | sed 's/^\tlib/lib/;s/\.so.*$$/.so/' | grep -q $${LIB}; \
  STAT=$${?};                                                                \
  if [[ 0 != $${STAT} ]];                                                    \
  then                                                                       \
    for DIR in $$(echo ${LD_LIBRARY_PATH} | sed 's/::/:/g;s/:/ /g');         \
    do                                                                       \
      if [[ -e $${DIR}/$${LIB} ]];                                           \
      then                                                                   \
        STAT=0;                                                              \
        break;                                                               \
      fi;                                                                    \
    done;                                                                    \
  fi;                                                                        \
  echo "$${STAT}"))
endef

################################################################################
# Gets the version of an application whose version is written to stdout.       #
#                                                                              #
# $(1) - application                                                           #
# $(2) - application flag to print the version (may be empty)                  #
# $(3) - command used to extract the version                                   #
################################################################################
get_stdout_version=$(eval $(1)_ver=$(shell $(1) $(2) 2> /dev/null $(3)))$(if $($(1)_ver),$($(1)_ver),UNKNOWN)

################################################################################
# Gets the version of an application whose version is written to stdout using  #
# the specified word index.                                                    #
#                                                                              #
# $(1) - application                                                           #
# $(2) - application flag to print the version (may be empty)                  #
# $(3) - word index                                                            #
################################################################################
get_stdout_version_index=$(eval $(1)_ver=$(word $(3),$(shell $(1) $(2) 2> /dev/null)))$(if $($(1)_ver),$($(1)_ver),UNKNOWN)

################################################################################
# Gets the version of the specified jar file whose version is written to       #
# stdout using the specified word index.                                       #
#                                                                              #
# $(1) - full path to jar file                                                 #
# $(2) - version flag                                                          #
# $(3) - word index                                                            #
################################################################################
get_stdout_java_jar_file_version_index=$(eval $(1)_ver=$(word $(3),$(shell java -jar $(1) $(2) | head -1)))$(if $($(1)_ver),$($(1)_ver),UNKNOWN)

################################################################################
# Gets the version of an application whose version is written to stderr.       #
#                                                                              #
# $(1) - application                                                           #
# $(2) - application flag to print the version (may be empty)                  #
# $(3) - command used to extract the version                                   #
################################################################################
get_stderr_version=$(eval $(1)_ver=$(shell $(1) $(2) 2>&1 $(3)))$(if $($(1)_ver),$($(1)_ver),UNKNOWN)

################################################################################
# Gets the version of an application whose version is written to stderr using  #
# the specified word index.                                                    #
#                                                                              #
# $(1) - application                                                           #
# $(2) - application flag to print the version (may be empty)                  #
# $(3) - word index                                                            #
################################################################################
get_stderr_version_index=$(eval $(1)_ver=$(word $(3),$(shell $(1) $(2) 2>&1)))$(if $($(1)_ver),$($(1)_ver),UNKNOWN)

################################################################################
# Gets the version of the installed rpm package.                               #
#                                                                              #
# $(1) - rpm package                                                           #
################################################################################
get_rpm_package_version=$(lastword $(shell { dnf info $(1) || echo 'UNKNOWN'; } | grep -E "^(Version *:|UNKNOWN)"))

################################################################################
# Gets the version of an installed Node.js package.                            #
#                                                                              #
# $(1) - Node.js package                                                       #
################################################################################
get_node_js_package_version=$(lastword $(subst @, ,$(shell npm list $(1) --global 2> /dev/null | sed -n '2{p;q}' | sed 's/(empty)/UNKNOWN/')))

################################################################################
# Gets the rub gem version.                                                    #
#                                                                              #
# $(1) - ruby gem                                                              #
################################################################################
get_ruby_gem_version=$(strip $(foreach tok,$(subst /, ,$(shell gem which $(1) 2> /dev/null)),$(if $(ver),,$(eval ver=$(if $(findstring $(1)-,$(tok)),$(tok)))))$(if $(ver),$(lastword $(subst -, ,$(ver))),UNKNOWN))

################################################################################
# Gets the go module version.                                                  #
#                                                                              #
# $(1) - full path to go executable                                            #
################################################################################
get_go_module_version=$(subst go,,$(word 2,$(shell { go version -m $(1) 2> /dev/null || echo 'UNONOWN'; })))

ifneq ($(filter updates show_updates,$(MAKECMDGOALS)),) # {

################################################################################
# $(1) - application                                                           #
# $(2) - application download web site url                                     #
# $(3) - currently installed version                                           #
# $(4) - latest available version                                              #
################################################################################
define check_updates
	@if [[ -z "$(1)" ]];                                                                                                                                                                   \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) Must specify application name to check for available update(s).';                                                                                     \
   elif [[ -z "$(2)" ]];                                                                                                                                                                 \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) For application [$(1)], must specify application download web site url.';                                                                             \
   elif [[ -z "$(3)" ]];                                                                                                                                                                 \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) For application [$(1)], must specify the currently installed version.';                                                                               \
   elif [[ -z "$(4)" ]];                                                                                                                                                                 \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) For application [$(1)], must specify the latest available version.';                                                                                  \
   elif [[ "$(3)" == "UNKNOWN" ]];                                                                                                                                                       \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) Could not determine the currently installed version of [$(1)].';                                                                                      \
   elif [[ "$(4)" == "UNKNOWN" ]];                                                                                                                                                       \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) Could not determine the latest version of [$(1)] from web site [$(2)].';                                                                              \
   elif [[ "$(3)" == "$(4)" ]];                                                                                                                                                          \
   then                                                                                                                                                                                  \
     echo '[$(0)] $(INFO_LABEL) Currently installed version of [$(1)] is [$(call add_green,$(3))]; it is the latest version.';                                                           \
   elif [[ "$(3)" != "$(4)" ]];                                                                                                                                                          \
   then                                                                                                                                                                                  \
     read MIN_VER MAX_VER <<< $$(echo '$(3) $(4)' | tr ' ' '\n' | sort -V | tr '\n' ' ');                                                                                                \
     if [[ $${MAX_VER} == "$(4)" ]];                                                                                                                                                     \
     then                                                                                                                                                                                \
       echo '[$(0)] $(INFO_LABEL) Currently installed version of [$(1)] is [$(call add_yellow,$(3))]; a newer version, [$(call add_green,$(4))], is available from [$(2)].';             \
     else                                                                                                                                                                                \
       echo '[$(0)] $(INFO_LABEL) Currently installed version of [$(1)], [$(call add_yellow,$(3))], is newer than the latest available version, [$(call add_green,$(4))], from [$(2)].'; \
     fi;                                                                                                                                                                                 \
   else                                                                                                                                                                                  \
     echo '[$(0)] $(WARNING_LABEL) For $(1), could not determine update availability status from [$(2)].';                                                                               \
   fi
endef

CLOC_DOWNLOAD:=https://github.com/AlDanial/cloc
CLOC_LATEST_VERSION=$(shell { curl -s ${CLOC_DOWNLOAD} 2> /dev/null; echo UNKNOWN; } | egrep '(UNKNOWN|Latest release:)' | sed 's/\(<p.*v\)\([0-9]\+\.[0-9]\+\)\(.*$$\)/\2/' | head -1)

VALGRIND_DOWNLOAD=https://valgrind.org/downloads/current.html
VALGRIND_LATEST_VERSION=$(shell { curl -s $(VALGRIND_DOWNLOAD) 2> /dev/null; echo UNKNOWN; } | egrep '(UNKNOWN|current.*Valgrind)' | head -1 | sed 's/\(<a.*Valgrind \)\([0-9]\+\.[0-9]\+\.[0-9]\+\)\(.*\)/\2/')
# qq redundant
VALGRIND_VERSION=$(call get_stdout_version,valgrind,--version,| head -1 | sed 's/^.*-//')

.PHONY: show_updates
show_updates::
	@$(call check_updates,cloc,$(CLOC_DOWNLOAD),$(CLOC_VERSION),$(CLOC_LATEST_VERSION))
	@$(call check_updates,valgrind,$(VALGRIND_DOWNLOAD),$(VALGRIND_VERSION),$(VALGRIND_LATEST_VERSION))

################################################################################
# This target can be used to check for available updates to some of the        #
# applications (as specified below).                                           #
################################################################################
.PHONY: updates
updates:
	+@$(MAKE_NO_DIR) show_updates | sort

endif # }

################################################################################
# Variables used by make for its implicit rules.                               #
################################################################################
.PHONY: gather_implicit_vars implicit_vars
gather_implicit_vars:
	@echo '| VARIABLE | DESCRIPTION | VALUE | DEFAULT VALUE |'
ifdef AR # {
	@echo '| AR | $(AR) | archiver| ar |'
	@echo '| ARFLAGS | $(AR) flags | $(ARFLAGS) | rv |'
endif # }
ifdef AS # {
	@echo '| AS | assembler | $(AS) | as |'
ifdef ASFLAGS # {
	@echo '| ASFLAGS | $$(AS) flags when explicitly invoked on *.[Ss] files | $(ASFLAGS) | empty string |'
else # } {
	@echo '| ASFLAGS | $$(AS) flags when explicitly invoked on *.[Ss] files | empty string | empty string |'
endif # }
endif # }
ifdef CC # {
	@echo '| CC | C compiler | $(CC) | cc |'
ifdef CFLAGS # {
	@echo '| CFLAGS | $$(CC) flags | $(CFLAGS) | empty string |'
else # } {
	@echo '| CFLAGS | $$(CC) flags | empty string | empty string |'
endif # }
endif # }
ifdef CO # {
	@echo '| CO | RCS file extractor | $(CO) | co |'
ifdef COFLAGS # {
	@echo '| COFLAGS | $$(CO) flags | $(COFLAGS) | empty string |'
else # } {
	@echo '| COFLAGS | $$(CO) flags | empty string | empty string |'
endif # }
endif # }
ifdef CPP # {
	@echo '| CPP | C preprocessor | $(CPP) | $(CC) -E |'
ifdef CPPFLAGS # {
	@echo '|  CPPFLAGS | $$(CPP) flags and for programs that use $(CPP) | $(CPPFLAGS) | empty string |'
else # } {
	@echo '| CPPFLAGS | $$(CPP) flags and for programs that use $(CPP) | empty string | empty string |'
endif # }
endif # }
ifdef CTANGLE # {
	@echo '| CTANGLE | C Web to C translator | $(CTANGLE) | ctangle |'
endif # }
ifdef CWEAVE # {
	@echo '| CWEAVE | C Web to TEX translator | $(CWEAVE) | cweave |'
endif # }
ifdef CXX # {
	@echo '| CXX | C++ compiler | $(CXX) | g++ |'
ifdef CXXFLAGS # {
	@echo '| CXXFLAGS | $$(CXX) flags | $(CXXFLAGS) | empty string |'
else # } {
	@echo '| CXXFLAGS | $$(CXX) flags | empty string | empty string |'
endif # }
endif # }
ifdef FC # {
	@echo '| FC | Fortran and Rational Fortran preprocessor/compiler | $(FC) | f77 |'
ifdef FFLAGS # {
	@echo '| FFLAGS | $$(FC) flags | $(FFLAGS) | empty string |'
else # } {
	@echo '| FFLAGS | $$(FC) flags | empty string | empty string |'
endif # }
ifdef RFLAGS # {
	@echo '| RFLAGS | $$(FC) flags for Rational Fortran programs | $(RFLAGS) | empty string |'
else # } {
	@echo '| RFLAGS | $$(FC) flags for Rational Fortran programs | empty string | empty string |'
endif # }
endif # }
ifdef GET # {
	@echo '| GET | SCCS file extractor | $(GET) | get |'
ifdef GFLAGS # {
	@echo '| GFLAGS | $$(GET) flags | $(GFLAGS) | empty string |'
else # } {
	@echo '| GFLAGS | $$(GET) flags | empty string | empty string |'
endif # }
endif # }
ifdef LEX # {
	@echo '| LEX | Lex grammars to source code converter | $(LEX) | lex |'
ifdef LFLAGS # {
	@echo '| LFLAGS | $$(LEX) flags | $(LFLAGS) | empty string |'
else # } {
	@echo '| LFLAGS | $$(LEX) flags | empty string | empty string |'
endif # }
endif # }
ifdef LINT # {
	@echo '| LINT | source code linter | $(LINT) | lint |'
ifdef LINTFLAGS # {
	@echo '| LINTFLAGS | $$(LINT) flags | $(LINTFLAGS) | empty string |'
else # } {
	@echo '| LINTFLAGS | $$(LINT) flags | empty string | empty string |'
endif # }
endif # }
ifdef M2C # {
	@echo '| M2C | Modula-2 compiler | $(M2C) | m2c |'
endif # }
ifdef MAKEINFO # {
	@echo '| MAKEINFO | Texinfo source file to Info file converter | $(MAKEINFO) | makeinfo |'
endif # }
ifdef PC # {
	@echo '| PC | Pascal compiler | $(PC) | pc |'
ifdef PFLAGS # {
	@echo '| PFLAGS | $$(PC) flags | $(PFLAGS) | empty string |'
else # } {
	@echo '| PFLAGS | $$(PC) flags | empty string | empty string |'
endif # }
endif # }
ifdef RM # {
	@echo '| RM | removes files | $(RM) | rm -f |'
endif # }
ifdef TANGLE # {
	@echo '| TANGLE | Web to Pascal translator | $(TANGLE) | tangle |'
endif # }
ifdef TEX # {
	@echo '| TEX | TEX source to TEX dvi converter | $(TEX) | tex |'
endif # }
ifdef TEXI2DVI # {
	@echo '| TEXI2DVI | Texinfo source to TEX dvi converter | $(TEXI2DVI) | texi2dvi |'
endif # }
ifdef WEAVE # {
	@echo '| WEAVE | Web to TEX translator | $(WEAVE) | weave |'
endif # }
ifdef YACC # {
	@echo '| YACC | yacc grammars to source code converter | $(YACC) | yacc |'
ifdef YFLAGS # {
	@echo '| YFLAGS | $$(YACC) flags | $(YFLAGS) | empty string |'
else # } {
	@echo '| YFLAGS | $$(YACC) flags | empty string | empty string |'
endif # }
endif # }
ifdef LDFLAGS # {
	@echo '| LDFLAGS | linker (e.g. ld) flags, such as -L | $(LDFLAGS) | empty string |'
else # } {
	@echo '| LDFLAGS | linker (e.g. ld) flags, such as -L | empty string | empty string |'
endif # }
ifdef LDLIBS # {
	@echo '| LDLIBS | library flags, e.g., -lfoo | $(LDLIBS) | empty string |'
else # } {
	@echo '| LDLIBS | library flags, e.g., -lfoo | empty string | empty string |'
endif # }

IMPLICIT_VARS_FILE=implicit_vars.log
implicit_vars:
	+@$(MAKE_NO_DIR) gather_implicit_vars | column -s"|" -o"|" -t | gawk 'BEGIN {                                     \
                                                                                        l_ind = 0;                  \
                                                                                      }                             \
                                                                                      {                             \
                                                                                        l_arr[l_ind] = $$0;         \
                                                                                        l_ind++;                    \
                                                                                      }                             \
                                                                                      END {                         \
                                                                                        l_len = length(l_arr[0]);   \
                                                                                        l_sep = "";                 \
                                                                                        for (i = 0; i < l_len; i++) \
                                                                                        {                           \
                                                                                          l_sep = l_sep "+";        \
                                                                                        }                           \
                                                                                        printf("%s\n", l_sep);      \
                                                                                        for (i = 0; i < l_ind; i++) \
                                                                                        {                           \
                                                                                          printf("%s\n", l_arr[i]); \
                                                                                          printf("%s\n", l_sep);    \
                                                                                        }                           \
                                                                                      }' >| $(IMPLICIT_VARS_FILE)
	@echo '$(EDITOR) $(IMPLICIT_VARS_FILE)'

################################################################################
# Setting the OpenCL library stem name.                                        #
################################################################################
OPENCL_LIB_STEM ?= OpenCL

.PHONY: implicit_vars_clean
implicit_vars_clean:
	@rm -f $(IMPLICIT_VARS_FILE)

vars_common_mk_help::
	@echo '$(MAKE) implicit_vars → lists the variables used by make for its implicit rules in [$(IMPLICIT_VARS_FILE)]'

show_help:: vars_common_mk_help

clean:: implicit_vars_clean

include $(PROJECT_ROOT)/plant_uml.mk

endif # }

