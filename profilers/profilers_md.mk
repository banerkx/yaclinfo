
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# NOTE: This makefile must be included **after** all doxygen and profilers     #
#       related makefiles have been included.                                  #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PROFILERS_MD_MK_INCLUDE_GUARD # {
PROFILERS_MD_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including markdowns.mk.                    #
################################################################################
ifeq ($(MARKDOWNS_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/markdowns.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including profilers.mk.                    #
################################################################################
ifeq ($(PROFILERS_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/profilers/profilers.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# Prints the value of the profiler variable and uses the wildcard function to  #
# determine if the profiler file or directory exists.                          #
#                                                                              #
# $(1) - profiler output variable name (using computed variable name technique)#
################################################################################
define profiler_output_info
echo '$(1) = [$($(1))] wildcard = [$(wildcard $($(1)))]';
endef

PROFILER_OUTPUT_FILES=CACHEGRIND_LOG        \
                      CALLGRIND_DOT_OUTPUT  \
                      CALLGRIND_LOG         \
                      DRD_LOG               \
                      GCOV_OUTPUT           \
                      GENHTML_INDEX         \
                      GMON_RESULTS          \
                      GPROF_DOT_OUTPUT      \
                      HELGRIND_LOG          \
                      LD_DEBUG_DIR          \
                      LD_DEBUG_OUTPUT_TXT   \
                      MASSIF_LOG            \
                      MEMCHECK_LOG          \
                      PPROF_HEAP_LEAKS_FILE \
                      SPROF_OUTPUT

.PHONY: profilers_output profilers_output_info
profilers_output_info:
	@$(foreach p,$(PROFILER_OUTPUT_FILES),$(call profiler_output_info,$(p)))

profilers_output::
	+@$(MAKE_NO_DIR) profilers_output_info | sed 's/ \+/ /g' | column -s"=" -o"=" -t

################################################################################
# Appends the value of the profiler variable to PROFILER_FILES.                #
#                                                                              #
# $(1) - profiler output variable name (using computed variable name technique)#
################################################################################
define add_profiler_file
PROFILER_FILES += $(wildcard $(strip $($(1))))
endef
$(foreach p,$(PROFILER_OUTPUT_FILES),$(eval $(call add_profiler_file,$(p))))
PROFILER_FILES:=$(sort $(PROFILER_FILES))

ifneq ($(PROFILER_FILES),) # {

DOXYGEN_EXTRA_FILES += $(filter-out $(GCOV_OUTPUT) $(GENHTML_INDEX) $(LD_DEBUG_DIR),$(PROFILER_FILES))

profilers_output::
	@echo 'DOXYGEN_EXTRA_FILES = [$(DOXYGEN_EXTRA_FILES)]' | sed 's/ = \[//;s/\]$$//;s/ /\n/g' | sed '/^$$/d' | sed '1 s/\(^.*$$\)/\n\1:/'

################################################################################
# Adds the profiler results file to the profiler markdown file used by         #
# doxygen.                                                                     #
#                                                                              #
# $(1) - profiler results file                                                 #
# $(2) - the profiler tool                                                     #
# $(3) - command used for the application execution                            #
# $(4) - the output markdown file                                              #
################################################################################
define add_profiler_info_row
$(if $(wildcard $(1)),                                                         \
  $(if $(filter $(GENHTML_INDEX),$(1)),                                        \
    $(eval FILE=$(subst $(GCOV_OUTPUT),$(DOXYGEN_HTML_DIR),$(GENHTML_INDEX))), \
    $(eval FILE=$(call replace_dir,$(1),$(DOXYGEN_HTML_DIR))))
  $(eval PROTO=file://)$(eval PROFILE_ROW_COUNTER += x))
echo '| $(words $(PROFILE_ROW_COUNTER)) | $(2) | $(3) | $(call create_url_link,$(PROTO)$(FILE)) |' >> $(4);
endef

ifneq ($(wildcard $(LD_DEBUG_DIR)),) # {
LD_DEBUG_OUTPUT_FILES_COUNT=$(shell ls -1 $(LD_DEBUG_DIR)/* | wc -l)
else # } {
LD_DEBUG_OUTPUT_FILES_COUNT=0
endif # }

ifneq ($(wildcard $(SPROF_OUTPUT)),) # {
SPROF_DOT_OUTPUTS=$(wildcard $(SPROF_OUTPUT)/lib*_$(EXE).$(DOT_IMAGE_EXTENSION))
SPROF_OUTPUT_FILES=$(wildcard $(SPROF_OUTPUT)/lib*_$(EXE).txt)
DOXYGEN_EXTRA_FILES += $(SPROF_OUTPUT_FILES) $(SPROF_DOT_OUTPUTS)
endif # }

.PHONY: Profilers
MARKDOWN_TARGETS += Profilers
markdown_files:: Profilers

Profilers: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Profiler Results,Profiler Results Links,code_profiler.png,>|)
	@echo '| Number | Profiler Tool | Executable Command | Results Link |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|:-------|:--------------|:-------------------|:-------------|' >> $(MARKDOWN_DIR)/$(@).md
	@$(if $(wildcard $(CACHEGRIND_LOG)),$(call add_profiler_info_row,$(CACHEGRIND_LOG),$(CACHEGRIND_TOOL),$(CACHEGRIND_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(CALLGRIND_DOT_OUTPUT)),$(call add_profiler_info_row,$(CALLGRIND_DOT_OUTPUT),$(CALLGRIND_TOOL) dot graph,$(CALLGRIND_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md), \
     $(if $(wildcard $(CALLGRIND_LOG)),$(call add_profiler_info_row,$(CALLGRIND_LOG),$(CALLGRIND_TOOL),$(CALLGRIND_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md)))
	@$(if $(wildcard $(DRD_LOG)),$(call add_profiler_info_row,$(DRD_LOG),$(DRD_TOOL),$(DRD_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(GENHTML_INDEX)),$(call add_profiler_info_row,$(GENHTML_INDEX),gcov,$(GCOV_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(GPROF_DOT_OUTPUT)),$(call add_profiler_info_row,$(GPROF_DOT_OUTPUT),gprof dot graph,$(GPROF_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(HELGRIND_LOG)),$(call add_profiler_info_row,$(HELGRIND_LOG),$(HELGRIND_TOOL),$(HELGRIND_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
ifneq ($(filter 1,$(LD_DEBUG_OUTPUT_FILES_COUNT)),) # {
	@$(if $(wildcard $(LD_DEBUG_OUTPUT_TXT)),$(call add_profiler_info_row,$(LD_DEBUG_OUTPUT_TXT),LD_DEBUG = [$(LD_DEBUG)],$(LD_DEBUG_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
else # } {
	@$(if $(wildcard $(LD_DEBUG_DIR)),$(call add_profiler_info_row,$(LD_DEBUG_DIR),LD_DEBUG = [$(LD_DEBUG)] (with child processes),$(LD_DEBUG_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
endif # }
	@$(if $(wildcard $(MASSIF_LOG)),$(call add_profiler_info_row,$(MASSIF_LOG),$(MASSIF_TOOL),$(MASSIF_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(MEMCHECK_LOG)),$(call add_profiler_info_row,$(MEMCHECK_LOG),$(MEMCHECK_TOOL),$(MEMCHECK_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(if $(wildcard $(PPROF_HEAP_LEAKS_FILE)),$(call add_profiler_info_row,$(PPROF_HEAP_LEAKS_FILE),gperftools dot graph,$(GPERF_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(foreach sprof_dot,$(wildcard $(SPROF_DOT_OUTPUTS)),$(call add_profiler_info_row,$(sprof_dot),sprof $(call get_lib_name,$(EXE),$(sprof_dot)) dot graph,$(SPROF_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
	@$(foreach sprof_file,$(wildcard $(SPROF_OUTPUT_FILES)),$(call add_profiler_info_row,$(sprof_file),sprof $(call get_lib_name,$(EXE),$(sprof_file)),$(SPROF_EXE_COMMAND),$(MARKDOWN_DIR)/$(@).md))
################################################################################
# We must change "--tool=" to "\\-\\-tool=" so that the rendered HTML properly #
# shows the double dashes.                                                     #
################################################################################
	@sed -i 's/--tool=/\\-\\-tool=/' $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),profiler results)
	@echo ''

post_doxygen::
ifneq ($(wildcard $(LCOV_OUTPUT)),) # {
	@[[ -d $(LCOV_OUTPUT) ]] && cp -R $(LCOV_OUTPUT) $(DOXYGEN_HTML_DIR)
endif # }
ifneq ($(wildcard $(LD_DEBUG_DIR)),) # {
ifneq ($(LD_DEBUG_OUTPUT_FILES_COUNT),0) # {
ifneq ($(LD_DEBUG_OUTPUT_FILES_COUNT),1) # {
	@[[ -d $(LD_DEBUG_DIR) ]] && cp -R $(LD_DEBUG_DIR) $(DOXYGEN_HTML_DIR)
endif # }
endif # }
endif # }

endif # }

endif # }

