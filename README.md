# README bbucket

OpenCL applications.

## Getting Started
To clone the git repository:
```
git clone --branch main https://bitbucket.org/banerkx/yaclinfo.git 
```
### Prerequisites
A functioning OpenCL installation. Additionally, the following software is required:

| Prerequisite                         | Link                                                                                                                            |
| ------------------------------------ | -------------------------------------------                                                                                     |
| Boost version 1.80                   | [https://www.boost.org/users/download/](https://www.boost.org/users/download/)   |
| GNU C++ compiler                     | [https://gcc.gnu.org](https://gcc.gnu.org)                                       |

To get full doxygen functionality, please install the following:

| Application  | Link                                                                                                                                                |
| ------------ | -----------------------------------------------                                                                                                     |
| PlantUML     | [https://plantuml.com](https://plantuml.com)                                                         |
| dvips        | [https://ctan.org/pkg/dvips?lang=en](https://ctan.org/pkg/dvips?lang=en)                             |
| graphviz/dot | [https://graphviz.gitlab.io](https://graphviz.gitlab.io)                                             |
| gs           | [https://www.ghostscript.com/download/gsdnld.html](https://www.ghostscript.com/download/gsdnld.html) |
| latex        | [https://www.latex-project.org/get](https://www.latex-project.org/get)                               |

| Environment Attribute                          | Value                                                                           |
| ---------------------------------------------  | -----------------------------------                                             |
| /bin/bash version                              | GNU bash, version 5.1.8(1)-release (x86_64-redhat-linux-gnu)                    |
| Boost version                                  | 1.80                                                                            |
| Host Platform Operating System                 | Fedora Linux 35 (Workstation Edition)                                           |
| Khronos Unified OpenCL C API Headers commit    | 8f33fb[]()a7c14b[]()926c65[]()51bf86[]()b5b255[]()e3e0f4[]()7f86                                        |
| Khronos Unified OpenCL C++ API Headers commit  | 4a1157[]()466afe[]()72a87e[]()8abc59[]()537ef5[]()77534c[]()cadf                                        |
| PlantUML version                               | 1.2022.12                                                                       |
| doxygen version                                | 1.9.5                                                                           |
| firefox version                                | 106.0.5                                                                         |
| g++ version                                    | 11.3.1                                                                          |
| git version                                    | 2.38.1                                                                          |
| make version                                   | GNU Make 4.3                                                                    |

Application(s):

| Application     | Readme                           | Description                                   |
| --------------- | -------------------------------- | --------------------------------------------- |
| clinfo          | [README.md](/clinfo/README.md)   | gathers and displays/saves OpenCL information |

### Author
 **K. Banerjee**
### License
Copyright © 2018-2022 K. Banerjee
This project is licensed under the GPL, version 3.0. See:  
[https://www.gnu.org/licenses/gpl-3.0.en.html](https://www.gnu.org/licenses/gpl-3.0.en.html)
  
[https://www.gnu.org/licenses/gpl-3.0.txt](https://www.gnu.org/licenses/gpl-3.0.txt)
