
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CLANG_STATIC_ANALYZER_MK_INCLUDE_GUARD # {
CLANG_STATIC_ANALYZER_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

SCAN_BUILD_COMMAND=scan-build
SCAN_BUILD:=$(shell command -v $(SCAN_BUILD_COMMAND))
CLANG_VERSION ?= $(call get_stdout_version_index,clang,--version,3)
SCAN_BUILD_VERSION=$(if $(shell command -v scan-build 2> /dev/null),$(CLANG_VERSION),UNKNOWN)
CLANG_STATIC_ANALYZER_URL=https://clang-analyzer.llvm.org

SCAN_BUILD_PROJECT=$(notdir $(CURDIR))
SCAN_BUILD_PREFIX=$(subst -,_,$(SCAN_BUILD_COMMAND))
SCAN_BUILD_OUTPUT=$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX)
SCAN_BUILD_INDEX_FILE=index.html

ifneq ($(SCAN_BUILD),) # {

SCAN_BUILD_CHECKER_SETS=core_checkers cpp_checkers deadcode_checkers default_checkers nullability_checkers optin_checkers security_checkers unix_checkers valist_checkers

SCAN_VIEW_COMMAND=scan-view
SCAN_VIEW:=$(shell command -v $(SCAN_VIEW_COMMAND))
SCAN_GREP_V=grep -v "^$(SCAN_BUILD_COMMAND): Run '$(SCAN_VIEW_COMMAND) "

EDITOR  ?= vim +1
BROWSER ?= firefox

################################################################################
# Determines the makefile variable that holds the list of checkers that        #
# corresponds to the checkers target name (this variable must already exist)   #
# and then uses this variable to create the correct rule for the checkers      #
# target.                                                                      #
#                                                                              #
# $(1) - checkers target name, e.g., core_checkers                             #
#                                                                              #
# NOTE: Assuming the latest, going from left to right, -enable-checker/        #
#       -disable-checker takes precedence on the scan-build command line.      #
################################################################################
define create_scan_build_target
.PHONY: $(1)
$(1): UPPER_$(1):=$(shell echo $(1) | tr [:lower:] [:upper:])
$(1): SCAN_BUILD_CHECKERS=$$(addprefix -enable-checker ,$$($$(UPPER_$(1))))
$(1): SCAN_BUILD_OUTPUT_DIR=$(CURDIR)/$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX)_$(1)
$(1): SCAN_BUILD_LOG=$$(SCAN_BUILD_OUTPUT_DIR)/$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX)_$(1).log
$(1): CHECKER_SET_HTML_TITLE=--html-title '$(SCAN_BUILD_PROJECT) $(SCAN_BUILD_COMMAND) $(subst _, ,$(1)) Results'
$(1): build_clean
	@$$(eval $$(@)_START_TIME_NSEC:=$$(shell date +%s%N))
	@rm -rf $$(SCAN_BUILD_LOG) $$(SCAN_BUILD_OUTPUT_DIR)
	@$$(call target_info,$$(@),$$(<),$$(?),$$(^),$$(|),$$(*))
	@mkdir $$(SCAN_BUILD_OUTPUT_DIR)
	@echo '$$(subst _, ,$$(@)):' >| $$(SCAN_BUILD_LOG)
ifeq ($(1),default_checkers) # {
	+@$(SCAN_BUILD) $(SCAN_BUILD_OPTS) -o $$(SCAN_BUILD_OUTPUT_DIR) $$(CHECKER_SET_HTML_TITLE) $$(SCAN_BUILD_CHECKERS) $(MAKE) SCAN_BUILD_FLAG=1 2>&1 | $(SCAN_GREP_V) | tee -a $$(SCAN_BUILD_LOG)
else # } {
	+@$(SCAN_BUILD) $(SCAN_BUILD_OPTS) -o $$(SCAN_BUILD_OUTPUT_DIR) $$(CHECKER_SET_HTML_TITLE) $(DISABLED_DEFAULT_CHECKERS) $$(SCAN_BUILD_CHECKERS) $(MAKE) SCAN_BUILD_FLAG=1 2>&1 | $(SCAN_GREP_V) | tee -a $$(SCAN_BUILD_LOG)
endif # }
	@[[ -r $$(SCAN_BUILD_LOG) ]] && echo '$(EDITOR) $$(SCAN_BUILD_LOG)' || :
	@INDEX_FILE=$$$$(find $(SCAN_BUILD_OUTPUT_DIR) -type f -name $(SCAN_BUILD_INDEX_FILE) 2> /dev/null);                \
   [[ ! -z "$$$${INDEX_FILE}" ]] && [[ ! -z "$(BROWSER)" ]] && echo "$$$$(basename $(BROWSER)) $$$${INDEX_FILE}" || :
	@grep 'scan-build: [0-9]\+ bugs found\.$$$$' $$(SCAN_BUILD_LOG) | sed 's/^scan-build:/$(1):/'
	@$$(call elapsed_time_nsec,$$(@),$$($$(@)_START_TIME_NSEC))

clang_static_analyzer_mk_help::
	@echo "$(MAKE) $(1) → use the clang static analyzer $(subst cpp,C++ ,$(subst _, ,$(1)))";

clean::
	@rm -rf $(CURDIR)/$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX)_$(1)

endef

################################################################################
# Lists the checkers for the specified clang static analyzer checkers name.    #
#                                                                              #
# $(1) - clang static analyzer checkers name, e.g., core_checkers              #
################################################################################
define create_list_checkers_target
.PHONY: list_$(firstword $(subst _, ,$(1)))
list_$(firstword $(subst _, ,$(1))): UPPER_$(1):=$(shell echo $(1) | tr [:lower:] [:upper:])
list_$(firstword $(subst _, ,$(1))):
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	@echo '$$(subst _, ,$(1)):'
	@echo '$$($$(UPPER_$(1)))' | tr -s '[:space:]' | tr ' ' '\n' | sort
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
	@echo ''

clang_static_analyzer_mk_help::
	@echo "$(MAKE) list_$(firstword $(subst _, ,$(1))) → lists the $(subst cpp,C++,$(subst _, ,$(1)))";

endef

################################################################################
# Experimental checkers in addition to the Default Checkers. These are         #
# checkers with known issues or limitations that keep them from being on by    #
# default. They are likely to have false positives.                            #
#                                                                              #
# See: https://clang-analyzer.llvm.org/alpha_checks.html                       #
################################################################################
ALPHA_CLONE_CHECKERS          += alpha.clone.CloneChecker              # Reports similar pieces of code.

ALPHA_CORE_CHECKERS           += alpha.core.BoolAssignment             # Warn about assigning non-{0,1} values to Boolean variables
ALPHA_CORE_CHECKERS           += alpha.core.CallAndMessageUnInitRefArg # Check for uninitialized arguments in function calls
ALPHA_CORE_CHECKERS           += alpha.core.CastSize                   # Check when casting a malloc'ed type T, whether the size is a multiple of the size of T
ALPHA_CORE_CHECKERS           += alpha.core.CastToStruct               # Check for cast from non-struct pointer to struct pointer
ALPHA_CORE_CHECKERS           += alpha.core.Conversion                 # Loss of sign or precision in implicit conversions.
ALPHA_CORE_CHECKERS           += alpha.core.DynamicTypeChecker         # Check for cases where the dynamic and the static type of an object are unrelated.
ALPHA_CORE_CHECKERS           += alpha.core.FixedAddr                  # Check for assignment of a fixed address to a pointer
ALPHA_CORE_CHECKERS           += alpha.core.IdenticalExpr              # Warn about unintended use of identical expressions in operators
ALPHA_CORE_CHECKERS           += alpha.core.PointerArithm              # Check for pointer arithmetic on locations other than array elements
ALPHA_CORE_CHECKERS           += alpha.core.PointerSub                 # Check for pointer subtractions on two pointers pointing to different memory chunks
ALPHA_CORE_CHECKERS           += alpha.core.SizeofPtr                  # Warn about unintended use of sizeof() on pointer expressions
ALPHA_CORE_CHECKERS           += alpha.core.StackAddressAsyncEscape    # Check that addresses to stack memory do not escape the
                                                                       # function that involves dispatch_after or dispatch_async.
ALPHA_CORE_CHECKERS           += alpha.core.TestAfterDivZero           # Check for division by variable that is later compared against 0.
                                                                       # Either the comparison is useless or there is division by zero.

ALPHA_CPP_CHECKERS            += alpha.cplusplus.DeleteWithNonVirtualDtor # Reports destructions of polymorphic objects with a non-virtual
                                                                          # destructor in their base class.
ALPHA_CPP_CHECKERS            += alpha.cplusplus.EnumCastOutOfRange       # Check for integer to enumeration casts that could result in undefined values.
ALPHA_CPP_CHECKERS            += alpha.cplusplus.InvalidatedIterator      # Check for use of invalidated iterators.
ALPHA_CPP_CHECKERS            += alpha.cplusplus.IteratorRange            # Check for iterators used outside their valid ranges.
ALPHA_CPP_CHECKERS            += alpha.cplusplus.MismatchedIterator       # Check for use of iterators of different containers where iterators
                                                                          # of the same container are expected.
ALPHA_CPP_CHECKERS            += alpha.cplusplus.Move # Method calls on a moved-from object and copying a moved-from object will be reported.

ALPHA_DEAD_CODE_CHECKERS      += alpha.deadcode.UnreachableCode # Check unreachable code

ALPHA_LLVM_CHECKERS           += alpha.llvm.Conventions # Check code for LLVM codebase conventions.

ALPHA_NONDETERMINISM_CHECKERS += alpha.nondeterminism.PointerIteration # Check for non-determinism caused by iterating unordered containers of pointers.
ALPHA_NONDETERMINISM_CHECKERS += alpha.nondeterminism.PointerSorting   # Check for non-determinism caused by sorting of pointers.

ALPHA_SECURITY_CHECKERS       += alpha.security.ArrayBound             # Warn about buffer overflows (older checker)
ALPHA_SECURITY_CHECKERS       += alpha.security.ArrayBoundV2           # Warn about buffer overflows (newer checker)
ALPHA_SECURITY_CHECKERS       += alpha.security.MallocOverflow         # Check for overflows in the arguments to malloc()
ALPHA_SECURITY_CHECKERS       += alpha.security.MmapWriteExec          # Warn on mmap() calls that are both writable and executable.
ALPHA_SECURITY_CHECKERS       += alpha.security.ReturnPtrRange         # Check for an out-of-bound pointer being returned to callers
ALPHA_SECURITY_CHECKERS       += alpha.security.taint.TaintPropagation # Generate taint information used by other checkers

ALPHA_UNIX_CHECKERS           += alpha.unix.BlockInCriticalSection    # Check for calls to blocking functions inside a critical section.
ALPHA_UNIX_CHECKERS           += alpha.unix.Chroot                    # Check improper use of chroot
ALPHA_UNIX_CHECKERS           += alpha.unix.PthreadLock               # Simple lock -> unlock checker
ALPHA_UNIX_CHECKERS           += alpha.unix.SimpleStream              # Check for misuses of stream APIs
ALPHA_UNIX_CHECKERS           += alpha.unix.Stream                    # Check stream handling functions
ALPHA_UNIX_CHECKERS           += alpha.unix.cstring.BufferOverlap     # Checks for overlap in two buffer arguments
ALPHA_UNIX_CHECKERS           += alpha.unix.cstring.NotNullTerminated # Check for arguments which are not null-terminating strings
ALPHA_UNIX_CHECKERS           += alpha.unix.cstring.OutOfBounds       # Check for out-of-bounds access in string functions

ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_CLONE_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_CORE_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_CPP_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_DEAD_CODE_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_LLVM_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_NONDETERMINISM_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_SECURITY_CHECKERS)
ALPHA_SCAN_BUILD_CHECKERS += $(ALPHA_UNIX_CHECKERS)

################################################################################
# See: https://clang-analyzer.llvm.org/available_checks.html                   #
################################################################################
CORE_CHECKERS += core.CallAndMessage      # Check for logical errors for function calls.
CORE_CHECKERS += core.DivideZero          # Check for division by zero
CORE_CHECKERS += core.NonNullParamChecker # Check for null pointers passed as arguments to a
                                          # function whose arguments are references or marked
                                          # with the 'nonnull' attribute

CORE_CHECKERS += core.NullDereference                     # Check for dereferences of null pointers
CORE_CHECKERS += core.StackAddressEscape                  # Check that addresses to stack memory do not escape the function
CORE_CHECKERS += core.UndefinedBinaryOperatorResult       # Check for undefined results of binary operators
CORE_CHECKERS += core.VLASize                             # Check for declarations of VLA of undefined or zero size
CORE_CHECKERS += core.uninitialized.ArraySubscript        # Check for uninitialized values used as array subscripts
CORE_CHECKERS += core.uninitialized.Assign                # Check for assigning uninitialized values
CORE_CHECKERS += core.uninitialized.Branch                # Check for uninitialized values used as branch conditions
CORE_CHECKERS += core.uninitialized.CapturedBlockVariable # Check for blocks that capture uninitialized values
CORE_CHECKERS += core.uninitialized.UndefReturn           # Check for uninitialized values being returned to the caller

CPP_CHECKERS += cplusplus.InnerPointer
CPP_CHECKERS += cplusplus.Move                # Method calls on a moved-from object and copying a moved-from object will be reported.
CPP_CHECKERS += cplusplus.NewDelete           # Check for double-free and use-after-free problems. Traces memory managed by new/delete.
CPP_CHECKERS += cplusplus.NewDeleteLeaks      # Check for memory leaks. Traces memory managed by new/delete.
CPP_CHECKERS += cplusplus.PlacementNew
CPP_CHECKERS += cplusplus.PureVirtualCall

DEADCODE_CHECKERS += deadcode.DeadStores # Check for values stored to variables that are never read afterwards

FUCHSIA_CHECKERS += fuchsia.HandleChecker # Detect leaks related to Fuchsia handles

NULLABILITY_CHECKERS += nullability.NullPassedToNonnull         # Warns when a null pointer is passed to a pointer which has a _Nonnull type.
NULLABILITY_CHECKERS += nullability.NullReturnedFromNonnull     # Warns when a null pointer is returned from a function that has _Nonnull return type.
NULLABILITY_CHECKERS += nullability.NullableDereferenced        # Warns when a nullable pointer is dereferenced.
NULLABILITY_CHECKERS += nullability.NullablePassedToNonnull     # Warns when a nullable pointer is passed to a pointer which has a _Nonnull type.
NULLABILITY_CHECKERS += nullability.NullableReturnedFromNonnull # Warns when a nullable pointer is returned from a function that has _Nonnull return type.

OPTIN_CHECKERS += optin.cplusplus.VirtualCall         # Check virtual member function calls during construction or destruction.
OPTIN_CHECKERS += optin.cplusplus.UninitializedObject # Reports uninitialized fields after object construction
OPTIN_CHECKERS += optin.mpi.MPI-Checker               # Checks MPI code.
OPTIN_CHECKERS += optin.performance.GCDAntipattern    # Check for performance anti-patterns when using Grand Central Dispatch
OPTIN_CHECKERS += optin.performance.Padding           # Check for excessively padded structs.
OPTIN_CHECKERS += optin.portability.UnixAPI           # Finds implementation-defined behavior in UNIX/Posix functions

SECURITY_CHECKERS += security.FloatLoopCounter                             # Warn on using a floating point value as a loop counter (CERT: FLP30-C, FLP30-CPP)
SECURITY_CHECKERS += security.insecureAPI.UncheckedReturn                  # Warn on uses of functions whose return values must be always checked
SECURITY_CHECKERS += security.insecureAPI.bcmp                             # Warn on uses of the bcmp function.
SECURITY_CHECKERS += security.insecureAPI.bcopy                            # Warn on uses of the bcopy function.
SECURITY_CHECKERS += security.insecureAPI.bzero                            # Warn on uses of the bzero function.
SECURITY_CHECKERS += security.insecureAPI.getpw                            # Warn on uses of the 'getpw' function
SECURITY_CHECKERS += security.insecureAPI.gets                             # Warn on uses of the 'gets' function
SECURITY_CHECKERS += security.insecureAPI.mkstemp                          # Warn when 'mkstemp' is passed fewer than 6 X's in the format string
SECURITY_CHECKERS += security.insecureAPI.mktemp                           # Warn on uses of the 'mktemp' function
SECURITY_CHECKERS += security.insecureAPI.rand                             # Warn on uses of the 'rand', 'random', and related functions
SECURITY_CHECKERS += security.insecureAPI.strcpy                           # Warn on uses of the 'strcpy' and 'strcat' functions
SECURITY_CHECKERS += security.insecureAPI.vfork                            # Warn on uses of the 'vfork' function
SECURITY_CHECKERS += security.insecureAPI.DeprecatedOrUnsafeBufferHandling # Warn on uses of unsecure or deprecated buffer manipulating functions
SECURITY_CHECKERS += security.insecureAPI.decodeValueOfObjCType            # Warn on uses of the '-decodeValueOfObjCType:at:' method

UNIX_CHECKERS += unix.API                    # Check calls to various UNIX/POSIX functions
UNIX_CHECKERS += unix.Malloc                 # Check for memory leaks, double free, and use-after-free
                                             # problems. Traces memory managed by malloc()/free().
UNIX_CHECKERS += unix.MallocSizeof           # Check for dubious malloc arguments involving sizeof
UNIX_CHECKERS += unix.MismatchedDeallocator  # Check for mismatched deallocators.
UNIX_CHECKERS += unix.Vfork                  # Check for proper usage of vfork
UNIX_CHECKERS += unix.cstring.BadSizeArg     # Check the size argument passed into C string functions for common erroneous patterns
UNIX_CHECKERS += unix.cstring.NullArg        # Check for null pointers being passed as arguments to C string functions

VALIST_CHECKERS += valist.CopyToSelf    # Check for va_lists which are copied onto itself.
VALIST_CHECKERS += valist.Uninitialized # Check for usages of uninitialized (or already released) va_lists.
VALIST_CHECKERS += valist.Unterminated  # Check for va_lists which are not released by a va_end call.

WEBKIT_CHECKERS += webkit.NoUncountedMemberChecker       # Check for no uncounted member variables.
WEBKIT_CHECKERS += webkit.RefCntblBaseVirtualDtor        # Check for any ref-countable base class having virtual destructor.
WEBKIT_CHECKERS += webkit.UncountedLambdaCapturesChecker # Check uncounted lambda captures.

DEFAULT_CHECKERS:=$(shell $(SCAN_BUILD) --help-checkers)

SCAN_BUILD_CHECKERS += $(CORE_CHECKERS)
SCAN_BUILD_CHECKERS += $(CPP_CHECKERS)
SCAN_BUILD_CHECKERS += $(DEADCODE_CHECKERS)
SCAN_BUILD_CHECKERS += $(NULLABILITY_CHECKERS)
SCAN_BUILD_CHECKERS += $(OPTIN_CHECKERS)
SCAN_BUILD_CHECKERS += $(SECURITY_CHECKERS)
SCAN_BUILD_CHECKERS += $(UNIX_CHECKERS)
SCAN_BUILD_CHECKERS += $(VALIST_CHECKERS)
SCAN_BUILD_CHECKERS:=$(addprefix -enable-checker ,$(SCAN_BUILD_CHECKERS))
DISABLED_DEFAULT_CHECKERS=$(addprefix -disable-checker ,$(shell $(SCAN_BUILD) --help-checkers))

SCAN_BUILD_LOG=$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX).log

CXX_COMPILER:=$(shell command -v $(CXX))
SCAN_BUILD_LOOP_ITERATIONS=4
SCAN_BUILD_VERBOSITY=-v -v -v
HTML_TITLE=--html-title '$(SCAN_BUILD_PROJECT) $(SCAN_BUILD_COMMAND) Results'

##SCAN_BUILD_OPTS += --view
SCAN_BUILD_OPTS += -analyze-headers
SCAN_BUILD_OPTS += --keep-going
SCAN_BUILD_OPTS += --keep-cc
SCAN_BUILD_OPTS += --use-c++=$(CXX_COMPILER)
SCAN_BUILD_OPTS += $(SCAN_BUILD_VERBOSITY)
SCAN_BUILD_OPTS += -maxloop $(SCAN_BUILD_LOOP_ITERATIONS)
SCAN_BUILD_OPTS += -stats
SCAN_BUILD_OPTS += -internal-stats
SCAN_BUILD_OPTS += --show-description

ifneq ($(SCAN_BUILD_EXCLUDE_DIRS),) # {
$(foreach path,$(SCAN_BUILD_EXCLUDE_DIRS),$(if $(wildcard $(realpath $(path))),,$(error $(ERROR_LABEL) The $(SCAN_BUILD_COMMAND) exclude path [$(path)] does not exist)))
SCAN_BUILD_EXCLUDE_DIRS:=$(addprefix --exclude ,$(realpath $(SCAN_BUILD_EXCLUDE_DIRS)))
SCAN_BUILD_OPTS += $(SCAN_BUILD_EXCLUDE_DIRS)
endif # }

.PHONY: scan scan_build
IMAGE_PATH ?= $(PROJECT_ROOT)/images
scan scan_build: SCAN_BUILD_FAVICON=$(IMAGE_PATH)/clang_blue_dragon.png
scan scan_build: SCAN_BUILD_OUTPUT_DIR=$(CURDIR)/$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX)
scan scan_build: SCAN_BUILD_LOG=$(SCAN_BUILD_OUTPUT_DIR)/$(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX).log
scan scan_build: build_clean scan_build_clean
	@$(eval $(@)_START_TIME_NSEC:=$(shell date +%s%N))
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@mkdir $(SCAN_BUILD_OUTPUT_DIR)
	+@$(SCAN_BUILD) $(SCAN_BUILD_OPTS) -o $(SCAN_BUILD_OUTPUT_DIR) $(HTML_TITLE) $(SCAN_BUILD_CHECKERS) $(MAKE) SCAN_BUILD_FLAG=1 2>&1 | $(SCAN_GREP_V) | tee $(SCAN_BUILD_LOG)
	@$(call open_file_command,$(SCAN_BUILD_LOG),$(EDITOR),scan-build log)
	@INDEX_FILE=$$(find $(SCAN_BUILD_OUTPUT_DIR) -type f -name $(SCAN_BUILD_INDEX_FILE) 2> /dev/null);            \
   [[ ! -z "$${INDEX_FILE}" ]] && [[ ! -z "$(BROWSER)" ]] && echo "$$(basename $(BROWSER)) $${INDEX_FILE}" || :
	@$(call elapsed_time_nsec,$(@),$($(@)_START_TIME_NSEC))

$(foreach checkers_set,$(SCAN_BUILD_CHECKER_SETS),$(eval $(call create_scan_build_target,$(checkers_set))))
$(foreach checkers_set,$(SCAN_BUILD_CHECKER_SETS),$(eval $(call create_list_checkers_target,$(checkers_set))))

.PHONY: scan_build_help
SCAN_BUILD_HELP=scan_build_help.log
scan_build_help:
	@$(SCAN_BUILD) --help >| $(SCAN_BUILD_HELP)
	@$(call open_file_command,$(SCAN_BUILD_HELP),$(EDITOR),scan-build help file)

clang_static_analyzer_mk_help::
	@echo '$(MAKE) scan_build_help → saves the $(notdir $(SCAN_BUILD)) help information to [$(SCAN_BUILD_HELP)]'

clean::
	@rm -f $(SCAN_BUILD_HELP)

.PHONY: all_checkers
all_checkers:
	@$(eval $(@)_START_TIME_NSEC:=$(shell date +%s%N))
	+@$(foreach checkers_set,$(filter-out default_checkers,$(SCAN_BUILD_CHECKER_SETS)),$(MAKE_NO_DIR) $(checkers_set);)
	@find . -mindepth 2 -maxdepth 3 -name "index\.html" | sed 's@^@$(BROWSER) @'
	@$(call elapsed_time_nsec,$(@),$($(@)_START_TIME_NSEC))

.PHONY: list_all_checkers
list_all_checkers:
	+@$(foreach checkers_set,$(SCAN_BUILD_CHECKER_SETS),$(MAKE_NO_DIR) $(addprefix list_,$(firstword $(subst _, ,$(checkers_set))));)

.PHONY: scan_build_clean
scan_build_clean:
	@rm -f $(SCAN_BUILD_PREFIX)_*.log
	@rm -rf $$(ls -d $(SCAN_BUILD_PROJECT)_$(SCAN_BUILD_PREFIX) 2> /dev/null)
	@clear

clean:: scan_build_clean

clang_static_analyzer_mk_help::
################################################################################
# Writing out the help info ∀ scan-build checker set targets.                  #
################################################################################
	@echo '$(MAKE) all_checkers → clang static analyzer for each checkers set, one by one'
	@$(foreach checkers_set,$(SCAN_BUILD_CHECKER_SETS),$(call checkers_target_help,$(checkers_set)))
	@$(foreach checkers_set,$(SCAN_BUILD_CHECKER_SETS),$(call checkers_list_target_help,$(checkers_set)))
	@echo '$(MAKE) scan_build → run the clang static analyzer'

show_help:: clang_static_analyzer_mk_help

clang_static_analyzer_mk_info:
	@echo 'scan-build version = [$(SCAN_BUILD_VERSION)]'

platform_info:: clang_static_analyzer_mk_info

else # } {

################################################################################
# At this point, we know that scan-build is not available. So we tell the user.#
################################################################################
LIST_TARGETS=$(subst _checkers, ,$(addprefix list_,$(SCAN_BUILD_CHECKER_SETS)))
SCAN_BUILD_TARGETS=all_checkers list_all_checkers scan scan_build scan_build_clean $(LIST_TARGETS) $(SCAN_BUILD_CHECKER_SETS)
.PHONY: $(SCAN_BUILD_TARGETS)
$(SCAN_BUILD_TARGETS):
	@$(error $(ERROR_LABEL) The [$(SCAN_BUILD_COMMAND)] executable is not available, ∴ the target [$(@)] can not be made)

endif # }

endif # }

