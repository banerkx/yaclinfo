
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CppUnitTestXMLOutputterHook.h"

#include <cppunit/tools/XmlDocument.h>
#include <cppunit/tools/XmlElement.h>

#include <cstring>
#include <ctime>

CPPUNIT_NS_BEGIN

CppUnitTestXMLOutputterHook::CppUnitTestXMLOutputterHook(
  const std::string& sTestsTitle,
  const std::string& sTotalExecutionTime) :
  m_sTestsTitle(sTestsTitle), m_sTotalExecutionTime(sTotalExecutionTime)
{
}

CppUnitTestXMLOutputterHook::~CppUnitTestXMLOutputterHook()
{
}

void CppUnitTestXMLOutputterHook::beginDocument(CppUnit::XmlDocument* pDocument)
{
  if (nullptr == pDocument)
  {
    return;
  }

  std::time_t  nCurrentTime = std::time(nullptr);
  struct tm*   pTimeinfo    = std::localtime(&nCurrentTime);
  const size_t nBufferSize  = 100;
  char         pBuffer[nBufferSize + 1];
  std::memset(pBuffer, 0, nBufferSize + 1);
  const char* pTimeFormat = "%c";
  std::strftime(pBuffer, nBufferSize, pTimeFormat, pTimeinfo);
  std::string sDateTime(pBuffer);

  CppUnit::XmlElement* pXMLElement = new CppUnit::XmlElement("CppUnitTestSuiteInfo", "");
  pXMLElement->addElement(new CppUnit::XmlElement("CppUnitTestsTitle", m_sTestsTitle));
  pXMLElement->addElement(new CppUnit::XmlElement("Time", sDateTime));
  if (false == m_sTotalExecutionTime.empty())
  {
    pXMLElement->addElement(new CppUnit::XmlElement("TotalExecutionTime", m_sTotalExecutionTime));
  }

  pDocument->rootElement().addElement(pXMLElement);
}

void CppUnitTestXMLOutputterHook::endDocument(CppUnit::XmlDocument* pDocument)
{
  if (nullptr == pDocument)
  {
    return;
  }
}

void CppUnitTestXMLOutputterHook::failTestAdded(XmlDocument*, XmlElement*, Test*, TestFailure*)
{
}

void CppUnitTestXMLOutputterHook::successfulTestAdded(XmlDocument*, XmlElement*, Test*)
{
}

void CppUnitTestXMLOutputterHook::statisticsAdded(XmlDocument*, XmlElement*)
{
}

CPPUNIT_NS_END

