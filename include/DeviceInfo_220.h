
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "DeviceInfo_200.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

class DeviceInfo_220 : public DeviceInfo_200
{
  public:

    DeviceInfo_220(
      const cl_device_id nDeviceID,
      const size_t       nDeviceNumber,
      cl_platform_id     tPlatformID,
      const size_t       nPlatformNumber) noexcept;

    virtual ~DeviceInfo_220() noexcept override;

    virtual void getTextOutput(std::ostream& tStream) const noexcept override;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept override;

  private:

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_fp_config,
      OpenCLError,
      OpenCLToolTip>
      m_tHalfPrecisionFloatingPointConfiguration;

    // qq CL_DEVICE_GLOBAL_MEM_TYPE in any header

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tILVersions;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxNumSubGroups;
    // qq Did not find CL_DEVICE_MAX_SUB_GROUPS in any header
    //    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint,
    //    OpenCLError, OpenCLToolTip> m_tMaxSubGroups;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tSubgroupIndependentForwardProgress;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxNamedBarrierCountKHR;

    virtual void getAllDeviceInfo() noexcept override;

    // Intentionally deleted.
    DeviceInfo_220()                          = delete;
    DeviceInfo_220(const DeviceInfo_220& rhs) = delete;
    DeviceInfo_220(DeviceInfo_220&& rhs)      = delete;
    DeviceInfo_220&       operator=(const DeviceInfo_220& rhs) = delete;
    DeviceInfo_220&       operator=(DeviceInfo_220&& rhs) = delete;
    DeviceInfo_220*       operator&()                     = delete;
    const DeviceInfo_220* operator&() const               = delete;
};

