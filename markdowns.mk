
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef MARKDOWNS_MK_INCLUDE_GUARD # {
MARKDOWNS_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/graph_viz.mk.    #
################################################################################
ifeq ($(GRAPH_VIZ_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/graph_viz.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

include $(PROJECT_ROOT)/makefile_includes_graph.mk

MARKDOWN_TARGETS += BoostInfo            \
                    BuildDependencyGraph \
                    Cloc                 \
                    CompilerMacros       \
                    DoxygenConfiguration \
                    DoxygenExternalTools \
                    DynamicLibraries     \
                    GitStatistics        \
                    LicenseAndDisclaimer \
                    LinuxPlatform        \
                    MakefileInformation  \
                    MessageDigests       \
                    OpenCLInformation    \
                    README               \
                    StaticAnalysis       \
                    StaticLibraryObjects \
                    SystemHeaders

.PHONY: $(MARKDOWN_TARGETS)

MARKDOWN_DIR ?= $(CURDIR)/markdown_files

################################################################################
# Extracts the ELF symbols from the input dynamic library and outputs the      #
# results, in doxygen markdown format, to the specified output file.           #
#                                                                              #
# $(1) - dynamic library                                                       #
# $(2) - output file                                                           #
################################################################################
READELF:=$(shell command -v readelf)
CPP_FILT:=$(shell command -v c++filt)
ifneq ($(READELF),) # {
ifneq ($(CPP_FILT),) # {
get_elf_symbols_markdown=$(if $(1), $(if $(2),$(READELF) -Ws $(1) | $(CPP_FILT) | sed 's/@/\\@/g;s/__/\\_\\_/g' | gawk -v SH_LIB=$(1) 'BEGIN { \
    printf("ELF Shared Library %s Symbols\n", SH_LIB);                                                                                         \
    printf("=============================\n");                                                                                                 \
    printf("<table>\n");                                                                                                                       \
    printf("<tr>\n");                                                                                                                          \
    printf("<td colspan=1>\n");                                                                                                                \
    printf("<center><img src=\"elf.png\"/></center>\n");                                                                                       \
    printf("<center><b>Dynamic Library Symbols (readelf -Ws $(1) | c++filt)</b></center>\n");                                                  \
    printf("</td>\n");                                                                                                                         \
  }                                                                                                                                            \
  /Num: * Value * Size * Type * Bind * Vis * Ndx * Name/{                                                                                      \
                                                          printf("| Num | Value | Size | Type | Bind | Vis | Ndx | Name |\n");                 \
                                                          printf("|-----|-------|------|------|------|-----|-----|------|\n");                 \
                                                        }                                                                                      \
  /^ *[[:digit:]]*:/{                                                                                                                          \
                      sub(/:/,"", $$1);                                                                                                        \
                      printf("| %s | %s | %s | %s | %s | %s | %s |", $$1, $$2, $$3, $$4, $$5, $$6, $$7);                                       \
                      for (i = 8; i <= NF; i++)                                                                                                \
                      {                                                                                                                        \
                        printf(" %s", $$i);                                                                                                    \
                      }                                                                                                                        \
                      printf(" |\n");                                                                                                          \
                    }                                                                                                                          \
  END {                                                                                                                                        \
    printf("</tr>\n");                                                                                                                         \
  }' >> $(2)))
else # } {
$(call doxygen_log_error,$(@),The [c++filt] executable was not found)
endif # }
else # } {
$(call doxygen_log_error,$(@),The [readelf] executable was not found)
endif # }

################################################################################
# Writes out the markdown table header lines to the specified output file. If  #
# the output file already exists, it is overwritten.                           #
# $(1) - markdown table output file                                            #
# $(2) - markdown table title (doxygen markdown level 1 header)                #
# $(3) - markdown table caption                                                #
# $(4) - markdown table image file                                             #
# $(5) - ">|" or ">>" operation                                                #
# $(6) - markdown title (doxygen markdown level 1 header), optional argument   #
################################################################################
define markdown_table_header
	@$(if $(1),$(if $(6),echo '# $(6)'                                           >| $(1);))
	@$(if $(1),$(if $(2),$(if $(6),echo "$(2)" >> $(1),echo "$(2)" $(5) $(1));))
	@$(if $(1),echo '======================'                                     >> $(1);)
	@$(if $(1),echo '<table>'                                                    >> $(1);)
	@$(if $(1),echo '<tr>'                                                       >> $(1);)
	@$(if $(1),echo '<td colspan=1>'                                             >> $(1);)
	@$(if $(1),$(if $(4),echo '<center><img src="$(4)"/></center>'               >> $(1);))
	@$(if $(1),$(if $(3),echo "<center><b>$(3)</b></center>"                     >> $(1);))
	@$(if $(1),echo '</td>'                                                      >> $(1);)
endef

################################################################################
# Writes out the markdown table header lines to the specified output file. If  #
# the output file already exists, it is overwritten.                           #
# $(1) - markdown table output file                                            #
# $(2) - markdown table title (doxygen markdown level 1 header)                #
# $(3) - markdown table caption                                                #
# $(4) - markdown table image file                                             #
# $(5) - ">|" or ">>" operation                                                #
# $(6) - image link url                                                        #
# $(7) - markdown title (doxygen markdown level 1 header), optional argument   #
################################################################################
define compiler_markdown_table_header
	@$(if $(1),$(if $(7),echo '# $(7)'                                                                                    >| $(1)))
	@$(if $(1),$(if $(2),$(if $(7),echo "$(2)" >> $(1),echo "$(2)" $(5) $(1))))
	@$(if $(1),echo '======================'                                                                              >> $(1))
	@$(if $(1),echo '<table>'                                                                                             >> $(1))
	@$(if $(1),echo '<tr>'                                                                                                >> $(1))
	@$(if $(1),echo '<td colspan=1>'                                                                                      >> $(1))
	@$(if $(1),$(if $(3),$(if $(4),$(if $(6),echo '<center><a href="$(6)" target="_blank"><img src="$(4)"/></a></center>' >> $(1); \
                                           echo '<center><b>$(3)</b></center>'                                          >> $(1)))))
	@$(if $(1),echo '</td>'                                                                                               >> $(1))
endef

################################################################################
# Updates the specified markdown file so that the anchor ("href") image is a   #
# link. This boils down to removing the substring:                             #
#   ' style="pointer-events: none;"'                                           #
#                                                                              #
# $(1) - markdown file without the ".md" file extension                        #
# $(2) - image file                                                            #
################################################################################
define update_doxygen_image_link
	@find $(DOXYGEN_HTML_DIR) -type f -name "md_*markdown_files_*$(1)\.html" -exec                                                  \
   sed -i 's@\(^.*$(2).*img src=.*$(2).*alt=""\)\( style="pointer-events: none;"\)\(.*\)@\1 style="pointer-events: auto;" \3@' {} \;
endef

.PHONY: markdown_files

MARKDOWN_TARGETS:=$(filter-out $(DISALLOWED_MARKDOWN_TARGETS),$(MARKDOWN_TARGETS))
markdown_files:: $(sort $(MARKDOWN_TARGETS))

$(MARKDOWN_DIR):
	@mkdir -p $(MARKDOWN_DIR)

.PHONY: markdown_clean
markdown_clean:
	@rm -rf $(MARKDOWN_DIR)

doxygen_clean:: markdown_clean

EXE_BUILT=$(wildcard $(EXE))

ifneq ($(BOOST),) # {
BoostInfo: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Boost C++ Libraries,Boost,boost.png,>|)
	@echo '| Attribute | Value |'                                                                       >> $(MARKDOWN_DIR)/$(@).md
	@echo '|-----------|-------|'                                                                       >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) boostinfo | sort | sed 's/^/| /;s/ = \[/ | /;s/\]/ |/'                        | \
   sed -e 's/^| BOOST /| boost include root directory /'                                           \
       -e 's/^| BOOST_DOWNLOAD /| boost website /'                                                 \
       -e 's/^| BOOST_INCLUDE_DIR /| boost include directory /'                                    \
       -e 's/^| BOOST_LATEST_VERSION /| boost latest available version /'                          \
       -e 's/^| BOOST_LIB_DIR /| boost library directory /'                                        \
       -e 's/^| BOOST_VERSION /| boost installed version /'                                        \
       -e 's@\(^.* \)\(http.*\)\( |$$\)@\1<a href="\2" style="color:blue;" target="_blank">\2</a>\3@' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                       >> $(MARKDOWN_DIR)/$(@).md
	@sed -i "s@${HOME}@\$${HOME}@"                                                                         $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),boost information markdown)
	@echo ''
else # } {
BoostInfo:;
endif # }

ifneq ($(EXE),) # {
BUILD_SUFFIX=$(EXE)
else # } {
BUILD_SUFFIX=$(notdir $(CURDIR))
endif # }
BUILD_DEPENDENCY_DOT_GRAPH=build_$(BUILD_SUFFIX).$(DOT_IMAGE_EXTENSION)
ifneq ($(wildcard $(CURDIR)/$(BUILD_DEPENDENCY_DOT_GRAPH)),) # {
DOXYGEN_EXTRA_FILES += $(BUILD_DEPENDENCY_DOT_GRAPH)
BuildDependencyGraph: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'Build Dependency Graph'                                                                                           >| $(MARKDOWN_DIR)/$(@).md
	@echo '======================'                                                                                           >> $(MARKDOWN_DIR)/$(@).md
	@echo '$(call create_image_link,file://$(DOXYGEN_HTML_DIR)/$(BUILD_DEPENDENCY_DOT_GRAPH),$(BUILD_DEPENDENCY_DOT_GRAPH))' >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),build dependency markdown)
	@echo ''
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
post_doxygen::
	@$(call update_doxygen_image_link,BuildDependencyGraph,$(BUILD_DEPENDENCY_DOT_GRAPH))
endif # }
else # } {
BuildDependencyGraph:;
endif # }

ifneq ($(CLOC),UNKNOWN) # {
Cloc: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	+@$(MAKE_NO_DIR) cloc                                                                                       | \
     gawk -v cloc_version=$(CLOC_VERSION) 'BEGIN {                                                              \
     file_array_index     = 0;                                                                                  \
     file_start           = 0;                                                                                  \
     language_array_index = 0;                                                                                  \
     language_start       = 0;                                                                                  \
   }                                                                                                            \
   /^--------------/{ next; }                                                                                   \
   {                                                                                                            \
     if ($$1 == "File")                                                                                         \
     {                                                                                                          \
       file_start = 1;                                                                                          \
     }                                                                                                          \
     else if ($$1 == "Language")                                                                                \
     {                                                                                                          \
       language_start = 1;                                                                                      \
       file_start     = 0;                                                                                      \
     }                                                                                                          \
     else if ((1 == file_start) && ($$1 != "SUM:"))                                                             \
     {                                                                                                          \
       file_lines[file_array_index] = $$0;                                                                      \
       file_array_index++;                                                                                      \
     }                                                                                                          \
     else if (($$1 == "SUM:") && (1 == file_start))                                                             \
     {                                                                                                          \
       file_sum   = $$0;                                                                                        \
       file_start = 0;                                                                                          \
     }                                                                                                          \
     else if ((1 == language_start) && ($$1 != "SUM:"))                                                         \
     {                                                                                                          \
       language_lines[language_array_index] = $$0;                                                              \
       language_array_index++;                                                                                  \
     }                                                                                                          \
     else if (($$1 == "SUM:") && (1 == language_start))                                                         \
     {                                                                                                          \
       language_sum   = $$0;                                                                                    \
       language_start = 0;                                                                                      \
     }                                                                                                          \
   }                                                                                                            \
   END {                                                                                                        \
     printf("# Code Counts (cloc version %s)\n", cloc_version);                                                 \
     printf("Lines Of Code Counts ([%'\''d] files)\n", file_array_index);                                       \
     printf("==========================================\n");                                                    \
     printf("<table>\n");                                                                                       \
     printf("<tr>\n");                                                                                          \
     printf("<td colspan=1>\n");                                                                                \
     printf("<center><img src=\"code_counter.png\"/></center><br>\n");                                          \
     printf("<center><b>Lines Of Code Counts (sorted by file name)</b></center>\n");                            \
     printf("</td>\n");                                                                                         \
     printf("|File Number|File|Blank|Comment|Code|\n");                                                         \
     printf("|----:|:-------|-------:|-------:|-------:|\n");                                                   \
     asort(file_lines);                                                                                         \
     for (i = 1; i <= file_array_index; i++)                                                                    \
     {                                                                                                          \
       field_count = split(file_lines[i], line_array);                                                          \
       printf("|%d|%s|%'\''d|%'\''d|%'\''d|\n", i, line_array[1], line_array[2], line_array[3], line_array[4]); \
     }                                                                                                          \
     field_count = split(file_sum, line_array);                                                                 \
     printf("||%s|%'\''d|%'\''d|%'\''d|\n", line_array[1], line_array[2], line_array[3], line_array[4]);        \
     printf("</tr>\n");                                                                                         \
     printf("Counts By Languages\n");                                                                           \
     printf("===================\n");                                                                           \
     printf("<table>\n");                                                                                       \
     printf("<tr>\n");                                                                                          \
     printf("<td colspan=1>\n");                                                                                \
     printf("<center><img src=\"programming_languages.png\"/></center><br>\n");                                 \
     printf("<center><b>Counts By Languages</b></center>\n");                                                   \
     printf("</td>\n");                                                                                         \
     printf("|Language Number|Language|Files|Blank|Comment|Code|\n");                                           \
     printf("|---:|:-------|-------:|-------:|-------:|-------:|\n");                                           \
     field_count  = split(language_sum, line_array);                                                            \
     files_sum    = line_array[2];                                                                              \
     blanks_sum   = line_array[3];                                                                              \
     comments_sum = line_array[4];                                                                              \
     code_sum     = line_array[5];                                                                              \
     asort(language_lines);                                                                                     \
     for (i = 1; i <= language_array_index; i++)                                                                \
     {                                                                                                          \
       field_count = split(language_lines[i], line_array);                                                      \
       language    = "";                                                                                        \
       for (j = 1; j < (field_count - 3); j++)                                                                  \
       {                                                                                                        \
         if (1 == j)                                                                                            \
         {                                                                                                      \
           language = line_array[j];                                                                            \
         }                                                                                                      \
         else                                                                                                   \
         {                                                                                                      \
           language = language " " line_array[j];                                                               \
         }                                                                                                      \
       }                                                                                                        \
       printf("|%'\''d|%s|%'\''d (%4.2f%%)|%'\''d (%4.2f%%)|%'\''d (%4.2f%%)|%'\''d (%4.2f%%)|\n",              \
               i, language,                                                                                     \
               line_array[field_count - 3], (line_array[field_count - 3] / files_sum)    * 100.0,               \
               line_array[field_count - 2], (line_array[field_count - 2] / blanks_sum)   * 100.0,               \
               line_array[field_count - 1], (line_array[field_count - 1] / comments_sum) * 100.0,               \
               line_array[field_count],     (line_array[field_count]     / code_sum)     * 100.0);              \
     }                                                                                                          \
     printf("||SUM:|%s|%'\''d|%'\''d|%'\''d|\n", files_sum, blanks_sum, comments_sum, code_sum);                \
     printf("</tr>\n");                                                                                         \
   }'                                                                                                          >| $(MARKDOWN_DIR)/$(@).md
	@sed -i "s@${HOME}@\$${HOME}@"                                                                                  $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),cloc markdown)
	@echo ''
else # } {
Cloc:
	@$(call doxygen_log_warning,$(@),The [cloc] executable was not found. Please install the cloc executable from [$(CLOC_DOWNLOAD)] or correct your PATH environment variable)
endif # }

CompilerMacros: CLANG_WEB_SITE=https://clang.llvm.org
CompilerMacros: ORACLE_DEVELOPER_STUDIO_WEB_SITE=https://www.oracle.com/tools/developerstudio
CompilerMacros: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call compiler_markdown_table_header,$(MARKDOWN_DIR)/$(@).md,GNU $(CXX) Compiler Macros,$(CXX) Compiler Macros (version $(CXX_VERSION)): $(CXX) $(COMPILER_MACROS_OPTS),cpp.png,>|,$(GCC_WEB_SITE),Compiler Macros)
	@echo '| Macro Number | g++ Macro $(if $(get_compiler_std),($(call get_compiler_std,g++))) | Value                                 |'                                 >> $(MARKDOWN_DIR)/$(@).md
	@echo '|--------------|-----------------------------|------------------------------------------------------------------------------|'                                 >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) CXX=$(CXX) compiler_macros | sed 's/^\#define /|/;s/ /|/;s/^|/|\\\#define /;s/_/\\_/g;s/$$/|/' | gawk '{ printf("|%d%s\n", NR, $$0); }'              >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                                                                         >> $(MARKDOWN_DIR)/$(@).md
ifneq ($(shell command -v clang),) # {
	@$(call compiler_markdown_table_header,$(MARKDOWN_DIR)/$(@).md,LLVM clang Compiler Macros,clang Compiler Macros (version $(CLANG_VERSION)): clang $(COMPILER_MACROS_OPTS),clang.png,>>,$(CLANG_WEB_SITE))
	@echo '| Macro Number | clang Macro $(if $(get_compiler_std),($(call get_compiler_std,clang))) | Value                             |'                                 >> $(MARKDOWN_DIR)/$(@).md
	@echo '|--------------|-----------------------------|------------------------------------------------------------------------------|'                                 >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) CXX=clang compiler_macros | sed 's/^\#define /|/;s/ /|/;s/^|/|\\\#define /;s/_/\\_/g;s/$$/|/' | gawk '{ printf("|%d%s\n", NR, $$0); }'               >> $(MARKDOWN_DIR)/$(@).md
	@sed -i 's@\(^|\)\(.*define\)\(.*$$\)@\1<span style="font-family:monospace; font-size:large">\2\3@'                                                                      $(MARKDOWN_DIR)/$(@).md
	@sed -i 's@^\(^.*define\)\(.*\)|$$@\1\2</span>|@'                                                                                                                        $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                                                                         >> $(MARKDOWN_DIR)/$(@).md
endif # }
ifneq ($(ORACLE_CC_VERSION),UNKNOWN) # {
	@$(eval CC=$(notdir $(ORACLE_CC)))
	@$(call compiler_markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Oracle $(CC) Compiler Macros,Oracle $(CC) Compiler Macros (version $(ORACLE_CC_VERSION)): $(CC) $(ORACLE_CC_MACROS_OPTS),oracle.png,>>,$(ORACLE_DEVELOPER_STUDIO_WEB_SITE))
	@echo '| Macro Number | $(CC) Macro $(if $(get_compiler_std),($(call get_compiler_std,$(CC)))) | Value                             |'                                 >> $(MARKDOWN_DIR)/$(@).md
	@echo '|--------------|-----------------------------|------------------------------------------------------------------------------|'                                 >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) CXX=$(ORACLE_CC) compiler_macros | sed 's/^\#define /|/;s/ /|/;s/^|/|\\\#define /;s/_/\\_/g;s/$$/|/' | gawk '{ printf("|%d%s\n", NR, $$0); }'        >> $(MARKDOWN_DIR)/$(@).md
	@sed -i 's@\(^|\)\(.*define\)\(.*$$\)@\1<span style="font-family:monospace; font-size:large">\2\3@'                                                                      $(MARKDOWN_DIR)/$(@).md
	@sed -i 's@^\(^.*define\)\(.*\)|$$@\1\2</span>|@'                                                                                                                        $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                                                                         >> $(MARKDOWN_DIR)/$(@).md
endif # }
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),compiler macros markdown)
	@echo ''

DoxygenConfiguration: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
################################################################################
# sed commands:                                                                #
#   o replace '&' with &amp; (for HTML)                                        #
#   o escape '\'                                                               #
#   o escape '@'                                                               #
#   o escape '^#' (to prevent bold text in markdown file                       #
#   o escape '_x' (to prevent italicized text in markdown file                 #
#   o escape single quote                                                      #
#   o escape double quote                                                      #
#   o add a new line at the end of every line that begins with a capital       #
#     letter                                                                   #
#   o join each line with the line below it if that line ends in '-'           #
#   o join the line that ends with Doxy to the one below it                    #
#   o replace all '<' and '>' characters with "&lt;" and "&gt;", respectively  #
#   o add <br> at the end of every line                                        #
#   o add "doxygen Configuration\n=====================' to the top of the     #
#     markdown file to create the title                                        #
################################################################################
	+@$(MAKE_NO_DIR) doxygen_config                          | \
   sed -e 's/&/\&amp;/g;s/\\/\\\\/g;s/@/\\@/g;s/#/\\#/g'     \
       -e 's/_xE3_x81_x84/\\_xE3\\_x81\\_x84/'               \
       -e "s/'/\\'/g"                                        \
       -e 's/"/\\"/g'                                        \
       -e 's/\(^[A-Z].*[^\\]$$\)/\1\n/'                           >| $(MARKDOWN_DIR)/$(@).md
	@sed -i '/^.*http.*-$$/N;s/\n//;s/-\\# //'                         $(MARKDOWN_DIR)/$(@).md
	@sed -i '/^.*http.*Doxy$$/N;s/\n//;s/Doxy.*genXcode/DoxygenXcode/' $(MARKDOWN_DIR)/$(@).md
	@sed -i 's/</\&lt;/g;s/>/\&gt;/g;s/$$/<br>/'                       $(MARKDOWN_DIR)/$(@).md
	@sed -i '1 i\<center><b>doxygen Configuration</b></center>'        $(MARKDOWN_DIR)/$(@).md
	@sed -i '1 i\<center><img src="doxygen_logo.png"/></center>'       $(MARKDOWN_DIR)/$(@).md
	@sed -i '1 i\doxygen Configuration\n====================='         $(MARKDOWN_DIR)/$(@).md
	@sed -i "s@${HOME}@\$${HOME}@g"                                    $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),doxygen configuration markdown)
	@echo ''

DoxygenExternalTools: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,doxygen External Tools,doxygen External Tools,dox_external_tools.png,>|)
	@echo '| Number | Tool | Version | Website |'                                                                                                     >> $(MARKDOWN_DIR)/$(@).md
	@echo '|--------|------|---------|---------|'                                                                                                     >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) dox_apps                                                                                                                     | \
   grep -v '^doxygen'                                                                                                                           | \
   sed 's@\(^.*[^ ]\)\( *version: \[\)\(.*\)\(\].*website: \[\)\(.*\)\(\].*\)@|\1|\3|<a href="\5" style="color:blue;" target="_blank">\5</a>|@' | \
   gawk '{printf("| %d %s\n", NR, $$0);}'                                                                                                           >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                                                     >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),doxygen external tools markdown)
	@echo ''

ifneq ($(wildcard $(MAIN)),) # {
ifneq ($(EXE_BUILT),) # {
DynamicLibraries: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'Dynamic Libraries'                                                >| $(MARKDOWN_DIR)/$(@).md
	@echo '================='                                                >> $(MARKDOWN_DIR)/$(@).md
	@echo '<center><img src="c++_dynamic_libraries.png"/></center>'          >> $(MARKDOWN_DIR)/$(@).md
	@echo '<center><b>Dynamic Libraries</b></center>'                        >> $(MARKDOWN_DIR)/$(@).md
	@echo '<pre>'                                                            >> $(MARKDOWN_DIR)/$(@).md
	@echo '<span style="font-family:monospace; font-size:large">'            >> $(MARKDOWN_DIR)/$(@).md
	@ldd ./$(EXE) | sed 's/ *(.*)$$//' | sort |                            \
   while IFS= read LIB_INFO;                                             \
   do                                                                    \
     NUM_TOKS=$$(echo $${LIB_INFO} | gawk '{ printf("%d\n", NF); }');    \
     if [[ 1 == $${NUM_TOKS} ]];                                         \
     then                                                                \
       echo $${LIB_INFO} | sed 's/ *(.*)$$//';                           \
     else                                                                \
       read -r LIB LIB_FILE <<< $$(echo $${LIB_INFO} | cut -d" " -f1,3); \
       echo $${LIB} "&rarr;" $$(readlink -f $${LIB_FILE});               \
     fi;                                                                 \
   done | column -t | sed 's/ *$$//'                                       >> $(MARKDOWN_DIR)/$(@).md
	@echo '</span>'                                                          >> $(MARKDOWN_DIR)/$(@).md
	@echo '</pre>'                                                           >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),dynamic libraries markdown)
	@echo ''
else # } {
DynamicLibraries:
	@$(call doxygen_log_warning,$(@),The executable [$(EXE)] was not found)
endif # }
else # } {
DynamicLibraries:
	@:
endif # }

ifneq ($(GIT_ROOT),UNKNOWN) # {
GitStatistics: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
ifeq ($(GIT_SUBMODULES),NONE) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,git Information,git Statistics (version $(GIT_VERSION)),git_logo_48x48.png,>|)
else # } {
	@echo '# git Information'                                                                              >| $(MARKDOWN_DIR)/$(@).md
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,git General Information,git Statistics (version $(GIT_VERSION)),git_logo_48x48.png,>>)
endif # }
	@echo '| Row Number | Attribute    | Value                                                                       |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|------------| -------------|-----------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) gitstats | grep -v "^Submodules *: \[[^N][^O][^N][^E]" | sed "s@${HOME}@\$${HOME}@"             | \
   sed 's/^/| /;s/ : /|/;s/\[//;s/\]/ |/'                                                                          | \
   sed 's@\(^.*|.*Remote url *|\)\(https.*\)\( *.*|\)@\1<a href="\2" target="_blank">\2</a><br>\3@'                | \
   gawk '{printf("| %d %s\n", NR, $$0);}'                                                                             >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                       >> $(MARKDOWN_DIR)/$(@).md
ifneq ($(GIT_SUBMODULES),NONE) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,git Submodules Information,git Submodules (version $(GIT_VERSION)),git_animated_submodules.gif,>>)
	+@$(MAKE_NO_DIR) git_submodules                                                      | \
    sed '/^|Submodule *|Latest Commit Date *|Latest Commit *|URL *|$$/a |:---|:---|:---|:---|:---|' | \
    sed 's/\(^|Submodule.*\)/|Number\1/'                                                            | \
    sed 's@\(^.*|\)\(http.*:.*\)\( |$$\)@\1<a href="\2" target="_blank">\2</a><br>\3@'              | \
    gawk '{if (NR > 2) {printf("|%d %s\n", NR - 2, $$0); } else { printf("%s\n", $$0); } }'              >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                          >> $(MARKDOWN_DIR)/$(@).md
endif # }
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),git statistics markdown)
	@echo ''
else # } {
GitStatistics:
	@$(call doxygen_log_warning,$(@),Could not determine the git root directory)
endif # }

LicenseAndDisclaimer: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo 'License And Disclaimer'                                                                                         >| $(MARKDOWN_DIR)/$(@).md
	@echo '======================'                                                                                         >> $(MARKDOWN_DIR)/$(@).md
	@echo '<a href="$(GPL3_TEXT)" target="_blank"><img src="gplv3_logo.png" width="136" height="68"/>$(GPL3_TEXT)</a><br>' >> $(MARKDOWN_DIR)/$(@).md
	@echo 'Copyright © 2018-2023 K. Banerjee<br>'                                                                          >> $(MARKDOWN_DIR)/$(@).md
	@echo 'This program is distributed in the hope that it will be useful,<br>'      | sed 's/ /\&nbsp;/g'                 >> $(MARKDOWN_DIR)/$(@).md
	@echo 'but **WITHOUT ANY WARRANTY**; without even the implied warranty of<br>'   | sed 's/ /\&nbsp;/g'                 >> $(MARKDOWN_DIR)/$(@).md
	@echo '**MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**. See the<br>' | sed 's/ /\&nbsp;/g'                 >> $(MARKDOWN_DIR)/$(@).md
	@echo 'GNU General Public License for more details.<br>'                         | sed 's/ /\&nbsp;/g'                 >> $(MARKDOWN_DIR)/$(@).md
	@echo 'See: $(call create_url_link,$(GPL3_HTML))'                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),license and disclaimer markdown)
	@echo ''

LinuxPlatform: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Linux Platform Information,Linux Platform Information,batman_tux.png,>|)
	@echo '| Row Number | Attribute              | Version                                                            |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|------------|------------------------|--------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) platform | sed 's/ version//;s/%/%%/g;s/^/| /;s/= \[/| /;s/\]$$/ |/' | column -s"|" -o"|" -t     |  \
  gawk '{printf("| %d %s\n", NR, $$0);}'                                                                               >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                        >> $(MARKDOWN_DIR)/$(@).md
	@sed -i "s@${HOME}@\$${HOME}@"                                                                                          $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),linux platform markdown)
	@echo ''

MAKEFILES_DOT_GRAPH_FILE=$(notdir $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE))
DOXYGEN_EXTRA_FILES += $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE)
MakefileInformation: MAKEFILE_FEATURES_URL=$(call create_url_link,https://www.gnu.org/software/make/manual/html_node/Special-Variables.html,,white)
MakefileInformation:: $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) makefile_include_order_constraints_graph | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Makefile List ([$(MAKEFILE_COUNT)] makefiles),Makefiles (version $(MAKE_VERSION)),makefile.png,>|,Makefile Information)
	@echo '| Makefile Number | Makefile Name                                                                          |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|-----------------|----------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md
	@DOX_MK_DIR=$(DOXYGEN_HTML_DIR)/$(DOXYGEN_PROJECT_STUB) && mkdir -p $${DOX_MK_DIR};               \
	for MFILE in $(sort $(realpath $(MAKEFILE_LIST)));                                                \
   do                                                                                               \
     MFILE_BASE=$$(basename $${MFILE});                                                             \
     cp $${MFILE} $${DOX_MK_DIR}/$${MFILE_BASE}.txt;                                                \
     echo "|<a href=\"file://$${DOX_MK_DIR}/$${MFILE_BASE}.txt\" target=\"blank_\">$${MFILE}</a>|"; \
   done | gawk '{ printf("|%d%s\n", NR, $$0); }'                                                  | \
   sed "s@\(^.*href=.*>\)\(${HOME}\)\(.*$$\)@\1\$${HOME}\3@"                                                           >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                        >> $(MARKDOWN_DIR)/$(@).md
ifneq ($(DOT),) # {
	@echo 'Makefile Includes Graph'                                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@echo '======================='                                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@echo '$(call create_image_link,file://$(DOXYGEN_HTML_DIR)/$(MAKEFILES_DOT_GRAPH_FILE),$(MAKEFILES_DOT_GRAPH_FILE))' >> $(MARKDOWN_DIR)/$(@).md
	@echo 'Makefile Includes Constraints Graph'                                                                          >> $(MARKDOWN_DIR)/$(@).md
	@echo '==================================='                                                                          >> $(MARKDOWN_DIR)/$(@).md
	@echo '$(call create_image_link,file://$(DOXYGEN_HTML_DIR)/$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE))' >> $(MARKDOWN_DIR)/$(@).md
endif # }
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,$(MAKE) .FEATURES ([$(words $(.FEATURES))] features),$(MAKE) .FEATURES,make_features.png,>>)
	@echo '|Feature Number | Feature (see: $(MAKEFILE_FEATURES_URL))|'                                                   >> $(MARKDOWN_DIR)/$(@).md
	@echo '|---------------|----------------------------------------|'                                                   >> $(MARKDOWN_DIR)/$(@).md
	@echo $(.FEATURES) | tr ' ' '\n' | sort | sed 's/^/|/;s/$$/|/' |  gawk '{ printf("|%d%s\n", NR, $$0); }'             >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                        >> $(MARKDOWN_DIR)/$(@).md
	@$(if $(filter-out $(@),$(MAKECMDGOALS)),,rm -rf $(DOXYGEN_DIR))
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),makefile markdown)
	@echo ''

ifneq ($(DOT),) # {
dot_markdown_clean:
	@rm -f $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)

post_doxygen:: dot_markdown_clean

clean:: dot_markdown_clean
endif # }

ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
post_doxygen::
ifneq ($(wildcard $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)),) # {
	@$(call update_doxygen_image_link,MakefileInformation,$(MAKEFILES_DOT_GRAPH_FILE))
endif # }
ifneq ($(wildcard $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE)),) # {
	@rm -f $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE)
	@$(call update_doxygen_image_link,MakefileInformation,$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE))
endif # }
endif # }

ifdef MSG_DIGESTS_MK_INCLUDE_GUARD # {
ifneq ($(wildcard $(MAIN)),) # {
ifneq ($(EXE_BUILT),) # {
MessageDigests: PRINTF_STAT=printf "%\x27d bytes\\n" \$$(stat \\-\\-format=\\%s $(EXE))
MessageDigests: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
ifneq ($(OPENSSL)$(GPG)$(SHASUM),) # {
	@echo '# Message Digests' >| $(MARKDOWN_DIR)/$(@).md
ifneq ($(OPENSSL),) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,OpenSSL Message Digests,OpenSSL Message Digests (version $(OPENSSL_VERSION)),$(OPENSSL_LOGO),>>)
	+@$(MAKE_NO_DIR) openssl_digests -j1 2> /dev/null                               | \
   gawk '{printf("%d%s\n", NR, $$0);}'                                            | \
   sed -e 's/\(^[0-9]\+\)\($(EXE) \+size\)\(.*=\)/|\1|$(PRINTF_STAT)|\2|/'          \
       -e 's/\(^[0-9]\+\)\(openssl dgst -.* $(EXE) \)\(→\)/|\1|\2|/'                \
       -e 's/ = /|/'                                                                \
       -e 's/$$/|/'                                                                 \
       -e '1s/^/|-|-----|-----|-----|\n/'                                           \
       -e '1s/^/|Row Number|Command|Digest Function|OpenSSL Digest for $(EXE)|\n/' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                    >> $(MARKDOWN_DIR)/$(@).md
endif # }
ifneq ($(GPG),) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,GnuPG Message Digests,GnuPG Message Digests (version $(GPG_VERSION)),$(GPG_LOGO),>>)
	+@$(MAKE_NO_DIR) gpg_digests -j1 2> /dev/null                                 | \
   gawk '{printf("%d%s\n", NR, $$0);}'                                          | \
   sed -e 's/\(^[0-9]\+\)\($(EXE) \+size\)\(.*=\)/|\1|$(PRINTF_STAT)|\2|/'        \
       -e 's/\(^[0-9]\+\)\(gpg --print-md .* $(EXE) \)\(→\)/|\1|\2|/'             \
       -e 's/--print-md/\\-\\-print-md/'                                          \
       -e 's/($(EXE)).*= /|/'                                                     \
       -e 's/$$/|/'                                                               \
       -e '1s/^/|-|-----|-----|-----|\n/'                                         \
       -e 's/= /|/'                                                               \
       -e '1s/^/|Row Number|Command|Digest Function|GnuPG Digest for $(EXE)|\n/' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                  >> $(MARKDOWN_DIR)/$(@).md
endif # }
ifneq ($(SHASUM),) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,shasum Message Digests,shasum Message Digests (version $(SHASUM_VERSION)),$(SHASUM_LOGO),>>)
	+@$(MAKE_NO_DIR) shasum_digests -j1 2> /dev/null                               | \
   gawk '{printf("%d%s\n", NR, $$0);}'                                           | \
   sed -e 's/\(^[0-9]\+\)\($(EXE) \+size\)\(.*=\)/|\1|$(PRINTF_STAT)|\2|/'         \
       -e 's/\(^[0-9]\+\)\(.*\)/|\1|\2/'                                           \
       -e 's/→/|/'                                                                 \
       -e 's/($(EXE)).*= /|/'                                                      \
       -e 's/$$/|/'                                                                \
       -e '1s/^/|-|-----|-----|-----|\n/'                                          \
       -e 's/= /|/'                                                                \
       -e '1s/^/|Row Number|Command|Digest Function|shasum Digest for $(EXE)|\n/' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                   >> $(MARKDOWN_DIR)/$(@).md
endif # }
ifneq ($(shell ls -1 /bin/*sum /usr/local/bin/*sum 2> /dev/null | egrep -v shasum | xargs -n 1 basename 2> /dev/null | egrep "^(md5|sha|b2).*sum" | sort -u),) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,GNU Core Utils Message Digests,GNU Core Utils Message Digests (version $(GNU_CORE_UTILS_VERSION)),$(GNU_CORE_UTILS_LOGO),>>)
	+@$(MAKE_NO_DIR) gnu_core_utils_digests -j1 2> /dev/null                               | \
   gawk '{printf("%d%s\n", NR, $$0);}'                                                   | \
   sed -e 's/\(^[0-9]\+\)\($(EXE) \+size\)\(.*=\)/|\1|$(PRINTF_STAT)|\2|/'                 \
       -e 's/\(^[0-9]\+\)\(.*\)/|\1|\2/'                                                   \
       -e 's/→/|/'                                                                         \
       -e 's/($(EXE)).*= /|/'                                                              \
       -e 's/$$/|/'                                                                        \
       -e '1s/^/|-|-----|-----|-----|\n/'                                                  \
       -e 's/= /|/'                                                                        \
       -e '1s/^/|Row Number|Command|Digest Function|GNU Core Utils Digest for $(EXE)|\n/' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                           >> $(MARKDOWN_DIR)/$(@).md
endif # }
else # } {
	@$(call doxygen_log_warning,$(@),None of openssl, gpg, and shasum executable were found)
endif # }
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),message digests markdown)
	@echo ''
else # } {
MessageDigests:
	@$(call doxygen_log_warning,$(@),The executable [$(EXE)] was not found)
endif # }
else # } {
MessageDigests:
	@:
endif # }
else # } {
MessageDigests:
	@:
endif # }

ifdef OPENCL_MK_INCLUDE_GUARD # {
OpenCLInformation: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@echo '# OpenCL Information'                                                                                                                                 >| $(MARKDOWN_DIR)/$(@).md
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,OpenCL Installable Client Driver Library,OpenCL Library (used for linking),opencl_logo.png,>>)
	@echo '| Library Name | Library File | SONAME | Size |'                                                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@echo '|:-------------|:-------------|-------:|-----:|'                                                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@echo "|$(notdir $(OPENCL_LIB_SO))|$(OPENCL_LIB_SO)|$(call get_soname,$(OPENCL_LIB_SO))|SIZE: $$(printf "%'d bytes" $$(stat --format=%s $(OPENCL_LIB_SO)))|" >> $(MARKDOWN_DIR)/$(@).md
	@$(call get_elf_symbols_markdown,$(OPENCL_LIB_SO),$(MARKDOWN_DIR)/$(@).md)
ifneq ($(wildcard $(OPENCL_ICD_PATH)/.),) # {
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,OpenCL ICD's ([$$(ls -1 $(OPENCL_ICD_PATH)/*.icd | wc -l)] ICD file(s)),OpenCL ICD's,opencl_icd.png,>>)
	@echo '| ICD Number | OpenCL ICD      | Library Name | Library File | SONAME | Size |'                                                                       >> $(MARKDOWN_DIR)/$(@).md
	@echo '|:-----------|:----------------|:-------------|:-------------|-------:|-----:|'                                                                       >> $(MARKDOWN_DIR)/$(@).md
	+@$(MAKE_NO_DIR) icd_opencl | egrep "(INFO|ERROR).*: OpenCL library: " | sed 's/^.*library: \[/|/;s/\] \+→ \+\[/|/g;s/\]/|/;s/was not found/__was not found__/;s/SONAME: //;s/SIZE: //' | \
    gawk '{ printf("|%d%s\n", NR, $$0); }'                                                                                                                     >> $(MARKDOWN_DIR)/$(@).md
	@echo    '</tr>'                                                                                                                                             >> $(MARKDOWN_DIR)/$(@).md
else # } {
	@$(call doxygen_log_warning,$(@),Could not find the OpenCL ICD directory [$(OPENCL_ICD_PATH)])
endif # }
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,OpenCL Applications/Libraries,OpenCL Applications/Libraries,opencl_logo_48x48.png,>>)
	@echo '| Number | Application | Link                                                                                      |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|--------| ------------|-------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md
	@{ echo '| BOLT               | $(call create_url_link,https://github.com/HSA-Libraries/Bolt/releases/tag/v1.1GA)         |'; \
     echo '| CLBlast            | $(call create_url_link,https://github.com/CNugteren/CLBlast)                              |'; \
     echo '| CLTune             | $(call create_url_link,https://github.com/CNugteren/CLTune/)                              |'; \
     echo '| DeepCL             | $(call create_url_link,https://github.com/hughperkins/DeepCL)                             |'; \
     echo '| EasyCL             | $(call create_url_link,https://github.com/hughperkins/EasyCL)                             |'; \
     echo '| GSL-CL             | $(call create_url_link,https://sourceforge.net/projects/gsl-cl/?source=directory)         |'; \
     echo '| OpenCV             | $(call create_url_link,https://opencv.org)                                                |'; \
     echo '| OpenCLBLAS         | $(call create_url_link,https://sourceforge.net/projects/openclblas/files/latest/download) |'; \
     echo '| RaijinCL           | $(call create_url_link,https://www.raijincl.org/)                                         |'; \
     echo '| ViennaCL           | $(call create_url_link,http://viennacl.sourceforge.net/)                                  |'; \
     echo '| clBLAS             | $(call create_url_link,https://github.com/clMathLibraries/clBLAS)                         |'; \
     echo '| clFFT              | $(call create_url_link,https://github.com/clMathLibraries/clFFT)                          |'; \
     echo '| clRNG              | $(call create_url_link,https://github.com/clMathLibraries/clRNG)                          |'; \
     echo '| clSPARSE           | $(call create_url_link,https://github.com/clMathLibraries/clSPARSE)                       |'; \
     echo '| clew               | $(call create_url_link,https://github.com/martijnberger/clew)                             |'; } | \
   gawk '{ printf("|%d%s\n", NR, $$0); }'                                                                                      >> $(MARKDOWN_DIR)/$(@).md
	@echo    '</tr>'                                                                                                             >> $(MARKDOWN_DIR)/$(@).md
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,OpenCL References,OpenCL References,opencl_shiny.png,>>)
	@echo '| Number | Reference                           | Link                                                                           |' >> $(MARKDOWN_DIR)/$(@).md
	@echo '|------- | ------------------------------------|--------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md
	@{ echo        '| AMD                                 | $(call create_url_link,https://developer.amd.com/tools-and-sdks)               |'; \
     echo        '| Intel                               | $(call create_url_link,https://software.intel.com/en-us/opencl-sdk)            |'; \
     echo        '| Intel Educational Resources         | $(call create_url_link,https://software.intel.com/en-us/opencl-sdk/training)   |'; \
     echo        '| Khronos                             | $(call create_url_link,https://www.khronos.org)                                |'; \
     echo        '| Khronos Reference Guides            | $(call create_url_link,https://www.khronos.org/developers/reference-cards)     |'; \
     echo        '| Khronos Unified C API Headers URL   | $(call create_url_link,$(KHRONOS_UNIFIED_C_HEADERS_URL))                       |'; \
     echo        '| Khronos Unified C++ API Headers URL | $(call create_url_link,$(KHRONOS_UNIFIED_CPP_HEADERS_URL))                     |'; \
     echo        '| OpenCL Implementations              | $(call create_url_link,https://www.iwocl.org/resources/opencl-implementations) |'; \
     echo        '| OpenCL Resources                    | $(call create_url_link,https://www.khronos.org/opencl/resources)               |'; \
     echo        '| OpenCL Specifications               | $(call create_url_link,https://www.khronos.org/registry/OpenCL/specs)          |'; \
     echo        '| POCL                                | $(call create_url_link,http://portablecl.org)                                  |'; \
     echo        '| YouTube OpenCL Videos               | $(call create_url_link,https://www.youtube.com/results?search_query=opencl)    |'; \
     echo        '| google OpenCL search                | $(call create_url_link,https://www.google.com/search?q=opencl)                 |'; \
     echo        '| nVidia                              | $(call create_url_link,https://developer.nvidia.com/opencl)                    |'; } | \
   gawk '{ printf("|%d%s\n", NR, $$0); }'                                                                                                   >> $(MARKDOWN_DIR)/$(@).md
	@echo    '</tr>'                                                                                                                          >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),OpenCL information markdown)
	@echo ''
else # } {
OpenCLInformation:
	@:
endif # }

README: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	+@$(MAKE_NO_DIR) readme README_MD=$(MARKDOWN_DIR)/README.md
	@sed -i 's@^\*\*Y\*\*et \*\*a\*\*nother Open\*\*CL\*\* \*\*info\*\*rmation application\.$$@<b>Y</b>et <b>a</b>nother Open<b>CL</b> <b>info</b>rmation application.@' $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/README.md,$(EDITOR),readme markdown)
	@echo ''

StaticAnalysis: DOXYGEN_CCCC_OUTPUT_DIR=$(DOXYGEN_HTML_DIR)/$(CCCC_OUTPUT_DIR)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(CCCC_OUTPUT_DIR)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(CODE2FLOW_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(CPPCHECK_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(CPPCLEAN_HTML)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(FLAKE8_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(PYCALLGRAPH_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(PYLINT_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT_DIRS += $(SCAN_BUILD_OUTPUT)
StaticAnalysis: STATIC_ANALYSIS_OUTPUT=$(wildcard $(STATIC_ANALYSIS_OUTPUT_DIRS))
StaticAnalysis: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(if $(STATIC_ANALYSIS_OUTPUT),echo '# Static Analysis' >| $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(CCCC_HTML_FILE)),                                                                                                                             \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,cccc Software Metrics,cccc Results,cyclomatic_formula.png,>>)                                            \
     echo '| <div style="width:500px"><a href="$(CCCC_GITHUB_URL)" style="color:white;" target="_blank"><b>cccc</b></a> Files</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_CCCC_OUTPUT_DIR)/$(notdir $(CCCC_HTML_FILE)),cccc HTML Results)                |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_CCCC_OUTPUT_DIR)/$(notdir $(CCCC_LOG)).txt,cccc Log File)                      |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_CCCC_OUTPUT_DIR)/$(notdir $(CCCC_DATABASE)),cccc Database Dump)                |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '</tr>'                                                                                                                      >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(CODE2FLOW_OUTPUT)/*.$(C2FLOW_EXT)),                                                                                                                                                  \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,code2flow Static Analysis,code2flow python Static Analysis Flowchart(s),flowchart.png,>>)                                                       \
     echo '| <div style="width:500px"><a href="$(CODE2FLOW_URL)" style="color:white;" target="_blank"><b>code2flow python Static Analyzer Flowchart(s)</b></a> Files</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|---------------------------------------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md; \
     $(foreach c,$(notdir $(wildcard $(CODE2FLOW_OUTPUT)/*.$(C2FLOW_EXT))),echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(CODE2FLOW_OUTPUT)/$(c),$(subst _, ,$(notdir $(c))) flowchart) |' >> $(MARKDOWN_DIR)/$(@).md;) \
     echo '</tr>'                                                                                                                                                             >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(CPPCHECK_OUTPUT)/index.html),                                                                                                                                      \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,cppcheck Static Analysis,cppcheck Static Analysis Results,cppcheck.png,>>)                                                    \
     echo '| <div style="width:500px"><a href="$(CPPCHECK_URL)" style="color:white;" target="_blank"><b>cppcheck Static Analyzer</b></a> HTML File</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|---------------------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(CPPCHECK_OUTPUT)/index.html,cppcheck HTML results)                                      |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '</tr>'                                                                                                                                           >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(CPPCLEAN_HTML)),                                                                                                                                                   \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,cppclean Static Analysis,cppclean Static Analysis Results,clean.png,>>)                                                       \
     echo '| <div style="width:500px"><a href="$(CPPCLEAN_URL)" style="color:white;" target="_blank"><b>cppclean Static Analyzer</b></a> HTML File</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|---------------------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(CPPCLEAN_OUTPUT)/$(notdir $(CPPCLEAN_HTML)),cppclean HTML results)                      |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '</tr>'                                                                                                                                           >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(FLAKE8_OUTPUT)),                                                                                                                                       \
     $(eval FLAKE8_DASHBOARD_SUFFIX=dashboard)                                                                                                                              \
     $(eval FLAKE8_HTML_SUFFIX=html)                                                                                                                                        \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,flake8 Static Analysis,flake8 Static Analysis Results,flake_small.png,>>)                                         \
     echo '| <div style="width:500px"><a href="$(FLAKE8_URL)" style="color:white;" target="_blank"><b>flake8 Linter</b></a> Files</div>    |' >> $(MARKDOWN_DIR)/$(@).md;   \
     echo '|-------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md;   \
     $(foreach py,$(subst .py,_py,$(notdir $(PYTHON_SCRIPTS))),                                                                                                             \
       $(if $(wildcard $(FLAKE8_OUTPUT)/$(py)/$(FLAKE8)_$(FLAKE8_DASHBOARD_SUFFIX)/index.html),                                                                             \
         echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(FLAKE8_OUTPUT)/$(py)/$(FLAKE8)_$(FLAKE8_DASHBOARD_SUFFIX)/index.html) |' >> $(MARKDOWN_DIR)/$(@).md;)  \
       $(if $(wildcard $(FLAKE8_OUTPUT)/$(py)/$(FLAKE8)_$(FLAKE8_HTML_SUFFIX)/index.html),                                                                                  \
         echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(FLAKE8_OUTPUT)/$(py)/$(FLAKE8)_$(FLAKE8_HTML_SUFFIX)/index.html)      |' >> $(MARKDOWN_DIR)/$(@).md;)) \
     echo '</tr>'                                                                                                                          >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(PYLINT_OUTPUT)),                                                                                                                                   \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,pylint Static Analysis,pylint Static Analysis Results,pylint.png,>>)                                          \
     echo '| <div style="width:500px"><a href="$(PYLINT_URL)" style="color:white;" target="_blank"><b>pylint Linter</b></a> Files</div> |' >> $(MARKDOWN_DIR)/$(@).md;  \
     echo '|----------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md;  \
     $(foreach file,$(wildcard $(PYLINT_OUTPUT)/$(PYLINT_REPORT_PREFIX)/*.html),                                                                                        \
       echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(PYLINT_OUTPUT)/$(PYLINT_REPORT_PREFIX)/$(notdir $(file)))            |' >> $(MARKDOWN_DIR)/$(@).md;) \
     $(foreach file,$(wildcard $(PYLINT_OUTPUT)/$(PYLINT_JSON2HTML_PREFIX)/*.html),                                                                                     \
       echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(PYLINT_OUTPUT)/$(PYLINT_JSON2HTML_PREFIX)/$(notdir $(file)))         |' >> $(MARKDOWN_DIR)/$(@).md;) \
     echo '</tr>'                                                                                                                          >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(PYCALLGRAPH_OUTPUT)/*.$(PYCALLGRAPH_EXT)),                                                                                                                                               \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,pycallgraph Static Analysis,pycallgraph python Static Analysis Flowchart(s),python_graph.png,>>)                                                    \
     echo '| <div style="width:500px"><a href="$(PYCALLGRAPH_URL)" style="color:white;" target="_blank"><b>pycallgraph python Static Analyzer Flowchart(s)</b></a> Files</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|---------------------------------------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md;     \
     $(foreach g,$(notdir $(wildcard $(PYCALLGRAPH_OUTPUT)/*.$(PYCALLGRAPH_EXT))),echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(PYCALLGRAPH_OUTPUT)/$(g),$(subst .$(PYCALLGRAPH_EXT),.py,$(notdir $(g))) flowchart) |' >> $(MARKDOWN_DIR)/$(@).md;) \
     echo '</tr>'                                                                                                                                                             >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(wildcard $(SCAN_BUILD_OUTPUT)),$(if $(SCAN_BUILD_INDEX_FILE),$(eval SCAN_BUILD_INDEX:=$(shell find $(SCAN_BUILD_OUTPUT) -type f -name $(SCAN_BUILD_INDEX_FILE)))))
	@$(if $(SCAN_BUILD_INDEX),                                                                                                                                                                  \
     $(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,clang Static Analysis,clang Static Analysis Results,static_analysis.png,>>)                                                         \
     echo '| <div style="width:500px"><a href="$(CLANG_STATIC_ANALYZER_URL)" style="color:white;" target="_blank"><b>clang Static Analyzer</b></a> Files</div> |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '|---------------------------------------------------------------------------------------------------------------------------------------------------|' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '| $(call create_url_link,file://$(DOXYGEN_HTML_DIR)/$(SCAN_BUILD_INDEX),scan-build HTML Results)                                                    |' >> $(MARKDOWN_DIR)/$(@).md; \
     echo '</tr>'                                                                                                                                                 >> $(MARKDOWN_DIR)/$(@).md)
	@$(if $(STATIC_ANALYSIS_OUTPUT),$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),static analysis  markdown))

################################################################################
# Copies static analysis files to the doxygen directory.                       #
#                                                                              #
# $(1) - static analysis files source directory                                #
################################################################################
define copy_static_analysis_files
$(if $(wildcard $(1)),                                                                                        \
  rm -rf $(DOXYGEN_HTML_DIR)/$(1) && mkdir $(DOXYGEN_HTML_DIR)/$(1) && cp -R $(1)/* $(DOXYGEN_HTML_DIR)/$(1))
endef

post_doxygen::
	@[[ -e "$(CCCC_LOG)" ]] && mv $(CCCC_LOG) $(CCCC_LOG).txt || :
	@$(call copy_static_analysis_files,$(CCCC_OUTPUT_DIR))
	@$(call copy_static_analysis_files,$(CODE2FLOW_OUTPUT))
	@$(call copy_static_analysis_files,$(CPPCHECK_OUTPUT))
	@$(call copy_static_analysis_files,$(CPPCLEAN_OUTPUT))
	@$(call copy_static_analysis_files,$(FLAKE8_OUTPUT))
	@$(call copy_static_analysis_files,$(PYLINT_OUTPUT))
	@$(call copy_static_analysis_files,$(PYCALLGRAPH_OUTPUT))
	@$(call copy_static_analysis_files,$(SCAN_BUILD_OUTPUT))

ifneq ($(wildcard $(LOCAL_LIB)),) # {
StaticLibraryObjects: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,Static Library $(notdir $(LOCAL_LIB)) Objects ([$(shell ar -t $(LOCAL_LIB) | wc -l)] object files),Static Library Objects (ar -tv $(LOCAL_LIB)),object_file.png,>|)
	@echo '| Object File Number | Permissions | Size | Date/Time | Object File |'                                                                                 >> $(MARKDOWN_DIR)/$(@).md
	@echo '|:-------------------|-------------|-----:|-----------|------------:|'                                                                                 >> $(MARKDOWN_DIR)/$(@).md
	@ar -tv $(LOCAL_LIB) | sed 's/-/\\-/g' | sort -k8,8 | gawk '{ printf("|%'\''d|%s|%'\''d bytes|%s %s, %s %s|%s|\n", NR, $$1, $$3, $$4, $$5, $$7, $$6, $$8); }' >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                                                                                 >> $(MARKDOWN_DIR)/$(@).md
	@sed -i "s@${HOME}@\$${HOME}@"                                                                                                                                   $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),static library markdown)
	@echo ''
endif # }

SOURCE_CODE_EXTENSIONS=c cpp h
CODE_SOURCE_FILES=$(filter $(addprefix %.,$(SOURCE_CODE_EXTENSIONS)),$(CODE_FILES))
ifneq ($(CODE_SOURCE_FILES),) # {
SystemHeaders: SPAN_START=<span style=\"font-family:monospace; font-size:large\">
SystemHeaders: SPAN_END=</span>
SystemHeaders: | $(MARKDOWN_DIR)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call markdown_table_header,$(MARKDOWN_DIR)/$(@).md,System Headers,System Headers,include.png,>|)
	@echo '| File Number | File Name |'                                                           >> $(MARKDOWN_DIR)/$(@).md
	@echo '|:------------|:----------|'                                                           >> $(MARKDOWN_DIR)/$(@).md
	@grep -h "^#include <.*>" $(CODE_SOURCE_FILES) | sort -u               | \
   sed 's/^#include //;s/</\&lt;/g;s/>/\&gt;/g;s/ /\&nbsp;/g;s/$$/<br>/' | \
   gawk '{ printf("|%d|$(SPAN_START)%s$(SPAN_END)|\n", NR, $$1); }'                             >> $(MARKDOWN_DIR)/$(@).md
	@echo '</tr>'                                                                                 >> $(MARKDOWN_DIR)/$(@).md
	@$(call open_sole_target_file,$(@),$(MARKDOWN_DIR)/$(@).md,$(EDITOR),system headers markdown)
	@echo ''
else # } {
SystemHeaders:
	@:
endif # }

endif # }

