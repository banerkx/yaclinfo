
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "DeviceInfo.h"
#include "OpenCLString.h"
#include "cl_command_queue_properties.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

// For those HTML rows that do not need a tool tip nor units.
template<class T>
void setHTMLTableRow(std::ostream& tStream, const std::string& sName, const T& tData) noexcept;
void setHTMLTableRow(std::ostream& tStream, const std::string& sName, const char* sData) noexcept;

// For those HTML rows that need a tool tip but no units.
template<class T> void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const T&             tData,
  const OpenCLToolTip& tToolTip) noexcept;
void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const char*          sData,
  const OpenCLToolTip& tToolTip) noexcept;

// For those HTML rows that don't need a tool tip but need units.
template<class T> void setHTMLTableRow(
  std::ostream&      tStream,
  const std::string& sName,
  const T&           tData,
  const OpenCLUnits& tUnits) noexcept;
void setHTMLTableRow(
  std::ostream&      tStream,
  const std::string& sName,
  const char*        sData,
  const OpenCLUnits& tUnits) noexcept;

// For those HTML rows that need a tool tip *and* units.
template<class T> void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const T&             tData,
  const OpenCLToolTip& tToolTip,
  const OpenCLUnits&   tUnits) noexcept;
void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const char*          sData,
  const OpenCLToolTip& tToolTip,
  const OpenCLUnits&   tUnits) noexcept;

std::string toHTMLTokens(const std::vector<std::string>& tTokens);

std::string changeToHTMLGigaHertz(const std::string& s);

const std::string& getHTMLUnits(const OpenCLUnits& tUnits);

