
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cerrno>
#include <cstring>
#include <map>
#include <memory>
#include <mutex>
#include <string>

#include "SingletonBase.h"

// NOTE: errno is thread local, meaning that if errno is set in one
//       thread, it does not affect errno's value in any other thread (see
//       man 3 errno).
class SimpleScopedErrno
{
  public:

    SimpleScopedErrno();

    virtual ~SimpleScopedErrno();

    inline int getSavedErrno() const
    {
      return (m_nSavedErrno);
    }

    // Returns the current errno, **not** the saved errno.
    inline operator int() const
    {
      return (errno);
    }

  private:

    const int m_nSavedErrno;

    // Intentionally deleted.
    SimpleScopedErrno(const SimpleScopedErrno& rhs) = delete;
    SimpleScopedErrno(SimpleScopedErrno&& rhs)      = delete;
    SimpleScopedErrno&       operator=(const SimpleScopedErrno& rhs) = delete;
    SimpleScopedErrno&       operator=(SimpleScopedErrno&& rhs) = delete;
    SimpleScopedErrno*       operator&()                        = delete;
    const SimpleScopedErrno* operator&() const                  = delete;
};

class StrError : public SingletonBase<StrError>
{
  public:

    const std::string getErrorMessage(const int nErrno);

    virtual ~StrError();

  private:

    StrError();

    // For the XSI-compliant version:
    //   int strerror_r(int errnum, char* buf, size_t buflen);
    void addErrorMessage(const int nStat, const int nErrno);

    // For the GNU specific version:
    //   char* strerror_r(int errnum, char* buf, size_t buflen);
    void addErrorMessage(const char* sErrorMessage, const int nErrno);

    void addRetrievalErrorMessage(const int nErrno);

    constexpr static int    m_nMaxErrno   = 255;
    constexpr static int    m_nMinErrno   = -StrError::m_nMaxErrno;
    constexpr static size_t m_nBufferSize = 256;

    std::map<int, std::string> m_tErrorMessages;
    std::unique_ptr<char>      m_sErrorMessage;
    std::mutex                 m_tMapMutex;

    constexpr static const char* sPrefix = "Could not retrieve error message for error code [";

    friend class SingletonBase<StrError>;

    // Intentionally deleted.
    StrError(const StrError& rhs) = delete;
    StrError(StrError&& rhs)      = delete;
    StrError&       operator=(const StrError& rhs) = delete;
    StrError&       operator=(StrError&& rhs) = delete;
    StrError*       operator&()               = delete;
    const StrError* operator&() const         = delete;
};

