
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StrError.h"

#include <sstream>

SimpleScopedErrno::SimpleScopedErrno() : m_nSavedErrno(errno)
{
  errno = 0;
}

SimpleScopedErrno::~SimpleScopedErrno()
{
  errno = m_nSavedErrno;
}

StrError::StrError() :
  SingletonBase<StrError>(),
  m_tErrorMessages(),
  m_sErrorMessage(new char[StrError::m_nBufferSize + 1]),
  m_tMapMutex()
{
  std::memset(m_sErrorMessage.get(), 0, StrError::m_nBufferSize + 1);
}

StrError::~StrError()
{
  m_tErrorMessages.clear();
}

const std::string StrError::getErrorMessage(const int nErrno)
{
  std::lock_guard<std::mutex> tGuard(m_tMapMutex);

  SimpleScopedErrno tErrno;

  const auto i = m_tErrorMessages.find(nErrno);
  if (i != m_tErrorMessages.end())
  {
    return (i->second);
  }

  if ((StrError::m_nMinErrno <= nErrno) && (nErrno <= StrError::m_nMaxErrno))
  {
    addErrorMessage(strerror_r(nErrno, m_sErrorMessage.get(), StrError::m_nBufferSize), nErrno);
    std::memset(m_sErrorMessage.get(), 0, StrError::m_nBufferSize);
    return (m_tErrorMessages.find(nErrno)->second);
  }

  const std::string sUnknownError("Unknown error ");
  return (sUnknownError + std::to_string(nErrno));
}

// For the XSI-compliant version:
//   int strerror_r(int errnum, char* buf, size_t buflen);
void StrError::addErrorMessage(const int nStat, const int nErrno)
{
  if (0 == nStat)
  {
    m_tErrorMessages.insert(std::make_pair(nErrno, m_sErrorMessage.get()));
  }
  else
  {
    addRetrievalErrorMessage(nErrno);
  }
}

// For the GNU specific version:
//   char* strerror_r(int errnum, char* buf, size_t buflen);
void StrError::addErrorMessage(const char* sErrorMessage, const int nErrno)
{
  if (nullptr != sErrorMessage)
  {
    m_tErrorMessages.insert(std::make_pair(nErrno, sErrorMessage));
  }
  else
  {
    addRetrievalErrorMessage(nErrno);
  }
}

void StrError::addRetrievalErrorMessage(const int nErrno)
{
  std::stringstream tErrorMessage;
  tErrorMessage << StrError::sPrefix << nErrno << "] due to [";

  switch (errno)
  {
    case EINVAL:
      tErrorMessage << "This errno value is not a valid error number.]";
      break;

    case ERANGE:
      tErrorMessage << "Insufficient storage of [" << StrError::m_nBufferSize
                    << "] bytes, was supplied to contain the error description string]";
      break;

    default:
      break;
  };
  m_tErrorMessages.insert(std::make_pair(nErrno, tErrorMessage.str()));
}

