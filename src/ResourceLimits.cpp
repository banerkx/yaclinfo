
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ResourceLimits.h"

#include <sys/resource.h>
#include <sys/time.h>

#include <algorithm>
#include <climits>
#include <iomanip>
#include <iterator>
#include <limits>
#include <map>
#include <numeric>

#include "ScopedErrno.h"
#include "StringToPrimitive.h"

ResourceLimits::ResourceLimits() :
  SingletonBase<ResourceLimits>(),
  m_tResourceLimits(),
  m_tConfigurationParams(),
  m_tConcatenatedResources(),
  m_nDescriptionWidth(0),
  m_nResultWidth(0),
  m_tRusageSelf(),
  m_nRusageSelfStat(-1),
  m_tSelfInfo(),
  m_tRusageChildren(),
  m_nRusageChildrenStat(-1),
  m_tChildrenInfo(),
  m_tRusageThread(),
  m_nRusageThreadStat(-1),
  m_tThreadInfo(),
  m_nProcessPriority(-1)
{
  populateResources();
  getLimits();
  populateConfigurationParams();
  getConfigurationParams();
  concatenate();
  setRusageInfo();
  setFieldWidths();
}

ResourceLimits::~ResourceLimits()
{
  m_tResourceLimits.clear();
  m_tConfigurationParams.clear();
  m_tConcatenatedResources.clear();
}

void ResourceLimits::populateResources()
{
  m_tResourceLimits.emplace_back(RLIMIT_AS, "MAX VIRTUAL MEMORY", "");
  m_tResourceLimits.emplace_back(RLIMIT_CORE, "MAX CORE FILE", "");
  m_tResourceLimits.emplace_back(RLIMIT_CPU, "MAX CPU TIME", "");
  m_tResourceLimits.emplace_back(RLIMIT_DATA, "MAX DATA SEGMENT", "");
  m_tResourceLimits.emplace_back(RLIMIT_FSIZE, "MAX FILE SIZE", "");
  m_tResourceLimits.emplace_back(RLIMIT_LOCKS, "MAX LOCKS AND LEASES", "");
  m_tResourceLimits.emplace_back(RLIMIT_MEMLOCK, "MAX LOCKED MEMORY", "");
  m_tResourceLimits.emplace_back(RLIMIT_MSGQUEUE, "MAX POSIX MESSAGE QUEUES SIZE", "");
  m_tResourceLimits.emplace_back(RLIMIT_NICE, "MAX NICE VALUE", "");
  m_tResourceLimits.emplace_back(RLIMIT_NOFILE, "MAX OPEN FILES PLUS 1", "");
  m_tResourceLimits.emplace_back(RLIMIT_NPROC, "MAX THREADS", "");
  m_tResourceLimits.emplace_back(RLIMIT_RSS, "MAX RESIDENT VIRTUAL PAGES", "");
  m_tResourceLimits.emplace_back(RLIMIT_RTPRIO, "MAX REAL-TIME PRIORITY", "");
  m_tResourceLimits.emplace_back(
    RLIMIT_RTTIME,
    "MAX REAL-TIME CPU CONSUMPTION W/O BLOCKING SYSTEM CALL",
    "");
  m_tResourceLimits.emplace_back(RLIMIT_SIGPENDING, "MAX PENDING SIGNALS", "");
  m_tResourceLimits.emplace_back(RLIMIT_STACK, "MAX STACK SIZE", "");
}

void ResourceLimits::getLimits()
{
  struct rlimit tLimits;
  ScopedErrno   tErrno;

  std::stringstream tInfo;
  tInfo.imbue(std::locale(""));
  for (size_t i = 0; i < m_tResourceLimits.size(); ++i)
  {
    if (-1 == getrlimit(std::get<RESOURCE_DESCRIPTOR>(m_tResourceLimits[i]), &tLimits))
    {
      switch (errno)
      {
        case EFAULT:
          tInfo << "ERROR: The pointer [" << &tLimits
                << "] is outside the accessible address space.";
          break;

        case EINVAL:
          tInfo << "ERROR: The specified resource ["
                << std::get<RESOURCE_DESCRIPTOR>(m_tResourceLimits[i]) << "] is invalid.";
          break;

        default:
          tInfo << "ERROR: Unknown getrlimit() error.";
          break;
      };
    }
    else
    {
      storeUlimitValues(tInfo, tLimits, std::get<RESOURCE_DESCRIPTOR>(m_tResourceLimits[i]));
    }
    std::get<RESOURCE_RESULT>(m_tResourceLimits[i]) = tInfo.str();
    tInfo.str("");
  }
  tInfo << SEM_VALUE_MAX;
  m_tResourceLimits.emplace_back(SEM_VALUE_MAX, "MAX SEMAPHORE VALUE", tInfo.str());
}

void ResourceLimits::storeUlimitValues(
  std::stringstream&   tInfo,
  const struct rlimit& tLimits,
  const int            nRLimit)
{
  constexpr static unsigned long nMAX = std::numeric_limits<unsigned long>::max();

  tInfo << "Soft Limit = [";
  if (nMAX == tLimits.rlim_cur)
  {
    tInfo << "unlimited";
  }
  else
  {
    tInfo << tLimits.rlim_cur;
  }

  tInfo << "] Hard Limit = [";
  if (nMAX == tLimits.rlim_max)
  {
    tInfo << "unlimited";
  }
  else
  {
    tInfo << tLimits.rlim_max;
  }
  tInfo << "] " << ResourceLimits::getRLimitUnits(nRLimit);
}

void ResourceLimits::populateConfigurationParams()
{
  m_tConfigurationParams.emplace_back(_SC_2_C_DEV, "POSIX.2 C LANGUAGE DEVELOPMENT SUPPORTED", "");
  m_tConfigurationParams.emplace_back(_SC_2_FORT_DEV, "POSIX.2 FORTRAN DEVELOPMENT SUPPORTED", "");
  m_tConfigurationParams.emplace_back(
    _SC_2_FORT_RUN,
    "POSIX.2 FORTRAN RUN-TIME UTILITIES SUPPORTED",
    "");
  m_tConfigurationParams.emplace_back(
    _SC_2_LOCALEDEF,
    "POSIX.2 LOCALE CREATION SUPPORTED VIA localedef()",
    "");
  m_tConfigurationParams.emplace_back(
    _SC_2_SW_DEV,
    "POSIX.2 SOFTWARE DEVELOPMENT UTILITIES OPTION SUPPORTED",
    "");
  m_tConfigurationParams.emplace_back(_SC_2_VERSION, "POSIX.2 STANDARD", "");
  m_tConfigurationParams.emplace_back(_SC_ARG_MAX, "MAX exec() ARGUMENT LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_AVPHYS_PAGES, "CURRENTLY AVAILABLE PHYSICAL MEMORY", "");
  m_tConfigurationParams.emplace_back(_SC_BC_BASE_MAX, "MAX bc obase", "");
  m_tConfigurationParams.emplace_back(_SC_BC_DIM_MAX, "MAX bc ARRAY LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_BC_SCALE_MAX, "MAX bc SCALE", "");
  m_tConfigurationParams.emplace_back(_SC_BC_STRING_MAX, "MAX bc STRING LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_CHILD_MAX, "MAX CHILD PROCESSES", "");
  m_tConfigurationParams.emplace_back(_SC_CLK_TCK, "CLOCK TICKS/SECOND", "");
  m_tConfigurationParams.emplace_back(
    _SC_COLL_WEIGHTS_MAX,
    "MAX NUMBER OF WEIGHTS FOR LC_COLLATE ORDER KEYWORD ENTRY",
    "");
  m_tConfigurationParams.emplace_back(_SC_EXPR_NEST_MAX, "MAX NESTED expr() EXPRESSIONS", "");
  m_tConfigurationParams.emplace_back(_SC_HOST_NAME_MAX, "MAX HOST NAME LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_LINE_MAX, "MAX LENGTH OF UTILITY INPUT LINE", "");
  m_tConfigurationParams.emplace_back(_SC_LOGIN_NAME_MAX, "MAX LOGIN NAME LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_NPROCESSORS_CONF, "NUMBER OF PROCESSORS CONFIGURED", "");
  m_tConfigurationParams.emplace_back(_SC_NPROCESSORS_ONLN, "NUMBER OF PROCESSORS ONLINE", "");
  m_tConfigurationParams.emplace_back(_SC_OPEN_MAX, "MAX OPEN FILES", "");
  m_tConfigurationParams.emplace_back(_SC_PAGESIZE, "PAGE SIZE", "");
  m_tConfigurationParams.emplace_back(_SC_PHYS_PAGES, "PHYSICAL MEMORY", "");
  m_tConfigurationParams.emplace_back(_SC_RE_DUP_MAX, "MAX DUCPLICATE BRE's", "");
  m_tConfigurationParams.emplace_back(_SC_SEM_NSEMS_MAX, "MAX SEMAPHORES/PROCESS", "");
  m_tConfigurationParams.emplace_back(_SC_STREAM_MAX, "MAX OPEN STREAMS", "");
  m_tConfigurationParams.emplace_back(_SC_SYMLOOP_MAX, "MAX SYMBOLIC LINKS IN PATH", "");
  m_tConfigurationParams.emplace_back(_SC_TTY_NAME_MAX, "MAX TERMINAL DEVICE NAME LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_TZNAME_MAX, "MAX TIME ZONE NAME LENGTH", "");
  m_tConfigurationParams.emplace_back(_SC_VERSION, "POSIX.1 STANDARD", "");
}

void ResourceLimits::getConfigurationParams()
{
  std::stringstream tInfo;
  tInfo.imbue(std::locale(""));

  for (size_t i = 0; i < m_tConfigurationParams.size(); ++i)
  {
    ScopedErrno tErrno;
    long nConfigParamValue = sysconf(std::get<RESOURCE_DESCRIPTOR>(m_tConfigurationParams[i]));
    if ((-1 == nConfigParamValue) && (EINVAL == errno))
    {
      tInfo << "ERROR: The specified configuration parameter ["
            << std::get<RESOURCE_DESCRIPTOR>(m_tConfigurationParams[i]) << "] is invalid.";
    }
    else if ((-1 == nConfigParamValue) && (0 != errno))
    {
      tInfo << "ERROR: Unknown sysconf() error.";
    }
    else
    {
      switch (std::get<ResourceLimits::RESOURCE_DESCRIPTOR>(m_tConfigurationParams[i]))
      {
        case _SC_ARG_MAX:
        case _SC_AVPHYS_PAGES:
        case _SC_BC_BASE_MAX:
        case _SC_BC_DIM_MAX:
        case _SC_BC_SCALE_MAX:
        case _SC_BC_STRING_MAX:
        case _SC_CHILD_MAX:
        case _SC_CLK_TCK:
        case _SC_COLL_WEIGHTS_MAX:
        case _SC_EXPR_NEST_MAX:
        case _SC_HOST_NAME_MAX:
        case _SC_LINE_MAX:
        case _SC_LOGIN_NAME_MAX:
        case _SC_NPROCESSORS_CONF:
        case _SC_NPROCESSORS_ONLN:
        case _SC_OPEN_MAX:
        case _SC_PAGESIZE:
        case _SC_PHYS_PAGES:
        case _SC_RE_DUP_MAX:
        case _SC_SEM_NSEMS_MAX:
        case _SC_STREAM_MAX:
        case _SC_SYMLOOP_MAX:
        case _SC_TTY_NAME_MAX:
        case _SC_TZNAME_MAX:
          if (-1 == nConfigParamValue)
          {
            tInfo << "unlimited";
          }
          else
          {
            tInfo << nConfigParamValue;
          }
          tInfo << ' '
                << ResourceLimits::getConfigParamUnits(
                     std::get<ResourceLimits::RESOURCE_DESCRIPTOR>(m_tConfigurationParams[i]));
          break;

        case _SC_2_C_DEV:
        case _SC_2_FORT_DEV:
        case _SC_2_FORT_RUN:
        case _SC_2_LOCALEDEF:
        case _SC_2_SW_DEV:
          tInfo << ((nConfigParamValue > 0) ? "true" : "false");
          break;

        case _SC_VERSION:
        case _SC_2_VERSION:
          tInfo << ResourceLimits::getMonthAndYear(nConfigParamValue);
          break;

        default:
          break;
      };
    }
    std::get<RESOURCE_RESULT>(m_tConfigurationParams[i]) = tInfo.str();
    tInfo.str("");
  }
}

void ResourceLimits::setFieldWidths()
{
  size_t nDescriptionWidth = 0;
  size_t nResultWidth      = 0;
  for (size_t i = 0; i < m_tConcatenatedResources.size(); ++i)
  {
    nDescriptionWidth = std::max(
      nDescriptionWidth,
      std::get<ResourceLimits::RESOURCE_DESCRIPTION>(m_tConcatenatedResources[i]).size());
    nResultWidth = std::max(
      nResultWidth,
      std::get<ResourceLimits::RESOURCE_RESULT>(m_tConcatenatedResources[i]).size());
  }

  for (size_t i = 0; i < m_tSelfInfo.size(); ++i)
  {
    nDescriptionWidth = std::max(nDescriptionWidth, m_tSelfInfo[i].first.size());
    nDescriptionWidth = std::max(nDescriptionWidth, m_tChildrenInfo[i].first.size());
    nDescriptionWidth = std::max(nDescriptionWidth, m_tThreadInfo[i].first.size());

    nResultWidth = std::max(nResultWidth, m_tThreadInfo[i].second.size());
    nResultWidth = std::max(nResultWidth, m_tChildrenInfo[i].second.size());
    nResultWidth = std::max(nResultWidth, m_tThreadInfo[i].second.size());
  }

  m_nDescriptionWidth = static_cast<int>(nDescriptionWidth);
  ++m_nDescriptionWidth;
  m_nResultWidth = static_cast<int>(nResultWidth);
}

std::ostream& operator<<(std::ostream& tStream, const ResourceLimits& rhs)
{
  for (size_t i = 0; i < rhs.m_tConcatenatedResources.size(); ++i)
  {
    tStream << std::setw(rhs.m_nDescriptionWidth) << std::left
            << std::get<ResourceLimits::RESOURCE_DESCRIPTION>(rhs.m_tConcatenatedResources[i])
            << " : " << std::setw(rhs.m_nResultWidth)
            << std::get<ResourceLimits::RESOURCE_RESULT>(rhs.m_tConcatenatedResources[i]) << '\n';
  }
  return (tStream);
}

std::ostream& ResourceLimits::showInts(std::ostream& tStream)
{
  static std::map<std::string, int> m_DescriptorValues;
  if (true == m_DescriptorValues.empty())
  {
    m_DescriptorValues.insert(std::make_pair("RLIMIT_AS", RLIMIT_AS));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_CORE", RLIMIT_CORE));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_CPU", RLIMIT_CPU));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_DATA", RLIMIT_DATA));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_FSIZE", RLIMIT_FSIZE));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_LOCKS", RLIMIT_LOCKS));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_MEMLOCK", RLIMIT_MEMLOCK));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_MSGQUEUE", RLIMIT_MSGQUEUE));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_NICE", RLIMIT_NICE));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_NOFILE", RLIMIT_NOFILE));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_NPROC", RLIMIT_NPROC));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_RSS", RLIMIT_RSS));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_RTPRIO", RLIMIT_RTPRIO));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_RTTIME", RLIMIT_RTTIME));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_SIGPENDING", RLIMIT_SIGPENDING));
    m_DescriptorValues.insert(std::make_pair("RLIMIT_STACK", RLIMIT_STACK));
    m_DescriptorValues.insert(std::make_pair("_SC_ARG_MAX", _SC_ARG_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_CHILD_MAX", _SC_CHILD_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_HOST_NAME_MAX", _SC_HOST_NAME_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_LOGIN_NAME_MAX", _SC_LOGIN_NAME_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_CLK_TCK", _SC_CLK_TCK));
    m_DescriptorValues.insert(std::make_pair("_SC_OPEN_MAX", _SC_OPEN_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_PAGESIZE", _SC_PAGESIZE));
    m_DescriptorValues.insert(std::make_pair("_SC_RE_DUP_MAX", _SC_RE_DUP_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_STREAM_MAX", _SC_STREAM_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_SYMLOOP_MAX", _SC_SYMLOOP_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_TTY_NAME_MAX", _SC_TTY_NAME_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_TZNAME_MAX", _SC_TZNAME_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_VERSION", _SC_VERSION));
    m_DescriptorValues.insert(std::make_pair("_SC_BC_BASE_MAX", _SC_BC_BASE_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_BC_DIM_MAX", _SC_BC_DIM_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_BC_SCALE_MAX", _SC_BC_SCALE_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_BC_STRING_MAX", _SC_BC_STRING_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_COLL_WEIGHTS_MAX", _SC_COLL_WEIGHTS_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_EXPR_NEST_MAX", _SC_EXPR_NEST_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_LINE_MAX", _SC_LINE_MAX));
    m_DescriptorValues.insert(std::make_pair("_SC_2_VERSION", _SC_2_VERSION));
    m_DescriptorValues.insert(std::make_pair("_SC_2_C_DEV", _SC_2_C_DEV));
    m_DescriptorValues.insert(std::make_pair("_SC_2_FORT_DEV", _SC_2_FORT_DEV));
    m_DescriptorValues.insert(std::make_pair("_SC_2_FORT_RUN", _SC_2_FORT_RUN));
    m_DescriptorValues.insert(std::make_pair("_SC_2_LOCALEDEF", _SC_2_LOCALEDEF));
    m_DescriptorValues.insert(std::make_pair("_SC_2_SW_DEV", _SC_2_SW_DEV));
    m_DescriptorValues.insert(std::make_pair("_SC_PHYS_PAGES", _SC_PHYS_PAGES));
    m_DescriptorValues.insert(std::make_pair("_SC_AVPHYS_PAGES", _SC_AVPHYS_PAGES));
    m_DescriptorValues.insert(std::make_pair("_SC_NPROCESSORS_CONF", _SC_NPROCESSORS_CONF));
    m_DescriptorValues.insert(std::make_pair("_SC_NPROCESSORS_ONLN", _SC_NPROCESSORS_ONLN));
  }

  auto NameWidthComparator =
    [](size_t& nNameWidth, const std::pair<std::string, int>& tPair) -> size_t
  { return (std::max(nNameWidth, tPair.first.size())); };
  size_t nNameWidth =
    std::accumulate(m_DescriptorValues.begin(), m_DescriptorValues.end(), 0UL, NameWidthComparator);

  for (const auto& i : m_DescriptorValues)
  {
    tStream << "[" << std::setw(static_cast<int>(nNameWidth)) << std::left << i.first << "] = ["
            << i.second << "]\n";
  }

  return (tStream);
}

void ResourceLimits::concatenate()
{
  m_tConcatenatedResources.assign(m_tResourceLimits.begin(), m_tResourceLimits.end());
  std::copy(
    m_tConfigurationParams.begin(),
    m_tConfigurationParams.end(),
    std::back_inserter(m_tConcatenatedResources));
  //  std::sort(m_tConcatenatedResources.begin(),
  //  m_tConcatenatedResources.end(), ResourceLimits::lessThan);
}

bool ResourceLimits::lessThan(const RESOURCE& lhs, const RESOURCE& rhs)
{
  return (
    std::get<ResourceLimits::RESOURCE_DESCRIPTOR>(lhs) <
    std::get<ResourceLimits::RESOURCE_DESCRIPTOR>(rhs));
}

std::string ResourceLimits::getMonthAndYear(const long nYYYYMM)
{
  static struct tm tTime;
  std::memset(&tTime, 0, sizeof(struct tm));
  tTime.tm_year = static_cast<int>(nYYYYMM / 100) - 1900;
  tTime.tm_mon  = static_cast<int>(nYYYYMM - ((tTime.tm_year + 1900) * 100) - 1);

  static const size_t nBufferSize                  = 16;
  static char         sDateBuffer[nBufferSize + 1] = {0};
  std::strftime(sDateBuffer, nBufferSize, "%B, %Y", &tTime);

  return (sDateBuffer);
}

std::string ResourceLimits::getRLimitUnits(const int nRLimit)
{
  switch (nRLimit)
  {
    case RLIMIT_AS:
    case RLIMIT_CORE:
    case RLIMIT_DATA:
    case RLIMIT_FSIZE:
    case RLIMIT_MEMLOCK:
    case RLIMIT_MSGQUEUE:
    case RLIMIT_STACK:
      return ("bytes");
      break;

    case RLIMIT_CPU:
      return ("seconds");
      break;

    case RLIMIT_RSS:
      return ("pages");
      break;

    case RLIMIT_RTTIME:
      return ("µsec");
      break;

    case RLIMIT_LOCKS:
    case RLIMIT_NICE:
    case RLIMIT_NOFILE:
    case RLIMIT_NPROC:
    case RLIMIT_RTPRIO:
    case RLIMIT_SIGPENDING:
    default:
      return ("");
      break;
  };
}

std::string ResourceLimits::getConfigParamUnits(const int nConfigParam)
{
  switch (nConfigParam)
  {
    case _SC_HOST_NAME_MAX:
    case _SC_PAGESIZE:
    case _SC_TZNAME_MAX:
      return ("bytes");
      break;

    case _SC_PHYS_PAGES:
    case _SC_AVPHYS_PAGES:
      return ("pages");
      break;

    case _SC_ARG_MAX:
    case _SC_CHILD_MAX:
    case _SC_LOGIN_NAME_MAX:
    case _SC_CLK_TCK:
    case _SC_OPEN_MAX:
    case _SC_RE_DUP_MAX:
    case _SC_STREAM_MAX:
    case _SC_SYMLOOP_MAX:
    case _SC_TTY_NAME_MAX:
    case _SC_VERSION:
    case _SC_BC_BASE_MAX:
    case _SC_BC_DIM_MAX:
    case _SC_BC_SCALE_MAX:
    case _SC_BC_STRING_MAX:
    case _SC_COLL_WEIGHTS_MAX:
    case _SC_EXPR_NEST_MAX:
    case _SC_LINE_MAX:
    case _SC_2_VERSION:
    case _SC_2_C_DEV:
    case _SC_2_FORT_DEV:
    case _SC_2_FORT_RUN:
    case _SC_2_LOCALEDEF:
    case _SC_2_SW_DEV:
    case _SC_NPROCESSORS_CONF:
    case _SC_NPROCESSORS_ONLN:
    default:
      return ("");
      break;
  };
}

void ResourceLimits::setRusageInfo()
{
  m_nRusageSelfStat = getrusage(RUSAGE_SELF, &m_tRusageSelf);
  ResourceLimits::getRUsageInfo(m_tSelfInfo, m_tRusageSelf, m_nRusageSelfStat);

  m_nRusageChildrenStat = getrusage(RUSAGE_CHILDREN, &m_tRusageChildren);
  ResourceLimits::getRUsageInfo(m_tChildrenInfo, m_tRusageChildren, m_nRusageChildrenStat);

  m_nRusageThreadStat = getrusage(RUSAGE_THREAD, &m_tRusageThread);
  ResourceLimits::getRUsageInfo(m_tThreadInfo, m_tRusageThread, m_nRusageThreadStat);
}

void ResourceLimits::getRUsageInfo(
  std::vector<std::pair<std::string, std::string>>& tInfo,
  const struct rusage&                              tUsage,
  const int                                         nStat)
{
  if (0 == nStat)
  {
    std::stringstream tValue;
    tValue.imbue(std::locale(""));

    tValue << ResourceLimits::convertToSeconds(tUsage.ru_utime) << " seconds";
    tInfo.emplace_back("User CPU Time", tValue.str());
    tValue.str("");

    tValue << ResourceLimits::convertToSeconds(tUsage.ru_stime) << " seconds";
    tInfo.emplace_back("System CPU Time", tValue.str());
    tValue.str("");

    tValue << tUsage.ru_maxrss << " kB";
    tInfo.emplace_back("Max Resident Set Size", tValue.str());
    tValue.str("");

    // tUsage.ru_ixrss is unused.
    // tUsage.ru_idrss is unused.
    // tUsage.ru_isrss is unused.

    tValue << tUsage.ru_minflt;
    tInfo.emplace_back("Soft Page Faults", tValue.str());
    tValue.str("");

    tValue << tUsage.ru_majflt;
    tInfo.emplace_back("Hard Page Faults", tValue.str());
    tValue.str("");

    // tUsage.ru_nswap is unused.

    tValue << tUsage.ru_inblock;
    tInfo.emplace_back("Block Input Operations via File System", tValue.str());
    tValue.str("");

    tValue << tUsage.ru_oublock;
    tInfo.emplace_back("Block Output Operations via File System", tValue.str());
    tValue.str("");

    // tUsage.ru_msgsnd is unused.
    // tUsage.ru_msgrcv is unused.
    // tUsage.ru_nsignals is unused.

    tValue << tUsage.ru_nvcsw;
    tInfo.emplace_back("Voluntary Context Switches", tValue.str());
    tValue.str("");

    tValue << tUsage.ru_nivcsw;
    tInfo.emplace_back("Involuntary Context Switches", tValue.str());
    tValue.str("");
  }
  else
  {
    tInfo.emplace_back("User CPU Time", "UNKNOWN");
    tInfo.emplace_back("System CPU Time", "UNKNOWN");
    tInfo.emplace_back("Max Resident Set Size", "UNKNOWN");

    // tUsage.ru_ixrss is unused.
    // tUsage.ru_idrss is unused.
    // tUsage.ru_isrss is unused.

    tInfo.emplace_back("Soft Page Faults", "UNKNOWN");
    tInfo.emplace_back("Hard Page Faults", "UNKNOWN");

    // tUsage.ru_nswap is unused.

    tInfo.emplace_back("Block Input Operations via File System", "UNKNOWN");
    tInfo.emplace_back("Block Output Operations via File System", "UNKNOWN");

    // tUsage.ru_msgsnd is unused.
    // tUsage.ru_msgrcv is unused.
    // tUsage.ru_nsignals is unused.

    tInfo.emplace_back("Voluntary Context Switches", "UNKNOWN");
    tInfo.emplace_back("Involuntary Context Switches", "UNKNOWN");
  }
}

double ResourceLimits::convertToSeconds(const struct timeval& tTime)
{
  double fSeconds =
    static_cast<double>(tTime.tv_sec) + (static_cast<double>(tTime.tv_usec) / 1.0e6);
  return (fSeconds);
}

void ResourceLimits::getPriority()
{
  m_nProcessPriority = getpriority(PRIO_PROCESS, 0);
}

