
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

GIT_IGNORE=$(PROJECT_ROOT)/.gitignore

include $(PROJECT_ROOT)/vars_common.mk
include $(PROJECT_ROOT)/boost.mk
include $(PROJECT_ROOT)/graph_viz.mk
include $(PROJECT_ROOT)/doxygen.mk
include $(PROJECT_ROOT)/git.mk
include $(PROJECT_ROOT)/opencl.mk

DATESTAMP:=$(shell date +%m%d%Y_%H%M)
PROJECT_DIRS:=$(subst /,,$(subst ./,,$(dir $(sort $(shell find . -maxdepth 2 -mindepth 2 -name Makefile)))))
NUM_PROJECTS=$(words $(PROJECT_DIRS))
PROJECT:=$(notdir $(CURDIR))

################################################################################
# $(1) - directory plus possible variable(s) defined on the make command line  #
# $(2) - target                                                                #
################################################################################
define make_target
if [[ -e $(1)/Makefile ]];                                   \
then                                                         \
  echo '========== $(firstword $(2)) BEGIN $(1) =========='; \
  $(MAKE) $(IGNORE_ERROR_KEEP_GOING) -C $(1) $(2);           \
  echo '========== $(firstword $(2))  END  $(1) =========='; \
fi;
endef

################################################################################
# Creates makefile recipes so the specified target can be processed in         #
# parallel. For a particular target, the recipes will resemble:                #
#                                                                              #
#   gitignore_PROJECT_DIRS=$(addsuffix .gitignore,$(PROJECT_DIRS))             #
#   .PHONY: gitignore $(gitignore_PROJECT_DIRS)                                #
#   gitignore: $(gitignore_PROJECT_DIRS)                                       #
#   	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))                       #
#                                                                              #
#   $(gitignore_PROJECT_DIRS):                                                 #
#   	@$(call target_info,$(@:.gitignore= gitignore),$(<),$(?),$(^),$(|),$(*)) #
#   	+@$(MAKE) $(IGNORE_ERROR_KEEP_GOING) -C $(@:.gitignore=) gitignore       #
#                                                                              #
# $(1) - the specified target                                                  #
################################################################################
define make_recipes
$(1)_PROJECT_DIRS=$$(addsuffix .$(1),$$(PROJECT_DIRS))
.PHONY: $(1) $$($(1)_PROJECT_DIRS)
$(1): $$($(1)_PROJECT_DIRS)
	@$$(call target_info,$$(@),$$(<),$$(?),$$(^),$$(|),$$(*))

$$($(1)_PROJECT_DIRS):
	@$$(call target_info,$$(@:.$(1)= $(1)),$$(<),$$(?),$$(^),$$(|),$$(*))
	+@$(MAKE) $(IGNORE_ERROR_KEEP_GOING) -C $$(@:.$(1)=) $(1)
endef

.DEFAULT_GOAL:=all
.PHONY: $(PROJECT_DIRS)
all: $(PROJECT_DIRS)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))

$(PROJECT_DIRS):
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	+@$(MAKE) $(IGNORE_ERROR_KEEP_GOING) -C $(@) all

logs:
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	+@$(foreach sub_project,$(PROJECT_DIRS),$(call make_target,$(sub_project),log NO_EDITOR=1))

.PHONY: junk_files_clean
junk_files_clean:
	@rm -f $(JUNK_FILES)

CLEAN_PROJECT_DIRS=$(addsuffix .clean,$(PROJECT_DIRS))
.PHONY: clean $(CLEAN_PROJECT_DIRS)
clean:: junk_files_clean $(CLEAN_PROJECT_DIRS)
	@clear

$(CLEAN_PROJECT_DIRS):
	@$(call target_info,$(@:.clean= clean),$(<),$(?),$(^),$(|),$(*))
	@$(MAKE) $(IGNORE_ERROR_KEEP_GOING) -C $(@:.clean=) clean

.PHONY: fast_clean
fast_clean:
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@find $(CURDIR) -type f \( -name "*\.o" -o -name "lib*\.a" -o -name "lib*\.so" \) -exec rm {} \;
	+@$(foreach sub_project, $(PROJECT_DIRS), rm -f $(sub_project)/$(sub_project);)
	@clear

# qq need to fix (overriding rule errors)
##TARGETS=gitignore
##$(foreach target,$(TARGETS),$(eval $(call make_recipes,$(target))))

.PHONY: zip
ifneq ($(ZIP),) # {
BINARY_EXCLUDES= '*.o' '*.a' '*.so'
DOXYGEN_EXCLUDES= '*dOxygen*'
ECLIPSE_EXCLUDES= \*.cproject \*.project '*.settings/*'
GIT_EXCLUDES= '*.git/*'
MISC_EXCLUDES= '*.swn' '*.swo' '*.swp' '*.nfs*'
SCAN_BUILD_EXCLUDES=\*scan_build_*_checkers.log '*scan_build_*_results*'
ZIP_EXCLUDES:=$(foreach dir, $(PROJECT_DIRS), \*$(dir)/$(dir))
ZIP_EXCLUDES += $(BINARY_EXCLUDES) $(DOXYGEN_EXCLUDES) $(GIT_EXCLUDES) $(ECLIPSE_EXCLUDES) $(MISC_EXCLUDES) $(SCAN_BUILD_EXCLUDES) '*.zip'
ZIP_EXCLUDES:=$(addprefix -x ,$(ZIP_EXCLUDES))
ifeq ($(GIT_ROOT),UNKNOWN) # {
  ZIP_FILE=$(CURDIR)/$(PROJECT)_$(DATESTAMP).zip
	ZIP_ROOT=$(CURDIR)
else # } {
  ZIP_FILE=$(GIT_ROOT)/$(PROJECT)_$(DATESTAMP).zip
	ZIP_ROOT=$(GIT_ROOT)
endif # }
zip:
	@rm -f $(ZIP_FILE)
	@zip $(filter-out -m --move,$(ZIP_OPTS)) $(ZIP_FILE) $(ZIP_ROOT) $(ZIP_EXCLUDES)
else # } {
zip:
	@$(error $(ERROR_LABEL) The zip command is not available)
endif # }

################################################################################
# BEGIN readme markdown section.                                               #
################################################################################

.PHONY: readme_md
readme_md: GIT_PROJECT=$(notdir $(CURDIR))
readme_md: CLONE_BRANCH=main
readme_md:
	@echo '# README $(GIT_PROJECT)'
	@echo ''
	@echo 'OpenCL applications.'
	@echo ''
	@echo '## Getting Started'
	@echo 'To clone the git repository:'
	@echo '```'
ifeq ($(GIT_SUBMODULES),NONE) # {
	@echo 'git clone --branch $(CLONE_BRANCH) $(GIT_REMOTE_REPO)'
	@echo '```'
else # } {
	@echo 'git clone --recursive --branch $(CLONE_BRANCH) $(GIT_REMOTE_REPO)'
	@echo '```'
	@echo '### Submodule(s) List'
	@echo '| Submodule |'
	@echo '| --------- |'
	@$(eval GIT_SUBMODULE_URLS:=$(shell find $(GIT_ROOT) -type f -name "\.gitmodules" -exec grep url {} \; | sort -u | sed 's/^.*url = //'))
	@$(foreach submodule_url, $(GIT_SUBMODULE_URLS), $(call create_submodule_link,$(submodule_url)))
endif # }
	@echo '### Prerequisites'
	@echo 'A functioning OpenCL installation. Additionally, the following software is required:'
	@echo ''
	@{ echo '| Prerequisite                         | Link                                        |';     \
     echo '| ------------------------------------ | ------------------------------------------- |';     \
     echo '| Boost version $(BOOST_VERSION)       | $(call create_url_link,$(BOOST_DOWNLOAD))   |';     \
     echo '| GNU C++ compiler                     | $(call create_url_link,$(GCC_WEB_SITE))     |'; } | \
     column -t -s"|" -o "|"
	@echo ''
	@echo 'To get full doxygen functionality, please install the following:'
	@echo ''
	@{ echo '| Application  | Link                                            |';     \
     echo '| ------------ | ----------------------------------------------- |';     \
     echo '| PlantUML     | $(call create_url_link,$(PLANT_UML_WEB_SITE))   |';     \
     echo '| dvips        | $(call create_url_link,$(DVIPS_WEB_SITE))       |';     \
     echo '| graphviz/dot | $(call create_url_link,$(DOT_WEB_SITE))         |';     \
     echo '| gs           | $(call create_url_link,$(GHOSTSCRIPT_WEB_SITE)) |';     \
     echo '| latex        | $(call create_url_link,$(LATEX_WEB_SITE))       |'; } | \
     column -t -s"|" -o "|"
	@echo ''
	@{ echo '| Environment Attribute                         | Value                                         |'; \
     echo '| --------------------------------------------- | ----------------------------------- |'; \
     echo '| $(SHELL) version                              | $(SHELL_VERSION)                    |'; \
     echo '| Boost version                                 | $(BOOST_VERSION)                    |'; \
     echo "| $(MAKE) version                               | $$($(MAKE) --version | head -1)     |"; \
     echo '| Host Platform Operating System                | $(BUILD_OS)                         |'; \
     echo '| Khronos Unified OpenCL C API Headers commit   | $(KHRONOS_UNIFIED_C_HEADERS_HASH)   |'; \
     echo '| Khronos Unified OpenCL C++ API Headers commit | $(KHRONOS_UNIFIED_CPP_HEADERS_HASH) |'; \
     echo '| PlantUML version                              | $(PLANT_UML_VERSION)                |'; \
     echo '| doxygen version                               | $(DOXYGEN_VERSION)                  |'; \
     if [[ -n "$(BROWSER)" ]]                                                                      ; \
     then                                                                                            \
       echo '| $(BROWSER) version                          | $(BROWSER_VERSION)                  |'; \
     fi                                                                                            ; \
     echo '| $(CXX) version                                | $(CXX_VERSION)                      |'; \
     echo '| git version                                   | $(GIT_VERSION)                      |'; } | \
     gawk 'NR <= 2; NR > 2 {print $0 | "sort -n"}' | column -t -s"|" -o "|"
	@echo ''
	@echo 'Application(s):'
	@echo ''
# qq using a README.md link may only work on github!!
	@{ echo '| Application     | Readme                           | Description                                   |';     \
     echo '| --------------- | -------------------------------- | --------------------------------------------- |';     \
     echo '| clinfo          | [README.md](/clinfo/README.md)   | gathers and displays/saves OpenCL information |'; } | \
     column -t -s"|" -o "|"
	@echo ''
	@echo '### Author'
	@echo ' **K. Banerjee**'
	@echo '### License'
	@echo 'Copyright © 2018-2023 K. Banerjee'
	@echo 'This project is licensed under the GPL, version 3.0. See:<br>'
	@echo '$(call create_url_link,$(GPL3_HTML))'
	@echo '<br>'
	@echo '$(call create_url_link,$(GPL3_TEXT))'

README_MD ?= README.md
.PHONY: readme
readme:
	+@$(MAKE_NO_DIR) readme_md | sed "s@${HOME}@\$${HOME}@g" >| $(README_MD)

################################################################################
# Need to modify html links for bitbucket.org markdown files.                  #
################################################################################
ifneq ($(findstring bitbucket.org,$(shell git config --get remote.origin.url)),) # {
	@sed -i 's@\(^.*\)\(<a href.*>\)\(http.*\)\(</a>\)\(.*$$\)@\1[\3](\3)\5@' $(README_MD)
	@sed -i 's/<br>$$/  /' $(README_MD)
################################################################################
# bitbucket markdown automatically creates an HTML link for hex strings that   #
# are between 7 and 40 characters long. To prevent this for the Khronos OpenCL #
# Unified headers commit hash, a "[]()" is inserted after every 6 characters.  #
################################################################################
	@sed -i 's@[0-9,a-f]\{6\}@&[]()@g' $(README_MD)
endif # }

################################################################################
# END readme markdown section.                                                 #
################################################################################

MAKEFILE_HELP_TARGET=$(notdir $(CURDIR))_mk_help
.PHONY: $(MAKEFILE_HELP_TARGET)
$(MAKEFILE_HELP_TARGET):
	@echo '$(MAKE) → recursively builds executables in subdirectories'
	@echo '$(MAKE) clean → recursively cleans subdirectories'
	@echo '$(MAKE) fast_clean → recursively deletes executables, object files, and libraries in subdirectories'
	@echo '$(MAKE) gitignore → recursively creates .gitignore files in subdirectories'
	@echo '$(MAKE) log → recursively builds executables, without parallelism, and saves $(MAKE) output to [$(LOG_FILE)]'
	@echo '$(MAKE) logs → recursively builds executables, saving output to log files, for each directory in [$(PROJECT_DIRS)]'
	@echo '$(MAKE) readme → creates the root level README.md markdown file'

show_help:: $(MAKEFILE_HELP_TARGET)

include $(PROJECT_ROOT)/trace.mk

