
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef FLAKE8_MK_INCLUDE_GUARD # {
FLAKE8_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

FLAKE8_PROJECT=$(notdir $(CURDIR))
FLAKE8=flake8
FLAKE8_OUTPUT=$(FLAKE8_PROJECT)_$(FLAKE8)
FLAKE8_URL=https://flake8.pycqa.org/en/latest

################################################################################
# NOTE: Include this makefile after including python.mk.                       #
################################################################################
ifeq ($(PYTHON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/python.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

FLAKE8_DASHBOARD=flake8-dashboard
FLAKE8_HTML=flake8-html
PYTHON_PACKAGES += flake8
$(eval $(call create_pip_show_target,flake8))
$(eval $(call create_pip_show_files_target,flake8))

EDITOR  ?= vim +1
BROWSER ?= firefox
IMAGE_PATH ?= $(PROJECT_ROOT)/images

FLAKE8_COMMAND:=$(shell command -v $(FLAKE8))
ifeq ($(FLAKE8_COMMAND),) # {
$(error $(ERROR_LABEL) The [$(FLAKE8)] command is not available)
endif # }
FLAKE8_VERSION=$(call get_stdout_version,$(FLAKE8),--version)

################################################################################
#  E111 → indentation is not a multiple of four                                #
#  E114 → indentation is not a multiple of four (comment)                      #
#  E115 → expected an indented block (comment)                                 #
#  E121 → continuation line under-indented for hanging indent                  #
#  E126 → continuation line over-indented for hanging indent                   #
#  E128 → continuation line under-indented for visual indent                   #
#  E203 → whitespace before ':'                                                #
#  E221 → multiple spaces before operator                                      #
#  E222 → multiple spaces after operator                                       #
#  E251 → unexpected spaces around keyword / parameter equals                  #
#  E266 → too many leading '#' for block comment                               #
#  E302 → expected 2 blank lines, found 0                                      #
#  E305 → expected 2 blank lines after class or function definition, found 1   #
#  E501 → line too long (80 > 79 characters)                                   #
#  E711 → comparison to None should be 'if cond is not None:'                  #
#  E712 → comparison to True should be 'if cond is True:' or 'if cond:'        #
#  W391 → blank line at end of file                                            #
#  W504 → line break after binary operator                                     #
################################################################################
FLAKE8_OUTPUT_FORMAT=pylint
FLAKE8_IGNORE=E114 E115 E121 E126 E128 E203 E221 E222 E251 E266 E302 E305 E501 E711 E712 W391 W504
FLAKE8_COMMON_OPTS=--exit-zero --filename=*.py --isolated --jobs=auto --max-complexity=30 --show-source --indent-size 2
ifneq ($(FLAKE8_IGNORE),) # {
FLAKE8_COMMON_OPTS += --ignore $(call commafy,$(sort $(FLAKE8_IGNORE)))
endif # }
FLAKE8_OPTS=$(FLAKE8_COMMON_OPTS) --benchmark --statistics --format=$(FLAKE8_OUTPUT_FORMAT)

################################################################################
# Checking to see if flake8-dashboard is available or not.                     #
################################################################################
ifneq ($(FLAKE8_DASHBOARD_VERSION),UNKNOWN) # {
FLAKE8_DASHBOARD_VERSION=$(call get_python_package_version,$(FLAKE8_DASHBOARD))
FLAKE8_DASHBOARD_OUTPUT_DIR=$(subst -,_,$(FLAKE8_DASHBOARD))
FLAKE8_DASHBOARD_OPTS=$(FLAKE8_COMMON_OPTS) --format=dashboard
DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
endif # }

################################################################################
# Checking to see if flake8-html is available or not.                          #
################################################################################
ifneq ($(FLAKE8_HTML_VERSION),UNKNOWN) # {
FLAKE8_HTML_VERSION=$(call get_python_package_version,$(FLAKE8_HTML))
FLAKE8_HTML_OUTPUT_DIR=$(subst -,_,$(FLAKE8_HTML))
FLAKE8_HTML_OPTS=$(FLAKE8_COMMON_OPTS) --format=html
endif # }

FLAKE8_WEB_SITE=https://www.flake8rules.com/rules/
PYDOCSTYLE_WEB_SITE_PREFIX=https://www.pydocstyle.org/en/6.1.1/search.html?q=
PYDOCSTYLE_WEB_SITE_SUFFIX=\&check_keywords=yes&area=default#
CURDIR_DOTS:=$(shell echo $(CURDIR)/ | sed 's@/@.@g;s@^\.@@')

PYTHON_SCRIPTS:=$(sort $(PYTHON_SCRIPTS))
$(call check_python_scripts_existence,$(PYTHON_SCRIPTS))

################################################################################
# Adds to the flake8 target for the input Python script so that flake8 can be  #
# run on all Python scripts at once.                                           #
#                                                                              #
# Example:                                                                     #
#   make flake8                                                                #
#                                                                              #
# Adds a "::" flake8 target for the script so that this target and another     #
# target can be run simultaneously.                                            #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make my_script.py                                                          #
#                                                                              #
# Adds a flake8 target, prefixed with 'f', so that flake8 can be run only for  #
# this script.                                                                 #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make my_script.py                                                          #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
.PHONY: flake8
FLAKE8_DASH_FAVICON=$(IMAGE_PATH)/flake.png
FLAKE8_FAVICON=$(IMAGE_PATH)/flake2.png
define add_flake8_targets
flake8::
	@$$(eval FLAKE8_STUB=$$(subst .py,_py,$$(notdir $(1))))
	@mkdir -p $(FLAKE8_OUTPUT)/$$(FLAKE8_STUB)
	@rm -rf $(FLAKE8_OUTPUT)/$$(FLAKE8_STUB)/*
	@$$(eval FLAKE8_LOG=$(FLAKE8_OUTPUT)/$$(FLAKE8_STUB)/$$(FLAKE8_STUB)_$(FLAKE8).log)
	@rm -f $$(FLAKE8_LOG)
	@$(FLAKE8) $(FLAKE8_OPTS) --output-file=$$(FLAKE8_LOG) $(1)
	@if [[ -s $$(FLAKE8_LOG) ]];                                                   \
   then                                                                          \
     grep -q "^$(1):[0-9]*: " $$(FLAKE8_LOG);                                    \
     if [[ 0 == $$$${?} ]];                                                      \
     then                                                                        \
       echo '$(EDITOR) $$(FLAKE8_LOG)';                                          \
     else                                                                        \
       echo '$(INFO_LABEL) No $(FLAKE8) issues found for Python script [$(1)].'; \
       rm -f $$(FLAKE8_LOG);                                                     \
       rmdir $(FLAKE8_OUTPUT)/$$(FLAKE8_STUB);                                   \
       rmdir --ignore-fail-on-non-empty $(FLAKE8_OUTPUT);                        \
     fi;                                                                         \
   else                                                                          \
     echo '$(INFO_LABEL) No $(FLAKE8) issues found for Python script [$(1)].';   \
     rm -f $$(FLAKE8_LOG);                                                       \
     rmdir $(FLAKE8_OUTPUT)/$$(FLAKE8_STUB);                                     \
     rmdir --ignore-fail-on-non-empty $(FLAKE8_OUTPUT);                          \
   fi
ifneq ($(FLAKE8_DASHBOARD_VERSION),UNKNOWN) # {
################################################################################
# If there were no flake8 issues found, then there is no point in using        #
# flake8-dashboard. Therefore, we first check to see if $$(FLAKE8_LOG) exists. #
################################################################################
	@if [[ -e $$(FLAKE8_LOG) ]];                                                                                                                                 \
   then                                                                                                                                                        \
     FLAKE8_DASHBOARD_RESULTS=$(FLAKE8_OUTPUT)/$$(FLAKE8_STUB)/$(FLAKE8_DASHBOARD_OUTPUT_DIR);                                                                 \
     rm -rf $$$${FLAKE8_DASHBOARD_RESULTS};                                                                                                                    \
     mkdir -p $$$${FLAKE8_DASHBOARD_RESULTS};                                                                                                                  \
     FLAKE8_DASHBOARD_TITLE="$$(notdir $(1)) $(DATE_TIME) $(FLAKE8_DASHBOARD) version = [$(FLAKE8_DASHBOARD_VERSION)]";                                        \
     $(FLAKE8) $(FLAKE8_DASHBOARD_OPTS) --outputdir=$$$${FLAKE8_DASHBOARD_RESULTS} --title="$$$${FLAKE8_DASHBOARD_TITLE}" $$$${FLAKE8_DASHBOARD_RESULTS} $(1); \
     FLAKE8_DASHBOARD_INDEX=$$$${FLAKE8_DASHBOARD_RESULTS}/index.html;                                                                                         \
     if [[ -s $$$${FLAKE8_DASHBOARD_INDEX} ]];                                                                                                                 \
     then                                                                                                                                                      \
       cp $(FLAKE8_DASH_FAVICON) $$$$(dirname $$$${FLAKE8_DASHBOARD_INDEX});                                                                                   \
       sed -i '\@<title>.*</title>@a\ \ \ \ \ \ <link rel="icon" type="image/x-icon" href="./$(notdir $(FLAKE8_DASH_FAVICON))">' $$$${FLAKE8_DASHBOARD_INDEX}; \
       echo "$(BROWSER) $$$${FLAKE8_DASHBOARD_INDEX}";                                                                                                         \
     else                                                                                                                                                      \
       echo '$(INFO_LABEL) No $(FLAKE8_DASHBOARD) issues found for Python script [$(1)].';                                                                     \
       rmdir --ignore-fail-on-non-empty $$$${FLAKE8_DASHBOARD_RESULTS};                                                                                        \
       rmdir --ignore-fail-on-non-empty $$(FLAKE8_DASHBOARD_OUTPUT_DIR);                                                                                       \
     fi;                                                                                                                                                       \
   fi
endif # }
ifneq ($(FLAKE8_HTML_VERSION),UNKNOWN) # {
################################################################################
# If there were no flake8 issues found, then there is no point in using        #
# flake8-html. Therefore, we first check to see if $$(FLAKE8_LOG) exists.      #
################################################################################
	@$$(eval FLAKE8_STUB_PREFIX:=$$(subst _py,,$$(FLAKE8_STUB)))
	@if [[ -e $$(FLAKE8_LOG) ]];                                                                                                                                                                             \
   then                                                                                                                                                                                                    \
     FLAKE8_HTML_RESULTS=$(FLAKE8_OUTPUT)/$$(FLAKE8_STUB)/$(FLAKE8_HTML_OUTPUT_DIR);                                                                                                                       \
     rm -rf $$$${FLAKE8_HTML_RESULTS};                                                                                                                                                                     \
     mkdir -p $$$${FLAKE8_HTML_RESULTS};                                                                                                                                                                   \
     $(FLAKE8) $(FLAKE8_HTML_OPTS) --htmldir=$$$${FLAKE8_HTML_RESULTS} $(1);                                                                                                                               \
     FLAKE8_HTML_INDEX=$$$${FLAKE8_HTML_RESULTS}/index.html;                                                                                                                                               \
     if [[ -s $$$${FLAKE8_HTML_INDEX} ]];                                                                                                                                                                  \
     then                                                                                                                                                                                                  \
       grep -q "<p>No flake8 errors found in [0-9]* files scanned.</p>" $$$${FLAKE8_HTML_INDEX};                                                                                                           \
       if [[ 0 == $$$${?} ]];                                                                                                                                                                              \
       then                                                                                                                                                                                                \
         echo '$(INFO_LABEL) No $(FLAKE8_HTML) issues found for Python script [$(1)].';                                                                                                                    \
         rm -rf $$$${FLAKE8_HTML_RESULTS};                                                                                                                                                                 \
         rmdir --ignore-fail-on-non-empty $(FLAKE8_HTML_OUTPUT_DIR);                                                                                                                                       \
       else                                                                                                                                                                                                \
         sed -i -e 's@\(^.*<strong>\)\([A-C,E-Z][0-9]\{3\}\)\(:\)\(</strong>.*$$$$\)@\1<a style="display: inline;" href="$(FLAKE8_WEB_SITE)\2.html" target="_blank">\2:</a>\4@'                            \
                -e 's@\(^.*<a data-code="[A-Z][0-9]\{3\}"\)\(>$$$$\)@\1 style="display: inline;" \2@'                                                                                                      \
         $$$${FLAKE8_HTML_RESULTS}/$(CURDIR_DOTS)$$(FLAKE8_STUB_PREFIX).report.html;                                                                                                                       \
         sed -i -e 's@\(^.*<strong>\)\(D[0-9]\{3\}\)\(:\)\(</strong>.*$$$$\)@\1<a style="display: inline;" href="$(PYDOCSTYLE_WEB_SITE_PREFIX)\2$(PYDOCSTYLE_WEB_SITE_SUFFIX)" target="_blank">\2:</a>\4@' \
                -e 's@\(^.*<a data-code="[A-Z][0-9]\{3\}"\)\(>$$$$\)@\1 style="display: inline;" \2@'                                                                                                      \
         $$$${FLAKE8_HTML_RESULTS}/$(CURDIR_DOTS)$$(FLAKE8_STUB_PREFIX).report.html;                                                                                                                       \
         cp $(FLAKE8_FAVICON) $$$$(dirname $$$${FLAKE8_HTML_INDEX});                                                                                                                                       \
         sed -i '\@<title>.*</title>@a\ \ \ \ \ \ <link rel="icon" type="image/x-icon" href="./$(notdir $(FLAKE8_FAVICON))">' $$$${FLAKE8_HTML_INDEX};                                                     \
         echo "$(BROWSER) $$$${FLAKE8_HTML_INDEX}";                                                                                                                                                        \
       fi;                                                                                                                                                                                                 \
     else                                                                                                                                                                                                  \
       echo '$(WARNING_LABEL) The [$(FLAKE8_HTML)] html index file [$$$${FLAKE8_HTML_INDEX}] was not found.';                                                                                              \
     fi;                                                                                                                                                                                                   \
   fi
endif # }
################################################################################
# Creating the "::" target.                                                    #
################################################################################
.PHONY: $(notdir $(1))
$(notdir $(1))::
	+@$(MAKE_NO_DIR) PYTHON_SCRIPTS=$(1) flake8
################################################################################
# Creating the single script target.                                           #
################################################################################
.PHONY: p$(notdir $(1))
f$(notdir $(1)):
	+@$(MAKE_NO_DIR) PYTHON_SCRIPTS=$(1) flake8
endef
$(foreach py,$(PYTHON_SCRIPTS),$(eval $(call add_flake8_targets,$(py))))

flake8_clean:
	@rm -rf $(FLAKE8_OUTPUT)

clean:: flake8_clean

platform_info::
	@echo '$(FLAKE8) version           = [$(FLAKE8_VERSION)]'
	@echo '$(FLAKE8_DASHBOARD) version = [$(FLAKE8_DASHBOARD_VERSION)]'
	@echo '$(FLAKE8_HTML) version      = [$(FLAKE8_HTML_VERSION)]'

################################################################################
# Prints help information of the double-colon and the "f" targets.             #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
define flake8_show_help
echo '$(MAKE) $(1) → double-colon target to run [$(FLAKE8)], and possibly other target(s), only on [$(1)]';
echo '$(MAKE) f$(1) → run [$(FLAKE8)] only on [$(1)]';
endef

PYTHON_SCRIPTS_COUNT=$(words $(PYTHON_SCRIPTS))
ifneq ($(PYTHON_SCRIPTS_COUNT),0) # {
PLURAL=$(if $(filter 1,$(PYTHON_SCRIPTS_COUNT)),,s)

flake8_mk_help::
	@echo '$(MAKE) flake8 → runs $(FLAKE8) on the Python script$(PLURAL) $(call setify,$(PYTHON_SCRIPTS)) and saves the output file$(PLURAL) under [$(FLAKE8_OUTPUT)]'
	@$(foreach py,$(notdir $(PYTHON_SCRIPTS)),$(call flake8_show_help,$(py)))

show_help:: flake8_mk_help

endif # }

endif # }

