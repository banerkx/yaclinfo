
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo_100.h"

#include "cl_command_queue_properties.h"
#include "cl_device_fp_config.h"

DeviceInfo_100::DeviceInfo_100(
  const cl_device_id   nDeviceID,
  const size_t         nDeviceNumber,
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nDeviceVersion) noexcept :
  DeviceInfo(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber, nDeviceVersion),
  m_tHalfPrecisionFloatingPointConfiguration(
    OPEN_cl_device_info::OPENCL_DEVICE_HALF_FP_CONFIG,
    "HALF PRECISION FLOATING POINT CONFIGURATION (OPTIONAL)",
    sizeof(cl_device_fp_config),
    cl_device_fp_config(0),
    OpenCLError(""),
    OpenCLToolTip("Optional half precision capability")),
  m_tMaxSimultaneousImageObjectsWrite(
    OPEN_cl_device_info_100::OPENCL_DEVICE_MAX_WRITE_IMAGE_ARGS,
    "MAX NUMBER SIMULTANEOUS IMAGE OBJECTS WRITE",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of simultaneous image objects that can be "
                  "written to by a kernel")),
  m_tMinDataTypeAlignSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
    "MIN DATA TYPE ALIGN SIZE",
    sizeof(cl_int),
    cl_int(0),
    OpenCLError(""),
    OpenCLToolTip("Smallest alignment which can be used for a data type")),
  m_tQueueProperties(
    OPEN_cl_device_info_100::OPENCL_DEVICE_QUEUE_PROPERTIES,
    "QUEUE PROPERTIES",
    sizeof(cl_command_queue_properties),
    cl_command_queue_properties(0),
    OpenCLError(""),
    OpenCLToolTip("Command-queue properties of the device."))
{
}

DeviceInfo_100::~DeviceInfo_100() noexcept
{
}

void DeviceInfo_100::getAllDeviceInfo() noexcept
{
  DeviceInfo::getAllDeviceInfo();

  getDeviceInfo(m_tHalfPrecisionFloatingPointConfiguration);
  getDeviceInfo(m_tMaxSimultaneousImageObjectsWrite);
  getDeviceInfo(m_tMinDataTypeAlignSize);
  getDeviceInfo(m_tQueueProperties);
}

void DeviceInfo_100::getTextOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getTextOutput(tStream);

  const std::string        sHorizontalIndent(static_cast<size_t>(m_nMaxDescriptionSize) + 4, ' ');
  std::vector<std::string> tInfo;

  DeviceInfo::writePrecisionInfo(
    tStream,
    m_nMaxDescriptionSize,
    tInfo,
    sIndent2,
    sHorizontalIndent,
    "No Half Precision Floating Point Capability found.",
    m_tHalfPrecisionFloatingPointConfiguration);

  tStream << m_tMaxSimultaneousImageObjectsWrite << std::endl;
  tStream << m_tMinDataTypeAlignSize << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tQueueProperties) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tQueueProperties).empty())
  {
    for (auto i = OPEN_cl_command_queue_properties::BEGIN;
         i != OPEN_cl_command_queue_properties::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tQueueProperties) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorHorizontally(tStream, tInfo);
    }
    else
    {
      tStream << "No Queue Properties found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tQueueProperties) << '\n';
  }
}

void DeviceInfo_100::getHTMLOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getHTMLOutput(tStream);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tHalfPrecisionFloatingPointConfiguration,
    OPEN_cl_device_fp_config::BEGIN,
    OPEN_cl_device_fp_config::END,
    "Could not determine Half Precision Floating Point Capability.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSimultaneousImageObjectsWrite);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMinDataTypeAlignSize);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tQueueProperties,
    OPEN_cl_command_queue_properties::BEGIN,
    OPEN_cl_command_queue_properties::END,
    "No Queue Properties found.");
}

