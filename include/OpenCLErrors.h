
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * A singleton class for OpenCL error codes.
 */

#pragma once

#include <string>
#include <tuple>
#include <vector>

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

#include "SingletonBase.h"

class OpenCLErrors : public ConstSingletonBase<OpenCLErrors>
{
  public:

    virtual ~OpenCLErrors()
    {
      m_tOpenCLErrors.clear();
    }

    const std::string& error(const int nError) const;
    const std::string& errorDescription(const int nError) const;
    const std::string& errorCategory(const int nError) const;
    const std::string& operator[](const int nError) const;

  private:

    typedef int         CLErrorCode;
    typedef std::string CLErrorName;
    typedef std::string CLFunction;
    typedef std::string CLErrorDescription;
    typedef std::string CLErrorCategory;
    typedef std::tuple<CLErrorCode, CLErrorName, CLFunction, CLErrorDescription, CLErrorCategory>
      OpenCLError;

    typedef enum
    {
      ERROR_CODE        = 0,
      ERROR_NAME        = 1,
      ERROR_FUNCTION    = 2,
      ERROR_DESCRIPTION = 3,
      ERROR_CATEGORY    = 4,
    } ERROR_INDEX;

    std::vector<OpenCLError> m_tOpenCLErrors;

    OpenCLErrors();
    void populateErrors();

    friend class ConstSingletonBase<OpenCLErrors>;

    // Intentionally deleted.
    OpenCLErrors(const OpenCLErrors& rhs) = delete;
    OpenCLErrors(OpenCLErrors&& rhs)      = delete;
    OpenCLErrors&       operator=(const OpenCLErrors& rhs) = delete;
    OpenCLErrors&       operator=(OpenCLErrors&& rhs) = delete;
    OpenCLErrors*       operator&()                   = delete;
    const OpenCLErrors* operator&() const             = delete;
};

