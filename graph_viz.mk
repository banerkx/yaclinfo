
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GRAPH_VIZ_MK_INCLUDE_GUARD # {
GRAPH_VIZ_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/vars_common.mk.  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

DOT_COMMAND=dot
DOT:=$(shell command -v $(DOT_COMMAND) 2> /dev/null)

ifneq ($(DOT),) # {

# qq need to find correct font for italics!!
ifdef DOT_ITALICS # {
DOT_ITAL_BEG=<I>
DOT_ITAL_END=</I>
endif # }

SUPER_MINUS=&\#x207b;
DEG_MINUS=$(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(SUPER_MINUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END)

SUPER_PLUS=&\#8314;
DEG_PLUS=$(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(SUPER_PLUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END)

DOT_NODE_COLOR ?= color="blue";
DOT_EDGE_COLOR ?= [color="red";];
DOT_PEN_WIDTH  ?= 2.0

DOT_FONT_SIZE ?= 20
DOT_LABEL_FONT_SIZE=<FONT POINT-SIZE="$(DOT_FONT_SIZE)">
DOT_COLOR_SCHEME ?= X11

################################################################################
# Fonts section. ("Times-Roman" is the default dot font.)                      #
################################################################################
DOT_FONT       ?= Times-Roman
DOT_GRAPH_FONT ?= $(DOT_FONT)
DOT_NODE_FONT  ?= $(DOT_FONT)
DOT_EDGE_FONT  ?= $(DOT_FONT)

DOT_VERSION=$(call get_stderr_version,$(DOT),-V,| sed 's/^.*version //;s/ .*$$//')

DOT_FILE_EXTENSION  ?= dot
DOT_IMAGE_EXTENSION ?= svg
DOT_RANK_DIR        ?= LR

################################################################################
# dot splines parameter:                                                       #
#                                                                              #
#   - No edges are drawn at all.                                               #
#       splines="none";                                                        #
#       splines="";                                                            #
#                                                                              #
#   - Edges are drawn as line segments (edges may go through nodes).           #
#       splines="line";                                                        #
#       splines="false";                                                       #
#                                                                              #
#   - Edges are drawn as splines routed around nodes. For dot, this is the     #
#     default.                                                                 #
#       splines="spline";                                                      #
#       splines="true";                                                        #
#                                                                              #
#   - Edges should be drawn as polylines.                                      #
#       splines="polyline";                                                    #
#                                                                              #
#   - Edges are drawn as curved arcs (edges may go through nodes).             #
#       splines="curved";                                                      #
#                                                                              #
#   - Edges should be routed as polylines of axis-aligned segments.            #
#      Currently, the routing does not handle ports or, in dot, edge labels.   #
#        splines="ortho";                                                      #
################################################################################
##DOT_SPLINE ?= none
##DOT_SPLINE ?= line
##DOT_SPLINE ?= false
##DOT_SPLINE ?= true
##DOT_SPLINE ?= polyline
##DOT_SPLINE ?= curved
##DOT_SPLINE ?= spline
DOT_SPLINE ?= ortho

XMLLINT:=$(notdir $(if $(XMLLINT),$(XMLLINT),$(shell command -v xmllint)))
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
DOT_SVG_RENDERER=:svg:core
endif # }

################################################################################
# Verifies the correct XML syntax and beautifies the input file.               #
#                                                                              #
# NOTE: If the xmllint command was not found, then calling this function will  #
#       result in the empty string.                                            #
#                                                                              #
# $(1) - input XML file name                                                   #
################################################################################
xml_validate_and_format=$(XMLLINT) --noout $(1) && $(XMLLINT) --output $(1) --format $(1)

################################################################################
# Creates the command to use xmllint for verifying correct XML syntax and for  #
# beautifying the XML file.                                                    #
#                                                                              #
# NOTE: If the xmllint command was not found, then calling this function will  #
#       result in the empty string.                                            #
#                                                                              #
# $(1) - input XML file name                                                   #
################################################################################
ifneq ($(XMLLINT),) # {
get_dot_xmllint_command=$(XMLLINT) --noout $(notdir $(1)) && $(XMLLINT) --output $(notdir $(1)) --format $(notdir $(1))
endif # }

################################################################################
# Assembles the command to execute the dot command on a dot file to create     #
# the dot graph image file, and to run XML format validation and beautifying;  #
# the latter is only added to the command if xmllint is available.             #
#                                                                              #
# $(1) - dot image file name                                                   #
# $(2) - dot input file name                                                   #
################################################################################
get_dot_command=$(DOT) -T$(if $(filter svg,$(DOT_IMAGE_EXTENSION)),$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER),$(DOT_IMAGE_EXTENSION)) -o $(notdir $(1)) $(notdir $(2)) $(if $(filter svg,$(DOT_IMAGE_EXTENSION)),$(if $(XMLLINT),&& $(call get_dot_xmllint_command,$(1))))

################################################################################
# Assembles the graph label.                                                   #
#                                                                              #
# $(1) - graph title                                                           #
# $(2) - date/time stamp (optional)                                            #
################################################################################
get_graph_label=<$(1) Date/Time: [$(if $(2),$(2),$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z"))] $(MAKE) version: [$(MAKE_VERSION)] dot version: [$(DOT_VERSION)] $(if $(GIT_ROOT),git root: [$(GIT_ROOT)]) $(if $(GIT_BRANCH),git branch: [$(GIT_BRANCH)]) </FONT>>;

################################################################################
# Counts the number of incoming edges for the specified node.                  #
#                                                                              #
# $(1) - node                                                                  #
# $(2) - graph log file                                                        #
# $(3) - separator, e.g., -> or →                                              #
################################################################################
get_node_degree_in=$(shell sed "s@\$${HOME}@${HOME}@g" $(2) | egrep -c ' *$(3) *"?$(subst $${HOME},\$${HOME},$(subst .,\.,$(1)))("\[|$$)')

################################################################################
# Counts the number of outgoing edges for the specified node.                  #
#                                                                              #
# $(1) - node                                                                  #
# $(2) - graph log file                                                        #
# $(3) - separator, e.g., -> or →                                              #
################################################################################
get_node_degree_out=$(shell sed "s@\$${HOME}@${HOME}@g" $(2) | grep -c '^ *"\?$(subst $${HOME},\$${HOME},$(subst .,\.,$(1)))"\? *$(3) *')

################################################################################
# Adds the in and out degrees to the label of each node.                       #
#                                                                              #
# $(1) - dot file                                                              #
################################################################################
SUP_MINUS=&\#x207b;
ESC_SUP_MINUS=$(subst &,\&,$(SUP_MINUS))
SUP_PLUS=&\#8314;
ESC_SUP_PLUS=$(subst &,\&,$(SUP_PLUS))
define add_node_degrees
	@sed -i 's@ ($(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(SUP_MINUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END) = [0-9]\+, $(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(SUP_PLUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END) = [0-9]\+)@@' $(1); \
   for NODE in $$(grep '^  ".*"\[' $(1) | grep -v " -> " | sed 's/\[.*$$//' | sort -u);                                                                                                                \
   do                                                                                                                                                                                                  \
     NODE=$$(echo $${NODE} | sed 's/"/\\"/g;s/\$$/\\$$/;s/\[.*$$//');                                                                                                                                  \
     IN_DEG=$$(printf "%'d" $$(grep -c " -> $${NODE}\["  $(1)));                                                                                                                                       \
     OUT_DEG=$$(printf "%'d" $$(grep -c "^  $${NODE} -> "  $(1)));                                                                                                                                     \
     sed -i "s@\(^  $${NODE}\[.*\)\(label=<.*\)\(>.*$$\)@\1\2 ($(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(ESC_SUP_MINUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END) = $${IN_DEG}, $(DOT_ITAL_BEG)d$(DOT_ITAL_END)$(ESC_SUP_PLUS)$(DOT_ITAL_BEG)(v)$(DOT_ITAL_END) = $${OUT_DEG})\3@" $(1); \
   done
endef

################################################################################
# Adds the cardinalities of the set of vertices (nodes) and edges to the dot   #
# file label.                                                                  #
#                                                                              #
# $(1) - the dot file name                                                     #
################################################################################
define add_nodes_edges_cardinalities
	@GRAPH_NODES=$$(printf "%'d" $$(grep -v " -> " $(1) | grep -c '^  ".*"\[.*\];$$')); \
   GRAPH_EDGES=$$(printf "%'d" $$(grep -c " -> " $(1)));                              \
   sed -i "s@\(^  label=<.*Dependency Graph \)\((|$(DOT_ITAL_BEG)V$(DOT_ITAL_END)|.* = [0-9]\+, |$(DOT_ITAL_BEG)E$(DOT_ITAL_END)| = [0-9]\+) \)\?\(Date.*>;$$\)@\1(|$(DOT_ITAL_BEG)V$(DOT_ITAL_END)| = $${GRAPH_NODES}, |$(DOT_ITAL_BEG)E$(DOT_ITAL_END)| = $${GRAPH_EDGES}) \3@" $(1)
endef

################################################################################
# Since the dot command is available, this target resolves to a no-op.         #
################################################################################
.PHONY: check_dot
check_dot:
	@:

platform_info::
	@echo 'dot version = [$(DOT_VERSION)]'

DOT_WEB_SITE=https://graphviz.gitlab.io
DOT_DOWNLOAD=$(DOT_WEB_SITE)/download/source/
DOT_LATEST_VERSION=$(firstword $(shell { curl -s $(DOT_DOWNLOAD) 2> /dev/null; echo UNKNOWN; } | egrep "(UNKNOWN|Graphviz .* Releases *graphviz.*\.[gx]z)" | head -1 | sed 's/\(^Graphviz .* Releases *graphviz-\)\([1-9].*[0-9]\)\( *graphviz-.*\.[gx]z.*\)/\2/'))
.PHONY: show_updates
show_updates::
	@$(call check_updates,dot,$(DOT_DOWNLOAD),$(DOT_VERSION),$(DOT_LATEST_VERSION))

else # } {

################################################################################
# Since the dot command is not available, whatever target needs the dot        #
# command will error out if this target is used as a prerequisite.             #
################################################################################
.PHONY: check_dot
check_dot:
	@$(error $(ERROR_LABEL) The dot command [$(DOT_COMMAND)] was not fount)

endif # }

endif # }

