
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "HostPlatformInfo.h"

#include <fcntl.h>
#include <gnu/libc-version.h>
#include <openssl/crypto.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>

#include <algorithm>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem.hpp>
#include <boost/version.hpp>
#include <climits>
#include <cstdlib>
#include <filesystem>
#include <iterator>
#include <locale>
#include <regex>
#include <set>
#include <sstream>
#include <thread>

#include "POpen.h"
#include "ScopedFile.h"
#include "StringToPrimitive.h"
#include "StringTokenizer.h"

HostPlatformInfo::HostPlatformInfo() :
  ConstSingletonBase<HostPlatformInfo>(),
  m_nHardwareConcurrency(std::thread::hardware_concurrency()),
  m_sAslrLevel(HostPlatformInfo::getASLRLevel()),
  m_sBoostVersion(HostPlatformInfo::getBoostVersion()),
  m_sCPUModel(HostPlatformInfo::getCpuModel()),
  m_sCompilerTargetMachine(HostPlatformInfo::getCompilerTargetMachine()),
  m_sCoreFilePattern(HostPlatformInfo::getCoreFilePattern()),
  m_sCoreFileSize(HostPlatformInfo::getCoreFileSize()),
  m_sCurrentDateTime(HostPlatformInfo::getCurrentDateTime()),
  m_sEndianess(HostPlatformInfo::endianCheck()),
  m_sGitBranch(
    HostPlatformInfo::getPlatformAttribute("git rev-parse --abbrev-ref HEAD 2> /dev/null")),
  m_sGitRemoteRepo(
    HostPlatformInfo::getPlatformAttribute("git config --get remote.origin.url 2> /dev/null | sed "
                                           "'s|//.*@|//|'")),
  m_sGitRoot(HostPlatformInfo::getPlatformAttribute("git rev-parse --show-toplevel 2> /dev/null")),
  m_sGitVersion(HostPlatformInfo::getPlatformAttribute(
    "git --version 2> /dev/null | head -1 | sed 's/^git version //'")),
  m_sHypervisorVendor(HostPlatformInfo::getPlatformAttribute(
    "(((type lscpu > /dev/null 2>&1) && ((lscpu | grep \"^Hypervisor "
    "vendor: \") || echo \"Hypervisor vendor: real machine\")) || echo "
    "\"Hypervisor vendor: UNKNOWN\") | sed 's/^Hypervisor vendor: //'")),
  m_sJavaHome(HostPlatformInfo::getJavaHome()),
  m_tClassPaths(HostPlatformInfo::getClassPaths()),
  m_sJavaVersion(HostPlatformInfo::getPlatformAttribute(
    "java -version 2>&1 | grep ' version ' | sed 's/^.* version "
    "\"//;s/\"//'")),
  m_sLibCPPVersion(HostPlatformInfo::getStdCPPVersion()),
  m_sLibCVersion(gnu_get_libc_version()),
  m_sLocalHost(IPAddresses::getInstance().localHostName()),
  m_sOpenSSLVersion(OpenSSL_version(OPENSSL_VERSION)),
  m_sOperatingSystem(HostPlatformInfo::getOperatingSystemInfo()),
  m_sPThreadsLibrary(HostPlatformInfo::getPthreadsLibrary()),
  m_sPlantUMLJar(HostPlatformInfo::getJarFileLocation("plantuml.jar", m_tClassPaths)),
  m_sPlantUMLVersion(HostPlatformInfo::getPlantUMLVersion(m_sPlantUMLJar)),
  m_sShell(),
  m_sShellVersion(),
  m_tResourceLimits(ResourceLimits::getInstance()),
  m_tIPAddresses(IPAddresses::getInstance().ipAddresses()),
  m_tVideoCardInfo(),
  m_tHostPlatformAttributes(),
  m_tSysInfo(),
  m_nSysInfoStat(sysinfo(&m_tSysInfo)),
  m_tGitSubmodules()
{
  setShellInfo();
  setVideoCardInfo();
  populateHostPlatformAttributes();
}

HostPlatformInfo::~HostPlatformInfo()
{
  m_tVideoCardInfo.clear();
}

std::string HostPlatformInfo::endianCheck()
{
  const uint16_t n = 1;
  const char*    p = (const char*) &n;

  if (p[0] == 1)  // Lowest address contains the most significant byte
  {
    return ("little");
  }
  else
  {
    return ("BIG");
  }
}

std::string HostPlatformInfo::getCoreFilePattern()
{
  ScopedInputFile tReader("/proc/sys/kernel/core_pattern");
  if (true == tReader.is_open())
  {
    std::string sLine;
    std::getline(tReader, sLine);
    return (sLine);
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::getCoreFileSize()
{
  std::stringstream tUlimitSize;
  tUlimitSize.imbue(std::locale(""));
  tUlimitSize << HostPlatformInfo::getPlatformAttribute("ulimit -c") << " bytes";
  return (tUlimitSize.str());
}

std::string HostPlatformInfo::getCurrentDateTime()
{
  std::time_t tDateTime = std::time(nullptr);
  std::time(&tDateTime);
  struct tm* pTimeInfo = std::localtime(&tDateTime);

  const size_t nBufferSize                = 45;
  char         sDateTime[nBufferSize + 1] = {0};

  static const std::string sTimeFormat("%A %B %e %I:%M:%S %p %Y %Z");
  (void) std::strftime(sDateTime, nBufferSize, sTimeFormat.c_str(), pTimeInfo);
  return (sDateTime);
}

std::string HostPlatformInfo::getCpuModel()
{
  ScopedInputFile tReader("/proc/cpuinfo");
  if (true == tReader.is_open())
  {
    std::string sLine;
    std::smatch tMatch;
    std::regex  tSearch("(^model name\t: )(.*$)");
    while (std::getline(tReader, sLine))
    {
      if (true == std::regex_search(sLine, tMatch, tSearch))
      {
        return (tMatch[2].str());
      }
    }
    return ("UNKNOWN");
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::getOperatingSystemInfo()
{
  struct utsname tUTSNameInfo;
  if (0 == uname(&tUTSNameInfo))
  {
    std::stringstream tInfo;
    tInfo << "OS Name: [" << tUTSNameInfo.sysname << "] Node Name: [" << tUTSNameInfo.nodename
          << "] Release: [" << tUTSNameInfo.release << "] Version: [" << tUTSNameInfo.version
          << "] HW Identifier: [" << tUTSNameInfo.machine << ']';
#ifdef _GNU_SOURCE
    tInfo << " NIS/YP Domain Name: [" << tUTSNameInfo.domainname << ']';
#else
    tInfo << " NIS/YP Domain Name: [UNKNOWN]";
#endif
    return (tInfo.str());
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::getStdCPPVersion()
{
  try
  {
    POpen tReader(
      "readelf -sV /usr/lib64/libstdc++.so* | sed -n 's/.*@@GLIBCXX_//p' | "
      "sort -u -V | tail -1",
      POpen::READ);

    const size_t nBufferSize              = 100;
    char         pBuffer[nBufferSize + 1] = {0};
    (void) std::fgets(pBuffer, nBufferSize, tReader);
    pBuffer[std::strlen(pBuffer) - 1] = 0;  // Overwriting '\n'.
    return (pBuffer);
  }
  catch (const std::runtime_error& e)
  {
    return ("UNKNOWN");
  }
}

std::string HostPlatformInfo::getASLRLevel()
{
  ScopedInputFile tReader("/proc/sys/kernel/randomize_va_space");
  if (true == tReader.is_open())
  {
    static std::map<std::string, std::string> tASLR;
    if (true == tASLR.empty())
    {
      tASLR.insert(std::make_pair("0", "Disanled (0)"));
      tASLR.insert(std::make_pair("1", "Conservative Randomization (1)"));
      tASLR.insert(std::make_pair("2", "Full Randomization (2)"));
    }

    std::string sLine;
    std::getline(tReader, sLine);
    std::map<std::string, std::string>::const_iterator i = tASLR.find(sLine);
    if (tASLR.end() != i)
    {
      return (i->second);
    }
    return ("UNKNOWN");
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::getBoostVersion()
{
  std::string sBoostVersion(BOOST_LIB_VERSION);

  size_t nUnderScoreIndex = sBoostVersion.find('_');
  while (std::string::npos != nUnderScoreIndex)
  {
    sBoostVersion.replace(nUnderScoreIndex, 1, 1, '.');
    nUnderScoreIndex = sBoostVersion.find('_');
  }
  return (sBoostVersion);
}

void HostPlatformInfo::setShellInfo()
{
  // Getting all the shells in /etc/shells.
  static std::set<std::string> tShells;
  if (true == tShells.empty())
  {
    ScopedInputFile tEtcShells("/etc/shells");
    if (true == tEtcShells.is_open())
    {
      std::regex tShellRegex("(^.*)sh$");

      std::string sLine;
      std::getline(tEtcShells, m_sShell);
      while (std::getline(tEtcShells, sLine))
      {
        if (true == std::regex_match(sLine, tShellRegex))
        {
          tShells.insert(boost::filesystem::canonical(sLine).string());
        }
      }
    }
  }

  // Getting the ancestor shell PID and program name.
  std::pair<pid_t, std::string> tAncestorShell;
  HostPlatformInfo::findAncestorShellProcess(tShells, tAncestorShell);

  // Was there an error?
  if (0 == tAncestorShell.first)
  {
    // We now try to get the SHELL environment variable.
    const char* pShell = getenv("SHELL");
    if (nullptr == pShell)
    {
      m_sShell        = "UNKNOWN";
      m_sShellVersion = "UNKNOWN";
      return;
    }
    else
    {
      m_sShell = pShell;
    }
  }
  else
  {
    m_sShell = tAncestorShell.second;
  }

  try
  {
    m_sShellVersion =
      HostPlatformInfo::getPlatformAttribute(m_sShell + " --version 2> /dev/null | head -1");

    // Was there an error?
    if (m_sShellVersion == "UNKNOWN")
    {
      // May be we have a Korn-like shell?
      std::string sStringsCommand("strings ");
      sStringsCommand += m_sShell + " 2> /dev/null | grep KSH_VERSION | head -1";

      const size_t nBufferSize              = 128;
      char         pBuffer[nBufferSize + 1] = {0};

      POpen tStringsReader(sStringsCommand, POpen::READ);
      (void) std::fgets(pBuffer, nBufferSize, tStringsReader);
      pBuffer[std::strlen(pBuffer) - 1] = 0;  // Overwriting '\n'.

      m_sShellVersion = (0 != std::strlen(pBuffer)) ? pBuffer : "UNKNOWN";
    }
  }
  catch (const std::runtime_error& e)
  {
    m_sShellVersion = "UNKNOWN";
  }
}

std::string HostPlatformInfo::getProcessCanonicalName(const pid_t nPID)
{
  std::stringstream tExe;
  tExe << "/proc/" << nPID << "/exe";
  return (boost::filesystem::canonical(tExe.str()).string());
}

void HostPlatformInfo::findAncestorShellProcess(
  const std::set<std::string>&   tShells,
  std::pair<pid_t, std::string>& tAncestorShell)
{
  try
  {
    constexpr static pid_t  nRootPID   = 1;
    constexpr static size_t nPIDIndex  = 0;
    constexpr static size_t nPPIDIndex = 3;

    static const size_t nBufferSize              = 1024;
    char                sBuffer[nBufferSize + 1] = {0};

    static const std::string sProc("/proc/");
    static const std::string sStat("/stat");

    pid_t nPID            = getppid();
    tAncestorShell.first  = 0;
    tAncestorShell.second = "";
    bool bSuccess         = false;
    while (nRootPID != tAncestorShell.first)
    {
      std::string          sStatFile = sProc + std::to_string(nPID) + sStat;
      ScopedFileDescriptor fd(open(sStatFile.c_str(), O_RDONLY));
      if (-1 == fd)
      {
        return;
      }

      ssize_t nBytesRead = read(fd, sBuffer, nBufferSize);
      if (nBytesRead < 0)
      {
        return;
      }

      StringTokenizer tTokens(sBuffer);
      tAncestorShell.first = stringToPrimitive<pid_t>(tTokens[nPIDIndex], bSuccess);
      if (false == bSuccess)
      {
        tAncestorShell.first  = 0;
        tAncestorShell.second = "";
        return;
      }

      std::string sProcessName(HostPlatformInfo::getProcessCanonicalName(tAncestorShell.first));
      const std::set<std::string>::const_iterator i =
        std::find(tShells.begin(), tShells.end(), sProcessName);
      if (i != tShells.end())
      {
        tAncestorShell.second = sProcessName;
        return;
      }
      std::memset(sBuffer, 0, nBufferSize + 1);
      nPID = stringToPrimitive<pid_t>(tTokens[nPPIDIndex], bSuccess);
      if (false == bSuccess)
      {
        tAncestorShell.first  = 0;
        tAncestorShell.second = "";
        return;
      }
    }
  }
  catch (...)
  {
    tAncestorShell.first  = 0;
    tAncestorShell.second = "";
    return;
  }
}

const std::vector<std::string>& HostPlatformInfo::getLibraryDirectories()
{
  static std::vector<std::string> tLibDirs;
  if (true == tLibDirs.empty())
  {
    static const std::string sLibEnvVar("LD_LIBRARY_PATH");
    const char*              pLibPath = getenv(sLibEnvVar.c_str());
    if (nullptr != pLibPath)
    {
      StringTokenizer tTokens(pLibPath, ':');
      tLibDirs.assign(tTokens.begin(), tTokens.end());

      // Trimming leading and trailing white space.
      std::for_each(
        tLibDirs.begin(),
        tLibDirs.end(),
        [](std::string& sDir) { boost::algorithm::trim(sDir); });

      // Deleting possible non-directories (this will also get rid of empty
      // strings).
      tLibDirs.erase(
        std::remove_if(
          tLibDirs.begin(),
          tLibDirs.end(),
          [](const std::string& sDir) { return (!boost::filesystem::is_directory(sDir)); }),
        tLibDirs.end());

      // Removing possible duplicate entries.
      std::set<std::string> tTemp(tLibDirs.begin(), tLibDirs.end());
      tLibDirs.assign(tTemp.begin(), tTemp.end());

      if (false == tLibDirs.empty())
      {
        // Want to move those directories containing "/lib64" to the front.
        std::regex tLibRegex("(^.*)libpthread\\.so\\.(.*$)");
        (void) std::partition(
          tLibDirs.begin(),
          tLibDirs.end(),
          [](const std::string& sLibDir) { return (std::string::npos != sLibDir.find("/lib64")); });
      }
    }
  }
  return (tLibDirs);
}

std::string HostPlatformInfo::getPthreadsLibrary()
{
  const std::vector<std::string>& tLibDirs = HostPlatformInfo::getLibraryDirectories();

  if (true == tLibDirs.empty())
  {
    return ("UNKNOWN");
  }

  std::regex tLibRegex("(^.*)libpthread\\.so\\.(.*$)");
  for (const auto& i : tLibDirs)
  {
    std::string sLibrary;
    for (auto j = boost::filesystem::directory_iterator(i);
         j != boost::filesystem::directory_iterator();
         ++j)
    {
      if (true == std::regex_match(j->path().string(), tLibRegex))
      {
        return (boost::filesystem::canonical(j->path()).string());
      }
    }
  }

  return ("UNKNOWN");
}

std::string HostPlatformInfo::getPlatformAttribute(std::string sCommand)
{
  try
  {
    POpen tReader(sCommand, POpen::READ);

    static constexpr size_t nBufferSize              = 1024;
    char                    pBuffer[nBufferSize + 1] = {0};
    (void) std::fgets(pBuffer, nBufferSize, tReader);

    if (0 == std::strlen(pBuffer))
    {
      return ("UNKNOWN");
    }

    pBuffer[std::strlen(pBuffer) - 1] = 0;  // Overwriting '\n'.
    return (pBuffer);
  }
  catch (const std::runtime_error& e)
  {
    return ("UNKNOWN");
  }
}

std::string HostPlatformInfo::getCompilerTargetMachine()
{
#if defined(__GNUC__) && defined(__GNUC_MINOR__) && defined(__GNUC_PATCHLEVEL__)
  return ((HostPlatformInfo::getPlatformAttribute("g++ -dumpmachine")));
#else
  return ("UNKNOWN");
#endif
}

std::string HostPlatformInfo::getCompilerVersion()
{
  std::stringstream tVersion;
#if defined(__GNUC__) && defined(__GNUC_MINOR__) && defined(__GNUC_PATCHLEVEL__)
  tVersion << "g++ " << __GNUC__ << '.' << __GNUC_MINOR__ << '.' << __GNUC_PATCHLEVEL__;
#else
  tVersion << "UNKNOWN";
#endif
  return (tVersion.str());
}

void HostPlatformInfo::setVideoCardInfo()
{
  try
  {
    const std::string sCommand("lspci | grep \" VGA compatible controller: \" | cut -d\" \" -f1");
    POpen             tReader(sCommand, POpen::READ);

    const size_t nBufferSize              = 1024;
    char         pBuffer[nBufferSize + 1] = {0};

    std::vector<std::string> tVideoCardSlots;

    FILE* pFile = tReader.file();
    while (!std::feof(pFile))
    {
      char* pReturn = std::fgets(pBuffer, nBufferSize, tReader);
      if ((nullptr == pReturn) || (0 == std::strlen(pBuffer)))
      {
        break;
      }
      pBuffer[std::strlen(pBuffer) - 1] = 0;  // Overwriting '\n'.
      tVideoCardSlots.push_back(pBuffer);
      std::memset(pBuffer, 0, nBufferSize);
    }

    std::memset(pBuffer, 0, nBufferSize);
    static const std::string lsPCI("lspci -vv -s ");
    std::string              sProperties;
    std::string              sProperty;
    std::string              sCardName;

    for (size_t i = 0; i < tVideoCardSlots.size(); ++i)
    {
      POpen tPropertyReader(lsPCI + tVideoCardSlots[i], POpen::READ);
      FILE* pProperty = tPropertyReader.file();
      while (!std::feof(pProperty))
      {
        char* pReturn = std::fgets(pBuffer, nBufferSize, tPropertyReader);
        if ((nullptr == pReturn) || (0 == std::strlen(pBuffer)))
        {
          break;
        }
        sProperty = pBuffer;
        if (std::string::npos == sProperty.find("<access denied>"))
        {
          boost::algorithm::trim(sProperty);
          if (std::string::npos != sProperty.find(" VGA compatible controller: "))
          {
            sCardName = HostPlatformInfo::getCardName(sProperty);
          }
          else if (false == sProperty.empty())
          {
            sProperties += (sProperty + "\n");
          }
        }
        std::memset(pBuffer, 0, nBufferSize);
      }
      m_tVideoCardInfo.insert(std::make_pair(sCardName, sProperties));
      sProperties.clear();
    }
  }
  catch (const std::runtime_error& e)
  {
    m_tVideoCardInfo.clear();
    m_tVideoCardInfo.insert(
      std::make_pair("UNKNOWN VIDEO CARD(s)", "UNKNOWN VIDEO CARD PROPERTIES"));
  }

  if (true == m_tVideoCardInfo.empty())
  {
    m_tVideoCardInfo.insert(
      std::make_pair("UNKNOWN VIDEO CARD(s)", "UNKNOWN VIDEO CARD PROPERTIES"));
  }
}

std::string HostPlatformInfo::getCardName(const std::string& sProperty)
{
  // The beginning part of sProperty is expected to resemble:
  //   "01:00.0 VGA compatible controller: "
  // The first step in extracting the card name is to get rid of
  // "01:00.0 VGA compatible controller: ".
  // NOTE: If sProperty is too short, "UNKNOWN" is returned.
  static const size_t nPrefixLength = 35;
  if (sProperty.size() <= nPrefixLength)
  {
    return ("UNKNOWN");
  }
  std::string sName(sProperty.substr(nPrefixLength, sProperty.size() - nPrefixLength));

  // The terminal part of sProperty is expected to resemble:
  //   " (prog-if 00 [VGA controller])"
  const size_t nTerminalIndex = sName.find(" (prog-if");

  // If, for some reason, the expected terminal part is not found, simply
  // return sName.
  if (std::string::npos == nTerminalIndex)
  {
    return (sName);
  }

  return (sName.substr(0, nTerminalIndex));
}

std::vector<std::string> HostPlatformInfo::getClassPaths()
{
  std::vector<std::string> tClassPaths;
  const char*              pClassPath = getenv("CLASSPATH");
  if (nullptr != pClassPath)
  {
    StringTokenizer tTokens(pClassPath, ':');
    tClassPaths.assign(tTokens.begin(), tTokens.end());
  }
  return (tClassPaths);
}

std::string HostPlatformInfo::getJarFileLocation(
  std::string                     sJarFile,
  const std::vector<std::string>& tClassPaths)
{
  boost::filesystem::path tPath(sJarFile);
  if (".jar" != tPath.extension().string())
  {
    return ("UNKNOWN");
  }

  sJarFile.insert(0, "/");
  for (const auto& sJar : tClassPaths)
  {
    const std::string sCanonicalJarFile(HostPlatformInfo::getCanonicalPath(sJar));
    if (sCanonicalJarFile != "UNKNOWN")
    {
      if (sCanonicalJarFile.size() >= sJarFile.size())
      {
        if ((sCanonicalJarFile.find(sJarFile)) == (sCanonicalJarFile.size() - sJarFile.size()))
        {
          return (sCanonicalJarFile);
        }
      }
    }
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::getPlantUMLVersion(const std::string& sPlantUMLJar)
{
  if ("UNKNOWN" != sPlantUMLJar)
  {
    return (HostPlatformInfo::getPlatformAttribute(
      "java -jar " + sPlantUMLJar +
      " -version 2> /dev/null | head -1 | sed 's/^.*version //;s/ .*$//'"));
  }
  return ("UNKNOWN");
}

std::string HostPlatformInfo::uptime() const
{
  if ((0 != m_nSysInfoStat) || (m_tSysInfo.uptime < 0))
  {
    return ("UNKNOWN");
  }

  constexpr size_t nSecondsPerMinute = 60;
  constexpr size_t nMinutesPerHour   = 60;
  constexpr size_t nHoursPerDay      = 24;
  constexpr size_t nDaysPerWeek      = 7;

  constexpr size_t nSecondsPerWeek =
    nDaysPerWeek * nHoursPerDay * nMinutesPerHour * nSecondsPerMinute;
  constexpr size_t nSecondsPerDay  = nHoursPerDay * nMinutesPerHour * nSecondsPerMinute;
  constexpr size_t nSecondsPerHour = nMinutesPerHour * nSecondsPerMinute;

  const size_t nWeeks      = static_cast<size_t>(m_tSysInfo.uptime) / nSecondsPerWeek;
  size_t nRemainingSeconds = static_cast<size_t>(m_tSysInfo.uptime) - (nWeeks * nSecondsPerWeek);

  const size_t nDays = nRemainingSeconds / nSecondsPerDay;
  nRemainingSeconds -= (nDays * nSecondsPerDay);

  const size_t nHours = nRemainingSeconds / nSecondsPerHour;
  nRemainingSeconds -= (nHours * nSecondsPerHour);

  const size_t nMinutes = nRemainingSeconds / nSecondsPerMinute;
  nRemainingSeconds -= (nMinutes * nSecondsPerMinute);

  std::stringstream tUptime;
  tUptime << nWeeks << " week(s), " << nDays << " day(s), " << nHours << " hour(s), " << nMinutes
          << " minute(s), " << nRemainingSeconds << " second(s)";

  return (tUptime.str());
}

std::string HostPlatformInfo::loadAverages() const
{
  if (0 != m_nSysInfoStat)
  {
    return ("UNKNOWN");
  }

  std::stringstream tLoadAverages;
  tLoadAverages << "1 min. avg. = [" << m_tSysInfo.loads[0] << "] "
                << "5 min. avg. = [" << m_tSysInfo.loads[1] << "] "
                << "15 min. avg. = [" << m_tSysInfo.loads[2] << "]";
  return (tLoadAverages.str());
}

double HostPlatformInfo::totalUsableMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.totalram) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

double HostPlatformInfo::availableMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.freeram) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

double HostPlatformInfo::sharedMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.sharedram) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

double HostPlatformInfo::bufferMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.bufferram) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

double HostPlatformInfo::totalSwap() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.totalswap) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

double HostPlatformInfo::freeSwap() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.freeswap) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

unsigned short HostPlatformInfo::processCount() const
{
  if (0 == m_nSysInfoStat)
  {
    return (m_tSysInfo.procs);
  }
  return (0);
}

// qq always returns 0!!
double HostPlatformInfo::highMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.totalhigh) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

// qq always returns 0!!
double HostPlatformInfo::freeHighMemory() const
{
  if (0 == m_nSysInfoStat)
  {
    return (static_cast<double>(m_tSysInfo.freehigh) / (1024.0 * 1024.0 * 1024.0));
  }
  return (0.0);
}

unsigned int HostPlatformInfo::memoryUnitSize() const
{
  if (0 == m_nSysInfoStat)
  {
    return (m_tSysInfo.mem_unit);
  }
  return (0);
}

std::string HostPlatformInfo::getJavaHome()
{
  const char* pJavaHome = getenv("JAVA_HOME");
  if (nullptr != pJavaHome)
  {
    char  pLinkTarget[PATH_MAX + 1] = {0};
    char* pRealPath                 = realpath(pJavaHome, pLinkTarget);
    if (nullptr != pRealPath)
    {
      return (pRealPath);
    }
  }
  return ("UNKNOWN");
}

void HostPlatformInfo::addPlatformAttribute(const char* sAttribute, const std::string& sValue) const
{
  m_tHostPlatformAttributes.insert(std::make_pair(std::string(sAttribute), sValue));
}

void HostPlatformInfo::populateHostPlatformAttributes()
{
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("ASLR Level"), m_sAslrLevel));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("CPU Model"), m_sCPUModel));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("Date/Time"), m_sCurrentDateTime));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("Endianess"), m_sEndianess));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("Hardware Concurrency"), std::to_string(m_nHardwareConcurrency)));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("JAVA_HOME"), m_sJavaHome));

  if (false == m_tClassPaths.empty())
  {
    m_tHostPlatformAttributes.insert(std::make_pair(
      std::string("CLASSPATH"),
      HostPlatformInfo::assembleStringsVector(m_tClassPaths)));
  }
  else
  {
    m_tHostPlatformAttributes.insert(std::make_pair(std::string("CLASSPATH"), "EMPTY"));
  }

  m_tHostPlatformAttributes.insert(std::make_pair(std::string("Local Host"), m_sLocalHost));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("Operating System"), m_sOperatingSystem));
  m_tHostPlatformAttributes.insert(std::make_pair(
    std::string("Process Priority"),
    std::to_string(m_tResourceLimits.processPriority())));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("boost version"), m_sBoostVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("compiler target machine"), m_sCompilerTargetMachine));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("core file pattern"), m_sCoreFilePattern));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("core file size"), m_sCoreFileSize));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("git version"), m_sGitVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("hypervisor vendor"), m_sHypervisorVendor));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("java version"), m_sJavaVersion));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("libc version"), m_sLibCVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("libstdc++ version"), m_sLibCPPVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("openssl version"), m_sOpenSSLVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("plantuml jar file"), m_sPlantUMLJar));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("plantuml jar version"), m_sPlantUMLVersion));
  m_tHostPlatformAttributes.insert(
    std::make_pair(std::string("pthread library"), m_sPThreadsLibrary));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("shell version"), m_sShellVersion));
  m_tHostPlatformAttributes.insert(std::make_pair(std::string("shell"), m_sShell));

  if (m_sGitRoot != "UNKNOWN")
  {
    m_tHostPlatformAttributes.insert(std::make_pair("git branch", m_sGitBranch));
    m_tHostPlatformAttributes.insert(std::make_pair("git remote repository", m_sGitRemoteRepo));
    m_tHostPlatformAttributes.insert(std::make_pair("git root", m_sGitRoot));

    getGitSubmodules();
    if (false == m_tGitSubmodules.empty())
    {
      m_tHostPlatformAttributes.insert(std::make_pair(
        "git submodules",
        HostPlatformInfo::assembleStringsVector(m_tGitSubmodules)));
    }
    else
    {
      m_tHostPlatformAttributes.insert(std::make_pair("git submodules", "NONE"));
    }
  }
}

std::string HostPlatformInfo::getCanonicalPath(const std::string& sFile)
{
  if (true == sFile.empty())
  {
    std::stringstream tError;
    tError << "ERROR: Can not determine canonical path of empty file.";
    return (tError.str());
  }

  std::filesystem::path tPath(sFile);
  std::error_code       tErrorCode;
  std::filesystem::path tCanonical = std::filesystem::canonical(tPath, tErrorCode);
  if (0 == tErrorCode.value())
  {
    return (tCanonical.string());
  }
  else
  {
    return ("UNKNOWN");
  }
}

void HostPlatformInfo::getGitSubmodules()
{
  static const std::string sSubModuleQuery(
    "git submodule --quiet foreach --recursive 'echo $displaypath'");
  std::string sSubModules(HostPlatformInfo::getPlatformAttribute(sSubModuleQuery));
  if (sSubModules == "UNKNOWN")
  {
    sSubModules.clear();
  }
  if (false == sSubModules.empty())
  {
    StringTokenizer tTokens(sSubModules, ' ');
    m_tGitSubmodules.assign(tTokens.begin(), tTokens.end());
    std::transform(
      m_tGitSubmodules.begin(),
      m_tGitSubmodules.end(),
      m_tGitSubmodules.begin(),
      HostPlatformInfo::getCanonicalPath);
  }
}

std::string HostPlatformInfo::assembleStringsVector(const std::vector<std::string>& tStrings)
{
  if (false == tStrings.empty())
  {
    std::stringstream tAllStrings;
    std::copy(
      tStrings.begin(),
      tStrings.end(),
      std::ostream_iterator<std::string>(tAllStrings, " "));
    return (tAllStrings.str().substr(0, tAllStrings.str().size() - 1));
  }
  return ("");
}

