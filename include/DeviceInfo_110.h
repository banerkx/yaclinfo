
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "DeviceInfo_100.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 110
#include <CL/opencl.h>

class DeviceInfo_110 : public DeviceInfo_100
{
  public:

    DeviceInfo_110(
      const cl_device_id   nDeviceID,
      const size_t         nDeviceNumber,
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nDeviceVersion = 110) noexcept;

    virtual ~DeviceInfo_110() noexcept override;

    virtual void getTextOutput(std::ostream& tStream) const noexcept override;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept override;

  protected:

    std::tuple<
      const OPEN_cl_device_info_110_120,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tHostUnifiedMemory;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tCharVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tShortVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tIntVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tLongVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tFloatVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tDoubleVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tVectorWidthHalf;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tOpenCLCVersion;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredVectorWidthHalf;

    virtual void getAllDeviceInfo() noexcept override;

  private:

    // Intentionally deleted.
    DeviceInfo_110()                          = delete;
    DeviceInfo_110(const DeviceInfo_110& rhs) = delete;
    DeviceInfo_110(DeviceInfo_110&& rhs)      = delete;
    DeviceInfo_110&       operator=(const DeviceInfo_110& rhs) = delete;
    DeviceInfo_110&       operator=(DeviceInfo_110&& rhs) = delete;
    DeviceInfo_110*       operator&()                     = delete;
    const DeviceInfo_110* operator&() const               = delete;
};

