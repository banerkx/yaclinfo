
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DynamicBuffer.h"

#include <bind/resolv.h>
#include <netinet/in.h>

#include <algorithm>

template<class T> DynamicBuffer<T>::DynamicBuffer(const size_t nSize, const T tInitValue) :
  m_nSize(nSize), m_pBuffer(new T[m_nSize])
{
  std::fill(m_pBuffer.get(), m_pBuffer.get() + m_nSize, tInitValue);
}

template<class T> DynamicBuffer<T>::~DynamicBuffer()
{
}

template class DynamicBuffer<char>;
template class DynamicBuffer<float>;

