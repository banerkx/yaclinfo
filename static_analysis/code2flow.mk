
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CODE2FLOW_MK_INCLUDE_GUARD # {
CODE2FLOW_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/common.mk.       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/graph_viz.mk.    #
################################################################################
ifeq ($(GRAPH_VIZ_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/graph_viz.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

CODE2FLOW_PROJECT=$(notdir $(CURDIR))
CODE2FLOW_OUTPUT=$(CODE2FLOW_PROJECT)_code2flow
C2FLOW_EXT=svg
CODE2FLOW_URL=https://github.com/scottrogowski/code2flow

EDITOR  ?= vim +1
BROWSER ?= firefox

CODE2FLOW_COMMAND=code2flow
CODE2FLOW:=$(shell command -v $(CODE2FLOW_COMMAND))

CODE2FLOW_GOALS=code2flow_help code2flow

ifneq ($(filter $(CODE2FLOW_GOALS) code2flow_mk_help show_help clean platform_info print-%,$(MAKECMDGOALS)),) # {

ifneq ($(CODE2FLOW),) # {

ifneq ($(DOT),) # {

CODE2FLOW_VERSION=$(call get_stdout_version_index,$(CODE2FLOW),--version,2)

.PHONY: code2flow_help
CODE2FLOW_HELP=code2flow_help.txt
code2flow_help:
	@$(CODE2FLOW) --help >| $(CODE2FLOW_HELP)
	@$(call open_file_command,$(CODE2FLOW_HELP),$(EDITOR),code2flow help)

.PHONY: code2flow

ifdef PYTHON_SCRIPTS # {

CODE2FLOW_FILE_EXT=gv
CODE2FLOW_EXT=_code2flow.$(CODE2FLOW_FILE_EXT)
CODE2FLOW_OPTS=--language py --no-grouping --no-trimming --verbose --output

################################################################################
# Creates the code2flow target name from the name of the python script.        #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_code2flow_target_name=$(subst .py,,$(notdir $(1)))_code2flow

################################################################################
# Creates the code2flow output diagram file name.                              #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_code2flow_file=$(CODE2FLOW_OUTPUT)/$(call get_code2flow_target_name,$(1)).$(CODE2FLOW_FILE_EXT)

################################################################################
# Creates the code2flow output diagram file name.                              #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_code2flow_diagram_file=$(CODE2FLOW_OUTPUT)/$(call get_code2flow_target_name,$(1)).$(C2FLOW_EXT)

################################################################################
# Creates the code2flow log file name.                                         #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_code2flow_log=$(CODE2FLOW_OUTPUT)/$(call get_code2flow_target_name,$(1)).log

$(CODE2FLOW_OUTPUT):
	@mkdir -p $(CODE2FLOW_OUTPUT)

DATE_TIME=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
CODE2FLOW_TITLE=code2flow Call Graph for XXXX Date/Time: [$(DATE_TIME)] code2flow version: [$(CODE2FLOW_VERSION)] dot version: [$(DOT_VERSION)]
ifdef GIT_ROOT # {
CODE2FLOW_TITLE += git root: [$(GIT_ROOT)]
endif # }
ifdef GIT_BRANCH # {
CODE2FLOW_TITLE += git branch: [$(GIT_BRANCH)]
endif # }

CODE2FLOW_DIAGRAM_COMMAND=$(DOT) -T$(C2FLOW_EXT):$(C2FLOW_EXT):core -o $(CODE2FLOW_OUTPUT)/XXXX_code2flow.$(C2FLOW_EXT) $(CODE2FLOW_OUTPUT)/XXXX_code2flow.gv
ifneq ($(XMLLINT),) # {
ifeq ($(C2FLOW_EXT),svg) # {
CODE2FLOW_DIAGRAM_COMMAND += && $(XMLLINT) --noout $(CODE2FLOW_OUTPUT)/XXXX_code2flow.$(C2FLOW_EXT) && $(XMLLINT) --output $(CODE2FLOW_OUTPUT)/XXXX_code2flow.$(C2FLOW_EXT) --format $(CODE2FLOW_OUTPUT)/XXXX_code2flow.$(C2FLOW_EXT)
endif # }
endif # }

################################################################################
# Creates a traget to run code2flow on the input python script.                #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
define cr8_code2flow_target

.PHONY: $(call get_code2flow_target_name,$(1))
$(call get_code2flow_target_name,$(1)): CODE2FLOW_LOG_FILE=$(call get_code2flow_log,$(1))
$(call get_code2flow_target_name,$(1)): CODE2FLOW_TITLE:=$(subst XXXX,$(notdir $(1)),$(CODE2FLOW_TITLE))
$(call get_code2flow_target_name,$(1)): CODE2FLOW_DIAGRAM_COMMAND:=$(subst XXXX,$(basename $(notdir $(1))),$(CODE2FLOW_DIAGRAM_COMMAND))
$(call get_code2flow_target_name,$(1)): | $(CODE2FLOW_OUTPUT)
	@rm -f $(subst .$(CODE2FLOW_FILE_EXT),.$(C2FLOW_EXT),$(call get_code2flow_file,$(1))) $(call get_code2flow_file,$(1)) $$(CODE2FLOW_LOG_FILE)
	@$(CODE2FLOW) $(CODE2FLOW_OPTS) $(call get_code2flow_file,$(1)) $(1) > $$(CODE2FLOW_LOG_FILE) 2>&1
	@sed -i 's/^digraph G.*{$$$$/digraph "$(notdir $(1))" {/' $(call get_code2flow_file,$(1))
	@NODES=$$$$(grep -c '^[}]\?node_.*\[label=' $(call get_code2flow_file,$(1)));                                                                                 \
   EDGES=$$$$(grep -c ' -> ' $(call get_code2flow_file,$(1)));                                                                                                  \
   CARDINALITIES="(|$(DOT_ITAL_BEG)V$(DOT_ITAL_END)| = $$$${NODES}, |$(DOT_ITAL_BEG)E$(DOT_ITAL_END)| = $$$${EDGES})";                                          \
   sed -i '\@^}$$$$@i labelloc="t";\nlabel=<<FONT POINT-SIZE="20">$$(subst $${HOME},\$$$${HOME},$$(CODE2FLOW_TITLE))</FONT>>;' $(call get_code2flow_file,$(1)); \
   sed -i "s@Date/@$$$${CARDINALITIES} Date/@" $(call get_code2flow_file,$(1))
	@$$(CODE2FLOW_DIAGRAM_COMMAND)
	@grep -qi "^Code2Flow: [^A-Z,a-z,_]error[^A-Z,a-z,_]" $$(CODE2FLOW_LOG_FILE)                           && \
   echo -e '$(WARNING_LABEL) The code2flow log file [$$(CODE2FLOW_LOG_FILE)] has error(s).\n' && \
   echo '$(EDITOR) $$(CODE2FLOW_LOG_FILE)' || :
	@rm -f $(call get_code2flow_file,$(1))
	@$$(call open_file_command,$(call get_code2flow_diagram_file,$(1)),$(BROWSER),$(1) call graph diagram)

code2flow: $(call get_code2flow_target_name,$(1))

endef
$(foreach py,$(PYTHON_SCRIPTS),$(eval $(call cr8_code2flow_target,$(py))))

code2flow:
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

else # } {

code2flow:
	@$(error $(ERROR_LABEL) No python scripts have been specified (in the variable [PYTHON_SCRIPTS]))

endif # }

code2flow_clean:
	@rm -rf $(CODE2FLOW_OUTPUT)

clean:: code2flow_clean
	@rm -f $(CODE2FLOW_HELP)

platform_info::
	@echo 'code2flow version = [$(CODE2FLOW_VERSION)]'

code2flow_mk_help::
	@echo '$(MAKE) code2flow → run code2flow on python scripts'
	@echo '$(MAKE) code2flow_help → save code2flow help information to [$(CODE2FLOW_HELP)]'

show_help:: code2flow_mk_help

else # } {

$(CODE2FLOW_GOALS):
	@$(error $(ERROR_LABEL) The [$(DOT_COMMAND)] executable is not available, ∴ the target [$(@)] can not be made)

endif # }

else # } {

$(CODE2FLOW_GOALS):
	@$(error $(ERROR_LABEL) The [code2flow] executable is not available, ∴ the target [$(@)] can not be made; either fix your [PATH] environment variable or install [code2flow] from [$(CODE2FLOW_URL)])

endif # }

endif # }

endif # }

