
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# This file is meant to be included in other makefiles. The purpose of this    #
# file is to enable the examination of makefiles.                              #
#                                                                              #
# Always include this file at the end of a makefile. The reason for this is    #
# that the default target in a makefile is the first target found (usually     #
# the "all" target). We don't want to alter the normal functionality of any    #
# makefile that includes this file.                                            #
#                                                                              #
# Do not include any other makefile in this makefile.                          #
#                                                                              #
# Each target in this makefile will only be defined if the including make      #
# file has not already defined the target.                                     #
#                                                                              #
# One can include this file in another makefile by using:                      #
#   include /path/to/this/file/trace.mk  ==> if trace.mk can not be found, a   #
#                                            warning is printed                #
#   -include /path/to/this/file/trace.mk ==> if trace.mk can not be found, no  #
#                                            warning is printed                #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef TRACE_MK_INCLUDE_GUARD # {
TRACE_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# Only create these targets and variables if TRACE is defined.                 #
################################################################################
ifneq ($(TRACE),) # {

################################################################################
# Adding the "-w" option to make (this option causes make to print the names   #
# of the directories it enters and leaves).                                    #
################################################################################
MAKEFLAGS += w

################################################################################
# Determining if the $(eval ...) function is available or not. If it is        #
# available, then eval_available will be set to T. Otherwise, it will be set   #
# to the empty string.                                                         #
################################################################################
$(eval eval_available:=T)
ifneq ($(eval_available),T) # {
$(error $(ERROR_LABEL) This version of make, [$(MAKE_VERSION)], does not have the $$(eval ...) function)
endif # }

################################################################################
# Function to check if the input target name, $(1), already exists or not.     #
# If the result of the function is 0, then the input target does not already   #
# exist.                                                                       #
#                                                                              #
# NOTE: The check_target function works in determining if the input target     #
#       exists or not. However, it is way too slow. Therefore, check_target    #
#       is "hard coded" to always return 0.                                    #
#       If the input target does exist in the including makefile, warning      #
#       messages will be printed resembling the following:                     #
#         trace.mk:102: warning: overriding commands for target `vars'         #
#         Makefile:127: warning: ignoring old commands for target `vars'       #
################################################################################
##check_target = $(shell $(MAKE) -n $(1) 2>&1 | grep -q "^make: \*\*\* No rule to make target \`$(strip $(1))'\.  Stop\.$$"; echo $$?)
check_target = $(shell echo 0)

################################################################################
# Function to check if the input implicit rule, $(1), already exists or not.   #
# If the result of the function is 1, then the input implicit rule does not    #
# already exist.                                                               #
#                                                                              #
# NOTE: The check_implicit_rule function works in determining if the input     #
#       implicit rule target exists or not. However, it is way too slow.       #
#       Therefore, check_implicit_rule is "hard coded" to always return 1.     #
################################################################################
##check_implicit_rule = $(shell $(MAKE) -p | sed -n '/^\# Implicit Rules$$/,$$p' | sed -e 's/\#.*$$//' -e '/%.*:/!d' | grep -q "^$(strip $(1)):"; echo $$?)
check_implicit_rule = $(shell echo 1)

################################################################################
# make -pn --> perform makefile analysis                                       #
#                                                                              #
# gawk '/^# Not a target/ {printf "\n" $0; next} {print $0}' --> if the        #
#     previous line is "^# Not a target", concatenate the current line to the  #
#     previous line                                                            #
#                                                                              #
# egrep -v '(^#|^$|:=)' --> remove lines beginning with "#", empty lines, and  #
#                           lines containing ":="                              #
#                                                                              #
# egrep '(:$|^[a-z,A-Z,0-9].*:)' --> get those lines that:                     #
#                                      end in ":"                              #
#                                      begin with a letter (or number) and end #
#                                      with a ":"                              #
#                                                                              #
# sed 's/:.*$//' --> from each line, remove the characters from ":" to the     #
#                    end of the line                                           #
#                                                                              #
# egrep -v '(^%|=)' --> filter out lines beginning with "%" or has a "="       #
#                                                                              #
# sed 's/:$//' --> remove ":" at the end of each line                          #
#                                                                              #
# NOTE: This target is only created if the including makefile does not         #
#       already have this target defined.                                      #
################################################################################
TARGET_STAT:=$(call check_target, targets)
ifeq ($(TARGET_STAT),0) # {
.PHONY: targets
targets:
	+@$(MAKE) -pnrR | sed '/^# Not a target/,+1d' | grep "^[A-Z,a-z].*:" | sed -e '/=/d' -e '/"/d' -e 's/:.*$$//' -e '/^make$$/d' -e '/%/d'  -e '/^all$$/d' | sort -u
endif # }

################################################################################
# Forcing make to execute serially.                                            #
################################################################################
.NOTPARALLEL:

################################################################################
# Target to print the .PHONY targets.                                          #
#                                                                              #
# NOTE: This target is only created if the including makefile does not         #
#       already have this target defined.                                      #
################################################################################
TARGET_STAT:=$(call check_target, phony)
ifeq ($(TARGET_STAT),0) # {
.PHONY: phony
phony:
	+@$(MAKE) -np | grep "^\.PHONY" | sed 's/^\.PHONY: //' | tr ' ' '\n' | sort
endif # }

################################################################################
# Determining if make version is less than 4.0 or not (0 means that we have    #
# make version less than 4.0).                                                 #
################################################################################
MAKE_GREATER_THAN_EQ_v4:=$(shell echo $(MAKE_VERSION) | grep -q "^[4-9]"; echo $${?})

################################################################################
# Prints usage information.                                                    #
#                                                                              #
# NOTE: This target is only created if the including makefile does not         #
#       already have this target defined.                                      #
################################################################################
TARGET_STAT:=$(call check_target, help)
ifeq ($(TARGET_STAT),0) # {

.PHONY: trace_help
trace_help:
	@echo 'Usage information for [$(TRACE_MAKE_FILE)]:'
	@echo 'To enable the functionality of this makefile, execute make'
	@echo 'by defining the TRACE variable on the command line:'
ifneq ($(MAKE_GREATER_THAN_EQ_v4),0) # {
	@echo '  $(MAKE) TRACE=N'
	@echo 'where N is in {0, 1, 2, 3}.'
	@echo 'For a TRACE value of:'
	@echo '  0 ==> $(MAKE) enables the targets in this makefile, no extra make information is printed '
	@echo '  1 ==> $(MAKE) prints target names and line numbers'
	@echo '  2 ==> the functionality of TRACE=1 plus variable expansion information'
	@echo '  3 ==> the functionality of TRACE=2 plus the echoing of each step'
else # } {
	@echo '  $(MAKE) TRACE=0'
	@echo '  0 ==> $(MAKE) enables the targets in this makefile, no extra make information is printed'
	@$(info $(INFO_LABEL) Use the flag "--trace" with make to get tracing information.)
endif # }
	@echo 'All other values of TRACE are ignored.'
	@echo 'Special targets:'
	@echo '  trace_help     ==> prints this usage information'
	@echo '  phony          ==> lists .PHONY targets'
	@echo '  targets        ==> lists all targets'
	@echo '  this_make_file ==> prints the absolute path of the makefile'
endif # }

################################################################################
# This technique is used to show the steps make performs. As the TRACE level   #
# increases, the amount of messages printed by make increases.                 #
#                                                                              #
# NOTE: Use TRACE=0 to disable the printing of these messages.                 #
################################################################################
ifneq ($(TRACE),0) # {

################################################################################
# $(MAKE) version >= 4.0 has the --trace command line option. The following    #
# allows makefile tracing for $(MAKE) versions < 4.0.                          #
################################################################################
ifneq ($(MAKE_GREATER_THAN_EQ_v4),0) # {

OLD_SHELL:=$(SHELL)
ifeq ($(TRACE),1) # {
  SHELL=$(warning $(if $(@),Target: [$(@)],Target: [UNKNOWN]))$(OLD_SHELL)
else ifeq ($(TRACE),2) # } {
  SHELL=$(warning $(if $(@),Target: [$(@)],Target: [UNKNOWN]) $(if $(<),First Pre-requisite: [$(<)]) $(if $(?),Newer Pre-requisite(s): [$(?)]) $(if $(^),All Pre-requisite(s): [$(^)]))$(OLD_SHELL)
else ifeq ($(TRACE),3) # } {
  SHELL=$(warning $(if $(@),Target: [$(@)],Target: [UNKNOWN]) $(if $(<),First Pre-requisite: [$(<)]) $(if $(?),Newer Pre-requisite(s): [$(?)]) $(if $(^),All Pre-requisite(s): [$(^)]))$(OLD_SHELL) -x
endif # }
else # } {
$(info $(INFO_LABEL) For make version >= 4.0, use the "--trace" flag with make.)
endif # }
endif # }

endif # }

.PHONY: trace_mk_help
trace_mk_help::
	@echo '$(MAKE) TRACE=0 phony → displays all .PHONY targets'
	@echo '$(MAKE) TRACE=0 targets → displays all targets'
	@echo '$(MAKE) TRACE=0 this_make_file → displays full path name of this makefile'
	@echo '$(MAKE) TRACE=0 trace_help → displays more detailed information about tracing this makefile'

show_help:: trace_mk_help

endif # }

