
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo_200.h"

#include "OpenCLHTMLUtilities.h"
#include "cl_command_queue_properties.h"
#include "cl_device_affinity_domain.h"
#include "cl_device_partition_property.h"
#include "cl_device_svm_capabilities.h"

DeviceInfo_200::DeviceInfo_200(
  const cl_device_id   nDeviceID,
  const size_t         nDeviceNumber,
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nDeviceVersion) noexcept :
  DeviceInfo(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber, nDeviceVersion),
  m_tBuiltInKernels(
    OPEN_cl_device_info::OPENCL_DEVICE_BUILT_IN_KERNELS,
    "BUILT-IN KERNELS",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Built-in kernels")),
  m_tGlobalVariablePreferredTotaLSize(
    OPEN_cl_device_info::OPENCL_DEVICE_GLOBAL_VARIABLE_PREFERRED_TOTAL_SIZE,
    "GLOBAL VARIABLE PREFERRED TOTAL SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max preferred size of variables in global address space"),
    OpenCLUnits::m_tBytes),
  m_tImageBaseAddressAlignment(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_BASE_ADDRESS_ALIGNMENT,
    "IMAGE BASE ADDRESS ALIGNMENT",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Min alignment of host_ptr specified to clCreateBuffer()"),
    OpenCLUnits::m_tPixels),
  m_tMax1D2DImageArray(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_MAX_ARRAY_SIZE,
    "MAX IMAGES IN 1D/2D IMAGE ARRAY",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of images in a 1D or 2D image array")),
  m_tMax1DImageBuffer(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_MAX_BUFFER_SIZE,
    "MAX 1D IMAGE FROM BUFFER",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of pixels for a 1D image created from a buffer"),
    OpenCLUnits::m_tPixels),
  m_tImagePitchAlignment(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_PITCH_ALIGNMENT,
    "IMAGE PITCH ALIGNMENT",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Row pitch alignment size for 2D images created from a buffer"),
    OpenCLUnits::m_tPixels),
  m_tHasLinkerAvailable(
    OPEN_cl_device_info::OPENCL_DEVICE_LINKER_AVAILABLE,
    "HAS LINKER AVAILABLE",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is linker available?")),
  m_tMaxGlobalVariableSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_GLOBAL_VARIABLE_SIZE,
    "MAX GLOBAL VARIABLE SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max storage for a variable in program scope"),
    OpenCLUnits::m_tBytes),
  m_tMaxOnDeviceEvents(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_ON_DEVICE_EVENTS,
    "MAX ON DEVICE EVENTS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max events in use by a device queue")),
  m_tMaxOnDeviceQueues(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_ON_DEVICE_QUEUES,
    "MAX ON DEVICE QUEUES",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max device queues that can be created per context")),
  m_tMaxPipeArgs(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_PIPE_ARGS,
    "MAX PIPE ARGS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max pipe objects that can be passed as arguments to a kernel")),
  m_tMaxReadWriteImageArgs(
    OPEN_cl_device_info_200::OPENCL_DEVICE_MAX_READ_WRITE_IMAGE_ARGS,
    "MAX READ WRITE IMAGE ARGS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max kernel image object arguments declared with the "
                  "write_only/read_only qualifier")),
  m_tCharVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
    "CHAR VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native char vector width")),
  m_tShortVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
    "SHORT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native short vector width")),
  m_tIntVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
    "INT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native int vector width")),
  m_tLongVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
    "LONG VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native long vector width")),
  m_tFloatVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
    "FLOAT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native float vector width")),
  m_tDoubleVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
    "DOUBLE VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native double vector width")),
  m_tVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
    "HALF VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, native half scalar and "
                  "vector width")),
  m_tOpenCLCVersion(
    OPEN_cl_device_info::OPENCL_DEVICE_OPENCL_C_VERSION,
    "OpenCL C VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("OpenCL C version")),
  m_tParentDeviceID(
    OPEN_cl_device_info::OPENCL_DEVICE_PARENT_DEVICE,
    "PARENT DEVICE ID",
    sizeof(cl_device_id),
    cl_device_id(nullptr),
    OpenCLError(""),
    OpenCLToolTip("cl_device_id of the parent device")),
  m_tPartitionAffinityDomain(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_AFFINITY_DOMAIN,
    "PARTITION AFFINITY DOMAIN",
    sizeof(cl_device_affinity_domain),
    cl_device_affinity_domain(0),
    OpenCLError(""),
    OpenCLToolTip("Affinity domains for partitioning the device")),
  m_tMaxSubDevices(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_MAX_SUB_DEVICES,
    "MAXIMUM NUMBER OF SUB_DEVICES",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of sub-devices that can be created on a "
                  "partitioned device")),
  m_tPartitionTypesList(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_PROPERTIES,
    "PARTITION TYPE LIST",
    0,
    std::unique_ptr<cl_device_partition_property[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Partition types")),
  m_tPartitionType(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_TYPE,
    "PARTITION TYPE",
    0,
    std::unique_ptr<cl_device_partition_property[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Properties argument specified in clCreateSubDevices")),
  m_tPipeMaxActiveReservations(
    OPEN_cl_device_info::OPENCL_DEVICE_PIPE_MAX_ACTIVE_RESERVATIONS,
    "PIPE MAX ACTIVE RESERVATIONS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max active reservations for a pipe per work-item")),
  m_tPipeMaxPacketSize(
    OPEN_cl_device_info::OPENCL_DEVICE_PIPE_MAX_PACKET_SIZE,
    "PIPE MAX PACKET SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of pipe"),
    OpenCLUnits::m_tBytes),
  m_tPreferredGlobalAtomicAlignment(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_GLOBAL_ATOMIC_ALIGNMENT,
    "PREFERRED GLOBAL ATOMIC ALIGNMENT",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred alignment for OpenCL 2.0 atomic types to "
                  "global memory"),
    OpenCLUnits::m_tBytes),
  m_tPreferredUserSynchronization(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_INTEROP_USER_SYNC,
    "PREFERRED USER SYNCHRONIZATION",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is the device's preference for the user to be "
                  "responsible for synchronization?")),
  m_tPreferredLocalAtomicAlignment(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_LOCAL_ATOMIC_ALIGNMENT,
    "PREFERRED LOCAL ATOMIC ALIGNMENT",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred alignment for OpenCL 2.0 atomic types to "
                  "local memory"),
    OpenCLUnits::m_tBytes),
  m_tPreferredPlatformAtomicAlignment(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_PLATFORM_ATOMIC_ALIGNMENT,
    "PREFERRED PLATFORM ATOMIC ALIGNMENT",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred alignment for OpenCL 2.0 fine-grained SVM "
                  "atomic types"),
    OpenCLUnits::m_tBytes),
  m_tPreferredVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
    "HALF PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, preferred native half scalar and "
                  "vector width that can be put into vectors")),
  m_tMaxPrintfBufferSize(
    OPEN_cl_device_info::OPENCL_DEVICE_PRINTF_BUFFER_SIZE,
    "MAX PRINTF BUFFER SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of the internal buffer that holds the output "
                  "of printf")),
  m_tQueueOnDeviceMaxSize(
    OPEN_cl_device_info::OPENCL_DEVICE_QUEUE_ON_DEVICE_MAX_SIZE,
    "QUEUE ON DEVICE MAX SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of device queue"),
    OpenCLUnits::m_tBytes),
  m_tQueueOnDevicePreferredSize(
    OPEN_cl_device_info::OPENCL_DEVICE_QUEUE_ON_DEVICE_PREFERRED_SIZE,
    "QUEUE ON DEVICE PREFERRED SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Size of preferred device queue"),
    OpenCLUnits::m_tBytes),
  m_tQueueOnDeviceProperties(
    OPEN_cl_device_info::OPENCL_DEVICE_QUEUE_ON_DEVICE_PROPERTIES,
    "QUEUE ON DEVICE PROPERTIES",
    sizeof(cl_command_queue_properties),
    cl_command_queue_properties(0),
    OpenCLError(""),
    OpenCLToolTip("Device command-queue properties"))
  // qq
  //  ,
  //  m_tQueueOnHostProperties(OPEN_cl_device_info_200::OPENCL_DEVICE_QUEUE_ON_HOST_PROPERTIES,
  //  "QUEUE ON HOST PROPERTIES", sizeof(cl_command_queue_properties),
  //  cl_command_queue_properties(0), OpenCLError(""), OpenCLToolTip("Host
  //  command-queue properties"))
  ,
  m_tReferenceCount(
    OPEN_cl_device_info::OPENCL_DEVICE_REFERENCE_COUNT,
    "REFERENCE COUNT",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Device reference count")),
  m_tSPIRVersions(
    OPEN_cl_device_info::OPENCL_DEVICE_SPIR_VERSIONS,
    "SPIR VERSIONS",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Supported SPIR versions")),
  m_tSVMCapabilities(
    OPEN_cl_device_info::OPENCL_DEVICE_SVM_CAPABILITIES,
    "SVM CAPABILITIES",
    sizeof(cl_device_svm_capabilities),
    cl_device_svm_capabilities(0),
    OpenCLError(""),
    OpenCLToolTip("SVM capabilities"))
//  qq
//  ,
//  m_tTerminateCapabilityKHR(OPEN_cl_device_info_200::OPENCL_DEVICE_TERMINATE_CAPABILITY_KHR,
//  "TERMINATE CAPABILITY KHR", sizeof(cl_device_terminate_capability_khr),
//  cl_device_terminate_capability_khr(0),                    OpenCLError(""),
//  OpenCLToolTip("Device termination capabilities"))
{
}

DeviceInfo_200::~DeviceInfo_200() noexcept
{
  clReleaseDevice(std::get<PARAM_VALUE>(m_tParentDeviceID));
}

void DeviceInfo_200::getAllDeviceInfo() noexcept
{
  DeviceInfo::getAllDeviceInfo();

  getDeviceInfo(m_tBuiltInKernels);
  getDeviceInfo(m_tGlobalVariablePreferredTotaLSize);
  getDeviceInfo(m_tImageBaseAddressAlignment);
  getDeviceInfo(m_tMax1D2DImageArray);
  getDeviceInfo(m_tMax1DImageBuffer);
  getDeviceInfo(m_tImagePitchAlignment);
  getDeviceInfo(m_tHasLinkerAvailable);
  getDeviceInfo(m_tMaxGlobalVariableSize);
  getDeviceInfo(m_tMaxOnDeviceEvents);
  getDeviceInfo(m_tMaxOnDeviceQueues);
  getDeviceInfo(m_tMaxPipeArgs);
  getDeviceInfo(m_tMaxReadWriteImageArgs);
  getDeviceInfo(m_tCharVectorWidth);
  getDeviceInfo(m_tShortVectorWidth);
  getDeviceInfo(m_tIntVectorWidth);
  getDeviceInfo(m_tLongVectorWidth);
  getDeviceInfo(m_tFloatVectorWidth);
  getDeviceInfo(m_tDoubleVectorWidth);
  getDeviceInfo(m_tVectorWidthHalf);
  getDeviceInfo(m_tOpenCLCVersion);
  getDeviceInfo(m_tParentDeviceID);
  getDeviceInfo(m_tPartitionAffinityDomain);
  getDeviceInfo(m_tMaxSubDevices);
  getDeviceInfo(m_tPartitionTypesList);
  getDeviceInfo(m_tPartitionType);
  getDeviceInfo(m_tPipeMaxActiveReservations);
  getDeviceInfo(m_tPipeMaxPacketSize);
  getDeviceInfo(m_tPreferredGlobalAtomicAlignment);
  getDeviceInfo(m_tPreferredUserSynchronization);
  getDeviceInfo(m_tPreferredLocalAtomicAlignment);
  getDeviceInfo(m_tPreferredPlatformAtomicAlignment);
  getDeviceInfo(m_tPreferredVectorWidthHalf);
  getDeviceInfo(m_tMaxPrintfBufferSize);
  getDeviceInfo(m_tQueueOnDeviceMaxSize);
  getDeviceInfo(m_tQueueOnDevicePreferredSize);
  getDeviceInfo(m_tQueueOnDeviceProperties);
  // qq
  //  getDeviceInfo(m_tQueueOnHostProperties);
  getDeviceInfo(m_tReferenceCount);
  getDeviceInfo(m_tSPIRVersions);
  getDeviceInfo(m_tSVMCapabilities);
  // qq cl_device_terminate_capability_khr and
  // CL_DEVICE_TERMINATE_CAPABILITY_CONTEXT_KHR not found in any header
  //  getDeviceInfo(m_tTerminateCapabilityKHR);
}

void DeviceInfo_200::getTextOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getTextOutput(tStream);

  const std::string        sHorizontalIndent(static_cast<size_t>(m_nMaxDescriptionSize) + 4, ' ');
  std::vector<std::string> tInfo;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tBuiltInKernels)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tBuiltInKernels).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tBuiltInKernels))
    {
      if ('\0' != std::get<PARAM_VALUE>(m_tBuiltInKernels)[0])
      {
        std::string sBuiltInKernels(std::get<PARAM_VALUE>(m_tBuiltInKernels).get());
        DeviceInfo::extractTokens(tInfo, sBuiltInKernels, ';');
        if (false == tInfo.empty())
        {
          DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
        }
      }
      else
      {
        tStream << "No Built-in Kernels found.";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tBuiltInKernels);
  }
  tStream << std::endl;

  tStream << m_tGlobalVariablePreferredTotaLSize << std::endl;
  tStream << m_tImageBaseAddressAlignment << std::endl;
  tStream << m_tMax1D2DImageArray << std::endl;
  tStream << m_tMax1DImageBuffer << std::endl;
  tStream << m_tImagePitchAlignment << std::endl;
  tStream << m_tHasLinkerAvailable << std::endl;
  tStream << m_tMaxGlobalVariableSize << std::endl;
  tStream << m_tMaxOnDeviceEvents << std::endl;
  tStream << m_tMaxOnDeviceQueues << std::endl;
  tStream << m_tMaxPipeArgs << std::endl;
  tStream << m_tMaxReadWriteImageArgs << std::endl;
  tStream << m_tCharVectorWidth << std::endl;
  tStream << m_tShortVectorWidth << std::endl;
  tStream << m_tIntVectorWidth << std::endl;
  tStream << m_tLongVectorWidth << std::endl;
  tStream << m_tFloatVectorWidth << std::endl;
  tStream << m_tDoubleVectorWidth << std::endl;
  tStream << m_tVectorWidthHalf << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tOpenCLCVersion)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tOpenCLCVersion).empty())
  {
    tStream << std::get<PARAM_VALUE>(m_tOpenCLCVersion).get();
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tOpenCLCVersion);
  }
  tStream << std::endl;

  tStream << m_tParentDeviceID;
  if (
    (true == std::get<PARAM_ERROR>(m_tParentDeviceID).empty()) &&
    (nullptr == std::get<PARAM_VALUE>(m_tParentDeviceID)))
  {
    tStream << " (ROOT-LEVEL DEVICE)";
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tPartitionAffinityDomain) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionAffinityDomain).empty())
  {
    for (auto i = OPEN_cl_device_affinity_domain::BEGIN; i != OPEN_cl_device_affinity_domain::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tPartitionAffinityDomain) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No Partition Affinity Domain information found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionAffinityDomain) << '\n';
  }

  tStream << m_tMaxSubDevices << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tPartitionTypesList) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionTypesList).empty())
  {
    const size_t nArrayLength =
      std::get<PARAM_SIZE>(m_tPartitionTypesList) / sizeof(cl_device_partition_property);
    for (size_t i = 0; i < nArrayLength; ++i)
    {
      tInfo.push_back(cl_device_partition_property_ToString(
        *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No Partition Types List found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionTypesList) << '\n';
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tPartitionType)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionType).empty())
  {
    if (0 == *std::get<PARAM_VALUE>(m_tPartitionType).get())
    {
      tStream << "No partition type information found.\n";
    }
    else
    {
      const size_t nArrayLength =
        std::get<PARAM_SIZE>(m_tPartitionType) / sizeof(cl_device_partition_property);
      for (size_t i = 0; i < nArrayLength; ++i)
      {
        tInfo.push_back(cl_device_partition_property_ToString(
          *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
      }
      if (false == tInfo.empty())
      {
        DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
      }
      else
      {
        tStream << "No partition type information found.\n";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionType) << '\n';
  }

  tStream << m_tPipeMaxActiveReservations << std::endl;
  tStream << m_tPipeMaxPacketSize << std::endl;
  tStream << m_tPreferredGlobalAtomicAlignment << std::endl;
  tStream << m_tPreferredUserSynchronization << std::endl;
  tStream << m_tPreferredLocalAtomicAlignment << std::endl;
  tStream << m_tPreferredPlatformAtomicAlignment << std::endl;
  tStream << m_tPreferredVectorWidthHalf << std::endl;
  tStream << m_tMaxPrintfBufferSize << std::endl;
  tStream << m_tQueueOnDeviceMaxSize << std::endl;
  tStream << m_tQueueOnDevicePreferredSize << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tQueueOnDeviceProperties) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tQueueOnDeviceProperties).empty())
  {
    for (auto i = OPEN_cl_command_queue_properties::BEGIN;
         i != OPEN_cl_command_queue_properties::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tQueueOnDeviceProperties) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No device command-queue properties found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tQueueOnDeviceProperties);
  }

  //  qq
  //  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) <<
  //  std::get<PARAM_DESC>(m_tQueueOnHostProperties) << ": "; if (true ==
  //  std::get<PARAM_ERROR>(m_tQueueOnHostProperties).empty())
  //  {
  //    for (auto i = OPEN_cl_command_queue_properties::BEGIN; i !=
  //    OPEN_cl_command_queue_properties::END; ++i)
  //    {
  //      if (std::get<PARAM_VALUE>(m_tQueueOnHostProperties) &
  //      scopedEnumToCLEnum(i))
  //      {
  //        tInfo.push_back(scopedEnumToString(i));
  //      }
  //    }
  //    if (false == tInfo.empty())
  //    {
  //      DeviceInfo::printVectorHorizontally(tStream, tInfo);
  //    }
  //    else
  //    {
  //      tStream << "No host device command-queue properties found.\n";
  //    }
  //  }
  //  else
  //  {
  //    tStream << std::get<PARAM_ERROR>(m_tQueueOnHostProperties);
  //  }
  //  tStream << '\n';

  tStream << m_tReferenceCount;
  if (true == std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability).empty())
  {
    tStream << ((1 == std::get<PARAM_VALUE>(m_tReferenceCount)) ? " (ROOT-LEVEL DEVICE)" : "");
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tSPIRVersions)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tSPIRVersions).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tSPIRVersions))
    {
      if ('\0' != std::get<PARAM_VALUE>(m_tSPIRVersions)[0])
      {
        std::string sBuiltInKernels(std::get<PARAM_VALUE>(m_tSPIRVersions).get());
        DeviceInfo::extractTokens(tInfo, sBuiltInKernels, ';');
        if (false == tInfo.empty())
        {
          DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
        }
      }
      else
      {
        tStream << "No supported SPIR versions found.\n";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tSPIRVersions) << '\n';
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tSVMCapabilities) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tSVMCapabilities).empty())
  {
    for (auto i = OPEN_cl_device_svm_capabilities::BEGIN; i != OPEN_cl_device_svm_capabilities::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tSVMCapabilities) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No SVM capabilities found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tSVMCapabilities);
  }

  // qq cl_device_terminate_capability_khr and
  // CL_DEVICE_TERMINATE_CAPABILITY_CONTEXT_KHR not found in any header
  //  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) <<
  //  std::get<PARAM_DESC>(m_tTerminateCapabilityKHR) << ": "; if (true ==
  //  std::get<PARAM_ERROR>(m_tTerminateCapabilityKHR).empty())
  //  {
  //    for (auto i = OPEN_cl_device_terminate_capability_khr::BEGIN; i !=
  //    OPEN_cl_device_terminate_capability_khr::END; ++i)
  //    {
  //      if (std::get<PARAM_VALUE>(m_tTerminateCapabilityKHR) &
  //      scopedEnumToCLEnum(i))
  //      {
  //        tInfo.push_back(scopedEnumToString(i));
  //      }
  //    }
  //    if (false == tInfo.empty())
  //    {
  //      DeviceInfo::printVectorHorizontally(tStream, tInfo);
  //    }
  //    else
  //    {
  //      tStream << "No termination capabilities found.\n";
  //    }
  //  }
  //  else
  //  {
  //    tStream << std::get<PARAM_ERROR>(m_tTerminateCapabilityKHR);
  //  }
  //  tStream << '\n';
}

void DeviceInfo_200::getHTMLOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getHTMLOutput(tStream);

  std::vector<std::string> tInfo;

  DeviceInfo::writeHTMLTableRow(tStream, m_tBuiltInKernels, "No Built-in Kernels found.", ';');

  DeviceInfo::writeHTMLTableRow(tStream, m_tGlobalVariablePreferredTotaLSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tImageBaseAddressAlignment);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax1D2DImageArray);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax1DImageBuffer);
  DeviceInfo::writeHTMLTableRow(tStream, m_tImagePitchAlignment);
  DeviceInfo::writeHTMLTableRow(tStream, m_tHasLinkerAvailable);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxGlobalVariableSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxOnDeviceEvents);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxOnDeviceQueues);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxPipeArgs);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxReadWriteImageArgs);

  DeviceInfo::writeHTMLTableRow(tStream, m_tCharVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tShortVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tIntVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tLongVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tFloatVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tDoubleVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tOpenCLCVersion);

  if (true == std::get<PARAM_ERROR>(m_tParentDeviceID).empty())
  {
    std::stringstream tParentDeviceIDInfo;
    tParentDeviceIDInfo << std::get<PARAM_VALUE>(m_tParentDeviceID);
    if (
      (0 != std::get<PARAM_SIZE>(m_tParentDeviceID)) &&
      (nullptr == std::get<PARAM_VALUE>(m_tParentDeviceID)))
    {
      tParentDeviceIDInfo << " (ROOT-LEVEL DEVICE)";
    }
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tParentDeviceID),
      tParentDeviceIDInfo.str(),
      std::get<PARAM_TOOL_TIP>(m_tParentDeviceID));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tParentDeviceID),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tParentDeviceID)),
      std::get<PARAM_TOOL_TIP>(m_tParentDeviceID));
  }

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tPartitionAffinityDomain,
    OPEN_cl_device_affinity_domain::BEGIN,
    OPEN_cl_device_affinity_domain::END,
    "No Partition Affinity Domain information found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSubDevices);
  if (true == std::get<PARAM_ERROR>(m_tPartitionTypesList).empty())
  {
    const size_t nArrayLength =
      std::get<PARAM_SIZE>(m_tPartitionTypesList) / sizeof(cl_device_partition_property);
    for (size_t i = 0; i < nArrayLength; ++i)
    {
      tInfo.push_back(cl_device_partition_property_ToString(
        *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
    }
    if (false == tInfo.empty())
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tPartitionTypesList),
        toHTMLTokens(tInfo),
        std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
      tInfo.clear();
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tPartitionTypesList),
        "No Partition Types List found.",
        std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tPartitionTypesList),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tPartitionTypesList)),
      std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
  }

  if (true == std::get<PARAM_ERROR>(m_tPartitionType).empty())
  {
    std::stringstream tPartitionTypeInfo;
    if (
      (0 == std::get<PARAM_SIZE>(m_tPartitionType)) ||
      (0 == *std::get<PARAM_VALUE>(m_tPartitionType).get()))
    {
      tPartitionTypeInfo << "No partition type information found.";
    }
    else
    {
      const size_t nArrayLength =
        std::get<PARAM_SIZE>(m_tPartitionType) / sizeof(cl_device_partition_property);
      for (size_t i = 0; i < nArrayLength; ++i)
      {
        tInfo.push_back(cl_device_partition_property_ToString(
          *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
      }
      if (false == tInfo.empty())
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tPartitionType),
          toHTMLTokens(tInfo),
          std::get<PARAM_TOOL_TIP>(m_tPartitionType));
        tInfo.clear();
      }
      else
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tPartitionType),
          "No partition type information found.",
          std::get<PARAM_TOOL_TIP>(m_tPartitionType));
      }
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tPartitionType),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tPartitionType)),
      std::get<PARAM_TOOL_TIP>(m_tPartitionType));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tPipeMaxActiveReservations);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPipeMaxPacketSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredGlobalAtomicAlignment);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredUserSynchronization);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredLocalAtomicAlignment);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredPlatformAtomicAlignment);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxPrintfBufferSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tQueueOnDeviceMaxSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tQueueOnDevicePreferredSize);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tQueueOnDeviceProperties,
    OPEN_cl_command_queue_properties::BEGIN,
    OPEN_cl_command_queue_properties::END,
    "No device command-queue properties found.");

  //  qq
  //  DeviceInfo::writeHTMLTokens(
  //    tStream,
  //    m_tQueueOnHostProperties,
  //    OPEN_cl_command_queue_properties::BEGIN,
  //    OPEN_cl_command_queue_properties::END,
  //    "No host device command-queue properties found.");

  std::stringstream tReferenceCountValue;
  if (true == std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability).empty())
  {
    tReferenceCountValue << std::get<PARAM_VALUE>(m_tReferenceCount);
    if (0 != std::get<PARAM_SIZE>(m_tSinglePrecisionFloatingPointCapability))
    {
      if (1 == std::get<PARAM_VALUE>(m_tReferenceCount))
      {
        tReferenceCountValue << " (ROOT-LEVEL DEVICE)";
      }
    }
  }
  else
  {
    tReferenceCountValue << DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tReferenceCount));
  }
  setHTMLTableRow(
    tStream,
    std::get<PARAM_DESC>(m_tReferenceCount),
    tReferenceCountValue,
    std::get<PARAM_TOOL_TIP>(m_tReferenceCount));

  DeviceInfo::writeHTMLTableRow(tStream, m_tSPIRVersions, "No supported SPIR versions found.", ';');

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tSVMCapabilities,
    OPEN_cl_device_svm_capabilities::BEGIN,
    OPEN_cl_device_svm_capabilities::END,
    "No SVM capabilities found.");

  // qq cl_device_terminate_capability_khr and
  // CL_DEVICE_TERMINATE_CAPABILITY_CONTEXT_KHR not found in any header
  //  DeviceInfo::writeHTMLTokens(
  //    tStream,
  //    m_tTerminateCapabilityKHR,
  //    OPEN_cl_device_terminate_capability_khr::BEGIN,
  //    OPEN_cl_device_terminate_capability_khr::END,
  //    "No termination capabilities found.");
}

