
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OpenCLString.h"

#include <map>
#include <utility>

OpenCLString::OpenCLString(const std::string& sInfo) : m_sInfo(sInfo)
{
}

OpenCLString::OpenCLString(const OpenCLString& rhs) : m_sInfo(rhs.m_sInfo)
{
}

OpenCLString::OpenCLString(OpenCLString&& rhs) : m_sInfo(std::move(rhs.m_sInfo))
{
}

OpenCLString& OpenCLString::operator=(const std::string& sLHS)
{
  m_sInfo = sLHS;
  return (*this);
}

OpenCLString::~OpenCLString()
{
}

std::string operator+(const std::string& lhs, const OpenCLString& rhs)
{
  return (lhs + rhs.m_sInfo);
}

std::ostream& operator<<(std::ostream& tStream, const OpenCLString& rhs)
{
  tStream << rhs.m_sInfo;
  return (tStream);
}

OpenCLError::OpenCLError(const std::string& sInfo) : OpenCLString(sInfo)
{
}

OpenCLError::OpenCLError(const OpenCLError& rhs) : OpenCLString(rhs)
{
}

OpenCLError::OpenCLError(OpenCLError&& rhs) : OpenCLString(rhs)
{
}

OpenCLError::~OpenCLError()
{
}

OpenCLToolTip::OpenCLToolTip(const std::string& sInfo) : OpenCLString(sInfo)
{
}

OpenCLToolTip::OpenCLToolTip(const OpenCLToolTip& rhs) : OpenCLString(rhs)
{
}

OpenCLToolTip::OpenCLToolTip(OpenCLToolTip&& rhs) : OpenCLString(rhs)
{
}

OpenCLToolTip::~OpenCLToolTip()
{
}

const OpenCLUnits OpenCLUnits::m_tBits(" bits");
const OpenCLUnits OpenCLUnits::m_tBytes(" bytes");
const OpenCLUnits OpenCLUnits::m_tGigaBytes(" GB");
const OpenCLUnits OpenCLUnits::m_tGigaHertz(" GHz");
const OpenCLUnits OpenCLUnits::m_tKiloBytes(" kB");
const OpenCLUnits OpenCLUnits::m_tMegaHertz(" MHz");
const OpenCLUnits OpenCLUnits::m_tMicroSeconds(" µsec");
const OpenCLUnits OpenCLUnits::m_tNanoSeconds(" nsec");
const OpenCLUnits OpenCLUnits::m_tSeconds(" seconds");
const OpenCLUnits OpenCLUnits::m_tPages(" pages");
const OpenCLUnits OpenCLUnits::m_tPixels(" pixels");

OpenCLUnits::OpenCLUnits(const std::string& sInfo) : OpenCLString(sInfo)
{
}

OpenCLUnits::OpenCLUnits(const OpenCLUnits& rhs) : OpenCLString(rhs)
{
}

OpenCLUnits::OpenCLUnits(OpenCLUnits&& rhs) : OpenCLString(rhs)
{
}

OpenCLUnits::~OpenCLUnits()
{
}

