
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "PlatformInfo.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 120
#include <CL/opencl.h>

class PlatformInfo_120 : public PlatformInfo
{
  public:

    PlatformInfo_120(
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nPlatformVersion = 120) noexcept;

    virtual ~PlatformInfo_120() noexcept override;

  private:

    virtual void populateDeviceTypes() override;

    virtual void getPlatformAttributes() override;

    // Intentionally deleted.
    PlatformInfo_120()                            = delete;
    PlatformInfo_120(const PlatformInfo_120& rhs) = delete;
    PlatformInfo_120(PlatformInfo_120&& rhs)      = delete;
    PlatformInfo_120&       operator=(const PlatformInfo_120& rhs) = delete;
    PlatformInfo_120&       operator=(PlatformInfo_120&& rhs) = delete;
    PlatformInfo_120*       operator&()                       = delete;
    const PlatformInfo_120* operator&() const                 = delete;
};

