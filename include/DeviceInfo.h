
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once
#pragma GCC diagnostic ignored "-Wignored-attributes"

#include <algorithm>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>
#include <vector>

#include "OpenCLString.h"
#include "cl_device_info.h"
#include "cl_device_info_100.h"
#include "cl_device_info_110_120.h"
#include "cl_device_info_200.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

enum DEVICE_INFO_INDEX
{
  PARAM_NAME       = 0,
  PARAM_DESC       = 1,
  PARAM_SIZE       = 2,
  PARAM_VALUE      = 3,
  PARAM_ERROR      = 4,
  PARAM_TOOL_TIP   = 5,
  PARAM_UNITS      = 6,
  PARAM_HTML_UNITS = 7
};

static const std::string sIndent2(2, ' ');

class ScopedNumericFormatter
{
  public:

    explicit ScopedNumericFormatter(std::ostream& tStream);

    virtual ~ScopedNumericFormatter();

    template<class T> ScopedNumericFormatter& operator<<(const T& rhs);

    ScopedNumericFormatter& operator<<(const cl_platform_id& rhs);

  protected:

    std::ostream& m_tStream;
    std::ios      m_tCurrentFormat;

    void restore();

  private:

    // Intentionally deleted.
    ScopedNumericFormatter()                                  = delete;
    ScopedNumericFormatter(const ScopedNumericFormatter& rhs) = delete;
    ScopedNumericFormatter(ScopedNumericFormatter&& rhs)      = delete;
    ScopedNumericFormatter&       operator=(const ScopedNumericFormatter& rhs) = delete;
    ScopedNumericFormatter&       operator=(ScopedNumericFormatter&& rhs) = delete;
    ScopedNumericFormatter*       operator&()                             = delete;
    const ScopedNumericFormatter* operator&() const                       = delete;
};

class ScopedOutputStream
{
  public:

    explicit ScopedOutputStream(std::ostream& tStream) noexcept;

    ~ScopedOutputStream() noexcept;

    inline operator std::ostream&() const noexcept
    {
      return (m_tStream);
    }

  private:

    std::ostream&         m_tStream;
    const std::streamsize m_nFieldWidth;

    // Intentionally unimplemented.
    ScopedOutputStream()                              = delete;
    ScopedOutputStream(const ScopedOutputStream& rhs) = delete;
    ScopedOutputStream(ScopedOutputStream&& rhs)      = delete;
    ScopedOutputStream&       operator=(const ScopedOutputStream& rhs) = delete;
    ScopedOutputStream&       operator=(ScopedOutputStream&& rhs) = delete;
    ScopedOutputStream*       operator&()                         = delete;
    const ScopedOutputStream* operator&() const                   = delete;
};

template<class CL_DEVICE_INFO, class T> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, unsigned long, T, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept;

template<class CL_DEVICE_INFO, class T> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<
    const CL_DEVICE_INFO,
    std::string,
    unsigned long,
    T,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>& tDeviceInfo) noexcept;

class DeviceInfo
{
  public:

    DeviceInfo(
      const cl_device_id   nDeviceID,
      const size_t         nDeviceNumber,
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nDeviceVersion) noexcept;

    virtual ~DeviceInfo() noexcept;

    void init();

    static void extractTokens(
      std::vector<std::string>& tTokens,
      const std::string&        sString,
      const char                cDelimiter = ' ') noexcept;

    template<class T> static void printVectorVertically(
      std::ostream&                           tStream,
      typename std::vector<T>::const_iterator pBegin,
      typename std::vector<T>::const_iterator pEnd,
      std::vector<T>&                         tVector,
      const std::string&                      sIndent = std::string("  ")) noexcept;

    template<class T> static void printVectorVertically(
      std::ostream&      tStream,
      std::vector<T>&    tVector,
      const std::string& sIndent = std::string("  ")) noexcept;

    friend std::ostream& operator<<(std::ostream& tStream, const DeviceInfo& rhs) noexcept;

    virtual void getTextOutput(std::ostream& tStream) const noexcept;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept;

    enum class OPEN_cl_bool : cl_uint
    {
      OPENCL_FALSE = CL_FALSE,
      OPENCL_TRUE  = CL_TRUE
    };

    inline unsigned short deviceVersion() const
    {
      return (m_nDeviceVersion);
    }

  protected:

    template<class CL_DEVICE_INFO, class T1, class T2> static void writeHTMLTokens(
      std::ostream& tStream,
      const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T1, OpenCLError, OpenCLToolTip>&
                         tDeviceInfo,
      T2                 nBegin,
      T2                 nEnd,
      const std::string& sNotFound) noexcept;

    template<class CL_DEVICE_INFO, class T1, class T2> static void writeHTMLTokens(
      std::ostream& tStream,
      const std::tuple<
        const CL_DEVICE_INFO,
        std::string,
        size_t,
        T1,
        OpenCLError,
        OpenCLToolTip,
        OpenCLUnits>&    tDeviceInfo,
      T2                 nBegin,
      T2                 nEnd,
      const std::string& sNotFound) noexcept;

    template<class CL_DEVICE_INFO, class T> void writeHTMLTokens(
      std::ostream& tStream,
      const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
        tDeviceInfo) noexcept;

    template<class CL_DEVICE_INFO, class T> void writeHTMLTokens(
      std::ostream& tStream,
      const std::tuple<
        const CL_DEVICE_INFO,
        std::string,
        size_t,
        T,
        OpenCLError,
        OpenCLToolTip,
        OpenCLUnits>& tDeviceInfo) noexcept;

    template<class CL_DEVICE_INFO, class T> static void writeHTMLTableRow(
      std::ostream& tStream,
      const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
        tDeviceInfo) noexcept;

    template<class CL_DEVICE_INFO, class T> static void writeHTMLTableRow(
      std::ostream& tStream,
      const std::tuple<
        const CL_DEVICE_INFO,
        std::string,
        size_t,
        T,
        OpenCLError,
        OpenCLToolTip,
        OpenCLUnits>& tDeviceInfo) noexcept;

    template<class CL_DEVICE_INFO, class T> static void writeHTMLTableRow(
      std::ostream& tStream,
      const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
                         tDeviceInfo,
      const std::string& sNotFound,
      const char         cDelimiter = ' ');

    template<class CL_DEVICE_INFO, class T> static void writeHTMLTableRow(
      std::ostream& tStream,
      const std::tuple<
        const CL_DEVICE_INFO,
        std::string,
        size_t,
        T,
        OpenCLError,
        OpenCLToolTip,
        OpenCLUnits>&    tDeviceInfo,
      const std::string& sNotFound,
      const char         cDelimiter = ' ');

    static void writePrecisionInfo(
      std::ostream&             tStream,
      const int                 nMaxDescriptionSize,
      std::vector<std::string>& tInfo,
      const std::string&        sIndent,
      const std::string&        sHorizontalIndent,
      const std::string&        sNotFound,
      const std::tuple<
        const OPEN_cl_device_info,
        std::string,
        size_t,
        cl_device_fp_config,
        OpenCLError,
        OpenCLToolTip>& tPrecisionInfo);

    static std::string tagError(const std::string& sError) noexcept;

    friend std::ostream& operator<<(std::ostream& tStream, const OPEN_cl_bool& e) noexcept
    {
      switch (e)
      {
        case OPEN_cl_bool::OPENCL_FALSE:
          tStream << "false";
          break;

        case OPEN_cl_bool::OPENCL_TRUE:
        default:
          tStream << "true";
          break;
      };
      return (tStream);
    }

    constexpr static const char* m_sCrossProduct = " × ";
    static const std::string     m_sHTMLCrossProduct;

    cl_device_id             m_nDeviceID;
    mutable int              m_nMaxDescriptionSize;
    const size_t             m_nDeviceNumber;
    cl_platform_id           m_tPlatformID;
    const size_t             m_nPlatformNumber;
    const unsigned short     m_nDeviceVersion;
    std::vector<std::string> m_tKernelErrors;
    const char*              m_sKernel;
    std::string              m_sKernelDeviceType;
    size_t                   m_nKernelDataSize;
    cl_ulong                 m_nKernelElapsedTime;

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tAddressBits;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tAvailable;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tCompilerAvailable;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_fp_config,
      OpenCLError,
      OpenCLToolTip>
      m_tDoublePrecisionFloatingPointConfiguration;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tIsLittleEndian;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tHasErrorCorrectionSupport;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_exec_capabilities,
      OpenCLError,
      OpenCLToolTip>
      m_tExecutionCapabilities;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceExtensions;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_ulong,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tGlobalMemoryCacheSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_mem_cache_type,
      OpenCLError,
      OpenCLToolTip>
      m_tGlobalMemoryCacheType;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tGlobalMemoryCachelineSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_ulong,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tGlobalMemorySize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tImageSupport;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax2DImageHeight;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax2DImageWidth;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax3DImageDepth;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax3DImageHeight;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax3DImageWidth;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_ulong,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tLocalMemoryAreaSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_local_mem_type,
      OpenCLError,
      OpenCLToolTip>
      m_tTypeOfLocalMemorySupported;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMaxConfiguredClockFrequency;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxParallelComputeUnits;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxConstantArgs;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_ulong,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMaxConstantBufferSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_ulong,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMaxMemoryObjectAllocation;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMaxParameterSize;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxSimultaneousImageObjectsRead;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxSamplers;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, size_t, OpenCLError, OpenCLToolTip>
      m_tMaxWorkGroupSize;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxWorkItemDimensions;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<size_t[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tMaxWorkItemSizes;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tLargestOpenCLDataTypeSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceName;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_platform_id,
      OpenCLError,
      OpenCLToolTip>
      m_tDevicePlatform;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredCharVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredShortVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredIntVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredLongVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredFloatVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredDoubleVectorWidth;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceProfile;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tProfilingTimerResolution;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_fp_config,
      OpenCLError,
      OpenCLToolTip>
      m_tSinglePrecisionFloatingPointCapability;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_type,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceType;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceVendor;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tDeviceVendorID;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDeviceVersion;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tDriverVersion;

    void initKernel();

    template<class CL_DEVICE_INFO>
    static std::string getCLError(const cl_int nStat, const CL_DEVICE_INFO tDeviceInfo);

    virtual void getAllDeviceInfo() noexcept;

    // For scalar parameters.
    template<class CL_DEVICE_INFO, class T> void getDeviceInfo(
      std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
        tDeviceInfo) noexcept;

    // For scalar parameters with units.
    template<class CL_DEVICE_INFO, class T> void getDeviceInfo(std::tuple<
                                                               const CL_DEVICE_INFO,
                                                               std::string,
                                                               size_t,
                                                               T,
                                                               OpenCLError,
                                                               OpenCLToolTip,
                                                               OpenCLUnits>& tDeviceInfo) noexcept;

    // For array parameters.
    template<class CL_DEVICE_INFO, class T>
    void getDeviceInfo(std::tuple<
                       const CL_DEVICE_INFO,
                       std::string,
                       size_t,
                       std::unique_ptr<T[]>,
                       OpenCLError,
                       OpenCLToolTip>& tDeviceInfo) noexcept;

    template<class CL_DEVICE_INFO> static void
    setError(const cl_int nStat, std::string& sError, const CL_DEVICE_INFO tDeviceInfo) noexcept;

    template<class T>
    static void printVectorHorizontally(std::ostream& tStream, std::vector<T>& tVector) noexcept;

    template<class T> static std::string toHex(const T n);

  private:

    void getTextKernel(
      std::ostream&             tStream,
      const std::string&        sHorizontalIndent,
      std::vector<std::string>& tInfo) const noexcept;

    void getHTMLKernel(std::ostream& tStream, std::vector<std::string>& tInfo) const noexcept;

    // Intentionally deleted.
    DeviceInfo()                      = delete;
    DeviceInfo(const DeviceInfo& rhs) = delete;
    DeviceInfo(DeviceInfo&& rhs)      = delete;
    DeviceInfo&       operator=(const DeviceInfo& rhs) = delete;
    DeviceInfo&       operator=(DeviceInfo&& rhs) = delete;
    DeviceInfo*       operator&()                 = delete;
    const DeviceInfo* operator&() const           = delete;
};

