
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# This makefile can be included to monitor the dynamic linker.                 #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef LD_DEBUG_MK_INCLUDE_GUARD # {
LD_DEBUG_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# Set the LD_DEBUG environment variable to one or more, comma separated list,  #
# of the standard keywords.                                                    #
#                                                                              #
#  - libs       display library search paths                                   #
#  - reloc      display relocation processing                                  #
#  - files      display progress for input file                                #
#  - symbols    display symbol table processing                                #
#               the output of this option, which traces symbol resolution, is  #
#               very voluminous                                                #
#  - bindings   display information about symbol binding                       #
#  - versions   display version dependencies                                   #
#  - all        all previous options combined                                  #
#  - statistics display relocation statistics                                  #
#  - unused     determine unused DSOs                                          #
#  - help       display this help message and exit                             #
#               if help is used, the dynamic linker shows help information     #
#               about LD_DEBUG and the specified binary executable and exits   #
#                                                                              #
# The PID of the process will be prepended to each line of the LD_DEBUG output.#
#                                                                              #
# For security reasons, LD_DEBUG is ignored in set-user-ID and set-group-ID    #
# applications.                                                                #
#                                                                              #
# LD_DEBUG works for both shared libraries loaded implicitly and for those     #
# loaded by dlopen().                                                          #
################################################################################

LD_DEBUG_EXE ?= $(EXE)

################################################################################
# To redirect out from from stderr to a file, use the LD_DEBUG_OUTPUT          #
# environment variable. The results file will be named $(LD_DEBUG_OUTPUT).PID. #
################################################################################
LD_DEBUG_OUTPUT=$(CURDIR)/ld_debug_output/$(notdir $(LD_DEBUG_EXE)).ld_debug
LD_DEBUG_OUTPUT_TXT=$(LD_DEBUG_OUTPUT).txt
LD_DEBUG_DIR=$(abspath $(dir $(LD_DEBUG_OUTPUT)))

PROFILER_OUTPUT_DIRS += $(dir $(LD_DEBUG_OUTPUT))

################################################################################
# Assigning a default value to LD_DEBUG and then ensuring that it is comma     #
# separated and sorted.                                                        #
################################################################################
LD_DEBUG ?= libs
LD_DEBUG:=$(sort $(shell echo $(LD_DEBUG) | sed 's/,/ /g'))
LD_DEBUG:=$(shell echo $(LD_DEBUG) | sed 's/ /,/g')

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=
LD_DEBUG_EXE_COMMAND=$(strip ./$(LD_DEBUG_EXE) $(RUN_OPTS))

LD_DEBUG_TARGETS=ld_debug ld_debug_children ld_debug_clean ld_debug_env ld_debug_help ld_debug_prep
.PHONY: $(LD_DEBUG_TARGETS)
LD_DEBUG_EXTERNAL_TARGETS=clean platform_info ld_debug_mk_help show_help vars print-% qprint-%
ifneq ($(filter $(LD_DEBUG_TARGETS) $(LD_DEBUG_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

################################################################################
# If FORCE_RECOMPILE is YES, then $(LD_DEBUG_CLEAN_TARGET) is a prerequisite   #
# and the executable under investigation is rebuilt.                           #
################################################################################
FORCE_RECOMPILE ?= YES

.PHONY: ld_debug_help
ld_debug_help:
	+@LD_DEBUG=help $(MAKE) 2>&1

.PHONY: ld_debug_env
ld_debug_env:
	@echo 'LD_DEBUG        = [$(LD_DEBUG)]'
	@echo 'LD_DEBUG_OUTPUT = [$(LD_DEBUG_OUTPUT)]'

################################################################################
# If the executable creates child processes, then there will be more than one  #
# LD_DEBUG_OUTPUT file. Use this function to delete those output files that    #
# are for child processes.                                                     #
# $(1) - executable name                                                       #
################################################################################
define delete_child_process_ld_debug_output
	@for FILE in $$(ls $(LD_DEBUG_OUTPUT).[0-9]* 2> /dev/null); \
   do                                                         \
     egrep -q "calling fini: ($(1)|./$(1)) \[0\]$$" $${FILE}; \
     if [[ 0 != $${?} ]];                                     \
     then                                                     \
       rm $${FILE};                                           \
     fi;                                                      \
   done
endef

################################################################################
# Retrieves the $(LD_DEBUG_OUTPUT).PID file.                                   #
################################################################################
define get_ld_debug_results_file
$$(ls -1tr $(LD_DEBUG_OUTPUT).[0-9]* 2> /dev/null | tail -1)
endef

################################################################################
# Adds information to the top of the LD_DEBUG results file.                    #
################################################################################
define add_ld_debug_preamble
  for LD_DEBUG_FILE in $(LD_DEBUG_OUTPUT).[0-9]*;                         \
  do                                                                      \
    sed -i "1iLD_DEBUG         = [$(LD_DEBUG)]\n"      $${LD_DEBUG_FILE}; \
    sed -i "1iLD_DEBUG_OUTPUT  = [$(LD_DEBUG_OUTPUT)]" $${LD_DEBUG_FILE}; \
    sed -i "1iDate/Time = [$(DATE_TIME)]"              $${LD_DEBUG_FILE}; \
    sed -i "1iCommand: ($(LD_DEBUG_EXE_COMMAND))"      $${LD_DEBUG_FILE}; \
  done
endef

.PHONY: ld_debug ld_debug_common ld_debug_children

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than clean.                                                            #
################################################################################
export LD_DEBUG_CLEAN_TARGET ?= clean

ifeq ($(FORCE_RECOMPILE),YES) # {
ld_debug_common: ld_debug_clean $(LD_DEBUG_CLEAN_TARGET) ld_debug_prep
	+@$(MAKE_NO_DIR) $(LD_DEBUG_EXE)
else # } {
ld_debug_common: ld_debug_clean ld_debug_prep
endif # }
	@mkdir -p $(LD_DEBUG_DIR)
	@LD_DEBUG=$(LD_DEBUG) LD_DEBUG_OUTPUT=$(LD_DEBUG_OUTPUT) $(LD_DEBUG_EXE_COMMAND) || :

ld_debug ld_debug_children: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
ld_debug: ld_debug_common
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] FORCE_RECOMPILE       = [$(FORCE_RECOMPILE)]'
	@echo '[$(@)] LD_DEBUG_CLEAN_TARGET = [$(LD_DEBUG_CLEAN_TARGET)]'
	@$(call delete_child_process_ld_debug_output,$(LD_DEBUG_EXE))
	@$(call add_ld_debug_preamble)
################################################################################
# Changing the file extension of the LD_DEBUG results file from the PID to     #
# ".txt".                                                                      #
################################################################################
	@mv $(call get_ld_debug_results_file) $(LD_DEBUG_OUTPUT_TXT)
	@$(call open_file_command,$(LD_DEBUG_OUTPUT_TXT),$(EDITOR),LD_DEBUG results file)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

ld_debug_children: ld_debug_common
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] FORCE_RECOMPILE       = [$(FORCE_RECOMPILE)]'
	@echo '[$(@)] LD_DEBUG_CLEAN_TARGET = [$(LD_DEBUG_CLEAN_TARGET)]'
	@$(call add_ld_debug_preamble)
	@echo "[$(@)] $(INFO_LABEL) [$$(ls -1 $(LD_DEBUG_OUTPUT).[0-9]* | wc -l)] LD_DEBUG output files in directory [$(LD_DEBUG_DIR)]."
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

.PHONY: ld_debug_prep
ld_debug_prep::

.PHONY: ld_debug_clean
ld_debug_clean:
	@rm -rf $(LD_DEBUG_DIR)

clean:: ld_debug_clean

.PHONY: ld_debug_mk_help
ld_debug_mk_help::
	@echo '$(MAKE) ld_debug_help → shows the LD_DEBUG help'
	@echo '$(MAKE) ld_debug → run the executable $(EXE) with LD_DEBUG, saving just the parent process output to [$(LD_DEBUG_OUTPUT).PID]'
	@echo '$(MAKE) ld_debug_children → run the executable $(EXE) with LD_DEBUG, saving both parent and child processes output to [$(LD_DEBUG_OUTPUT).PIDs]'

show_help:: ld_debug_mk_help

endif # }

endif # }

