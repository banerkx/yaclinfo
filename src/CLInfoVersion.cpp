
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "CLInfoVersion.h"

namespace CLINFO
{
  const char* clinfoVersion()
  {
    return (CLINFO_VERSION);
  }

  unsigned short clinfoMajorVersion()
  {
    return (CLINFO_MAJOR_VERSION);
  }

  unsigned short clinfoMinorVersion()
  {
    return (CLINFO_MINOR_VERSION);
  }

  unsigned short clinfoPatchVersion()
  {
    return (CLINFO_PATCH_VERSION);
  }
}  // namespace CLINFO

