
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# See:                                                                         #
#   https://www.gnu.org/software/make/manual/html_node/Include.html            #
#   https://www.gnu.org/software/make/manual/html_node/Special-Variables.html  #
#   https://www.gnu.org/software/make/manual/html_node/MAKEFILES-Variable.html#MAKEFILES-Variable
#                                                                              #
# >>>>>>>>>>>>>>>>>> How GNU make Includes Other Makefiles <<<<<<<<<<<<<<<<<<< #
#                                                                              #
# The include directive:                                                       #
#                                                                              #
#   include [filenames]                                                        #
#                                                                              #
# filenames can contain shell file name patterns. If filenames is empty,       #
# nothing is included and no error is printed.                                 #
#                                                                              #
# Extra spaces are allowed and ignored at the beginning of the line, but the   #
# first character must not be a tab or the value of .RECIPEPREFIX. If the line #
# begins with a tab or the value of .RECIPEPREFIX, it will be considered a     #
# recipe line. Whitespace is required between include and the file names, and  #
# between the file names (the include directive can specify more than one file #
# on the line). Extra whitespace is ignored there and at the end of the        #
# directive. A comment starting with ‘#’ is allowed at the end of the line. If #
# the file names contain any variable or function references, they are         #
# expanded.                                                                    #
#                                                                              #
# For example, if you have three .mk files, a.mk, b.mk, and c.mk, and $(bar)   #
# expands to "file1.mk file2.mk", then the following expression:               #
#                                                                              #
#   include foo *.mk $(bar)                                                    #
#                                                                              #
# is equivalent to:                                                            #
#                                                                              #
#   include foo a.mk b.mk c.mk file1.mk file2.mk                               #
#                                                                              #
# If the specified name does not start with a slash, and the file is not found #
# in the current directory, several other directories are searched in the      #
# following order:                                                             #
#                                                                              #
#   ◆ directories specified with the ‘-I’ or ‘--include-dir’ option            #
#   ◆ the following directories (if they exist) are searched in order          #
#       ★ prefix/include (usually /usr/local/include)                          #
#       ★ /usr/gnu/include                                                     #
#       ★ /usr/local/include                                                   #
#       ★ /usr/include                                                         #
#                                                                              #
# From experimenting with the GNU make special variable .INCLUDE_DIRS, which   #
# holds the directories that make searches for included makefiles, the         #
# precedence of the directories that make searches for included make files is: #
#                                                                              #
#   ★ directories specified with the -I,--include-dir option                   #
#   ★ /usr/include                                                             #
#   ★ /usr/gnu/include                                                         #
#   ★ /usr/local/include                                                       #
#   ★ /usr/include                                                             #
#                                                                              #
# so it seems that prefix/include is actually equal to /usr/include. In the    #
# context of executing a configure script (to build a package from source)     #
# prefix/include has relevance.                                                #
#                                                                              #
# Also from experimenting, $(CURDIR) has precedence over the directories in    #
# $(.INCLUDE_DIRS).                                                            #
#                                                                              #
# If you want make to simply ignore a makefile which does not exist with no    #
# error, use the -include directive instead of include:                        #
#                                                                              #
#   -include [filenames]                                                       #
#                                                                              #
# For compatibility with some other make implementations, sinclude is another  #
# name for the -include directive; sinclude is ignored by this makefile.       #
#                                                                              #
# Finally, GNU make's -f option can be used to specify makefiles:              #
#                                                                              #
#   make -f myfile.mk                                                          #
#                                                                              #
# If the environment variable MAKEFILES is defined, make considers its value   #
# as a list of names (separated by whitespace) of additional makefiles to be   #
# read before the others. This works much like the include directive: various  #
# directories are searched for those files. In addition, the default goal is   #
# never taken from one of these makefiles (or any makefile included by them)   #
# and it is not an error if the files listed in MAKEFILES are not found.       #
#                                                                              #
# Notes:                                                                       #
#                                                                              #
#  ◆ this makefile should be included only once in your project's makefile     #
#    include graph                                                             #
#                                                                              #
#  ◆ this makefile does not modify MAKEFILE_LIST is any way                    #
#                                                                              #
#  ◆ **DO NOT** modify MAKEFILE_LIST is any way                                #
#                                                                              #
#  ◆ **DO NOT** modify .INCLUDE_DIRS is any way                                #
#                                                                              #
#  ◆ **DO NOT** use any variable or function name used in this makefile in     #
#    any other makefile                                                        #
#                                                                              #
#  ◆ if .DEFAULT_GOAL has not been set, it is set it to all; otherwise,        #
#     .DEFAULT_GOAL would be the first target in this makefile                 #
#                                                                              #
#  ◆ **CAVEAT** generating the makefile includes graph is **very** slow!       #
#                                                                              #
#  ◆ **CAVEAT** no idea how any of this works with cmake, GNU autoconf, or     #
#    any other build system that generates makefiles                           #
#                                                                              #
# The default method used to generate the makefile includes graph is strace    #
# and this method is algorithmic. If the grep method should be used, set       #
# MAKEFILE_INCLUDES_GREP=1; the grep method is heuristic.                      #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef MAKEFILE_INCLUDES_GRAPH_MK_INCLUDE_GUARD # {
MAKEFILE_INCLUDES_GRAPH_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/vars_common.mk.  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/markdowns.mk.    #
################################################################################
ifeq ($(MARKDOWNS_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/markdowns.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

BROWSER ?= firefox
EDITOR  ?= vim +1

################################################################################
# If .DEFAULT_GOAL has not been set, we set it to all. Otherwise,              #
# .DEFAULT_GOAL would be the first target in this makefile.                    #
################################################################################
ifeq ($(.DEFAULT_GOAL),) # {
.DEFAULT_GOAL:=all
endif # }

################################################################################
# Red color for highlighted error messages.                                    #
################################################################################
ifndef ESCAPE # {
ifndef STOP_COLOR # {
ifndef RED # {
ifndef ERROR_LABEL # {
ifneq ($(MAKE_TERMOUT),) # {
ESCAPE=
STOP_COLOR =$(ESCAPE)[0m
RED        =$(ESCAPE)[0;31m
ERROR_LABEL:=$(RED)ERROR$(STOP_COLOR):
else # } {
ERROR_LABEL:=ERROR:
endif # }
endif # }
endif # }
endif # }
endif # }

################################################################################
# Prints the target and its prerequisites as graph edges.                      #
#                                                                              #
# $(1) = target: $(@)                                                          #
# $(2) = all prerequisite(s): $(^) (may also include $(|))                     #
#                                                                              #
# Example:                                                                     #
#   $(call makefile_includes_dependencies_info,$(@),$(^) $(|))                 #
#                                                                              #
# NOTE: The $(|) argument may be omitted if there are no order-only            #
#       prerequisites.                                                         #
# NOTE: Using the $(filter-out ...) function ensures that this function is     #
#       actually called from the recipe for a target that has no               #
#       prerequisites.                                                         #
################################################################################
define makefile_includes_dependencies_info
$(if $(filter-out ,$(2)),$(foreach prereq,$(2),echo '$(prereq)→$(1)';),echo '$(1)';)
endef

################################################################################
# Determines the index of $(1) in the list $(2). If $(1) is not in $(2), 0 is  #
# returned. Otherwise, the index of the first match is returned.               #
#                                                                              #
# $(1) - list item to find                                                     #
# $(2) - list of string tokens                                                 #
################################################################################
find_word_index=$(eval COUNTER=)$(strip $(eval INDEX=0)$(foreach tok,$(2),$(eval COUNTER:=$(COUNTER) x)$(if $(filter $(1),$(tok)),$(if $(filter 0,$(INDEX)),$(eval INDEX=$(words $(COUNTER)))))))$(INDEX)

################################################################################
# Gets the make include directories, which encompasses $(CURDIR), those        #
# directories specified on the make command line, and the standard GNU make    #
# include directories.                                                         #
#                                                                              #
# NOTE: The variable .INCLUDE_DIRS holds those directories specified on the    #
#       make command line and the standard GNU make include directories.       #
################################################################################
get_makefile_include_dirs=$(realpath $(call remove_dupes,$(CURDIR) $(.INCLUDE_DIRS)))

################################################################################
# Tries to determine the root makefile using the same precedence as GNU make   #
# does:                                                                        #
#   GNUmakefile                                                                #
#   makefile                                                                   #
#   Makefile                                                                   #
#                                                                              #
# NOTE: Not only must one of {GNUmakefile, makefile, Makefile} exist in the    #
#       current directory, but the root makefile must also be in               #
#       $(MAKEFILE_LIST).                                                      #
# NOTE: If none of {GNUmakefile, makefile, Makefile} exist in the current      #
#       directory, or not exist in $(MAKEFILE_LIST), the empty string is       #
#       returned.                                                              #
################################################################################
get_standard_root_makefile=$(eval GNU_MAKEFILE=$(firstword $(filter $(realpath $(wildcard GNUmakefile makefile Makefile)),$(realpath $(MAKEFILE_LIST)))))$(if $(GNU_MAKEFILE),$(GNU_MAKEFILE))

################################################################################
# This function determines the named makefiles specified to make. In this      #
# context, a named makefile means any makefile specified on the make command   #
# line by using any of the following (and equivalent) options:                 #
#                                                                              #
#   -f file, --file=file, --makefile=FILE                                      #
#                                                                              #
# This determination is made by processing the full output of ps for the       #
# current make process PID. To get the PID of the current make process, the    #
# $(shell ...) function is used to simply echo the parent PID of the process   #
# started by $(shell ...).                                                     #
#                                                                              #
# NOTE: It is assumed that ps is not restricted in such a way that a           #
#       process' command line arguments are not visible.                       #
################################################################################
get_named_makefiles_from_ps=$(eval MAKE_PID=$(shell echo $${PPID}))$(realpath $(shell ps --pid $(MAKE_PID) -f | sed 's/[^-]-f/\n-f/g;s/--file=/\n--file=/g;s/--makefile=/\n--makefile=/g' | grep -E "^(-f|--file=|--makefile=)" | sed 's/^-f *//;s/ .*$$//;s/^--file=//;s/^--makefile=//'))

################################################################################
# Removes the specified makefile(s) from the makefile list.                    #
#                                                                              #
# $(1) - makefiles to be removed                                               #
# $(2) - makefile list variable name                                           #
################################################################################
define remove_makefile
$(eval $(2):=$(filter-out $(1),$($(2))))
endef

MAKEFILE_GRAPH_PROJECT=$(if $(EXE),$(EXE),$(notdir $(CURDIR)))
MAKEFILE_INCLUDES=$(MAKEFILE_GRAPH_PROJECT)_makefile_includes_graph
MAKEFILE_INCLUDES_GRAPH_LOG ?= $(MAKEFILE_INCLUDES).log
MAKEFILE_INCLUDES_DOT_GRAPH_FILE=$(MAKEFILE_INCLUDES).$(DOT_FILE_EXTENSION)
MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE ?= $(IMAGE_PATH)/$(subst $(DOT_FILE_EXTENSION),$(DOT_IMAGE_EXTENSION),$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE))
MAKEFILE_INCLUDES_GRAPH=$(MAKEFILE_INCLUDES)_dot

DEPENDENCIES_TARGETS= $(MAKEFILE_INCLUDES)                 \
                      $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)  \
                      $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) \
                      makefile_graphs                      \
                      makefile_includes_graph_dot          \
                      unformatted_makefiles_graph

GREP_VERSION=$(call get_stdout_version_index,grep,--version,4)
STRACE_VERSION:=$(shell { strace -V 2> /dev/null || echo 'UNKNOWN'; } | head -1 | sed 's/^.*version //')

platform_info::
	@echo 'grep version   = [$(GREP_VERSION)]'
	@echo 'strace version = [$(STRACE_VERSION)]'

################################################################################
#   ◆ the following directories (if they exist) are searched in order          #
#       ★ prefix/include (usually /usr/local/include)                          #
# Using strace to generate the makefile includes graph. This works by:         #
#                                                                              #
#   ◆ running "make strace_no_op" under strace and saving the resulting        #
#     sequence of open and close system calls to a file                        #
#   ◆ examining the open and close system calls by treating each open call as  #
#     a "push" onto a stack and each close call as a "pop" from the stack      #
#   ◆ as the stack is traversed, the current open system call file is a node   #
#     from which an edge originates and ends on the close system call          #
#                                                                              #
#  ◆ **CAVEAT** does not work on CYGWIN!                                       #
################################################################################
ifndef MAKEFILE_INCLUDES_GREP # {

include $(PROJECT_ROOT)/data_structures.mk

################################################################################
# This target should not be called directly.                                   #
################################################################################
.PHONY: strace_no_op
strace_no_op:
	@:

################################################################################
# Prints the makefile includes edges and nodes.                                #
#                                                                              #
# $(1) - stack name                                                            #
# $(2) - strace output line, which must contain (open XOR close) system call   #
# $(3) - name of variable that holds that list of remaining makefiles to be    #
#        processed                                                             #
#                                                                              #
# The strace output line, $(2), is expected to resemble:                       #
#                                                                              #
#   open:/path/to/some/makefile/abc.mk                                         #
#                                                                              #
# or:                                                                          #
#                                                                              #
#   close:/path/to/some/makefile/abc.mk                                        #
#                                                                              #
# So the first to do is to extract the system call and the makefile name from  #
# $(2).                                                                        #
#                                                                              #
# Then we:                                                                     #
#                                                                              #
#   if (open == $(SYS_CALL))                                                   #
#   {                                                                          #
#     if (stack_size == 0)                                                     #
#     {                                                                        #
#       stack_push($(INC_MK_FILE))                                             #
#     }                                                                        #
#     else                                                                     #
#     {                                                                        #
#       print_edge($(FROM) --> $(INC_MK_FILE))                                 #
#       stack_push($(INC_MK_FILE))                                             #
#       remove_from_makefile_list($(FROM),$(INC_MK_FILE))                      #
#     }                                                                        #
#   }                                                                          #
#   else // close system call                                                  #
#   {                                                                          #
#     if (false == is_in_makefile_list($(INC_MK_FILE)))                        #
#     {                                                                        #
#       stack_void_pop()                                                       #
#     }                                                                        #
#     else                                                                     #
#     {                                                                        #
#       print_node($(INC_MK_FILE))                                             #
#       stack_pop()                                                            #
#       remove_from_makefile_list($(INC_MK_FILE))                              #
#     }                                                                        #
#   }                                                                          #
#                                                                              #
################################################################################
define process_inc_mk_node
$(eval SYS_CALL=$(firstword $(subst :, ,$(2))))
$(eval INC_MK_FILE=$(lastword $(subst :, ,$(2))))
$(if $(filter open,$(SYS_CALL)),                                                                                                                                  \
  $(if $(filter 0,$(call stack_size,$(1))),                                                                                                                       \
    $(call stack_push,$(1),$(INC_MK_FILE)),                                                                                                                       \
    $(eval FROM=$(call stack_peek,$(1)))echo '$(FROM)→$(INC_MK_FILE)';$(call stack_push,$(1),$(INC_MK_FILE))$(call remove_makefile,$(FROM) $(INC_MK_FILE),$(3))), \
$(if $(filter 0,$(call find_word_index,$(INC_MK_FILE),$($(3)))),                                                                                                  \
  $(call stack_void_pop,$(1)),                                                                                                                                    \
  echo '$(INC_MK_FILE)';$(call remove_makefile,$(call stack_pop,$(1)),$(3))))
endef

################################################################################
# This target should not be called directly.                                   #
################################################################################
.PHONY: makefile_includes_info
makefile_includes_info:
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,process_inc_mk_node,$(@))
else # } {
	@$(if $(wildcard $(MAKEFILE_LIST_STRACE)),,$(error [$(@)] $(ERROR_LABEL) The file [$(MAKEFILE_LIST_STRACE)] does not exist))
################################################################################
# The file $(MAKEFILE_LIST_STRACE) must exist **before** this target is made   #
# since the $(shell ...) function is the first recipe line that executes when  #
# this target is made.                                                         #
################################################################################
	@$(eval REMAINING_MAKEFILES=$(realpath $(ORIG_MAKEFILE_LIST)))
	@$(eval MAKE_INCLUDES=$(strip $(shell sed 's/^open /open:/;s/^close /close:/' $(MAKEFILE_LIST_STRACE))))
	@$(foreach i,$(MAKE_INCLUDES),$(call process_inc_mk_node,MAKEFILE_INCLUDES_STACK,$(i),REMAINING_MAKEFILES))
endif # }

################################################################################
# Checks certain attributes of the strace log file.                            #
#                                                                              #
# $(1) - strace log file                                                       #
# $(2) - message label                                                         #
#                                                                              #
# The following checks are made on the strace log file:                        #
#   - the log file name must be non-empty                                      #
#   - the log file must exist                                                  #
#   - the log file must be readable                                            #
#   - the first line of the log file must be an open system call               #
#   - the last line of the log file must be a close system call                #
#   - the log file must contain an even number of lines                        #
#   - the number of open system calls must equal the number of close system    #
#     calls                                                                    #
#   - the log file must not contain duplicate makefiles                        #
#   - the log file must contain only open and close systems calls              #
################################################################################
define check_strace_log_file
[[ -z "$(1)" ]] && { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file name is empty." >&2; exit 1; };                                                                      \
[[ ! -e "$(1)" ]] && { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] does not exist." >&2; exit 1; };                                                            \
[[ ! -r "$(1)" ]] && { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] is not readable." >&2; exit 1; };                                                           \
{ head -1 $(1) | grep -q "^open "; } || { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The first line of the strace log file [$(1)] does not contain an open system call." >&2; exit 1; }; \
{ tail -1 $(1) | grep -q "^close "; } || { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The last line of the strace log file [$(1)] does not contain a close system call." >&2; exit 1; }; \
[[ 0 != $$(($$(wc -l < $(1)) % 2)) ]] && { printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] contains an odd number of lines." >&2; exit 1; };                       \
OPEN_CALLS=$$(grep -c "^open " $(1));                                                                                                                                                    \
CLOSE_CALLS=$$(grep -c "^close " $(1));                                                                                                                                                  \
[[ $${OPEN_CALLS} == $${CLOSE_CALLS} ]] ||                                                                                                                                               \
{ printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] has [$${OPEN_CALLS}] open system calls != its number of close system calls [$${CLOSE_CALLS}]." >&2; exit 1; };   \
NON_SYS_CALLS=$$(egrep -cv "^(open|close) " $(1));                                                                                                                                       \
[[ 0 == $${NON_SYS_CALLS} ]] ||                                                                                                                                                          \
{ printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] has non-open/close system call line(s) {$$(egrep -v "^(open|close) " $(1) | tr '\n' ',' | sed 's/,$$//;s/,/, /g')}." >&2; exit 1; }; \
LINE_COUNT=$$(cat $(1) | wc -l);                                                                                                                                                         \
UNIQ_LINE_COUNT=$$(sort -u $(1) | wc -l);                                                                                                                                                \
[[ $${LINE_COUNT} == $${UNIQ_LINE_COUNT} ]] ||                                                                                                                                           \
{ printf "%s\n" "[$(0)::$(2)] $(ERROR_LABEL) The strace log file [$(1)] has the duplicate makefiles {$$(grep -v "close " $(1) | sort | uniq -c | grep -v " 1 " | sed 's@^.* @@' | tr '\n' ',' | sed 's@,$$@@;s@,@, @g')}." >&2; exit 1; }
endef

################################################################################
# This target should not be called directly.                                   #
################################################################################
STRACE_VERSION_THRESHOLD=4.24
.PHONY: umtree unformatted_makefiles_graph
umtree unformatted_makefiles_graph: DATESTAMP:=$(shell date +%m%d%Y_%H%M%S_%N)
umtree unformatted_makefiles_graph: ORIG_MAKEFILE_LIST:=$(if $(ORIG_MAKEFILE_LIST),$(ORIG_MAKEFILE_LIST),$(realpath $(MAKEFILE_LIST)))
umtree unformatted_makefiles_graph: MAKEFILE_LIST_STRACE ?= makefile_list_strace_$(DATESTAMP).log
umtree unformatted_makefiles_graph: SYSCALL_LIST=/proc/kallsyms
ifeq ($(call ver_greater_than,$(subst UNKNOWN,0.0,$(STRACE_VERSION)),$(STRACE_VERSION_THRESHOLD)),T) # {
umtree unformatted_makefiles_graph: SYSCALL_SET=close openat
umtree unformatted_makefiles_graph: DECODE_SET=--decode-fds=path
umtree unformatted_makefiles_graph: QUIET_SET=--quiet=exit,path-resolution
else # } {
umtree unformatted_makefiles_graph: QUIET_PATH_RESOLUTION= 2>&1 | { grep -v "strace: Requested path .* resolved into" || :; }
umtree unformatted_makefiles_graph: SYSCALL_SET=close open
umtree unformatted_makefiles_graph: DECODE_SET=-y
umtree unformatted_makefiles_graph: QUIET_SET=-qq
umtree unformatted_makefiles_graph: SIGNAL_SET=-e signal=none
endif # }
umtree unformatted_makefiles_graph: SYSCALL_SET:=$(if $(wildcard $(SYSCALL_LIST)),$(foreach sys,$(SYSCALL_SET),$(if $(shell grep " \(__x64_\)\?sys_$(sys)$$" $(SYSCALL_LIST)), $(sys),$(warning $(WARNING_LABEL) [$(sys)] is not a valid system call; fix the $$(SYSCALL_SET) variable.))),$(SYSCALL_SET))
umtree unformatted_makefiles_graph: SYSCALL_SET:=-e trace=$(subst $(escaped_space),$(escaped_comma),$(strip $(SYSCALL_SET)))
umtree unformatted_makefiles_graph:
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,check_strace_log_file,$(@))
	@$(call makefile_includes_dependencies_info,find_word_index,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,get_makefile_include_dirs,$(@))
	@$(call makefile_includes_dependencies_info,get_named_makefiles_from_ps,$(@))
	@$(call makefile_includes_dependencies_info,makefile_includes_info,$(@))
	@$(call makefile_includes_dependencies_info,remove_makefile,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,stack_peek,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,stack_pop,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,stack_push,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,stack_size,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,stack_void_pop,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,strace_no_op,$(@))
else # } {
	@$(if $(INCLUDE_DIRS),,$(eval INCLUDE_DIRS:=$(addprefix -I ,$(call get_makefile_include_dirs))))
	@$(if $(NAMED_MAKEFILES_OPTS),,$(eval NAMED_MAKEFILES_OPTS=$(addprefix --file=,$(call get_named_makefiles_from_ps))))
	@$(eval SYSCALL_PATHS=$(addprefix -P,$(ORIG_MAKEFILE_LIST)))
	@$(eval STRACE_COMMAND=strace $(SYSCALL_SET) $(DECODE_SET) $(SYSCALL_PATHS) $(SIGNAL_SET) $(QUIET_SET) -o $(MAKEFILE_LIST_STRACE))
	@$(if $(filter-out /dev/null,$(MAKEFILE_LIST_STRACE)),rm -f $(MAKEFILE_LIST_STRACE))
	+@$(STRACE_COMMAND) $(MAKE_NO_DIR_BUILT_IN_RULES) $(INCLUDE_DIRS) $(NAMED_MAKEFILES_OPTS) strace_no_op $(QUIET_PATH_RESOLUTION)
	@{ [[ -e $(MAKEFILE_LIST_STRACE) ]] &&                                                         \
   sed -i 's/\(^.*\)\((.*<\)\(.*\)\(>.*\)/\1 \3/;s/openat/open/' $(MAKEFILE_LIST_STRACE); } || :
	@$(call check_strace_log_file,$(MAKEFILE_LIST_STRACE),$(@))
	+@$(MAKE_NO_DIR_BUILT_IN_RULES) MAKEFILE_LIST_STRACE=$(MAKEFILE_LIST_STRACE) ORIG_MAKEFILE_LIST="$(ORIG_MAKEFILE_LIST)" $(NAMED_MAKEFILES_OPTS) makefile_includes_info | sort
ifdef STRACE_KEEP_LOG # {
	@echo '$(EDITOR) $(MAKEFILE_LIST_STRACE)' > $$(tty)
else # } {
	@rm -f $(MAKEFILE_LIST_STRACE)
endif # }
endif # }

.PHONY: mtree $(MAKEFILE_INCLUDES)
mtree $(MAKEFILE_INCLUDES): DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
mtree $(MAKEFILE_INCLUDES):
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,unformatted_makefiles_graph,$(MAKEFILE_INCLUDES))
	@$(call makefile_includes_dependencies_info,get_makefile_include_dirs,$(MAKEFILE_INCLUDES))
	@$(call makefile_includes_dependencies_info,get_named_makefiles_from_ps,$(MAKEFILE_INCLUDES))
else # } {
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(eval $(@)_START_TIME:=$(shell date +%s)))
	@$(eval INCLUDE_DIRS:=$(call get_makefile_include_dirs))
	@$(eval PS_NAMED_MAKEFILES=$(call get_named_makefiles_from_ps))
	@$(eval NAMED_MAKEFILES_OPTS=$(if $(PS_NAMED_MAKEFILES),$(addprefix --file=,$(PS_NAMED_MAKEFILES))))
	+@$(MAKE_NO_DIR_BUILT_IN_RULES) INCLUDE_DIRS="$(addprefix -I ,$(INCLUDE_DIRS))" ORIG_MAKEFILE_LIST="$(MAKEFILE_LIST)" NAMED_MAKEFILES_OPTS="$(NAMED_MAKEFILES_OPTS)" $(NAMED_MAKEFILES_OPTS) unformatted_makefiles_graph | \
   sed 's/→/ → /' | column -s" " -o" " -t | sed 's/ *$$//'         >| $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@echo -e '\nNodes = [$(words $(MAKEFILE_LIST))]'                 >> $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@echo "Edges = [$$(grep -c "→" $(MAKEFILE_INCLUDES_GRAPH_LOG))]" >> $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@sed -i '1 i\$(DATE_TIME)\n'                                        $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@$(if $(filter $(@),$(MAKECMDGOALS)),echo '$(EDITOR) $(MAKEFILE_INCLUDES_GRAPH_LOG)')
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$($(@)_START_TIME)))
endif # }

.PHONY: dependencies_graph
dependencies_graph: DEPENDENCIES_TARGETS += makefile_includes_info
dependencies_graph:
	+@$(MAKE) $(NO_DIR_BUILT_IN_RULES) -Otarget -s DEPENDENCIES_INFO=1 $(DEPENDENCIES_TARGETS)

else # } {

################################################################################
# Expands the input list of makefile, which may contain variable sin their     #
# paths, to their full paths.                                                  #
#                                                                              #
# $(1) - makefile search directories                                           #
# $(2) - input list of included makefiles (as found by grep)                   #
# $(3) - makefile nodes list                                                   #
# $(4) - makefile being processed                                              #
# $(5) - results variable name                                                 #
################################################################################
define expand_mkfile_names
################################################################################
# For a makefile to be included by $(MK_FILE), it must satisfy the following   #
# constraints:                                                                 #
#   - the makefile must exist                                                  #
#   - the makefile must be in the list $(MKFILE_NODES)                         #
#   - the makefile must be to the right of $(MK_FILE) in the list              #
#     $(MKFILE_NODES)                                                          #
################################################################################
$(eval ALL_INC_MKS_UNIQ=$(call remove_dupes,$(2)))
$(eval MKFILE_NODES=$(3))
################################################################################
# We get the index of $(MK_FILE) in the list $(MKFILE_NODES) and use it to get #
# the sub-list from this index to the end of $(MKFILE_NODES), removing all     #
# the makefiles to the left of $(MK_FILE).                                     #
################################################################################
$(eval CURR_MK_IDX:=$(call find_word_index,$(4),$(3)))
$(eval NORM_MK_NODES:=$(if $(filter-out 0,$(CURR_MK_IDX)),$(wordlist $(CURR_MK_IDX),$(words $(MKFILE_NODES)),$(MKFILE_NODES)),$(MKFILE_NODES)))
################################################################################
# For each item in $(ALL_INC_MKS_UNIQ), we:                                    #
#   - we check to see, using $(wildcard ...), if the included makefile         #
#     actually exists                                                          #
#   - if it does, we add it to $(RESOLVED_MKS) and remove it from              #
#     $(ALL_INC_MKS_UNIQ)                                                      #
#   - then we ensure that the makefiles in $(RESOLVED_MKS), are in fact,       #
#     actually present in $(NORM_MK_NODES)                                     #
################################################################################
$(foreach inc,$(ALL_INC_MKS_UNIQ),$(if $(wildcard $(inc)),$(eval RESOLVED_MKS += $(realpath $(wildcard $(inc))))$(eval ALL_INC_MKS_UNIQ:=$(filter-out $(inc),$(ALL_INC_MKS_UNIQ)))))
$(eval RESOLVED_MKS:=$(filter $(call remove_dupes,$(RESOLVED_MKS)),$(NORM_MK_NODES)))
################################################################################
# $(REMAINING_MKS) will hold those makefiles from $(ALL_INC_MKS_UNIQ) that do  #
# not have full paths.                                                         #
#                                                                              #
# For each file in $(REMAINING_MKS), we look in each directory in              #
# $(REMAINING_MKS) to see if the file exists. If it does, it is saved in       #
# $(FUL_PATH_MKS).                                                             #
################################################################################
$(if $(MK_SEARCH_DIRS),                                                                                                                                                   \
$(eval REMAINING_MKS:=$(notdir $(strip $(sort $(filter-out $(realpath $(ALL_INC_MKS_UNIQ)),$(ALL_INC_MKS_UNIQ))))))                                                       \
$(eval FUL_PATH_MKS:=$(call remove_dupes,$(strip $(if $(REMAINING_MKS),$(foreach mk,$(REMAINING_MKS),$(firstword $(wildcard $(addsuffix /$(mk),$(MK_SEARCH_DIRS))))))))))
################################################################################
# The included makefiles consist of $(RESOLVED_MKS) and those makefiles from   #
# $(FUL_PATH_MKS) that exist in $(NORM_MK_NODES).                              #
################################################################################
$(eval $(5)=$(sort $(RESOLVED_MKS) $(if $(FUL_PATH_MKS),$(strip $(filter $(FUL_PATH_MKS),$(NORM_MK_NODES))))))
endef

################################################################################
# For the input makefile, the makefiles it includes are written out as edges.  #
#                                                                              #
# $(1) - makefile                                                              #
# $(2) - makefile nodes list variable name                                     #
# $(3) - makefile search directories                                           #
# $(4) - name of the variable that will hold the graph edges                   #
# $(5) - any other needed make command line arguments (optional)               #
################################################################################
define process_inc_mk_node
################################################################################
# Finding the included makefiles by grepping (not all of the included          #
# makefiles may be actual included makefiles).                                 #
################################################################################
$(eval ALL_INC_MKS=$(filter-out include -include,$(strip $(shell grep "^[ \t]*-\?include[ \t]\+" $(1)))))
################################################################################
# Expanding the variables that may be in the name of the include makefile;     #
# this will yield the full names of the included makefiles. then adding the    #
# found edges to $(4) and then removing the current makefile being examined    #
# and its included makefiles from the list of the make file nodes.             #
################################################################################
$(if $(strip $($(2))),$(if $(ALL_INC_MKS),$(call expand_mkfile_names,$(3),$(ALL_INC_MKS),$($(2)),$(1),ALL_INC_MKS)),$(eval ALL_INC_MKS=))
$(if $(ALL_INC_MKS),$(eval $(4) += $(addprefix $(1)→,$(ALL_INC_MKS)))$(call remove_makefile,$(1) $(ALL_INC_MKS),$(2)))
endef

################################################################################
# Writes out the list of makefile includes graph edges to stdout.              #
#                                                                              #
# $(1) - list of makefiles                                                     #
# $(2) - any other needed make command line arguments (optional)               #
################################################################################
define cr8_makefiles_graph
################################################################################
# Getting the list of make include dirs (including the standard GNU make       #
# include directories).                                                        #
################################################################################
$(eval MK_SEARCH_DIRS=$(call get_makefile_include_dirs))
################################################################################
# MKFILE_NODES is set to the list of makefiles, i.e., $(MAKEFILE_LIST).        #
################################################################################
$(eval MKFILE_NODES=$(1))
################################################################################
# Looping through each makefile to get its included makefiles to build the     #
# graph.                                                                       #
################################################################################
$(foreach mk,$(MKFILE_NODES),$(call process_inc_mk_node,$(mk),MKFILE_NODES,$(MK_SEARCH_DIRS),GRAPH,$(2)))
$(eval GRAPH += $(MKFILE_NODES))
{ $(foreach g,$(sort $(GRAPH)),echo '$(g)';) }
endef

################################################################################
# Prints the edges and free vertices of the makefile includes graph to stdout. #
################################################################################
.PHONY: umtree unformatted_makefiles_graph
umtree unformatted_makefiles_graph:
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,cr8_makefiles_graph,$(@) get_makefile_include_dirs)
else # } {
	@$(eval PS_NAMED_MAKEFILES=$(call get_named_makefiles_from_ps))
	@$(eval NAMED_MAKEFILES_OPTS=$(if $(PS_NAMED_MAKEFILES),$(addprefix --file=,$(PS_NAMED_MAKEFILES))))
	@$(call cr8_makefiles_graph,$(strip $(realpath $(if ${ORIG_MAKEFILE_LIST},${ORIG_MAKEFILE_LIST},$(MAKEFILE_LIST)))),$(NAMED_MAKEFILES_OPTS))
endif # }

################################################################################
# Writes the edges and free vertices of the makefile includes graph, and the   #
# edge and vertices counts, and the date/time to                               #
# $(MAKEFILE_INCLUDES_GRAPH_LOG).                                              #
################################################################################
.PHONY: mtree $(MAKEFILE_INCLUDES)
mtree $(MAKEFILE_INCLUDES): DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
mtree $(MAKEFILE_INCLUDES):
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,cr8_makefiles_graph,$(MAKEFILE_INCLUDES))
	@$(call makefile_includes_dependencies_info,expand_mkfile_names,process_inc_mk_node)
	@$(call makefile_includes_dependencies_info,find_word_index,expand_mkfile_names)
	@$(call makefile_includes_dependencies_info,process_inc_mk_node,cr8_makefiles_graph)
	@$(call makefile_includes_dependencies_info,remove_dupes,expand_mkfile_names)
	@$(call makefile_includes_dependencies_info,remove_makefile,process_inc_mk_node)
else # } {
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(eval $(@)_START_TIME:=$(shell date +%s)))
	@$(eval PS_NAMED_MAKEFILES=$(call get_named_makefiles_from_ps))
	@$(eval NAMED_MAKEFILES_OPTS=$(if $(PS_NAMED_MAKEFILES),$(addprefix --file=,$(PS_NAMED_MAKEFILES))))
	@$(call cr8_makefiles_graph,$(strip $(realpath $(MAKEFILE_LIST))),$(NAMED_MAKEFILES_OPTS))         | \
    sed 's/→/ → /' | column -s" " -o" " -t | sed 's/ *$$//'        >| $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@echo -e '\nNodes = [$(words $(MAKEFILE_LIST))]'                 >> $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@echo "Edges = [$$(grep -c "→" $(MAKEFILE_INCLUDES_GRAPH_LOG))]" >> $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@sed -i '1 i\$(DATE_TIME)\n'                                        $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@$(if $(filter $(@),$(MAKECMDGOALS)),echo '$(EDITOR) $(MAKEFILE_INCLUDES_GRAPH_LOG)')
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$($(@)_START_TIME)))
endif # }

.PHONY: dependencies_graph
dependencies_graph: DEPENDENCIES_TARGETS += $(MAKEFILE_INCLUDES_GRAPH)
dependencies_graph:
	+@$(MAKE) $(NO_DIR_BUILT_IN_RULES) -Otarget -s DEPENDENCIES_INFO=1 $(DEPENDENCIES_TARGETS)

endif # }

################################################################################
# This target prints the dot file for the makefile includes graph. If          #
# $(DOT_OUTPUT_FILE) is defined, then the dot file will be saved to it.        #
################################################################################
.PHONY: mkdot makefile_includes_graph_dot
MAKEFILE_COUNT=$(words $(MAKEFILE_LIST))
mkdot makefile_includes_graph_dot: DOT_RANK_DIR=TB
mkdot makefile_includes_graph_dot: REDIRECTED_DOT_OUTPUT_FILE=$(if $(DOT_OUTPUT_FILE), >> $(DOT_OUTPUT_FILE))
mkdot makefile_includes_graph_dot: DATE_TIME=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
mkdot makefile_includes_graph_dot: DOT_MAKEFILE_GRAPH ?= $(if $(IMAGE_PATH),$(IMAGE_PATH)/)$(MAKEFILE_GRAPH_PROJECT)_makefiles.$(DOT_IMAGE_EXTENSION)
mkdot makefile_includes_graph_dot: DOT_COMMAND=$(call get_dot_command,$(DOT_MAKEFILE_GRAPH),$(subst $(DOT_IMAGE_EXTENSION),$(DOT_FILE_EXTENSION),$(DOT_MAKEFILE_GRAPH)))
mkdot makefile_includes_graph_dot: DOT_NODE_COLOR=color="blue";
mkdot makefile_includes_graph_dot: DOT_EDGE_COLOR=[color="red"];
mkdot makefile_includes_graph_dot: DOT_MIN_FONT_SIZE=20
mkdot makefile_includes_graph_dot: DOT_FONT_SIZE:=$(shell echo $$(($(MAKEFILE_COUNT) < $(DOT_MIN_FONT_SIZE) ? $(DOT_MIN_FONT_SIZE) : $(MAKEFILE_COUNT))))
mkdot makefile_includes_graph_dot: DOT_GRAPH_LABEL=<<FONT POINT-SIZE="$(DOT_FONT_SIZE)"> $(MAKEFILE_GRAPH_PROJECT) makefile Includes Graph (|$(DOT_ITAL_BEG)V$(DOT_ITAL_END)| = $(MAKEFILE_COUNT), |$(DOT_ITAL_BEG)E$(DOT_ITAL_END)| = 0) Date/Time: [$(DATE_TIME)] $(MAKE) version: [$(MAKE_VERSION)] dot version: [$(DOT_VERSION)] $(if $(GIT_ROOT),git root: [$(subst ${HOME},$${HOME},$(GIT_ROOT))]) $(if $(GIT_BRANCH),git branch: [$(GIT_BRANCH)]) </FONT>>;
mkdot makefile_includes_graph_dot:
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,unformatted_makefiles_graph,makefile_includes_graph_dot)
else # } {
################################################################################
# If the dot file already exists, it is deleted.                               #
################################################################################
	@$(if $(DOT_OUTPUT_FILE),rm -f $(DOT_OUTPUT_FILE))
################################################################################
# Creating the dot file header.                                                #
################################################################################
	@echo -e '\n// $(DOT_COMMAND)\n' $(REDIRECTED_DOT_OUTPUT_FILE)
	@SIZE=$$(($(MAKEFILE_COUNT) * 3 ));                                      \
   echo 'digraph "Makefile Includes Graph"' $(REDIRECTED_DOT_OUTPUT_FILE); \
   echo '{'                                 $(REDIRECTED_DOT_OUTPUT_FILE); \
   echo '  rankdir="$(DOT_RANK_DIR)";'      $(REDIRECTED_DOT_OUTPUT_FILE); \
   echo "  size=\"$${SIZE},$${SIZE}\";"     $(REDIRECTED_DOT_OUTPUT_FILE); \
   echo '  bgcolor="#C6CFD532";'            $(REDIRECTED_DOT_OUTPUT_FILE); \
   echo '  splines="$(DOT_SPLINE)";'        $(REDIRECTED_DOT_OUTPUT_FILE)
################################################################################
# Writing the vertices of the graph.                                           #
################################################################################
	@$(foreach make,$(sort $(realpath $(MAKEFILE_LIST))),echo '  "$(notdir $(make))"[$(DOT_NODE_COLOR)label=<$(subst ${HOME},$${HOME},$(make))>];' $(REDIRECTED_DOT_OUTPUT_FILE);)
################################################################################
# Writing the edges of the graph.                                              #
################################################################################
	+@typeset -i EDGE_COUNT=0;                                                                                                             \
    export ORIG_MAKEFILE_LIST="$(if ${ORIG_MAKEFILE_LIST},${ORIG_MAKEFILE_LIST},$(MAKEFILE_LIST))";                                      \
    while read EDGE;                                                                                                                     \
    do                                                                                                                                   \
      echo $${EDGE} | sed 's@\(^.*\)\(→\)\(.*\)@  "\1" -> "\3"$(DOT_EDGE_COLOR)@'                        $(REDIRECTED_DOT_OUTPUT_FILE);  \
      EDGE_COUNT=$$((EDGE_COUNT + 1));                                                                                                   \
    done < <($(MAKE_NO_DIR_BUILT_IN_RULES) unformatted_makefiles_graph | grep '→' | sed 's@\(^.*/\)\(.*\)→\(.*/\)\(.*\)@\2→\4@' | sort); \
   echo '  labelloc="t";' $(REDIRECTED_DOT_OUTPUT_FILE);                                                                                 \
   echo '  label=$(DOT_GRAPH_LABEL)' | sed "s@E$(DOT_ITAL_END)| = 0@E$(DOT_ITAL_END)| = $${EDGE_COUNT}@" $(REDIRECTED_DOT_OUTPUT_FILE);  \
   echo '}'                                                                                              $(REDIRECTED_DOT_OUTPUT_FILE)
endif # }

################################################################################
# Creates the makefile includes graph dot file and saves it to                 #
# $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE).                                         #
################################################################################
.PHONY: makefiles_dot_file $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
.INTERMEDIATE: $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
makefiles_dot_file $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE): DOT_OUTPUT_FILE=$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
makefiles_dot_file $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE): ORIG_MAKEFILE_LIST=$(MAKEFILE_LIST)
makefiles_dot_file $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE): makefile_includes_graph_dot
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE),$(^))
else # } {
	@$(if $(add_node_degrees),$(call add_node_degrees,$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)))
	@$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE),$(EDITOR),makefile includes graph dot file)
	@echo ''
endif # }

################################################################################
# Creates the makefile includes graph image file and saves it to               #
# $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE).                                        #
################################################################################
.PHONY: mdgraph $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)
mdgraph $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE): $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE),$(^))
else # } {
	@rm -f $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)
	@$(DOT) -T$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER) -o $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
	@$(call xml_validate_and_format,$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE))
endif # }
	@$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE),$(BROWSER),makefile includes graph)
	@echo ''
endif # }

################################################################################
# Creates the makefile includes graph image file in the current directory.     #
################################################################################
.PHONY: mgraph $(MAKEFILE_INCLUDES_GRAPH)
MAKEFILE_INCLUDES_GRAPH_FILE=$(MAKEFILE_INCLUDES).$(DOT_IMAGE_EXTENSION)
mgraph $(MAKEFILE_INCLUDES_GRAPH): MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE:=$(notdir $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE))
mgraph $(MAKEFILE_INCLUDES_GRAPH): $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,$(MAKEFILE_INCLUDES_GRAPH),$(^))
else # } {
	@$(DOT) -T$(DOT_IMAGE_EXTENSION)$(DOT_SVG_RENDERER) -o $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)
ifeq ($(DOT_IMAGE_EXTENSION),svg) # {
	@$(call xml_validate_and_format,$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE))
endif # }
	@$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE),$(BROWSER),makefile includes graph )
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))
endif # }

################################################################################
# Writes the edges and free vertices of the makefile includes graph, and the   #
# edge and vertices counts, and the date/time to                               #
# $(MAKEFILE_INCLUDES_GRAPH_LOG) and creates the makefile includes graph image #
# file in the current directory.                                               #
################################################################################
.PHONY: mgraphs makefile_graphs
mgraphs makefile_graphs: MAKEFILES_DOT_GRAPH=$(MAKEFILE_INCLUDES_GRAPH_FILE)
mgraphs makefile_graphs: $(MAKEFILE_INCLUDES) $(MAKEFILE_INCLUDES_GRAPH)
ifdef DEPENDENCIES_INFO # {
	@$(call makefile_includes_dependencies_info,makefile_graphs,$(^))
else # } {
	@echo '$(EDITOR) $(MAKEFILE_INCLUDES_GRAPH_LOG)'
	@echo '$(BROWSER) $(MAKEFILES_DOT_GRAPH)'
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))
endif # }

################################################################################
# Writes the makefile include order constraints graph log to stdout.           #
################################################################################
.PHONY: mc makefile_include_order_constraints
mc makefile_include_order_constraints:
################################################################################
# To avoid makefile syntax errors, using hex equivalents for '(' and ')':      #
#                                                                              #
#   '(' = \x28                                                                 #
#   ')' = \x29                                                                 #
#                                                                              #
# in the following grep and sed commands.                                      #
################################################################################
	@$(eval MAKE_VARS:=$(sort $(shell grep --no-filename "^\$$.error.*Must include" $(MAKEFILE_LIST) | sed 's/\$$.error \$$\x28ERROR_LABEL\x29//;s/\$$\x28THIS_MAKE_FILE\x29//;s/\$$\x28/\n$$\x28/' | grep  -P '^\$$\x28' | sed 's/\x29.*$$//;s/^\$$\x28//')))
	@$(foreach var,$(MAKE_VARS),$(eval SED_COMMAND += $(if $(SED_COMMAND),;s@\x28$(var)\x29@$($(var))@,| sed "s@\x28$(var)\x29@$($(var))@)))
	@$(if $(SED_COMMAND),$(eval SED_COMMAND:=$(SED_COMMAND)"))
	@for mk_file in $(MAKEFILE_LIST);                                                       \
   do                                                                                     \
   grep -q "^\$$(error.*Must include" $${mk_file};                                        \
   if [[ 0 == $${?} ]];                                                                   \
   then                                                                                   \
     grep "^\$$(error.*Must include" $${mk_file}                                        | \
     sed "s@\$$(THIS_MAKE_FILE)@$${mk_file}@"                                           | \
     sed "s@\(^.*include \[\)\(.*\)\(\] before including.* \[\)\(.*\)\(\])$$\)@\2→\4@";   \
   fi;                                                                                    \
  done | sort $(SED_COMMAND) | sed "s@${HOME}@\$${HOME}@g" | sed 's/^\$$//'

################################################################################
# Creates a dot graph file from the input graph log file.                      #
#                                                                              #
# $(1) - input graph log file (must have file extension ".log")                #
# $(2) - dot output file name (must have file extension ".dot")                #
# $(3) - graph label (optional)                                                #
# $(4) - dot node color (optional)                                             #
# $(5) - dot edge color (optional)                                             #
################################################################################
define graph_log_2_dot
$(if $(wildcard $(1)),,$(error [$(0)] $(ERROR_LABEL) The graph log file [$(1)] does not exist))
$(if $(filter .log,$(call get_file_extension,$(1))),,$(error [$(0)] $(ERROR_LABEL) The graph log file [$(1)] does not have the file extension [.log]))
$(if $(filter .dot,$(call get_file_extension,$(2))),,$(error [$(0)] $(ERROR_LABEL) The dot output file [$(2)] does not have the file extension [.dot]))
$(eval FILE_STEM=$(subst .log,,$(1)))
$(eval GRAPH_IMAGE_FILE=$(FILE_STEM).$(DOT_IMAGE_EXTENSION))
$(eval DATE_TIME:=$(shell rm -f $(2) $(GRAPH_IMAGE_FILE); date "+%a %b %d, %Y %I:%M:%S %p %Z"))
$(eval DOT_COMMAND=$(call get_dot_command,$(GRAPH_IMAGE_FILE),$(2)))
$(eval COUNTS:=$(shell egrep "^(Nodes|Edges) = \[" $(1) | sed 's/^.*\[//;s/\]//'))
$(if $(filter-out 2,$(words $(COUNTS))),$(error [$(0)] $(ERROR_LABEL) Could not retrieve the node/edge counts from graph file [$(1)]))
$(eval NODE_COUNT:=$(firstword $(COUNTS)))
$(eval EDGE_COUNT:=$(lastword $(COUNTS)))
$(eval SIZE=$(shell expr $(NODE_COUNT) \* 3))
$(eval GRAPH_LABEL:=$(if $(3),$(3),$(subst ${HOME},$$$${HOME},$(realpath $(1)))))
echo -e "\n// $(DOT_COMMAND)\n"       >> $(2);
echo 'digraph "$(GRAPH_LABEL) Graph"' >> $(2);
echo '{'                              >> $(2);
echo '  rankdir="$(DOT_RANK_DIR)";'   >> $(2);
echo '  size="$(SIZE),$(SIZE)";'      >> $(2);
echo '  bgcolor="#C6CFD532";'         >> $(2);
echo '  splines="$(DOT_SPLINE)";'     >> $(2);
$(eval node_color=$(if $(4),$(4),$(DOT_NODE_COLOR)))
$(eval NODES=$(sort $(shell sed  '1,2d' $(1) | egrep -v "^($$|Nodes|Edges)" | sed 's/ //g;s/→/\n/')))
$(foreach node,$(NODES),echo '  "$(notdir $(node))"[$(node_color)label=<$(node) ($(DEG_MINUS) = $(call get_node_degree_in,$(node),$(1),→), $(DEG_PLUS) = $(call get_node_degree_out,$(node),$(1),→))>];' >> $(2);)
$(eval edge_color=$(if $(5),$(5),$(DOT_EDGE_COLOR)))
$(eval EDGES=$(shell sed  '1,2d' $(1) | egrep -v "^(Nodes|Edges) = " | sed "s@\$${HOME}@${HOME}@g;s@ @@g" | grep "→"))
$(foreach edge,$(EDGES),$(eval from=$(notdir $(firstword $(subst →, ,$(edge)))))$(eval to=$(notdir $(lastword $(subst →, ,$(edge)))))echo '  "$(from)" -> "$(to)"$(edge_color)' >> $(2);)
echo '  labelloc="t";' >> $(2);
$(eval DOT_GRAPH_LABEL=<<FONT POINT-SIZE="$(DOT_FONT_SIZE)"> $(GRAPH_LABEL) Graph (|$(DOT_ITAL_BEG)V$(DOT_ITAL_END)| = $(NODE_COUNT), |$(DOT_ITAL_BEG)E$(DOT_ITAL_END)| = $(EDGE_COUNT)) Date/Time: [$(DATE_TIME)] make version: [$(MAKE_VERSION)] dot version: [$(DOT_VERSION)] git root: [$(GIT_ROOT)] git branch: [$(GIT_BRANCH)] </FONT>>;)
echo '  label=$(DOT_GRAPH_LABEL)' | sed "s@${HOME}@\$${HOME}@g" >> $(2);
echo '}'                                                        >> $(2);
sed -i "s@${HOME}@\$${HOME}@g"                                     $(2);
endef

################################################################################
# Writes the edges of the makefile include order constraints graph, and the    #
# edge and vertices counts, and the date/time to                               #
# $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_GRAPH_LOG).                             #
################################################################################
.PHONY: mclog makefile_include_order_constraints_log
MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG ?= $(MAKEFILE_GRAPH_PROJECT)_makefile_include_order_constraints.log
mclog makefile_include_order_constraints_log: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
mclog makefile_include_order_constraints_log:
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(eval $(@)_START_TIME:=$(shell date +%s)))
	@$(MAKE) $(NO_DIR) makefile_include_order_constraints           | \
    sed 's/→/ → /' | column -s" " -o" " -t | sed 's/ *$$//'                         >| $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)
	@NODE_COUNT=$$(sed 's/ *→ */\n/' $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG) | sort -u | wc -l); \
   echo -e "\nNodes = [$${NODE_COUNT}]"                                             >> $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)
	@echo "Edges = [$$(grep -c "→" $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG))]"       >> $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)
	@sed -i '1 i\$(DATE_TIME)\n'                                                         $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG),$(EDITOR),makefiles include order constraints log file))
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$($(@)_START_TIME)))

################################################################################
# Creates the dot file $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT) from the      #
# graph log file $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG).                    #
################################################################################
.PHONY: mcgraph makefile_include_order_constraints_dot
MAKEFILE_INCLUDES_CONSTRAINTS_LABEL=$(MAKEFILE_GRAPH_PROJECT) Makefile Includes Constraints
MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT ?= $(subst .log,.dot,$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG))
mcdot makefile_include_order_constraints_dot: DOT_RANK_DIR=TB
mcdot makefile_include_order_constraints_dot: makefile_include_order_constraints_log
	@$(call graph_log_2_dot,$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT),$(MAKEFILE_INCLUDES_CONSTRAINTS_LABEL))
	@sed -i 's@\(^ *"\)\(.*\)\(" -> "\)\(.*\)\("\[\)\(.*$$\)@\1\2\3\4\5xlabel="include \2 before including \4";fontsize="12";\6@' $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT)
	@rm -f $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT),$(EDITOR),makefiles include order constraints dot file))
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))

################################################################################
# Creates the dot image file $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE) from  #
# the graph log dot file $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT).            #
################################################################################
.PHONY: mcgraph makefile_include_order_constraints_dot_graph
MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE ?= $(subst .log,.$(DOT_IMAGE_EXTENSION),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG))
mcgraph makefile_include_order_constraints_graph: makefile_include_order_constraints_dot
	@$(call get_dot_command,$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT))
	@rm -f $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT)
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call open_sole_target_file,$(@),$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE),$(BROWSER),makefiles include order constraints graph))
	@$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME)))

.PHONY: makefile_include_order_constraints_clean
makefile_include_order_constraints_clean:
	@rm -f $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG) $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT) $(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE)

.PHONY: makefile_includes_clean
makefile_includes_clean:
	@rm -f $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE) $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)
	@rm -f $(MAKEFILE_INCLUDES_GRAPH_FILE) $(MAKEFILE_INCLUDES_GRAPH_LOG)
	@rm -f $(notdir $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)) makefile_list_strace*.log

################################################################################
# Creates the makefile includes methods dependencies graph.                    #
#                                                                              #
# $(1) - calling target name                                                   #
################################################################################
define cr8_method_dependencies_graph
$(MAKE) $(NO_DIR_BUILT_IN_RULES_VARS) $(if $(findstring grep,$(1)),MAKEFILE_INCLUDES_GREP=1) dependencies_graph | column -s"→" -o"→" -t | sed 's/→/ → /' | sort >| $(1).log; \
echo -e "\nNodes = [$$(cat $(1).log | sed 's/ *→ */\n/' | sort -u | wc -l)]" >> $(1).log;                                                                                    \
echo "Edges = [$$(grep -c ' *→ *' $(1).log)]" >> $(1).log;                                                                                                                   \
sed -i "s@${HOME}@\$${HOME}@" $(1).log;                                                                                                                                      \
sed -i "1 i$$(date)\\n" $(1).log
endef

USER_DEFINED_FUNCTION_NODES=check_strace_log_file       \
                            cr8_makefiles_graph         \
                            expand_mkfile_names         \
                            find_word_index             \
                            get_makefile_include_dirs   \
                            get_named_makefiles_from_ps \
                            process_inc_mk_node         \
                            remove_dupes                \
                            remove_makefile             \
                            stack_peek                  \
                            stack_pop                   \
                            stack_push                  \
                            stack_size                  \
                            stack_void_pop

################################################################################
# Post processes the input dot file.                                           #
#                                                                              #
# $(1) - dot file                                                              #
################################################################################
define post_process_dot_dependencies_file
$(foreach node,$(USER_DEFINED_FUNCTION_NODES),sed -i 's@^  "$(node)"\[@  "$(node)"[style=dashed;@' $(1);)
sed -i 's@size=.*;@size="20,20";@'                                                                                    $(1);
sed -i '/^  splines="$(DOT_SPLINE)";$$/a \ \ graph[fontname="$(DOT_GRAPH_FONT)";colorscheme="$(DOT_COLOR_SCHEME)";];' $(1);
sed -i '/^  splines="$(DOT_SPLINE)";$$/a \ \ node[fontname="$(DOT_NODE_FONT)";];'                                     $(1);
sed -i '/^  splines="$(DOT_SPLINE)";$$/a \ \ edge[fontname="$(DOT_EDGE_FONT)";];'                                     $(1);
rm -f $(subst .dot,.log,$(1));
endef

################################################################################
# Creates targets for making makefile includes method dependencies graph log,  #
# dot file, and graph svg image files.                                         #
#                                                                              #
# $(1) - makefile includes method dependencies target                          #
# $(2) - graph label                                                           #
################################################################################
define cr8_makefile_includes_method_graph_targets
.PHONY: cr8_$(subst _graph,_log,$(1))
cr8_$(subst _graph,_log,$(1)):
	+@$$(call cr8_method_dependencies_graph,$(1))

.PHONY: $(1)
$(1): DOT_RANK_DIR=LR
$(1): cr8_$(subst _graph,_log,$(1))
	@$$(call graph_log_2_dot,$$(@).log,$$(@).$(DOT_FILE_EXTENSION),$(2),color="blue";penwidth="$(DOT_PEN_WIDTH)";)
	@$$(call post_process_dot_dependencies_file,$$(@).dot)
	@$$(call get_dot_command,$$(@).$(DOT_IMAGE_EXTENSION),$$(@).$(DOT_FILE_EXTENSION))
	@rm -f $$(@).dot
endef
GREP_METHOD_DEPENDENCIES_GRAPH=grep_method_dependencies_graph
STRACE_METHOD_DEPENDENCIES_GRAPH=strace_method_dependencies_graph
$(eval $(call cr8_makefile_includes_method_graph_targets,$(GREP_METHOD_DEPENDENCIES_GRAPH),grep Method Pseudo-Dependencies))
$(eval $(call cr8_makefile_includes_method_graph_targets,$(STRACE_METHOD_DEPENDENCIES_GRAPH),strace Method Pseudo-Dependencies))

clean:: makefile_includes_clean
	@rm -f $(addprefix $(GREP_METHOD_DEPENDENCIES_GRAPH),.dot .svg) $(addprefix $(STRACE_METHOD_DEPENDENCIES_GRAPH),.dot .svg)

.PHONY: makefile_includes_graph_mk_help
makefile_includes_graph_mk_help:
	@echo '$(MAKE) $(MAKEFILE_INCLUDES) → saves the makefile includes graph to the text file [$(MAKEFILE_INCLUDES_GRAPH_LOG)]'
	@echo '$(MAKE) $(MAKEFILE_INCLUDES_DOT_GRAPH_FILE) → writes makefile includes graph dot file to [$(MAKEFILE_INCLUDES_DOT_GRAPH_FILE)]'
	@echo '$(MAKE) $(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE) → write the makefile includes graph image file [$(MAKEFILE_INCLUDES_DOT_GRAPH_IMAGE)]'
	@echo '$(MAKE) $(MAKEFILE_INCLUDES_GRAPH) → writes the makefile includes graph image file in the current directory'
	@echo '$(MAKE) makefile_graphs → makes both the $(MAKEFILE_INCLUDES) and $(MAKEFILE_INCLUDES_GRAPH) targets'
	@echo '$(MAKE) makefile_include_order_constraints_dot → creates the makefile include order constraints dot file [$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_DOT)]'
	@echo '$(MAKE) makefile_include_order_constraints_graph → creates the makefile include order constraints image file [$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_IMAGE)]'
	@echo '$(MAKE) makefile_include_order_constraints_log → creates the makefile include order constraints graph log file [$(MAKEFILE_INCLUDE_ORDER_CONSTRAINTS_LOG)]'
	@echo '$(MAKE) makefile_includes_graph_dot → prints the dot file for the makefile includes graph'
	@echo '$(MAKE) unformatted_makefiles_graph → prints the makefile includes graph to stdout in a simple format'

show_help:: makefile_includes_graph_mk_help

endif # }

