
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ScopedDynamicLibrary.h"

#include <sstream>

ScopedDynamicLibrary::ScopedDynamicLibrary(const std::string& sSharedLibrary, const int nFlag) :
  m_sSharedLibrary(sSharedLibrary), m_nFlag(nFlag), m_sError(), m_pLibraryHandle(nullptr)
{
  if ((0 == (RTLD_LAZY & m_nFlag)) && (0 == (RTLD_NOW & m_nFlag)))
  {
    std::stringstream tError;
    tError << "ERROR: For shared object [" << m_sSharedLibrary << "], neither [" << RTLD_LAZY
           << "] nor [" << RTLD_NOW << "] is present in the open flag [" << m_nFlag << "].";
    m_sError = tError.str();
  }
  else
  {
    const int nBadFlag = m_nFlag & (~RTLD_LAZY) & (~RTLD_NOW) & (~RTLD_GLOBAL) & (~RTLD_LOCAL) &
                         (~RTLD_NODELETE) & (~RTLD_NOLOAD) & (~RTLD_DEEPBIND);

    if (0 != nBadFlag)
    {
      std::stringstream tError;
      tError << "ERROR: For shared object [" << m_sSharedLibrary << "], the open flag [" << m_nFlag
             << "] contains the invalid value [" << nBadFlag << "].";
      m_sError = tError.str();
    }
    else
    {
      (void) dlerror();  // Get rid of any current errors.
      m_pLibraryHandle.reset(dlopen(m_sSharedLibrary.c_str(), m_nFlag), dlclose);
      if (nullptr == m_pLibraryHandle)
      {
        const char* pError = dlerror();
        if (nullptr != pError)
        {
          std::stringstream tError;
          tError << "ERROR: For shared object [" << m_sSharedLibrary << "] " << pError << '.';
          m_sError = tError.str();
        }
      }
    }
  }
}

ScopedDynamicLibrary::~ScopedDynamicLibrary()
{
}

