
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GCOV_MK_INCLUDE_GUARD # {
GCOV_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# See: https://gcc.gnu.org/onlinedocs/gcc/Gcov.html.                           #
################################################################################

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=
GCOV_EXE ?= $(EXE)
GCOV_EXE_COMMAND=$(strip ./$(GCOV_EXE) $(RUN_OPTS))

GCOV_COMMAND=gcov
GCOV_OUTPUT=$(CURDIR)/gcov_output
LCOV_OUTPUT=$(GCOV_OUTPUT)/lcov_output
GENHTML_INDEX=$(LCOV_OUTPUT)/$(EXE)_gcov_index.html

PROFILER_OUTPUT_DIRS += $(GCOV_OUTPUT)

GCOV_TARGETS=gcov gcov_clean gcov_prep
.PHONY: $(GCOV_TARGETS)
GCOV_EXTERNAL_TARGETS=clean gcov_mk_help platform_info show_help vars print-% qprint-%
ifneq ($(filter $(GCOV_TARGETS) $(GCOV_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

ifneq ($(CXX),g++) # {
ifneq ($(filter $(GCOV_TARGETS),$(MAKECMDGOALS)),) # {
$(error $(ERROR_LABEL) [$(GCOV_COMMAND)] can only be used with [g++], not with [$(CXX)])
endif # }
endif # }

GCOV_LOG=$(GCOV_OUTPUT)/$(GCOV_EXE)_gcov.log
LCOV_INFO_FILE=$(GCOV_OUTPUT)/$(GCOV_EXE)_lcov_coverage.lcov

################################################################################
# Relevant target(s):                                                          #
#                                                                              #
#   make gcov --> performs a gcov run and creates the gcov analysis output     #
#                                                                              #
# NOTE:                                                                        #
#                                                                              #
#   - If the value of $(GCOV_OPTS) should be changed from the default (see     #
#     below), then $(GCOV_OPTS) must be specified **before** including this    #
#     makefile.                                                                #
#   - If running gcov succeeds, then the output will be saved in the directory #
#     $(GCOV_OUTPUT).                                                          #
################################################################################

GCOV_OPTS ?= --all-blocks                      \
             --branch-probabilities            \
             --unconditional-branches          \
             --branch-counts                   \
             --long-file-names                 \
             --preserve-paths                  \
             --function-summaries              \
             --display-progress                \
             --demangled-names                 \
             --relative-only                   \
             --object-directory $(GCOV_OUTPUT)

GCOV:=$(shell command -v $(GCOV_COMMAND))
GCOV_VERSION=$(call get_stdout_version_index,$(GCOV),--version,3)

ifneq ($(GCOV),) # {

################################################################################
# The "--demangled-names" gcov option can only be used for gcov                #
# (versions > 4.8.5). So if the version is <= 4.8.5, we try use the c++filt    #
# application.                                                                 #
################################################################################
GCOV_DEMANGLE_OPT_FLOOR=4.8.5
ifeq ($(call ver_greater_than,$(GCOV_VERSION),$(GCOV_DEMANGLE_OPT_FLOOR)),F) # {
  GCOV_OPTS:=$(filter-out --demangled-names,$(GCOV_OPTS))

  GCOV_CPP_FILT_COMMAND=c++filt
  GCOV_CPP_FILT:=$(shell command -v $(GCOV_CPP_FILT_COMMAND))
ifneq ($(GCOV_CPP_FILT),) # {
  GCOV_CPP_FILT_PIPE= | $(GCOV_CPP_FILT)
endif # }

endif # }

ifdef DOXYGEN_INPUT # {
  GCOV_SOURCES:=$(shell echo $(DOXYGEN_INPUT) | tr ' ' '\n' | egrep '(\.cpp|\.h)$$')
else ifdef HEADERS # } {
  GCOV_SOURCES =  $(MAIN)
  GCOV_SOURCES += $(HEADERS)
  GCOV_SOURCES += $(wildcard $(subst .h,.cpp,$(subst $(INCLUDE_DIR),$(SRC_DIR),$(HEADERS))))
else # } {
  GCOV_SOURCES = $(MAIN)
endif # }
GCOV_SOURCES:=$(wildcard $(sort $(GCOV_SOURCES)))
GCOV_SOURCES:=$(if $(GCOV_SOURCES),$(shell realpath --relative-to=$(CURDIR) $(GCOV_SOURCES)))

LCOV_COMMAND=lcov
LCOV:=$(shell command -v $(LCOV_COMMAND))
LCOV_VERSION=$(call get_stdout_version,$(LCOV_COMMAND),--version,| sed 's/^.* //')

LCOV_OPTS=--base-directory $(CURDIR)      \
          --capture                       \
          --checksum                      \
          --directory $(GCOV_OUTPUT)      \
          --exclude "/opt/*"              \
          --exclude "/usr/*"              \
          --gcov-tool $(GCOV_COMMAND)     \
          --list-full-path                \
          --output-file $(LCOV_INFO_FILE)

GENHTML_COMMAND=genhtml
GENHTML:=$(shell command -v $(GENHTML_COMMAND))
GENHTML_VERSION=$(call get_stdout_version_index,$(GENHTML_COMMAND),-version,4)
GENHTML_TITLE=$(GCOV_EXE) Coverage
GENHTML_TITLE += $(GCOV_EXE_COMMAND)
GENHTML_OPTS=$(LCOV_INFO_FILE)                 \
             --branch-coverage                 \
             --demangle-cpp                    \
             --frames                          \
             --function-coverage               \
             --html-extension html             \
             --legend                          \
             --missed                          \
             --num-spaces 2                    \
             --output-directory $(LCOV_OUTPUT) \
             --precision 2                     \
             --show-details                    \
             --title "$(GENHTML_TITLE)"

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than clean.                                                            #
################################################################################
export GCOV_CLEAN_TARGET ?= clean

IMAGE_PATH ?= $(PROJECT_ROOT)/images
GENHTML_FAVICON=$(IMAGE_PATH)/code_coverage.png

# Use "--coverage" to enable gcov (--coverage is a synonym for
# "-fprofile-arcs -ftest-coverage" (when compiling) and "-lgcov" (when linking))
$(GCOV_COMMAND): GCOV_CXXFLAGS= $(filter-out -g $(OPT),$(CXXFLAGS)) --coverage
$(GCOV_COMMAND): DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
$(GCOV_COMMAND): gcov_clean $(GCOV_CLEAN_TARGET) gcov_prep
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] GCOV_CLEAN_TARGET = [$(GCOV_CLEAN_TARGET)]'
	@mkdir -p $(GCOV_OUTPUT)
	+@$(MAKE_NO_DIR) CXXFLAGS="$(GCOV_CXXFLAGS)" $(GCOV_EXE)
	@$(GCOV_EXE_COMMAND) || :
	@find $(CURDIR) -not -path "$(GCOV_OUTPUT)/*" -type f \( -name "*\.gcda" -o -name "*\.gcno" \) -exec mv {} $(GCOV_OUTPUT) \;
	@echo 'Command      = [$(GCOV_EXE_COMMAND)]'                                            | tee    $(GCOV_LOG)
	@echo 'Date/Time    = [$(DATE_TIME)]'                                                   | tee -a $(GCOV_LOG)
	@echo 'gcov sources = [$(GCOV_SOURCES)]'                                                | tee -a $(GCOV_LOG)
	@echo "++++++++++ BEGIN $(notdir $(GCOV)) ++++++++++"                                   | tee -a $(GCOV_LOG)
	@echo '[$(@)] $(INFO_LABEL) $(GCOV) $(GCOV_OPTS) $(GCOV_SOURCES) $(GCOV_CPP_FILT_PIPE)' | tee -a $(GCOV_LOG)
	@$(GCOV) $(GCOV_OPTS) $(GCOV_SOURCES) 2>&1 $(GCOV_CPP_FILT_PIPE)                        | tee -a $(GCOV_LOG)
	@echo "++++++++++  END  $(notdir $(GCOV)) ++++++++++"                                   | tee -a $(GCOV_LOG)
	@find $(CURDIR) -not -path "$(GCOV_OUTPUT)/*" -type f -name "*\.gcov" -exec mv {} $(GCOV_OUTPUT) \;
ifneq ($(LCOV),) # {
ifneq ($(GENHTML),) # {
	@echo "++++++++++ BEGIN $(notdir $(LCOV)) ++++++++++"    | tee -a $(GCOV_LOG)
	@echo '[$(@)] $(INFO_LABEL) $(LCOV) $(LCOV_OPTS)'        | tee -a $(GCOV_LOG)
	@$(LCOV) $(LCOV_OPTS) 2>&1                               | tee -a $(GCOV_LOG)
	@echo "++++++++++  END  $(notdir $(LCOV)) ++++++++++"    | tee -a $(GCOV_LOG)
	@echo "++++++++++ BEGIN $(notdir $(GENHTML)) ++++++++++" | tee -a $(GCOV_LOG)
	@echo '[$(@)] $(INFO_LABEL) $(GENHTML) $(GENHTML_OPTS)'  | tee -a $(GCOV_LOG)
	@$(GENHTML) $(GENHTML_OPTS) 2>&1                         | tee -a $(GCOV_LOG)
	@echo "++++++++++  END  $(notdir $(GENHTML)) ++++++++++" | tee -a $(GCOV_LOG)

################################################################################
# Adding the executable name and git branch to the title of each genhtml       #
# output file.                                                                 #
################################################################################
	@find $(LCOV_OUTPUT) -type f -name "*\.html" -exec \
   sed -i "s/\(^.*<tr><td class=\"title\">\)\(LCOV - code coverage report\)\(<\/td><\/tr>$$\)/\1\2 for executable [$(GCOV_EXE)], git branch: $(GIT_BRANCH)\3/" {} \;

################################################################################
# Changing the date format from:                                               #
#   2020-03-15 15:17:54                                                        #
# to:                                                                          #
#   Sun Mar 15, 2020 03:17:54 PM MDT                                           #
# in the genhtml output files.                                                 #
################################################################################
	@find $(LCOV_OUTPUT) -type f -name "*\.html" -exec \
   sed -i "/<td class=\"headerItem\">Date:<\/td>/{n;s/\(^.*<td class=\"headerValue\">\)\([0-9]\{4\}-[0-9]\{2\}-[0-9]\{2\} \([0-9]\{2\}:\)\{2\}[0-9]\{2\}\)\(<\/td>\)/\1$(DATE_TIME)\4/}" {} \;
	@mv $(LCOV_OUTPUT)/index.html $(GENHTML_INDEX)
	@$(call add_favicon,$(GENHTML_FAVICON),$(GENHTML_INDEX))
	@$(call open_file_command,$(GENHTML_INDEX),$(BROWSER),$(GENHTML_COMMAND) index file)
else # } {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(GENHTML_COMMAND)] command was not found; no HTML coverage report will be created.)
endif # }
else # } {
	@$(warning [$(@)] $(WARNING_LABEL) The [$(LCOV_COMMAND)] command was not found; no HTML coverage report will be created.)
endif # }
	@$(call open_file_command,$(GCOV_LOG),$(EDITOR),$(GCOV_COMMAND) log file)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

gcov_prep::;

.PHONY: gcov_clean
gcov_clean:
	@rm -f *.gcda *.gcno *.gcov $(LCOV_INFO_FILE) $(GCOV_LOG)
	@rm -rf $(GCOV_OUTPUT) $(LCOV_OUTPUT)

clean:: gcov_clean

.PHONY: gcov_mk_help
gcov_mk_help::
ifneq ($(wildcard $(GCOV_EXE)),) # {
	@echo '$(MAKE) $(GCOV_COMMAND) → perform a $(GCOV_COMMAND) run and create the $(GCOV_LOG) output file (HTML output will also be created if both the $(LCOV_COMMAND) and $(GENHTML_COMMAND) commands are available)'

endif # }

show_help:: gcov_mk_help

platform_info::
	@echo '$(GCOV_COMMAND) version    = [$(GCOV_VERSION)]'
	@echo '$(GENHTML_COMMAND) version = [$(GENHTML_VERSION)]'
	@echo '$(LCOV_COMMAND) version    = [$(LCOV_VERSION)]'

else # } {

$(GCOV_TARGETS):
	@$(warning [$(@)] $(WARNING_LABEL) Can not make target [$(@)]; the [$(GCOV_COMMAND)] command is not available.)

endif # }

endif # }

endif # }

