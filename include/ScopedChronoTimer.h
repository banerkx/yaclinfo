
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sys/time.h>

#include <chrono>
#include <iomanip>
#include <iostream>

// This is on a per thread basis.
// Example usage:
//   long double fElapsedWallTime = 0.0;
//   long double fElapsedCPUTime  = 0.0;
//
//   {
//     ScopedChronoTimer tTimer(fElapsedWallTime, fElapsedCPUTime);
//     // Do stuff that you want to time.
//   }
//   std::cout << "ELAPSED WALL TIME = [" << fElapsedWallTime << "] sec." <<
//   std::endl; std::cout << "ELAPSED CPU  TIME = [" << fElapsedCPUTime  << "]
//   sec." << std::endl;
class ScopedChronoTimer
{
  public:

    ScopedChronoTimer(long double& fElapsedWallTime, long double& fElapsedCPUTime);

    ~ScopedChronoTimer();

    static long double avgCPUTime(const long double fTime);

    static void printTimingInfo(
      const long double fElapsedWallTime,
      const long double fElapsedCPUTime,
      std::ostream&     tStream = std::cout)
    {
      tStream << "ELAPSED WALL TIME = [" << std::setw(21) << std::setprecision(15)
              << std::scientific << fElapsedWallTime << "] sec." << std::endl;
      tStream << "ELAPSED CPU  TIME = [" << std::setw(21) << std::setprecision(15)
              << std::scientific << fElapsedCPUTime << "] sec." << std::endl
              << std::endl;
    }

  private:

    long double&                                   m_fElapsedWallTime;
    long double&                                   m_fElapsedCPUTime;
    std::chrono::high_resolution_clock::time_point m_tStartWallTime;
    const clock_t                                  m_nClockTicks;

    long double elapsedWallTime() const;

    long double elapsedCPUTime() const;

    // Intentionally deleted.
    ScopedChronoTimer()                             = delete;
    ScopedChronoTimer(const ScopedChronoTimer& rhs) = delete;
    ScopedChronoTimer(ScopedChronoTimer&& rhs)      = delete;
    ScopedChronoTimer&       operator=(const ScopedChronoTimer& rhs) = delete;
    ScopedChronoTimer&       operator=(ScopedChronoTimer&& rhs) = delete;
    ScopedChronoTimer*       operator&()                        = delete;
    const ScopedChronoTimer* operator&() const                  = delete;
};

