
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CPP_UNIT_TEST_MK_INCLUDE_GUARD # {
CPP_UNIT_TEST_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including unit_tests_common.mk.            #
################################################################################
ifeq ($(UNIT_TESTS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/unit_tests/unit_tests_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

CPP_UNIT_TEST_INCLUDE ?= /usr/include
$(eval $(call add_to_includes,$(CPP_UNIT_TEST_INCLUDE)))

CPP_UNIT_VERSION=$(shell { grep "^\#define CPPUNIT_VERSION" $(CPP_UNIT_TEST_INCLUDE)/cppunit/config-auto.h 2> /dev/null || echo 'UNKNOWN'; } | sed 's/^.* "//;s/"//')

CPP_UNIT_TEST_HEADERS=$(if $(INCLUDE_DIR),$(addprefix $(INCLUDE_DIR)/,CppUnitTestRunner.h CppUnitTestTimingsCollector.h CppUnitTestXMLOutputterHook.h))
HEADERS += $(CPP_UNIT_TEST_HEADERS)
CPP_UNIT_TEST_OBJECTS=$(subst $(INCLUDE_DIR),$(OBJECTS_DIR),$(CPP_UNIT_TEST_HEADERS:.h=.o))
CPP_UNIT_TEST_SO_OBJECTS=$(subst $(OBJECTS_DIR),$(SO_OBJECTS_DIR),$(CPP_UNIT_TEST_OBJECTS))

ifneq ($(filter-out $(MARKDOWN_DIR) $(PYTHON_SCRIPTS) $(SCRIPTS),$(DOXYGEN_INPUT)),) # {
DOXYGEN_INPUT += $(CPP_UNIT_TEST_HEADERS)
DOXYGEN_INPUT += $(wildcard $(subst $(INCLUDE_DIR),$(SRC_DIR),$(CPP_UNIT_TEST_HEADERS:.h=.cpp)))
endif # }

CPP_UNIT_TEST_LIB_DIR ?= /usr/local/lib
ifeq ($(findstring -L $(CPP_UNIT_TEST_LIB_DIR),$(LIB_DIRS)),) # {
LIB_DIRS += -L $(CPP_UNIT_TEST_LIB_DIR)
endif # }

CPP_UNIT_TEST_LIBS=-lcppunit
LIBS += $(CPP_UNIT_TEST_LIBS)

CPP_UNIT_MAIN ?= $(MAIN)
CPP_UNIT_EXE  ?= $(CPP_UNIT_MAIN:.cpp=)

CPP_UNIT_TEST_RESULTS     ?= $(EXE)_cpp_unit_results.txt
CPP_UNIT_TEST_RESULTS_XML ?= $(EXE)_cpp_unit_results.xml
CPP_UNIT_TEST_TITLE       ?= "$(EXE) CPP Unit Tests"

CPP_UNIT_TEST_EXE_OPTS ?= -t "$(CPP_UNIT_TEST_TITLE)" --cpp $(CPP_UNIT_TEST_RESULTS)
.PHONY: cpp_unit crun
crun cpp_unit: unit_tests_setup $(CPP_UNIT_EXE)
	@./$(CPP_UNIT_EXE) $(CPP_UNIT_TEST_EXE_OPTS) || :
ifneq ($(filter vi vim,$(EDITOR)),) # {
	@egrep -q "^!!!FAILURES!!!$$" $(CPP_UNIT_TEST_RESULTS) && echo -e '$(WARNING_LABEL) Cpp Unit test failure(s) exist.\n$(filter-out +1,$(EDITOR)) +/"\!\!\!FAILURES\!\!\!" $(CURDIR)/$(CPP_UNIT_TEST_RESULTS)' || echo -e '$(INFO_LABEL) No Cpp Unit test failure(s).\n$(EDITOR) $(CURDIR)/$(CPP_UNIT_TEST_RESULTS)'
else # } {
	@egrep -q "^!!!FAILURES!!!$$" $(CPP_UNIT_TEST_RESULTS) && echo '$(WARNING_LABEL) Cpp Unit test failure(s) exist.' || echo '$(INFO_LABEL) No Cpp Unit test failure(s).'
	@echo '$(EDITOR) $(CURDIR)/$(CPP_UNIT_TEST_RESULTS)''
endif # }
	+@$(MAKE_NO_DIR) unit_tests_teardown

CPP_UNIT_TEST_EXE_XML_OPTS ?= -x $(subst .txt,.xml,$(CPP_UNIT_TEST_EXE_OPTS))
.PHONY: cpp_unit_xmlrun
xcrun cpp_unit_xmlrun: unit_tests_setup $(CPP_UNIT_EXE)
	@./$(CPP_UNIT_EXE) $(CPP_UNIT_TEST_EXE_XML_OPTS) || :
ifneq ($(filter cpp_unit_xmlrun,$(MAKECMDGOALS)),) # {
ifneq ($(filter vi vim,$(EDITOR)),) # {
	@grep -q "<FailedTest id" $(CPP_UNIT_TEST_RESULTS_XML) && echo -e '$(WARNING_LABEL) Cpp Unit test failure(s) exist.\n$(filter-out +1,$(EDITOR)) +/"<failure message=" $(CURDIR)/$(CPP_UNIT_TEST_RESULTS_XML)' || echo -e '$(INFO_LABEL) No Cpp Unit test failure(s).\n$(EDITOR) $(CURDIR)/$(CPP_UNIT_TEST_RESULTS_XML)'
else # } {
	@grep -q "<FailedTest id=" $(CPP_UNIT_TEST_RESULTS_XML) && echo '$(WARNING_LABEL) Cpp Unit test failure(s) exist.' || echo '$(INFO_LABEL) No Cpp Unit test failure(s).'
	@echo '$(EDITOR) $(CURDIR)/$(CPP_UNIT_TEST_RESULTS_XML)'
endif # }
endif # }
##	@$(call xml_beautify,$(CPP_UNIT_TEST_RESULTS_XML))
	+@$(MAKE_NO_DIR) unit_tests_teardown

clean::
	@rm -f $(CPP_UNIT_TEST_RESULTS) $(CPP_UNIT_TEST_RESULTS_XML)

platform_info::
	@echo 'CPP Unit version = [$(CPP_UNIT_VERSION)]'

.PHONY: cpp_unit_test_mk_help
cpp_unit_test_mk_help:cpp_unit_test
	@echo '$(MAKE) cpp_unit  → execute the Cpp Unit test(s), saving results to [$(CPP_UNIT_TEST_RESULTS)]'
	@echo '$(MAKE) cpp_unit_xmlrun → execute the Cpp Unit test(s), saving results, as XML, to [$(CPP_UNIT_TEST_RESULTS_XML)]'

show_help:: cpp_unit_testelp

endif # }

