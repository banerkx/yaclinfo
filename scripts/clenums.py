#!/usr/bin/python3

################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

"""Creates the C++ scoped enum header and implementation files for an OpenCL enum."""

import ctypes
import datetime
import getopt
import inspect
import os
import platform
import re
import sys
import traceback

py_exe = os.path.basename(sys.argv[0])

minimum_python_version = (3, 5)
if sys.version_info < minimum_python_version:
  current_python_version = (
    str(sys.version_info[0])
    + "."
    + str(sys.version_info[1])
    + "."
    + str(sys.version_info[2])
  )
  print("Current Python version = [" + current_python_version + "], ", end="")
  sys.exit("Python version ≥ %s.%s is required.\n" % minimum_python_version)


class OpenCLEnumError(Exception):
  """Raise exceptions for errors encountered during processing of OpenCL enum C++ files.

  Attributes:
    message -- error message
  """

  def __init__(self, message):
    """Construct object."""
    self.message = message
    super().__init__(self.message)

  def __str__(self):
    """Return a string containing all of the relevant information."""
    stack = inspect.stack()
    stack_size = len(stack)
    function_calls = ""
    if 2 == stack_size:
      function_calls = (
        stack[1].function
        + "["
        + str(stack[1].lineno)
        + "]::"
        + stack[0].function
        + "["
        + str(stack[0].lineno)
        + "] "
      )

    for i in range(stack_size - 1, 1, -1):
      if 0 == len(function_calls):
        function_calls = stack[i].function + "[" + str(stack[i].lineno) + "]"
      else:
        function_calls += (
          "::" + stack[i].function + "[" + str(stack[i].lineno) + "]"
        )
    return function_calls + self.message


################################################################################
# From:                                                                        #
#   https://www.tutorialspoint.com/python_design_patterns/python_design_patterns_singleton.htm #
################################################################################
class Parameters:
  """Singleton wrapper for otherwise global parameters."""

  __instance = None

  @staticmethod
  def get_instance():
    """Access the static instance."""
    if None == Parameters.__instance:
      Parameters()
    return Parameters.__instance

  def enum_file(self):
    """
    Access self.__enum_file.

    param: Parameters self - this instance
    :return: self.__enum_file
    :rtype: str
    """
    return self.__enum_file

  def set_enum_file(self, file):
    """
    Set self.__enum_file.

    param: Parameters self - this instance
    param: str file - OpenCL enum header file
    """
    self.__enum_file = file

  def include_dir(self):
    """
    Access self.__include_dir.

    param: Parameters self - this instance
    :return: self.__include_dir
    :rtype: str
    """
    return self.__include_dir

  def set_include_dir(self, directory):
    """
    Set self.__include_dir.

    param: Parameters self - this instance
    param: str directory - OpenCL enum file output directory
    """
    self.__include_dir = directory

  def source_dir(self):
    """
    Access self.__source_dir.

    param: Parameters self - this instance
    :return: self.__source_dir
    :rtype: str
    """
    return self.__source_dir

  def set_source_dir(self, directory):
    """
    Set self.__source_dir.

    param: Parameters self - this instance
    param: str directory - OpenCL enum file output directory
    """
    self.__source_dir = directory

  def __init__(self):
    """Virtually private constructor."""
    if None == Parameters.__instance:
      Parameters.__instance = self

      self.__enum_file = ""
      self.__include_dir = ""
      self.__source_dir = ""


def log_message(message_level, message):
  """
  Input message is logged.

  param: str message_level - the message level, e.g., WARNING
  param: str message - the message
  """
  stack = inspect.stack()
  stack_size = len(stack)
  stack_trace = ""

  ################################################################################
  # NOTE: stack_size will never equal 1.                                         #
  ################################################################################
  if 2 == stack_size:
    stack_trace = (
      stack[1].function
      + "["
      + str(stack[1].lineno)
      + "]::"
      + stack[0].function
      + "["
      + str(stack[0].lineno)
      + "]"
    )
    print("[" + py_exe + "]" + stack_trace + "] " + message_level + ": " + message)
    return

  for i in range(stack_size - 1, 1, -1):
    if 0 == len(stack_trace):
      stack_trace = stack[i].function + "[" + str(stack[i].lineno) + "]"
    else:
      stack_trace += "::" + stack[i].function + "[" + str(stack[i].lineno) + "]"

  print("[" + py_exe + "]" + stack_trace + " " + message_level + ": ", end="")
  if 0 != len(message):
    print(message)
  else:
    print("")


def log_info(message):
  """
  Input message is logged, using INFO as the message level.

  param: str message - the message
  """
  log_message("INFO", message)


def log_warning(message):
  """
  Input message is logged, using WARNING as the message level.

  param: str message - the message
  """
  log_message("WARNING", message)


def log_error(message, exit_code=0):
  """
  Input message is logged, using ERROR as the message level.

  param: str message - the message
  param: int exit_code - exit code (optional)
  """
  log_message("ERROR", message)
  if 0 != exit_code:
    os._exit(exit_code)


def usage(exit_code):
  """
  Usage information is displayed and then this script exits.

  param: int exit_code - the exit code
  """
  print(
    py_exe
    + " -e,--enum [OpenCL enum file] -i,--include [header directory] -s,--src [implementation directory] -v,--version -h,--help\n"
  )
  print("-e,--enum    Used to specify the OpenCL enum file. Required.")
  print("-i,--include Used to specify the OpenCL header file directory. Required.")
  print(
    "-s,--src     Used to specify the OpenCL implementation file directory. Required."
  )
  print(
    "-v,--version Prints the "
    + py_exe
    + " version and exits. Takes precedence over all"
  )
  print("             other options except -h,--help. Optional.")
  print("-h,--help    Print this usage information and exit. This option takes")
  print("             precedence over all other options. Optional.")

  print("OS Information: [", end="")
  os_info = platform.uname()
  for i in range(len(os_info) - 2):
    print(os_info[i], end=" ")
  print(os_info[len(os_info) - 2] + "]")
  print("Python version: [" + sys.version.replace("\n", "") + "]")

  sys.exit(exit_code)


def print_error(error_message, exit_code):
  """
  Input message is displayed and then this script exits.

  param: str error_message - the error message
  param: int exit_code - the exit code
  """
  print("[" + py_exe + "] ERROR: " + error_message, file=sys.stderr)
  usage(exit_code)


def parse_command_line(argv):
  """
  Command line arguments are parsed.

  param: list(str) argv - the command line arguments
  """
  params_instance = Parameters.get_instance()

  ##  log_info('Command Line = [' + ' '.join(sys.argv) + ']')
  if 8 != ctypes.sizeof(ctypes.c_voidp):
    print("[" + py_exe + "] must be executed on a 64-bit system.")
    sys.exit(1)

  try:

    long_options = ["enum=", "include=", "src=", "version", "help"]
    short_options = ""
    for i in long_options:
      short_options += i[0]
      if "=" == i[-1]:
        short_options += ":"

    ################################################################################
    # NOTE: getopt.getopt(...) actually returns a 2-tuple. To get the entire       #
    #       2-tuple, use:                                                          #
    #                                                                              #
    #         opts, args = getopt.getopt(...)                                      #
    #                                                                              #
    #       args will hold those options that were not used on the command line.   #
    #       However, since we don't need to use args, we ignore it to eliminate    #
    #       an "unused variable" message from pylint.                              #
    ################################################################################
    opts = getopt.getopt(argv, short_options, long_options)[0]

  except getopt.GetoptError as getopt_excep:
    print_error(
      "Invalid command line option or missing command line option required argument: ["
      + getopt_excep.msg
      + "].",
      1,
    )

  clenums_version = 1.0
  print_version = False
  for opt, arg in opts:
    if opt in ("-e", "--enum"):
      params_instance.set_enum_file(arg)
    elif opt in ("-i", "--include"):
      params_instance.set_include_dir(arg)
    elif opt in ("-s", "--src"):
      params_instance.set_source_dir(arg)
    elif opt in ("-v", "--version"):
      print_version = True
    elif opt in ("-h", "--help"):
      usage(0)

  if True == print_version:
    print(py_exe + " version " + str(clenums_version))
    sys.exit(0)

  if 0 == len(params_instance.enum_file()):
    print_error("Must specify an OpenCL enum file.", 1)

  if 0 == len(params_instance.include_dir()):
    print_error("Must specify an OpenCL enum header file directory.", 1)

  if 0 == len(params_instance.source_dir()):
    print_error("Must specify an OpenCL enum implementation file directory.", 1)

  if False == os.path.exists(params_instance.enum_file()):
    print_error(
      "The specified OpenCL enum file ["
      + params_instance.enum_file()
      + "] does not exist.",
      1,
    )

  if False == os.path.isfile(params_instance.enum_file()):
    print_error(
      "The specified OpenCL enum file ["
      + params_instance.enum_file()
      + "] is not a file nor a link to a file.",
      1,
    )

  if False == os.access(params_instance.enum_file(), os.R_OK):
    print_error(
      "The specified OpenCL enum file ["
      + params_instance.enum_file()
      + "] is not readable.",
      1,
    )

  if 0 == os.path.getsize(params_instance.enum_file()):
    print_error(
      "The specified OpenCL enum file ["
      + params_instance.enum_file()
      + "] is is empty.",
      1,
    )


def make_directory(directory):
  """
  Create directory, including any parent directories.

  param: str directory - the directory to be made
  """
  try:
    if False == os.path.exists(directory):
      os.makedirs(directory, exist_ok=True)
    else:
      if False == os.access(directory, os.W_OK):
        raise OpenCLEnumError(
          "Can not create file in directory [" + directory + "]."
        )

  except PermissionError as e:
    raise OpenCLEnumError(
      "The specified directory ["
      + directory
      + "] could not be created due to ["
      + str(e)
      + "]."
    ) from e


def read_opencl_enum_file(enum_filename):
  """
  Read enum file.

  param: str enum_filename - OpenCL enum file
  :return: OpenCL enums
  :rtype: list(str)
  """
  with open(enum_filename, "r") as enum_file_fd:
    ocl_enums = enum_file_fd.readlines()
    return ocl_enums


def write_copy_right(fd):
  """
  Copyright information is written to the input file object.

  param: file object fd - opened file object
  """
  fd.write("\n")
  fd.write("/**\n")
  fd.write(" * \\file\n")
  fd.write(" * \\copyright\n")

  fd.write(" *  Copyright © 2018-")
  current_year = datetime.date.today().strftime("%Y")
  fd.write(current_year)
  fd.write(" K. Banerjee\n")

  fd.write(" *\n")
  fd.write(" *  This file is part of clinfo.\n")
  fd.write(" *\n")
  fd.write(" *  clinfo is free software: you can redistribute it and/or modify\n")
  fd.write(
    " *  it under the terms of the GNU General Public License as published by\n"
  )
  fd.write(" *  the Free Software Foundation, either version 3 of the License, or\n")
  fd.write(" *  (at your option) any later version.\n")
  fd.write(" *\n")
  fd.write(" *  clinfo is distributed in the hope that it will be useful,\n")
  fd.write(" *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of\n")
  fd.write(
    " *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the\n"
  )
  fd.write(" *  GNU General Public License for more details.\n")
  fd.write(" *\n")
  fd.write(" *  You should have received a copy of the GNU General Public License\n")
  fd.write(" *  along with clinfo.  If not, see <https://www.gnu.org/licenses/>.\n")
  fd.write(" */\n")
  fd.write("\n")


def remove_file_if_exists(file):
  """
  If the input file exists, it is removed.

  param: str file - file
  """
  if True == os.path.exists(file):
    os.remove(file)


def create_opencl_enum_header_file(
  inc_dir,
  clenum_name,
  opencl_enums,
  clenum_integer_type,
  scoped_enum_to_enum_return_type,
  prefix,
  max_clenum_size,
  max_value,
):
  """
  Create scoped OpenCL enum header.

  param: str inc_dir - directory for the header file
  param: str clenum_name - OpenCL enum name, e.g., cl_bool
  param: list(str) opencl_enums - list of OpenCL enums
  param: str clenum_integer_type - integer type that is big enough to hold the OpenCL enum value
  param: str scoped_enum_to_enum_return_type - return type for the scopedEnumToCLEnum() function
  param: str prefix - prefix for the OpenCL enum
  param: int max_clenum_size - maximum length of the OpenCL enum names
  param: int max_value - maximum OpenCL enum value
  """
  header_file = inc_dir + "/" + clenum_name + ".h"
  remove_file_if_exists(header_file)

  with open(header_file, "a") as fd:
    write_copy_right(fd)

    fd.write("#pragma once\n")
    fd.write("\n")
    fd.write("#ifdef CL_TARGET_OPENCL_VERSION\n")
    fd.write("#undef CL_TARGET_OPENCL_VERSION\n")
    fd.write("#endif\n")
    fd.write("#define CL_TARGET_OPENCL_VERSION 300\n\n")
    fd.write("#include <string>\n\n")
    fd.write("#include <CL/opencl.h>\n")
    fd.write("\n")

    begin = "BEGIN"
    end = "END"
    len_prefix = len(prefix)
    if max_clenum_size < len(begin):
      max_clenum_size = len(begin)

    fd.write("enum class OPEN_" + clenum_name + " : " + clenum_integer_type + "\n")
    fd.write("{\n")
    fd.write(
      "  "
      + begin
      + (" " * (len_prefix + max_clenum_size - len(begin)))
      + " = "
      + opencl_enums[0]
      + ",\n"
    )
    for i in opencl_enums:
      fd.write(
        "  "
        + prefix
        + i
        + (" " * (max_clenum_size - len(i)))
        + " = "
        + i
        + ",\n"
      )
    fd.write(
      "  "
      + end
      + (" " * (len_prefix + max_clenum_size - len(end)))
      + " = "
      + str(max_value + 1)
      + " // = (max "
      + clenum_name
      + " enum value) + 1\n"
    )
    fd.write("};\n\n")

    clenum_prefix = prefix + "_" + clenum_name
    fd.write(clenum_prefix + "& operator++(" + clenum_prefix + "& e);\n\n")
    fd.write(
      "std::ostream& operator<<(std::ostream& tStream, const "
      + clenum_prefix
      + "& e);\n\n"
    )
    fd.write("void " + clenum_name + "_printEnumValues(std::ostream& tStream);\n\n")
    fd.write("size_t " + clenum_name + "_get_max_clenum_name_length();\n\n")
    fd.write("std::string scopedEnumToString(const " + clenum_prefix + "& e);\n\n")
    fd.write(
      "std::string "
      + clenum_name
      + "_ToString(const "
      + clenum_integer_type
      + " clEnum);\n\n"
    )
    fd.write(
      scoped_enum_to_enum_return_type
      + " scopedEnumToCLEnum(const "
      + clenum_prefix
      + "& e);\n\n"
    )


def create_opencl_enum_source_file(
  src_dir,
  clenum_name,
  opencl_enums,
  clenum_integer_type,
  scoped_enum_to_enum_return_type,
  prefix,
):
  """
  Create scoped OpenCL enum implementation file.

  param: str src_dir - directory for the implementation file
  param: str clenum_name - OpenCL enum name, e.g., cl_bool
  param: list<str> opencl_enums - list of OpenCL enums
  param: str clenum_integer_type - integer type that is big enough to hold the OpenCL enum value
  param: str scoped_enum_to_enum_return_type - return type for the scopedEnumToCLEnum() function
  param: str prefix - prefix for the OpenCL enum
  """
  source_file = src_dir + "/" + clenum_name + ".cpp"
  remove_file_if_exists(source_file)

  last_index = len(opencl_enums) - 1

  with open(source_file, "a") as fd:
    write_copy_right(fd)

    fd.write("#include <iomanip>\n")
    fd.write("#include <stdexcept>\n")
    fd.write("#include <sstream>\n\n")
    fd.write('#include "Macros.h"\n')
    fd.write('#include "' + clenum_name + '.h"\n\n')

    clenum_prefix = prefix + "_" + clenum_name
    fd.write(clenum_prefix + "& operator++(" + clenum_prefix + "& e)\n")
    fd.write("{\n")
    fd.write("  switch (e)\n")
    fd.write("  {\n")
    for i in range(len(opencl_enums)):
      fd.write(
        "    case " + clenum_prefix + "::" + prefix + opencl_enums[i] + ":\n"
      )
      if last_index != i:
        fd.write(
          "      e = "
          + clenum_prefix
          + "::"
          + prefix
          + opencl_enums[i + 1]
          + ";\n"
        )
        fd.write("    break;\n\n")
      else:
        fd.write("    default:\n")
        fd.write("      e = " + clenum_prefix + "::END;\n")
        fd.write("    break;\n")
    fd.write("  };\n")
    fd.write("  return (e);\n")
    fd.write("}\n\n")

    fd.write(
      "std::ostream& operator<<(std::ostream& tStream, const "
      + clenum_prefix
      + "& e)\n"
    )
    fd.write("{\n")
    fd.write("  tStream << (scopedEnumToString(e));\n")
    fd.write("  return (tStream);\n")
    fd.write("}\n\n")

    fd.write("void " + clenum_name + "_printEnumValues(std::ostream& tStream)\n")
    fd.write("{\n")
    fd.write(
      "  static const int nMax = static_cast<int>("
      + clenum_name
      + "_get_max_clenum_name_length());\n"
    )
    fd.write(
      "  for (auto i = "
      + clenum_prefix
      + "::BEGIN; i != "
      + clenum_prefix
      + "::END; ++i)\n"
    )
    fd.write("  {\n")
    fd.write(
      '    tStream << "[" << std::left << std::setw(nMax) << scopedEnumToString(i) << "] → [" << static_cast<typename std::underlying_type<'
      + clenum_prefix
      + '>::type>(i) << "]\\n";\n'
    )
    fd.write("  }\n")
    fd.write("}\n\n")

    max_enum_length = 0
    for i in opencl_enums:
      max_enum_length = max(max_enum_length, len(i))

    fd.write("size_t " + clenum_name + "_get_max_clenum_name_length()\n")
    fd.write("{\n")
    fd.write(
      "  constexpr static const size_t nMax = " + str(max_enum_length) + ";\n"
    )
    fd.write("  return (nMax);\n")
    fd.write("}\n\n")

    fd.write("std::string scopedEnumToString(const " + clenum_prefix + "& e)\n")
    fd.write("{\n")
    fd.write(
      "  return ("
      + clenum_name
      + "_ToString(static_cast<typename std::underlying_type<"
      + clenum_prefix
      + ">::type>(e)));\n"
    )
    fd.write("}\n")
    fd.write("\n")

    fd.write(
      "std::string "
      + clenum_name
      + "_ToString(const "
      + clenum_integer_type
      + " clEnum)\n"
    )
    fd.write("{\n")
    fd.write("  switch (clEnum)\n")
    fd.write("  {\n")
    for i in range(len(opencl_enums)):
      fd.write("    case " + opencl_enums[i] + ":\n")
      fd.write('      return ("' + opencl_enums[i] + '");\n')
      fd.write("    break;\n\n")
    fd.write("    default:\n")
    fd.write('      return ("CL_INVALID_VALUE");\n')
    fd.write("    break;\n")
    fd.write("  };\n")
    fd.write("}\n\n")

    scoped_enum_to_enum_return_type = re.sub("(_[123][012]0)+$", "", clenum_name)
    fd.write(
      scoped_enum_to_enum_return_type
      + " scopedEnumToCLEnum(const "
      + clenum_prefix
      + "& e)\n"
    )
    fd.write("{\n")
    fd.write("  switch (e)\n")
    fd.write("  {\n")
    for i in range(len(opencl_enums)):
      fd.write(
        "    case " + clenum_prefix + "::" + prefix + opencl_enums[i] + ":\n"
      )
      fd.write("      return (" + opencl_enums[i] + ");\n")
      fd.write("    break;\n\n")
    fd.write("    default:\n")
    fd.write("    {\n")
    fd.write("      constexpr static const int nWidth = 4;\n")
    fd.write("      std::stringstream tError;\n")
    fd.write("      GET_ERROR_PREFIX(tError)\n")
    fd.write(
      '      tError << "Unknown OpenCL '
      + clenum_name
      + ' value: [" << std::internal\n'
    )
    fd.write(
      "             << \"0x\" << std::setfill('0') << std::setw(nWidth) << std::hex << std::uppercase\n"
    )
    fd.write(
      "             << static_cast<typename std::underlying_type<"
      + clenum_prefix
      + '>::type>(e) << "] = ["\n'
    )
    fd.write(
      "             << std::nouppercase << std::dec << static_cast<typename std::underlying_type<"
      + clenum_prefix
      + '>::type>(e) << "].";\n'
    )
    fd.write("      throw std::runtime_error(tError.str());\n")
    fd.write("    }\n")
    fd.write("    break;\n")
    fd.write("  };\n")
    fd.write("}\n\n")


def main():
  """Scoped OpenCL enum class files are created."""
  parse_command_line(sys.argv[1:])

  params_instance = Parameters.get_instance()
  enum_file = params_instance.enum_file()
  include_dir = params_instance.include_dir()
  source_dir = params_instance.source_dir()

  make_directory(include_dir)
  make_directory(source_dir)

  clenum_name = os.path.basename(enum_file)
  opencl_enums = read_opencl_enum_file(enum_file)

  opencl_enums = [i.strip() for i in opencl_enums]
  clenum_integer_type = re.sub("^# ", "", opencl_enums.pop(0))
  max_clenum_size = len(max(opencl_enums, key=len))
  max_value = int(opencl_enums.pop(0).split()[-1])

  integral_types = [
    "char",
    "unsigned char",
    "short",
    "unsigned short",
    "int",
    "unsigned int",
    "long",
    "unsigned long",
  ]
  if not (clenum_integer_type in integral_types):
    print_error(
      "The OpenCL enum integer type ["
      + clenum_integer_type
      + "] from enum file ["
      + enum_file
      + "] is invalid.",
      10,
    )

  scoped_enum_to_enum_return_type = re.sub("(_[123][012]0)+$", "", clenum_name)
  prefix = "OPEN"
  create_opencl_enum_header_file(
    include_dir,
    clenum_name,
    opencl_enums,
    clenum_integer_type,
    scoped_enum_to_enum_return_type,
    prefix,
    max_clenum_size,
    max_value,
  )
  create_opencl_enum_source_file(
    source_dir,
    clenum_name,
    opencl_enums,
    clenum_integer_type,
    scoped_enum_to_enum_return_type,
    prefix,
  )


if __name__ == "__main__":
  try:
    main()
  except OpenCLEnumError as e:
    log_error(str(e), 1)
  except Exception as e:
    traceback.print_exc()
    log_error(str(e), 1)

