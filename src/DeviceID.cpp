
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceID.h"

#include <sstream>

#include "Macros.h"
#include "OpenCLHTMLUtilities.h"

DeviceID::DeviceID() noexcept :
  m_tPlatformID(), m_eDeviceType(), m_nNumberOfDevices(0), m_pDeviceIds(nullptr), m_sErrorMessage()
{
}

DeviceID::~DeviceID() noexcept
{
  deleteDeviceIDMem(m_pDeviceIds.get(), m_nNumberOfDevices);
}

void DeviceID::reset(
  const cl_platform_id          tPlatformID,
  const cl_device_type          eDeviceType,
  const cl_uint                 nNumberOfDevices,
  std::shared_ptr<cl_device_id> pDeviceIds,
  const std::string&            sErrorMessage) noexcept
{
  m_tPlatformID      = tPlatformID;
  m_eDeviceType      = eDeviceType;
  m_nNumberOfDevices = nNumberOfDevices;
  m_sErrorMessage    = sErrorMessage;

  if (m_nNumberOfDevices > 0)
  {
    m_pDeviceIds = pDeviceIds;
  }
  else
  {
    m_pDeviceIds.reset();
  }
}

void DeviceID::resetForError(
  const cl_platform_id tPlatformID,
  const cl_device_type eDeviceType,
  const std::string&   sError) noexcept
{
  reset(tPlatformID, eDeviceType, 0, nullptr, sError);
}

cl_device_id DeviceID::operator[](const size_t nIndex)
{
  if ((nullptr == m_pDeviceIds) || (0 == m_nNumberOfDevices))
  {
    std::stringstream tError;
    GET_ERROR_PREFIX(tError)
    tError << "No OpenCL device id's found.";
    throw std::runtime_error(tError.str());
  }

  if (nIndex >= m_nNumberOfDevices)
  {
    std::stringstream tError;
    GET_ERROR_PREFIX(tError)
    tError << "The input OpenCL device id index [" << nIndex << "] is not in the range [0,"
           << (m_nNumberOfDevices - 1) << "], inclusive.";
    throw std::out_of_range(tError.str());
  }
  return ((m_pDeviceIds.get())[nIndex]);
}

void DeviceID::deleteDeviceIDMem(cl_device_id* p, const cl_uint nSize)
{
  for (cl_uint i = 0; i < nSize; ++i)
  {
    (void) clReleaseDevice(p[i]);
    p[i] = nullptr;
  }
}

void DeviceID::deleteDeviceIDArray(cl_device_id*& p)
{
  delete[] p;
  p = nullptr;
}

