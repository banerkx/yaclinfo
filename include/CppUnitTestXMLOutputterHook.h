
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cppunit/XmlOutputterHook.h>

#include <string>

CPPUNIT_NS_BEGIN

class CppUnitTestXMLOutputterHook : public CPPUNIT_NS::XmlOutputterHook
{
  public:

    CppUnitTestXMLOutputterHook(
      const std::string& sTestsTitle,
      const std::string& sTotalExecutionTime);

    virtual ~CppUnitTestXMLOutputterHook() override;

    /**
     * Called before any elements is added to the root element.
     * @param pDocument - XML Document being created.
     */
    void beginDocument(CppUnit::XmlDocument* pDocument) override;

    /**
     * Called after adding all elements to the root element.
     * @param pDocument - XML Document being created.
     */
    void endDocument(CppUnit::XmlDocument* pDocument) override;

    /**
     * Called after adding a fail test element.
     * @param pDocument - XML Document being created.
     * @param pTestElement - <FailedTest\> element.
     * @param pTest - Test that failed.
     * @param pFailure - Test failure data.
     */
    virtual void failTestAdded(
      XmlDocument* pDocument,
      XmlElement*  pTestElement,
      Test*        pTest,
      TestFailure* pFailure) override;

    /**
     * Called after adding a successful test element.
     * @param pDocument - XML Document being created.
     * @param pTestElement - <Test\> element.
     * @param pTest - Test that was successful.
     */
    virtual void successfulTestAdded(XmlDocument* pDocument, XmlElement* pTestElement, Test* pTest)
      override;

    /**
     * Called after adding the statistic element.
     * @param pDocument - XML Document being created.
     * @param pStatisticsElement - <Statistics\> element.
     */
    virtual void statisticsAdded(XmlDocument* pDocument, XmlElement* pStatisticsElement) override;

    /**
     * testsTitle
     * @brief accessor for the tests title
     * @return m_sTestsTitle
     */
    inline const std::string& testsTitle() const
    {
      return (m_sTestsTitle);
    }

    /**
     * totalExecutionTime
     * @brief accessor for the total execution time
     * @return m_sTotalExecutionTime
     */
    inline const std::string& totalExecutionTime() const
    {
      return (m_sTotalExecutionTime);
    }

  private:

    const std::string m_sTestsTitle;
    const std::string m_sTotalExecutionTime;

    // Intentionally deleted.
    CppUnitTestXMLOutputterHook()                                       = delete;
    CppUnitTestXMLOutputterHook(const CppUnitTestXMLOutputterHook& rhs) = delete;
    CppUnitTestXMLOutputterHook(CppUnitTestXMLOutputterHook&& rhs)      = delete;
    CppUnitTestXMLOutputterHook& operator=(const CppUnitTestXMLOutputterHook& rhs) = delete;
    CppUnitTestXMLOutputterHook& operator=(CppUnitTestXMLOutputterHook&& rhs) = delete;
};

CPPUNIT_NS_END

