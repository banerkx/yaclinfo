
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef DOXYGEN_MK_INCLUDE_GUARD # {
DOXYGEN_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

DOXYGEN:=$(notdir $(shell command -v doxygen))

DOXYGEN_PROJECT_STUB=$(notdir $(CURDIR))
ifdef EXE # {
DOXYGEN_PROJECT ?= $(EXE)
else ifdef PROJECT # } {
DOXYGEN_PROJECT ?= $(PROJECT)
else # } {
DOXYGEN_PROJECT ?= $(DOXYGEN_PROJECT_STUB)
endif # }

DVIPS_WEB_SITE=https://ctan.org/pkg/dvips?lang=en
GHOSTSCRIPT_WEB_SITE=https://www.ghostscript.com/download/gsdnld.html
LATEX_WEB_SITE=https://www.latex-project.org/get

DOXYGEN_WARNINGS:=$(CURDIR)/$(DOXYGEN_PROJECT_STUB)_doxygen_warnings.log
DOXYGEN_MESSAGES_LOG:=$(CURDIR)/doxygen_messages.log

DOXYGEN_WEB_SITE=http://www.doxygen.nl

################################################################################
# If the specified output file exists, writes the input message, with the      #
# input label, to the specified output file.                                   #
#                                                                              #
# $(1) - the target                                                            #
# $(2) - the message                                                           #
# $(3) - the log file                                                          #
# $(4) - the label                                                             #
################################################################################
doxygen_log_message=$(if $(1),$(if $(2),$(file >> $(3),$(4) [$(1)] $(2).)))

################################################################################
# Writes out the input warning message to the doxygen messages file and using  #
# the $(warning ...) function.                                                 #
#                                                                              #
# $(1) - the target                                                            #
# $(2) - warning message                                                       #
################################################################################
define doxygen_log_warning
  $(call doxygen_log_message,$(1),$(2),$(DOXYGEN_MESSAGES_LOG),$(WARNING_LABEL))
  $(if $(1),$(if $(2),$(warning [$(1)] $(WARNING_LABEL) $(2).)))
endef

################################################################################
# If any messages have been written to the doxygen messages file, they are     #
# copied to the doxygen warnings file and the doxygen messages file is deleted.#
# Then the error message is written to the doxygen warnings file and to stderr #
# using the $(error ...) function.                                             #
#                                                                              #
# $(1) - target                                                                #
# $(2) - error message                                                         #
################################################################################
define doxygen_log_error
  $(if $(1),$(if $(2),$(if $(wildcard $(DOXYGEN_MESSAGES_LOG)),$(if $(wildcard $(DOXYGEN_WARNINGS)),$(shell cat $(DOXYGEN_MESSAGES_LOG) >> $(DOXYGEN_WARNINGS); rm $(DOXYGEN_MESSAGES_LOG))))))
  $(call doxygen_log_message,$(1),$(2),$(DOXYGEN_WARNINGS),$(ERROR_LABEL))
  $(if $(1),$(if $(2),$(error $(ERROR_LABEL) [$(1)] $(2))))
endef

################################################################################
# Creates an HTML link string from the input URL and image; the link will open #
# in a new tab.                                                                #
#                                                                              #
# $(1) - URL, including the protocol                                           #
# $(2) - image file                                                            #
################################################################################
create_image_link=$(if $(1),$(if $(2),<a href="$(1)" target="_blank"><img src="$(2)"></a>))

ifeq ($(DOXYGEN),doxygen) # {

DOXYGEN_VERSION=$(call get_stdout_version,doxygen,--version)
DOXYGEN_DIR ?= $(CURDIR)/dOxygen
DOXYGEN_HTML_DIR=$(DOXYGEN_DIR)/html

IMAGE_PATH ?= $(PROJECT_ROOT)/images

DOXYGEN_CUSTOM_DIRECTORY ?= $(CURDIR)/custom_doxygen
$(DOXYGEN_CUSTOM_DIRECTORY):
	@mkdir -p $(DOXYGEN_CUSTOM_DIRECTORY)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/graph_viz.mk.    #
################################################################################
ifeq ($(GRAPH_VIZ_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/graph_viz.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

-include $(PROJECT_ROOT)/markdowns.mk

DOX_FILES=default_dox_files default_dox_header default_dox_footer default_dox_css
DOXYGEN_TARGETS=create_doxygen_custom_footer create_doxygen_custom_header dox doxconfig doxygen dox_apps dox_cleanup doxygen_apps doxygen_clean doxygen_config post_doxygen
.PHONY: $(DOXYGEN_TARGETS)
DOXYGEN_EXTERNAL_TARGETS=show_updates clean platform_info print-% qprint-% show_help vars
ifneq ($(filter $(DOXYGEN_TARGETS) $(DOX_FILES) $(DOXYGEN_EXTERNAL_TARGETS) $(MARKDOWN_TARGETS),$(MAKECMDGOALS)),) # {

DOXYGEN_DATE:=$(shell date +"%a %b %d %I:%M:%S %p %Z %Y")

DVIPS_VERSION=$(call get_stdout_version_index,dvips,-version,4)
GHOST_SCRIPT_VERSION=$(call get_stdout_version_index,gs,-version,3)
LATEX_VERSION=$(call get_stdout_version_index,latex,-version,2)

platform_info::
	@echo 'doxygen version = [$(DOXYGEN_VERSION)]'
	@echo 'dvips version   = [$(DVIPS_VERSION)]'
	@echo 'gs version      = [$(GHOST_SCRIPT_VERSION)]'
	@echo 'latex version   = [$(LATEX_VERSION)]'

show_updates:: DOXYGEN_DOWNLOAD=https://www.doxygen.nl/download.html
show_updates:: DOXYGEN_LATEST_VERSION=$(shell { curl -s $(DOXYGEN_DOWNLOAD) 2> /dev/null; echo UNKNOWN; } | egrep '(UNKNOWN|The latest version of doxygen is )' | sed 's/^.*The latest version of doxygen is //;s/ .*//' | head -1)
show_updates::
	@$(call check_updates,doxygen,$(DOXYGEN_DOWNLOAD),$(DOXYGEN_VERSION),$(DOXYGEN_LATEST_VERSION))

HAVE_DOT:=$(if $(DOT),YES,NO)
ifeq ($(HAVE_DOT),YES) # {
DOXYGEN_BUILD_INFO=[dot $(DOT_VERSION)]
DOT_GRAPH_MAX_NODES=150
endif # }

################################################################################
# Target to print the versions of the applications used by doxygen.            #
################################################################################
dox_apps:
	+@$(MAKE_NO_DIR) doxygen_apps | sort | column -s"|" -o" " -t

doxygen_apps::
	@echo 'doxygen        | version: [$(DOXYGEN_VERSION)] | website: [$(DOXYGEN_WEB_SITE)]'
	@echo '$(DOT_COMMAND) | version: [$(DOT_VERSION)]     | website: [$(DOT_WEB_SITE)]'

ifneq ($(DVIPS_VERSION),) # {
ifneq ($(DVIPS_VERSION),UNKNOWN) # {

DOXYGEN_BUILD_INFO += [dvips $(DVIPS_VERSION)]
doxygen_apps::
	@echo 'dvips        | version: [$(DVIPS_VERSION)] | website: [$(DVIPS_WEB_SITE)]'

endif # }
endif # }

ifneq ($(GHOST_SCRIPT_VERSION),) # {
ifneq ($(GHOST_SCRIPT_VERSION),UNKNOWN) # {

DOXYGEN_BUILD_INFO += [gs $(GHOST_SCRIPT_VERSION)]
doxygen_apps::
	@echo 'gs           | version: [$(GHOST_SCRIPT_VERSION)] | website: [$(GHOSTSCRIPT_WEB_SITE)]'

endif # }
endif # }

ifneq ($(LATEX_VERSION),) # {
ifneq ($(LATEX_VERSION),UNKNOWN) # {

DOXYGEN_BUILD_INFO += [latex $(LATEX_VERSION)]
doxygen_apps::
	@echo 'latex        | version: [$(LATEX_VERSION)] | website: [$(LATEX_WEB_SITE)]'

endif # }
endif # }

ifneq ($(PLANT_UML_VERSION),UNKNOWN) # {
DOXYGEN_BUILD_INFO += [plantuml $(PLANT_UML_VERSION)]
doxygen_apps::
	@echo 'plantuml     | version: [$(PLANT_UML_VERSION)] | website: [$(PLANT_UML_DOWNLOAD)]'
endif # }

DOXYGEN_BUILD_INFO += $(DOXYGEN_DATE)

DOXYGEN_FILE_PATTERNS ?= *.awk *.cpp *.h *.md *.py
DOXYGEN_EXCLUDE_DIRS ?=
DOXYGEN_EXCLUDE_PATTERNS ?=

################################################################################
# Setting the input for doxygen:                                               #
#   - adding all of the header files                                           #
#   - deriving the name of the implementation files from the names of the      #
#     header files and then adding only those implementation files that exist  #
#     and then adding the file $(MAIN)                                         #
#   - if MARKDOWN_DIR is defined, then adding it                               #
#   - removing leading/trailing white space                                    #
################################################################################
DOXYGEN_INPUT=$(HEADERS)
DOXYGEN_INPUT += $(if $(strip $(HEADERS)),$(wildcard $(subst $(INCLUDE_DIR),$(SRC_DIR),$(HEADERS:.h=.cpp)) $(MAIN)))
ifdef MARKDOWN_DIR # {
DOXYGEN_INPUT += $(MARKDOWN_DIR)
endif # }
DOXYGEN_INPUT += $(PYTHON_SCRIPTS) $(SCRIPTS)
DOXYGEN_INPUT:=$(realpath $(strip $(DOXYGEN_INPUT)))

FAVICON               ?= $(IMAGE_PATH)/ozone.png
LOGO                  ?= $(IMAGE_PATH)/C++_Logo.png
DOXYGEN_EXTRA_FILES   += $(FAVICON) $(LOGO)
DOXYGEN_CUSTOM_HEADER ?= $(DOXYGEN_CUSTOM_DIRECTORY)/$(DOXYGEN_PROJECT_STUB)_custom_header.html

################################################################################
# The GIT_VERSION_FILTER variable:                                             #
#   o specified as a bash function so that ${1} captures the file name         #
#   o the function get_ver then calls this function with the file name as its  #
#     single argument                                                          #
#                                                                              #
# The git_ver function does the following:                                     #
#   (file is not binary) AND                                                   #
#   (file is tracked by git) AND                                               #
#   (get most recent date and commit of file)                                  #
################################################################################
GIT_VERSION_FILTER=\"git_ver() { !(file --mime \$${1} | grep -q '; charset=binary$$') \&\& git ls-files --error-unmatch \$${1} > /dev/null 2>\&1 \&\& git log -n1 --pretty='format:%ad commit: %H' --date=format-local:'%a %b %d %r %Z %Y' \$${1}; }; git_ver \"

HTML_HUE   ?= 220
HTML_SAT   ?= 255
HTML_GAMMA ?= 80

################################################################################
# PlantUML related doxygen configuration variables.                            #
################################################################################
ifneq ($(PLANT_UML_JAR),UNKNOWN) # {
  PLANTUML_JAR_PATH=$(PLANT_UML_JAR)
  PLANTUML_INCLUDE_PATH=$(call check_directory_exists,$(realpath $(wildcard $(CURDIR)/uml/.)))
  PLANTUML_CFG_FILE=$(notdir $(wildcard $(PLANTUML_INCLUDE_PATH)/$(EXE).uml))
endif # }

################################################################################
# Create a recipe for this target (in whatever makefile includes this make     #
# file) for any needed UML diagrams.                                           #
################################################################################
.PHONY: uml
uml:: no_op_target

################################################################################
# Create a recipe for this target (in whatever makefile includes this make     #
# file) for any needed doxygen custom header.                                  #
################################################################################
.PHONY: create_doxygen_custom_header $(DOXYGEN_CUSTOM_HEADER)
FULL_REMOTE_URL=$(call get_remote_branch_url)
create_doxygen_custom_header $(DOXYGEN_CUSTOM_HEADER): | $(DOXYGEN_CUSTOM_DIRECTORY)
ifneq ($(DOXYGEN_CUSTOM_HEADER),) # {
	@rm -f $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!-- HTML header for doxygen $(DOXYGEN_VERSION)-->'                                                                           >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">'   >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<html xmlns="http://www.w3.org/1999/xhtml">'                                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<head>'                                                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
ifneq ($(wildcard $(FAVICON)),) # {
	@echo '<link rel="shortcut icon" href="$$relpath^$(notdir $(FAVICON))" type="image/x-icon"/>'                                        >> $(DOXYGEN_CUSTOM_HEADER)
endif # }
	@echo '<meta http-equiv="Content-Type" content="text/xhtml;charset=UTF-8"/>'                                                         >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<meta http-equiv="X-UA-Compatible" content="IE=9"/>'                                                                          >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<meta name="generator" content="Doxygen $$doxygenversion"/>'                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<meta name="viewport" content="width=device-width, initial-scale=1"/>'                                                        >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!--BEGIN PROJECT_NAME--><title>$$projectname: $$title</title><!--END PROJECT_NAME-->'                                        >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!--BEGIN !PROJECT_NAME--><title>$$title</title><!--END !PROJECT_NAME-->'                                                     >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<link href="$$relpath^tabs.css" rel="stylesheet" type="text/css"/>'                                                           >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<script type="text/javascript" src="$$relpath^jquery.js"></script>'                                                           >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<script type="text/javascript" src="$$relpath^dynsections.js"></script>'                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '$$treeview'                                                                                                                   >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '$$search'                                                                                                                     >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '$$mathjax'                                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<link href="$$relpath^$$stylesheet" rel="stylesheet" type="text/css" />'                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '$$extrastylesheet'                                                                                                            >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '</head>'                                                                                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<body>'                                                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<div id="top"><!-- do not remove this div, it is closed by doxygen! -->'                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!--BEGIN TITLEAREA-->'                                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<div id="titlearea">'                                                                                                         >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<table cellspacing="0" cellpadding="0">'                                                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo ' <tbody>'                                                                                                                     >> $(DOXYGEN_CUSTOM_HEADER)
	@echo ' <tr style="height: 56px;">'                                                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--BEGIN PROJECT_LOGO-->'                                                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
ifneq ($(FULL_REMOTE_URL),) # {
	@echo '  <td id="projectlogo"><a href="$(FULL_REMOTE_URL)" target="_blank"><img alt="Logo" src="$$relpath^$$projectlogo"/></a></td>' >> $(DOXYGEN_CUSTOM_HEADER)
else # } {
	@echo '  <td id="projectlogo"><img alt="Logo" src="$$relpath^$$projectlogo"/></td>'                                                  >> $(DOXYGEN_CUSTOM_HEADER)
endif # }
ifneq ($(PROJECT_LOGO_2),) # {
	@echo '  <td id="projectlogo2"><img alt="Logo" src="$$relpath^$(PROJECT_LOGO_2)"/></td>'                                             >> $(DOXYGEN_CUSTOM_HEADER)
endif # }
	@echo '  <!--END PROJECT_LOGO-->'                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--BEGIN PROJECT_NAME-->'                                                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <td id="projectalign" style="padding-left: 0.5em;">'                                                                        >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <div id="projectname">$$projectname'                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--BEGIN PROJECT_NUMBER-->&#160;<span id="projectnumber">$$projectnumber</span><!--END PROJECT_NUMBER-->'                 >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   </div>'                                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--BEGIN PROJECT_BRIEF--><div id="projectbrief">$$projectbrief</div><!--END PROJECT_BRIEF-->'                             >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  </td>'                                                                                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--END PROJECT_NAME-->'                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--BEGIN !PROJECT_NAME-->'                                                                                                 >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--BEGIN PROJECT_BRIEF-->'                                                                                                >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '    <td style="padding-left: 0.5em;">'                                                                                        >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '    <div id="projectbrief">$$projectbrief</div>'                                                                              >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '    </td>'                                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--END PROJECT_BRIEF-->'                                                                                                  >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--END !PROJECT_NAME-->'                                                                                                   >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--BEGIN DISABLE_INDEX-->'                                                                                                 >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--BEGIN SEARCHENGINE-->'                                                                                                 >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <td>$$searchbox</td>'                                                                                                      >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '   <!--END SEARCHENGINE-->'                                                                                                   >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '  <!--END DISABLE_INDEX-->'                                                                                                   >> $(DOXYGEN_CUSTOM_HEADER)
	@echo ' </tr>'                                                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
	@echo ' </tbody>'                                                                                                                    >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '</table>'                                                                                                                     >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '</div>'                                                                                                                       >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!--END TITLEAREA-->'                                                                                                         >> $(DOXYGEN_CUSTOM_HEADER)
	@echo '<!-- end header part -->'                                                                                                     >> $(DOXYGEN_CUSTOM_HEADER)
	@$(call open_sole_target_file,$(@),$(DOXYGEN_CUSTOM_HEADER),$(EDITOR),doxygen custom header)
	@echo ''

else # } {
	@:
endif # }

DOXYGEN_CUSTOM_FOOTER:=$(DOXYGEN_CUSTOM_DIRECTORY)/custom_footer.html
.PHONY: create_doxygen_custom_footer $(DOXYGEN_CUSTOM_FOOTER)
create_doxygen_custom_footer $(DOXYGEN_CUSTOM_FOOTER): | $(DOXYGEN_CUSTOM_DIRECTORY)
	@rm -f $(DOXYGEN_CUSTOM_FOOTER)
	@doxygen -w html /dev/null $(DOXYGEN_CUSTOM_FOOTER) /dev/null
	@sed -i 's@\(^.*</a>.*doxygenversion \)\(.*</li\>\)@\1$(DOXYGEN_BUILD_INFO)\2@' $(DOXYGEN_CUSTOM_FOOTER)
	@echo ''
	@$(call open_sole_target_file,$(@),$(DOXYGEN_CUSTOM_FOOTER),$(EDITOR),doxygen custom footer)

################################################################################
# Sets the doxygen configuration file parameters FILTER_PATTERNS and           #
# EXTENSION_MAPPING; these parameters are used to filter input files.          #
#                                                                              #
# Define filter patterns and extension mappsings as needed and then append     #
# then to the following variables:                                             #
#                                                                              #
#   DOXYGEN_FILTER_PATTERNS                                                    #
#   DOXYGEN_EXTENSION_MAPPINGS                                                 #
################################################################################

################################################################################
# doxygen filter mapping(s):                                                   #
################################################################################

################################################################################
# For filtering gawk scripts:                                                  #
#   s|^\#\# |//! |      → replace initial 2 #'s with "//! "                    #
#   s|^\# |//! |        → replace initial # with "//! "                        #
#   s|\#$$$$||          → remove ending #                                      #
#   s/^\#\\\\+\#$$$$/ / → replace a line of #'s with an empty line             #
#                                                                              #
# NOTE: A doxygen filter should not add lines to nor remove lines from a file. #
################################################################################
AWK_FILTER_PATTERN=*.awk=\"sed 's|^\#\# |//! |;s|^\# |//! |;s|\#$$$$||;s/^\#\\\\+\#$$$$//'\"

################################################################################
# This filter adds a space after the '#' character in the words #define,       #
# #ifdef, and #endif from comment lines in python scripts (this prevents       #
# unneeded doxygen warnings).                                                  #
# NOTE: \x5c is hex for '\'                                                    #
################################################################################
PYTHON_FILTER_PATTERN=\"py_filter() { (file \$${1} | grep -q 'Python script') \&\& sed ':loop;s/\\x5c(^\#.*\\x5c)\\x5c(\#\\x5c)\\x5c([dei][enf][fd][ie][nf]\\x5c)\\x5c(.*$$\\x5c)/\\x5c1\\x5c2 \\x5c3\\x5c4/g;t loop' \$${1}; }; py_filter \"
DOXYGEN_FILTER_PATTERNS += *.py=$(PYTHON_FILTER_PATTERN)

################################################################################
# doxygen extension mapping(s):                                                #
################################################################################
AWK_EXTENSION_MAPPING= awk=C++

.PHONY: doxygen_config
################################################################################
# The first sed command joins lines that end with '\' and removes trailing     #
# spaces.                                                                      #
################################################################################
doxygen_config:
	@doxygen -g -                                                                                       | \
  sed 's/ *$$//;:x /\\$$/ { N; s/\\\n//g ; bx };s/  */ /g'                                            | \
  sed "s/^ALPHABETICAL_INDEX .*$$/ALPHABETICAL_INDEX         = YES/;                                    \
       s/^ALWAYS_DETAILED_SEC .*$$/ALWAYS_DETAILED_SEC       = YES/;                                    \
       s/^BUILTIN_STL_SUPPORT .*$$/BUILTIN_STL_SUPPORT       = YES/;                                    \
       s/^CALLER_GRAPH .*$$/CALLER_GRAPH                     = YES/;                                    \
       s/^CALL_GRAPH .*$$/CALL_GRAPH                         = YES/;                                    \
       s/^CASE_SENSE_NAMES .*$$/CASE_SENSE_NAMES             = YES/;                                    \
       s/^CLASS_GRAPH .*$$/CLASS_GRAPH                       = YES/;                                    \
       s/^COLLABORATION_GRAPH .*$$/COLLABORATION_GRAPH       = YES/;                                    \
       s/^CREATE_SUBDIRS .*$$/CREATE_SUBDIRS                 = YES/;                                    \
       s/^DIRECTORY_GRAPH .*$$/DIRECTORY_GRAPH               = YES/;                                    \
       s/^DISABLE_INDEX .*$$/DISABLE_INDEX                   = NO/;                                     \
       s/^DOT_CLEANUP .*$$/DOT_CLEANUP                       = YES/;                                    \
       s/^DOT_COMMON_ATTR .*$$/DOT_COMMON_ATTR               = "fontname=Helvetica,fontsize=10"/;       \
       s/^DOT_IMAGE_FORMAT .*$$/DOT_IMAGE_FORMAT             = svg/;                                    \
       s/^DOT_GRAPH_MAX_NODES .*$$/DOT_GRAPH_MAX_NODES       = $(DOT_GRAPH_MAX_NODES)/;                 \
       s/^DOT_MULTI_TARGETS .*$$/DOT_MULTI_TARGETS           = YES/;                                    \
       s/^DOT_UML_DETAILS .*$$/DOT_UML_DETAILS               = YES/;                                    \
       s/^ENUM_VALUES_PER_LINE .*$$/ENUM_VALUES_PER_LINE     = 1/;                                      \
       s@^EXCLUDE .*@EXCLUDE                                 = $(DOXYGEN_EXCLUDE_DIRS)@;                \
       s@^EXCLUDE_PATTERNS .*@EXCLUDE_PATTERNS               = $(DOXYGEN_EXCLUDE_PATTERNS)@;            \
       s/^EXCLUDE_SYMLINKS .*$$/EXCLUDE_SYMLINKS             = NO/;                                     \
       s/^EXTENSION_MAPPING .*$$/EXTENSION_MAPPING           = $(strip $(DOXYGEN_EXTENSION_MAPPINGS))/; \
       s/^EXPAND_ONLY_PREDEF .*$$/EXPAND_ONLY_PREDEF         = NO/;                                     \
       s/^EXTRACT_ALL .*$$/EXTRACT_ALL                       = YES/;                                    \
       s/^EXTRACT_ANON_NSPACES .*$$/EXTRACT_ANON_NSPACES     = YES/;                                    \
       s/^EXTRACT_LOCAL_CLASSES .*$$/EXTRACT_LOCAL_CLASSES   = YES/;                                    \
       s/^EXTRACT_PACKAGE .*$$/EXTRACT_PACKAGE               = YES/;                                    \
       s/^EXTRACT_PRIVATE .*$$/EXTRACT_PRIVATE               = YES/;                                    \
       s/^EXTRACT_PRIV_VIRTUAL .*$$/EXTRACT_PRIV_VIRTUAL     = YES/;                                    \
       s/^EXTRACT_STATIC .*$$/EXTRACT_STATIC                 = YES/;                                    \
       s/^FILE_PATTERNS .*$$/FILE_PATTERNS                   = $(DOXYGEN_FILE_PATTERNS)/;               \
       s@^FILE_VERSION_FILTER .*@FILE_VERSION_FILTER         = $(GIT_VERSION_FILTER)@;                  \
       s@^FILTER_PATTERNS .*@FILTER_PATTERNS                 = $(strip $(DOXYGEN_FILTER_PATTERNS))@;    \
       s/^FORCE_LOCAL_INCLUDES .*$$/FORCE_LOCAL_INCLUDES     = YES/;                                    \
       s/^FULL_PATH_NAMES .*$$/FULL_PATH_NAMES               = YES/;                                    \
       s/^GENERATE_AUTOGEN_DEF .*$$/GENERATE_AUTOGEN_DEF     = NO/;                                     \
       s/^GENERATE_ECLIPSEHELP .*$$/GENERATE_ECLIPSEHELP     = YES/;                                    \
       s/^GENERATE_HTML .*$$/GENERATE_HTML                   = YES/;                                    \
       s/^GENERATE_HTMLHELP .*$$/GENERATE_HTMLHELP           = NO/;                                     \
       s/^GENERATE_LATEX .*$$/GENERATE_LATEX                 = NO/;                                     \
       s/^GENERATE_MAN .*$$/GENERATE_MAN                     = NO/;                                     \
       s/^GENERATE_TODOLIST .*$$/GENERATE_TODOLIST           = NO/;                                     \
       s/^GENERATE_TREEVIEW .*$$/GENERATE_TREEVIEW           = YES/;                                    \
       s/^HTML_COLORSTYLE_GAMMA .*$$/HTML_COLORSTYLE_GAMMA   = $(HTML_GAMMA)/;                          \
       s/^HTML_COLORSTYLE_HUE .*$$/HTML_COLORSTYLE_HUE       = $(HTML_HUE)/;                            \
       s/^HTML_COLORSTYLE_SAT .*$$/HTML_COLORSTYLE_SAT       = $(HTML_SAT)/;                            \
       s/^HTML_FORMULA_FORMAT .*$$/HTML_FORMULA_FORMAT       = svg/;                                    \
       s/^HAVE_DOT .*$$/HAVE_DOT                             = $(HAVE_DOT)/;                            \
       s/^HIDE_UNDOC_RELATIONS .*$$/HIDE_UNDOC_RELATIONS     = NO/;                                     \
       s/^HTML_DYNAMIC_SECTIONS .*$$/HTML_DYNAMIC_SECTIONS   = YES/;                                    \
       s@^HTML_EXTRA_FILES .*@HTML_EXTRA_FILES               = $(DOXYGEN_EXTRA_FILES)@;                 \
       s@^HTML_FOOTER .*@HTML_FOOTER                         = $(DOXYGEN_CUSTOM_FOOTER)@;               \
       s@^HTML_HEADER .*@HTML_HEADER                         = $(DOXYGEN_CUSTOM_HEADER)@;               \
       s/^HTML_TIMESTAMP .*$$/HTML_TIMESTAMP                 = NO/;                                     \
       s/^IDL_PROPERTY_SUPPORT .*$$/IDL_PROPERTY_SUPPORT     = NO/;                                     \
       s@^IMAGE_PATH .*@IMAGE_PATH                           = $(IMAGE_PATH)@;                          \
       s/^INLINE_INFO .*$$/INLINE_INFO                       = YES/;                                    \
       s/^INLINE_INHERITED_MEMB .*$$/INLINE_INHERITED_MEMB   = YES/;                                    \
       s/^INLINE_SOURCES .*$$/INLINE_SOURCES                 = YES/;                                    \
       s@^INPUT .*@INPUT                                     = $(wildcard $(sort $(DOXYGEN_INPUT)))@;   \
       s@^INPUT_FILTER .*@INPUT_FILTER                       = $(DOXYGEN_INPUT_FILTER)@;                \
       s/^INTERACTIVE_SVG .*$$/INTERACTIVE_SVG               = YES/;                                    \
       s/^MACRO_EXPANSION .*$$/MACRO_EXPANSION               = YES/;                                    \
       s/^MAN_LINKS .*$$/MAN_LINKS                           = NO/;                                     \
       s/^MAX_INITIALIZER_LINES .*$$/MAX_INITIALIZER_LINES   = 10000/;                                  \
       s/^OPTIMIZE_OUTPUT_JAVA .*$$/OPTIMIZE_OUTPUT_JAVA     = NO/;                                     \
       s@^OUTPUT_DIRECTORY .*@OUTPUT_DIRECTORY               = $(DOXYGEN_DIR)@;                         \
       s/^PAPER_TYPE .*$$/PAPER_TYPE                         = letter/;                                 \
       s@^PLANTUML_CFG_FILE .*@PLANTUML_CFG_FILE             = $(PLANTUML_CFG_FILE)@;                   \
       s@^PLANTUML_INCLUDE_PATH .*@PLANTUML_INCLUDE_PATH     = $(PLANTUML_INCLUDE_PATH)@;               \
       s@^PLANTUML_JAR_PATH .*@PLANTUML_JAR_PATH             = $(PLANTUML_JAR_PATH)@;                   \
       s@^PROJECT_LOGO .*@PROJECT_LOGO                       = $(LOGO)@;                                \
       s/^PROJECT_NAME .*$$/PROJECT_NAME                     = \"$(DOXYGEN_PROJECT)\"/;                 \
       s/^PROJECT_NUMBER .*$$/PROJECT_NUMBER                 = \"$(DOXYGEN_PROJECT_NUMBER)\"/;          \
       s/^RECURSIVE .*$$/RECURSIVE                           = YES/;                                    \
       s/^REFERENCED_BY_RELATION .*$$/REFERENCED_BY_RELATION = YES/;                                    \
       s/^REFERENCES_RELATION .*$$/REFERENCES_RELATION       = YES/;                                    \
       s/^SEARCHENGINE .*$$/SEARCHENGINE                     = YES/;                                    \
       s/^SHOW_GROUPED_MEMB_INC .*$$/SHOW_GROUPED_MEMB_INC   = YES/;                                    \
       s/^SHOW_HEADERFILE .*$$/SHOW_HEADERFILE               = YES/;                                    \
       s/^SHOW_INCLUDE_FILES .*$$/SHOW_INCLUDE_FILES         = YES/;                                    \
       s/^SHOW_FILES .*$$/SHOW_FILES                         = YES/;                                    \
       s/^SHOW_NAMESPACES .*$$/SHOW_NAMESPACES               = YES/;                                    \
       s/^SKIP_FUNCTION_MACROS .*$$/SKIP_FUNCTION_MACROS     = NO/;                                     \
       s/^SORT_MEMBERS_CTORS_1ST .*$$/SORT_MEMBERS_CTORS_1ST = YES/;                                    \
       s/^SOURCE_BROWSER .*$$/SOURCE_BROWSER                 = YES/;                                    \
       s@^STRIP_FROM_PATH .*@STRIP_FROM_PATH                 = ${HOME}@;                                \
       s/^TAB_SIZE .*$$/TAB_SIZE                             = 2/;                                      \
       s/^TEMPLATE_RELATIONS .*$$/TEMPLATE_RELATIONS         = YES/;                                    \
       s/^TOC_EXPAND .*$$/TOC_EXPAND                         = YES/;                                    \
       s/^TYPEDEF_HIDES_STRUCT .*$$/TYPEDEF_HIDES_STRUCT     = YES/;                                    \
       s/^UML_LIMIT_NUM_FIELDS .*$$/UML_LIMIT_NUM_FIELDS     = 0/;                                      \
       s/^UML_LOOK .*$$/UML_LOOK                             = YES/;                                    \
       s/^USE_MATHJAX .*$$/USE_MATHJAX                       = YES/;                                    \
       s@^WARN_LOGFILE .*@WARN_LOGFILE                       = $(DOXYGEN_WARNINGS)@;                    \
       s/^WARN_NO_PARAMDOC .*$$/WARN_NO_PARAMDOC             = YES/"                                  | \
       sed 's@^ALIASES .*@ALIASES                            = $(DOXYGEN_ALIASES)@'                   | \
       egrep -v "(^|^ * )$$" | sed 's/ * =/ =/;s/\(^[A-Z].*$$\)/\1\n/' | grep -v "^$$"                | \
       sed 's@^# Doxyfile \(.*$$\)@# doxygen \1 $(DOXYGEN_BUILD_INFO)@'

################################################################################
# Checks for the existence of the input application.                           #
#                                                                              #
# $(1) - the makefile variable that holds the application name                 #
# $(2) - the literal application name                                          #
# $(3) - web page url from which the application can be acquired               #
################################################################################
define check_doxygen_app
  $(if $(2),$(if $(1),,$(call doxygen_log_error,$(@),The $(2) application was not found. Please install [$(2)] from: [$(3)])))
endef

################################################################################
# The following applications are needed by doxygen if there are any LaTex      #
# "formulas" that must be processed. If any of these applications can not be   #
# found, an error will be reported.                                            #
################################################################################
DVIPS:=$(shell command -v dvips)
GHOST_SCRIPT:=$(shell command -v gs)
LATEX:=$(shell command -v latex)

.PHONY: check_doxygen_apps
check_doxygen_apps: no_op_target
	@$(call check_doxygen_app,$(DVIPS),dvips,$(DVIPS_WEB_SITE))
	@$(call check_doxygen_app,$(GHOST_SCRIPT),gs,$(GHOSTSCRIPT_WEB_SITE))
	@$(call check_doxygen_app,$(LATEX),latex,$(LATEX_WEB_SITE))
ifeq ($(HAVE_DOT),NO) # {
	@$(call check_doxygen_app,,$(DOT_COMMAND),$(DOT_DOWNLOAD))
endif # }
ifeq ($(PLANT_UML_JAR),UNKNOWN) # {
	@$(call check_doxygen_app,,$(PLANT_UML_JAR_NAME),$(PLANT_UML_WEB_SITE)/download.html)
endif # }

DOXYGEN_INDEX_HTML=$(DOXYGEN_DIR)/html/index.html
dox doxygen: check_doxygen_apps $(DOXYGEN_CUSTOM_FOOTER) $(DOXYGEN_CUSTOM_HEADER) uml markdown_files | $(DOXYGEN_CUSTOM_DIRECTORY)
################################################################################
# NOTE: If the file $(DOXYGEN_WARNINGS) already exists, executing doxygen will #
#       delete it and then any mew warnings will be written to it.             #
################################################################################
	+@$(MAKE_NO_DIR) doxygen_config | doxygen $(DOXYGEN_OPTIONS) -

################################################################################
# If $(DOXYGEN_MESSAGES_LOG) has messages, we add them to the doxygen warnings #
# file.                                                                        #
################################################################################
	@if [[ -e $(DOXYGEN_MESSAGES_LOG) ]];                  \
   then                                                  \
     cat $(DOXYGEN_MESSAGES_LOG) >> $(DOXYGEN_WARNINGS); \
     rm $(DOXYGEN_MESSAGES_LOG);                         \
   fi

################################################################################
# doxygen errors of the following type are assumed to be spurious.             #
################################################################################
	@([ -f $(DOXYGEN_WARNINGS) ] && [ -w $(DOXYGEN_WARNINGS) ] && sed -i '/error: md5 hash does not match for two different runs of/d' $(DOXYGEN_WARNINGS) 2> /dev/null) || :

################################################################################
# If $(DOXYGEN_WARNINGS) is not empty, a warning message is displayed.         #
# Otherwise, it is deleted.                                                    #
################################################################################
	@if [[ -e $(DOXYGEN_WARNINGS) && -s $(DOXYGEN_WARNINGS) ]];           \
   then                                                                 \
     echo '$(WARNING_LABEL) [$(DOXYGEN_WARNINGS)] contains warnings.';  \
     sed -i 's/\x1b\[[^@-~]*[@-~]//g' $(DOXYGEN_WARNINGS);              \
     echo '$(EDITOR) $(DOXYGEN_WARNINGS)';                              \
   else                                                                 \
     echo '$(INFO_LABEL) [$(DOXYGEN_WARNINGS)] contained no warnings.'; \
     rm -f $(DOXYGEN_WARNINGS);                                         \
   fi

################################################################################
# Performing some cleanup.                                                     #
################################################################################
	@rm -rf $(MARKDOWN_DIR)/*.md $(DOXYGEN_CUSTOM_FOOTER) $(DOXYGEN_CUSTOM_HEADER)
	@([ -d $(MARKDOWN_DIR) ] && [ -z "$$(ls -A $(MARKDOWN_DIR))" ] && rmdir $(MARKDOWN_DIR)) || :
	@([ -d $(DOXYGEN_CUSTOM_DIRECTORY) ] && [ -z "$$(ls -A $(DOXYGEN_CUSTOM_DIRECTORY))" ] && rmdir $(DOXYGEN_CUSTOM_DIRECTORY)) || :
	+@$(MAKE_NO_DIR) post_doxygen
	@echo ''
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))
	@$(call open_file_command,$(DOXYGEN_INDEX_HTML),$(BROWSER),doxygen index file)

################################################################################
# This target produces the doxygen documentation and then "cleans up" files    #
# that are no longer needed.                                                   #
################################################################################
.PHONY: dox_cleanup
dox_cleanup: doxygen
	@rm -rf $(CLINFO_OUTPUT)
	@rm -rf $(PROFILER_OUTPUT_DIRS)
	@rm -rf $(CCCC_OUTPUT_DIR) $(CODE2FLOW_OUTPUT) $(CPPCHECK_OUTPUT) $(CPPCLEAN_OUTPUT) $(FLAKE8_OUTPUT) $(PYLINT_OUTPUT) $(SCAN_BUILD_OUTPUT)
	@rm -f $(BUILD_DEPENDENCY_DOT_GRAPH) $(subst .$(DOT_IMAGE_EXTENSION),.$(DOT_FILE_EXTENSION),$(BUILD_DEPENDENCY_DOT_GRAPH)) $(MAN_FILE_HTML)

################################################################################
# This target can be used to perform any post-doxygen cleanup tasks.           #
################################################################################
.PHONY: post_doxygen
post_doxygen:: no_op_target

DOXYGEN_CONFIG=$(CURDIR)/$(DOXYGEN_PROJECT_STUB)_doxygen.cfg
doxconfig:
	+@$(MAKE_NO_DIR) doxygen_config | sed 's/^[^#].*$$/&\n/' >| $(DOXYGEN_CONFIG)
	@$(call open_file_command,$(DOXYGEN_CONFIG),$(EDITOR),doxygen configuration file)

################################################################################
# Create a recipe for this target (in whatever makefile includes this make     #
# file, either directly or indirectly) for any needed creation of markdown     #
# files.                                                                       #
################################################################################
.PHONY: markdown_files
markdown_files::;

DOXYGEN_HTML_DEFAULT_HEADER=$(DOXYGEN_PROJECT_STUB)_header.html
DOXYGEN_HTML_DEFAULT_FOOTER=$(DOXYGEN_PROJECT_STUB)_footer.html
DOXYGEN_HTML_DEFAULT_CSS=$(DOXYGEN_PROJECT_STUB)_stylesheet.css
DOXYGEN_HTML_CONFIG_FILES=$(DOXYGEN_HTML_DEFAULT_HEADER) $(DOXYGEN_HTML_DEFAULT_FOOTER) $(DOXYGEN_HTML_DEFAULT_CSS)

.PHONY: default_dox_files default_dox_header default_dox_footer default_dox_css
default_dox_files:
	@rm -f $(DOXYGEN_HTML_CONFIG_FILES)
	@doxygen -w html $(DOXYGEN_HTML_CONFIG_FILES)
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_HEADER),$(EDITOR),doxygen default html header file)
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_FOOTER),$(EDITOR),doxygen default html footer file)
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_CSS),$(EDITOR),doxygen default css stylesheet file)

default_dox_header:
	@rm -f $(DOXYGEN_HTML_DEFAULT_HEADER)
	@doxygen -w html $(DOXYGEN_HTML_DEFAULT_HEADER) /dev/null /dev/null
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_HEADER),$(EDITOR),doxygen default html header file)

default_dox_footer:
	@rm -f $(DOXYGEN_HTML_DEFAULT_FOOTER)
	@doxygen -w html /dev/null $(DOXYGEN_HTML_DEFAULT_FOOTER) /dev/null
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_FOOTER),$(EDITOR),doxygen default html footer file)

default_dox_css:
	@rm -f $(DOXYGEN_HTML_DEFAULT_CSS)
	@doxygen -w html /dev/null /dev/null $(DOXYGEN_HTML_DEFAULT_CSS)
	@$(call open_file_command,$(DOXYGEN_HTML_DEFAULT_CSS),$(EDITOR),doxygen default css stylesheet file)

doxygen_clean::
	@rm -f $(DOXYGEN_WARNINGS) $(DOXYGEN_MESSAGES_LOG) $(DOXYGEN_CUSTOM_FOOTER) $(DOXYGEN_CUSTOM_HEADER)
	@([ -d $(DOXYGEN_CUSTOM_DIRECTORY) ] && [ -z "$$(ls -A $(DOXYGEN_CUSTOM_DIRECTORY))" ] && rmdir $(DOXYGEN_CUSTOM_DIRECTORY)) || :
	@rm -rf $(DOXYGEN_DIR) $(DOXYGEN_CONFIG)
	@rm -f $(DOXYGEN_HTML_CONFIG_FILES)

clean:: doxygen_clean

.PHONY: doxygen_mk_help
doxygen_mk_help::
	@echo '$(MAKE) dox_cleanup → creates doxygen documentation and removes files that are no longer needed'
	@echo '$(MAKE) doxconfig → writes the doxygen config file to [$(DOXYGEN_CONFIG)]'
	@echo '$(MAKE) doxygen → creates doxygen documentation'
	@echo '$(MAKE) doxygen_apps → show the versions of the applications used by doxygen'
	@echo '$(MAKE) doxygen_clean → deletes doxygen documentation'

show_help:: doxygen_mk_help

endif # }

else # } {

################################################################################
# If doxygen is not installed, then the doxygen_clean target will do nothing.  #
################################################################################
doxygen_clean::;

dox doxconfig doxygen doxygen_apps:
	@$(call doxygen_log_error,$(@),Can not make the target since the doxygen application was not found. Please install [doxygen] from: [$(DOXYGEN_WEB_SITE)/download.html])

endif # }

endif # }

