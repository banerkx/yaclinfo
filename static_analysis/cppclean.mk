
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CPPCLEAN_MK_INCLUDE_GUARD # {
CPPCLEAN_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

CPPCLEAN_COMMAND=cppclean
CPPCLEAN:=$(shell command -v $(CPPCLEAN_COMMAND))
CPPCLEAN_VERSION=$(call get_stdout_version_index,$(CPPCLEAN),--version,2)

CPPCLEAN_PROJECT=$(notdir $(CURDIR))
CPPCLEAN_OUTPUT=$(CPPCLEAN_PROJECT)_$(CPPCLEAN_COMMAND)
CPPCLEAN_URL=https://github.com/myint/cppclean
CPPCLEAN_EXT=_$(CPPCLEAN_COMMAND)

ifneq ($(CPPCLEAN),) # {

CPPCLEAN_FILES=$(call get_source_files)

CPPCLEAN_TARGETS=cppclean cppclean_html cppclean_prep

ifneq ($(CPPCLEAN_FILES),) # {

EDITOR ?= vim +1
BROWSER ?= firefox
IMAGE_PATH ?= $(PROJECT_ROOT)/images

################################################################################
# Exclude files matching this pattern; specify this multiple times for         #
# multiple patterns.                                                           #
################################################################################
CPPCLEAN_EXCLUDES:=$(addprefix --exclude ,$(CPPCLEAN_EXCLUDES))

################################################################################
# Add a header include path; specify this multiple times for multiple include  #
# paths.                                                                       #
################################################################################
CPPCLEAN_INCLUDE_DIRS=$(subst -isystem,,$(subst -I,,$(INCLUDES)))
CPPCLEAN_INCLUDE_DIRS:=$(addprefix --include-path ,$(CPPCLEAN_INCLUDE_DIRS))

################################################################################
# Same as --include-path but explicitly designates all header files found in   #
# these directories as "non- system" includes.                                 #
################################################################################
CPPCLEAN_NON_SYSTEM_INCLUDE_DIRS:=$(addprefix --include-path-non-system ,$(CPPCLEAN_NON_SYSTEM_INCLUDE_DIRS))

################################################################################
# Same as --include-path but explicitly designates all header files found in   #
# these directories as "system" includes.                                      #
################################################################################
CPPCLEAN_SYSTEM_INCLUDE_DIRS:=$(addprefix --include-path-system ,$(CPPCLEAN_SYSTEM_INCLUDE_DIRS))

################################################################################
# Ignore parse errors.                                                         #
################################################################################
CPPCLEAN_IGNORE_PARSE_ERRORS ?= --quiet

################################################################################
# Print verbose messages.                                                      #
################################################################################
CPPCLEAN_VERBOSE ?= --verbose

CPPCLEAN_OPTS += $(CPPCLEAN_EXCLUDES)
CPPCLEAN_OPTS += $(CPPCLEAN_IGNORE_PARSE_ERRORS)
CPPCLEAN_OPTS += $(CPPCLEAN_INCLUDE_DIRS)
CPPCLEAN_OPTS += $(CPPCLEAN_NON_SYSTEM_INCLUDE_DIRS)
CPPCLEAN_OPTS += $(CPPCLEAN_SYSTEM_INCLUDE_DIRS)
CPPCLEAN_OPTS += $(CPPCLEAN_VERBOSE)

$(CPPCLEAN_OUTPUT): cppclean_clean
	@mkdir -p $(CPPCLEAN_OUTPUT)

################################################################################
# Cleans the cppclean output files.                                            #
#                                                                              #
# $(1)- cppclean output directory                                              #
# $(2)- target name                                                            #
################################################################################
define cppclean_clean_files
if [[ -d $(1) ]];                                                                                       \
then                                                                                                    \
  sed -i '/: static data/d' $(1)/*;                                                                     \
  sed -i '/operator.* not found in expected header .* or any other directly #included header/d' $(1)/*; \
  sed -i '/^$(MAIN):.* not found in any directly #included header/d' $(1)/$(MAIN)$(CPPCLEAN_EXT);       \
  find $(1) -mindepth 1 -maxdepth 1 -type f -empty -exec rm {} \;;                                      \
  if [[ "$$(ls -A $(1))" ]];                                                                            \
  then                                                                                                  \
    ls -1 $(1) | sed 's@^@$(EDITOR) $(1)/@' || :;                                                       \
  else                                                                                                  \
    rmdir $(1);                                                                                         \
    echo '[$(2)] $(INFO_LABEL) No $(CPPCLEAN_COMMAND) results files found.';                            \
  fi;                                                                                                   \
else                                                                                                    \
  echo '[$(2)] $(INFO_LABEL) No $(CPPCLEAN_COMMAND) results files found.';                              \
fi
endef

.PHONY: cppclean_prep
cppclean_prep::
	@:

################################################################################
# Uses cppclean on the input source file.                                      #
#                                                                              #
# $(1) - input source file                                                     #
################################################################################
define make_$(CPPCLEAN_COMMAND)
$(eval CPPCLEAN_RESULTS_FILE:=$(CPPCLEAN_OUTPUT)/$(notdir $(1))$(CPPCLEAN_EXT))
$(CPPCLEAN) $(CPPCLEAN_OPTS) $(1) 2>&1 | tee $(CPPCLEAN_RESULTS_FILE)
NUM_LINES=$$(wc -l <(sed '/^Processing /d' $(CPPCLEAN_RESULTS_FILE)) | cut -d' ' -f1); \
if [[ 0 == $${NUM_LINES} ]];                                                           \
then                                                                                   \
  rm $(CPPCLEAN_RESULTS_FILE);                                                         \
fi
endef

DATE_TIME=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
.PHONY: $(CPPCLEAN_COMMAND)
cppclean: cppclean_prep cppclean_clean | $(CPPCLEAN_OUTPUT)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
##	@echo 'CPPCLEAN_OPTS  = [$(CPPCLEAN_OPTS)]'
##	@echo 'CPPCLEAN_FILES = [$(CPPCLEAN_FILES)]'
	@$(foreach file,$(CPPCLEAN_FILES),$(call make_$(CPPCLEAN_COMMAND),$(file)))
	@rmdir --ignore-fail-on-non-empty $(CPPCLEAN_OUTPUT)
	@if [[ -d "$(CPPCLEAN_OUTPUT)" ]];                                                        \
   then                                                                                     \
     sed -i '1 i\Date/Time: [$(DATE_TIME)]' $(CPPCLEAN_OUTPUT)/*;                           \
     sed -i '1 i\$(CPPCLEAN_COMMAND) version = [$(CPPCLEAN_VERSION)]' $(CPPCLEAN_OUTPUT)/*; \
     if [[ "$(@)" == "$(MAKECMDGOALS)" ]];                                                  \
     then                                                                                   \
       ls -1 $(CPPCLEAN_OUTPUT)/* | sed 's@^@$(EDITOR) @';                                  \
     fi;                                                                                    \
   elif [[ "$(@)" == "$(MAKECMDGOALS)" ]];                                                  \
   then                                                                                     \
     echo '[$(@)] $(INFO_LABEL) No $(CPPCLEAN_COMMAND) results files found.';               \
   fi
##	@$(call cppclean_clean_files,$(CPPCLEAN_OUTPUT),$(@))
	@$(call sole_target_elapsed_time,$(@),$(TARGET_START_TIME))

################################################################################
# Adds table rows generated from the input cppclean results file to the the    #
# specified HTML output file.                                                  #
#                                                                              #
# $(1) - cppclean results file                                                 #
# $(2) - HTML results file                                                     #
################################################################################
define add_cppclean_results
sed '1,3d' $(1) | sed 's@\(^.*\)\(:\)\([0-9]\+\)\(: \)\(.*\)@    <tr><td class="c1">\3</td><td class="c1">\1</td><td class="c1">\5</td></tr>@' >> $(2);
endef

.PHONY: cppclean_html
CPPCLEAN_HTML=$(CPPCLEAN_OUTPUT)/$(CPPCLEAN_PROJECT)_cppclean.html
CPPCLEAN_FAVICON=$(IMAGE_PATH)/clean.png
cppclean_html: cppclean
	@rm -f $(CPPCLEAN_HTML)
	@$(if $(wildcard $(CPPCLEAN_OUTPUT)/*),                                                                                              \
   echo '<!DOCTYPE html>'                                                                                         >> $(CPPCLEAN_HTML); \
   echo '<html lang="en">'                                                                                        >> $(CPPCLEAN_HTML); \
   echo '<head>'                                                                                                  >> $(CPPCLEAN_HTML); \
   echo '  <meta charset="utf-8">'                                                                                >> $(CPPCLEAN_HTML); \
   echo '  <meta name="viewport" content="width=device-width$(escaped_comma) initial-scale=1">'                   >> $(CPPCLEAN_HTML); \
   echo '  <style>'                                                                                               >> $(CPPCLEAN_HTML); \
   echo '    table$(escaped_comma) th$(escaped_comma) td {'                                                       >> $(CPPCLEAN_HTML); \
   echo '      border: 1px solid;'                                                                                >> $(CPPCLEAN_HTML); \
   echo '    }'                                                                                                   >> $(CPPCLEAN_HTML); \
   echo '    img {'                                                                                               >> $(CPPCLEAN_HTML); \
   echo '      border: 1px solid #ddd;'                                                                           >> $(CPPCLEAN_HTML); \
   echo '      border-radius: 4px;'                                                                               >> $(CPPCLEAN_HTML); \
   echo '      padding: 5px;'                                                                                     >> $(CPPCLEAN_HTML); \
   echo '      width: 150px;'                                                                                     >> $(CPPCLEAN_HTML); \
   echo '    }'                                                                                                   >> $(CPPCLEAN_HTML); \
   echo '    tr:hover {background-color: silver;}'                                                                >> $(CPPCLEAN_HTML); \
   echo '    th {'                                                                                                >> $(CPPCLEAN_HTML); \
   echo '      background-color: #709BB2;'                                                                        >> $(CPPCLEAN_HTML); \
   echo '      color: white;'                                                                                     >> $(CPPCLEAN_HTML); \
   echo '    }'                                                                                                   >> $(CPPCLEAN_HTML); \
   echo '    img:hover {'                                                                                         >> $(CPPCLEAN_HTML); \
   echo '      box-shadow: 0 0 2px 1px rgba(0$(escaped_comma) 140$(escaped_comma) 186$(escaped_comma) 0.5);'      >> $(CPPCLEAN_HTML); \
   echo '    }'                                                                                                   >> $(CPPCLEAN_HTML); \
   echo '  </style>'                                                                                              >> $(CPPCLEAN_HTML); \
   echo '  <title>cppclean Results</title>'                                                                       >> $(CPPCLEAN_HTML); \
   echo '  <style type="text/css">'                                                                               >> $(CPPCLEAN_HTML); \
   echo '  div.c1 {text-align: center}'                                                                           >> $(CPPCLEAN_HTML); \
   echo '  img.c2 {width:150px}'                                                                                  >> $(CPPCLEAN_HTML); \
   echo '  </style>'                                                                                              >> $(CPPCLEAN_HTML); \
   echo '  <style type="text/css">'                                                                               >> $(CPPCLEAN_HTML); \
   echo '  td.c1 {font-family:Courier New}'                                                                       >> $(CPPCLEAN_HTML); \
   echo '  </style>'                                                                                              >> $(CPPCLEAN_HTML); \
   echo '</head>'                                                                                                 >> $(CPPCLEAN_HTML); \
   echo '<body>'                                                                                                  >> $(CPPCLEAN_HTML); \
   echo '  <div class="c1">'                                                                                      >> $(CPPCLEAN_HTML); \
   echo '    <h4>$(CPPCLEAN_PROJECT) cppclean (version $(CPPCLEAN_VERSION)) Results Date/Time: $(DATE_TIME)</h4>' >> $(CPPCLEAN_HTML); \
   echo '  </div>'                                                                                                >> $(CPPCLEAN_HTML); \
   echo '  <table>'                                                                                               >> $(CPPCLEAN_HTML); \
   echo '    <tr>'                                                                                                >> $(CPPCLEAN_HTML); \
   echo '      <th>Line Number</th>'                                                                              >> $(CPPCLEAN_HTML); \
   echo '      <th>File</th>'                                                                                     >> $(CPPCLEAN_HTML); \
   echo '      <th>Error</th>'                                                                                    >> $(CPPCLEAN_HTML); \
   echo '    </tr>'                                                                                               >> $(CPPCLEAN_HTML); \
   $(foreach file,$(wildcard $(CPPCLEAN_OUTPUT)/*_cppclean),$(call add_cppclean_results,$(file),$(CPPCLEAN_HTML)))                     \
   echo '  </table>'                                                                                              >> $(CPPCLEAN_HTML); \
   echo '</body>'                                                                                                 >> $(CPPCLEAN_HTML); \
   echo '</html>'                                                                                                 >> $(CPPCLEAN_HTML); \
   sed -i "s@${HOME}@\$${HOME}@" $(CPPCLEAN_HTML);                                                                                     \
   rm -f $(CPPCLEAN_OUTPUT)/*_cppclean;,                                                                                               \
   echo '[$(@)] $(INFO_LABEL) No $(CPPCLEAN_COMMAND) results files found.';)
	@$(if $(wildcard $(CPPCLEAN_OUTPUT)),$(call add_favicon,$(CPPCLEAN_FAVICON),$(CPPCLEAN_HTML)))
	@$(if $(wildcard $(CPPCLEAN_OUTPUT)),$(call beautify_html,$(CPPCLEAN_HTML)))
	@$(if $(wildcard $(CPPCLEAN_OUTPUT)),$(call open_file_command,$(CPPCLEAN_HTML),$(BROWSER),cppclean html results file,$(EDITOR)))
	@$(call sole_target_elapsed_time,$(@),$(TARGET_START_TIME))

else # } {

$(CPPCLEAN_TARGETS):
	@echo '[$(@)] $(INFO_LABEL) No source files found.'

endif # }

platform_info::
	@echo '$(CPPCLEAN_COMMAND) version = [$(CPPCLEAN_VERSION)]'

cppclean_mk_help::
	@echo '$(MAKE) $(CPPCLEAN_COMMAND) → run $(CPPCLEAN_COMMAND) on source files'
	@echo '$(MAKE) cppclean_help → save $(CPPCLEAN_COMMAND) help information to [$(CPPCLEAN_HELP)]'

show_help:: cppclean_mk_help

.PHONY: cppclean_help
CPPCLEAN_HELP=cppclean_help.txt
cppclean_help:
	@$(CPPCLEAN) --help >| $(CPPCLEAN_HELP)
	@$(call open_file_command,$(CPPCLEAN_HELP),$(EDITOR),$(CPPCLEAN_COMMAND) help)

cppclean_clean:
	@rm -rf $(CPPCLEAN_OUTPUT) $(CPPCLEAN_HELP)

clean:: cppclean_clean

else # } {

$(CPPCLEAN_TARGETS):
	@$(error $(ERROR_LABEL) The [$(CPPCLEAN_COMMAND)] executable is not available, ∴ the target [$(@)] can not be made)

endif # }

endif # }

