
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of clinfo.
 *
 *  clinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  clinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with clinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "GoogleTestRunner.h"

#include <getopt.h>
#include <gtest/gtest.h>
#include <unistd.h>

#include <boost/filesystem.hpp>
#include <cstring>
#include <iostream>

#include "StringToPrimitive.h"

namespace GoogleTestRunner
{
  void usage(
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const short        nExitValue)
  {
    std::cout << "Usage: \n"
              << boost::filesystem::basename(sProgramName)
              << " --debug,-d --file,-f [test results file] --list,-l --noelapsed,-n "
              << "--repeat,-r [n] --random,-R --title,-t [test title] --xml,-x "
                 "--help,-h\n"
              << "--debug,-d                    Turn assertion failures into debugger "
                 "break points. Optional.\n"
              << "--file,-f [test results file] Specifies the tests results output "
                 "file. Default is ["
              << sDefaultTestsResultsFile << "].\n"
              << "                              Optional.\n"
              << "--list,-l                     Only list the available tests and "
                 "exit. This option takes precedence over\n"
              << "                              all other options except --help,-h.\n"
              << "--noelapsed,-n                Do not save the elapsed times of the "
                 "tests. Default is to save the elapsed\n"
              << "                              times. Optional.\n"
              << "--repeat,-r [n]               Execute the tests n number of times. "
                 "Default is to execute the tests exactly\n"
              << "                              once. Optional.\n"
              << "--random,-R                   Randomize the tests' execution order. "
                 "Optional.\n"
              << "--title,-t [test title]       Specifies the tests' title. Default is "
                 "["
              << sDefaultTestsTitle << "]. Optional.\n"
              << "--xml,-x                      Save the tests' results in XML format "
                 "to the tests results file. If this option\n"
              << "                              is used, then the --random,-R option "
                 "has no effect. Optional.\n"
              << "--help,-h                     Print this usage information and exit. "
                 "Takes precedence over all other options. Optional.\n";
    exit(nExitValue);
  }

  std::string createOptionsString(const struct option* pOptions)
  {
    std::string sOptions;
    for (unsigned short i = 0; 0 != pOptions[i].val; ++i)
    {
      sOptions.push_back(static_cast<char>(pOptions[i].val));
      if (required_argument == pOptions[i].has_arg)
      {
        sOptions.push_back(':');
      }
      else if (optional_argument == pOptions[i].has_arg)
      {
        sOptions.push_back(':');
        sOptions.push_back(':');
      }
    }
    return (sOptions);
  }

  void handleOptionError(
    const char         cOpt,
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const std::string& sOptions)
  {
    if (isprint(cOpt))
    {
      // The error could be that a required parameter was not used with the
      // command line cOption.
      const size_t nOptLocation = sOptions.find_first_of(cOpt);

      // The command line cOption is not valid.
      if (std::string::npos == nOptLocation)
      {
        std::cerr << "ERROR: The option [" << static_cast<char>(cOpt) << "] is not recognized.\n";
      }
      // The command line option is missing a required parameter.
      else
      {
        std::cerr << "ERROR: The option [" << static_cast<char>(cOpt)
                  << "] is missing a required parameter.\n";
      }

      usage(sProgramName, sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
    }
    else
    {
      std::cerr << "ERROR: The command line option is not printable and unknown.\n";
      usage(sProgramName, sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
    }
  }

  void ensureFileExtension(std::string& sFile, const char* pExt)
  {
    if (("/dev/null" == sFile) || (true == sFile.empty()))
    {
      return;
    }
    boost::filesystem::path tPath(sFile);
    tPath.replace_extension(pExt);
    sFile = tPath.string();
  }

  void ensureTextExtension(std::string& sFile)
  {
    ensureFileExtension(sFile, "txt");
  }

  void ensureXMLExtension(std::string& sFile)
  {
    ensureFileExtension(sFile, "xml");
  }

  void ensureHTMLExtension(std::string& sFile)
  {
    ensureFileExtension(sFile, "html");
  }

  void processCommandLineOptions(
    bool&              bDebug,
    const std::string& sDefaultTestsResultsFile,
    std::string&       sTestsResultsFile,
    bool&              bListTests,
    bool&              bNoElapsedTime,
    unsigned short&    nRepeatCount,
    bool&              bRandomTests,
    const std::string& sDefaultTestsTitle,
    std::string&       sTestsTitle,
    bool&              bXML,
    int                argc,
    char**             argv)
  {
    static struct option pOptions[] = {
      {"debug",     no_argument,       nullptr, 'd'},
      {"file",      required_argument, nullptr, 'f'},
      {"list",      no_argument,       nullptr, 'l'},
      {"noelapsed", no_argument,       nullptr, 'n'},
      {"repeat",    required_argument, nullptr, 'r'},
      {"random",    no_argument,       nullptr, 'R'},
      {"title",     required_argument, nullptr, 't'},
      {"xml",       no_argument,       nullptr, 'x'},
      {"help",      no_argument,       nullptr, 'h'},
      {nullptr,     0,                 nullptr, 0  },
    };

    const std::string sOptions(createOptionsString(pOptions));

    int nOpt         = 0;
    int nOptionIndex = 0;

    bool bSuccess = false;

    // Suppressing getopt_long()'s own error messages.
    opterr = 0;

    while ((nOpt = getopt_long(argc, argv, sOptions.c_str(), pOptions, &nOptionIndex)) != -1)
    {
      switch (nOpt)
      {
        case 'd':
          bDebug = true;
          break;

        case 'f':
          sTestsResultsFile = optarg;
          break;

        case 'l':
          bListTests = true;
          break;

        case 'n':
          bNoElapsedTime = true;
          break;

        case 'r':
        {
          bSuccess    = false;
          short nTemp = stringToPrimitive<short>(optarg, bSuccess);
          if (false == bSuccess)
          {
            std::cerr << "ERROR: Could not convert the specified repeat count [" << optarg
                      << "] into a short. Exiting.\n";
            usage(argv[0], sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
          }
          if (0 >= nTemp)
          {
            std::cerr << "ERROR: The specified repeat count [" << optarg
                      << "] is not positive. Exiting.\n";
            usage(argv[0], sDefaultTestsResultsFile, sDefaultTestsTitle, 1);
          }

          // NOTE: It is safe to convert nTemp into an unsigned short because, at
          // this point,
          //       we know that nTemp is positive.
          nRepeatCount = static_cast<unsigned short>(nTemp);
        }
        break;

        case 'R':
          bRandomTests = true;
          break;

        case 't':
          sTestsTitle = optarg;
          break;

        case 'x':
          bXML = true;
          break;

        case 'h':
          usage(argv[0], sDefaultTestsResultsFile, sDefaultTestsTitle, 0);
          break;

        case '?':
        default:

          handleOptionError(
            static_cast<char>(optopt),
            argv[0],
            sDefaultTestsResultsFile,
            sDefaultTestsTitle,
            sOptions);
          break;
      };
    }
  }

  void deleteGoogleOptionsMemory(char**& gtest_argv, std::vector<char*>& tMemory)
  {
    for (auto p : tMemory)
    {
      delete[] p;
    }
    tMemory.clear();
    delete[] gtest_argv;
    gtest_argv = nullptr;
  }

  void addToGoogleTestsArgv(
    int&                gtest_argc,
    char**              gtest_argv,
    const char*         pArgument,
    std::vector<char*>& tMemory)
  {
    tMemory.push_back(new char[std::strlen(pArgument) + 1]);
    gtest_argv[gtest_argc]                         = tMemory.back();
    gtest_argv[gtest_argc][std::strlen(pArgument)] = 0;
    std::strncpy(gtest_argv[gtest_argc], pArgument, std::strlen(pArgument));
    ++gtest_argc;
  }

  int executeGoogleTests(
    const bool&           bDebug,
    std::string           sTestsResultsFile,
    const bool&           bListTests,
    const bool&           bNoElapsedTime,
    const unsigned short& nRepeatCount,
    const bool&           bRandomTests,
    const std::string&    sTestsTitle,
    const bool&           bXML,
    char**                argv)
  {
    // ::testing::InitGoogleTest() does funny things with the memory allocated to
    // gtest_argv[]. Therefore, we use tMemory to keep proper track of the memory
    // allocations made for gtest_argv[]. This will allow us to delete all of
    // the dynamic memory.
    std::vector<char*> tMemory;

    constexpr static const unsigned short GTEST_ARGV_SIZE = 32;
    int                                   gtest_argc      = 0;
    char**                                gtest_argv      = new char*[GTEST_ARGV_SIZE];
    for (unsigned short i = 0; i < GTEST_ARGV_SIZE; ++i)
    {
      gtest_argv[i] = nullptr;
    }
    addToGoogleTestsArgv(gtest_argc, gtest_argv, argv[0], tMemory);

    if (true == bListTests)
    {
      addToGoogleTestsArgv(gtest_argc, gtest_argv, "--gtest_list_tests", tMemory);
      int nRetVal = 0;

      ::testing::InitGoogleTest(&gtest_argc, gtest_argv);
      if (false == sTestsResultsFile.empty())
      {
        std::freopen(sTestsResultsFile.c_str(), "w", stdout);

        // Outputting the tests' title and time.
        std::cout << sTestsTitle << '\n';
        time_t nRawTime;
        time(&nRawTime);
        std::cout << ctime(&nRawTime) << '\n';
        nRetVal = RUN_ALL_TESTS();
        std::fclose(stdout);
      }
      else
      {
        nRetVal = RUN_ALL_TESTS();
      }

      deleteGoogleOptionsMemory(gtest_argv, tMemory);
      return (nRetVal);
    }

    if (true == bDebug)
    {
      addToGoogleTestsArgv(gtest_argc, gtest_argv, "--gtest_break_on_failure", tMemory);
    }

    if (true == bNoElapsedTime)
    {
      addToGoogleTestsArgv(gtest_argc, gtest_argv, "--gtest_print_time=0", tMemory);
    }

    if (1 < nRepeatCount)
    {
      std::string sRepeatOption("--gtest_repeat=");
      sRepeatOption += std::to_string(nRepeatCount);
      addToGoogleTestsArgv(gtest_argc, gtest_argv, sRepeatOption.c_str(), tMemory);
    }

    if (true == bRandomTests)
    {
      addToGoogleTestsArgv(gtest_argc, gtest_argv, "--gtest_shuffle", tMemory);
    }

    if (true == bXML)
    {
      GoogleTestRunner::ensureXMLExtension(sTestsResultsFile);
      sTestsResultsFile = "--gtest_output=xml:" + sTestsResultsFile;
      addToGoogleTestsArgv(gtest_argc, gtest_argv, sTestsResultsFile.c_str(), tMemory);

      // Redirecting stdout to /dev/null since the user only wants XML output.
      std::freopen("/dev/null", "w", stdout);
    }
    else
    {
      GoogleTestRunner::ensureTextExtension(sTestsResultsFile);
      // Redirecting stdout to sTestsResultsFile so that the test results
      // will be written to it.
      std::freopen(sTestsResultsFile.c_str(), "w", stdout);
    }

    ::testing::InitGoogleTest(&gtest_argc, gtest_argv);

    // Outputting the tests' title and time.
    std::cout << sTestsTitle << '\n';
    time_t nRawTime;
    time(&nRawTime);
    std::cout << ctime(&nRawTime) << '\n';

    const int nRetVal = RUN_ALL_TESTS();

    deleteGoogleOptionsMemory(gtest_argv, tMemory);
    std::fclose(stdout);

    return (nRetVal);
  }

  int googleTestHelp()
  {
    // ::testing::InitGoogleTest() does funny things with the memory allocated to
    // gtest_argv[]. Therefore, we use tMemory to keep proper track of the memory
    // allocations made for gtest_argv[]. This will allow us to delete all of
    // the dynamic memory.
    std::vector<char*> tMemory;

    constexpr static const unsigned short GTEST_ARGV_SIZE = 2;
    int                                   gtest_argc      = 0;
    char**                                gtest_argv      = new char*[GTEST_ARGV_SIZE];
    gtest_argv[0]                                         = nullptr;
    gtest_argv[1]                                         = nullptr;
    addToGoogleTestsArgv(gtest_argc, gtest_argv, "", tMemory);
    addToGoogleTestsArgv(gtest_argc, gtest_argv, "--help", tMemory);
    ::testing::InitGoogleTest(&gtest_argc, gtest_argv);
    const int nRetVal = RUN_ALL_TESTS();
    deleteGoogleOptionsMemory(gtest_argv, tMemory);
    return (nRetVal);
  }

  std::string getCurrentDateTime()
  {
    std::time_t tDateTime = std::time(nullptr);
    std::time(&tDateTime);
    struct tm* pTimeInfo = std::localtime(&tDateTime);

    const size_t nBufferSize                = 45;
    char         sDateTime[nBufferSize + 1] = {0};

    static const std::string sTimeFormat("%A %B %e %I:%M:%S %p %Y %Z");
    (void) std::strftime(sDateTime, nBufferSize, sTimeFormat.c_str(), pTimeInfo);
    return (sDateTime);
  }

  /**
   * getGoogleTestVersion
   * @brief tries to get the google test version
   */
  std::string getGoogleTestVersion()
  {
    static std::string sGtestVersion;
    static bool        bAttemptMade = false;

    if (false == bAttemptMade)
    {
      // First, look for GTEST_VERSION.
      const char* pVersion = getenv("GTEST_VERSION");
      if (nullptr == pVersion)
      {
        // Now look for GOOGLE_TEST_VERSION.
        pVersion = getenv("GOOGLE_TEST_VERSION");
      }

      if (nullptr != pVersion)
      {
        sGtestVersion = pVersion;
      }
      bAttemptMade = true;
    }
    return (sGtestVersion);
  }
}  // namespace GoogleTestRunner

