
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef GOOGLE_TEST_MK_INCLUDE_GUARD # {
GOOGLE_TEST_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including vars_common.mk.                  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including platform.mk.                     #
################################################################################
ifeq ($(PLATFORM_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/platform.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including python.mk.                       #
################################################################################
ifeq ($(PYTHON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/python.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including unit_tests_common.mk.            #
################################################################################
ifeq ($(UNIT_TESTS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/unit_tests/unit_tests_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

GTEST2HTML_OUTPUT_DIR ?= gtest_html

GTEST_INCLUDE ?= $(subst -I,,$(strip $(shell $(PKG_CONFIG) gtest --cflags-only-I 2> /dev/null)))
$(eval $(call add_to_includes,$(GTEST_INCLUDE)))

GTEST_VERSION:=$(call get_static_lib_version,gtest)

GTEST_HEADERS=$(if $(INCLUDE_DIR),$(addprefix $(INCLUDE_DIR)/,GoogleTestRunner.h))
HEADERS += $(GTEST_HEADERS)
GTEST_OBJECTS=$(subst $(INCLUDE_DIR),$(OBJECTS_DIR),$(GTEST_HEADERS:.h=.o))
GTEST_SO_OBJECTS=$(subst $(OBJECTS_DIR),$(SO_OBJECTS_DIR),$(GTEST_OBJECTS))

ifneq ($(filter-out $(MARKDOWN_DIR) $(PYTHON_SCRIPTS) $(SCRIPTS),$(DOXYGEN_INPUT)),) # {
DOXYGEN_INPUT += $(GTEST_HEADERS)
DOXYGEN_INPUT += $(wildcard $(subst $(INCLUDE_DIR),$(SRC_DIR),$(GTEST_HEADERS:.h=.cpp)))
endif # }

GTEST_LIB_DIR ?= $(subst -L,,$(strip $(shell $(PKG_CONFIG) gtest --libs-only-L 2> /dev/null)))
LIB_DIRS += -L $(GTEST_LIB_DIR)
GTEST_LIBS=$(strip $(shell $(PKG_CONFIG) gtest gtest_main --libs-only-l 2> /dev/null))
LIBS += $(GTEST_LIBS)

################################################################################
# If Google mock needs to be used, then the following variables can be used to #
# specify the Google mock header and library directories. Specifically, append #
# these variables to $(INCLUDES) and $(LIBS) in the makefile for your Google   #
# tests. (It is assumed that the Google test and Google mock libraries are in  #
# the same directory.)                                                         #
#                                                                              #
# NOTE: If $(GTEST_INCLUDE) and $(GMOCK_INCLUDE) refer to the same             #
#       directory, we no longer need GMOCK_INCLUDE.                            #
#                                                                              #
# Example:                                                                     #
#   include $(UNIT_TESTS)/google_test.mk                                       #
#                      .                                                       #
#                      . (a bunch of stuff in your makefile)                   #
#                      .                                                       #
#   ifdef GMOCK_INCLUDE                                                        #
#   $(call add_to_includes,$(GMOCK_INCLUDE))                                   #
#   endif                                                                      #
#                                                                              #
#   LIBS += $(GMOCK_LIBS)                                                      #
#                      .                                                       #
#                      . (remainder of your makefile)                          #
#                      .                                                       #
#                                                                              #
# See:                                                                         #
# https://github.com/google/googletest/tree/main/googlemock                    #
# https://donsoft.io/gmock-presentation/                                       #
# https://stackoverflow.com/questions/1460703/comparison-of-arrays-in-google-test #
################################################################################
GMOCK_VERSION:=$(call get_static_lib_version,gmock)
GMOCK_INCLUDE ?= $(subst -I,,$(strip $(shell $(PKG_CONFIG) gmock --cflags-only-I 2> /dev/null)))
GMOCK_LIBS=$(strip $(shell $(PKG_CONFIG) gmock gmock_main --libs-only-l 2> /dev/null))

LIBS:=$(call enforce_whole_lib,$(LOCAL_LIB_STUB),$(LIBS))

################################################################################
# Google Test uses threads.                                                    #
################################################################################
ifeq ($(findstring $(PTHREAD),$(CXXFLAGS)),) # {
CXXFLAGS += $(PTHREAD)
endif # }

GTEST_MAIN ?= $(MAIN)
GTEST_EXE  ?= $(GTEST_MAIN:.cpp=)

GTEST_RESULTS     ?= $(EXE)_gtest_results.txt
GTEST_RESULTS_XML ?= $(EXE)_gtest_results.xml
GTEST_TITLE       ?= "$(EXE) Google Tests"

.PHONY: glist
glist: $(GTEST_EXE)
	@./$(GTEST_EXE) --list

GTESTS_LIST_FILE ?= $(GTEST_EXE)_google_test_list.txt
.PHONY: gtest_list
gtest_list: $(GTEST_EXE)
	@./$(GTEST_EXE) --list=$(GTESTS_LIST_FILE)
	@$(call open_sole_target_file,$(@),$(GTESTS_LIST_FILE),$(EDITOR),Google test list)

GTEST_EXE_OPTS ?= -t "$(GTEST_TITLE)" --gtest=$(GTEST_RESULTS)
.PHONY: gtest
gtest: unit_tests_setup $(GTEST_EXE)
	@./$(GTEST_EXE) $(GTEST_EXE_OPTS) || :
ifneq ($(filter gtest,$(MAKECMDGOALS)),) # {
	@T_SUITES=0; T_TESTS=0; T_PASSED=0; T_FAILURES=0; T_SKIPPED=0; T_TIME=0;                                                                              \
   { for TEST_SUITE in $$(grep "^\[-\{10\}\] [0-9]\+ tests from " $(GTEST_RESULTS) | grep -v " ([0-9]\+ ms total)" | sed 's/,//' | gawk '{print $$5}'); \
     do                                                                                                                                                 \
       ESC_SUITE=$$(echo $${TEST_SUITE} | sed 's@/@[/]@');                                                                                              \
       RESULTS=$$(sed -n "/\[-\{10\}\] [0-9]\+ tests from $${ESC_SUITE}/,/^\[-\{10\}\] [0-9]\+ tests from $${ESC_SUITE} ([0-9]\+ ms total)/p" $(GTEST_RESULTS) | \
       egrep -v "\[ *(RUN|OK) *\]");                                                                                                                    \
       TESTS=$$(echo $${RESULTS} | head -1 | gawk '{print $$2}');                                                                                       \
       FAILURES=$$(echo $${RESULTS} | sed 's/\[ *FAILED *\]/\nFAILED\n/g' | grep -c '^FAILED$$');                                                       \
       SKIPPED=$$(echo $${RESULTS} | sed 's/\[ *SKIPPED *\]/\nSKIPPED\n/g' | grep -c '^SKIPPED$$');                                                     \
       PASSED=$$((TESTS - FAILURES - SKIPPED));                                                                                                         \
       TYPE="";                                                                                                                                         \
       grep -q " from .*$${ESC_SUITE}.*TypeParam" $(GTEST_RESULTS);                                                                                     \
       if [[ 0 == $${?} ]];                                                                                                                             \
       then                                                                                                                                             \
         TYPE=$$(grep " from .*$${ESC_SUITE}" $(GTEST_RESULTS) | grep -v " ms " | sed 's/\(^.*TypeParam = \)\(.*$$\)/ Type → \2/');                     \
       fi;                                                                                                                                              \
       echo "[$(@)] $(INFO_LABEL) Test Suite     = [$${TEST_SUITE}$${TYPE}]";                                                                           \
       echo "[$(@)] $(INFO_LABEL) Test(s)        = [$${TESTS}]";                                                                                        \
       echo "[$(@)] $(INFO_LABEL) Passed Test(s) = [$${PASSED}]";                                                                                       \
       if [[ 0 == $${FAILURES} ]];                                                                                                                      \
       then                                                                                                                                             \
         echo "[$(@)] $(INFO_LABEL) Failed Test(s)    = [0]";                                                                                           \
       else                                                                                                                                             \
         echo "[$(@)] $(WARNING_LABEL) Failed Test(s) = [$${FAILURES}]";                                                                                \
       fi;                                                                                                                                              \
       echo "[$(@)] $(INFO_LABEL) Skipped Test(s) = [$${SKIPPED}]";                                                                                     \
       TIME=$$(grep "^\[-\{10\}\] [0-9]\+ tests from $${ESC_SUITE}" $(GTEST_RESULTS) |                                                                  \
               grep " ([0-9]\+ ms total)" | sed 's@,@@' | gawk '{print $$6}' | sed 's@(@@');                                                            \
       echo "[$(@)] $(INFO_LABEL) Time = [$${TIME} ms]";                                                                                                \
       T_SUITES=$$((T_SUITES + 1));                                                                                                                     \
       T_TESTS=$$((T_TESTS + TESTS));                                                                                                                   \
       T_PASSED=$$((T_PASSED + PASSED));                                                                                                                \
       T_FAILURES=$$((T_FAILURES + FAILURES));                                                                                                          \
       T_SKIPPED=$$((T_SKIPPED + SKIPPED));                                                                                                             \
       T_TIME=$$((T_TIME + TIME));                                                                                                                      \
     done;                                                                                                                                              \
     echo "[$(@)] $(INFO_LABEL) Test Suite(s) = [$${T_SUITES}]";                                                                                        \
     echo "[$(@)] $(INFO_LABEL) Total Test(s) = [$${T_TESTS}]";                                                                                         \
     echo "[$(@)] $(INFO_LABEL) Total Passing = [$${T_PASSED}]";                                                                                        \
     if [[ 0 == $${T_FAILURES} ]];                                                                                                                      \
     then                                                                                                                                               \
        echo "[$(@)] $(INFO_LABEL) Total Test Failure(s)    = [0]";                                                                                     \
     else                                                                                                                                               \
        echo "[$(@)] $(WARNING_LABEL) Total Test Failure(s) = [$${T_FAILURES}]";                                                                        \
     fi;                                                                                                                                                \
     echo "[$(@)] $(INFO_LABEL) Total Skipped Test(s) = [$${T_SKIPPED}]";                                                                               \
     echo "[$(@)] $(INFO_LABEL) Total Time = [$${T_TIME} ms]"; } | column -s"=" -o"=" -t
	@$(call open_file_command,$(GTEST_RESULTS),$(EDITOR),Google tests text results)
endif # }
	+@$(MAKE_NO_DIR) unit_tests_teardown

GTEST_EXE_XML_OPTS ?= --xml $(subst .txt,.xml,$(GTEST_EXE_OPTS))
.PHONY: xgtest
xgtest: unit_tests_setup $(GTEST_EXE)
	@$(if $(filter-out UNKNOWN,$(GTEST_VERSION)),GTEST_VERSION=$(GTEST_VERSION)) ./$(GTEST_EXE) $(GTEST_EXE_XML_OPTS) || :
	@$(call xml_beautify,$(GTEST_RESULTS_XML))
ifneq ($(filter xgtest,$(MAKECMDGOALS)),) # {
	@read -r TESTS FAILURES DISABLED ERRORS <<< $$(grep "^<testsuites " $(GTEST_RESULTS_XML)                               | \
   gawk '{print $$2, $$3, $$4, $$5 }'                                                                                    | \
   sed 's/\(^[a-z]\+="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\("$$\)/\2 \4 \6 \8/');        \
   SKIPPED=$$(grep -c ' result="skipped" ' $(GTEST_RESULTS_XML));                                                          \
   TEST_SUITES=$$(grep -c "<testsuite " $(GTEST_RESULTS_XML));                                                             \
   { echo "[$(@)] $(INFO_LABEL) Test Suite(s)    = [$${TEST_SUITES}]";                                                     \
   echo "[$(@)] $(INFO_LABEL) Total Test(s) = [$${TESTS}]";                                                                \
   PASSED=$$((TESTS - SKIPPED - DISABLED - FAILURES));                                                                     \
   echo "[$(@)] $(INFO_LABEL) Total Passing Test(s)    = [$${PASSED}]";                                                    \
   if [[ 0 == $${FAILURES} ]];                                                                                             \
   then                                                                                                                    \
      echo "[$(@)] $(INFO_LABEL) Total Test Failure(s)    = [0]";                                                          \
   else                                                                                                                    \
      echo "[$(@)] $(WARNING_LABEL) Total Test Failure(s) = [$${FAILURES}]";                                               \
   fi;                                                                                                                     \
   echo "[$(@)] $(INFO_LABEL) Total Disabled Test(s) = [$${DISABLED}]";                                                    \
   if [[ 0 == $${ERRORS} ]];                                                                                               \
   then                                                                                                                    \
      echo "[$(@)] $(INFO_LABEL) Total Test Error(s)    = [0]";                                                            \
   else                                                                                                                    \
      echo "[$(@)] $(WARNING_LABEL) Total Test Error(s) = [$${ERRORS}]";                                                   \
   fi;                                                                                                                     \
   echo "[$(@)] $(INFO_LABEL) Total Skipped Test(s) = [$${SKIPPED}]";                                                      \
   TIME=$$(grep '^<testsuites tests="' $(GTEST_RESULTS_XML) | gawk '{print $$6}' | sed 's/^.*="//;s/"//');                 \
   echo "[$(@)] $(INFO_LABEL) Total Time = [$${TIME} sec]";                                                                \
   for TEST_SUITE in $$(grep '^  <testsuite name="' $(GTEST_RESULTS_XML) | sed 's/\(^.*name="\)\(.*\)\(" test.*$$\)/\2/'); \
   do                                                                                                                      \
     ESC_SUITE=$$(echo $${TEST_SUITE} | sed 's@/@[/]@');                                                                   \
     RESULTS=$$(sed -n "/<testsuite name=\"$${ESC_SUITE}\"/,/<\/testsuite>/p" $(GTEST_RESULTS_XML));                       \
     SKIPPED=$$(echo $${RESULTS} | grep -c ' result="skipped" ');                                                          \
     read -r TESTS FAILURES DISABLED ERRORS <<< $$(echo $${RESULTS} | grep "<testsuite "                                 | \
     gawk '{print $$3, $$4, $$5, $$6 }'                                                                                  | \
     sed 's/\(^[a-z]\+="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\(.*="\)\([0-9]\+\)\("$$\)/\2 \4 \6 \8/');      \
     echo "[$(@)] $(INFO_LABEL) Test Suite = [$${TEST_SUITE}]";                                                            \
     echo "[$(@)] $(INFO_LABEL) Test(s) = [$${TESTS}]";                                                                    \
     if [[ 0 == "$${#FAILURES}" ]];                                                                                        \
     then                                                                                                                  \
       FAILURES=0;                                                                                                         \
     fi;                                                                                                                   \
     PASSED=$$((TESTS - FAILURES - SKIPPED - DISABLED));                                                                   \
     echo "[$(@)] $(INFO_LABEL) Passed Test(s) = [$${PASSED}]";                                                            \
     if [[ 0 == $${FAILURES} ]];                                                                                           \
     then                                                                                                                  \
       echo "[$(@)] $(INFO_LABEL) Failed Test(s) = [0]";                                                                   \
     else                                                                                                                  \
       echo "[$(@)] $(WARNING_LABEL) Failed Test(s) = [$${FAILURES}]";                                                     \
     fi;                                                                                                                   \
     echo "[$(@)] $(INFO_LABEL) Disabled Test(s) = [$${DISABLED}]";                                                        \
     if [[ 0 == $${ERRORS} ]];                                                                                             \
     then                                                                                                                  \
       echo "[$(@)] $(INFO_LABEL) Test Error(s)    = [0]";                                                                 \
     else                                                                                                                  \
       echo "[$(@)] $(WARNING_LABEL) Test Error(s) = [$${ERRORS}]";                                                        \
     fi;                                                                                                                   \
     echo "[$(@)] $(INFO_LABEL) Skipped Test(s) = [$${SKIPPED}]";                                                          \
     TIME=$$(grep "^  <testsuite name=\".*$${ESC_SUITE}" $(GTEST_RESULTS_XML) | gawk '{print $$7}' | sed 's/^.*="//;s/"//'); \
     echo "[$(@)] $(INFO_LABEL) Time = [$${TIME} sec]";                                                                    \
   done; } | column -s"=" -o"=" -t
	@$(call open_file_command,$(GTEST_RESULTS_XML),$(EDITOR),Google tests XML results)
endif # }
	+@$(MAKE_NO_DIR) unit_tests_teardown

.PHONY: xgtest_html
ifeq ($(PYTHON_COMMAND),) # {

xgtest_html:
	@$(warning [$(@)] $(WARNING_LABEL) The [$(PYTHON)] executable was not found; can not convert Google test XML results to HTML.)
	@:

else # } {

GTEST2HTML_SCRIPT=gtest2html.py
GTEST2HTML=$(call get_python_script,$(GTEST2HTML_SCRIPT))

ifneq ($(GTEST2HTML),) # {

XGTEST_HTML_FILE=$(GTEST2HTML_OUTPUT_DIR)/$(subst .xml,.html,$(GTEST_RESULTS_XML))
GTEST_FAVICON=$(IMAGE_PATH)/gtest_favicon.png

xgtest_html: DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
xgtest_html: xgtest
	@mkdir -p $(GTEST2HTML_OUTPUT_DIR)
	@$(PYTHON_COMMAND) -m $(subst .py,,$(GTEST2HTML_SCRIPT)) $(GTEST_RESULTS_XML) $(XGTEST_HTML_FILE) | \
   grep -v "^Warning: Attribute .* was not found inside xml node .*\. Set it to its default value"  | \
   sed 's/Html was generated successful\.$$/HTML was generated successfully./'
	@sed -i 's@${HOME}@$${HOME}@' $(XGTEST_HTML_FILE)
	@find $(dir $(XGTEST_HTML_FILE)) -type f -executable -exec chmod 644 {} \;
	@sed -i 's@Test Summary of AllTests@Test Summary of All Tests<br>Date/Time: $(DATE_TIME)<br>Google Test Version: $(GTEST_VERSION)@' $(XGTEST_HTML_FILE)
	@$(call add_favicon,$(GTEST_FAVICON),$(XGTEST_HTML_FILE))
	@$(if $(filter 1,$(words $(MAKECMDGOALS))),$(if $(filter $(@),$(MAKECMDGOALS)),$(call elapsed_time,$(@),$(TARGET_START_TIME))))
	@$(if $(filter 1,$(words $(MAKECMDGOALS))),$(if $(filter $(@),$(MAKECMDGOALS)),$(call open_file_command,$(XGTEST_HTML_FILE),$(BROWSER),Google Tests HTML file)))

gtest_clean::
	@rm -rf $(GTEST2HTML_OUTPUT_DIR)/css $(GTEST2HTML_OUTPUT_DIR)/js $(GTEST2HTML_OUTPUT_DIR)/open-iconic $(GTEST2HTML_OUTPUT_DIR)/stacktable $(XGTEST_HTML_FILE) $(GTESTS_LIST_FILE)
	@rmdir --ignore-fail-on-non-empty $(GTEST2HTML_OUTPUT_DIR) 2> /dev/null || :

google_test_mk_help::
	@echo '$(MAKE) xgtest_html → converts gtest XML output to HTML'

else # } {

xgtest_html: GTEST2HTML_URL=https://gitlab.uni-koblenz.de/agrt/gtest2html
xgtest_html:
	@$(warning [$(@)] $(WARNING_LABEL) The [$(GTEST2HTML_SCRIPT)] python script could not be found in PYTHONPATH = [${PYTHONPATH}]; obtain [$(GTEST2HTML_SCRIPT)] from [$(GTEST2HTML_URL)] and/or update the [PYTHONPATH] environment variable.)
	@:

endif # }
endif # }

gtest_clean::
	@rm -f $(GTEST_RESULTS) $(GTEST_RESULTS_XML)

clean:: gtest_clean

platform_info::
	@echo 'Google Mock version = [$(GMOCK_VERSION)]'
	@echo 'Google Test version = [$(GTEST_VERSION)]'

.PHONY: google_test_mk_help
google_test_mk_help::
	@echo '$(MAKE) gtest → execute the google test(s), saving results to [$(GTEST_RESULTS)]'
	@echo '$(MAKE) glist → list the Google tests on stdout'
	@echo '$(MAKE) gtest_list → save the list of Google tests to the file [$(GTESTS_LIST_FILE)]'
	@echo '$(MAKE) xgtest → execute the google test(s), saving results, as XML, to [$(GTEST_RESULTS_XML)]'

show_help:: google_test_mk_help

endif # }

