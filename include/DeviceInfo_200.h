
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "DeviceInfo.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 200
#include <CL/opencl.h>

class DeviceInfo_200 : public DeviceInfo
{
  public:

    DeviceInfo_200(
      const cl_device_id   nDeviceID,
      const size_t         nDeviceNumber,
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nDeviceVersion = 200) noexcept;

    virtual ~DeviceInfo_200() noexcept override;

    virtual void getTextOutput(std::ostream& tStream) const noexcept override;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept override;

  protected:

    typedef cl_bitfield cl_device_terminate_capability_khr;

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tBuiltInKernels;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tGlobalVariablePreferredTotaLSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tImageBaseAddressAlignment;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, size_t, OpenCLError, OpenCLToolTip>
      m_tMax1D2DImageArray;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax1DImageBuffer;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_uint,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tImagePitchAlignment;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tHasLinkerAvailable;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMaxGlobalVariableSize;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxOnDeviceEvents;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxOnDeviceQueues;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxPipeArgs;
    std::
      tuple<const OPEN_cl_device_info_200, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
        m_tMaxReadWriteImageArgs;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tCharVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tShortVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tIntVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tLongVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tFloatVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tDoubleVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tVectorWidthHalf;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tOpenCLCVersion;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_id,
      OpenCLError,
      OpenCLToolTip>
      m_tParentDeviceID;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_affinity_domain,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionAffinityDomain;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxSubDevices;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<cl_device_partition_property[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionTypesList;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<cl_device_partition_property[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionType;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPipeMaxActiveReservations;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tPipeMaxPacketSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tPreferredGlobalAtomicAlignment;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tPreferredUserSynchronization;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tPreferredLocalAtomicAlignment;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tPreferredPlatformAtomicAlignment;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredVectorWidthHalf;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, size_t, OpenCLError, OpenCLToolTip>
      m_tMaxPrintfBufferSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tQueueOnDeviceMaxSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tQueueOnDevicePreferredSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_command_queue_properties,
      OpenCLError,
      OpenCLToolTip>
      m_tQueueOnDeviceProperties;
    // qq
    //    std::tuple<const OPEN_cl_device_info_200, std::string, size_t,
    //    cl_command_queue_properties,                     OpenCLError,
    //    OpenCLToolTip>               m_tQueueOnHostProperties;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tReferenceCount;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tSPIRVersions;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_svm_capabilities,
      OpenCLError,
      OpenCLToolTip>
      m_tSVMCapabilities;
    // qq
    //    std::tuple<const OPEN_cl_device_info_200, std::string, size_t,
    //    cl_device_terminate_capability_khr,              OpenCLError,
    //    OpenCLToolTip>               m_tTerminateCapabilityKHR;

    virtual void getAllDeviceInfo() noexcept override;

  private:

    // Intentionally deleted.
    DeviceInfo_200()                          = delete;
    DeviceInfo_200(const DeviceInfo_200& rhs) = delete;
    DeviceInfo_200(DeviceInfo_200&& rhs)      = delete;
    DeviceInfo_200&       operator=(const DeviceInfo_200& rhs) = delete;
    DeviceInfo_200&       operator=(DeviceInfo_200&& rhs) = delete;
    DeviceInfo_200*       operator&()                     = delete;
    const DeviceInfo_200* operator&() const               = delete;
};

