
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo_120.h"

#include "OpenCLHTMLUtilities.h"
#include "cl_command_queue_properties.h"
#include "cl_device_affinity_domain.h"
#include "cl_device_fp_config.h"
#include "cl_device_partition_property.h"

DeviceInfo_120::DeviceInfo_120(
  const cl_device_id nDeviceID,
  const size_t       nDeviceNumber,
  cl_platform_id     tPlatformID,
  const size_t       nPlatformNumber) noexcept :
  DeviceInfo(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber, 120),
  m_tBuiltInKernels(
    OPEN_cl_device_info::OPENCL_DEVICE_BUILT_IN_KERNELS,
    "BUILT-IN KERNELS",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Built-in kernels")),
  m_tHalfPrecisionFloatingPointConfiguration(
    OPEN_cl_device_info::OPENCL_DEVICE_HALF_FP_CONFIG,
    "HALF PRECISION FLOATING POINT CONFIGURATION (OPTIONAL)",
    sizeof(cl_device_fp_config),
    cl_device_fp_config(0),
    OpenCLError(""),
    OpenCLToolTip("Optional half precision capability")),
  m_tHostUnifiedMemory(
    OPEN_cl_device_info_110_120::OPENCL_DEVICE_HOST_UNIFIED_MEMORY,
    "HAS UNIFIED MEMORY",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Do host and device have unified memory?")),
  m_tMax1DImageBuffer(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_MAX_BUFFER_SIZE,
    "MAX 1D IMAGE FROM BUFFER",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of pixels for a 1D image created from a buffer"),
    OpenCLUnits::m_tPixels),
  m_tMax1D2DImageArray(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_MAX_ARRAY_SIZE,
    "MAX IMAGES IN 1D/2D IMAGE ARRAY",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of images in a 1D or 2D image array")),
  m_tHasLinkerAvailable(
    OPEN_cl_device_info::OPENCL_DEVICE_LINKER_AVAILABLE,
    "HAS LINKER AVAILABLE",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is linker available?")),
  m_tMaxSimultaneousImageObjectsWrite(
    OPEN_cl_device_info_100::OPENCL_DEVICE_MAX_WRITE_IMAGE_ARGS,
    "MAX NUMBER SIMULTANEOUS IMAGE OBJECTS WRITE",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of simultaneous image objects that can be "
                  "written to by a kernel")),
  m_tCharVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
    "CHAR VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native char vector width")),
  m_tShortVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
    "SHORT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native short vector width")),
  m_tIntVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
    "INT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native int vector width")),
  m_tLongVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
    "LONG VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native long vector width")),
  m_tFloatVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
    "FLOAT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native float vector width")),
  m_tDoubleVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
    "DOUBLE VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native double vector width")),
  m_tVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
    "HALF VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, native half scalar and "
                  "vector width")),
  m_tOpenCLCVersion(
    OPEN_cl_device_info::OPENCL_DEVICE_OPENCL_C_VERSION,
    "OpenCL C VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("OpenCL C version")),
  m_tParentDeviceID(
    OPEN_cl_device_info::OPENCL_DEVICE_PARENT_DEVICE,
    "PARENT DEVICE ID",
    sizeof(cl_device_id),
    cl_device_id(nullptr),
    OpenCLError(""),
    OpenCLToolTip("cl_device_id of the parent device")),
  m_tMaxSubDevices(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_MAX_SUB_DEVICES,
    "MAXIMUM NUMBER OF SUB_DEVICES",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of sub-devices that can be created on a "
                  "partitioned device")),
  m_tPartitionTypesList(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_PROPERTIES,
    "PARTITION TYPE LIST",
    0,
    std::unique_ptr<cl_device_partition_property[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Partition types")),
  m_tPartitionAffinityDomain(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_AFFINITY_DOMAIN,
    "PARTITION AFFINITY DOMAIN",
    sizeof(cl_device_affinity_domain),
    cl_device_affinity_domain(0),
    OpenCLError(""),
    OpenCLToolTip("Affinity domains for partitioning the device")),
  m_tPartitionType(
    OPEN_cl_device_info::OPENCL_DEVICE_PARTITION_TYPE,
    "PARTITION TYPE",
    0,
    std::unique_ptr<cl_device_partition_property[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Properties argument specified in clCreateSubDevices")),
  m_tPreferredVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
    "HALF PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, preferred native half scalar and "
                  "vector width that can be put into vectors")),
  m_tMaxPrintfBufferSize(
    OPEN_cl_device_info::OPENCL_DEVICE_PRINTF_BUFFER_SIZE,
    "MAX PRINTF BUFFER SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of the internal buffer that holds the output "
                  "of printf")),
  m_tPreferredUserSynchronization(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_INTEROP_USER_SYNC,
    "PREFERRED USER SYNCHRONIZATION",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is the device's preference for the user to be "
                  "responsible for synchronization?")),
  m_tQueueProperties(
    OPEN_cl_device_info_100::OPENCL_DEVICE_QUEUE_PROPERTIES,
    "QUEUE PROPERTIES",
    sizeof(cl_command_queue_properties),
    cl_command_queue_properties(0),
    OpenCLError(""),
    OpenCLToolTip("Command-queue properties of the device.")),
  m_tReferenceCount(
    OPEN_cl_device_info::OPENCL_DEVICE_REFERENCE_COUNT,
    "REFERENCE COUNT",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Device reference count"))
{
}

DeviceInfo_120::~DeviceInfo_120() noexcept
{
  clReleaseDevice(std::get<PARAM_VALUE>(m_tParentDeviceID));
}

void DeviceInfo_120::getAllDeviceInfo() noexcept
{
  DeviceInfo::getAllDeviceInfo();

  getDeviceInfo(m_tBuiltInKernels);
  getDeviceInfo(m_tHalfPrecisionFloatingPointConfiguration);
  getDeviceInfo(m_tHostUnifiedMemory);
  getDeviceInfo(m_tMax1DImageBuffer);
  getDeviceInfo(m_tMax1D2DImageArray);
  getDeviceInfo(m_tHasLinkerAvailable);
  getDeviceInfo(m_tMaxSimultaneousImageObjectsWrite);
  getDeviceInfo(m_tCharVectorWidth);
  getDeviceInfo(m_tShortVectorWidth);
  getDeviceInfo(m_tIntVectorWidth);
  getDeviceInfo(m_tLongVectorWidth);
  getDeviceInfo(m_tFloatVectorWidth);
  getDeviceInfo(m_tDoubleVectorWidth);
  getDeviceInfo(m_tVectorWidthHalf);
  getDeviceInfo(m_tOpenCLCVersion);
  getDeviceInfo(m_tParentDeviceID);
  getDeviceInfo(m_tMaxSubDevices);
  getDeviceInfo(m_tPartitionTypesList);
  getDeviceInfo(m_tPartitionAffinityDomain);
  getDeviceInfo(m_tPartitionType);
  getDeviceInfo(m_tPreferredVectorWidthHalf);
  getDeviceInfo(m_tMaxPrintfBufferSize);
  getDeviceInfo(m_tPreferredUserSynchronization);
  getDeviceInfo(m_tQueueProperties);
  getDeviceInfo(m_tReferenceCount);
}

void DeviceInfo_120::getTextOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getTextOutput(tStream);

  const std::string        sHorizontalIndent(static_cast<size_t>(m_nMaxDescriptionSize) + 4, ' ');
  std::vector<std::string> tInfo;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tBuiltInKernels)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tBuiltInKernels).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tBuiltInKernels))
    {
      if ('\0' != std::get<PARAM_VALUE>(m_tBuiltInKernels)[0])
      {
        std::string sBuiltInKernels(std::get<PARAM_VALUE>(m_tBuiltInKernels).get());
        DeviceInfo::extractTokens(tInfo, sBuiltInKernels, ';');
        if (false == tInfo.empty())
        {
          DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
        }
      }
      else
      {
        tStream << "No Built-in Kernels found.";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tBuiltInKernels);
  }
  tStream << std::endl;

  DeviceInfo::writePrecisionInfo(
    tStream,
    m_nMaxDescriptionSize,
    tInfo,
    sIndent2,
    sHorizontalIndent,
    "No Half Precision Floating Point Capability found.",
    m_tHalfPrecisionFloatingPointConfiguration);

  tStream << m_tHostUnifiedMemory << std::endl;
  tStream << m_tMax1DImageBuffer << std::endl;
  tStream << m_tMax1D2DImageArray << std::endl;
  tStream << m_tHasLinkerAvailable << std::endl;
  tStream << m_tMaxSimultaneousImageObjectsWrite << std::endl;
  tStream << m_tCharVectorWidth << std::endl;
  tStream << m_tShortVectorWidth << std::endl;
  tStream << m_tIntVectorWidth << std::endl;
  tStream << m_tLongVectorWidth << std::endl;
  tStream << m_tFloatVectorWidth << std::endl;
  tStream << m_tDoubleVectorWidth << std::endl;
  tStream << m_tVectorWidthHalf << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tOpenCLCVersion)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tOpenCLCVersion).empty())
  {
    tStream << std::get<PARAM_VALUE>(m_tOpenCLCVersion).get();
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tOpenCLCVersion);
  }
  tStream << std::endl;

  tStream << m_tParentDeviceID;
  if (
    (true == std::get<PARAM_ERROR>(m_tParentDeviceID).empty()) &&
    (nullptr == std::get<PARAM_VALUE>(m_tParentDeviceID)))
  {
    tStream << " (ROOT-LEVEL DEVICE)";
  }
  tStream << std::endl;

  tStream << m_tMaxSubDevices << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tPartitionTypesList) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionTypesList).empty())
  {
    const size_t nArrayLength =
      std::get<PARAM_SIZE>(m_tPartitionTypesList) / sizeof(cl_device_partition_property);
    for (size_t i = 0; i < nArrayLength; ++i)
    {
      tInfo.push_back(cl_device_partition_property_ToString(
        *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No Partition Types List found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionTypesList) << '\n';
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tPartitionAffinityDomain) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionAffinityDomain).empty())
  {
    for (auto i = OPEN_cl_device_affinity_domain::BEGIN; i != OPEN_cl_device_affinity_domain::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tPartitionAffinityDomain) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No Partition Affinity Domain information found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionAffinityDomain) << '\n';
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tPartitionType)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tPartitionType).empty())
  {
    if (0 == *std::get<PARAM_VALUE>(m_tPartitionType).get())
    {
      tStream << "No partition type information found.\n";
    }
    else
    {
      const size_t nArrayLength =
        std::get<PARAM_SIZE>(m_tPartitionType) / sizeof(cl_device_partition_property);
      for (size_t i = 0; i < nArrayLength; ++i)
      {
        tInfo.push_back(cl_device_partition_property_ToString(
          *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
      }
      if (false == tInfo.empty())
      {
        DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
      }
      else
      {
        tStream << "No partition type information found.\n";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tPartitionType) << '\n';
  }

  tStream << m_tPreferredVectorWidthHalf << std::endl;
  tStream << m_tMaxPrintfBufferSize << std::endl;
  tStream << m_tPreferredUserSynchronization << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tQueueProperties) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tQueueProperties).empty())
  {
    for (auto i = OPEN_cl_command_queue_properties::BEGIN;
         i != OPEN_cl_command_queue_properties::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tQueueProperties) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorHorizontally(tStream, tInfo);
    }
    else
    {
      tStream << "No Queue Properties found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tQueueProperties) << '\n';
  }

  tStream << m_tReferenceCount;
  if (true == std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability).empty())
  {
    tStream << ((1 == std::get<PARAM_VALUE>(m_tReferenceCount)) ? " (ROOT-LEVEL DEVICE)" : "");
  }
  tStream << std::endl;
}

void DeviceInfo_120::getHTMLOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo::getHTMLOutput(tStream);

  std::vector<std::string> tInfo;

  DeviceInfo::writeHTMLTableRow(tStream, m_tBuiltInKernels, "No Built-in Kernels found.", ';');

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tHalfPrecisionFloatingPointConfiguration,
    OPEN_cl_device_fp_config::BEGIN,
    OPEN_cl_device_fp_config::END,
    "No Half Precision Floating Point Capability found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tHostUnifiedMemory);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax1DImageBuffer);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax1D2DImageArray);
  DeviceInfo::writeHTMLTableRow(tStream, m_tHasLinkerAvailable);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSimultaneousImageObjectsWrite);
  DeviceInfo::writeHTMLTableRow(tStream, m_tCharVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tShortVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tIntVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tLongVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tFloatVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tDoubleVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tOpenCLCVersion);

  if (true == std::get<PARAM_ERROR>(m_tParentDeviceID).empty())
  {
    std::stringstream tParentDeviceIDInfo;
    tParentDeviceIDInfo << std::get<PARAM_VALUE>(m_tParentDeviceID);
    if (
      (0 != std::get<PARAM_SIZE>(m_tParentDeviceID)) &&
      (nullptr == std::get<PARAM_VALUE>(m_tParentDeviceID)))
    {
      tParentDeviceIDInfo << " (ROOT-LEVEL DEVICE)";
    }
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tParentDeviceID),
      tParentDeviceIDInfo.str(),
      std::get<PARAM_TOOL_TIP>(m_tParentDeviceID));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tParentDeviceID),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tParentDeviceID)),
      std::get<PARAM_TOOL_TIP>(m_tParentDeviceID));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSubDevices);

  if (true == std::get<PARAM_ERROR>(m_tPartitionTypesList).empty())
  {
    const size_t nArrayLength =
      std::get<PARAM_SIZE>(m_tPartitionTypesList) / sizeof(cl_device_partition_property);
    for (size_t i = 0; i < nArrayLength; ++i)
    {
      tInfo.push_back(cl_device_partition_property_ToString(
        *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
    }
    if (false == tInfo.empty())
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tPartitionTypesList),
        toHTMLTokens(tInfo),
        std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
      tInfo.clear();
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tPartitionTypesList),
        "No Partition Types List found.",
        std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tPartitionTypesList),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tPartitionTypesList)),
      std::get<PARAM_TOOL_TIP>(m_tPartitionTypesList));
  }

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tPartitionAffinityDomain,
    OPEN_cl_device_affinity_domain::BEGIN,
    OPEN_cl_device_affinity_domain::END,
    "No Partition Affinity Domain information found.");

  if (true == std::get<PARAM_ERROR>(m_tPartitionType).empty())
  {
    std::stringstream tPartitionTypeInfo;
    if (
      (0 == std::get<PARAM_SIZE>(m_tPartitionType)) ||
      (0 == *std::get<PARAM_VALUE>(m_tPartitionType).get()))
    {
      tPartitionTypeInfo << "No partition type information found.";
    }
    else
    {
      const size_t nArrayLength =
        std::get<PARAM_SIZE>(m_tPartitionType) / sizeof(cl_device_partition_property);
      for (size_t i = 0; i < nArrayLength; ++i)
      {
        tInfo.push_back(cl_device_partition_property_ToString(
          *(std::get<PARAM_VALUE>(m_tPartitionTypesList).get() + i)));
      }
      if (false == tInfo.empty())
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tPartitionType),
          toHTMLTokens(tInfo),
          std::get<PARAM_TOOL_TIP>(m_tPartitionType));
        tInfo.clear();
      }
      else
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tPartitionType),
          "No partition type information found.",
          std::get<PARAM_TOOL_TIP>(m_tPartitionType));
      }
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tPartitionType),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tPartitionType)),
      std::get<PARAM_TOOL_TIP>(m_tPartitionType));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxPrintfBufferSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredUserSynchronization);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tQueueProperties,
    OPEN_cl_command_queue_properties::BEGIN,
    OPEN_cl_command_queue_properties::END,
    "No Queue Properties found.");

  std::stringstream tReferenceCountValue;
  if (true == std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability).empty())
  {
    tReferenceCountValue << std::get<PARAM_VALUE>(m_tReferenceCount);
    if (0 != std::get<PARAM_SIZE>(m_tSinglePrecisionFloatingPointCapability))
    {
      if (1 == std::get<PARAM_VALUE>(m_tReferenceCount))
      {
        tReferenceCountValue << " (ROOT-LEVEL DEVICE)";
      }
    }
  }
  else
  {
    tReferenceCountValue << DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tReferenceCount));
  }
  setHTMLTableRow(
    tStream,
    std::get<PARAM_DESC>(m_tReferenceCount),
    tReferenceCountValue,
    std::get<PARAM_TOOL_TIP>(m_tReferenceCount));
}

