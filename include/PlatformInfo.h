
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <algorithm>
#include <cstring>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "DeviceID.h"
#include "DeviceInfo.h"
#include "cl_device_type.h"
#include "cl_platform_info.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 220
#include <CL/opencl.h>

class PlatformInfo
{
  public:

    void init();

    virtual ~PlatformInfo() noexcept;

    void getTextOutput(std::ostream& tStream) const noexcept;

    void getHTMLOutput(std::ostream& tStream, const size_t nPlatform) const noexcept;

    inline unsigned short platformVersion() const
    {
      return (m_nPlatformVersion);
    }

    friend std::ostream& operator<<(std::ostream& tStream, const PlatformInfo& rhs) noexcept;

  protected:

    cl_platform_id                           m_tPlatformID;
    const size_t                             m_nPlatformNumber;
    std::map<cl_platform_info, std::string>  m_tPlatformInfo;
    std::vector<std::shared_ptr<DeviceID>>   m_pDeviceIDs;
    std::vector<std::shared_ptr<DeviceID>>   m_pBadDeviceIDs;
    std::vector<std::shared_ptr<DeviceInfo>> m_pDeviceInfo;
    const unsigned short                     m_nPlatformVersion;
    std::vector<OPEN_cl_platform_info>       m_tPlatformInfoTypes;
    std::vector<OPEN_cl_device_type>         m_tDeviceTypes;

    PlatformInfo(
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nPlatformVersion);

    void add(const cl_platform_info eInfoEnum, const std::string& sPlatformInfo);

    void populatePlatformExtensions(std::vector<std::string>& tPlatformExtensions) const noexcept;

    std::string find(const cl_platform_info eInfoEnum) const noexcept;

    virtual void getPlatformAttributes() = 0;

    virtual void populateDeviceTypes();
    virtual void populatePlatformInfoTypes();

    void getDeviceIds() noexcept;

    void getDeviceInfo() noexcept;

    cl_int getDeviceIDInfo(
      std::shared_ptr<DeviceID> tDeviceID,
      const cl_platform_id      tPlatformID,
      const cl_device_type      eDeviceType,
      const cl_uint             nNumMatchingDevices) noexcept;

    static DeviceInfo* deviceInfoFactory(
      const cl_device_id   nDeviceID,
      const size_t         nDeviceNumber,
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nPlatformVersion);

  private:

    // Intentionally deleted.
    PlatformInfo()                        = delete;
    PlatformInfo(const PlatformInfo& rhs) = delete;
    PlatformInfo(PlatformInfo&& rhs)      = delete;
    PlatformInfo&       operator=(const PlatformInfo& rhs) = delete;
    PlatformInfo&       operator=(PlatformInfo&& rhs) = delete;
    PlatformInfo*       operator&()                   = delete;
    const PlatformInfo* operator&() const             = delete;
};

