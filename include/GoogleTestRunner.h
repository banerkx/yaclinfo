
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of clinfo.
 *
 *  clinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  clinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with clinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>

// Example:
//  PRIVATE_GTEST(FileReceiverGoogleTest, ToStringTest)
// will expand to:
//  TEST_F(FileReceiverGoogleTest, ToStringTest) {
//  FileReceiverGoogleTest::ToStringTest(); }
#define PRIVATE_GTEST(x, y) \
  TEST_F(x, y)              \
  {                         \
    x::y();                 \
  }

// Use this macro to skip a Google test.
#define SKIP_PRIVATE_GTEST(x, y) \
  TEST_F(x, y)                   \
  {                              \
    GTEST_SKIP();                \
    x::y();                      \
  }

// Example:
//  PRIVATE_TYPED_GTEST(FileReceiverGoogleTest, ToStringTest)
// will expand to:
//  TYPED_TEST(FileReceiverGoogleTest, ToStringTest) {
//  FileReceiverGoogleTest<T>::ToStringTest(); }
#define PRIVATE_TYPED_GTEST(x, y) \
  TYPED_TEST(x, y)                \
  {                               \
    x<TypeParam>::y();            \
  }

// Use this macro to skip a templatetized Google test.
#define SKIP_PRIVATE_TYPED_GTEST(x, y) \
  TYPED_TEST(x, y)                     \
  {                                    \
    GTEST_SKIP();                      \
    x<TypeParam>::y();                 \
  }

namespace GoogleTestRunner
{

  /**
   * usage
   * @brief prints usage information to stdout and exits
   * @param sProgramName - the program name
   * @param sDefaultTestsResultsFile - default test results file
   * @param sDefaultTestsTitle - default tests title
   * @param nExitValue - exit value
   */
  void usage(
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const short        nExitValue);

  /**
   * createOptionsString
   * @brief assembles the command line options string
   * @param pOptions - holds the command line options information
   * @return command line options string meant to be used by getopt_long()
   */
  std::string createOptionsString(const struct option* pOptions);

  /**
   * handleOptionError
   * @brief processes a command line option for error
   * @param cOpt - the command line option in question
   * @param sProgramName - the program name
   * @param sDefaultTestsResultsFile - default test results file
   * @param sDefaultTestsTitle - default tests title
   * @param sOptions - command line options
   */
  void handleOptionError(
    const char         cOpt,
    const std::string& sProgramName,
    const std::string& sDefaultTestsResultsFile,
    const std::string& sDefaultTestsTitle,
    const std::string& sOptions);

  /**
   * ensureFileExtension
   * @brief ensures the the input file has the specified extension
   * @param sFile - input file
   * @param pExt - file extension
   */
  void ensureFileExtension(std::string& sFile, const char* pExt);

  /**
   * ensureTextExtension
   * @brief ensures the the input file has a ".txt" extension
   * @param sFile - input file
   */
  void ensureTextExtension(std::string& sFile);

  /**
   * ensureXMLExtension
   * @brief ensures the the input file has an ".xml" extension
   * @param sFile - input file
   */
  void ensureXMLExtension(std::string& sFile);

  /**
   * ensureHTMLExtension
   * @brief ensures the the input file has an ".html" extension
   * @param sFile - input file
   */
  void ensureHTMLExtension(std::string& sFile);

  /**
   * processCommandLineOptions
   * @brief processes command line options
   * @param bDebug - flag for debugger break points
   * @param sDefaultTestsResultsFile - default name for the tests results file
   * @param sTestsResultsFile - will hold the name of the tests results file
   * @param bListTests - flag for simply listing the Google tests
   * @param bNoElapsedTime - flag for printing the elapsed tests' times
   * @param nRepeatCount - number of times for repeating the tests
   * @param bRandomTests - flag for executing the tests in a random order
   * @param sDefaultTestsTitle - default tests title
   * @param sTestsTitle - will hold the tests title
   * @param bXML - flag for saving tests results in XML format
   * @param argc - size of argv array
   * @param argv - array of command line arguments
   */
  void processCommandLineOptions(
    bool&              bDebug,
    const std::string& sDefaultTestsResultsFile,
    std::string&       sTestsResultsFile,
    bool&              bListTests,
    bool&              bNoElapsedTime,
    unsigned short&    nRepeatCount,
    bool&              bRandomTests,
    const std::string& sDefaultTestsTitle,
    std::string&       sTestsTitle,
    bool&              bXML,
    int                argc,
    char**             argv);

  /**
   * deleteGoogleOptionsMemory
   * @brief deletes the memory allocated for Google test
   * @param gtest_argv - array of Google tests options
   * @param tMemory - holds pointers for Google test memory allocations
   */
  void deleteGoogleOptionsMemory(char**& gtest_argv, std::vector<char*>& tMemory);

  /**
   * addToGoogleTestsArgv
   * @brief creates the command line options for Google test's InitGoogleTest()
   * function
   * @param gtest_argc - size of array gtest_argv
   *                     NOTE: This size is incremented by this function.
   * @param gtest_argv - array of Google tests options
   * @param pArgument - the command line argument to add
   * @param tMemory - holds pointers for Google test memory allocations
   */
  void addToGoogleTestsArgv(
    int&                gtest_argc,
    char**              gtest_argv,
    const char*         pArgument,
    std::vector<char*>& tMemory);

  /**
   * executeGoogleTests
   * @brief execute the Google tests
   * @param bDebug - flag for debugger break points
   * @param sTestsResultsFile -  the name of the tests results file
   * @param bListTests - flag for simply listing the Google tests
   * @param bNoElapsedTime - flag for printing the elapsed tests' times
   * @param nRepeatCount - number of times for repeating the tests
   * @param bRandomTests - flag for executing the tests in a random order
   * @param sTestsTitle - the tests title
   * @param bXML - flag for saving tests results in XML format
   * @param argv - array of Google tests options
   * @return status of executing the Google tests
   */
  int executeGoogleTests(
    const bool&           bDebug,
    std::string           sTestsResultsFile,
    const bool&           bListTests,
    const bool&           bNoElapsedTime,
    const unsigned short& nRepeatCount,
    const bool&           bRandomTests,
    const std::string&    sTestsTitle,
    const bool&           bXML,
    char**                argv);

  /**
   * googleTestHelp
   * @brief prints Google test help information
   */
  int googleTestHelp();

  /**
   * getCurrentDateTime
   * @brief returns the current date and time
   */
  std::string getCurrentDateTime();

  /**
   * getGoogleTestVersion
   * @brief tries to get the google test version
   */
  std::string getGoogleTestVersion();
}  // namespace GoogleTestRunner

