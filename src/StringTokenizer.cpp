
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StringTokenizer.h"

#include <algorithm>
#include <boost/algorithm/string/trim.hpp>
#include <iterator>
#include <sstream>

StringTokenizer::StringTokenizer(
  const std::string& inString,
  const std::string& inDelimiters) noexcept :
  m_tTokens(), m_sString(inString), m_sDelimiters(inDelimiters)
{
  tokenize();
}

StringTokenizer::StringTokenizer() noexcept : m_tTokens(), m_sString(), m_sDelimiters()
{
}

StringTokenizer::StringTokenizer(const std::string& inString, const char inDelimiter) noexcept :
  m_tTokens(), m_sString(inString), m_sDelimiters(1, inDelimiter)
{
  tokenize();
}

StringTokenizer::StringTokenizer(const StringTokenizer& rhs) noexcept :
  m_tTokens(rhs.m_tTokens), m_sString(rhs.m_sString), m_sDelimiters(rhs.m_sDelimiters)
{
}

StringTokenizer::StringTokenizer(StringTokenizer&& rhs) noexcept :
  m_tTokens(rhs.m_tTokens), m_sString(rhs.m_sString), m_sDelimiters(rhs.m_sDelimiters)
{
  rhs.m_tTokens.clear();
  rhs.m_sString.clear();
  rhs.m_sDelimiters.clear();
}

StringTokenizer& StringTokenizer::operator=(const StringTokenizer& rhs) noexcept
{
  if (this != &rhs)
  {
    m_tTokens     = rhs.m_tTokens;
    m_sString     = rhs.m_sString;
    m_sDelimiters = rhs.m_sDelimiters;
  }
  return (*this);
}

StringTokenizer& StringTokenizer::operator=(StringTokenizer&& rhs) noexcept
{
  if (this != &rhs)
  {
    m_tTokens     = rhs.m_tTokens;
    m_sString     = rhs.m_sString;
    m_sDelimiters = rhs.m_sDelimiters;

    rhs.m_tTokens.clear();
    rhs.m_sString.clear();
    rhs.m_sDelimiters.clear();
  }
  return (*this);
}

void StringTokenizer::reset(const std::string& sNewString) noexcept
{
  m_sString = sNewString;
  tokenize();
}

void StringTokenizer::reset(
  const std::string& sNewString,
  const std::string& sNewDelimiters) noexcept
{
  m_sString     = sNewString;
  m_sDelimiters = sNewDelimiters;
  tokenize();
}

void StringTokenizer::reset(const std::string& sNewString, const char cNewDelimiter) noexcept
{
  m_sString     = sNewString;
  m_sDelimiters = std::string(1, cNewDelimiter);
  tokenize();
}

void StringTokenizer::tokenize() noexcept
{
  m_tTokens.clear();
  if ((true == m_sDelimiters.empty()) || (true == m_sString.empty()))
  {
    return;
  }

  boost::char_separator<char> tSeparators(m_sDelimiters.c_str());

  // NOTE: Empty tokens are ignored.
  boost::tokenizer<boost::char_separator<char>> tTokenizer(m_sString, tSeparators);

  for (boost::tokenizer<boost::char_separator<char>>::iterator i = tTokenizer.begin();
       i != tTokenizer.end();
       ++i)
  {
    m_tTokens.push_back(*i);
  }
}

std::ostream& StringTokenizer::printTokens(std::ostream& tStream) const noexcept
{
  tStream << "NUMBER OF TOKENS: [" << m_tTokens.size() << "]" << std::endl;
  tStream << (*this) << std::endl;
  return (tStream);
}

std::ostream& operator<<(std::ostream& tStream, const StringTokenizer& tTokenizer) noexcept
{
  std::string sSeparator(1, tTokenizer.m_sDelimiters[0]);
  tStream << "[";
  std::copy(
    tTokenizer.m_tTokens.begin(),
    tTokenizer.m_tTokens.end() - 1,
    std::ostream_iterator<std::string>(tStream, sSeparator.c_str()));
  tStream << *(tTokenizer.m_tTokens.end() - 1) << "]";
  return (tStream);
}

std::string StringTokenizer::getToken(const size_t nIndex)
{
  return (*this)[nIndex];
}

std::string StringTokenizer::operator[](const size_t nIndex)
{
  if (nIndex >= m_tTokens.size())
  {
    std::stringstream errorMessage;
    errorMessage << "The specified index [" << nIndex << "] must be < [" << m_tTokens.size() << "]";
    throw std::out_of_range(errorMessage.str());
  }

  return (m_tTokens[nIndex]);
}

size_t StringTokenizer::getMaxTokenLength() const
{
  size_t nMaxLength = 0;
  for (const auto& tToken : m_tTokens)
  {
    nMaxLength = std::max(nMaxLength, tToken.size());
  }
  return (nMaxLength);
}

const std::string& StringTokenizer::at(const size_t nIndex)
{
  return (m_tTokens[nIndex]);
}

void StringTokenizer::trim()
{
  std::for_each(m_tTokens.begin(), m_tTokens.end(), StringTokenizer::trimWhiteSpace);

  // Removing empty tokens.
  std::vector<std::string>::iterator i = std::remove_if(
    m_tTokens.begin(),
    m_tTokens.end(),
    [](const std::string& s) { return (s.empty()); });
  m_tTokens.erase(i, m_tTokens.end());
}

void StringTokenizer::trimWhiteSpace(std::string& s)
{
  boost::algorithm::trim(s);
}

