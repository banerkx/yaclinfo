
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "OpenCLHTMLUtilities.h"

#include <cstdint>
#include <iostream>
#include <map>
#include <sstream>

namespace
{
  class ScopedHTMLTableRow
  {
    public:

      explicit ScopedHTMLTableRow(std::ostream& tStream) : m_tStream(tStream)
      {
        m_tStream << "  <tr>\n";
      }

      ~ScopedHTMLTableRow()
      {
        m_tStream << "  </tr>\n";
      }

      void addName(const std::string& sName)
      {
        m_tStream << "    <td>" << sName << "</td>\n";
      }

      void addNameAndToolTip(const std::string& sName, const OpenCLToolTip& tToolTip)
      {
        m_tStream << "    <td><div class=\"tooltip\">" << sName << "<span class=\"tooltiptext\">"
                  << tToolTip << "</span></div></td>\n";
      }

      template<class T> void addDataCell(const T& tData) noexcept
      {
        m_tStream << "    <td>" << tData << "</td>\n";
      }

      template<class T> void addDataCell(const T& tData, const OpenCLUnits& tUnits) noexcept
      {
        m_tStream << "    <td>" << tData << getHTMLUnits(tUnits) << "</td>\n";
      }

      void addDataCell(const char* sData) noexcept
      {
        m_tStream << "    <td>" << sData << "</td>\n";
      }

      void addDataCell(const char* sData, const OpenCLUnits& tUnits) noexcept
      {
        m_tStream << "    <td>" << sData << getHTMLUnits(tUnits) << "</td>\n";
      }

    private:

      std::ostream& m_tStream;

      // Intentionally deleted.
      ScopedHTMLTableRow()                              = delete;
      ScopedHTMLTableRow(const ScopedHTMLTableRow& rhs) = delete;
      ScopedHTMLTableRow(ScopedHTMLTableRow&& rhs)      = delete;
      ScopedHTMLTableRow&       operator=(const ScopedHTMLTableRow& rhs) = delete;
      ScopedHTMLTableRow&       operator=(ScopedHTMLTableRow&& rhs) = delete;
      ScopedHTMLTableRow*       operator&()                         = delete;
      const ScopedHTMLTableRow* operator&() const                   = delete;
  };

  template<> void ScopedHTMLTableRow::addDataCell(const std::stringstream& tData) noexcept
  {
    m_tStream << "    <td>" << tData.str() << "</td>\n";
  }

  // NOTE: cl_uint is equivalent to unsigned int.
  template<> void ScopedHTMLTableRow::addDataCell(const cl_uint& tData) noexcept
  {
    ScopedNumericFormatter tFormatter(m_tStream);
    m_tStream << "    <td>" << tData << "</td>\n";
  }

  // NOTE: cl_ulong is equivalent to size_t.
  template<> void ScopedHTMLTableRow::addDataCell(const cl_ulong& tData) noexcept
  {
    ScopedNumericFormatter tFormatter(m_tStream);
    m_tStream << "    <td>" << tData << "</td>\n";
  }

  template<> void ScopedHTMLTableRow::addDataCell(const std::unique_ptr<char[]>& tData) noexcept
  {
    addDataCell(tData.get());
  }

  template<> void ScopedHTMLTableRow::addDataCell(const cl_platform_id& tData) noexcept
  {
    m_tStream << "    <td>" << tData << "</td>\n";
  }

  template<> void ScopedHTMLTableRow::addDataCell(
    const std::stringstream& tData,
    const OpenCLUnits&       tUnits) noexcept
  {
    m_tStream << "    <td>" << tData.str() << getHTMLUnits(tUnits) << "</td>\n";
  }

  // NOTE: cl_uint is equivalent to unsigned int.
  template<>
  void ScopedHTMLTableRow::addDataCell(const cl_uint& tData, const OpenCLUnits& tUnits) noexcept
  {
    ScopedNumericFormatter tFormatter(m_tStream);
    m_tStream << "    <td>" << tData << getHTMLUnits(tUnits) << "</td>\n";
  }

  // NOTE: cl_ulong is equivalent to size_t.
  template<>
  void ScopedHTMLTableRow::addDataCell(const cl_ulong& tData, const OpenCLUnits& tUnits) noexcept
  {
    ScopedNumericFormatter tFormatter(m_tStream);
    m_tStream << "    <td>" << tData << getHTMLUnits(tUnits) << "</td>\n";
  }

  template<> void ScopedHTMLTableRow::addDataCell(
    const std::unique_ptr<char[]>& tData,
    const OpenCLUnits&             tUnits) noexcept
  {
    addDataCell(tData.get(), tUnits);
  }

  template<> void ScopedHTMLTableRow::addDataCell(
    const cl_platform_id& tData,
    const OpenCLUnits&    tUnits) noexcept
  {
    ScopedNumericFormatter tFormatter(m_tStream);
    m_tStream << "    <td>" << (tData) << getHTMLUnits(tUnits) << "</td>\n";
  }

  const std::string m_sBits(" <i>bits</i>");
  const std::string m_sBytes(" <i>bytes</i>");
  const std::string m_sGigaBytes(" &#13191;");             // GB
  const std::string m_sGigaHertz(" &#13203;");             // GHz
  const std::string m_sKiloBytes(" &#13189;");             // kB
  const std::string m_sMegaHertz(" &#13202;");             // MHz
  const std::string m_sMicroSeconds(" <i>&#181;sec</i>");  // µsec
  const std::string m_sNanoSeconds(" &#13233;");           // ns
  const std::string m_sSeconds(" <i>seconds</i>");
  const std::string m_sPages(" <i>pages</i>");
  const std::string m_sPixels(" <i>pixels</i>");
}  // namespace

//
// For those HTML rows that do not need a tool tip nor units.
//
template<class T>
void setHTMLTableRow(std::ostream& tStream, const std::string& sName, const T& tData) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addName(sName);
  tTableRowTag.addDataCell(tData);
}

void setHTMLTableRow(std::ostream& tStream, const std::string& sName, const char* sData) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addName(sName);
  tTableRowTag.addDataCell(sData);
}

//
// For those HTML rows that need a tool tip but no units.
//
template<class T> void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const T&             tData,
  const OpenCLToolTip& tToolTip) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addNameAndToolTip(sName, tToolTip);
  tTableRowTag.addDataCell(tData);
}

void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const char*          sData,
  const OpenCLToolTip& tToolTip) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addNameAndToolTip(sName, tToolTip);
  tTableRowTag.addDataCell(sData);
}

//
// For those HTML rows that don't need a tool tip but need units.
//
template<class T> void setHTMLTableRow(
  std::ostream&      tStream,
  const std::string& sName,
  const T&           tData,
  const OpenCLUnits& tUnits) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addName(sName);
  tTableRowTag.addDataCell(tData, tUnits);
}

void setHTMLTableRow(
  std::ostream&      tStream,
  const std::string& sName,
  const char*        sData,
  const OpenCLUnits& tUnits) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addName(sName);
  tTableRowTag.addDataCell(sData, tUnits);
}

//
// For those HTML rows that need a tool tip *and* units.
//
template<class T> void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const T&             tData,
  const OpenCLToolTip& tToolTip,
  const OpenCLUnits&   tUnits) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addNameAndToolTip(sName, tToolTip);
  tTableRowTag.addDataCell(tData, tUnits);
}

void setHTMLTableRow(
  std::ostream&        tStream,
  const std::string&   sName,
  const char*          sData,
  const OpenCLToolTip& tToolTip,
  const OpenCLUnits&   tUnits) noexcept
{
  ScopedHTMLTableRow tTableRowTag(tStream);
  tTableRowTag.addNameAndToolTip(sName, tToolTip);
  tTableRowTag.addDataCell(sData, tUnits);
}

std::string toHTMLTokens(const std::vector<std::string>& tTokens)
{
  if (false == tTokens.empty())
  {
    std::stringstream tHTMLTokens;
    for (size_t i = 0; i < tTokens.size() - 1; ++i)
    {
      tHTMLTokens << tTokens[i] << "</br>";
    }
    tHTMLTokens << tTokens.back();
    return (tHTMLTokens.str());
  }
  return ("");
}

std::string changeToHTMLGigaHertz(const std::string& s)
{
  const size_t nIndex = s.find("GHz");
  if (std::string::npos != nIndex)
  {
    std::string s2(s);
    s2.replace(nIndex, 3, m_sGigaHertz);
    return (s2);
  }
  return (s);
}

const std::string& getHTMLUnits(const OpenCLUnits& tUnits)
{
  static std::map<OpenCLUnits, std::string, OpenCLUnitsLess> tUnitsMap;
  if (true == tUnitsMap.empty())
  {
    tUnitsMap.insert(std::make_pair(OpenCLUnits("GB"), m_sGigaBytes));
    tUnitsMap.insert(std::make_pair(OpenCLUnits("GHz"), m_sGigaHertz));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tMegaHertz, m_sMegaHertz));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tMicroSeconds, m_sMicroSeconds));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tSeconds, m_sSeconds));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tNanoSeconds, m_sNanoSeconds));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tBytes, m_sBytes));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tBits, m_sBits));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tPages, m_sPages));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tPixels, m_sPixels));
    tUnitsMap.insert(std::make_pair(OpenCLUnits::m_tKiloBytes, m_sKiloBytes));
  }

  const auto i = tUnitsMap.find(tUnits);
  if (tUnitsMap.end() != i)
  {
    return (i->second);
  }
  static std::string sUnknown("UNKNOWN Units");
  return (sUnknown);
}

// Explicit instantiations (for those HTML rows that do not need a tool tip nor
// units).
template void setHTMLTableRow<DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::string&,
  const DeviceInfo::OPEN_cl_bool&);
template void
setHTMLTableRow<cl_platform_id>(std::ostream&, const std::string&, const cl_platform_id&);
template void setHTMLTableRow<double>(std::ostream&, const std::string&, const double&);
template void setHTMLTableRow<size_t>(std::ostream&, const std::string&, const size_t&);
template void setHTMLTableRow<std::string>(std::ostream&, const std::string&, const std::string&);
template void
setHTMLTableRow<std::stringstream>(std::ostream&, const std::string&, const std::stringstream&);
template void setHTMLTableRow<unsigned int>(std::ostream&, const std::string&, const unsigned int&);
template void setHTMLTableRow<std::unique_ptr<char[]>>(
  std::ostream&,
  const std::string&,
  const std::unique_ptr<char[]>&);
template void
setHTMLTableRow<unsigned short>(std::ostream&, const std::string&, const unsigned short&);
template void setHTMLTableRow<int>(std::ostream&, const std::string&, const int&);

// Explicit instantiations (for those HTML rows that need a tool tip but no
// units).
template void setHTMLTableRow<DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::string&,
  const DeviceInfo::OPEN_cl_bool&,
  const OpenCLToolTip&);
template void setHTMLTableRow<cl_platform_id>(
  std::ostream&,
  const std::string&,
  const cl_platform_id&,
  const OpenCLToolTip&);
template void
setHTMLTableRow<double>(std::ostream&, const std::string&, const double&, const OpenCLToolTip&);
template void
setHTMLTableRow<size_t>(std::ostream&, const std::string&, const size_t&, const OpenCLToolTip&);
template void setHTMLTableRow<std::string>(
  std::ostream&,
  const std::string&,
  const std::string&,
  const OpenCLToolTip&);
template void setHTMLTableRow<std::stringstream>(
  std::ostream&,
  const std::string&,
  const std::stringstream&,
  const OpenCLToolTip&);
template void setHTMLTableRow<unsigned int>(
  std::ostream&,
  const std::string&,
  const unsigned int&,
  const OpenCLToolTip&);
template void setHTMLTableRow<std::unique_ptr<char[]>>(
  std::ostream&,
  const std::string&,
  const std::unique_ptr<char[]>&,
  const OpenCLToolTip&);
template void
setHTMLTableRow<int>(std::ostream&, const std::string&, const int&, const OpenCLToolTip&);
template void setHTMLTableRow<OPEN_cl_command_queue_properties>(
  std::ostream&,
  const std::string&,
  const OPEN_cl_command_queue_properties&,
  const OpenCLToolTip&);

// Explicit instantiations (for those HTML rows that don't need a tool tip but
// need units).
template void setHTMLTableRow<DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::string&,
  const DeviceInfo::OPEN_cl_bool&,
  const OpenCLUnits&);
template void setHTMLTableRow<cl_platform_id>(
  std::ostream&,
  const std::string&,
  const cl_platform_id&,
  const OpenCLUnits&);
template void
setHTMLTableRow<double>(std::ostream&, const std::string&, const double&, const OpenCLUnits&);
template void setHTMLTableRow<long double>(
  std::ostream&,
  const std::string&,
  const long double&,
  const OpenCLUnits&);
template void
setHTMLTableRow<size_t>(std::ostream&, const std::string&, const size_t&, const OpenCLUnits&);
template void setHTMLTableRow<std::string>(
  std::ostream&,
  const std::string&,
  const std::string&,
  const OpenCLUnits&);
template void setHTMLTableRow<std::stringstream>(
  std::ostream&,
  const std::string&,
  const std::stringstream&,
  const OpenCLUnits&);
template void setHTMLTableRow<unsigned int>(
  std::ostream&,
  const std::string&,
  const unsigned int&,
  const OpenCLUnits&);
template void setHTMLTableRow<std::unique_ptr<char[]>>(
  std::ostream&,
  const std::string&,
  const std::unique_ptr<char[]>&,
  const OpenCLUnits&);

// Explicit instantiations (for those HTML rows that need a tool tip *and*
// units).
template void setHTMLTableRow<DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::string&,
  const DeviceInfo::OPEN_cl_bool&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<cl_platform_id>(
  std::ostream&,
  const std::string&,
  const cl_platform_id&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<double>(
  std::ostream&,
  const std::string&,
  const double&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<size_t>(
  std::ostream&,
  const std::string&,
  const size_t&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<std::string>(
  std::ostream&,
  const std::string&,
  const std::string&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<std::stringstream>(
  std::ostream&,
  const std::string&,
  const std::stringstream&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<unsigned int>(
  std::ostream&,
  const std::string&,
  const unsigned int&,
  const OpenCLToolTip&,
  const OpenCLUnits&);
template void setHTMLTableRow<std::unique_ptr<char[]>>(
  std::ostream&,
  const std::string&,
  const std::unique_ptr<char[]>&,
  const OpenCLToolTip&,
  const OpenCLUnits&);

