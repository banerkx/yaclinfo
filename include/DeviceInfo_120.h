
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "DeviceInfo_110.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 120
#include <CL/opencl.h>

class DeviceInfo_120 : public DeviceInfo
{
  public:

    DeviceInfo_120(
      const cl_device_id nDeviceID,
      const size_t       nDeviceNumber,
      cl_platform_id     tPlatformID,
      const size_t       nPlatformNumber) noexcept;

    virtual ~DeviceInfo_120() noexcept override;

    virtual void getTextOutput(std::ostream& tStream) const noexcept override;

    virtual void getHTMLOutput(std::ostream& tStream) const noexcept override;

  private:

    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tBuiltInKernels;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_fp_config,
      OpenCLError,
      OpenCLToolTip>
      m_tHalfPrecisionFloatingPointConfiguration;
    std::tuple<
      const OPEN_cl_device_info_110_120,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tHostUnifiedMemory;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      size_t,
      OpenCLError,
      OpenCLToolTip,
      OpenCLUnits>
      m_tMax1DImageBuffer;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, size_t, OpenCLError, OpenCLToolTip>
      m_tMax1D2DImageArray;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tHasLinkerAvailable;
    std::
      tuple<const OPEN_cl_device_info_100, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
        m_tMaxSimultaneousImageObjectsWrite;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tCharVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tShortVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tIntVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tLongVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tFloatVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tDoubleVectorWidth;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tVectorWidthHalf;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<char[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tOpenCLCVersion;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_id,
      OpenCLError,
      OpenCLToolTip>
      m_tParentDeviceID;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tMaxSubDevices;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<cl_device_partition_property[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionTypesList;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      cl_device_affinity_domain,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionAffinityDomain;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      std::unique_ptr<cl_device_partition_property[]>,
      OpenCLError,
      OpenCLToolTip>
      m_tPartitionType;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tPreferredVectorWidthHalf;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, size_t, OpenCLError, OpenCLToolTip>
      m_tMaxPrintfBufferSize;
    std::tuple<
      const OPEN_cl_device_info,
      std::string,
      size_t,
      OPEN_cl_bool,
      OpenCLError,
      OpenCLToolTip>
      m_tPreferredUserSynchronization;
    std::tuple<
      const OPEN_cl_device_info_100,
      std::string,
      size_t,
      cl_command_queue_properties,
      OpenCLError,
      OpenCLToolTip>
      m_tQueueProperties;
    std::tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>
      m_tReferenceCount;

    virtual void getAllDeviceInfo() noexcept override;

    // Intentionally deleted.
    DeviceInfo_120()                          = delete;
    DeviceInfo_120(const DeviceInfo_120& rhs) = delete;
    DeviceInfo_120(DeviceInfo_120&& rhs)      = delete;
    DeviceInfo_120&       operator=(const DeviceInfo_120& rhs) = delete;
    DeviceInfo_120&       operator=(DeviceInfo_120&& rhs) = delete;
    DeviceInfo_120*       operator&()                     = delete;
    const DeviceInfo_120* operator&() const               = delete;
};

