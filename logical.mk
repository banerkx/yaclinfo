
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>           #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef LOGICAL_MK_INCLUDE_GUARD # {
LOGICAL_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# makefile user defined functions that perform logical operations.             #
# NOTE: In make, empty string is false and non-empty string is true.           #
# From: The GNU Make Book by JOHN GRAHAM-CUMMING                               #
################################################################################
or=$(if $(1)$(2),T)
and=$(if $(1),$(if $(2),T))
not=$(if $(1),,T)
nand=$(if $(1),$(if $(2),,T),T)
nor=$(if $(1)$(2),,T)
xor=$(if $(1),$(if $(2),,T),$(if $(2),T))

################################################################################
# Use for "||" (OR operator) for makefile ifdef:                               #
#   ifdef V1 "or" V2 "or" ... Vn ==> ifneq ($(call ifdef_or,V1 V2 ... Vn),)    #
################################################################################
ifdef_or=$(filter-out undefined,$(foreach v,$(1),$(origin $(v))))

################################################################################
# Use for "&&" (AND operator) for makefile ifdef:                              #
#   ifdef V1 "and" V2 "and" ... Vn ==> ifeq ($(call ifdef_and,V1 V2 ... Vn),)  #
################################################################################
ifdef_and=$(filter undefined,$(foreach v,$(1),$(origin $(v))))

endif # }

