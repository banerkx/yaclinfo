
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <limits>

enum class OpenCL_Version : unsigned short
{
  OPENCL_UNKNOWN = 0,
  OPENCL_100     = 100,
  OPENCL_110     = 110,
  OPENCL_120     = 120,
  OPENCL_200     = 200,
  OPENCL_210     = 210,
  OPENCL_220     = 220,
  OPENCL_300     = 300,
  OPENCL_END     = std::numeric_limits<unsigned short>::max()
};

