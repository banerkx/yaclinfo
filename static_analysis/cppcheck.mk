
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef CPPCHECK_MK_INCLUDE_GUARD # {
CPPCHECK_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

CPPCHECK_COMMAND=cppcheck
CPPCHECK:=$(shell command -v cppcheck)
CPPCHECK_VERSION=$(call get_stdout_version,$(CPPCHECK),--version,| sed 's/^.* //')

CPPCHECK_PROJECT=$(notdir $(CURDIR))
CPPCHECK_OUTPUT=$(CPPCHECK_PROJECT)_cppcheck
CPPCHECK_URL=https://github.com/danmar/cppcheck

ifneq ($(CPPCHECK),) # {

CPPCHECK_FILES=$(call get_source_files)

CPPCHECK_FILE_TARGETS=cppcheck cppcheck_xml cppcheck_html

ifneq ($(CPPCHECK_FILES),) # {

BROWSER ?= firefox
EDITOR ?= vim +1

CPPCHECK_SUPPRESSIONS ?= missingIncludeSystem unmatchedSuppression *:/usr/include/*

CPPCHECK_EXT=_cppcheck

################################################################################
# See:                                                                         #
#   https://cppcheck.sourceforge.io/manual.pdf                                 #
################################################################################

################################################################################
# Execute addon, i.e., --addon=cert.py. If options must be provided, a json    #
# configuration is needed.                                                     #
#                                                                              #
# NOTE: Using this option makes cppcheck produce *.dump files, even if the     #
#       --dump option is not used.                                             #
################################################################################
CPPCHECK_ADDONS_DIR=/usr/share/Cppcheck/addons
CPPCHECK_REJECTED_ADDONS=misra.py namingng.py

################################################################################
# Do not need to worry about the y2038 bug on a 64-bit platform.               #
################################################################################
ifeq (x86_64,$(shell uname --machine))
CPPCHECK_REJECTED_ADDONS += y2038.py
endif

CPPCHECK_ADDONS:=$(addprefix --addon=,$(filter-out $(addprefix $(CPPCHECK_ADDONS_DIR)/,$(CPPCHECK_REJECTED_ADDONS)),$(shell find $(CPPCHECK_ADDONS_DIR) -type f -name "*\.py" -executable)))

################################################################################
# You can specify the python interpreter either in the addon json files or     #
# through this command line option. If not present, Cppcheck will try          #
# "python3" first and then "python".                                           #
################################################################################
CPPCHECK_PYTHON ?= --addon-python=python3

################################################################################
# Enable noisy and soundy analysis. The normal Cppcheck analysis is turned     #
# off.                                                                         #
################################################################################
CPPCHECK_BUG_HUNTING ?= --bug-hunting

################################################################################
# Cppcheck work folder. Advantages:                                            #
#  * whole program analysis                                                    #
#  * faster analysis; Cppcheck will reuse the results if the hash for a file   #
#    is unchanged.                                                             #
#  * some useful debug information, i.e. commands used to execute              #
#    clang/clang-tidy/addons.                                                  #
################################################################################
CPPCHECK_BUILD_DIR:=$(addprefix --cppcheck-build-dir=,$(CPPCHECK_BUILD_DIR))

################################################################################
# Show information messages when library files have incomplete info.           #
################################################################################
CPPCHECK_LIBRARY ?= --check-library

################################################################################
# Experimental: Use Clang parser instead of the builtin Cppcheck parser.       #
# Takes the executable as optional parameter and defaults to `clang`.          #
# Cppcheck will run the given Clang executable, import the Clang AST and       #
# convert it into Cppcheck data. After that the normal Cppcheck analysis is    #
# used. You must have the executable in PATH if no path is given.              #
################################################################################
##CPPCHECK_CLANG_PATH:=$(shell command -v clang)
##CPPCHECK_CLANG_PATH:=$(if $(CPPCHECK_CLANG_PATH),--clang=$(CPPCHECK_CLANG_PATH),)

################################################################################
# Path (prefix) to be excluded from configuration checking. Preprocessor       #
# configurations defined in headers (but not sources) matching the prefix      #
# will not be considered for evaluation.                                       #
################################################################################
CPPCHECK_CONFIG_EXCLUDE:=$(addprefix --config-exclude=,$(CPPCHECK_CONFIG_EXCLUDE))

################################################################################
# A file that contains a list of config-excludes                               #
################################################################################
CPPCHECK_CONFIG_EXCLUDES_FILE:=$(addprefix --config-exclude-file=,$(CPPCHECK_CONFIG_EXCLUDES_FILE))

################################################################################
# Dump xml data for each translation unit. The dump files have the extension   #
# .dump and contain ast, tokenlist, symboldatabase, and valueflow.             #
################################################################################
CPPCHECK_DUMP_XML_DATA=--dump

################################################################################
# Define preprocessor symbol. Unless --max-configs or --force is used,         #
# Cppcheck will only check the given configuration when -D is used. Example:   #
# '-DDEBUG=1 -D__cplusplus'.                                                   #
################################################################################
CPPCHECK_DEFINE_SYMBOLS:=$(addprefix -D,$(CPPCHECK_DEFINE_SYMBOLS))

################################################################################
# Print preprocessor output on stdout and don't do any further processing.     #
################################################################################
##CPPCHECK_PRINT_PREPROCESSOR_OUTPUT=-E

################################################################################
# Enable additional checks. The available ids are:                             #
#  * all                                                                       #
#      Enable all checks. It is recommended to only use --enable=all when the  #
#      whole program is scanned, because this enables unusedFunction.          #
#  * warning                                                                   #
#      Enable warning messages                                                 #
#  * style                                                                     #
#      Enable all coding style checks. All messages with the severities        #
# 'style', 'warning', 'performance' and 'portability' are enabled.             #
#  * performance                                                               #
#      Enable performance messages                                             #
#  * portability                                                               #
#      Enable portability messages                                             #
#  * information                                                               #
#      Enable information messages                                             #
#  * unusedFunction                                                            #
#      Check for unused functions. It is recommended to only enable this when  #
#      the whole program is scanned.                                           #
#  * missingInclude                                                            #
#      Warn if there are missing includes. For detailed information, use       #
#      --check-config. Several ids can be given if you separate them with      #
#      commas. See also --std.                                                 #
################################################################################
CPPCHECK_ENABLE ?= --enable=all

################################################################################
# If errors are found, integer [n] is returned instead of the default '0'.     #
# '1' is returned if arguments are not valid or if no input files are          #
# provided. Note that your operating system can modify this value, e.g. '256'  #
# can become '0'.                                                              #
################################################################################
CPPCHECK_EXITCODE ?= --error-exitcode=-1

################################################################################
# Print a list of all the error messages in XML format.                        #
################################################################################
CPPCHECK_PRINT_ERRORS_XML=--errorlist

################################################################################
# Used when certain messages should be displayed but should not cause a        #
# non-zero exitcode.                                                           #
################################################################################
CPPCHECK_EXIT_CODE_SUPPRESSIONS:=$(addprefix --exitcode-suppressions=,$(CPPCHECK_EXIT_CODE_SUPPRESSIONS))

################################################################################
# Analyze only those files matching the given filter str. Can be used multiple #
# times Example: --file-filter=*bar.cpp analyzes only files that end with      #
# bar.cpp.                                                                     #
################################################################################
CPPCHECK_FILE_FILTERS:=$(addprefix --file-filter=,$(CPPCHECK_FILE_FILTERS))

################################################################################
# Specify the files to check in a text file. Add one filename per line. When   #
# file is '-,' the file list will be read from standard input.                 #
################################################################################
CPPCHECK_FILE_LIST:=$(addprefix --file-list=,$(CPPCHECK_FILE_LIST))

################################################################################
# Force checking of all configurations in files. If used together with         #
# '--max-configs=', the last option is the one that is effective.              #
################################################################################
CPPCHECK_FORCE ?= --force

################################################################################
# Give path to search for include files. Give several -I parameters to give    #
# several paths. First given path is searched for contained header files       #
# first. If paths are relative to source files, this is not needed.            #
################################################################################
CPPCHECK_INCLUDE_DIRS += $(subst -isystem,,$(subst -I,,$(INCLUDES)))
CPPCHECK_INCLUDE_DIRS:=$(addprefix -I ,$(CPPCHECK_INCLUDE_DIRS))

################################################################################
# Specify directory paths to search for included header files in a text file.  #
# Add one include path per line. First given path is searched for contained    #
# header files first. If paths are relative to source files, this is not       #
# needed.                                                                      #
################################################################################
CPPCHECK_INCLUDES_FILE:=$(addprefix --includes-file=,$(CPPCHECK_INCLUDES_FILE))

################################################################################
# Force inclusion of a file before the checked file.                           #
################################################################################
CPPCHECK_FORCE_INCLUSION:=$(addprefix --include=,$(CPPCHECK_FORCE_INCLUSION))

################################################################################
# Give a source file or source file directory to exclude from the check. This  #
# applies only to source files so header files included by source files are    #
# not matched. Directory name is matched to all parts of the path.             #
################################################################################
ifneq ($(CPPCHECK_EXCLUDE_PATHS),) # {
$(foreach path,$(CPPCHECK_EXCLUDE_PATHS),$(if $(wildcard $(path)),,$(error $(ERROR_LABEL) The cppcheck exclude path [$(path)] does not exist)))
CPPCHECK_EXCLUDE_PATHS:=$(addprefix -i ,$(realpath $(CPPCHECK_EXCLUDE_PATHS)))
endif # }

################################################################################
# Allow that Cppcheck reports even though the analysis is inconclusive. There  #
# are false positives with this option. Each result must be carefully          #
# investigated before you know if it is good or bad.                           #
################################################################################
CPPCHECK_ALLOW_INCLUSIVE=--inconclusive

################################################################################
# Enable inline suppressions. Use them by placing one or more comments, like:  #
# '// cppcheck-suppress warningId' on the lines before the warning to          #
# suppress.                                                                    #
################################################################################
CPPCHECK_ENABLE_INLINE_SUPPRESSIONS=--inline-suppr

################################################################################
# Start <jobs> threads to do the checking simultaneously.                      #
# NOTE: unusedFunction check can't be used with '-j'                           #
################################################################################
CPPCHECK_JOBS ?= -j 4

################################################################################
# Specifies that no new threads should be started if there are other threads   #
# running and the load average is at least <load>.                             #
################################################################################
CPPCHECK_LOAD_CAP=-l 1.6

################################################################################
# Forces cppcheck to check all files as the given language. Valid values are:  #
# c, c++                                                                       #
################################################################################
CPPCHECK_LANGUAGE ?= --language=c++

################################################################################
# Load file <cfg> that contains information about types and functions. With    #
# such information Cppcheck understands your code better and therefore you     #
# get better results. The std.cfg file that is distributed with Cppcheck is    #
# loaded automatically. For more information about library files, read the     #
# manual.                                                                      #
################################################################################
CPPCHECK_LIB_INFO_FILE:=$(addprefix --library=,$(CPPCHECK_LIB_INFO_FILE))

################################################################################
# Max depth in whole program analysis. The default value is 2. A larger value  #
# will mean more errors can be found but also means the analysis will be       #
# slower.                                                                      #
################################################################################
CPPCHECK_CTU_DEPTH ?= --max-ctu-depth=4

################################################################################
# Write results to file, rather than standard error.                           #
################################################################################
CPPCHECK_OUTPUT_FILE:=$(addprefix --output-file=,$(CPPCHECK_OUTPUT_FILE))

################################################################################
# Run Cppcheck on project. The <file> can be a Visual Studio Solution          #
# (*.sln), Visual Studio Project (*.vcxproj), compile database                 #
# (compile_commands.json), or Borland C++ Builder 6 (*.bpr). The files to      #
# analyse, include paths, defines, platform and undefines in the specified     #
# file will be used.                                                           #
################################################################################
##CPPCHECK_PROJECT_FILE:=$(addprefix --project=,$(CPPCHECK_PROJECT_FILE))

################################################################################
# If used together with a Visual Studio Solution (*.sln) or Visual Studio      #
# Project (*.vcxproj) you can limit the configuration cppcheck should check.   #
# For example: '--project-configuration=Release|Win32'                         #
################################################################################
##CPPCHECK_VS_PROJECT_CONFIG:=$(addprefix --project-configuration=,$(CPPCHECK_VS_PROJECT_CONFIG))

################################################################################
# Maximum number of configurations to check in a file before skipping it.      #
# Default is '12'. If used together with '--force', the last option is the     #
# one that is effective.                                                       #
################################################################################
CPPCHECK_MAX_CONFIGS_LIMIT:=$(addprefix --max-configs=,$(CPPCHECK_MAX_CONFIGS_LIMIT))

################################################################################
# Specifies platform specific types and sizes. The available builtin           #
# platforms are:                                                               #
#  * unix32                                                                    #
#      32 bit unix variant                                                     #
#  * unix64                                                                    #
#      64 bit unix variant                                                     #
#  * win32A                                                                    #
#      32 bit Windows ASCII character encoding                                 #
#  * win32W                                                                    #
#      32 bit Windows UNICODE character encoding                               #
#  * win64                                                                     #
#      64 bit Windows                                                          #
#  * avr8                                                                      #
#      8 bit AVR microcontrollers                                              #
#  * elbrus-e1cp                                                               #
#      Elbrus e1c+ architecture                                                #
#  * pic8                                                                      #
#      8 bit PIC microcontrollers; baseline and mid-range architectures        #
#  * pic8-enhanced                                                             #
#      8 bit PIC microcontrollers; enhanced mid-range and high end (PIC18)     #
#      architectures                                                           #
#  * pic16                                                                     #
#      16 bit PIC microcontrollers                                             #
#  * mips32                                                                    #
#      32 bit MIPS microcontrollers                                            #
#  * native                                                                    #
#      Type sizes of host system are assumed, but no further assumptions.      #
#  * unspecified                                                               #
#      Unknown type sizes                                                      #
################################################################################
override CPPCHECK_PLATFORM=--platform=unix64

################################################################################
# Generate Clang-plist output files in folder.                                 #
################################################################################
CPPCHECK_CLANG_PLIST_OUTOUT:=$(addprefix --plist-output=,$(CPPCHECK_CLANG_PLIST_OUTOUT))

################################################################################
# Do not show progress reports.                                                #
################################################################################
CPPCHECK_PROGRESS ?= --quiet

################################################################################
# Use relative paths in output. When given, <paths> are used as base. You can  #
# separate multiple paths by ';'. Otherwise path where source files are        #
# searched is used.  We use string comparison to create relative paths, so     #
# using e.g. ~ for home folder does not work. It is currently only possible    #
# to apply the base paths to files that are on a lower level in the directory  #
# tree.                                                                        #
################################################################################
CPPCHECK_USER_REL_PATHS:=$(addprefix --relative-paths=,$(CPPCHECK_USER_REL_PATHS))

################################################################################
# Report progress messages while checking a file.                              #
################################################################################
CPPCHECK_REPORT_PROGRESS ?= --report-progress

################################################################################
# Match regular expression.                                                    #
################################################################################
CPPCHECK_MATCH_REG_EXP:=$(addprefix --rule=,$(CPPCHECK_MATCH_REG_EXP))

################################################################################
# Use given rule file. For more information, see:                              #
#  http://sourceforge.net/projects/cppcheck/files/Articles/                    #
################################################################################
CPPCHECK_RULE_FILE:=$(addprefix --rule-file=,$(CPPCHECK_RULE_FILE))

################################################################################
# Set standard. The available options are:                                     #
#  * c89                                                                       #
#      C code is C89 compatible                                                #
#  * c99                                                                       #
#      C code is C99 compatible                                                #
#  * c11                                                                       #
#      C code is C11 compatible (default)                                      #
#  * c++03                                                                     #
#      C++ code is C++03 compatible                                            #
#  * c++11                                                                     #
#      C++ code is C++11 compatible                                            #
#  * c++14                                                                     #
#      C++ code is C++14 compatible                                            #
#  * c++17                                                                     #
#      C++ code is C++17 compatible                                            #
#  * c++20                                                                     #
#      C++ code is C++20 compatible (default)                                  #
################################################################################
CPPCHECK_CPP_STD ?= $(if $(get_compiler_std),$(if $(CXX),$(subst --std=c++98,--std=c++03,$(filter-out --std=UNKNOWN,--std=$(call get_compiler_std,$(CXX))))))

################################################################################
# Suppress warnings that match <spec>. The format of <spec> is:                #
#  [error id]:[filename]:[line]                                                #
# The [filename] and [line] are optional. If [error id] is a wildcard '*',     #
# all error ids match.                                                         #
#                                                                              #
# cppcheck is really bad at finding standard include headers, on Mac and       #
# Linux. Fortunately, you can suppress this check, and only scan your custom   #
# header files.                                                                #
#                                                                              #
# See:                                                                         #
# https://stackoverflow.com/questions/6986033/cppcheck-cant-find-include-files #
################################################################################
CPPCHECK_SUPPRESSIONS:=$(addprefix --suppress=,$(CPPCHECK_SUPPRESSIONS))

################################################################################
# Suppress warnings listed in the file. Each suppression is in the same        #
# format as <spec> above.                                                      #
#                                                                              #
# NOTE: The variable CPPCHECK_SUPPRESSIONS_FILE must be set **before**         #
#       including this makefile.                                               #
################################################################################
ifneq ($(CPPCHECK_SUPPRESSIONS_FILE),) # {
$(if $(wildcard $(CPPCHECK_SUPPRESSIONS_FILE)),,                                                                      \
  $(error $(ERROR_LABEL) The [cppcheck] suppressions file [$(CPPCHECK_SUPPRESSIONS_FILE)] does not exist))
CPPCHECK_SUPPRESSIONS_FILE:= --suppressions-list=$(CPPCHECK_SUPPRESSIONS_FILE)
endif # }

################################################################################
# Suppress warnings listed in a xml file. XML file should follow the           #
# manual.pdf format specified in section: 6.4 XML suppressions .               #
################################################################################
CPPCHECK_XML_SUPPRESSIONS_FILE:=$(addprefix --suppress-xml=,$(CPPCHECK_XML_SUPPRESSIONS_FILE))

################################################################################
# Format the error messages. Available fields:                                 #
#  {file}              file name                                               #
#  {line}              line number                                             #
#  {column}            column number                                           #
#  {callstack}         show a callstack. Example: [file.c:1] -> [file.c:100]   #
#  {inconclusive:text} if warning is inconclusive, text is written             #
#  {severity}          severity                                                #
#  {message}           warning message                                         #
#  {id}                warning id                                              #
#  {cwe}               CWE id (Common Weakness Enumeration)                    #
#  {code}              show the real code                                      #
#  \t                  insert tab                                              #
#  \n                  insert newline                                          #
#  \r                  insert carriage return                                  #
# Example formats:                                                             #
#  '{file}:{line},{severity},{id},{message}' or                                #
#  '{file}({line}):({severity}) {message}' or                                  #
#  '{callstack} {message}'                                                     #
# Pre-defined templates: gcc (default), cppcheck1 (old default), vs, edit.     #
################################################################################
CPPCHECK_MESSAGE_TEMPLATE ?= --template='\n{id}:{file}:{line} Severity = [{severity}]\nCODE = {code}\n{message}'
##CPPCHECK_MESSAGE_TEMPLATE ?= --template=gcc

################################################################################
# Format error message location. If this is not provided then no extra         #
# location info is shown. Available fields:                                    #
#  {file}      file name                                                       #
#  {line}      line number                                                     #
#  {column}    column number                                                   #
#  {info}      location info                                                   #
#  {code}      show the real code                                              #
#  \t         insert tab                                                       #
#  \n         insert newline                                                   #
#  \r         insert carriage return                                           #
# Example format (gcc-like):                                                   #
#  '{file}:{line}:{column}: note: {info}\n{code}'                              #
################################################################################
CPPCHECK_LOCATION_TEMPLATE ?= --template-location='{file}:{line}:{info}\nCODE = {code}'

################################################################################
# Undefine preprocessor symbol. Use -U to explicitly hide certain #ifdef <ID>  #
# code paths from checking. Example: '-UDEBUG'                                 #
################################################################################
CPPCHECK_UNDEFINES:=$(addprefix -U,$(CPPCHECK_UNDEFINES))

################################################################################
# Output more detailed error information.                                      #
################################################################################
CPPCHECK_VERBOSE ?= --verbose

################################################################################
# Write results in xml format to error stream (stderr).                        #
################################################################################
CPPCHECK_XML_OUTPUT=--xml

################################################################################
# Select XML version. Currently versions 1 and 2 are available. The default    #
# version is 1.                                                                #
################################################################################
CPPCHECK_XML_VERSION:=--xml-version=2

CPPCHECK_OPTS += $(CPPCHECK_ADDONS)
CPPCHECK_OPTS += $(CPPCHECK_BUILD_DIR)
CPPCHECK_OPTS += $(CPPCHECK_CLANG_PLIST_OUTOUT)
CPPCHECK_OPTS += $(CPPCHECK_CONFIG_EXCLUDE)
CPPCHECK_OPTS += $(CPPCHECK_CONFIG_EXCLUDES_FILE)
CPPCHECK_OPTS += $(CPPCHECK_CPP_STD)
CPPCHECK_OPTS += $(CPPCHECK_CTU_DEPTH)
CPPCHECK_OPTS += $(CPPCHECK_DEFINE_SYMBOLS)
CPPCHECK_OPTS += $(CPPCHECK_ENABLE)
CPPCHECK_OPTS += $(CPPCHECK_EXCLUDE_PATHS)
CPPCHECK_OPTS += $(CPPCHECK_EXITCODE)
CPPCHECK_OPTS += $(CPPCHECK_EXIT_CODE_SUPPRESSIONS)
CPPCHECK_OPTS += $(CPPCHECK_FILE_FILTERS)
CPPCHECK_OPTS += $(CPPCHECK_FILE_LIST)
CPPCHECK_OPTS += $(CPPCHECK_FORCE)
CPPCHECK_OPTS += $(CPPCHECK_FORCE_INCLUSION)
CPPCHECK_OPTS += $(CPPCHECK_INCLUDES_FILE)
CPPCHECK_OPTS += $(CPPCHECK_INCLUDE_DIRS)
CPPCHECK_OPTS += $(CPPCHECK_LANGUAGE)
CPPCHECK_OPTS += $(CPPCHECK_LIBRARY)
CPPCHECK_OPTS += $(CPPCHECK_LIB_INFO_FILE)
CPPCHECK_OPTS += $(CPPCHECK_LOAD_CAP)
CPPCHECK_OPTS += $(CPPCHECK_LOCATION_TEMPLATE)
CPPCHECK_OPTS += $(CPPCHECK_MATCH_REG_EXP)
CPPCHECK_OPTS += $(CPPCHECK_MAX_CONFIGS_LIMIT)
CPPCHECK_OPTS += $(CPPCHECK_MESSAGE_TEMPLATE)
CPPCHECK_OPTS += $(CPPCHECK_PLATFORM)
CPPCHECK_OPTS += $(CPPCHECK_PROJECT_FILE)
CPPCHECK_OPTS += $(CPPCHECK_PYTHON)
CPPCHECK_OPTS += $(CPPCHECK_REPORT_PROGRESS)
CPPCHECK_OPTS += $(CPPCHECK_RULE_FILE)
CPPCHECK_OPTS += $(CPPCHECK_SUPPRESSIONS)
CPPCHECK_OPTS += $(CPPCHECK_SUPPRESSIONS_FILE)
CPPCHECK_OPTS += $(CPPCHECK_UNDEFINES)
CPPCHECK_OPTS += $(CPPCHECK_USER_REL_PATHS)
CPPCHECK_OPTS += $(CPPCHECK_VERBOSE)
CPPCHECK_OPTS += $(CPPCHECK_VS_PROJECT_CONFIG)
CPPCHECK_OPTS += $(CPPCHECK_XML_SUPPRESSIONS_FILE)

##CPPCHECK_OPTS += $(CPPCHECK_BUG_HUNTING)
##CPPCHECK_OPTS += $(CPPCHECK_ALLOW_INCLUSIVE)
##CPPCHECK_OPTS += $(CPPCHECK_CLANG_PATH)
##CPPCHECK_OPTS += $(CPPCHECK_DUMP_XML_DATA)
##CPPCHECK_OPTS += $(CPPCHECK_ENABLE_INLINE_SUPPRESSIONS)
##CPPCHECK_OPTS += $(CPPCHECK_JOBS)
##CPPCHECK_OPTS += $(CPPCHECK_OUTPUT_FILE)
##CPPCHECK_OPTS += $(CPPCHECK_PRINT_ERRORS_XML)
##CPPCHECK_OPTS += $(CPPCHECK_PRINT_PREPROCESSOR_OUTPUT)

$(CPPCHECK_OUTPUT): cppcheck_clean
	@mkdir -p $(CPPCHECK_OUTPUT)

################################################################################
# Cleans the cppcheck output files.                                            #
#                                                                              #
# $(1)- cppcheck output directory                                              #
# $(2)- target name                                                            #
################################################################################
define cppcheck_clean_files
if [[ -d $(1) ]];                                                          \
then                                                                       \
  sed -i '/ Cannot determine that .* is initialized$$/d' $(1)/*;           \
  sed -i '\@:information:\[/usr/include/@d' $(1)/*;                        \
  find $(1) -mindepth 1 -maxdepth 1 -type f -empty -exec rm {} \;;         \
  if [[ "$$(ls -A $(1))" ]];                                               \
  then                                                                     \
    ls -1 $(1) | sed 's@^@$(EDITOR) $(1)/@' || :;                          \
  else                                                                     \
    rmdir $(1);                                                            \
    echo '[$(2)] $(INFO_LABEL) No $(CPPCHECK) results files found.';       \
  fi;                                                                      \
else                                                                       \
  echo '[$(2)] $(INFO_LABEL) No cppcheck results files found.'; \
fi
endef

.PHONY: cppcheck_prep
cppcheck_prep::
	@:

################################################################################
# Uses cppcheck on the input source file.                                      #
#                                                                              #
# $(1) - input source file                                                     #
################################################################################
define make_cppcheck
$(eval CPPCHECK_RESULTS_FILE:=$(CPPCHECK_OUTPUT)/$(notdir $(1))$(CPPCHECK_EXT))
$(CPPCHECK) $(CPPCHECK_OPTS) --output-file=$(CPPCHECK_RESULTS_FILE) $(1) || :
if [[ ! -s $(CPPCHECK_RESULTS_FILE) ]]; \
then                                    \
  rm $(CPPCHECK_RESULTS_FILE);          \
fi
$(if $(findstring $(CPPCHECK_ADDONS),$(CPPCHECK_OPTS)),$(if $(findstring $(CPPCHECK_DUMP_XML_DATA),$(CPPCHECK_OPTS)),:,rm -f $(1).dump))
endef

################################################################################
# Uses cppcheck on the input source file and saves the output in XML           #
# format.                                                                      #
#                                                                              #
# $(1) - input source file                                                     #
################################################################################
define make_cppcheck_xml
$(eval CPPCHECK_RESULTS_FILE_XML:=$(CPPCHECK_OUTPUT)/$(notdir $(1))$(CPPCHECK_EXT).xml)
$(CPPCHECK) $(CPPCHECK_OPTS) $(1) 2> $(CPPCHECK_RESULTS_FILE_XML) || :
endef

################################################################################
# Deletes dump files.                                                          #
#                                                                              #
# $(1) - list of files that may have associated dump files                     #
################################################################################
del_dump_files=rm -f $(addsuffix .dump,$(1))

DATE_TIME=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
.PHONY: cppcheck
cppcheck: cppcheck_prep | $(CPPCHECK_OUTPUT)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
##	@echo 'CPPCHECK_OPTS  = [$(CPPCHECK_OPTS)]'
##	@echo 'CPPCHECK_FILES = [$(CPPCHECK_FILES)]'
	@$(foreach file,$(CPPCHECK_FILES),$(call make_cppcheck,$(file)))
	@rmdir --ignore-fail-on-non-empty $(CPPCHECK_OUTPUT)
	@[[ -d $(CPPCHECK_OUTPUT) ]] && sed -i '1 i\Date/Time: [$(DATE_TIME)]' $(CPPCHECK_OUTPUT)/* || :
	@[[ -d $(CPPCHECK_OUTPUT) ]] && sed -i '1 i\cppcheck version = [$(CPPCHECK_VERSION)]' $(CPPCHECK_OUTPUT)/* || :
	@[[ -d $(CPPCHECK_OUTPUT) ]] && ls -1 $(CPPCHECK_OUTPUT)/* | sed 's@^@$(EDITOR) @' || echo '[$(@)] $(INFO_LABEL) No cppcheck results files found.'
##	@$(call cppcheck_clean_files,$(CPPCHECK_OUTPUT),$(@))
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

.PHONY: cppcheck_xml
cppcheck_xml: CPPCHECK_OPTS += $(CPPCHECK_PROGRESS) $(CPPCHECK_XML_VERSION) $(CPPCHECK_XML_OUTPUT)
cppcheck_xml: cppcheck_prep | $(CPPCHECK_OUTPUT)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
##	@echo 'CPPCHECK_OPTS  = [$(CPPCHECK_OPTS)]'
##	@echo 'CPPCHECK_FILES = [$(CPPCHECK_FILES)]'
	@$(foreach file,$(CPPCHECK_FILES),$(call make_cppcheck_xml,$(file)))
	@rmdir --ignore-fail-on-non-empty $(CPPCHECK_OUTPUT)
	@$(call del_dump_files,$(CPPCHECK_FILES))
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

.PHONY: cppcheck_html
IMAGE_PATH ?= $(PROJECT_ROOT)/images
CPPCHECK_FAVICON=$(IMAGE_PATH)/cppcheck.png
CPPCHECK_HTML=cppcheck-htmlreport
CPPCHECK_HTML_FILE=$(CPPCHECK_OUTPUT)/index.html
cppcheck_html: cppcheck_xml
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(CPPCHECK_HTML) --title='$(CPPCHECK_PROJECT) $(DATE_TIME)' $(addprefix --file=,$(wildcard $(CPPCHECK_OUTPUT)/*.xml)) --report-dir=$(CPPCHECK_OUTPUT)
	@rm -f $(CPPCHECK_OUTPUT)/*.xml
	@$(call add_favicon,$(CPPCHECK_FAVICON),$(CPPCHECK_HTML_FILE))
	@$(call open_sole_target_file,$(@),$(CPPCHECK_HTML_FILE),$(BROWSER),cppcheck HTML results)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

.PHONY: cppcheck_list
CPPCHECK_LIST=cppchecks.txt
cppcheck_list:
	@$(CPPCHECK) --doc >| $(CPPCHECK_LIST)
	@$(call open_file_command,$(CPPCHECK_LIST),$(EDITOR),cppcheck list of checks)

.PHONY: cppcheck_error_list
CPPCHECK_ERROR_LIST=cppchecks_errors.xml
cppcheck_error_list:
	@$(CPPCHECK) --errorlist >| $(CPPCHECK_ERROR_LIST)
	@$(call open_file_command,$(CPPCHECK_ERROR_LIST),$(EDITOR),cppcheck error list)

show_help:: cppcheck_mk_help

else # } {

$(CPPCHECK_FILE_TARGETS):
	@echo '[$(@)] $(INFO_LABEL) No source files found.'

endif # }

.PHONY: cppcheck_config
################################################################################
# Check cppcheck configuration. The normal code analysis is disabled by this   #
# flag.                                                                        #
################################################################################
cppcheck_config: override CPPCHECK_CONFIG=--check-config
cppcheck_config: CPPCHECK_OPTS += $(CPPCHECK_CONFIG)
cppcheck_config:
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))
	@$(call make_cppcheck,$(firstword $(CPPCHECK_FILES)))

.PHONY: cppcheck_help
CPPCHECK_HELP=cppcheck_help.txt
cppcheck_help:
	@$(CPPCHECK) --help >| $(CPPCHECK_HELP)
	@$(call open_file_command,$(CPPCHECK_HELP),$(EDITOR),cppcheck help)

cppcheck_clean:
	@rm -rf $(CPPCHECK_OUTPUT) $(CPPCHECK_HELP)
	@rm -f $(CPPCHECK_LIST) $(CPPCHECK_ERROR_LIST)
	@$(call del_dump_files,$(CPPCHECK_FILES))

clean:: cppcheck_clean

platform_info::
	@echo 'cppcheck version = [$(CPPCHECK_VERSION)]'

cppcheck_mk_help::
	@echo '$(MAKE) cppcheck → run cppcheck on source files'
	@echo '$(MAKE) cppcheck_config → check cppcheck configuration'
	@echo '$(MAKE) cppcheck_error_list → save cppcheck error messages list to [$(CPPCHECK_ERROR_LIST)]'
	@echo '$(MAKE) cppcheck_help → save cppcheck help information to [$(CPPCHECK_HELP)]'
	@echo '$(MAKE) cppcheck_html → run cppcheck on source files, saving output as HTML'
	@echo '$(MAKE) cppcheck_list → save cppcheck list of checks to [$(CPPCHECK_LIST)]'
	@echo '$(MAKE) cppcheck_xml → run cppcheck on source files, saving output as XML'

else # } {

CPPCHECK_TARGETS=cppcheck cppcheck_config cppcheck_error_list cppcheck_help cppcheck_html cppcheck_list cppcheck_xml

$(CPPCHECK_TARGETS):
	@$(error $(ERROR_LABEL) The [cppcheck] executable is not available, ∴ the target [$(@)] can not be made)

endif # }

endif # }

