
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <vector>

#include "SingletonBase.h"

class ResourceLimits : public SingletonBase<ResourceLimits>
{
  public:

    typedef int         ResourceDescriptor;
    typedef std::string ResourceDescripton;
    typedef std::string ResourceResult;

    typedef std::tuple<ResourceDescriptor, ResourceDescripton, ResourceResult> RESOURCE;

    typedef enum
    {
      RESOURCE_DESCRIPTOR  = 0,
      RESOURCE_DESCRIPTION = 1,
      RESOURCE_RESULT      = 2
    } RESOURCE_INDICES;

    virtual ~ResourceLimits();

    inline const std::vector<RESOURCE>& resourceLimits() const
    {
      return (m_tResourceLimits);
    }

    inline const std::vector<RESOURCE>& getInfo() const
    {
      return (m_tConcatenatedResources);
    }

    operator const std::vector<RESOURCE>&() const
    {
      return (m_tConcatenatedResources);
    }

    std::ostream& showInts(std::ostream& tStream = std::cout);

    std::vector<RESOURCE>::const_iterator begin() const
    {
      return (m_tConcatenatedResources.begin());
    }

    std::vector<RESOURCE>::const_iterator end() const
    {
      return (m_tConcatenatedResources.end());
    }

    inline int descriptionWidth() const
    {
      return (m_nDescriptionWidth);
    }

    inline int resultWidth() const
    {
      return (m_nResultWidth);
    }

    static std::string getMonthAndYear(const long nYYYYMM);

    friend std::ostream& operator<<(std::ostream& tStream, const ResourceLimits& rhs);

    inline const std::vector<std::pair<std::string, std::string>>& getRUsageSelfInfo() const
    {
      return (m_tSelfInfo);
    }

    inline const std::vector<std::pair<std::string, std::string>>& getRUsageChildrenInfo() const
    {
      return (m_tChildrenInfo);
    }

    inline const std::vector<std::pair<std::string, std::string>>& getRUsageThreadInfo() const
    {
      return (m_tThreadInfo);
    }

    inline int processPriority() const
    {
      return (m_nProcessPriority);
    }

  private:

    std::vector<RESOURCE>                            m_tResourceLimits;
    std::vector<RESOURCE>                            m_tConfigurationParams;
    std::vector<RESOURCE>                            m_tConcatenatedResources;
    int                                              m_nDescriptionWidth;
    int                                              m_nResultWidth;
    struct rusage                                    m_tRusageSelf;
    int                                              m_nRusageSelfStat;
    std::vector<std::pair<std::string, std::string>> m_tSelfInfo;
    struct rusage                                    m_tRusageChildren;
    int                                              m_nRusageChildrenStat;
    std::vector<std::pair<std::string, std::string>> m_tChildrenInfo;
    struct rusage                                    m_tRusageThread;
    int                                              m_nRusageThreadStat;
    std::vector<std::pair<std::string, std::string>> m_tThreadInfo;
    int                                              m_nProcessPriority;

    ResourceLimits();

    void populateResources();

    void getLimits();

    void
    storeUlimitValues(std::stringstream& tInfo, const struct rlimit& tLimits, const int nRLimit);

    void populateConfigurationParams();

    void getConfigurationParams();

    void concatenate();

    void setFieldWidths();

    static std::string getRLimitUnits(const int nRLimit);

    static std::string getConfigParamUnits(const int nConfigParam);

    static bool lessThan(const RESOURCE& lhs, const RESOURCE& rhs);

    void setRusageInfo();

    void getPriority();

    static void getRUsageInfo(
      std::vector<std::pair<std::string, std::string>>& tInfo,
      const struct rusage&                              tUsage,
      const int                                         nStat);

    static double convertToSeconds(const struct timeval& tTime);

    friend class SingletonBase<ResourceLimits>;

    // Intentionally deleted.
    ResourceLimits(const ResourceLimits& rhs) = delete;
    ResourceLimits(ResourceLimits&& rhs)      = delete;
    ResourceLimits&       operator=(const ResourceLimits& rhs) = delete;
    ResourceLimits&       operator=(ResourceLimits&& rhs) = delete;
    ResourceLimits*       operator&()                     = delete;
    const ResourceLimits* operator&() const               = delete;
};

