
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <ifaddrs.h>
#include <netdb.h>

#include <iostream>
#include <string>
#include <tuple>
#include <vector>

#include "SingletonBase.h"

class IPAddresses : public SingletonBase<IPAddresses>
{
  public:

    constexpr static const int m_nInterfaceWidth  = 12;
    constexpr static const int m_nFamilyWidth     = 16;
    constexpr static const int m_nIPAddressWidth  = 40;
    constexpr static const int m_nSubnetMaskWidth = 40;
    constexpr static const int m_nMACAddressWidth = 17;

    typedef std::string IP_Interface;
    typedef std::string IP_Family;
    typedef std::string IP_Address;
    typedef std::string NetMask;
    typedef std::string MAC_Address;

    typedef std::tuple<IP_Interface, IP_Family, IP_Address, NetMask, MAC_Address> IPAddressInfo;
    typedef std::tuple<IP_Interface, IP_Family, IP_Address> IPGatewayInterfaceFamily;

    typedef enum
    {
      IP_INTERFACE = 0,
      IP_FAMILY    = 1,
      IP_ADDRESS   = 2,
      SUBNET_MASK  = 3,
      MAC_ADDRESS  = 4
    } IP_INFO_FIELD;

    virtual ~IPAddresses();

    inline const std::vector<IPAddressInfo>& ipAddresses() const
    {
      return (m_tIPAddresses);
    }

    const std::vector<IPAddressInfo>& inetAddresses();

    const std::vector<IPAddressInfo>& inet6Addresses();

    const std::vector<IPAddressInfo>& packetAddresses();

    inline bool empty() const
    {
      return (m_tIPAddresses.empty());
    }

    inline bool size() const
    {
      return (m_tIPAddresses.size());
    }

    inline const std::string& localHostName() const
    {
      return (m_sLocalHostName);
    }

    inline const std::string& localIPv4Address() const
    {
      return (m_sLocalIPv4Address);
    }

    inline const std::string& localIPv6Address() const
    {
      return (m_sLocalIPv6Address);
    }

    inline const std::string& localMACAddress() const
    {
      return (m_sLocalMACAddress);
    }

    inline const std::vector<std::string>& errors() const
    {
      return (m_tErrors);
    }

    inline bool hasErrors() const
    {
      return (false == m_tErrors.empty());
    }

    friend std::ostream& operator<<(std::ostream& tStream, const IPAddresses& rhs) noexcept;

  private:

    typedef bool (*PartitionPredicate)(const IPAddressInfo&);

    std::vector<IPAddressInfo> m_tIPAddresses;
    std::vector<IPAddressInfo> m_tAFInetIPAddresses;
    bool                       m_bPartionedInet;
    std::vector<IPAddressInfo> m_tAFInet6IPAddresses;
    bool                       m_bPartionedInet6;
    std::vector<IPAddressInfo> m_tAFInetPacketIPAddresses;
    bool                       m_bPartionedInetPacket;
    std::string                m_sLocalHostName;
    std::string                m_sLocalIPv4Address;
    std::string                m_sLocalIPv6Address;
    std::string                m_sLocalMACAddress;
    std::vector<std::string>   m_tErrors;

    IPAddresses();

    std::string getLocalHostName() noexcept;

    void getLocalHostIPAddresses();

    static void ifEmptySetUnknown(std::string& sAddress);

    static bool lessThan(const IPAddressInfo& lhs, const IPAddressInfo& rhs);

    static void setIPAddress(IPAddressInfo& tInfo, std::string& sIPAddress, const char* pIPFamily);

    void setMACAddress(IPAddressInfo& tInfo);

    friend std::ostream& operator<<(
      std::ostream&                     tStream,
      const IPAddresses::IPAddressInfo& rhs) noexcept;

    void partition(std::vector<IPAddressInfo>& tIPAddresses, PartitionPredicate tPredicate);

    static bool isAFInet(const IPAddressInfo& tIPInfo);

    static bool isAFInet6(const IPAddressInfo& tIPInfo);

    static bool isAFPacket(const IPAddressInfo& tIPInfo);

    friend class SingletonBase<IPAddresses>;

    // Intentionally deleted.
    IPAddresses(const IPAddresses& rhs) = delete;
    IPAddresses(IPAddresses&& rhs)      = delete;
    IPAddresses&       operator=(const IPAddresses& rhs) = delete;
    IPAddresses&       operator=(IPAddresses&& rhs) = delete;
    IPAddresses*       operator&()                  = delete;
    const IPAddresses* operator&() const            = delete;
};

