
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef MSG_DIGESTS_MK_INCLUDE_GUARD # {
MSG_DIGESTS_MK_INCLUDE_GUARD:=$(lastword $(MAKEFILE_LIST))

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/vars_common.mk.  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

GNU_CORE_UTILS_VERSION=$(call get_rpm_package_version,coreutils)

GPG=$(shell command -v gpg)
GPG_VERSION=$(call get_stdout_version_index,gpg,--version,3)

OPENSSL=$(shell command -v openssl)
OPENSSL_VERSION=$(call get_stdout_version_index,openssl,version,2)

SHASUM=$(shell command -v shasum)
SHASUM_VERSION=$(call get_stdout_version,shasum,--version)

.PHONY: msg_digests_info
msg_digests_info:
	@echo 'GNU Core Utils version = [$(GNU_CORE_UTILS_VERSION)]'
	@echo 'gpg version            = [$(GPG_VERSION)]'
	@echo 'openssl version        = [$(OPENSSL_VERSION)]'
	@echo 'shasum version         = [$(SHASUM_VERSION)]'

platform_info:: msg_digests_info

ifneq ($(EXE),) # {
################################################################################
# Determining how to get the list of the message digest algorithms supported   #
# by the openssl executable (since this depends on the version of openssl).    #
################################################################################
ifeq ($(shell echo $(OPENSSL_VERSION) | grep -q "[0-9]\+\.[0-9]\+\.[0-9]\+.-fips"; echo $${?}),0) # {
OPENSSL_LIST_ALGORITHMS_OPTION:=list-message-digest-algorithms
else ifeq ($(shell echo $(OPENSSL_VERSION) | grep -q "[0-9]\+\.[0-9]\+\.[0-9]\+[a-z]"; echo $${?}),0) # } {
OPENSSL_LIST_ALGORITHMS_OPTION:=list -digest-algorithms
endif # }

################################################################################
# Computes the OpenSSL message digests of the input file.                      #
#   $(1) - input file                                                          #
# NOTE: The command line option used to list the message digest algorithms     #
#       depends on the version of openssl being used.                          #
################################################################################
ifdef OPENSSL_LIST_ALGORITHMS_OPTION # {
ifneq ($(OPENSSL),) # {
define openssl_compute_digests
	@{ printf "$(1) size = %'d bytes \n" $$(stat --format=%s $(1));                                                                                                                     \
     while IFS= read -r ALGORITHM;                                                                                                                                                    \
     do                                                                                                                                                                               \
       echo "openssl dgst -$${ALGORITHM} $(1) → $$(openssl dgst -$${ALGORITHM} $(1) | sed 's/($(1))=/ =/')";                                                                          \
     done < <(openssl $(OPENSSL_LIST_ALGORITHMS_OPTION) | egrep -v "(=> |-|DSA)" | sort -u) ; } | gawk 'NR < 2 {print $$0; next } {print $$0 | "sort"}'                               \
                                                                                             | column -s" " -o" " -t                                                                  \
                                                                                             | sed "s/\(^.*$(1)\)\( \+→ \+\)\(.*$$\)/\1 → \3/"                                        \
                                                                                             | column -s"=" -o"=" -t                                                                  \
                                                                                             | sed "s/\(^$(1).*size \+\)\(= \+\)\([0-9].*$$\)/\1= \3/;s/openssl \+dgst/openssl dgst/"
endef
else # } {
define openssl_compute_digests
	@$(warning $(WARNING_LABEL) The openssl executable was not found. Please install the openssl executable or correct your PATH environment variable.)
endef
endif # }
else # } {
define openssl_compute_digests
	@$(warning $(WARNING_LABEL) Could not determine how to generate the list of digest algorithms for OpenSSL version [$(OPENSSL_VERSION)].)
endef
endif # }

################################################################################
# Computes the GnuPG message digests of the input file.                        #
#   $(1) - input file                                                          #
################################################################################
ifneq ($(GPG),) # {
define gpg_compute_digests
	@{ printf "$(1) size = %'d bytes \n" $$(stat --format=%s $(1));                                                                                                                                                         \
     for ALGORITHM in $$(gpg --version | grep "^Hash" | sed 's/^Hash: //;s/,/ /g');                                                                                                                                       \
     do                                                                                                                                                                                                                   \
       echo "gpg --print-md $${ALGORITHM} $(1) → $${ALGORITHM} = $$(gpg --print-md $${ALGORITHM} $(1) | sed 's/^ */ /' | sed ':a;/:/{N;s/\n//;ba}' | sed "s/\(^ \)\($(1)\)\(: \)\(.*$$\)/\2:\L\4\E/;s/$(1)://;s/ //g;")"; \
     done; } | gawk 'NR < 2 {print $$0; next } {print $$0 | "sort"}'                                                                                                                                                      \
             | column -s" " -o" " -t                                                                                                                                                                                      \
             | sed "s/\(^.*$(1)\)\( \+→ \+\)\(.*$$\)/\1 → \3/"                                                                                                                                                            \
             | column -s"=" -o"=" -t                                                                                                                                                                                      \
             | sed "s/\(^gpg \+\)\(--.*$$\)/gpg \2/;s/\(^$(1) size \+\)\(= \+\)\(.*$$\)/\1 = \3/"                                                                                                                         \
             | column -s"=" -o"=" -t
endef
else # } {
define gpg_compute_digests
	@$(warning $(WARNING_LABEL) The gpg executable was not found. Please install the gpg executable or correct your PATH environment variable.)
endef
endif # }

################################################################################
# Computes the shasum message digests of the input file.                       #
#   $(1) - input file                                                          #
################################################################################
ifneq ($(SHASUM),) # {
define shasum_compute_digests
	@{ printf "$(1) size = %'d bytes\n" $$(stat --format=%s $(1));                                                 \
     for ALGORITHM in 1 224 256 384 512 512224 512256;                                                           \
     do                                                                                                          \
       echo "shasum -a $${ALGORITHM} $(1) → $${ALGORITHM} = $$(shasum -a $${ALGORITHM} $(1) | sed 's/ .*$$//')"; \
     done; } | column -s" " -o" " -t                                                                             \
             | sed "s/\(^.*$(1)\)\( \+→ \+\)\(.*$$\)/\1 → \3/"                                                   \
             | column -s"=" -o"=" -t                                                                             \
             | sed 's/= \+/= /'
endef
else # } {
define shasum_compute_digests
	@$(warning $(WARNING_LABEL) The shasum executable was not found. Please install the shasum executable or correct your PATH environment variable.)
endef
endif # }

################################################################################
# Computes the message digests of the input file using the standard availabkle #
# appliations, e.g., md5sim, sha1sum, etc, in the directory /bin (part of GNU  #
# core utils).                                                                 #
#   $(1) - input file                                                          #
################################################################################
define gnu_core_utils_compute_digests
	@{ printf "$(1) size = %'d bytes\n" $$(stat --format=%s $(1));                                                                                                         \
     for ALGORITHM in $$(ls -1 /bin/*sum /usr/local/bin/*sum 2> /dev/null | egrep -v shasum | xargs -n 1 basename 2> /dev/null | egrep "^(md5|sha|b2).*sum" | sort -u ); \
     do                                                                                                                                                                  \
       echo "$${ALGORITHM} $(1) → $$(echo $${ALGORITHM} | sed 's/sum//' | tr '[:lower:]' '[:upper:]') = $$($${ALGORITHM} $(1) | sed 's/ .*$$//')";                       \
     done; } | column -s" " -o" " -t                                                                                                                                     \
             | sed "s/\(^.*$(1)\)\( \+→ \+\)\(.*$$\)/\1 → \3/"                                                                                                           \
             | column -s"=" -o"=" -t                                                                                                                                     \
             | sed 's/= \+/= /'
endef

################################################################################
# Writes out the message digests for the input file as simple ASCII tables.    #
# $(1) - existing digest makefile target                                       #
# $(2) - digest label                                                          #
################################################################################
define create_digests_table
	+@$(MAKE_NO_DIR) $(1)                                                                              | \
    sed 's/^/| /;s/$$/|/;s/=/|/;s/→/|/;s/size/| size/;s/ \+/ /g'                                     | \
    sed "/^|.* size .* bytes.*$$/a | Command | Algorithm | Message Digest |" | column -s"|" -o"|" -t | \
    sed "s/\(^| \)\(.*[^ ]\)\( \+ | \)\(size\)\( \+ | \)\([0-9]\+.* bytes\)\(.*\)/$(2): \2 size \6/" | \
    gawk ' /./ {                                                                                       \
      len = length($$0);                                                                               \
      if (match($$0,/ size .* bytes/) > 0)                                                             \
      {                                                                                                \
        if (1 == first_digest)                                                                         \
        {                                                                                              \
          printf("%s\n\n", dashes);                                                                    \
        }                                                                                              \
        printf("+-%s-+\n", gensub(/ /, "-", "g", sprintf("%*s", len, "")));                            \
        printf("| %s |\n", $$0);                                                                       \
        size_line = 1;                                                                                 \
        first_digest = 1;                                                                              \
      }                                                                                                \
      else if (1 == size_line)                                                                         \
      {                                                                                                \
        split($$0, line_chars, "");                                                                    \
        pipe_index_2 = 0;                                                                              \
        pipe_index_3 = 0;                                                                              \
        for (i = 2; i < len; i++)                                                                      \
        {                                                                                              \
          if ("|" == line_chars[i])                                                                    \
          {                                                                                            \
            if (0 == pipe_index_2)                                                                     \
            {                                                                                          \
              pipe_index_2 = i;                                                                        \
            }                                                                                          \
            else                                                                                       \
            {                                                                                          \
              pipe_index_3 = i;                                                                        \
              break;                                                                                   \
            }                                                                                          \
          }                                                                                            \
        }                                                                                              \
        dashes = gensub(/ /, "-", "g", sprintf("%*s", len - 2, ""));                                   \
        dashes = "+"dashes"+";                                                                         \
        dashes = replace_at(dashes, pipe_index_2, "+");                                                \
        dashes = replace_at(dashes, pipe_index_3, "+");                                                \
        printf("%s\n", dashes);                                                                        \
        printf("%s\n", $$0);                                                                           \
        printf("%s\n", dashes);                                                                        \
        size_line = 0;                                                                                 \
      }                                                                                                \
      else                                                                                             \
      {                                                                                                \
        printf("%s\n", $$0);                                                                           \
      }                                                                                                \
    }                                                                                                  \
    END {                                                                                              \
      printf("%s\n\n", dashes);                                                                        \
    }                                                                                                  \
    function replace_at(s, pos, rep)                                                                   \
    {                                                                                                  \
      return (substr(s, 1, pos - 1) rep substr(s, pos + 1));                                           \
    }'
endef

.PHONY: digest_tables
digest_tables:: $(EXE)
	@$(call create_digests_table,openssl_digests,OpenSSL Digests)
	@$(call create_digests_table,gpg_digests,GnuPG Digests)
	@$(call create_digests_table,shasum_digests,shasum Digests)
	@$(call create_digests_table,gnu_core_utils_digests,GNU Core Utils Digests)

DIGESTS_FILE ?= $(CURDIR)/$(notdir $(CURDIR)).dgst
DIGESTS_DIRECTORY=$(dir $(DIGESTS_FILE))
$(DIGESTS_DIRECTORY):
	@mkdir -p $(DIGESTS_DIRECTORY)

.PHONY: digests_file
digests_file $(DIGESTS_FILE): | $(DIGESTS_DIRECTORY)
	+@$(MAKE_NO_DIR) digest_tables >| $(DIGESTS_FILE)

.PHONY: openssl_digests
OPENSSL_LOGO=openssl.png
openssl_digests:: $(EXE)
	@$(call openssl_compute_digests,$(<))

.PHONY: gpg_digests
GPG_LOGO=gnupg_logo.png
gpg_digests:: $(EXE)
	@$(call gpg_compute_digests,$(<))

.PHONY: shasum_digests
SHASUM_LOGO=hash.png
shasum_digests:: $(EXE)
	@$(call shasum_compute_digests,$(<))

.PHONY: gnu_core_utils_digests
GNU_CORE_UTILS_LOGO=gnu_core_utils_message_digests.png
gnu_core_utils_digests:: $(EXE)
	@$(call gnu_core_utils_compute_digests,$(<))

.PHONY: digests
digests:
	+@$(MAKE_NO_DIR) openssl_digests
	+@$(MAKE_NO_DIR) gpg_digests
	+@$(MAKE_NO_DIR) shasum_digests
	+@$(MAKE_NO_DIR) gnu_core_utils_digests

.PHONY: digest_clean
digest_clean:
	@rm -f $(DIGESTS_FILE)
	@[[ -d $(DIGESTS_DIRECTORY) ]] && rmdir --ignore-fail-on-non-empty $(DIGESTS_DIRECTORY)

clean:: digest_clean

.PHONY: msg_digests_mk_help
msg_digests_mk_help:
	@echo '$(MAKE) digest_tables → prints message digests as simple ASCII tables'
	@echo '$(MAKE) digests → prints message digests to stdout'

show_help:: msg_digests_mk_help
endif # }

endif # }

