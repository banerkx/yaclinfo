
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PYLAMA_MK_INCLUDE_GUARD # {
PYLAMA_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including vars_common.mk.                  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including python.mk.                       #
################################################################################
ifeq ($(PYTHON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/python.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

PYTHON_PACKAGES += pylama
$(eval $(call create_pip_show_target,pylama))
$(eval $(call create_pip_show_files_target,pylama))

EDITOR  ?= vim +1
BROWSER ?= firefox

PYLAMA=pylama
PYLAMA_COMMAND:=$(shell command -v $(PYLAMA))
ifeq ($(PYLAMA_COMMAND),) # {
$(error $(ERROR_LABEL) The [$(PYLAMA)] command is not available)
endif # }
PYLAMA_VERSION=$(call get_stdout_version,$(PYLAMA),--version,| sed 's/^.* //')

################################################################################
#  C0121 → singleton comparison                                                #
#  C0122 → misplaced comparison constant                                       #
#  C0325 → unnecessary parens                                                  #
#  E111  → indentation is not a multiple of four                               #
#  E114  → indentation is not a multiple of four (comment)                     #
#  E115  → expected an indented block (comment)                                #
#  E121  → continuation line under-indented for hanging indent                 #
#  E126  → continuation line over-indented for hanging indent                  #
#  E128  → continuation line under-indented for visual indent                  #
#  E203  → whitespace before ':'                                               #
#  E221  → multiple spaces before operator                                     #
#  E222  → multiple spaces after operator                                      #
#  E251  → unexpected spaces around keyword / parameter equals                 #
#  E266  → too many leading '#' for block comment                              #
#  E302  → expected 2 blank lines, found 0                                     #
#  E305  → expected 2 blank lines after class or function definition, found 1  #
#  E501  → line too long (80 > 79 characters)                                  #
#  E711  → comparison to None should be 'if cond is not None:'                 #
#  E712  → comparison to True should be 'if cond is True:' or 'if cond:'       #
#  W391  → blank line at end of file                                           #
#  W504  → line break after binary operator                                    #
#  W0311 → Bad indentation                                                     #
################################################################################
PYLAMA_IGNORE=C0121 C0122 C0325 E111 E114 E115 E121 E126 E128 E203 E221 E222 E251 E266 E302 E305 E501 E711 E712 W391 W504 W0311

PYLAMA_ABSPATH=--abspath
PYLAMA_FORMAT=--format json
PYLAMA_PYDOC_STYLE_CONVENTION=--pydocstyle-convention pep257
PYLAMA_SORT=--sort E,W,D

PYLAMA_LINTERS += eradicate    # helps clean up existing commented out code; see: https://github.com/myint/eradicate
PYLAMA_LINTERS += import-order
PYLAMA_LINTERS += import_order
PYLAMA_LINTERS += isort
PYLAMA_LINTERS += mccabe       # code complexity checker; see: https://nedbatchelder.com/blog/200803/python_code_complexity_microtool.html
PYLAMA_LINTERS += mypy         # python static typing; see: https://github.com/python/mypy
PYLAMA_LINTERS += pycodestyle  # python style guide checks (formerly pep8); see: https://github.com/PyCQA/pycodestyle
PYLAMA_LINTERS += pydocstyle   # docstring style checker; see: https://github.com/PyCQA/pydocstyle
PYLAMA_LINTERS += pyflakes     # checks python files for errors; see: https://github.com/PyCQA/pyflakes
PYLAMA_LINTERS += pylint       # python linter; see: https://pylint.org
PYLAMA_LINTERS += radon        # computes various metrics from the source code; see: https://github.com/rubik/radon
PYLAMA_LINTERS += vulture      # finds dead code; see: https://github.com/jendrikseipp/vulture

PYLAMA_OPTS=$(PYLAMA_ABSPATH) $(PYLAMA_FORMAT) $(PYLAMA_PYDOC_STYLE_CONVENTION) $(PYLAMA_SORT)

ifneq ($(PYLAMA_IGNORE),) # {
PYLAMA_OPTS += --ignore $(call commafy,$(sort $(PYLAMA_IGNORE)))
endif # }

ifneq ($(PYLAMA_LINTERS),) # {
PYLAMA_OPTS += --linters $(call commafy,$(sort $(PYLAMA_LINTERS)))
endif # }

PYLAMA_PROJECT=$(notdir $(CURDIR))
PYLAMA=pylama
PYLAMA_OUTPUT=$(PYLAMA_PROJECT)_$(PYLAMA)
PYLAMA_URL=https://klen.github.io/pylama/

PYTHON_SCRIPTS:=$(sort $(PYTHON_SCRIPTS))
$(call check_python_scripts_existence,$(PYTHON_SCRIPTS))

################################################################################
# Adds to the pylama target for the input Python script so that pylama can be  #
# run on all Python scripts at once.                                           #
#                                                                              #
# Example:                                                                     #
#   make pylama                                                                #
#                                                                              #
# Adds a "::" pylama target for the script so that this target and another     #
# target can be run simultaneously.                                            #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make my_script.py                                                          #
#                                                                              #
# Adds a pylama target, prefixed with 'f', so that pylama can be run only for  #
# this script.                                                                 #
#                                                                              #
# Example (assume that the script name is /dir1/dir1/dir3/my_script.py):       #
#   make fmy_script.py                                                         #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
##python -c "import json.tool" 2> /dev/null then check ${?}
.PHONY: pylama
define add_pylama_targets
pylama::
	@$$(eval PYLAMA_STUB=$$(subst .py,_py,$$(notdir $(1))))
	@mkdir -p $(PYLAMA_OUTPUT)/$$(PYLAMA_STUB)
	@rm -rf $(PYLAMA_OUTPUT)/$$(PYLAMA_STUB)/*
	@$$(eval PYLAMA_JSON=$(PYLAMA_OUTPUT)/$$(PYLAMA_STUB)/$(PYLAMA)_$$(PYLAMA_STUB).json)
	@rm -f $$(PYLAMA_JSON)
##	@$(PYLAMA) $(PYLAMA_OPTS) $(1) python -m json.tool >| $$(PYLAMA_JSON) || :
	@$(PYLAMA) $(PYLAMA_OPTS) $(1) | python -m json.tool | tee rest_py.json | pylint-json2html --input-format pylint --output rest_py.html
##	@if [[ -s $$(PYLAMA_JSON) ]];                                                  \
##   then                                                                          \
##     grep -q "^$(1):[0-9]*: " $$(PYLAMA_JSON);                                   \
##     if [[ 0 == $$$${?} ]];                                                      \
##     then                                                                        \
##       echo '$(EDITOR) $$(PYLAMA_JSON)';                                         \
##     else                                                                        \
##       echo '$(INFO_LABEL) No $(PYLAMA) issues found for Python script [$(1)].'; \
##       rm -f $$(PYLAMA_JSON);                                                    \
##       rmdir $(PYLAMA_OUTPUT)/$$(PYLAMA_STUB);                                   \
##       rmdir --ignore-fail-on-non-empty $(PYLAMA_OUTPUT);                        \
##     fi;                                                                         \
##   else                                                                          \
##     echo '$(INFO_LABEL) No $(PYLAMA) issues found for Python script [$(1)].';   \
##     rm -f $$(PYLAMA_JSON);                                                      \
##     rmdir $(PYLAMA_OUTPUT)/$$(PYLAMA_STUB);                                     \
##     rmdir --ignore-fail-on-non-empty $(PYLAMA_OUTPUT);                          \
##   fi
################################################################################
# Creating the "::" target.                                                    #
################################################################################
.PHONY: $(notdir $(1))
$(notdir $(1))::
	+@$(MAKE) $(NO_DIR) PYTHON_SCRIPTS=$(1) pylama
################################################################################
# Creating the single script target.                                           #
################################################################################
.PHONY: l$(notdir $(1))
l$(notdir $(1)):
	+@$(MAKE) $(NO_DIR) PYTHON_SCRIPTS=$(1) pylama
endef
$(foreach py,$(PYTHON_SCRIPTS),$(eval $(call add_pylama_targets,$(py))))

pylama_clean:
	@rm -rf $(PYLAMA_OUTPUT)

clean:: pylama_clean

platform_info::
	@echo '$(PYLAMA) version = [$(PYLAMA_VERSION)]'

################################################################################
# Prints help information of the double-colon and the "f" targets.             #
#                                                                              #
# $(1) - Python script                                                         #
################################################################################
define pylama_show_help
echo '$(MAKE) $(1) → double-colon target to run [$(PYLAMA)], and possibly other target(s), only on [$(1)]';
echo '$(MAKE) l$(1) → run [$(PYLAMA)] only on [$(1)]';
endef

PYTHON_SCRIPTS_COUNT=$(words $(PYTHON_SCRIPTS))
ifneq ($(PYTHON_SCRIPTS_COUNT),0) # {
PLURAL=$(if $(filter 1,$(PYTHON_SCRIPTS_COUNT)),,s)

pylama_mk_help::
	@echo '$(MAKE) pylama → runs $(PYLAMA) on the Python script$(PLURAL) $(call setify,$(PYTHON_SCRIPTS)) and saves the output file$(PLURAL) under [$(PYLAMA_OUTPUT)]'
	@$(foreach py,$(notdir $(PYTHON_SCRIPTS)),$(call pylama_show_help,$(py)))

show_help:: pylama_mk_help

endif # }

endif # }

