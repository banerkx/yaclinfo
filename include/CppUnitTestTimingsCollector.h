
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <cppunit/Test.h>
#include <cppunit/TestResultCollector.h>
#include <sys/time.h>

#include <chrono>
#include <iostream>
#include <locale>
#include <string>

CPPUNIT_NS_BEGIN

class CppUnitTestTimingsCollector : public CPPUNIT_NS::TestResultCollector
{
  public:

    typedef struct timespec TimeInfo;

    /**
     * Constructor
     *   @param tStream - stream to capture test execution times
     *   @param cSeparator - character used to separate the unit test name from
     * the execution time.  Default is space.
     */
    explicit CppUnitTestTimingsCollector(std::ostream& tStream, const char cSeparator = ' ');

    /**
     * Virtual destructor
     */
    virtual ~CppUnitTestTimingsCollector() override;

    /**
     * Called before the start of each unit test. Saves the start time of the
     * unit test.
     *   @param pTest - the unit test
     */
    void startTest(CPPUNIT_NS::Test* pTest) override;

    /**
     * Called after the end of each unit test. Computes the elapsed time for the
     * unit test.
     *   @param pTest - the unit test
     */
    void endTest(CPPUNIT_NS::Test* pTest) override;

    /**
     * Converts the difference between the start and stop times into a
     * string representation: days, hours, minutes, seconds, microseconds,
     * nanoseconds
     *   @param tStopTime - the stop time
     *   @param tStartTime - the start time
     *   @return - a string representation in days, hours, minutes, seconds,
     *             microseconds, and nanoseconds
     */
    static std::string secondsToText(
      const std::chrono::high_resolution_clock::time_point& tStopTime,
      const std::chrono::high_resolution_clock::time_point& tStartTime);

  private:

    std::ostream&                                  m_tStream;
    char                                           m_cSeparator;
    std::locale                                    m_tCurrentLocale;
    std::chrono::high_resolution_clock::time_point m_tStartTime;

    // Intentionally deleted.
    CppUnitTestTimingsCollector(const CppUnitTestTimingsCollector& rhs) = delete;
    CppUnitTestTimingsCollector(CppUnitTestTimingsCollector&& rhs)      = delete;
    CppUnitTestTimingsCollector& operator=(const CppUnitTestTimingsCollector& rhs) = delete;
    CppUnitTestTimingsCollector& operator=(CppUnitTestTimingsCollector&& rhs) = delete;
};

CPPUNIT_NS_END

