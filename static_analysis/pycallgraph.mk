
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PYCALLGRAPH_MK_INCLUDE_GUARD # {
PYCALLGRAPH_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/common.mk.       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/graph_viz.mk.    #
################################################################################
ifeq ($(GRAPH_VIZ_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/graph_viz.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/python.mk.       #
################################################################################
ifeq ($(PYTHON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/python.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

PYCALLGRAPH_PROJECT=$(notdir $(CURDIR))
PYCALLGRAPH_OUTPUT=$(PYCALLGRAPH_PROJECT)_pycallgraph
PYCALLGRAPH_EXT=png
PYCALLGRAPH_URL=https://github.com/daneads/pycallgraph2

EDITOR ?= vim +1
BROWSER ?= firefox

PYCALLGRAPH_COMMAND=pycallgraph
PYCALLGRAPH:=$(shell command -v $(PYCALLGRAPH_COMMAND))

PYCALLGRAPH_GOALS=pycallgraph_help pycallgraph

ifneq ($(filter $(PYCALLGRAPH_GOALS) pycallgraph_mk_help show_help clean platform_info print-%,$(MAKECMDGOALS)),) # {

ifneq ($(PYCALLGRAPH),) # {

PYCALLGRAPH_VERSION=$(call get_python_package_version,$(PYCALLGRAPH_COMMAND)2)

ifneq ($(DOT),) # {

.PHONY: pycallgraph_help
PYCALLGRAPH_HELP=pycallgraph_help.txt
pycallgraph_help:
	@$(PYCALLGRAPH) --help >| $(PYCALLGRAPH_HELP)
	@$(call open_file_command,$(PYCALLGRAPH_HELP),$(EDITOR),pycallgraph help)

.PHONY: pycallgraph

ifdef PYTHON_SCRIPTS # {

################################################################################
# Creates the pycallgraph target name from the name of the python script.      #
#                                                                              #
# $(1) - python script                                                         #
################################################################################
get_pycallgraph_target_name=$(subst .py,,$(notdir $(1)))_pycallgraph

$(PYCALLGRAPH_OUTPUT):
	@mkdir -p $(PYCALLGRAPH_OUTPUT)

##PYCALLGRAPH_MAX_DEPTH=--max-depth 5
##PYCALLGRAPH_VERBOSE=--verbose
##PYCALLGRAPH_DEBUG=--debug
PYCALLGRAPH_OPTS=$(strip $(PYCALLGRAPH_MAX_DEPTH) $(PYCALLGRAPH_VERBOSE) $(PYCALLGRAPH_DEBUG))
PYCALLGRAPH_OUTPUT_TYPE=graphviz

################################################################################
# Creates a traget to run pycallgraph on the input python script.              #
#                                                                              #
# $(1) - python script                                                         #
#                                                                              #
# NOTE: pycallgraph performs its analysis by executing the specified python    #
#       script.                                                                #
################################################################################
define cr8_pycallgraph_target

$(call get_pycallgraph_target_name,$(1)): $(PYCALLGRAPH_OUTPUT)
	@$$(eval PYCALLGRAPH_OUTPUT_FILE=$(PYCALLGRAPH_OUTPUT)/$(subst .py,.$(PYCALLGRAPH_EXT),$(notdir $(1))))
	@rm -f $$(PYCALLGRAPH_OUTPUT_FILE)
	@pycallgraph $(PYCALLGRAPH_OPTS) $(PYCALLGRAPH_OUTPUT_TYPE) --output-file=$$(PYCALLGRAPH_OUTPUT_FILE) -- $(1) > /dev/null 2>&1 || :
	@$$(call open_file_command,$$(PYCALLGRAPH_OUTPUT_FILE),$(BROWSER),$(1) python call graph)

pycallgraph: $(call get_pycallgraph_target_name,$(1))

endef
$(foreach py,$(PYTHON_SCRIPTS),$(eval $(call cr8_pycallgraph_target,$(py))))

pycallgraph:
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))

else # } {

pycallgraph:
	@$(error $(ERROR_LABEL) No python scripts have been specified (in the variable [PYTHON_SCRIPTS]))

endif # }

pycallgraph_clean:
	@rm -rf $(PYCALLGRAPH_OUTPUT)

clean:: pycallgraph_clean
	@rm -f $(PYCALLGRAPH_HELP)

platform_info::
	@echo 'pycallgraph version = [$(PYCALLGRAPH_VERSION)]'

pycallgraph_mk_help:
	@echo '$(MAKE) pycallgraph → run pycallgraph on python scripts'
	@echo '$(MAKE) pycallgraph_help → save pycallgraph help information to [$(PYCALLGRAPH_HELP)]'

show_help:: pycallgraph_mk_help

else # } {

$(PYCALLGRAPH_GOALS):
	@$(error $(ERROR_LABEL) The [$(DOT_COMMAND)] executable is not available, ∴ the target [$(@)] can not be made)

endif # }

else # } {

$(PYCALLGRAPH_GOALS):
	@$(error $(ERROR_LABEL) The [pycallgraph] executable is not available, ∴ the target [$(@)] can not be made; either fix your [PATH] environment variable or install [pycallgraph]; see: [$(PYCALLGRAPH_URL)])

endif # }

endif # }

endif # }

