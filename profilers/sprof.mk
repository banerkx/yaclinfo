
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef SPROF_MK_INCLUDE_GUARD # {
SPROF_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including common.mk.                       #
################################################################################
ifeq ($(COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

################################################################################
# See: man 1 sprof                                                             #
################################################################################

SPROF_COMMAND=sprof
SPROF_OUTPUT=$(CURDIR)/sprof_output
SPROF_EXE ?= $(EXE)

PROFILER_OUTPUT_DIRS += $(SPROF_OUTPUT)

.PHONY: sprof_mk_help
SPROF:=$(shell command -v $(SPROF_COMMAND))
SPROF_VERSION=$(call get_stdout_version_index,$(SPROF),--version,4)

SPROF_EXTERNAL_TARGETS=clean help sprof_mk_help platform_info show_help vars print-% qprint-%

ifneq ($(SPROF),) # {
sprof_mk_help::
ifneq ($(SPROF_EXTERNAL_TARGETS),) # {
	@echo '$(MAKE) sprof → perform $(SPROF_COMMAND) run(s)'

show_help:: sprof_mk_help

platform_info::
	@echo '$(SPROF_COMMAND) = [$(SPROF_VERSION)]'

endif # }
endif # }

DOT_IMAGE_EXTENSION=svg

################################################################################
# Gets the dynamic library name portion from the sprof output file name.       #
#                                                                              #
# $(1) - executable name                                                       #
# $(2) - sprof output file                                                     #
################################################################################
get_lib_name=$(notdir $(subst _$(1),,$(basename $(2))))

.PHONY: sprof_clean
sprof_clean:
	@rm -rf $(SPROF_OUTPUT)

.PHONY: sprof_output_clean
sprof_output_clean::
	@:

clean:: sprof_clean

################################################################################
# NOTE: Fill in $(RUN_OPTS) **before** including this file.                    #
################################################################################
RUN_OPTS ?=
SPROF_EXE_COMMAND=$(strip ./$(SPROF_EXE) $(RUN_OPTS))

SPROF_TARGETS=sprof sprof_clean sprof_prep
.PHONY: $(SPROF_TARGETS)

################################################################################
# Checking to see that the SPROF_PROFILER_LIBS variable has been set. This     #
# variable must be set **before** including this makefile.                     #
################################################################################
ifeq ($(SPROF_PROFILER_LIBS),) # {
ifneq ($(filter $(SPROF_TARGETS),$(MAKECMDGOALS)),) # {
$(error $(ERROR_LABEL) The SPROF_PROFILER_LIBS variable has not been set; [$(SPROF_COMMAND)] profiling is not possible)
endif # }
endif # }

ifneq ($(filter $(SPROF_TARGETS) $(SPROF_EXTERNAL_TARGETS),$(MAKECMDGOALS)),) # {

ifneq ($(CXX),g++) # {
ifneq ($(filter $(SPROF_TARGETS),$(MAKECMDGOALS)),) # {
$(error $(ERROR_LABEL) [$(SPROF_COMMAND)] can only be used with [g++], not with [$(CXX)])
endif # }
endif # }

################################################################################
# Relevant target(s):                                                          #
#                                                                              #
#   make sprof --> produces call graph, flat profile, and call pairs for the   #
#                  analysis for the specified dynamic library                  #
#                                                                              #
# NOTE:                                                                        #
#                                                                              #
#   - Must set the variable SPROF_PROFILER_LIBS to specify the dynamic library #
#     (or libraries) of interest **before** making the $(SPROF_COMMAND)        #
#     target.                                                                  #
#   - If running with the sprof profiler succeeds, then the output will be     #
#     saved to the file $(SPROF_OUTPUT_FILE).                                  #
#   - If needed, add the directory containing the dynamic library to           #
#     LD_LIBRARY_PATH by adding the following line to the makefile that        #
#     includes this makefile:                                                  #
#        LD_LIBRARY_PATH:=${LD_LIBRARY_PATH}:/new/lib/dir                      #
################################################################################

ifneq ($(SPROF),) # {

SPROF_CPP_FILT_COMMAND=c++filt
SPROF_CPP_FILT:=$(shell command -v $(SPROF_CPP_FILT_COMMAND))
ifneq ($(SPROF_CPP_FILT),) # {
SPROF_CPP_FILT_PIPE= | $(SPROF_CPP_FILT)

SPROF_DOT_COMMAND=dot
SPROF_DOT:=$(shell command -v $(SPROF_DOT_COMMAND))

ifeq ($(SPROF_DOT),) # {
$(warning $(WARNING_LABEL) The [$(SPROF_DOT_COMMAND)] command was not found; no [$(SPROF_COMMAND)] call graph image will be generated.)
else # } {

SPROF_GPROF2_DOT_COMMAND=gprof2dot
GPROF2_DOT:=$(shell command -v $(SPROF_GPROF2_DOT_COMMAND))

ifeq ($(GPROF2_DOT),) # {
$(warning $(WARNING_LABEL) The [$(SPROF_GPROF2_DOT_COMMAND)] command was not found; no [$(SPROF_COMMAND)] call graph image will be generated.)
else # } {

SPROF_DOT_SVG_RENDERER ?= :svg:core
SPROF_GPROF2_DOT_OPTS=--color-nodes-by-selftime \
                      --colormap=color          \
                      --format=prof             \
                      --node-thres=1.0          \
                      --total=callratios
endif # }

endif # }

endif # }

$(SPROF_OUTPUT):
	@mkdir -p $(SPROF_OUTPUT)

################################################################################
# Gets the stem name of the input dynamic library, i.e., everything after      #
# ".so" is removed.                                                            #
#                                                                              #
# $(1) - dynamic library                                                       #
################################################################################
get_lib_stem_name=$(shell echo $(1) | sed 's@\.so.*$$@@')

################################################################################
# Set this variable **before** including this makefile to set it to something  #
# other than clean.                                                            #
################################################################################
export SPROF_CLEAN_TARGET ?= clean

################################################################################
# Pattern rule to run sprof for each dynamic library in $(SPROF_PROFILER_LIBS).#
################################################################################
lib%:
################################################################################
# Getting the dynamic library stem name, i.e., dropping the ".so" portion.     #
################################################################################
	@$(eval SPROF_PROFILER_LIB_STEM:=$(call get_lib_stem_name,$(*)))
	@ldd $(SPROF_EXE) | grep -q $$'^\t'"lib$(SPROF_PROFILER_LIB_STEM)\.so\(\.[0-9]\+\)* =>";                \
   if [[ 0 != $${?} ]];                                                                                   \
   then                                                                                                   \
     echo '[$(@)] $(ERROR_LABEL) The executable [$(SPROF_EXE)] does not use the dynamic library [$(@)].'; \
     exit 1;                                                                                              \
   fi
	@$(eval SPROF_LIB_FULL_PATH:=$(shell ldd $(SPROF_EXE) | grep "$(@).*$(@)" | sed 's/^.*=> //;s/ .*$$//'))
	@if [[ "" == "$(SPROF_LIB_FULL_PATH)" ]];                                                                                                     \
   then                                                                                                                                         \
     echo '[$(@)] $(ERROR_LABEL) Could not determine the full path for the dynamic library [$(@)]; can not perform an [$(SPROF_COMMAND)] run.'; \
     exit 1;                                                                                                                                    \
   fi
	@$(eval SPROF_LIB_SONAME:=$(strip $(call get_soname,$(SPROF_LIB_FULL_PATH))))
	@if [[ "" == "$(SPROF_LIB_SONAME)" || "NONE" == "$(SPROF_LIB_SONAME)" || "UNKNOWN" == "$(SPROF_LIB_SONAME)" ]];        \
   then                                                                                                                  \
     echo '[$(@)] $(ERROR_LABEL) Could not determine the soname for [$(@)]; can not perform an [$(SPROF_COMMAND)] run.'; \
     exit 1;                                                                                                             \
   fi
	@$(eval SPROF_OUTPUT_FILE:=$(SPROF_OUTPUT)/$(SPROF_LIB_SONAME)_$(SPROF_EXE).txt)
	@$(eval SPROF_LOG:=$(SPROF_OUTPUT)/$(@).log)
	@rm -f $(SPROF_OUTPUT_FILE)
	@$(eval LD_PROFILE:=$(SPROF_LIB_SONAME))
	@echo 'LD_PROFILE=$(LD_PROFILE) LD_PROFILE_OUTPUT=$(SPROF_OUTPUT) $(SPROF_EXE_COMMAND)'                | tee    $(SPROF_LOG)
	@LD_PROFILE=$(LD_PROFILE) LD_PROFILE_OUTPUT=$(SPROF_OUTPUT) $(SPROF_EXE_COMMAND) 2>&1 || :             | tee -a $(SPROF_LOG)
	@echo 'LD_PROFILE=[$(LD_PROFILE)] LD_PROFILE_OUTPUT=[$(SPROF_OUTPUT)]'                                 | tee    $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Flat Profile for dynamic library [$(@)] and soname [$(SPROF_LIB_SONAME)]'                       | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Date/Time: [$(DATE_TIME)]'                                                                      | tee -a $(SPROF_OUTPUT_FILE)
	@$(eval PROFILE:=$(SPROF_OUTPUT)/$(SPROF_LIB_SONAME).profile)
	@$(eval SPROF_FLAT_PROFILE:=$(SPROF) --flat-profile $(LD_PROFILE) $(PROFILE) $(SPROF_CPP_FILT_PIPE))
	@echo '[$(@)] $(INFO_LABEL) Flat Profile Command: $(SPROF_FLAT_PROFILE)'                               | tee -a $(SPROF_OUTPUT_FILE)
	@echo '[$(@)] $(INFO_LABEL) Command: [$(SPROF_EXE_COMMAND)]'                                           | tee -a $(SPROF_OUTPUT_FILE)
	@$(SPROF_FLAT_PROFILE)                                                                                 | tee -a $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo ''                                                                                               | tee -a $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Call Graph for dynamic library [$(@)] and soname [$(SPROF_LIB_SONAME)]'                         | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Date/Time: [$(DATE_TIME)]'                                                                      | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'NOTE: "<UNKNOWN>" represents identifiers that are outside of the profiled object.'              | tee -a $(SPROF_OUTPUT_FILE)
	@$(eval SPROF_GRAPH:=$(SPROF) --graph $(SPROF_LIB_SONAME) $(PROFILE) $(SPROF_CPP_FILT_PIPE))
	@echo '[$(@)] $(INFO_LABEL) Graph Command: $(SPROF_GRAPH)'                                             | tee -a $(SPROF_OUTPUT_FILE)
	@echo '[$(@)] $(INFO_LABEL) Command: [$(SPROF_EXE_COMMAND)]'                                           | tee -a $(SPROF_OUTPUT_FILE)
	@$(SPROF_GRAPH)                                                                                        | tee -a $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo ''                                                                                               | tee -a $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Call Pairs for dynamic library [$(@)] and soname [$(SPROF_LIB_SONAME)]'                         | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'Date/Time: [$(DATE_TIME)]'                                                                      | tee -a $(SPROF_OUTPUT_FILE)
	@echo 'NOTE: "<UNKNOWN>" represents identifiers that are outside of the profiled object.'              | tee -a $(SPROF_OUTPUT_FILE)
	@$(eval SPROF_CALL_PAIRS:=$(SPROF) --call-pairs $(SPROF_LIB_SONAME) $(PROFILE) $(SPROF_CPP_FILT_PIPE))
	@echo '[$(@)] $(INFO_LABEL) Call Pairs Command: $(SPROF_CALL_PAIRS)'                                   | tee -a $(SPROF_OUTPUT_FILE)
	@echo '[$(@)] $(INFO_LABEL) Command: [$(SPROF_EXE_COMMAND)]'                                           | tee -a $(SPROF_OUTPUT_FILE)
	@$(SPROF_CALL_PAIRS)                                                                                   | tee -a $(SPROF_OUTPUT_FILE)
	@echo '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'               | tee -a $(SPROF_OUTPUT_FILE)
	@echo ''                                                                                               | tee -a $(SPROF_OUTPUT_FILE)
	@sed -i 's@us/call@µs/call@g'                                                                                   $(SPROF_OUTPUT_FILE)
	@sed -i 's/\x1b\[[^@-~]*[@-~]//g'                                                                               $(SPROF_OUTPUT_FILE)
	@[[ -e $(SPROF_OUTPUT_FILE) ]] && sed -i 's/Flat profile:/Flat Profile:/' $(SPROF_OUTPUT_FILE) || :
ifeq ($(if $(GPROF2_DOT),$(if $(SPROF_DOT),T)),T) # {
	@$(eval SPROF_DOT_OUTPUT:=$(SPROF_OUTPUT)/$(SPROF_LIB_SONAME)_$(SPROF_EXE).$(DOT_IMAGE_EXTENSION))
	@$(eval SPROF_DOT_OPTS=-T$(DOT_IMAGE_EXTENSION)$(SPROF_DOT_SVG_RENDERER) -o $(SPROF_DOT_OUTPUT))
	@sed '$$a' $(SPROF_OUTPUT_FILE)                                                                                                                                     | \
   $(GPROF2_DOT) $(SPROF_GPROF2_DOT_OPTS) 2> /dev/null                                                                                                                  | \
   sed '$$i\\ \ labelloc="t";'                                                                                                                                          | \
   sed '$$i\\ \ label="$(SPROF_COMMAND) Flat Profile Output for [$(SPROF_EXE_COMMAND)] and library [$(@)] and soname [$(SPROF_LIB_SONAME)] Date/Time: [$(DATE_TIME)]";' | \
   $(SPROF_DOT) $(SPROF_DOT_OPTS)
################################################################################
# If the diagram for the flat profile information was generated, the we delete #
# this information from the text file.                                         #
################################################################################
	@[[ -e $(SPROF_DOT_OUTPUT) ]] && sed -i -e '/^Flat Profile /,/^+++++/{d;}' -e '1,2d;' -e '1,2d' $(SPROF_OUTPUT_FILE) || :
	@$(call open_file_command,$(SPROF_DOT_OUTPUT),$(BROWSER),$(SPROF_COMMAND) call graph diagram)
endif # }
	@$(call open_file_command,$(SPROF_LOG),$(EDITOR),$(SPROF_COMMAND) log file)
	@$(call open_file_command,$(SPROF_OUTPUT_FILE),$(EDITOR),$(SPROF_COMMAND) profiler results)
	+@$(MAKE) sprof_output_clean
	@echo ''

################################################################################
# If we have more than 1 shared library to profile, then we must not run       #
# $(MAKE) in parallel mode (running in parallel causes problems for sprof??).  #
# Also, when not running in parallel, the order of double-colon rules is their #
# order of appearance in the makefile.                                         #
################################################################################
.NOTPARALLEL:

# Filtering out compiler flags that cause problems for sprof.
$(SPROF_COMMAND): SPROF_CXXFLAGS:=$(filter-out -pg,$(CXXFLAGS)) -Wno-unused-function
$(SPROF_COMMAND): DATE_TIME:=$(shell date "+%a %b %d, %Y %I:%M:%S %p %Z")
$(SPROF_COMMAND):: $(SPROF_CLEAN_TARGET) sprof_clean sprof_prep | $(SPROF_OUTPUT)
	@$(call target_info,$(@) Profiler,$(<),$(?),$(^),$(|),$(*))
	@echo '[$(@)] SPROF_CLEAN_TARGET = [$(SPROF_CLEAN_TARGET)]'
	+@$(MAKE_NO_DIR) CXXFLAGS="$(SPROF_CXXFLAGS)" $(SPROF_EXE)

$(SPROF_COMMAND):: $(SPROF_PROFILER_LIBS)
	@$(call elapsed_time,$(@),$(TARGET_START_TIME))
	@echo ''

.PHONY: sprof_prep
sprof_prep::;

else # } {

$(SPROF_TARGETS):
	@$(warning $(WARNING_LABEL) Can not make target [$(@)]; the [$(SPROF_COMMAND)] command is not available)

endif # }

endif # }

endif # }

