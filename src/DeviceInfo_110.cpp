
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo_110.h"

#include "cl_command_queue_properties.h"

DeviceInfo_110::DeviceInfo_110(
  const cl_device_id   nDeviceID,
  const size_t         nDeviceNumber,
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nDeviceVersion) noexcept :
  DeviceInfo_100(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber, nDeviceVersion),
  m_tHostUnifiedMemory(
    OPEN_cl_device_info_110_120::OPENCL_DEVICE_HOST_UNIFIED_MEMORY,
    "HAS UNIFIED MEMORY",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Do host and device have unified memory?")),
  m_tCharVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
    "CHAR VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native char vector width")),
  m_tShortVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
    "SHORT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native short vector width")),
  m_tIntVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
    "INT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native int vector width")),
  m_tLongVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
    "LONG VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native long vector width")),
  m_tFloatVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
    "FLOAT VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native float vector width")),
  m_tDoubleVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
    "DOUBLE VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Native double vector width")),
  m_tVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
    "HALF VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, native half scalar and "
                  "vector width")),
  m_tOpenCLCVersion(
    OPEN_cl_device_info::OPENCL_DEVICE_OPENCL_C_VERSION,
    "OpenCL C VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("OpenCL C version")),
  m_tPreferredVectorWidthHalf(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
    "HALF PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("If cl_khr_fp16 is supported, preferred native half scalar and "
                  "vector width that can be put into vectors"))
{
}

DeviceInfo_110::~DeviceInfo_110() noexcept
{
}

void DeviceInfo_110::getAllDeviceInfo() noexcept
{
  DeviceInfo_100::getAllDeviceInfo();

  getDeviceInfo(m_tHostUnifiedMemory);
  getDeviceInfo(m_tCharVectorWidth);
  getDeviceInfo(m_tShortVectorWidth);
  getDeviceInfo(m_tIntVectorWidth);
  getDeviceInfo(m_tLongVectorWidth);
  getDeviceInfo(m_tFloatVectorWidth);
  getDeviceInfo(m_tDoubleVectorWidth);
  getDeviceInfo(m_tVectorWidthHalf);
  getDeviceInfo(m_tOpenCLCVersion);
  getDeviceInfo(m_tPreferredVectorWidthHalf);
}

void DeviceInfo_110::getTextOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo_100::getTextOutput(tStream);

  tStream << m_tHostUnifiedMemory << std::endl;
  tStream << m_tCharVectorWidth << std::endl;
  tStream << m_tShortVectorWidth << std::endl;
  tStream << m_tIntVectorWidth << std::endl;
  tStream << m_tLongVectorWidth << std::endl;
  tStream << m_tFloatVectorWidth << std::endl;
  tStream << m_tDoubleVectorWidth << std::endl;
  tStream << m_tVectorWidthHalf << std::endl;
  tStream << m_tPreferredVectorWidthHalf << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tOpenCLCVersion)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tOpenCLCVersion).empty())
  {
    tStream << std::get<PARAM_VALUE>(m_tOpenCLCVersion).get();
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tOpenCLCVersion);
  }
  tStream << std::endl;
}

void DeviceInfo_110::getHTMLOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo_100::getHTMLOutput(tStream);

  DeviceInfo::writeHTMLTableRow(tStream, m_tHostUnifiedMemory);
  DeviceInfo::writeHTMLTableRow(tStream, m_tCharVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tShortVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tIntVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tLongVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tFloatVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tDoubleVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredVectorWidthHalf);
  DeviceInfo::writeHTMLTableRow(tStream, m_tOpenCLCVersion);
}

