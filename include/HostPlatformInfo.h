
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <sys/sysinfo.h>

#include <map>
#include <set>
#include <string>
#include <vector>

#include "IPAddresses.h"
#include "ResourceLimits.h"
#include "SingletonBase.h"

class HostPlatformInfo : public ConstSingletonBase<HostPlatformInfo>
{
  public:

    typedef std::string VideoCard;
    typedef std::string VideoCardProperties;

    virtual ~HostPlatformInfo();

    static const std::vector<std::string>& getLibraryDirectories();

    double totalUsableMemory() const;

    double availableMemory() const;

    double sharedMemory() const;

    double bufferMemory() const;

    double totalSwap() const;

    double freeSwap() const;

    unsigned short processCount() const;

    double highMemory() const;

    double freeHighMemory() const;

    unsigned int memoryUnitSize() const;

    inline unsigned int hardwareConcurrency() const
    {
      return (m_nHardwareConcurrency);
    }

    inline const std::string& aslrLevel() const
    {
      return (m_sAslrLevel);
    }

    inline const std::string& boostVersion() const
    {
      return (m_sBoostVersion);
    }

    inline const std::string& compilerTargetMachine() const
    {
      return (m_sCompilerTargetMachine);
    }

    inline const std::string& coreFilePattern() const
    {
      return (m_sCoreFilePattern);
    }

    inline const std::string& coreFileSize() const
    {
      return (m_sCoreFileSize);
    }

    inline const std::string& cpuModel() const
    {
      return (m_sCPUModel);
    }

    inline const std::string& currentDateTime() const
    {
      return (m_sCurrentDateTime);
    }

    inline const std::string& endianess() const
    {
      return (m_sEndianess);
    }

    inline const std::string& gitBranch() const
    {
      return (m_sGitBranch);
    }

    inline const std::string& gitRemoteRepo() const
    {
      return (m_sGitRemoteRepo);
    }

    inline const std::string& gitRoot() const
    {
      return (m_sGitRoot);
    }

    inline const std::string& gitVersion() const
    {
      return (m_sGitVersion);
    }

    inline const std::string& hypervisorVendor() const
    {
      return (m_sHypervisorVendor);
    }

    inline const std::string& javaHome() const
    {
      return (m_sJavaHome);
    }

    inline const std::vector<std::string>& classPaths() const
    {
      return (m_tClassPaths);
    }

    inline const std::string& javaVersion() const
    {
      return (m_sJavaVersion);
    }

    inline const std::string& libCPPVersion() const
    {
      return (m_sLibCPPVersion);
    }

    inline const std::string& libCVersion() const
    {
      return (m_sLibCVersion);
    }

    inline const std::string& localHost() const
    {
      return (m_sLocalHost);
    }

    inline const std::string& openSSLVersion() const
    {
      return (m_sOpenSSLVersion);
    }

    inline const std::string& operatingSystem() const
    {
      return (m_sOperatingSystem);
    }

    inline const std::string& plantUMLJar() const
    {
      return (m_sPlantUMLJar);
    }

    inline const std::string& plantUMLVersion() const
    {
      return (m_sPlantUMLVersion);
    }

    inline const std::string& pthreadsLibrary() const
    {
      return (m_sPThreadsLibrary);
    }

    inline const std::string& shell() const
    {
      return (m_sShell);
    }

    inline const std::string& shellVersion() const
    {
      return (m_sShellVersion);
    }

    inline const ResourceLimits& resourceLimits() const
    {
      return (m_tResourceLimits);
    }

    inline int processPriority() const
    {
      return (m_tResourceLimits.processPriority());
    }

    inline const std::vector<IPAddresses::IPAddressInfo>& ipAddresses() const
    {
      return (m_tIPAddresses);
    }

    inline const std::map<VideoCard, VideoCardProperties>& videoCardInfo() const
    {
      return (m_tVideoCardInfo);
    }

    inline const std::map<std::string, std::string>& hostPlatformAttributes() const
    {
      return (m_tHostPlatformAttributes);
    }

    inline const std::vector<std::string>& gitSubModules() const
    {
      return (m_tGitSubmodules);
    }

    std::string uptime() const;

    void addPlatformAttribute(const char* sAttribute, const std::string& sValue) const;

    std::string loadAverages() const;

  private:

    const unsigned int                            m_nHardwareConcurrency;
    const std::string                             m_sAslrLevel;
    const std::string                             m_sBoostVersion;
    const std::string                             m_sCPUModel;
    const std::string                             m_sCompilerTargetMachine;
    const std::string                             m_sCoreFilePattern;
    const std::string                             m_sCoreFileSize;
    const std::string                             m_sCurrentDateTime;
    const std::string                             m_sEndianess;
    const std::string                             m_sGitBranch;
    const std::string                             m_sGitRemoteRepo;
    const std::string                             m_sGitRoot;
    const std::string                             m_sGitVersion;
    const std::string                             m_sHypervisorVendor;
    const std::string                             m_sJavaHome;
    const std::vector<std::string>                m_tClassPaths;
    const std::string                             m_sJavaVersion;
    const std::string                             m_sLibCPPVersion;
    const std::string                             m_sLibCVersion;
    const std::string                             m_sLocalHost;
    const std::string                             m_sOpenSSLVersion;
    const std::string                             m_sOperatingSystem;
    const std::string                             m_sPThreadsLibrary;
    const std::string                             m_sPlantUMLJar;
    const std::string                             m_sPlantUMLVersion;
    std::string                                   m_sShell;
    std::string                                   m_sShellVersion;
    const ResourceLimits&                         m_tResourceLimits;
    const std::vector<IPAddresses::IPAddressInfo> m_tIPAddresses;
    std::map<VideoCard, VideoCardProperties>      m_tVideoCardInfo;
    mutable std::map<std::string, std::string>    m_tHostPlatformAttributes;
    struct sysinfo                                m_tSysInfo;
    int                                           m_nSysInfoStat;
    std::vector<std::string>                      m_tGitSubmodules;

    HostPlatformInfo();

    void setShellInfo();

    static std::string endianCheck();

    static std::string getCoreFilePattern();

    static std::string getCoreFileSize();

    static std::string getCurrentDateTime();

    static std::string getCpuModel();

    static std::string getOperatingSystemInfo();

    static std::string getStdCPPVersion();

    static std::string getASLRLevel();

    static std::string getBoostVersion();

    static void findAncestorShellProcess(
      const std::set<std::string>&   tShells,
      std::pair<pid_t, std::string>& tAncestorShell);

    static std::string getProcessCanonicalName(const pid_t nPID);

    static std::string getPthreadsLibrary();

    static std::string getPlatformAttribute(std::string sCommand);

    void setVideoCardInfo();

    static std::string getCardName(const std::string& sProperty);

    static std::string getCompilerTargetMachine();

    static std::string getCompilerVersion();

    static std::string getJarFileLocation(std::string sJarFile);
    static std::string getJarFileLocation(
      std::string                     sJarFile,
      const std::vector<std::string>& tClassPaths);

    static std::string getPlantUMLVersion(const std::string& sPlantUMLJar);

    static std::string getJavaHome();

    void populateHostPlatformAttributes();

    static std::string getCanonicalPath(const std::string& sFile);

    static std::string assembleStringsVector(const std::vector<std::string>& tStrings);

    static std::vector<std::string> getClassPaths();

    void getGitSubmodules();

    friend class ConstSingletonBase<HostPlatformInfo>;

    // Intentionally deleted.
    HostPlatformInfo(const HostPlatformInfo& rhs) = delete;
    HostPlatformInfo(HostPlatformInfo&& rhs)      = delete;
    HostPlatformInfo&       operator=(const HostPlatformInfo& rhs) = delete;
    HostPlatformInfo&       operator=(HostPlatformInfo&& rhs) = delete;
    HostPlatformInfo*       operator&()                       = delete;
    const HostPlatformInfo* operator&() const                 = delete;
};

