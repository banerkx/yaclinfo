
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef OPENCL_MK_INCLUDE_GUARD # {
OPENCL_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/git.mk.          #
################################################################################
ifeq ($(GIT_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/git.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

KHRONOS_UNIFIED_C_HEADERS_URL:=https://github.com/KhronosGroup/OpenCL-Headers.git
KHRONOS_UNIFIED_C_HEADERS_HASH:=$(call git_head_remote_hash,$(KHRONOS_UNIFIED_C_HEADERS_URL))

KHRONOS_UNIFIED_CPP_HEADERS_URL:=https://github.com/KhronosGroup/OpenCL-CLHPP.git
KHRONOS_UNIFIED_CPP_HEADERS_HASH:=$(call git_head_remote_hash,$(KHRONOS_UNIFIED_CPP_HEADERS_URL))

OPENCL_ICD_PATH=/etc/OpenCL/vendors
.PHONY: icd_opencl
ifneq ($(wildcard $(OPENCL_ICD_PATH)/.),) # {

KHRONOS_OPENCL_C_UNIFIED_HEADERS ?= /usr/include

################################################################################
# Setting the realpath of the OpenCL library. Then extracting the directory    #
# of the library and its stem name.                                            #
################################################################################
OPENCL_LIB_STEM ?= OpenCL
OPENCL_LIB_SO ?= $(call get_shared_lib_path,$(OPENCL_LIB_STEM))
OPENCL_LIB_DIR ?= $(dir $(OPENCL_LIB_SO))
OPENCL_LIB_SONAME ?= $(call get_soname,$(OPENCL_LIB_SO))
OPENCL_LIB_SO_VERSION=$(call get_shared_lib_version, $(OPENCL_LIB_SO))
OPENCL_LIB ?= $(notdir $(OPENCL_LIB_SO))
OPENCL_LIBS=$(realpath $(wildcard $(addsuffix /lib$(OPENCL_LIB_STEM).so,$(subst :, ,${LD_LIBRARY_PATH}))))

################################################################################
# HAVE_OPENCL      - "boolean" indicating OpenCL is present or not             #
# ICD_FILES        - the icd files                                             #
# OCL_LIBS         - the OpenCL dynamic library names                          #
# LIB_ARCHITECTURE - 64 (for 64-bit) or "*" (for 32-bit)                       #
# OPENCL_LIB_INFO  - list of statements that print the OpenCL library          #
#                    information                                               #
################################################################################
HAVE_OPENCL=0
ICD_FILES:=$(wildcard $(OPENCL_ICD_PATH)/*.icd)
ifdef ICD_FILES # {
OCL_LIBS:=$(shell cat $(ICD_FILES) 2> /dev/null | grep "^lib.*\.so" | sort -u)
endif # }
LIB_ARCHITECTURE:=$(shell if [[ $$(uname -m) == "x86_64" ]]; then echo 64; else echo "*"; fi)

ifeq ($(.DEFAULT_GOAL),) # {
.DEFAULT_GOAL:=all
endif # }

################################################################################
# Checks for the existence of the input OpenCL dynamic library file.           #
# $(1) - OpenCL dynamic library file                                           #
################################################################################
OPENCL_LIB_INFO=
define check_icd_lib
.PHONY: $(1)
CL_LIB_$(1)=$(shell /sbin/ldconfig -p | grep "$(LIB_ARCHITECTURE)/$(1)" | sed 's/^.* =>//')
ifeq ($$(CL_LIB_$(1)),) # {
CL_LIB_$(1)=$(call get_shared_lib_path,$(1))
endif # }
ICD_LIB_FILE_$(1)=$(shell grep -H $(1) $(ICD_FILES) | cut -d":" -f1)
ifeq ($$(CL_LIB_$(1)),) # {
OPENCL_LIB_RESULT_$(1)="ERROR: OpenCL library: [$$(ICD_LIB_FILE_$(1))] → [$(1)] → [was not found] → [SONAME: N/A] → [SIZE: N/A]"
else # } {
CL_LIB_REALPATH_$(1)=$$(sort $$(realpath $$(CL_LIB_$(1))))
ALL_OPENCL_LIBS += $$(CL_LIB_REALPATH_$(1))
ifeq ($$(CL_LIB_REALPATH_$(1)),) # {
OPENCL_LIB_RESULT_$(1)="ERROR: OpenCL library: [$$(ICD_LIB_FILE_$(1))] → [$(1)] → [library link target for [$$(CL_LIB_$(1))] was not found] → [SONAME: N/A] → [SIZE: N/A]"
else # } {
HAVE_OPENCL=1
OPENCL_LIB_RESULT_$(1)="$(INFO_LABEL) OpenCL library: [$$(ICD_LIB_FILE_$(1))] → [$(1)] → [$$(CL_LIB_REALPATH_$(1))] → [SONAME: $$(call get_soname,$$(CL_LIB_REALPATH_$(1)))] → [SIZE: $$(shell printf "%'d bytes" $$$$(stat --format=%s $$(CL_LIB_REALPATH_$(1))))]"
endif # }
endif # }
OPENCL_LIB_INFO += echo $$(OPENCL_LIB_RESULT_$(1));
endef
$(foreach icd_lib, $(OCL_LIBS), $(eval $(call check_icd_lib,$(icd_lib))))

################################################################################
# Phony target to check for the existence of OpenCL dynamic libraries.         #
################################################################################
icd_opencl:
	@echo "LIB_ARCHITECTURE                      = [$$(uname -m)]"
	@echo 'OPENCL_ICD_PATH                       = [$(OPENCL_ICD_PATH)]'
	@echo 'OPENCL_LIB_STEM                       = [$(OPENCL_LIB_STEM)]'
	@echo 'OPENCL_LIB_DIR                        = [$(OPENCL_LIB_DIR)]'
	@echo 'OPENCL_LIB_SO                         = [$(OPENCL_LIB_SO)]'
	@echo 'OPENCL_LIB_SO_VERSION                 = [$(OPENCL_LIB_SO_VERSION)]'
	@echo 'OPENCL_LIB_SONAME                     = [$(OPENCL_LIB_SONAME)]'
	@echo 'OPENCL_LIBS                           = [$(OPENCL_LIBS)]'
	@echo 'Khronos Unified C API Headers Include = [$(KHRONOS_OPENCL_C_UNIFIED_HEADERS)/CL]'
	@{ $(OPENCL_LIB_INFO) } | sort | column -s"→" -o"→" -t
ifeq ($(ICD_FILES),) # {
	@$(error $(ERROR_LABEL) No OpenCL ICD file(s) were found in [$(OPENCL_ICD_PATH)])
endif # }
ifeq ($(OCL_LIBS),) # {
	@$(error $(ERROR_LABEL) No OpenCL dynamic libraries were specified by the ICD file(s) [$(ICD_FILES)])
endif # }
ifeq ($(HAVE_OPENCL),0) # {
	@$(error $(ERROR_LABEL) No OpenCL dynamic librares were found)
endif # }
else # } {
icd_opencl:
	@$(error $(ERROR_LABEL) The OpenCL vendors directory [$(OPENCL_ICD_PATH)] does not exist)
endif # }

ifneq ($(PROJECT_ROOT),$(CURDIR)) # {

.PHONY: opencl_enums_clean
ifdef OPENCL_ENUMS_HEADERS # {
opencl_enums_clean:
	@rm -f $(addprefix $(SRC_DIR)/,$(OPENCL_ENUMS_HEADERS:.h=.cpp)) $(addprefix $(INCLUDE_DIR)/,$(OPENCL_ENUMS_HEADERS))
else # } {
opencl_enums_clean:;
endif # }

build_clean:: opencl_enums_clean

################################################################################
# Pattern rule to create OpenCL enum header and implementation files from a    #
# file specifying an OpenCL enum.                                              #
# NOTE: .PRECIOUS tells make to preserve intermediate files instead of         #
#       automatically deleting them.                                           #
################################################################################
OPENCL_ENUMS_SCRIPT=$(realpath $(PROJECT_ROOT)/scripts/clenums.py)
.PRECIOUS: $(INCLUDE_DIR)/%.h $(SRC_DIR)/%.cpp
$(INCLUDE_DIR)/%.h $(SRC_DIR)/%.cpp &: $(OPENCL_ENUMS_DIR)/% $(OPENCL_ENUMS_SCRIPT)
	@echo 'OpenCL $(*) enum PATTERN RULE'
	@$(call target_info,$(INCLUDE_DIR)/$(*).h $(SRC_DIR)/$(*).cpp,$(<),$(?),$(^),$(|),$(*))
	$(OPENCL_ENUMS_SCRIPT) --include $(INCLUDE_DIR) --src $(SRC_DIR) -e $(<)
	@echo ''

################################################################################
# This target builds the OpenCL enums include and implementation files.        #
################################################################################
clenums: $(addprefix $(INCLUDE_DIR)/,$(OPENCL_ENUMS_HEADERS)) $(OPENCL_ENUMS_CPP)
	@$(call target_info,$(@),$(<),$(?),$(^),$(|),$(*))

.PHONY: opencl_mk_help
opencl_mk_help::
	@echo '$(MAKE) opencl → checks for the presence of OpenCL libraries'
ifneq ($(OPENCL_ENUMS_HEADERS),) # {
	@echo '$(MAKE) clenums → creates the OpenCL enums header and implementation files'
endif # }

show_help:: opencl_mk_help

endif # }

endif # }

