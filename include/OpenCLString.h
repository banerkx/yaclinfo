
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>

class OpenCLString
{
  public:

    OpenCLString& operator=(const std::string& sLHS);

    inline operator const std::string&() const
    {
      return (m_sInfo);
    }

    inline operator std::string&()
    {
      return (m_sInfo);
    }

    inline bool empty() const
    {
      return (m_sInfo.empty());
    }

    friend std::string operator+(const std::string& lhs, const OpenCLString& rhs);

    friend std::ostream& operator<<(std::ostream& tStream, const OpenCLString& rhs);

    virtual ~OpenCLString();

  protected:

    explicit OpenCLString(const std::string& sInfo = "");

    OpenCLString(const OpenCLString& rhs);

    OpenCLString(OpenCLString&& rhs);

    std::string m_sInfo;

  private:

    // Intentionally deleted.
    OpenCLString&       operator=(const OpenCLString& rhs) = delete;
    OpenCLString&       operator=(OpenCLString&& rhs) = delete;
    OpenCLString*       operator&()                   = delete;
    const OpenCLString* operator&() const             = delete;
};

class OpenCLError : public OpenCLString
{
  public:

    explicit OpenCLError(const std::string& sInfo = "");

    OpenCLError(const OpenCLError& rhs);

    OpenCLError(OpenCLError&& rhs);

    virtual ~OpenCLError() override;

  private:

    // Intentionally deleted.
    OpenCLError&       operator=(const OpenCLError& rhs) = delete;
    OpenCLError&       operator=(OpenCLError&& rhs) = delete;
    OpenCLError*       operator&()                  = delete;
    const OpenCLError* operator&() const            = delete;
};

class OpenCLToolTip : public OpenCLString
{
  public:

    explicit OpenCLToolTip(const std::string& sInfo = "");

    OpenCLToolTip(const OpenCLToolTip& rhs);

    OpenCLToolTip(OpenCLToolTip&& rhs);

    virtual ~OpenCLToolTip() override;

  private:

    // Intentionally deleted.
    OpenCLToolTip&       operator=(const OpenCLToolTip& rhs) = delete;
    OpenCLToolTip&       operator=(OpenCLToolTip&& rhs) = delete;
    OpenCLToolTip*       operator&()                    = delete;
    const OpenCLToolTip* operator&() const              = delete;
};

class OpenCLUnits : public OpenCLString
{
  public:

    explicit OpenCLUnits(const std::string& sInfo = "");

    OpenCLUnits(const OpenCLUnits& rhs);

    OpenCLUnits(OpenCLUnits&& rhs);

    virtual ~OpenCLUnits() override;

    static const OpenCLUnits m_tBits;
    static const OpenCLUnits m_tBytes;
    static const OpenCLUnits m_tGigaBytes;
    static const OpenCLUnits m_tGigaHertz;
    static const OpenCLUnits m_tKiloBytes;
    static const OpenCLUnits m_tMegaHertz;
    static const OpenCLUnits m_tMicroSeconds;
    static const OpenCLUnits m_tNanoSeconds;
    static const OpenCLUnits m_tSeconds;
    static const OpenCLUnits m_tPages;
    static const OpenCLUnits m_tPixels;

  private:

    friend class OpenCLUnitsLess;

    // Intentionally deleted.
    OpenCLUnits&       operator=(const OpenCLUnits& rhs) = delete;
    OpenCLUnits&       operator=(OpenCLUnits&& rhs) = delete;
    OpenCLUnits*       operator&()                  = delete;
    const OpenCLUnits* operator&() const            = delete;
};

class OpenCLUnitsLess
{
  public:

    OpenCLUnitsLess()
    {
    }

    ~OpenCLUnitsLess()
    {
    }

    bool operator()(const OpenCLUnits& lhs, const OpenCLUnits& rhs) const
    {
      return (lhs.m_sInfo < rhs.m_sInfo);
    }

  private:

    // Intentionally deleted.
    OpenCLUnitsLess(const OpenCLUnitsLess& rhs) = delete;
    OpenCLUnitsLess(OpenCLUnitsLess&& rhs)      = delete;
    OpenCLUnitsLess&       operator=(const OpenCLUnitsLess& rhs) = delete;
    OpenCLUnitsLess&       operator=(OpenCLUnitsLess&& rhs) = delete;
    OpenCLUnitsLess*       operator&()                      = delete;
    const OpenCLUnitsLess* operator&() const                = delete;
};

