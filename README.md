# README glabyaclinfo

OpenCL applications.

## Getting Started
To clone the git repository:
```
git clone --branch main https://gitlab.com/banerkx/yaclinfo.git 
```
### Prerequisites
A functioning OpenCL installation. Additionally, the following software is required:

| Prerequisite                         | Link                                                                                                                            |
| ------------------------------------ | -------------------------------------------                                                                                     |
| Boost version 1.80                   | <a href="https://www.boost.org/users/download/" style="color:blue;" target="_blank">https://www.boost.org/users/download/</a>   |
| GNU C++ compiler                     | <a href="https://gcc.gnu.org" style="color:blue;" target="_blank">https://gcc.gnu.org</a>                                       |

To get full doxygen functionality, please install the following:

| Application  | Link                                                                                                                                                |
| ------------ | -----------------------------------------------                                                                                                     |
| PlantUML     | <a href="https://plantuml.com" style="color:blue;" target="_blank">https://plantuml.com</a>                                                         |
| dvips        | <a href="https://ctan.org/pkg/dvips?lang=en" style="color:blue;" target="_blank">https://ctan.org/pkg/dvips?lang=en</a>                             |
| graphviz/dot | <a href="https://graphviz.gitlab.io" style="color:blue;" target="_blank">https://graphviz.gitlab.io</a>                                             |
| gs           | <a href="https://www.ghostscript.com/download/gsdnld.html" style="color:blue;" target="_blank">https://www.ghostscript.com/download/gsdnld.html</a> |
| latex        | <a href="https://www.latex-project.org/get" style="color:blue;" target="_blank">https://www.latex-project.org/get</a>                               |

| Environment Attribute                          | Value                                                                           |
| ---------------------------------------------  | -----------------------------------                                             |
| /bin/bash version                              | GNU bash, version 5.1.8(1)-release (x86_64-redhat-linux-gnu)                    |
| Boost version                                  | 1.80                                                                            |
| Host Platform Operating System                 | Fedora Linux 35 (Workstation Edition)                                           |
| Khronos Unified OpenCL C API Headers commit    | 8f33fba7c14b926c6551bf86b5b255e3e0f47f86                                        |
| Khronos Unified OpenCL C++ API Headers commit  | 4a1157466afe72a87e8abc59537ef577534ccadf                                        |
| PlantUML version                               | 1.2022.12                                                                       |
| doxygen version                                | 1.9.5                                                                           |
| firefox version                                | 106.0.5                                                                         |
| g++ version                                    | 11.3.1                                                                          |
| git version                                    | 2.38.1                                                                          |
| make version                                   | GNU Make 4.3                                                                    |

Application(s):

| Application     | Readme                           | Description                                   |
| --------------- | -------------------------------- | --------------------------------------------- |
| clinfo          | [README.md](/clinfo/README.md)   | gathers and displays/saves OpenCL information |

### Author
 **K. Banerjee**
### License
Copyright © 2018-2022 K. Banerjee
This project is licensed under the GPL, version 3.0. See:<br>
<a href="https://www.gnu.org/licenses/gpl-3.0.en.html" style="color:blue;" target="_blank">https://www.gnu.org/licenses/gpl-3.0.en.html</a>
<br>
<a href="https://www.gnu.org/licenses/gpl-3.0.txt" style="color:blue;" target="_blank">https://www.gnu.org/licenses/gpl-3.0.txt</a>
