
################################################################################
#  Copyright © 2018-2023 K. Banerjee                                           #
#                                                                              #
#  This file is part of yaclinfo.                                              #
#                                                                              #
#  yaclinfo is free software: you can redistribute it and/or modify            #
#  it under the terms of the GNU General Public License as published by        #
#  the Free Software Foundation, either version 3 of the License, or           #
#  (at your option) any later version.                                         #
#                                                                              #
#  yaclinfo is distributed in the hope that it will be useful,                 #
#  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#  GNU General Public License for more details.                                #
#                                                                              #
#  You should have received a copy of the GNU General Public License           #
#  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.          #
################################################################################

################################################################################
# Defining an include guard.                                                   #
################################################################################
ifndef PLANT_UML_MK_INCLUDE_GUARD # {
PLANT_UML_MK_INCLUDE_GUARD:=1

SHELL=/bin/bash

PROJECT_ROOT ?= $(shell git rev-parse --show-toplevel 2> /dev/null)

################################################################################
# NOTE: Include this makefile after including $(PROJECT_ROOT)/vars_common.mk.  #
################################################################################
ifeq ($(VARS_COMMON_MK_INCLUDE_GUARD),) # {
THIS_MAKE_FILE:=$(realpath $(lastword $(MAKEFILE_LIST)))
$(error $(ERROR_LABEL) Must include [$(PROJECT_ROOT)/vars_common.mk] before including this makefile [$(THIS_MAKE_FILE)])
endif # }

BROWSER ?= firefox
EDITOR ?= vim + 1

PLANT_UML_JAR_NAME=plantuml.jar
PLANT_UML_JAR=$(call find_jar_location,$(PLANT_UML_JAR_NAME))

PLANT_UML_MAIN_CLASS:=$(shell unzip -qp $(PLANT_UML_JAR) META-INF/MANIFEST.MF | grep "Main-Class: " | sed 's/^.* //;s/\r$$//')

PLANT_UML_WEB_SITE:=https://plantuml.com
PLANT_UML_DOWNLOAD:=$(PLANT_UML_WEB_SITE)/download

PLANT_UML_GOALS=plantuml phelp plantuml_help

JAVA_COMMAND=java
JAVA:=$(shell command -v $(JAVA_COMMAND))
ifeq ($(JAVA),) # {
$(error $(ERROR_LABEL) The executable [$(JAVA_COMMAND)] was not found)
endif # }

JAVA_VERSION:=$(shell { $(JAVA) --version 2> /dev/null || echo 'UNKNOWN'; } | head -1 | cut -d" " -f2)
plant_uml_info::
	@echo 'CLASSPATH           = [$(subst ${HOME},$${HOME},$(subst $(escaped_space),:,$(realpath $(subst :, ,${CLASSPATH}))))]'
	@echo 'JAVA_HOME           = [$(realpath ${JAVA_HOME})]'
	@echo 'java                = [$(realpath $(JAVA))]'
	@echo 'java version        = [$(JAVA_VERSION)]'

ifneq ($(PLANT_UML_JAR),UNKNOWN) # {

ifneq ($(PLANT_UML_MAIN_CLASS),) # {

PLANT_UML_VERSION:=$(call get_stdout_java_jar_file_version_index,$(PLANT_UML_JAR),-version,3)
PLANT_UML_LATEST_VERSION:=$(shell curl -s $(PLANT_UML_DOWNLOAD) | sed 's/PlantUML compiled Jar/\nPlantUML compiled Jar/' | grep "^PlantUML compiled Jar" | sed 's/<.*$$//;s/^.*Version //;s/\x29$$//')

.PHONY: plantuml_help
PLANT_UML_HELP_FILE=plantuml_help.txt
phelp plantuml_help:
	@java -jar $(PLANT_UML_JAR) -help >| $(PLANT_UML_HELP_FILE)
	@$(call open_file_command,$(PLANT_UML_HELP_FILE),$(EDITOR),PlantUML help file)

################################################################################
# Creates a target to generate a PlantUML diagram from a PlantUML text file    #
# and a target to check the uml file for syntax errors.                        #
#                                                                              #
# ${1} - file name                                                             #
# $(2) - PlantUML text file prerequisite                                       #
# $(3) - PlantUML command line options (optional)                              #
################################################################################
define cr8_plant_uml_target
$(if $(filter-out 1,$(words $(1))),.PHONY: $(firstword $(1)))
$(1): $(2)
################################################################################
# If the output file extension is "svg", then the "-tsvg" option must be added #
# to the PlantUML command.                                                     #
################################################################################
	@$(eval SVG=$(if $(filter svg,$(lastword $(subst ., ,$(1)))),-tsvg))
################################################################################
# If the PlantUML options in $(3) does not contain "-output", then we          #
# explicitly tell PlantUML to save the output to the current directory.        #
################################################################################
	@$(eval OUT_DIR=$(if $(filter -output,$(3)),,-output $(CURDIR)))
	@echo '[$$(@)] UML_FILE = [$(2)]'
	@rm -f $(1)
##	@$(JAVA_COMMAND) -jar $(PLANT_UML_JAR) -duration $(SVG) $(3) $(OUT_DIR) $(2)
##	@$(JAVA_COMMAND) -cp ${CLASSPATH} $(PLANT_UML_MAIN_CLASS) -duration $(SVG) $(3) $(OUT_DIR) $(2)
	@$(JAVA_COMMAND) $(PLANT_UML_MAIN_CLASS) -duration $(SVG) $(3) $(OUT_DIR) $(2)
	@$$(call open_sole_target_file,$$(@),$(1),$(BROWSER),OpenCL enums diagram)

clean::
	@rm -f $(1)

################################################################################
# If the uml file to be checked is:                                            #
#   abc.uml                                                                    #
# then make:                                                                   #
#   make check_abc                                                             #
#                                                                              #
# If the uml file to be checked is:                                            #
#   umls/xyz.uml                                                               #
# then make:                                                                   #
#   make check_xyz                                                             #
#                                                                              #
################################################################################
.PHONY: check_$(notdir $(subst .uml,,$(2)))
check_$(notdir $(subst .uml,,$(2))):
	@echo '[$$(@)] UML_FILE = [$(2)]'
	@$(JAVA_COMMAND) -jar $(PLANT_UML_JAR) -duration -verbose -checkonly $(2)

endef

################################################################################
# Target to create an svg file with the PlantUML color palette.                #
################################################################################
.PHONY: pcolors
COLORS_UML=colors.uml
COLORS_SVG=colors.svg
pcolors:
	@echo '@startuml colors' >| $(COLORS_UML)
	@echo 'colors'           >> $(COLORS_UML)
	@echo '@enduml'          >> $(COLORS_UML)
	@$(JAVA_COMMAND) -jar $(PLANT_UML_JAR) -duration -tsvg $(COLORS_UML)
	@rm $(COLORS_UML)
	@$(call open_sole_target_file,$(@),$(COLORS_SVG),$(BROWSER),PlantUML color palette)

clean::
	@rm -f $(COLORS_UML) $(COLORS_SVG)

################################################################################
# Creates a PlantUML color chart.                                              #
#                                                                              #
# $(1) - PlantUML color                                                        #
################################################################################
PLANT_UML_COLORS:=$(shell $(JAVA_COMMAND) -jar $(PLANT_UML_JAR) -language | sed '1,/^;color/d' | sed '/^;/d' | sed -n '/^$$/q;p')
define puml_color_chart
pcolor_$(1): COLOR_UML=$(1).uml
pcolor_$(1):
	@echo '@startuml $(1)' >| $$(COLOR_UML)
	@echo 'colors $(1)'    >> $$(COLOR_UML)
	@echo '@enduml'        >> $$(COLOR_UML)
	@$(JAVA_COMMAND) -jar $(PLANT_UML_JAR) -duration -tsvg $$(COLOR_UML)
	@rm $$(COLOR_UML)
	@$$(call open_sole_target_file,$$(@),$(1).svg,$(BROWSER),PlantUML color palette)

clean::
	@rm -f $(1).uml $(1).svg
endef
$(foreach color,$(PLANT_UML_COLORS),$(eval $(call puml_color_chart,$(color))))

################################################################################
# Pattern rule to ensure that only valid PlantUML colors are used for the      #
# pcolor targets.                                                              #
################################################################################
.PHONY: pcolor_%
pcolor_%:
	@$(error $(ERROR_LABEL) [$(*)] is not a valid PlantUML color)

plant_uml_info::
	@echo 'plantuml jar file   = [$(subst ${HOME},$${HOME},$(PLANT_UML_JAR))]'
	@echo 'plantuml version    = [$(PLANT_UML_VERSION)]'
	@echo 'plantuml main class = [$(PLANT_UML_MAIN_CLASS)]'

clean::
	@rm -f $(ICONIC_UML) $(ICONIC_SVG) $(PLANT_UML_HELP_FILE)

.PHONY: plant_uml_mk_help
plant_uml_mk_help::
	@echo '$(MAKE) plantuml_help → displays PlantUML help'
	@echo '$(MAKE) plant_uml_info → displays the values of some relevant variables'

show_help:: plant_uml_mk_help

show_updates::
	@$(call check_updates,$(PLANT_UML_JAR_NAME),$(PLANT_UML_DOWNLOAD),$(PLANT_UML_VERSION),$(PLANT_UML_LATEST_VERSION))

else # } {

$(PLANT_UML_GOALS):
	@$(error $(ERROR_LABEL) Could not determine the java main class from PlantUML jar file [$(PLANT_UML_JAR)])

endif # }

else # } {

$(PLANT_UML_GOALS):
	@$(error $(ERROR_LABEL) The PlantUML jar file [$(PLANT_UML_JAR_NAME)] could not be found; download PlantUML from [$(PLANT_UML_DOWNLOAD)] and/or fix the CLASSPATH environment variable)

endif # }

platform_info:: plant_uml_info

endif # }

