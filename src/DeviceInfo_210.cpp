
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo_210.h"

#include "cl_command_queue_properties.h"

DeviceInfo_210::DeviceInfo_210(
  const cl_device_id nDeviceID,
  const size_t       nDeviceNumber,
  cl_platform_id     tPlatformID,
  const size_t       nPlatformNumber) noexcept :
  DeviceInfo_200(nDeviceID, nDeviceNumber, tPlatformID, nPlatformNumber, 210),
  m_tILVersions(
    OPEN_cl_device_info::OPENCL_DEVICE_IL_VERSION,
    "IL VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Intermediate languages that can be "
                  "supported by clCreateProgramWithIL()")),
  m_tMaxNumSubGroups(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_NUM_SUB_GROUPS,
    "MAX NUM SUB GROUPS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max work-group sub-groups that a device can execute "
                  "on single compute unit")),
  m_tSubgroupIndependentForwardProgress(
    OPEN_cl_device_info::OPENCL_DEVICE_SUB_GROUP_INDEPENDENT_FORWARD_PROGRESS,
    "SUBGROUP INDEPENDENT FORWARD PROGRESS",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Supports independent forward progress of sub-groups?"))

{
}

DeviceInfo_210::~DeviceInfo_210() noexcept
{
}

void DeviceInfo_210::getAllDeviceInfo() noexcept
{
  DeviceInfo_200::getAllDeviceInfo();

  getDeviceInfo(m_tILVersions);
  getDeviceInfo(m_tMaxNumSubGroups);
  getDeviceInfo(m_tSubgroupIndependentForwardProgress);
}

void DeviceInfo_210::getTextOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo_200::getTextOutput(tStream);

  const std::string        sHorizontalIndent(static_cast<size_t>(m_nMaxDescriptionSize) + 4, ' ');
  std::vector<std::string> tInfo;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tILVersions)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tILVersions).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tILVersions))
    {
      if ('\0' != std::get<PARAM_VALUE>(m_tILVersions)[0])
      {
        std::string sILVersions(std::get<PARAM_VALUE>(m_tILVersions).get());
        DeviceInfo::extractTokens(tInfo, sILVersions, ';');
        if (false == tInfo.empty())
        {
          DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
        }
      }
      else
      {
        tStream << "No intermediate language versions found.";
      }
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tILVersions);
  }
  tStream << std::endl;

  tStream << m_tMaxNumSubGroups << std::endl;
  tStream << m_tSubgroupIndependentForwardProgress << std::endl;
}

void DeviceInfo_210::getHTMLOutput(std::ostream& tStream) const noexcept
{
  DeviceInfo_200::getHTMLOutput(tStream);

  DeviceInfo::writeHTMLTableRow(
    tStream,
    m_tILVersions,
    "No intermediate language versions found.",
    ';');

  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxNumSubGroups);
  DeviceInfo::writeHTMLTableRow(tStream, m_tSubgroupIndependentForwardProgress);
}

