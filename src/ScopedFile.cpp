
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ScopedFile.h"

#include <unistd.h>

#include <sstream>

ScopedInputFile::ScopedInputFile(const std::string& sFileName) :
  std::ifstream(sFileName.c_str(), ios_base::in), m_sFileName(sFileName)
{
}

ScopedInputFile::~ScopedInputFile()
{
  close();
}

ScopedOutputFile::ScopedOutputFile(
  const std::string&            sFileName,
  const std::ios_base::openmode nOpenMode) :
  std::ofstream(), m_sFileName(sFileName), m_nOpenMode(nOpenMode | std::ios_base::out)
{
  if (true == m_sFileName.empty())
  {
    std::stringstream tError;
    tError << "ERROR: The file name is empty.";
    throw std::runtime_error(tError.str());
  }

  if (("/dev/null" != m_sFileName) && (0 == access(m_sFileName.c_str(), F_OK)))
  {
    std::stringstream tError;
    tError << "ERROR: The file [" << m_sFileName << "] already exists. It will not be overwritten.";
    throw std::runtime_error(tError.str());
  }

  if (std::ios::in & m_nOpenMode)
  {
    std::stringstream tError;
    tError << "ERROR: The file [" << m_sFileName << "] must not be used for input.";
    throw std::runtime_error(tError.str());
  }

  std::ofstream::open(m_sFileName.c_str(), m_nOpenMode);
  if (false == std::ofstream::is_open())
  {
    std::stringstream tError;
    tError << "ERROR: Unable to open file [" << m_sFileName << "].";
    throw std::runtime_error(tError.str());
  }
}

ScopedOutputFile::~ScopedOutputFile()
{
  std::ofstream::close();
}

ScopedBinaryOutputFile::ScopedBinaryOutputFile(
  const std::string&            sFileName,
  const std::ios_base::openmode nOpenMode) :
  ScopedOutputFile(sFileName, nOpenMode | std::ios::binary)
{
}

ScopedBinaryOutputFile::~ScopedBinaryOutputFile()
{
}

ScopedFileDescriptor::ScopedFileDescriptor(const int nFileDescriptor) :
  m_nFileDescriptor(nFileDescriptor)
{
}

ScopedFileDescriptor& ScopedFileDescriptor::operator=(const int nFD)
{
  m_nFileDescriptor = nFD;
  return (*this);
}

ScopedFileDescriptor::~ScopedFileDescriptor()
{
  // Will not close stdin, stdout, and stderr.
  if (m_nFileDescriptor > 2)
  {
    (void) close(m_nFileDescriptor);
  }
}

ScopedFileDescriptor::operator int() const
{
  return (m_nFileDescriptor);
}

bool ScopedFileDescriptor::operator==(const int rhs) const
{
  return (m_nFileDescriptor == rhs);
}

bool operator==(const int lhs, const ScopedFileDescriptor& rhs)
{
  return (lhs == rhs.m_nFileDescriptor);
}

bool ScopedFileDescriptor::operator!=(const int rhs) const
{
  return (m_nFileDescriptor != rhs);
}

bool operator!=(const int lhs, const ScopedFileDescriptor& rhs)
{
  return (lhs != rhs.m_nFileDescriptor);
}

ScopedFILE::ScopedFILE(FILE* pFile) : m_pFile(pFile)
{
}

ScopedFILE::~ScopedFILE()
{
  if (nullptr != m_pFile)
  {
    (void) std::fclose(m_pFile);
  }
}

ScopedFILE::operator FILE*() const
{
  return (m_pFile);
}

