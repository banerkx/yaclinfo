
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include "PlatformInfo_200.h"

// NOTE: Ensure that is the last #define for CL_TARGET_OPENCL_VERSION.
#ifdef CL_TARGET_OPENCL_VERSION
#undef CL_TARGET_OPENCL_VERSION
#endif
#define CL_TARGET_OPENCL_VERSION 210
#include <CL/opencl.h>

class PlatformInfo_210 : public PlatformInfo_200
{
  public:

    PlatformInfo_210(
      cl_platform_id       tPlatformID,
      const size_t         nPlatformNumber,
      const unsigned short nPlatformVersion = 210) noexcept;

    virtual ~PlatformInfo_210() noexcept override;

  private:

    virtual void populateDeviceTypes() override;

    virtual void populatePlatformInfoTypes() override;

    virtual void getPlatformAttributes() override;

    // Intentionally deleted.
    PlatformInfo_210()                            = delete;
    PlatformInfo_210(const PlatformInfo_210& rhs) = delete;
    PlatformInfo_210(PlatformInfo_210&& rhs)      = delete;
    PlatformInfo_210&       operator=(const PlatformInfo_210& rhs) = delete;
    PlatformInfo_210&       operator=(PlatformInfo_210&& rhs) = delete;
    PlatformInfo_210*       operator&()                       = delete;
    const PlatformInfo_210* operator&() const                 = delete;
};

