
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StringToPrimitive.h"

#include <algorithm>
#include <boost/lexical_cast.hpp>
#include <cstring>
#include <stdexcept>
#include <string>

#include "GetTypeName.h"

template<class T> T stringToPrimitive(const std::string& sPrimitive, bool& bSuccess) noexcept
{
  try
  {
    bSuccess = true;
    return (boost::lexical_cast<T>(sPrimitive));
  }
  catch (const boost::bad_lexical_cast&)
  {
    bSuccess = false;
  }
  return (T());
}

template<> bool stringToPrimitive<bool>(const std::string& sPrimitive, bool& bSuccess) noexcept
{
  std::string sTemp(sPrimitive);
  bSuccess = true;
  std::transform(sTemp.begin(), sTemp.end(), sTemp.begin(), ::tolower);

  if (("true" == sTemp) || ("t" == sTemp) || ("1" == sTemp) || ("on" == sTemp) || ("yes" == sTemp))
  {
    return (true);
  }

  if (("false" == sTemp) || ("f" == sTemp) || ("0" == sTemp) || ("off" == sTemp) || ("no" == sTemp))
  {
    return (false);
  }

  bSuccess = false;
  return (bool());
}

template<>
std::string stringToPrimitive<std::string>(const std::string& sPrimitive, bool& bSuccess) noexcept
{
  bSuccess = true;
  return (sPrimitive);
}

template<class T> void throwRuntimeError(const std::string& sPrimitive)
{
  std::stringstream tError;
  tError << "Could not convert [" << sPrimitive << "] to type [" << getTypeName<T>() << "].";
  throw std::runtime_error(tError.str());
}

// Explicit instantiations.
template double stringToPrimitive<double>(const std::string& sPrimitive, bool& bSuccess) noexcept;

template unsigned short stringToPrimitive<unsigned short>(
  const std::string& sPrimitive,
  bool&              bSuccess) noexcept;

template int stringToPrimitive<int>(const std::string& sPrimitive, bool& bSuccess) noexcept;

