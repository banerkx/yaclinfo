
/**
 * \file
 * \copyright
 *  Copyright © 2018-2023 K. Banerjee
 *
 *  This file is part of yaclinfo.
 *
 *  yaclinfo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  yaclinfo is distributed in the hope that it will be useful,
 *  but **WITHOUT ANY WARRANTY**; without even the implied warranty of
 *  **MERCHANTABILITY** or **FITNESS FOR A PARTICULAR PURPOSE**.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with yaclinfo.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DeviceInfo.h"

#include <cstring>
#include <iomanip>
#include <regex>
#include <type_traits>

#include "OpenCLHTMLUtilities.h"
#include "OpenCLSimpleKernel.h"
#include "StringTokenizer.h"
#include "cl_command_queue_properties.h"
#include "cl_device_affinity_domain.h"
#include "cl_device_exec_capabilities.h"
#include "cl_device_fp_config.h"
#include "cl_device_local_mem_type.h"
#include "cl_device_mem_cache_type.h"
#include "cl_device_partition_property.h"
#include "cl_device_svm_capabilities.h"
#include "cl_device_type.h"

static int MAX_DESCRIPTION_WIDTH = 0;

template<class T> constexpr bool is_actual_integer()
{
  if (
    (true == std::is_same<T, short>::value) || (true == std::is_same<T, int>::value) ||
    (true == std::is_same<T, long>::value) || (true == std::is_same<T, long long>::value) ||
    (true == std::is_same<T, unsigned short>::value) ||
    (true == std::is_same<T, unsigned int>::value) ||
    (true == std::is_same<T, unsigned long>::value) ||
    (true == std::is_same<T, unsigned long long>::value) ||
    (true == std::is_same<T, const short>::value) || (true == std::is_same<T, const int>::value) ||
    (true == std::is_same<T, const long>::value) ||
    (true == std::is_same<T, const long long>::value) ||
    (true == std::is_same<T, const unsigned short>::value) ||
    (true == std::is_same<T, const unsigned int>::value) ||
    (true == std::is_same<T, const unsigned long>::value) ||
    (true == std::is_same<T, const unsigned long long>::value))
  {
    return (true);
  }

  // NOTE: std::is_integral<T>() returns true for the following types:
  //         bool
  //         char
  //         char16_t
  //         char32_t
  //         wchar_t
  //         signed char
  //       We need to exclude the preceding types.
  return (false);
}

template<class CL_DEVICE_INFO, class T> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    tStream << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO, class T> std::ostream& operator<<(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip, OpenCLUnits>&
      tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    tStream << std::get<PARAM_VALUE>(tDeviceInfo) << std::get<PARAM_UNITS>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<> std::ostream& operator<<<OPEN_cl_device_info, cl_uint>(
  std::ostream& tStream,
  const std::
    tuple<const OPEN_cl_device_info, std::string, size_t, cl_uint, OpenCLError, OpenCLToolTip>&
      tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<
    const CL_DEVICE_INFO,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>& tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo) << std::get<PARAM_UNITS>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, cl_ulong, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO> std::ostream& operator<<(
  std::ostream& tStream,
  const std::tuple<
    const CL_DEVICE_INFO,
    std::string,
    size_t,
    cl_ulong,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>& tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo) << std::get<PARAM_UNITS>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<> std::ostream& operator<<<OPEN_cl_device_info, cl_ulong>(
  std::ostream& tStream,
  const std::
    tuple<const OPEN_cl_device_info, std::string, size_t, cl_ulong, OpenCLError, OpenCLToolTip>&
      tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<> std::ostream& operator<<<OPEN_cl_device_info, cl_ulong>(
  std::ostream& tStream,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    cl_ulong,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>& tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo) << std::get<PARAM_UNITS>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

template<class CL_DEVICE_INFO> std::ostream& operator<<(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, cl_platform_id, OpenCLError, OpenCLToolTip>&
      tDeviceInfo) noexcept
{
  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(tDeviceInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << std::get<PARAM_VALUE>(tDeviceInfo);
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tDeviceInfo);
  }
  return (tStream);
}

ScopedNumericFormatter::ScopedNumericFormatter(std::ostream& tStream) :
  m_tStream(tStream), m_tCurrentFormat(nullptr)
{
  m_tCurrentFormat.copyfmt(m_tStream);
  m_tStream.imbue(std::locale(""));
}

ScopedNumericFormatter::~ScopedNumericFormatter()
{
  restore();
}

template<class T> ScopedNumericFormatter& ScopedNumericFormatter::operator<<(const T& rhs)
{
  m_tStream << rhs;
  return (*this);
}

ScopedNumericFormatter& ScopedNumericFormatter::operator<<(const cl_platform_id& rhs)
{
  std::ofstream tCurrentFormat;
  tCurrentFormat.copyfmt(m_tStream);
  restore();
  m_tStream << rhs;
  m_tStream.copyfmt(tCurrentFormat);
  return (*this);
}

void ScopedNumericFormatter::restore()
{
  m_tStream.copyfmt(m_tCurrentFormat);
}

ScopedOutputStream::ScopedOutputStream(std::ostream& tStream) noexcept :
  m_tStream(tStream), m_nFieldWidth(tStream.width())
{
}

ScopedOutputStream::~ScopedOutputStream() noexcept
{
  m_tStream.width(m_nFieldWidth);
}

const std::string DeviceInfo::m_sHTMLCrossProduct = " &#10799; ";

DeviceInfo::DeviceInfo(
  const cl_device_id   nDeviceID,
  const size_t         nDeviceNumber,
  cl_platform_id       tPlatformID,
  const size_t         nPlatformNumber,
  const unsigned short nDeviceVersion) noexcept :
  m_nDeviceID(nDeviceID),
  m_nMaxDescriptionSize(0),
  m_nDeviceNumber(nDeviceNumber),
  m_tPlatformID(tPlatformID),
  m_nPlatformNumber(nPlatformNumber),
  m_nDeviceVersion(nDeviceVersion),
  m_tKernelErrors(),
  m_sKernel("UNKNOWN"),
  m_sKernelDeviceType("UNKNOWN"),
  m_nKernelDataSize(0),
  m_nKernelElapsedTime(0),
  m_tAddressBits(
    OPEN_cl_device_info::OPENCL_DEVICE_ADDRESS_BITS,
    "ADDRESS SPACE SIZE",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Default compute device address space size"),
    OpenCLUnits::m_tBits),
  m_tAvailable(
    OPEN_cl_device_info::OPENCL_DEVICE_AVAILABLE,
    "DEVICE IS AVAILABLE",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is device available?")),
  m_tCompilerAvailable(
    OPEN_cl_device_info::OPENCL_DEVICE_COMPILER_AVAILABLE,
    "COMPILER IS AVAILABLE",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Does implementation have a compiler available?")),
  m_tDoublePrecisionFloatingPointConfiguration(
    OPEN_cl_device_info::OPENCL_DEVICE_DOUBLE_FP_CONFIG,
    "DOUBLE PRECISION FLOATING POINT CONFIGURATION",
    sizeof(cl_device_fp_config),
    cl_device_fp_config(0),
    OpenCLError(""),
    OpenCLToolTip("Double precision capability")),
  m_tIsLittleEndian(
    OPEN_cl_device_info::OPENCL_DEVICE_ENDIAN_LITTLE,
    "IS LITTLE ENDIAN",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Is device little endian?")),
  m_tHasErrorCorrectionSupport(
    OPEN_cl_device_info::OPENCL_DEVICE_ERROR_CORRECTION_SUPPORT,
    "HAS ERROR CORRECTION SUPPORT",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Does device support error correction for device memory?")),
  m_tExecutionCapabilities(
    OPEN_cl_device_info::OPENCL_DEVICE_EXECUTION_CAPABILITIES,
    "EXECUTION CAPABILITIES",
    sizeof(cl_device_exec_capabilities),
    cl_device_exec_capabilities(0),
    OpenCLError(""),
    OpenCLToolTip("Execution capabilities")),
  m_tDeviceExtensions(
    OPEN_cl_device_info::OPENCL_DEVICE_EXTENSIONS,
    "DEVICE EXTENSIONS",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Device extensions")),
  m_tGlobalMemoryCacheSize(
    OPEN_cl_device_info::OPENCL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
    "GLOBAL MEMORY CACHE SIZE",
    sizeof(cl_ulong),
    cl_ulong(0),
    OpenCLError(""),
    OpenCLToolTip("Global memory cache size"),
    OpenCLUnits::m_tBytes),
  m_tGlobalMemoryCacheType(
    OPEN_cl_device_info::OPENCL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
    "GLOBAL MEMORY CACHE TYPE",
    sizeof(cl_device_mem_cache_type),
    cl_device_mem_cache_type(0),
    OpenCLError(""),
    OpenCLToolTip("Type of global memory cache")),
  m_tGlobalMemoryCachelineSize(
    OPEN_cl_device_info::OPENCL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
    "GLOBAL MEMORY CACHE LINE SIZE",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Global memory cache line size"),
    OpenCLUnits::m_tBytes),
  m_tGlobalMemorySize(
    OPEN_cl_device_info::OPENCL_DEVICE_GLOBAL_MEM_SIZE,
    "GLOBAL MEMORY SIZE",
    sizeof(cl_ulong),
    cl_ulong(0),
    OpenCLError(""),
    OpenCLToolTip("Global device memory"),
    OpenCLUnits::m_tBytes),
  m_tImageSupport(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE_SUPPORT,
    "HAS IMAGE SUPPORT",
    sizeof(OPEN_cl_bool),
    OPEN_cl_bool::OPENCL_FALSE,
    OpenCLError(""),
    OpenCLToolTip("Are images are supported?")),
  m_tMax2DImageHeight(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE2D_MAX_HEIGHT,
    "MAX 2D IMAGE HEIGHT",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max height of 2D image"),
    OpenCLUnits::m_tPixels),
  m_tMax2DImageWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE2D_MAX_WIDTH,
    "MAX 2D IMAGE WIDTH",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max 2D image or 1D image not created from a buffer width"),
    OpenCLUnits::m_tPixels),
  m_tMax3DImageDepth(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE3D_MAX_DEPTH,
    "MAX 3D IMAGE DEPTH",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max 3D image depth"),
    OpenCLUnits::m_tPixels),
  m_tMax3DImageHeight(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE3D_MAX_HEIGHT,
    "MAX 3D IMAGE HEIGHT",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max 3D image height"),
    OpenCLUnits::m_tPixels),
  m_tMax3DImageWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_IMAGE3D_MAX_WIDTH,
    "MAX 3D IMAGE WIDTH",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max 3D image width"),
    OpenCLUnits::m_tPixels),
  m_tLocalMemoryAreaSize(
    OPEN_cl_device_info::OPENCL_DEVICE_LOCAL_MEM_SIZE,
    "LOCAL MEMORY AREA SIZE",
    sizeof(cl_ulong),
    cl_ulong(0),
    OpenCLError(""),
    OpenCLToolTip("Size of local memory arena"),
    OpenCLUnits::m_tBytes),
  m_tTypeOfLocalMemorySupported(
    OPEN_cl_device_info::OPENCL_DEVICE_LOCAL_MEM_TYPE,
    "TYPE OF LOCAL MEMORY SUPPORTED",
    sizeof(cl_device_local_mem_type),
    cl_device_local_mem_type(0),
    OpenCLError(""),
    OpenCLToolTip("Local memory type")),
  m_tMaxConfiguredClockFrequency(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_CLOCK_FREQUENCY,
    "MAX CLOCK FREQUENCY",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max configured clock frequency"),
    OpenCLUnits::m_tMegaHertz),
  m_tMaxParallelComputeUnits(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_COMPUTE_UNITS,
    "MAX PARALLEL COMPUTE UNITS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max parallel compute units")),
  m_tMaxConstantArgs(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_CONSTANT_ARGS,
    "MAX CONSTANT ARGUMENTS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of arguments declared with "
                  "the __constant qualifier")),
  m_tMaxConstantBufferSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
    "MAX CONSTANT BUFFER SIZE",
    sizeof(cl_ulong),
    cl_ulong(0),
    OpenCLError(""),
    OpenCLToolTip("Max size constant buffer"),
    OpenCLUnits::m_tBytes),
  m_tMaxMemoryObjectAllocation(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_MEM_ALLOC_SIZE,
    "MAX MEMORY OBJECT ALLOCATION SIZE",
    sizeof(cl_ulong),
    cl_ulong(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of memory object allocation"),
    OpenCLUnits::m_tBytes),
  m_tMaxParameterSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_PARAMETER_SIZE,
    "MAX KERNEL ARGUMENT SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max size of the arguments that can be passed to a kernel"),
    OpenCLUnits::m_tBytes),
  m_tMaxSimultaneousImageObjectsRead(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_READ_IMAGE_ARGS,
    "MAX NUMBER SIMULTANEOUS OBJECTS READ",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of simultaneous image objects that can be "
                  "read by a kernel")),
  m_tMaxSamplers(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_SAMPLERS,
    "MAX KERNEL SAMPLERS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of samplers that can be used in a kernel")),
  m_tMaxWorkGroupSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_WORK_GROUP_SIZE,
    "MAX WOKRGROUP SIZE",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Max number of work-items in a work-group executing a "
                  "kernel on a single compute unit")),
  m_tMaxWorkItemDimensions(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
    "MAX WORK ITEM DIMENSIONS",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Max dimensions that specify the global and local "
                  "work-item IDs")),
  m_tMaxWorkItemSizes(
    OPEN_cl_device_info::OPENCL_DEVICE_MAX_WORK_ITEM_SIZES,
    "MAX WORK ITEM SIZES",
    0,
    std::unique_ptr<size_t[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Max number of work-items that can be specified in "
                  "each dimension of the work-group")),
  m_tLargestOpenCLDataTypeSize(
    OPEN_cl_device_info::OPENCL_DEVICE_MEM_BASE_ADDR_ALIGN,
    "LARGEST OpenCL BUILT-IN DATA TYPE SIZE",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Min value of the size of the largest OpenCL built-in data type"),
    OpenCLUnits::m_tBits),
  m_tDeviceName(
    OPEN_cl_device_info::OPENCL_DEVICE_NAME,
    "DEVICE NAME",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Device name")),
  m_tDevicePlatform(
    OPEN_cl_device_info::OPENCL_DEVICE_PLATFORM,
    "DEVICE PLATFORM",
    sizeof(cl_platform_id),
    cl_platform_id(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Platform associated with this device")),
  m_tPreferredCharVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
    "CHAR PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in char type "
                  "that can be put into vectors")),
  m_tPreferredShortVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
    "SHORT PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in short type "
                  "that can be put into vectors")),
  m_tPreferredIntVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
    "INT PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in int type "
                  "that can be put into vectors")),
  m_tPreferredLongVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
    "LONG PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in long type "
                  "that can be put into vectors")),
  m_tPreferredFloatVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
    "FLOAT PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in float type "
                  "that can be put into vectors")),
  m_tPreferredDoubleVectorWidth(
    OPEN_cl_device_info::OPENCL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
    "DOUBLE PREFERRED VECTOR WIDTH",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Preferred native vector width for built-in double "
                  "type that can be put into vectors")),
  m_tDeviceProfile(
    OPEN_cl_device_info::OPENCL_DEVICE_PROFILE,
    "DEVICE PROFILE",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Profile string")),
  m_tProfilingTimerResolution(
    OPEN_cl_device_info::OPENCL_DEVICE_PROFILING_TIMER_RESOLUTION,
    "PREFERRED TIMER RESOLUTION",
    sizeof(size_t),
    size_t(0),
    OpenCLError(""),
    OpenCLToolTip("Resolution of device timer"),
    OpenCLUnits::m_tNanoSeconds),
  m_tSinglePrecisionFloatingPointCapability(
    OPEN_cl_device_info::OPENCL_DEVICE_SINGLE_FP_CONFIG,
    "SINGLE PRECISION FLOATING POINT CAPABILITY",
    sizeof(cl_device_fp_config),
    cl_device_fp_config(0),
    OpenCLError(""),
    OpenCLToolTip("Single precision floating-point capability")),
  m_tDeviceType(
    OPEN_cl_device_info::OPENCL_DEVICE_TYPE,
    "DEVICE TYPE",
    sizeof(cl_device_type),
    cl_device_type(0),
    OpenCLError(""),
    OpenCLToolTip("Device type")),
  m_tDeviceVendor(
    OPEN_cl_device_info::OPENCL_DEVICE_VENDOR,
    "DEVICE VENDOR",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Vendor name")),
  m_tDeviceVendorID(
    OPEN_cl_device_info::OPENCL_DEVICE_VENDOR_ID,
    "DEVICE VENDOR ID",
    sizeof(cl_uint),
    cl_uint(0),
    OpenCLError(""),
    OpenCLToolTip("Unique device vendor identifier")),
  m_tDeviceVersion(
    OPEN_cl_device_info::OPENCL_DEVICE_VERSION,
    "DEVICE VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("OpenCL version")),
  m_tDriverVersion(
    OPEN_cl_device_info::OPENCL_DRIVER_VERSION,
    "DRIVER VERSION",
    0,
    std::unique_ptr<char[]>(nullptr),
    OpenCLError(""),
    OpenCLToolTip("Software driver version"))
{
}

void DeviceInfo::init()
{
  getAllDeviceInfo();
  initKernel();
}

void DeviceInfo::initKernel()
{
  OpenCLSimpleKernel tOpenCLSimpleKernel(
    m_tPlatformID,
    m_nPlatformNumber,
    m_nDeviceID,
    m_nDeviceNumber,
    std::get<PARAM_VALUE>(m_tDeviceType));
  m_sKernel            = tOpenCLSimpleKernel.kernel();
  m_tKernelErrors      = tOpenCLSimpleKernel.errors();
  m_sKernelDeviceType  = tOpenCLSimpleKernel.deviceType();
  m_nKernelDataSize    = tOpenCLSimpleKernel.dataSize();
  m_nKernelElapsedTime = tOpenCLSimpleKernel.elapsedTime();
}

DeviceInfo::~DeviceInfo() noexcept
{
}

void DeviceInfo::extractTokens(
  std::vector<std::string>& tTokens,
  const std::string&        sString,
  const char                cDelimiter) noexcept
{
  tTokens.clear();
  boost::char_separator<char>                   tSeparator(std::string(1, cDelimiter).c_str());
  boost::tokenizer<boost::char_separator<char>> tTokenizer(sString, tSeparator);
  for (auto i = tTokenizer.begin(); i != tTokenizer.end(); ++i)
  {
    tTokens.push_back(*i);
  }
  std::sort(tTokens.begin(), tTokens.end());
}

template<class T>
void DeviceInfo::printVectorHorizontally(std::ostream& tStream, std::vector<T>& tVector) noexcept
{
  if (false == tVector.empty())
  {
    tStream << "[";
    std::copy(tVector.begin(), tVector.end() - 1, std::ostream_iterator<T>(tStream, ", "));
    tStream << tVector.back() << "]" << std::endl;
    tVector.clear();
  }
}

template<> void DeviceInfo::printVectorHorizontally(
  std::ostream&        tStream,
  std::vector<size_t>& tVector) noexcept
{
  if (false == tVector.empty())
  {
    ScopedNumericFormatter tFormatter(tStream);
    tFormatter << "[";
    std::copy(
      tVector.begin(),
      tVector.end() - 1,
      std::ostream_iterator<size_t>(tStream, DeviceInfo::m_sCrossProduct));
    tStream << tVector.back() << "]" << std::endl;
    tVector.clear();
  }
}

template<class T> void DeviceInfo::printVectorVertically(
  std::ostream&                           tStream,
  typename std::vector<T>::const_iterator pBegin,
  typename std::vector<T>::const_iterator pEnd,
  std::vector<T>&                         tVector,
  const std::string&                      sIndent) noexcept
{
  if (pBegin != pEnd)
  {
    tStream << sIndent;
    const std::string sDelimiter("\n" + sIndent);
    std::copy(pBegin, pEnd - 1, std::ostream_iterator<T>(tStream, sDelimiter.c_str()));
    tStream << tVector.back() << std::endl;
  }
  tVector.clear();
}

template<class T> void DeviceInfo::printVectorVertically(
  std::ostream&      tStream,
  std::vector<T>&    tVector,
  const std::string& sIndent) noexcept
{
  tStream << tVector[0] << '\n';
  if (1 == tVector.size())
  {
    tVector.clear();
    return;
  }
  DeviceInfo::printVectorVertically(tStream, tVector.begin() + 1, tVector.end(), tVector, sIndent);
}

std::ostream& operator<<(std::ostream& tStream, const DeviceInfo& rhs) noexcept
{
  rhs.getTextOutput(tStream);
  return (tStream);
}

void DeviceInfo::getTextKernel(
  std::ostream&             tStream,
  const std::string&        sHorizontalIndent,
  std::vector<std::string>& tInfo) const noexcept
{
  std::vector<std::string> tLabels;
  tLabels.push_back("OpenCL Kernel");
  tLabels.push_back("OpenCL Kernel Device Type");
  tLabels.push_back("OpenCL Kernel Errors");
  tLabels.push_back("OpenCL Kernel Data Size");
  tLabels.push_back("OpenCL Kernel Elapsed Time");

  for (const auto& sLabel : tLabels)
  {
    m_nMaxDescriptionSize = std::max(m_nMaxDescriptionSize, static_cast<int>(sLabel.size()));
  }
  MAX_DESCRIPTION_WIDTH = m_nMaxDescriptionSize;

  StringTokenizer tKernelTokens(m_sKernel, '\n');
  tInfo.assign(tKernelTokens.begin(), tKernelTokens.end());
  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << tLabels[0] << ": ";
  DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << tLabels[1] << ": ";
  tStream << m_sKernelDeviceType << '\n';

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << tLabels[2] << ": ";
  if (true == m_tKernelErrors.empty())
  {
    tStream << "None" << std::endl;
  }
  else
  {
    tInfo.assign(m_tKernelErrors.begin(), m_tKernelErrors.end());
    DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << tLabels[3] << ": ";
  ScopedNumericFormatter(tStream) << m_nKernelDataSize << '\n';

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << tLabels[4] << ": ";
  ScopedNumericFormatter(tStream) << m_nKernelElapsedTime << OpenCLUnits::m_tNanoSeconds << '\n';
}

void DeviceInfo::getTextOutput(std::ostream& tStream) const noexcept
{
  ScopedOutputStream tScopedStream(tStream);
  MAX_DESCRIPTION_WIDTH = m_nMaxDescriptionSize;
  const std::string        sHorizontalIndent(static_cast<size_t>(m_nMaxDescriptionSize) + 4, ' ');
  std::vector<std::string> tInfo;

  getTextKernel(tStream, sHorizontalIndent, tInfo);

  tStream << m_tAddressBits << std::endl;
  tStream << m_tAvailable << std::endl;

  tStream << m_tCompilerAvailable << std::endl;

  DeviceInfo::writePrecisionInfo(
    tStream,
    m_nMaxDescriptionSize,
    tInfo,
    sIndent2,
    sHorizontalIndent,
    "No Double Precision Floating Point Capability found.",
    m_tDoublePrecisionFloatingPointConfiguration);

  tStream << m_tIsLittleEndian << std::endl;
  tStream << m_tHasErrorCorrectionSupport << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tExecutionCapabilities) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tExecutionCapabilities).empty())
  {
    for (auto i = OPEN_cl_device_exec_capabilities::BEGIN;
         i != OPEN_cl_device_exec_capabilities::END;
         ++i)
    {
      if (std::get<PARAM_VALUE>(m_tExecutionCapabilities) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorHorizontally(tStream, tInfo);
    }
    else
    {
      tStream << "No Execution Capabilities found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tExecutionCapabilities);
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tDeviceExtensions) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceExtensions).empty())
  {
    std::string sDeviceExtensions(std::get<PARAM_VALUE>(m_tDeviceExtensions).get());
    DeviceInfo::extractTokens(tInfo, sDeviceExtensions);
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << "No Device Extensions found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceExtensions) << '\n';
  }

  tStream << m_tGlobalMemoryCacheSize << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tGlobalMemoryCacheType) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tGlobalMemoryCacheType).empty())
  {
    tStream << cl_device_mem_cache_type_ToString(std::get<PARAM_VALUE>(m_tGlobalMemoryCacheType));
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tGlobalMemoryCacheType);
  }
  tStream << std::endl;

  tStream << m_tGlobalMemoryCachelineSize << std::endl;
  tStream << m_tGlobalMemorySize << std::endl;

  tStream << m_tImageSupport << std::endl;
  tStream << m_tMax2DImageHeight << std::endl;
  tStream << m_tMax2DImageWidth << std::endl;

  tStream << m_tMax3DImageDepth << std::endl;
  tStream << m_tMax3DImageHeight << std::endl;
  tStream << m_tMax3DImageWidth << std::endl;

  tStream << m_tLocalMemoryAreaSize << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tTypeOfLocalMemorySupported) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tTypeOfLocalMemorySupported).empty())
  {
    tStream << cl_device_local_mem_type_ToString(
      std::get<PARAM_VALUE>(m_tTypeOfLocalMemorySupported));
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tTypeOfLocalMemorySupported);
  }
  tStream << std::endl;

  tStream << m_tMaxConfiguredClockFrequency << std::endl;
  tStream << m_tMaxParallelComputeUnits << std::endl;
  tStream << m_tMaxConstantArgs << std::endl;
  tStream << m_tMaxConstantBufferSize << std::endl;
  tStream << m_tMaxMemoryObjectAllocation << std::endl;
  tStream << m_tMaxParameterSize << std::endl;
  tStream << m_tMaxSimultaneousImageObjectsRead << std::endl;
  tStream << m_tMaxSamplers << std::endl;
  tStream << m_tMaxWorkGroupSize << std::endl;
  tStream << m_tMaxWorkItemDimensions << std::endl;

  // Fetches an array of size std::get<PARAM_VALUE>(m_tMaxWorkItemDimensions).
  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize)
          << std::get<PARAM_DESC>(m_tMaxWorkItemSizes) << ": ";
  if (true == std::get<PARAM_ERROR>(m_tMaxWorkItemSizes).empty())
  {
    std::vector<size_t> tDimensions(std::get<PARAM_VALUE>(m_tMaxWorkItemDimensions));
    std::memcpy(
      &tDimensions[0],
      std::get<PARAM_VALUE>(m_tMaxWorkItemSizes).get(),
      tDimensions.size() * sizeof(size_t));
    if (false == tDimensions.empty())
    {
      DeviceInfo::printVectorHorizontally(tStream, tDimensions);
    }
    else
    {
      tStream << "No Maximum Work Item Size information found.\n";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tMaxWorkItemSizes) << '\n';
  }

  tStream << m_tLargestOpenCLDataTypeSize << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDeviceName)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceName).empty())
  {
    tStream << std::get<PARAM_VALUE>(m_tDeviceName).get();
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceName);
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(m_tDevicePlatform)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDevicePlatform).empty())
  {
    tStream << DeviceInfo::toHex(std::get<PARAM_VALUE>(m_tDevicePlatform));
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDevicePlatform);
  }

  tStream << std::endl;
  tStream << m_tPreferredCharVectorWidth << std::endl;
  tStream << m_tPreferredShortVectorWidth << std::endl;
  tStream << m_tPreferredIntVectorWidth << std::endl;
  tStream << m_tPreferredLongVectorWidth << std::endl;
  tStream << m_tPreferredFloatVectorWidth << std::endl;
  tStream << m_tPreferredDoubleVectorWidth << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDeviceProfile)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceProfile).empty())
  {
    tStream << std::get<PARAM_VALUE>(m_tDeviceProfile).get();
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceProfile);
  }
  tStream << std::endl;

  tStream << m_tProfilingTimerResolution << std::endl;

  DeviceInfo::writePrecisionInfo(
    tStream,
    m_nMaxDescriptionSize,
    tInfo,
    sIndent2,
    sHorizontalIndent,
    "No Single Precision Floating Point Capability found.",
    m_tSinglePrecisionFloatingPointCapability);

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDeviceType)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceType).empty())
  {
    for (auto i = OPEN_cl_device_type::BEGIN; i != OPEN_cl_device_type::END; ++i)
    {
      if (std::get<PARAM_VALUE>(m_tDeviceType) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorHorizontally(tStream, tInfo);
    }
    else
    {
      tStream << "No Device Type information found.";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceType) << '\n';
  }

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDeviceVendor)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceVendor).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tDeviceVendor))
    {
      tStream << std::get<PARAM_VALUE>(m_tDeviceVendor).get();
    }
    else
    {
      tStream << "No Device Vendor information found.";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceVendor);
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(MAX_DESCRIPTION_WIDTH) << std::get<PARAM_DESC>(m_tDeviceVendorID)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceVendorID).empty())
  {
    tStream << DeviceInfo::toHex(std::get<PARAM_VALUE>(m_tDeviceVendorID));
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceVendorID);
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDeviceVersion)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDeviceVersion).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tDeviceVersion))
    {
      tStream << std::get<PARAM_VALUE>(m_tDeviceVersion).get();
    }
    else
    {
      tStream << "No Device Version information found.";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDeviceVersion);
  }
  tStream << std::endl;

  tStream << sIndent2 << std::setw(m_nMaxDescriptionSize) << std::get<PARAM_DESC>(m_tDriverVersion)
          << ": ";
  if (true == std::get<PARAM_ERROR>(m_tDriverVersion).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tDriverVersion))
    {
      tStream << std::get<PARAM_VALUE>(m_tDriverVersion).get();
    }
    else
    {
      tStream << "No Driver Version information found.";
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(m_tDriverVersion);
  }
  tStream << std::endl;
}

void DeviceInfo::getHTMLKernel(std::ostream& tStream, std::vector<std::string>& tInfo)
  const noexcept
{
  static const std::regex tSpace(" ");
  StringTokenizer         tKernelTokens(std::regex_replace(m_sKernel, tSpace, "&nbsp;"), '\n');

  tInfo.assign(tKernelTokens.begin(), tKernelTokens.end());
  tInfo[0] = "<font face=\"Courier\" size=\"4\">" + tInfo[0];
  tInfo.back() += "</font>";
  setHTMLTableRow(
    tStream,
    "OpenCL Kernel",
    toHTMLTokens(tInfo),
    OpenCLToolTip("Simple OpenCL kernel"));
  tInfo.clear();

  setHTMLTableRow(
    tStream,
    "OpenCL Kernel Device Type",
    m_sKernelDeviceType,
    OpenCLToolTip("OpenCL kernel device type"));

  if (true == m_tKernelErrors.empty())
  {
    setHTMLTableRow(
      tStream,
      "OpenCL Kernel Errors",
      "None",
      OpenCLToolTip("Simple OpenCL kernel errors"));
  }
  else
  {
    tInfo.resize(m_tKernelErrors.size());
    std::transform(
      m_tKernelErrors.begin(),
      m_tKernelErrors.end(),
      tInfo.begin(),
      [](const std::string& sError) { return (DeviceInfo::tagError(sError)); });

    setHTMLTableRow(
      tStream,
      "OpenCL Kernel Errors",
      toHTMLTokens(tInfo),
      OpenCLToolTip("Simple OpenCL kernel errors"));
    tInfo.clear();
  }

  setHTMLTableRow(
    tStream,
    "OpenCL Kernel Data Size",
    m_nKernelDataSize,
    OpenCLToolTip("OpenCL kernel data Size"));

  setHTMLTableRow(
    tStream,
    "OpenCL Kernel Elapsed Time",
    m_nKernelElapsedTime,
    OpenCLToolTip("OpenCL kernel elapsed time"),
    OpenCLUnits::m_tNanoSeconds);
}

void DeviceInfo::getHTMLOutput(std::ostream& tStream) const noexcept
{
  std::vector<std::string> tInfo;
  getHTMLKernel(tStream, tInfo);

  DeviceInfo::writeHTMLTableRow(tStream, m_tAddressBits);
  DeviceInfo::writeHTMLTableRow(tStream, m_tAvailable);

  DeviceInfo::writeHTMLTableRow(tStream, m_tCompilerAvailable);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tDoublePrecisionFloatingPointConfiguration,
    OPEN_cl_device_fp_config::BEGIN,
    OPEN_cl_device_fp_config::END,
    "No Double Precision Floating Point Capability found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tIsLittleEndian);
  DeviceInfo::writeHTMLTableRow(tStream, m_tHasErrorCorrectionSupport);

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tExecutionCapabilities,
    OPEN_cl_device_exec_capabilities::BEGIN,
    OPEN_cl_device_exec_capabilities::END,
    "No Execution Capabilities found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tDeviceExtensions, "No Device Extensions found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tGlobalMemoryCacheSize);

  if (true == std::get<PARAM_ERROR>(m_tGlobalMemoryCacheType).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tGlobalMemoryCacheType),
      cl_device_mem_cache_type_ToString(std::get<PARAM_VALUE>(m_tGlobalMemoryCacheType)),
      std::get<PARAM_TOOL_TIP>(m_tGlobalMemoryCacheType));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tGlobalMemoryCacheType),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tGlobalMemoryCacheType)),
      std::get<PARAM_TOOL_TIP>(m_tGlobalMemoryCacheType));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tGlobalMemoryCachelineSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tGlobalMemorySize);

  DeviceInfo::writeHTMLTableRow(tStream, m_tImageSupport);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax2DImageHeight);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax2DImageWidth);

  DeviceInfo::writeHTMLTableRow(tStream, m_tMax3DImageDepth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax3DImageHeight);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMax3DImageWidth);

  DeviceInfo::writeHTMLTableRow(tStream, m_tLocalMemoryAreaSize);

  if (true == std::get<PARAM_ERROR>(m_tTypeOfLocalMemorySupported).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tTypeOfLocalMemorySupported),
      cl_device_local_mem_type_ToString(std::get<PARAM_VALUE>(m_tTypeOfLocalMemorySupported)),
      std::get<PARAM_TOOL_TIP>(m_tTypeOfLocalMemorySupported));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tTypeOfLocalMemorySupported),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tTypeOfLocalMemorySupported)),
      std::get<PARAM_TOOL_TIP>(m_tTypeOfLocalMemorySupported));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxConfiguredClockFrequency);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxParallelComputeUnits);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxConstantArgs);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxConstantBufferSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxMemoryObjectAllocation);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxParameterSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSimultaneousImageObjectsRead);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxSamplers);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxWorkGroupSize);
  DeviceInfo::writeHTMLTableRow(tStream, m_tMaxWorkItemDimensions);

  // An array of size std::get<PARAM_VALUE>(m_tMaxWorkItemDimensions).
  if (true == std::get<PARAM_ERROR>(m_tMaxWorkItemSizes).empty())
  {
    if (nullptr != std::get<PARAM_VALUE>(m_tMaxWorkItemSizes))
    {
      std::vector<size_t> tDimensions(std::get<PARAM_VALUE>(m_tMaxWorkItemDimensions));
      std::memcpy(
        &tDimensions[0],
        std::get<PARAM_VALUE>(m_tMaxWorkItemSizes).get(),
        tDimensions.size() * sizeof(size_t));

      if (false == tDimensions.empty())
      {
        std::stringstream      tWorkItemSizes;
        ScopedNumericFormatter tFormatter(tWorkItemSizes);
        tFormatter << "[";
        for (size_t i = 0; i < tDimensions.size() - 1; ++i)
        {
          tFormatter << tDimensions[i] << DeviceInfo::m_sHTMLCrossProduct;
        }
        tFormatter << tDimensions.back() << "]";
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tMaxWorkItemSizes),
          tWorkItemSizes,
          std::get<PARAM_TOOL_TIP>(m_tMaxWorkItemSizes));
      }
      else
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(m_tMaxWorkItemSizes),
          "No Maximum Work Item Size information found.",
          std::get<PARAM_TOOL_TIP>(m_tMaxWorkItemSizes));
      }
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tMaxWorkItemSizes),
        "No Maximum Work Item Size information found.",
        std::get<PARAM_TOOL_TIP>(m_tMaxWorkItemSizes));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tMaxWorkItemSizes),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tMaxWorkItemSizes)),
      std::get<PARAM_TOOL_TIP>(m_tMaxWorkItemSizes));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tLargestOpenCLDataTypeSize);

  if (true == std::get<PARAM_ERROR>(m_tDeviceName).empty())
  {
    std::string sValue(std::get<PARAM_VALUE>(m_tDeviceName).get());
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDeviceName),
      changeToHTMLGigaHertz(sValue),
      std::get<PARAM_TOOL_TIP>(m_tDeviceName));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDeviceName),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tDeviceName)),
      std::get<PARAM_TOOL_TIP>(m_tDeviceName));
  }

  if (true == std::get<PARAM_ERROR>(m_tDevicePlatform).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDevicePlatform),
      DeviceInfo::toHex(std::get<PARAM_VALUE>(m_tDevicePlatform)),
      std::get<PARAM_TOOL_TIP>(m_tDevicePlatform));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDevicePlatform),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tDevicePlatform)),
      std::get<PARAM_TOOL_TIP>(m_tDevicePlatform));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredCharVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredShortVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredIntVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredLongVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredFloatVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tPreferredDoubleVectorWidth);
  DeviceInfo::writeHTMLTableRow(tStream, m_tDeviceProfile);
  DeviceInfo::writeHTMLTableRow(tStream, m_tProfilingTimerResolution);

  if (true == std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability).empty())
  {
    for (auto i = OPEN_cl_device_fp_config::BEGIN; i != OPEN_cl_device_fp_config::END; ++i)
    {
      if (std::get<PARAM_VALUE>(m_tSinglePrecisionFloatingPointCapability) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tSinglePrecisionFloatingPointCapability),
        toHTMLTokens(tInfo),
        std::get<PARAM_TOOL_TIP>(m_tSinglePrecisionFloatingPointCapability));
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(m_tSinglePrecisionFloatingPointCapability),
        "No Single Precision Floating Point Capability information found.",
        std::get<PARAM_TOOL_TIP>(m_tSinglePrecisionFloatingPointCapability));
    }
    tInfo.clear();
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tSinglePrecisionFloatingPointCapability),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tSinglePrecisionFloatingPointCapability)),
      std::get<PARAM_TOOL_TIP>(m_tSinglePrecisionFloatingPointCapability));
  }

  DeviceInfo::writeHTMLTokens(
    tStream,
    m_tDeviceType,
    OPEN_cl_device_type::BEGIN,
    OPEN_cl_device_type::END,
    "No Device Type information found.");

  DeviceInfo::writeHTMLTableRow(tStream, m_tDeviceVendor);

  if (true == std::get<PARAM_ERROR>(m_tDeviceVendorID).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDeviceVendorID),
      DeviceInfo::toHex(std::get<PARAM_VALUE>(m_tDeviceVendorID)),
      std::get<PARAM_TOOL_TIP>(m_tDeviceVendorID));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(m_tDeviceVendorID),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(m_tDeviceVendorID)),
      std::get<PARAM_TOOL_TIP>(m_tDeviceVendorID));
  }

  DeviceInfo::writeHTMLTableRow(tStream, m_tDeviceVersion);
  DeviceInfo::writeHTMLTableRow(tStream, m_tDriverVersion);
}

std::string DeviceInfo::tagError(const std::string& sError) noexcept
{
  std::string sHTMLifiedError(sError);
  std::regex  tRightArrow("(^.*)(==>)(.*)");
  if (true == std::regex_match(sError, tRightArrow))
  {
    sHTMLifiedError = std::regex_replace(sHTMLifiedError, tRightArrow, "$1&rArr;$3");
  }

  std::regex tIntegerBaseRegex("(^ERROR: Invalid value: \\[CL_DEVICE_.*\\] )(.*)( \\[0x.*)(\\])( = "
                               ")(\\[.*)(\\])(\\.)");
  if (true == std::regex_match(sError, tIntegerBaseRegex))
  {
    sHTMLifiedError = std::regex_replace(
      sHTMLifiedError,
      tIntegerBaseRegex,
      "$1$2$3$4<sub>16</sub>$5$6$7<sub>10</sub>$8");
  }

  std::regex tRegEx("(^ERROR)(: )(.*)");
  if (true == std::regex_match(sHTMLifiedError, tRegEx))
  {
    return (std::regex_replace(sHTMLifiedError, tRegEx, "<blink><b>$1</b></blink>$2$3"));
  }
  else
  {
    static const std::string sErrorTag("<blink><b>ERROR</b></blink>: ");
    return (sErrorTag + sHTMLifiedError);
  }
}

template<class CL_DEVICE_INFO, class T1, class T2> void DeviceInfo::writeHTMLTokens(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T1, OpenCLError, OpenCLToolTip>&
                     tDeviceInfo,
  T2                 nBegin,
  T2                 nEnd,
  const std::string& sNotfound) noexcept
{
  std::vector<std::string> tInfo;
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    for (auto i = nBegin; i != nEnd; ++i)
    {
      if (std::get<PARAM_VALUE>(tDeviceInfo) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        toHTMLTokens(tInfo),
        std::get<PARAM_TOOL_TIP>(tDeviceInfo));
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        sNotfound,
        std::get<PARAM_TOOL_TIP>(tDeviceInfo));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T1, class T2> void DeviceInfo::writeHTMLTokens(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, T1, OpenCLError, OpenCLToolTip, OpenCLUnits>&
                     tDeviceInfo,
  T2                 nBegin,
  T2                 nEnd,
  const std::string& sNotfound) noexcept
{
  std::vector<std::string> tInfo;
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    for (auto i = nBegin; i != nEnd; ++i)
    {
      if (std::get<PARAM_VALUE>(tDeviceInfo) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        toHTMLTokens(tInfo),
        std::get<PARAM_TOOL_TIP>(tDeviceInfo),
        std::get<PARAM_UNITS>(tDeviceInfo));
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        sNotfound,
        std::get<PARAM_TOOL_TIP>(tDeviceInfo));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTokens(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      std::get<PARAM_VALUE>(tDeviceInfo).get(),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTokens(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip, OpenCLUnits>&
      tDeviceInfo) noexcept
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      std::get<PARAM_VALUE>(tDeviceInfo).get(),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo),
      std::get<PARAM_UNITS>(tDeviceInfo));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTableRow(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      std::get<PARAM_VALUE>(tDeviceInfo),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTableRow(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip, OpenCLUnits>&
      tDeviceInfo) noexcept
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      std::get<PARAM_VALUE>(tDeviceInfo),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo),
      std::get<PARAM_UNITS>(tDeviceInfo));
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTableRow(
  std::ostream& tStream,
  const std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
                     tDeviceInfo,
  const std::string& sNotFound,
  const char         cDelimiter)
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    if ('\0' != std::get<PARAM_VALUE>(tDeviceInfo)[0])
    {
      std::vector<std::string> tInfo;
      std::string              sInfo(std::get<PARAM_VALUE>(tDeviceInfo).get());
      DeviceInfo::extractTokens(tInfo, sInfo, cDelimiter);
      if (false == tInfo.empty())
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(tDeviceInfo),
          toHTMLTokens(tInfo),
          std::get<PARAM_TOOL_TIP>(tDeviceInfo));
      }
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        sNotFound,
        std::get<PARAM_TOOL_TIP>(tDeviceInfo));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::writeHTMLTableRow(
  std::ostream& tStream,
  const std::
    tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip, OpenCLUnits>&
                     tDeviceInfo,
  const std::string& sNotFound,
  const char         cDelimiter)
{
  if (true == std::get<PARAM_ERROR>(tDeviceInfo).empty())
  {
    if ('\0' != std::get<PARAM_VALUE>(tDeviceInfo)[0])
    {
      std::vector<std::string> tInfo;
      std::string              sInfo(std::get<PARAM_VALUE>(tDeviceInfo).get());
      DeviceInfo::extractTokens(tInfo, sInfo, cDelimiter);
      if (false == tInfo.empty())
      {
        setHTMLTableRow(
          tStream,
          std::get<PARAM_DESC>(tDeviceInfo),
          toHTMLTokens(tInfo),
          std::get<PARAM_TOOL_TIP>(tDeviceInfo),
          std::get<PARAM_UNITS>(tDeviceInfo));
      }
    }
    else
    {
      setHTMLTableRow(
        tStream,
        std::get<PARAM_DESC>(tDeviceInfo),
        sNotFound,
        std::get<PARAM_TOOL_TIP>(tDeviceInfo));
    }
  }
  else
  {
    setHTMLTableRow(
      tStream,
      std::get<PARAM_DESC>(tDeviceInfo),
      DeviceInfo::tagError(std::get<PARAM_ERROR>(tDeviceInfo)),
      std::get<PARAM_TOOL_TIP>(tDeviceInfo));
  }
}

void DeviceInfo::getAllDeviceInfo() noexcept
{
  getDeviceInfo(m_tAddressBits);
  getDeviceInfo(m_tAvailable);
  getDeviceInfo(m_tCompilerAvailable);
  getDeviceInfo(m_tDoublePrecisionFloatingPointConfiguration);
  getDeviceInfo(m_tIsLittleEndian);
  getDeviceInfo(m_tHasErrorCorrectionSupport);
  getDeviceInfo(m_tExecutionCapabilities);
  getDeviceInfo(m_tDeviceExtensions);
  getDeviceInfo(m_tGlobalMemoryCacheSize);
  getDeviceInfo(m_tGlobalMemoryCacheType);
  getDeviceInfo(m_tGlobalMemoryCachelineSize);
  getDeviceInfo(m_tGlobalMemorySize);
  getDeviceInfo(m_tImageSupport);
  getDeviceInfo(m_tMax2DImageHeight);
  getDeviceInfo(m_tMax2DImageWidth);
  getDeviceInfo(m_tMax3DImageDepth);
  getDeviceInfo(m_tMax3DImageHeight);
  getDeviceInfo(m_tMax3DImageWidth);
  getDeviceInfo(m_tLocalMemoryAreaSize);
  getDeviceInfo(m_tTypeOfLocalMemorySupported);
  getDeviceInfo(m_tMaxConfiguredClockFrequency);
  getDeviceInfo(m_tMaxParallelComputeUnits);
  getDeviceInfo(m_tMaxConstantArgs);
  getDeviceInfo(m_tMaxConstantBufferSize);
  getDeviceInfo(m_tMaxMemoryObjectAllocation);
  getDeviceInfo(m_tMaxParameterSize);
  getDeviceInfo(m_tMaxSimultaneousImageObjectsRead);
  getDeviceInfo(m_tMaxSamplers);
  getDeviceInfo(m_tMaxWorkGroupSize);
  getDeviceInfo(m_tMaxWorkItemDimensions);
  getDeviceInfo(m_tMaxWorkItemSizes);
  getDeviceInfo(m_tLargestOpenCLDataTypeSize);
  getDeviceInfo(m_tDeviceName);
  getDeviceInfo(m_tDevicePlatform);
  getDeviceInfo(m_tPreferredCharVectorWidth);
  getDeviceInfo(m_tPreferredShortVectorWidth);
  getDeviceInfo(m_tPreferredIntVectorWidth);
  getDeviceInfo(m_tPreferredLongVectorWidth);
  getDeviceInfo(m_tPreferredFloatVectorWidth);
  getDeviceInfo(m_tPreferredDoubleVectorWidth);
  getDeviceInfo(m_tDeviceProfile);
  getDeviceInfo(m_tProfilingTimerResolution);
  getDeviceInfo(m_tSinglePrecisionFloatingPointCapability);
  getDeviceInfo(m_tDeviceType);
  getDeviceInfo(m_tDeviceVendor);
  getDeviceInfo(m_tDeviceVendorID);
  getDeviceInfo(m_tDeviceVersion);
  getDeviceInfo(m_tDriverVersion);
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::getDeviceInfo(
  std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip>&
    tDeviceInfo) noexcept
{
  size_t       nReturnedSize = 0;
  const cl_int nStat         = clGetDeviceInfo(
    m_nDeviceID,
    scopedEnumToCLEnum(std::get<PARAM_NAME>(tDeviceInfo)),
    sizeof(T),
    &std::get<PARAM_VALUE>(tDeviceInfo),
    &nReturnedSize);

  if (CL_SUCCESS != nStat)
  {
    DeviceInfo::setError(
      nStat,
      std::get<PARAM_ERROR>(tDeviceInfo),
      std::get<PARAM_NAME>(tDeviceInfo));
  }
  else
  {
    std::get<PARAM_SIZE>(tDeviceInfo) = nReturnedSize;
  }
  m_nMaxDescriptionSize =
    std::max(m_nMaxDescriptionSize, static_cast<int>(std::get<PARAM_DESC>(tDeviceInfo).size()));
}

template<class CL_DEVICE_INFO, class T> void DeviceInfo::getDeviceInfo(
  std::tuple<const CL_DEVICE_INFO, std::string, size_t, T, OpenCLError, OpenCLToolTip, OpenCLUnits>&
    tDeviceInfo) noexcept
{
  size_t       nReturnedSize = 0;
  const cl_int nStat         = clGetDeviceInfo(
    m_nDeviceID,
    scopedEnumToCLEnum(std::get<PARAM_NAME>(tDeviceInfo)),
    sizeof(T),
    &std::get<PARAM_VALUE>(tDeviceInfo),
    &nReturnedSize);

  if (CL_SUCCESS != nStat)
  {
    DeviceInfo::setError(
      nStat,
      std::get<PARAM_ERROR>(tDeviceInfo),
      std::get<PARAM_NAME>(tDeviceInfo));
  }
  else
  {
    std::get<PARAM_SIZE>(tDeviceInfo) = nReturnedSize;
  }
  m_nMaxDescriptionSize =
    std::max(m_nMaxDescriptionSize, static_cast<int>(std::get<PARAM_DESC>(tDeviceInfo).size()));
}

// For array parameters with out units.
template<class CL_DEVICE_INFO, class T>
void DeviceInfo::getDeviceInfo(std::tuple<
                               const CL_DEVICE_INFO,
                               std::string,
                               size_t,
                               std::unique_ptr<T[]>,
                               OpenCLError,
                               OpenCLToolTip>& tDeviceInfo) noexcept
{
  size_t nReturnedSize = 0;
  cl_int nStat         = clGetDeviceInfo(
    m_nDeviceID,
    scopedEnumToCLEnum(std::get<PARAM_NAME>(tDeviceInfo)),
    0,
    nullptr,
    &nReturnedSize);
  if (CL_SUCCESS == nStat)
  {
    std::get<PARAM_VALUE>(tDeviceInfo).reset(new T[nReturnedSize]);
    nStat = clGetDeviceInfo(
      m_nDeviceID,
      scopedEnumToCLEnum(std::get<PARAM_NAME>(tDeviceInfo)),
      nReturnedSize,
      std::get<PARAM_VALUE>(tDeviceInfo).get(),
      nullptr);
  }

  if (CL_SUCCESS != nStat)
  {
    DeviceInfo::setError(
      nStat,
      std::get<PARAM_ERROR>(tDeviceInfo),
      std::get<PARAM_NAME>(tDeviceInfo));
  }
  else
  {
    std::get<PARAM_SIZE>(tDeviceInfo) = nReturnedSize;
  }
  m_nMaxDescriptionSize =
    std::max(m_nMaxDescriptionSize, static_cast<int>(std::get<PARAM_DESC>(tDeviceInfo).size()));
}

template<class CL_DEVICE_INFO> void DeviceInfo::setError(
  const cl_int         nStat,
  std::string&         sError,
  const CL_DEVICE_INFO tDeviceInfo) noexcept
{
  sError = DeviceInfo::getCLError(nStat, tDeviceInfo);
}

template<class CL_DEVICE_INFO>
std::string DeviceInfo::getCLError(const cl_int nStat, const CL_DEVICE_INFO tDeviceInfo)
{
  switch (nStat)
  {
    case CL_INVALID_DEVICE:
      return ("ERROR: Invalid device.");
      break;

    case CL_INVALID_VALUE:
    {
      std::stringstream tError;
      tError << "ERROR: Invalid value: [" << tDeviceInfo << "] ==> [0x" << std::internal
             << std::setfill('0') << std::setw(4) << std::hex << std::uppercase
             << scopedEnumToCLEnum(tDeviceInfo) << "] = [" << std::nouppercase << std::dec
             << scopedEnumToCLEnum(tDeviceInfo) << "].";
      return (tError.str());
    }
    break;

    case CL_OUT_OF_RESOURCES:
      return ("ERROR: Failure to allocate resources on device.");
      break;

    case CL_OUT_OF_HOST_MEMORY:
      return ("ERROR: Out of host memory.");
      break;

    // Unknown error.
    default:
      return ("ERROR: Unknown error.");
      break;
  };
}

void DeviceInfo::writePrecisionInfo(
  std::ostream&             tStream,
  const int                 nMaxDescriptionSize,
  std::vector<std::string>& tInfo,
  const std::string&        sIndent,
  const std::string&        sHorizontalIndent,
  const std::string&        sNotFound,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    cl_device_fp_config,
    OpenCLError,
    OpenCLToolTip>& tPrecisionInfo)
{
  tStream << sIndent << std::setw(nMaxDescriptionSize) << std::get<PARAM_DESC>(tPrecisionInfo)
          << ": ";
  if (true == std::get<PARAM_ERROR>(tPrecisionInfo).empty())
  {
    for (auto i = OPEN_cl_device_fp_config::BEGIN; i != OPEN_cl_device_fp_config::END; ++i)
    {
      if (std::get<PARAM_VALUE>(tPrecisionInfo) & scopedEnumToCLEnum(i))
      {
        tInfo.push_back(scopedEnumToString(i));
      }
    }
    if (false == tInfo.empty())
    {
      DeviceInfo::printVectorVertically(tStream, tInfo, sHorizontalIndent);
    }
    else
    {
      tStream << sNotFound << '\n';
    }
  }
  else
  {
    tStream << std::get<PARAM_ERROR>(tPrecisionInfo) << '\n';
  }
  tInfo.clear();
}

template<class T> std::string DeviceInfo::toHex(const T n)
{
  std::stringstream tHex;
  tHex << "0x" << std::hex << std::uppercase << uintptr_t(n);
  return (tHex.str());
}

// Required explicit template instantiations.

template std::ostream& operator<<<OPEN_cl_device_info, cl_device_id>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    cl_device_id,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

template std::ostream& operator<<<OPEN_cl_device_info, cl_uint>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>&) noexcept;

template std::ostream& operator<<<OPEN_cl_device_info, int>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    int,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

template std::ostream& operator<<<OPEN_cl_device_info_100, cl_uint>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_100,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

template std::ostream& operator<<<OPEN_cl_device_info_110_120, DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_110_120,
    std::string,
    size_t,
    DeviceInfo::OPEN_cl_bool,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

template std::ostream& operator<<<OPEN_cl_device_info_200, cl_uint>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_200,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info,
//! DeviceInfo::OPEN_cl_bool>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, DeviceInfo::OPEN_cl_bool>(
  std::tuple<
    OPEN_cl_device_info const,
    std::string,
    unsigned long,
    DeviceInfo::OPEN_cl_bool,
    OpenCLError,
    OpenCLToolTip>&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info, cl_device_id>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, cl_device_id>(
  std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    cl_device_id,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info, int>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, int>(std::tuple<
                                                                  const OPEN_cl_device_info,
                                                                  std::string,
                                                                  size_t,
                                                                  int,
                                                                  OpenCLError,
                                                                  OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info, long>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, long>(std::tuple<
                                                                   const OPEN_cl_device_info,
                                                                   std::string,
                                                                   size_t,
                                                                   std::unique_ptr<long[]>,
                                                                   OpenCLError,
                                                                   OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info, unsigned int>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, unsigned int>(
  std::tuple<
    OPEN_cl_device_info const,
    std::string,
    unsigned long,
    unsigned int,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits>&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info, unsigned long>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info, unsigned long>(
  std::tuple<
    OPEN_cl_device_info const,
    std::string,
    unsigned long,
    unsigned long,
    OpenCLError,
    OpenCLToolTip>&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info_100, int>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info_100, cl_uint>(
  std::tuple<
    const OPEN_cl_device_info_100,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info_100, size_t>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info_100, size_t>(
  std::tuple<
    const OPEN_cl_device_info_100,
    std::string,
    size_t,
    size_t,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info_110_120,
//! DeviceInfo::OPEN_cl_bool>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info_110_120, DeviceInfo::OPEN_cl_bool>(
  std::tuple<
    const OPEN_cl_device_info_110_120,
    std::string,
    size_t,
    DeviceInfo::OPEN_cl_bool,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info_200, cl_uint>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info_200, cl_uint>(
  std::tuple<
    const OPEN_cl_device_info_200,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::getDeviceInfo<OPEN_cl_device_info_200, size_t>(...).
template void DeviceInfo::getDeviceInfo<OPEN_cl_device_info_200, size_t>(
  std::tuple<
    const OPEN_cl_device_info_200,
    std::string,
    size_t,
    size_t,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::printVectorHorizontally(...) noexcept;
template void DeviceInfo::printVectorHorizontally(
  std::ostream&,
  std::vector<std::string>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::printVectorVertically<std::string>(...).
template void DeviceInfo::printVectorVertically<std::string>(
  std::ostream&,
  std::vector<std::string>&,
  const std::string&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info,
//! DeviceInfo::OPEN_cl_bool>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  std::tuple<
    OPEN_cl_device_info const,
    std::string,
    unsigned long,
    DeviceInfo::OPEN_cl_bool,
    OpenCLError,
    OpenCLToolTip> const&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, int>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, int>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    int,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, size_t>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, size_t>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    size_t,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, std::unique_ptr<char [],
//! std::default_delete<char []>>>(...).
template void DeviceInfo::
  writeHTMLTableRow<OPEN_cl_device_info, std::unique_ptr<char[], std::default_delete<char[]>>>(
    std::ostream&,
    std::tuple<
      OPEN_cl_device_info const,
      std::string,
      unsigned long,
      std::unique_ptr<char[], std::default_delete<char[]>>,
      OpenCLError,
      OpenCLToolTip> const&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, unsigned int>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info, unsigned int>(
  std::ostream&,
  std::tuple<
    OPEN_cl_device_info const,
    std::string,
    unsigned long,
    unsigned int,
    OpenCLError,
    OpenCLToolTip,
    OpenCLUnits> const&);

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_100,
//! OPEN_cl_command_queue_properties>(...).
template void
DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_100, OPEN_cl_command_queue_properties>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_100,
    std::string,
    size_t,
    OPEN_cl_command_queue_properties,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_100, cl_uint>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_100, cl_uint>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_100,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_110_120,
//! DeviceInfo::OPEN_cl_bool>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_110_120, DeviceInfo::OPEN_cl_bool>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_110_120,
    std::string,
    size_t,
    DeviceInfo::OPEN_cl_bool,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_200, cl_uint>(...).
template void DeviceInfo::writeHTMLTableRow<OPEN_cl_device_info_200, cl_uint>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info_200,
    std::string,
    size_t,
    cl_uint,
    OpenCLError,
    OpenCLToolTip>&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long,
//! OPEN_cl_command_queue_properties>(...).
template void
DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long, OPEN_cl_command_queue_properties>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    unsigned long,
    OpenCLError,
    OpenCLToolTip>&,
  OPEN_cl_command_queue_properties,
  OPEN_cl_command_queue_properties,
  const std::string&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long,
//! OPEN_cl_device_affinity_domain>(...).
template void
DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long, OPEN_cl_device_affinity_domain>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    unsigned long,
    OpenCLError,
    OpenCLToolTip>&,
  OPEN_cl_device_affinity_domain,
  OPEN_cl_device_affinity_domain,
  const std::string&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long,
//! OPEN_cl_device_svm_capabilities>(...).
template void
DeviceInfo::writeHTMLTokens<OPEN_cl_device_info, unsigned long, OPEN_cl_device_svm_capabilities>(
  std::ostream&,
  const std::tuple<
    const OPEN_cl_device_info,
    std::string,
    size_t,
    unsigned long,
    OpenCLError,
    OpenCLToolTip>&,
  OPEN_cl_device_svm_capabilities,
  OPEN_cl_device_svm_capabilities,
  const std::string&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTokens<OPEN_cl_device_info_100, unsigned long,
//! OPEN_cl_command_queue_properties>(...).
template void DeviceInfo::
  writeHTMLTokens<OPEN_cl_device_info_100, unsigned long, OPEN_cl_command_queue_properties>(
    std::ostream&,
    const std::tuple<
      const OPEN_cl_device_info_100,
      std::string,
      size_t,
      unsigned long,
      OpenCLError,
      OpenCLToolTip>&,
    OPEN_cl_command_queue_properties,
    OPEN_cl_command_queue_properties,
    const std::string&) noexcept;

//! \relates DeviceInfo
//! \brief This is an explicit instantiation of
//! DeviceInfo::writeHTMLTokens<OPEN_cl_device_info_200, unsigned long,
//! OPEN_cl_command_queue_properties>(...).
template void DeviceInfo::
  writeHTMLTokens<OPEN_cl_device_info_200, unsigned long, OPEN_cl_command_queue_properties>(
    std::ostream&,
    const std::tuple<
      const OPEN_cl_device_info_200,
      std::string,
      size_t,
      unsigned long,
      OpenCLError,
      OpenCLToolTip>&,
    OPEN_cl_command_queue_properties,
    OPEN_cl_command_queue_properties,
    const std::string&) noexcept;

